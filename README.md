Roofing Sales Booster & Project Management Platform - Raising the Roof on Efficiency & Profitability!<br>
<br>
<b>Any roofer with a ladder, a laptop and a little common sense should employ Odie</b>- a unique software-as-a-service (SaaS) platform built on best-of-breed technologies, aimed to optimize return on investment (ROI) from lead sources while managing your roofing business workflows.  Odie is a whitelabel platform, operating on your domain and with the look-and-feel of your corporate identity.  Armed with your material and labor cost list, Odie compiles a roof take-off, build sheet and provides your sales team with automated calculation of estimates.  Odie's AI engine considers roof pitch, permitting costs, commissions and other considerations in calculating your total job cost - right down to the number of 10 foot strips of drip edge, the lead boot count and of course, those pesky 8d coil nails.  Odie even considers your customized waste factors and profit margins to prepare a professional, turnkey estimate for e-signature, complete with gentle e-mail reminders akin to professional e-signature platforms.<br>
<br>
Odie does not stop there - when you win these jobs, Odie proceeds to meticulously manage each new job from cradle to grave with ease and transparency.  After Odie streamlines your workflow, you will have more time and the confidence to take on additional jobs.  To your pleasure, you discover that Odie is even better at finding homeowners with tired roofs than managing your jobs!  Odie then unveils Harvester&copy;, which identifies "roofing candidates" in your service area - homeowners of value-specified homes with roofs with 15 years of age or older!  Yep, Odie will devise and initiate a postcard marketing campaign to those homeowners, which you can track in Odie's lead management module.  You won't sweat the increased workload knowing Odie is handling everything from notifications to invoicing and of course, tracking your commissions as well as the profits with ease.<br>
<br>
Several years later, you will look back and wonder why all roofing contractors don't employ Odie.  Built by roofers and web artisans, Odie is the successful roofer's trade secret.  Make it yours as well.<br>
<br>
Odie is built upon an open source philosophy and thus, some magnificent APIs, including:<br>
&nbsp; Google Maps API - https://developers.google.com/maps/documentation/javascript/tutorial<br>
&nbsp; MailGun - https://documentation.mailgun.com/en/latest/api_reference.html<br>
&nbsp; People Data Labs (identity enhancement) - https://docs.peopledatalabs.com/docs<br>
&nbsp; Estated (property details enhancement) - https://estated.com/developers/docs/v4<br>
&nbsp; Twilio (SMS & interactive voice) - https://www.twilio.com/sms/api<br>
<br>
Launching with MVC v1.0a Release (July 23, 2020):<br>
&nbsp; Maximus - proprietary claims administration platform for assignment of windstorm claims<br>
<br>
Background<br>
After evaluating several project management platforms for a successful roofing venture in Southwest Florida, Gabriel Murphy concluded that there is a significant void in the market for a sales and platform management suite customized to and for the roofing trade.  In September 2019, Gabriel commenced development efforts on Odie.  In early 2020 and after migrating a large roofing firm from one CRM to another, Gabriel accelerated completion the MVP.  Shortly thereafter, Gabriel partnered with Abdul Majeed Shehzad (co-creator of the Odie platform) and together they have developed the MVP release candidate of Odie.<br>
<br>
Built with love ❤ in Lahore 🚀 and cash 💸 from the Cape ☀ <br>
