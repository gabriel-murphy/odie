<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOdmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odmails', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('mailable_type')->nullable();
            $table->unsignedBigInteger('mailable_id')->nullable();
            $table->unsignedBigInteger('template_id')->nullable();
            $table->string('from', 999)->nullable();
            $table->string('subject', 999)->nullable();
            $table->longText('body')->nullable();
            $table->string('module')->default('Odie');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odmails');
    }
}
