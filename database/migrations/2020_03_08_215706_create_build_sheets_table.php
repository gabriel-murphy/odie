<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('build_sheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('estimate_id');
            $table->string('type');
            $table->string('name')->nullable();
            $table->string('unit')->nullable();
            $table->double('unit_price')->nullable();
            $table->unsignedSmallInteger('actual_quantity')->default(1);
            $table->unsignedSmallInteger('adjusted_quantity')->default(0);
            $table->double('cost')->nullable();
            $table->double('waste_factor')->nullable();
            $table->double('waste_factor_cost')->nullable();
            $table->double('total_cost')->nullable();
            $table->json('additional_data')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('build_sheets');
    }
}
