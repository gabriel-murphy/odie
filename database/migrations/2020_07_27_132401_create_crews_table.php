<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crews', function (Blueprint $table) {
            $table->id();
            $table->string('name');                     // Example:  Gabriel's Tear-Off Crew // Atif's Metal Install Crew
            $table->string('description');              // Description of crew and notes about anything
            $table->string('type_id');                  // Tear off or Finish Crew (or both)?  Shingle, Tile, Metal install?
            $table->string('lead_id');                  // The Crew Team Leader (person who submits Tear-Off Report)
            $table->string('foreman_id');               // ID of the Foreman (English speaking person) who will visit jobsite when work underway
            $table->json('members')->nullable();        // IDs from the Users table of the members of the team (excluding lead_id and foreman_id)
            $table->double('rating')->nullable();       // Rating of Crew (1 to 5 stars)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crews');
    }
}
