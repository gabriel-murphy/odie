<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWasteFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    /* Waste Factors are set per client and can be configured seperately for material and labor and on a per roof material type basis, or the client can set an overall waste factor to apply to material and labor the same, but still for each type of roofing material */

    public function up()
    {
        Schema::create('waste_factors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->nullable();                         // NULL so these waste factors are set by default for future clients
            $table->bigInteger('roof_type_id');
            $table->double('overall_factor', 3, 3)->nullable();     // If NULL, then material and labor are set below
            $table->double('material_factor', 3, 3)->nullable();    // Load factor applied to material for this type of roof material
            $table->double('labor_factor', 3, 3)->nullable();       // Load factor applied to labor for this type of roof material
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_factors');
    }
}
