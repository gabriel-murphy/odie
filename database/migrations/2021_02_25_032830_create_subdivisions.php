<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubdivisions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdivisions', function (Blueprint $table) {
            $table->id();
            $table->string('legal_name')->nullable();
            $table->string('known_name')->nullable();
            $table->boolean('is_gated')->default(0);
            $table->string('website')->nullable();
            //$table->string('phone')->nullable();
            $table->string('number_units')->nullable();
            $table->string('number_phases')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subdivisions');
    }
}
