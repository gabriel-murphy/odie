<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('subdivision_id')->nullable();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->string('addressable_type');
            $table->unsignedBigInteger('addressable_id');
	        $table->string('street')->nullable();
	        $table->string('city')->nullable();
            $table->string('zip_code', 20)->nullable();
            $table->string('county')->nullable();
	        $table->string('state')->nullable();
	        $table->string('country')->nullable();
            $table->double('latitude')->nullable();         // MUST BE OF PARCEL CENTROID
            $table->double('longitude')->nullable();        // MUST BE OF PARCEL CENTROID
            $table->unsignedBigInteger('current_roof_type_id')->nullable();     // Should really exist at structure level
            $table->unsignedBigInteger('preferred_roof_type_id')->nullable();   // Should really exist at structure level
            $table->json('enhanced_data')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
