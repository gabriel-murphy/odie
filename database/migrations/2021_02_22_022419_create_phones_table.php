<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id')->nullable(); // clients id coming from clients table
            $table->unsignedBigInteger('associate_type_id'); // associate type_id to identify this is customer of field adjuster.
            $table->string('phoneable_type');
            $table->unsignedBigInteger('countries_id');
            $table->unsignedBigInteger('phone_types_id');
            $table->unsignedBigInteger('phoneable_id');
            $table->string('number')->nullable();
            $table->string('extension')->nullable();
            $table->softDeletes();
            $table->timestamp('verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phones');
    }
}
