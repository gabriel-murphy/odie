<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trade_type_id');                        // Roofing, but could be Gutters in the future.....
            $table->unsignedBigInteger('client_id')->nullable();                // If client of ODIE, ID in Client Table inserted here
            $table->char('name', '100')->nullable();
            $table->unsignedBigInteger('address_id');                           // ID in Addresses table of contractor's mailing address
            $table->char('license_number', '100')->nullable();
            $table->char('issue_state')->nullable();
            $table->unsignedBigInteger('person_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractors');
    }
}
