<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDumpFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dump_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id');                        // Dump fees (if outsourced) can be paid per roof type being dumped and either flat fee per project or cost per square basis
            $table->string('name');
            $table->unsignedBigInteger('roof_type_id')->nullable();
            $table->enum('cost_type', ['Fixed','Per Square']);
            $table->double('cost_per_dump', 5,2);
            $table->double('squares_per_dump', 5,2);
            $table->softDeletes();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dump_fees');
    }
}
