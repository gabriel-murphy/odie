<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubdivisionGateCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdivision_gate_codes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('person_id')->nullable();        // ID in persons table of the person who provided the gate code (User or Customer via CP)
            $table->char('code')->nullable();                           // The gate code to the gated community, as provided by the Customer in the Customer Portal (CP)
            $table->timestamp('entered_at')->nullable();                // When did the person_id enter the gate code?  (So we know how current the code is)
            $table->timestamp('verified_at')->nullable();               // Has this gate code been verified to work and if so, when did that occur?
            $table->unsignedBigInteger('verified_by')->nullable();      // Would be verified by crew members using the gate code to enter community to perform work
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subdivision_gate_codes');
    }
}
