<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('us_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('name', 35);
            $table->char('county', 80);
            $table->char('state_code', 2);
            $table->char('state', 20);
            $table->longText('us_zip_codes');
            $table->string('us_zip_codes_id')->nullable();
            $table->char('type', 23);
            $table->string('latitude');
            $table->string('longitude');
            $table->char('area_code', 33);
            $table->integer('population');
            $table->integer('households');
            $table->integer('median_income');
            $table->bigInteger('land_area');
            $table->bigInteger('water_area');
            $table->char('time_zone', 30);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('us_cities');
    }
}
