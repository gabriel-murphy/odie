<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('phase_id');
            $table->unsignedBigInteger('project_type_id');
            $table->unsignedBigInteger('user_id');
            $table->string('lead_source_ids')->nullable(); //lead_souces id
            $table->unsignedBigInteger('lead_source_people_id')->nullable(); //ultimately a people_id of specific referal selected in appoitments tab
            $table->unsignedBigInteger('property_id')->nullable();
            $table->unsignedBigInteger('billing_address_id')->nullable();
            $table->unsignedBigInteger('current_roof_type_id')->nullable();
            $table->unsignedBigInteger('preferred_roof_type_id')->nullable();
            $table->unsignedBigInteger('arial_report_provider')->nullable();
            $table->string('number_of_stories')->nullable();
            $table->string('roof_age')->nullable();
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->text('customer_notes')->nullable();
            $table->date('evaluation_date')->nullable();
            $table->date('sold_date')->nullable();                              // When estimate is signed, sold date updated to NOW()
            $table->date('permit_applied_date')->nullable();
            $table->date('permit_approved_date')->nullable();
            $table->date('start_date_tearoff')->nullable();
            $table->date('start_date_install')->nullable();
            $table->date('completion_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
