<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roof_manufacturer_id');
            $table->unsignedBigInteger('roof_type_id');
            $table->unsignedBigInteger('roof_product_id')->nullable();
            $table->string('name');
            $table->boolean('color_affects_price')->default(0);         // stats that does this product's color affect the price?
            $table->boolean('is_estimateable')->default(1);             // Can ODIE automatically estimate the cost of this material?
            $table->boolean('discontinued')->default(0);                             // Is this roof product still sold by the manufacturer?
    //        $table->boolean('color_impacts_price');                         // Does the color selection make a difference on the price of the product?
            $table->string('color_description')->nullable();                // description of colors for tile and natural products (non-painted)
            $table->string('code')->nullable();                             // roof manufacturer SKU/code for the product
            $table->string('url', 999)->nullable();                         // website of the product
            $table->json('samples')->nullable();                            // samples of the product -- unsure of this (GCM @ 3/5/2021)
            $table->string('specs')->nullable();                            // local directory path of detailed specifications of the product (data sheet in PDF)
            $table->string('brochure')->nullable();                         // brochure of the product (PDF) // marketing
            $table->string('notes')->nullable();                            // notes about use of the product
            $table->json('details')->nullable();                            // array of details that are non-selectable, such as weight and category
            $table->json('options')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_products');
    }
}
