<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaborsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id');
            $table->bigInteger('location_id')->nullable();
            $table->bigInteger('trade_type_id');
            $table->char('code', 10)->nullable();
            $table->char('giddy_up_code', 10)->nullable();
            $table->char('name', 100);
            $table->char('category', 30)->nullable();
            $table->char('description', 150)->nullable();
            $table->char('project_type_id', 30)->nullable();           // Is this labor for re-roof or new construction?
            $table->char('pitch_id', 30)->nullable();               // This platform is pitch-agnostic!  (price based on pitch)
            $table->bigInteger('tearoff_roof_type_id')->nullable(); // Match to the roof type being torn-off (if applicable)
            $table->bigInteger('new_roof_type_id')->nullable();     // Match to the roof type being installed (if applicable)
            $table->double('cost', 6, 2)->nullable();               // Unit cost is the client's (Roman) cost per unit (square)
            $table->enum('options', ['Yes','No'])->nullable();
            $table->enum('active', ['Yes','No'])->nullable();       
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('labors');
    }
}
