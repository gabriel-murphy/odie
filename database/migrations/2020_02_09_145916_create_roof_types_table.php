<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('roof_type_id')->nullable();
            $table->string('name');
            $table->string('long_name')->nullable();
            $table->json('additional_data')->nullable();
            $table->boolean('active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_types');
    }
}
