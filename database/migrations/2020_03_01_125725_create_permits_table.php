<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id')->nullable();                    // Roman Roofing
            $table->unsignedBigInteger('permit_jurisdiction_id')->nullable();       // What is the permitting jurisdiction?
            $table->unsignedBigInteger('trade_type_id')->nullable();                // What is the trade?  Only 2 and both are in the constants table....
            $table->unsignedBigInteger('structure_id')->nullable();                 // What is the structure ID (not address ID) tied to this permit?
            $table->unsignedBigInteger('contractor_id')->nullable();                // The person_id associated with the (roofing) contractor or "licensed professional" shown on the permit in public records
            $table->unsignedBigInteger('applicant_person_id')->nullable();          // The person_id associated with the Applicant shown on the permit
            $table->string('permit_number', '100')->nullable();                     // Assigned by the permitting jurisdiction
            $table->string('permit_status', '100')->nullable();                     // Status of this permit as shown in public record
            $table->string('permit_link', '100')->nullable();                       // link to a ODIE URL showing a screen capture or PDF of the actual permit data from the jurisdiction
            $table->timestamp('issued_date')->nullable();                           // This is a key date as it tells ODIE that the roofing contractor won this job recently
            $table->timestamp('expired_date')->nullable();
            $table->string('job_value', '100')->nullable();                         // Incredibly valuable data to ODIE!!
            $table->longText('description', '100')->nullable();                     // Scraped by Harvester from Permits
            $table->string('permit_type', '100')->nullable();                       // "Single Family Edition" for example.....
            $table->string('permit_sub_type', '100')->nullable();                   // Perhaps this column specifies re-roof versus a new construction (different types of permits in Lee County)
            $table->boolean('green_project')->default(0);                           // Will the application recycle the construction debris?  (taken from permit data via Harvester)
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permits');
    }
}
