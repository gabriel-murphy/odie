<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermitTierFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permit_tier_fee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('permit_jurisdictions_id');                      // What dumbass jurisdiction decided to do this stupid tier pricing?
            $table->double('tier_start_amount', 8,2);
            $table->double('tier_end_amount', 8,2)->nullable();     // Nullable because final tier goes to infinity pricing
            $table->double('tier_permit_fee', 6,2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permit_tier_fee');
    }
}
