<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->char('company_name', 50);                   // Legal name of client
            $table->char('short_name', 50);                     // Informal, shortened name
            $table->bigInteger('admin_user_id');
            $table->bigInteger('trade_type_id');                // could be more than one, such as roofing and gutters (1, 2)
            $table->bigInteger('headquarters_address_id');      // Address of headquarters, or "location 1"
            $table->string('main_phone');
            $table->string('fax_phone')->nullable();            // Nullable since most of the Platform's future customers will have disdain for antiquated tech like facsimile
            $table->string('billing_phone')->nullable();        // Billing # for customer inquiries shown on invoices
            $table->string('domain_name')->nullable();          // fqdn (fully qualified domain name) to use for whitelabel purposes
            $table->char('logo_image', 50)->nullable();
            $table->char('logo_image_2', 50)->nullable();
            $table->char('logo_image_3', 50)->nullable();
            $table->char('primary_hex_color', 6)->nullable();   // For whitelabel purposes and to customize the CSS of the theme to the client's logo colors
            $table->char('secondary_hex_color', 6)->nullable();
            $table->char('tertiary_hex_color', 6)->nullable();
            $table->char('appointment_minutes', '120')->nullable();         // Time between each estimate (how much time to schedule in Step 3 of OGW by default)
            $table->char('email_address', 100)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
