<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssociatablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associatables', function (Blueprint $table) {
            $table->unsignedBigInteger('associate_id');
            $table->unsignedBigInteger('associatable_type');
            $table->unsignedBigInteger('associatable_id');
            $table->unsignedBigInteger('association_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associatables');
    }
}
