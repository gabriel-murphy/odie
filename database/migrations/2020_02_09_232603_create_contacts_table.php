<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 50);
            $table->string('middle_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('title', 100)->nullable();
            $table->string('company', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('cell_phone', 15)->nullable();
            $table->string('home_phone', 15)->nullable();
            $table->string('work_phone', 15)->nullable();
            $table->string('extension', 20)->nullable();
            $table->string('fax_phone', 15)->nullable();
            $table->bigInteger('insurance_carrier_id')->nullable();
            $table->bigInteger('carrier_relationship_id')->nullable();
            $table->bigInteger('client_relationship_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
