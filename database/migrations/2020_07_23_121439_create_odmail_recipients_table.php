<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOdmailRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odmail_recipients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('odmail_id');
            $table->string('name')->nullable();
            $table->string('email');
            $table->enum('type', ['to', 'cc', 'bcc'])->default('cc');
            $table->timestamp('delivered_at')->nullable();
            $table->timestamp('opened_at')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odmail_recipients');
    }
}
