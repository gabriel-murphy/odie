<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplyHousesInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_houses_invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('supply_houses_id')->nullable();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->timestamp('invoice_date')->nullable();
            $table->timestamp('delivery_date')->nullable();
            $table->unsignedBigInteger('salesperson_id')->nullable();        // ID in persons table of the salesperson at the supply house on the invoice
            $table->unsignedBigInteger('address_id')->nullable();   // This is the jobsite address!
            $table->string('invoice_file')->nullable();             // Location of PDF of invoice for viewing
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_houses_invoices');
    }
}
