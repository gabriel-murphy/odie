<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * @structure_id Estimates are made on a per structure basis, and multiple structures can apply to a single project (address)
     * We we will have to pull all estimates for each structure for a project to see all estimates for that project.
     * @user_id User generating the estimate, the active user logged into Odie
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('structure_id');
            $table->unsignedBigInteger('roof_product_id');
            $table->string('name')->nullable();
            $table->boolean('insurance')->default(0);
            $table->boolean('financing')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimates');
    }
}
