<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->string('review_sources_id')->default(1);                // See ConstantsTableSeeder, line 683
            $table->string('name')->nullable();
            $table->string('picture')->nullable();                              // The local filepath to the avatar/photo associated with the reviewer
            $table->integer('rating')->nullable()->default('0');
            $table->longText('comment')->nullable();
            $table->longText('owner_reply')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
