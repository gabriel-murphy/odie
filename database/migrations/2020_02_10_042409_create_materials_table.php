<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * $properties this column will hold all the properties of the material width,length,
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id');
            $table->bigInteger('location_id')->nullable();              // NULL means the material applicable and available to ALL locations for the client
            $table->bigInteger('trade_type_id');
            $table->bigInteger('roof_material_category')->nullable();
            $table->string('code', 20)->nullable();                       // Assigned by Odie
            $table->string('giddy_up_code', 20)->nullable();              // Imported from Giddy-Up
            $table->string('name', 250);
            $table->string('short_name', 100)->nullable();                // Used to display to the customer on bids (if turned on)
            $table->string('description')->nullable();
            $table->text('comments')->nullable();                   // Comments about the material - only seen by client
            $table->double('cost', 7, 2)->nullable();                   // The client's cost per unit
            $table->unsignedInteger('unit')->nullable();                  // The material is purchased by the client in this measure of units
            $table->unsignedInteger('items_per_unit')->nullable();           // The material comes with this many per unit (such as nails per box)
            $table->string('units_purchased_at_once')->nullable();        // This is the quantity of units purchased by the client when the material is purchased (not necessary bought one at a time for pricing purposes) - takes into account quantity purchasing discounts
            $table->unsignedInteger('coverage_value')->nullable();                // This is the denominator in determining quantity of this material item on the estimates  (if the material covers 20 squares, then the factor_value is 20)
            $table->unsignedInteger('coverage_unit')->nullable();                      // This is the denominator units (squares, LF of drip edge, hip/ridge lineal feet) in determining quantity of this material item on the estimates
            $table->unsignedBigInteger('roof_component_id')->nullable();        // This is the column name in the roof_measurements table where Odie looks to determine calculations based on units AND measurements (example: LF of valley for valley metal)
            $table->unsignedBigInteger('roof_type_id')->nullable();                     // Does this material only apply to a certain roof_types_id?  NULL applies to all (nails, plywood, etc.)
            $table->unsignedBigInteger('project_type')->nullable();             // Is this material specific only to a certain project type?  For example, blind boot count (material) applying only to new construction (project type)?  Yes, this is rare...
            $table->unsignedBigInteger('roof_manufacturer_id')->nullable();    // Is this material tied to a certain roof manufacturer?
            $table->json('properties')->nullable();
            $table->boolean('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
