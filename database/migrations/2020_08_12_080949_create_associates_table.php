<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssociatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peoples', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('associate_type_id');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('nick_name')->nullable();                  // A name someone is known by that is not their legal name
            $table->string('gender_id')->nullable();                  // The person's gender
            $table->string('company', 255)->nullable();
            $table->json('enhanced_data')->nullable();
            $table->bigInteger('insurance_carrier_id')->nullable();
            $table->bigInteger('carrier_relationship_id')->nullable();
            $table->bigInteger('client_relationship_id')->nullable();
            $table->string('title', 100)->nullable();
            $table->unsignedBigInteger('relationship_id')->nullable(); // coming from constants table property relationship is basically connection to property
            $table->longText('customer_notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associates');
    }
}
