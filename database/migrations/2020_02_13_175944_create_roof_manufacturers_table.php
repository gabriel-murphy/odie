<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_manufacturers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('short_name');
            $table->string('long_name');
            $table->string('series_name')->nullable();              // Each manufacturer might refer to the "series" as something else, such as "profiles" or the series is listed by a property, such as concrete and clay tiles.  This would be a "Type"
            $table->string('website')->nullable();
            $table->string('logo_url', 999)->nullable();    // Fetched by the Platform once to store logo locally for display with the Platform
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_manufacturers');
    }
}
