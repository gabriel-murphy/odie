<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colors', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->bigInteger('roof_manufacturers_id')->nullable();                // Colors are tied to manufacturers, but may also be tied within various series (see roof_material_colors_matrix)
//            $table->bigInteger('roof_manufacturers_series_types_id')->nullable();   // Colors can also be tied to a series (such as Eagle)
            $table->bigInteger('roof_manufacturers_products_id')->nullable();       // Colors can also be tied to a specific product lines
//            $table->bigInteger('roof_manufacturers_finish_types_id')->nullable();   // Colors can also be tied to a finish type (Gulf Coast Metals)
              $table->char('name', '50');
              $table->char('palette_id', '50')->nullable(); //remove this
              $table->char('image_url', '200')->nullable();
              $table->boolean('active')->default(1);
              $table->string('description')->nullable();
              $table->string('pairing')->nullable();
//            $table->char('code', '10')->nullable();
//            $table->double('reflectivity', '3,2')->nullable();
//            $table->double('emissivity', '3,2')->nullable();
//            $table->unsignedTinyInteger('sri')->nullable();
//            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colors');
    }
}
