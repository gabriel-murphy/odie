<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfitMarginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profit_margins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('roof_types_id');                    // Profit margins configurable on a material type basis
            $table->double('total_markup', 4,2)->nullable();        // Profit applied to total cost of project and if NOT NULL, it ignores materials_markup, labor_markup and permit_markup below
            $table->double('materials_markup', 4,2)->nullable();
            $table->double('labor_markup', 4,2)->nullable();
            $table->double('permit_markup', 4,2)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profit_margins');
    }
}
