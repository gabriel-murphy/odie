<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofManufacturersProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_manufacturers_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('roof_manufacturers_series_types_id')->nullable();
            $table->char('name', '50');
            $table->char('code', '100')->nullable();        // Set by the manufacturer to identify the product line item
            $table->char('website', '200')->nullable();     // The URL to info on the specific roofing product line item
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_manufacturers_products');
    }
}
