<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofWarrantiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_warranties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->nullable();        // Nullable as warranties are manufacturer driven (such as GAF Golden Pledge) and thus, should not be available to all clients who can and likely offer the same product-based warranty(ies)    
            $table->bigInteger('roof_manufacturers_id');
            $table->bigInteger('roof_types_id');                // Manufacturers offer warranties on types of roofing materials (shingle, metal, etc.)
            $table->char('short_name', 30);
            $table->enum('priced_per_square', ['Yes', 'No'])->nullable();   // Is the warranty priced per square of the project?
            $table->double('cost_per_square', 5,2)->nullablle();            // Cost of the warranty on a per square basis
            $table->double('fixed_cost', 5,2)->nullable();                  // Cost of each warranty regardless of project size
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_warranties');
    }
}
