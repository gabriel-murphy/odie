<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_structure', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id');
            $table->enum('fixed_amount', ['No', 'Yes'])->nullable();    // Default is No to setting commission_rate as fixed amount
            $table->double('commission_amount', 6,2)->nullable();       // Default is No to setting commission_rate as fixed amount
            $table->enum('percent_sale', ['Yes', 'No'])->nullable();    // Default is Yes, otherwise, comm is % of gross margin
            $table->enum('percent_margin', ['No', 'Yes'])->nullable();  // Default is No to setting commission_rate as % of gross margin
            $table->double('sale_percentage', 5,2)->nullable();         // Commission as a % of the sale amount 
            $table->double('margin_percentage', 5,2)->nullable();       // Commission as a % of the gross margin amount
            $table->enum('insurance_bonus', ['Yes', 'No'])->nullable(); // Bonus paid for insurance deals
            $table->double('insurance_bonus_amount', 6,2)->nullable();  // Amount of Bonus payment
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_structure');
    }
}
