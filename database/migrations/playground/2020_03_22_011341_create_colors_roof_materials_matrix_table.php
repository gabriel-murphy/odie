<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorsRoofMaterialsMatrixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colors_roof_materials_matrix', function (Blueprint $table) {
            $table->id();
            $table->string('dB_location');      // The column name of what this color is tied to.  A series, a line, a finish?  This is the table name, such as roof_manufacturers_lines.name
            $table->bigInteger('dB_id');        // The ID of the of the row from the location above, so we can determine the material
            $table->bigInteger('color_id');     // The ID of the color from colors.id
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colors_roof_materials_matrix');
    }
}
