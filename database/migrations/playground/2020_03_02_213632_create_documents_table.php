<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('estimate_id');
            $table->bigInteger('document_types_id');
            $table->string('location', 100);        // This is the location of the document on the server
            $table->enum('require_customer_signature', ['no','yes'])->nullable();       // No by default
            $table->enum('require_customer_initials', ['no','yes'])->nullable();       // If yes, Platform requests initials on all pages but signature page
            $table->enum('customer_viewable', ['no','yes'])->nullable();                // Can be seen in customer portal?
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
