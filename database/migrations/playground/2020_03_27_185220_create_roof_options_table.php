<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_options', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roof_manufacturer_id');
            $table->string('roof_option_group');
            $table->string('name');
            $table->string('value')->nullable();
            $table->text('additional_data')->nullable();
            $table->boolean('active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_options');
    }
}
