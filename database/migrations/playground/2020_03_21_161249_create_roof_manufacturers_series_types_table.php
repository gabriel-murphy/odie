<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofManufacturersSeriesTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_manufacturers_series_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('website')->nullable();
            $table->bigInteger('roof_manufacturer_id')->nullable();         // To what roof manufacturer does this series belong?  The higharchy is roof_manufacturers::roof_manufacturers_series_types::roof_manufacturers_lines
            $table->bigInteger('roof_types_roof_manufacturers_matrix_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_manufacturers_series_types');
    }
}
