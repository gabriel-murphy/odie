<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefaultMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id');
            $table->bigInteger('roof_layers_id');                   //  The layer of the roof to which the default applies 
            $table->bigInteger('material_id');                      // The material.id which is the default for something below.
            $table->bigInteger('roof_types_id')->nullable();        
            $table->bigInteger('roof_components_id')->nullable();
            $table->bigInteger('roof_material_categories_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_materials');
    }
}
