<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFreeMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id');
            $table->bigInteger('material_id');      // The material.id which is provided for free at the quantity and on each estimate for each roof_type below
            $table->bigInteger('roof_layers_id');
            $table->bigInteger('quantity');         // The quantity of material to be provided for free on each estimate
            $table->bigInteger('roof_types_id');    // The roof type in which the free material qualifies, NULL is all roof types will be provided the the material and at the quanitity indicated for free and on each 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_materials');
    }
}
