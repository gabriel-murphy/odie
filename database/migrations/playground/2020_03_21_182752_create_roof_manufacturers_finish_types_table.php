<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofManufacturersFinishTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        ## This Seeder applies generally to metal roofs only (which have finishes, versus shinge and tile)

        Schema::create('roof_manufacturers_finish_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('detail');
            $table->bigInteger('roof_manufacturers_id')->nullable();                // What manufacturer do these finish options belong to (if applicable to all lines)?
            $table->bigInteger('roof_manufacturers_lines_id')->nullable();          // What product line do these finish options belong to?
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_manufacturers_finish_types');
    }
}
