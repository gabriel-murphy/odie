<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_repairs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('maximus_claim_id');
            $table->date('invoice_date')->nullable();
            $table->unsignedBigInteger('invoice_amount')->nullable();               // Amount the client is attempting to collect for the temp repair
            $table->string('invoice_number')->nullable();                           // Invoice number associated with the invoice submitted for the temp repair
            $table->date('damage_photo_count')->nullable();                         // # of damage photos submitted by the carrier
            $table->date('completed_on')->nullable();                               // Date Temp Repair was completed
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_repairs');
    }
}
