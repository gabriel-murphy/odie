<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('phase_id')->nullable();                     // The ID of the group names for the phases
            $table->bigInteger('client_id')->nullable();                    // Nullable so these phases and phase groups are available out-of-the-box for all clients
            $table->unsignedBigInteger('module_id')->nullable();
            $table->boolean('active')->default(1);
            $table->string('name', 25);
            $table->string('hex_color', 20)->nullable();
            $table->string('icon', 50)->nullable();
            $table->boolean('customer_viewable')->default(1);
            $table->unsignedBigInteger('order_by')->nullable();
            $table->unsignedSmallInteger('comes_after_id')->nullable();
            $table->text('additional_data')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phases');
    }
}
