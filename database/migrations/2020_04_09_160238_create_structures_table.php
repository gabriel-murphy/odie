<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('structures', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('address_id')->nullable();       // Structures are tied to the same address
            $table->string('name')->nullable();             // Main House, Detached Garage, In-Law Quarters
            $table->unsignedBigInteger('current_roof_type_id')->nullable();
            $table->unsignedBigInteger('preferred_roof_type_id')->nullable();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->json('penetrations')->nullable();
            $table->json('measurements')->nullable();       // JSON output of QuickMeasure API
            $table->json('objects')->nullable();            // JSON output of ODIE AI detection of objects on roof from an image
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('structures');
    }
}
