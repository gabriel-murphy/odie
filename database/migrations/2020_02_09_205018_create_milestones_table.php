<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // Milestones trigger events, such as notifications (tasks, emails, SMS notifications) and/or phase updates
    public function up()
    {
        Schema::create('milestones', function (Blueprint $table) {
            $table->id();                                                       // Milestones are the smallest increments in the progression of an opportunity / claim / project
            $table->char('name', 50);                                           // Name of the milestone - example, "Tear-Off Underway"
            $table->unsignedBigInteger('sub_phase_id')->nullable();             // Sub_phases MAY consist of one or more milestones (example: "tear-off underway" is sub_phase of "Dry In", which is a phase of production)
            $table->unsignedBigInteger('phase_id')->nullable();                 // Phases consist of one or more sub_phases grouped together
            $table->unsignedBigInteger('progress_board_id')->nullable();        // In which progress_board does this milestone belong?  (harvester, opportunity, maximus, project, halo)
            $table->unsignedBigInteger('client_id')->nullable();                // Roman Roofing, obviously!
            $table->unsignedBigInteger('module_id')->nullable();
            $table->char('description', 200)->nullable();
            $table->bigInteger('comes_after_id')->nullable();           // Not sure what I was thinking back on Feb 9 2020....
            $table->bigInteger('responsible_id')->nullable();           // ID from TBD constants (Customer, Client, ODIE, Government, Insurance Carrier, Attorney) who is the one RESPONSIBLE for making the milestone occur (or not).
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('milestones');
    }
}
