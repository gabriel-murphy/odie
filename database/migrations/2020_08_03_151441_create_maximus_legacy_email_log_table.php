<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaximusLegacyEmailLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maximus_legacy_email_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('claim_id');
            $table->unsignedTinyInteger('claim_submission_type_id')->default(1);
            $table->string('message_id')->nullable();
            $table->string('recipients')->nullable();
            $table->string('message_subject')->nullable();
            $table->longText('message_body')->nullable();
            $table->string('email')->nullable();
            $table->enum('type', ['to', 'cc', 'bcc'])->default('cc');
            $table->timestamp('delivered_at');
            $table->timestamp('resubmitted_at')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maximus_legacy_email_log');
    }
}
