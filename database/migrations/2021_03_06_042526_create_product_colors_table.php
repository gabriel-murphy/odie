<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_colors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roof_manufacturers_product_id')->nullable();
            $table->unsignedBigInteger('roof_product_color_id')->nullable();
//            $table->foreign('roof_manufacturers_products_id')->references('id')->on('roof_products');
//            $table->foreign('roof_product_color_id')->references('id')->on('roof_product_colors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_colors');
    }
}
