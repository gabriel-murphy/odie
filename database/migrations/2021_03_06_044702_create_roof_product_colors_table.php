<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofProductColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_product_colors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('roof_product_id')->nullable();       // Colors can also be tied to a specific product lines for shingle
            $table->char('name', '50');
            $table->unsignedBigInteger('palette_id')->nullable();
            $table->char('image_url', '200')->nullable();
            $table->boolean('active')->default(1);              // is available by clients ?
            $table->boolean('discontinued')->default(0);        // is available by manufacturer ? default set false mean it is available
            $table->string('description')->nullable();
            $table->string('pairing')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_product_colors');
    }
}
