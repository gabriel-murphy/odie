<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplyHousesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supply_houses_items', function (Blueprint $table) {
            $table->id();
            $table->string('item_number')->nullable();
            $table->longText('description')->nullable();
            $table->char('unit_id')->nullable();                    // Unit ID of how item is sold (see constants table)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supply_houses_items');
    }
}
