<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoofComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roof_components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('link_estimating', ['no', 'yes'])->nullable();
            $table->char('roof_measurements_column', 40)->nullable();
            $table->double('divisor', 5,2)->nullable();                     // If NULL, then divide by 1 (this is used for squares to convert from square feet)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roof_components');
    }
}
