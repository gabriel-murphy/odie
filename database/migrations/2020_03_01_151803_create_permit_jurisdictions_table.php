<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermitJurisdictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    /* The Permit Fee Auto-Calcuation by the Platform is a key selling point and thus, there will NOT be a CRUD for the 
    Permitting, as it is the Plaform's operators responsiblity to ensure the permit fees are updated as may be changed, so
    that clients (Roman) do NOT have to think about calcuation of the permit fee - the Platform always does it for them.
    NO other CRM platform does auto-calcuation of permitting fees regardless of jurisdiction! */

    /*
     * If the jurisdiction bases their fee on a range of prices for the projector.
     * Ex. Project fee of $0-14,999 is permit fee of $200, $15,000 to $29,999 is $383, etc.
     * Program looks at permit_tier_fee table for the permit fee tiers for the jurisdiction.
     * */

    public function up()
    {
        Schema::create('permit_jurisdictions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('type', ['county','city']);
            $table->bigInteger('us_cities_id')->nullable();
            $table->decimal('fixed_fee', 8,2)->nullable();
            $table->decimal('per_square_fee', 8,2)->nullable();
            $table->boolean('tier_fee')->default(0);
            $table->decimal('percentage_fee', 2,2)->nullable();
            $table->string('document')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permit_jurisdictions');
    }
}
