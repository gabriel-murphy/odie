<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaximusClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maximus_claims', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();                                              // User ID who submitted the AoB notification
            $table->unsignedBigInteger('estimator_id')->nullable();                             // The Estimator for the Client who signed the AoB or Insurance Proceeds
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('mortgage_company_id')->nullable();
            $table->unsignedBigInteger('insurance_carrier_id')->nullable();                                 // The Insurance Carrier the Client is going to war with...
            $table->unsignedBigInteger('desk_adjuster_id')->nullable();                         // Contact_ID of Desk Adjuster
            $table->unsignedBigInteger('field_adjuster_id')->nullable();                        // Contact_ID of Field Adjuster
            $table->unsignedBigInteger('escalator_id')->nullable();
            $table->unsignedBigInteger('claim_type_id')->default(0);                                 // Claim can either be an AoB or for Insurance Proceeds
            $table->string('mortgage_account_number')->nullable();
            $table->string('claim_number')->nullable();
            $table->boolean('previously_denied')->default(0);                             // Need to as user if a claim for this same loss was previously denied
            $table->string('policy_number')->nullable();
            $table->date('date_of_loss')->nullable();                                                       // This should default to September 10, 2017 on the front-end UI
            $table->date('aob_signed_on')->nullable();                                          // This should match the date shown on the AoB unless it is a "for insurance proceeds" deal in which case it is NULL
            $table->unsignedFloat('xactimate_amount')->nullable();                              // The amount we are demanding from the carrier, as shown in the Xactimate
            $table->unsignedBigInteger('deductible_amount')->nullable();                        // The amount of the policy holder's deductible
            $table->unsignedBigInteger('property_id')->nullable();
            $table->longText('description')->nullable();                                        // Text area for user to enter damage description
            $table->unsignedTinyInteger('roof_leak_type_id')->default(0);                 // Type of leak on the property
            $table->unsignedMediumInteger('minimum_damaged_units')->nullable();                  // # of minimum damaged units of roof material (tile, shingle) observed
            $table->boolean('temp_repair_needed')->default(0);                           // Temp repair needed?
            $table->timestamp('adjuster_meeting_at')->nullable();                         // Date and time of adjuster meeting
            $table->longText('adjuster_meeting_notes')->nullable();                             // Estimator notes from the adjuster meeting
            $table->unsignedBigInteger('status_id')->nullable();                                // default pending
            $table->unsignedBigInteger('coverage_type_id')->nullable();     // What type of coverage does the carrier ultimately provide on this claim?
            $table->unsignedDouble('coverage_amount')->nullable();     // Coverage amount if partial
            $table->string('denial_reason', 999)->nullable();     // Reason if denied
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maximus_claims');
    }
}
