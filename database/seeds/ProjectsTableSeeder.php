<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = array(
            array(
                "id" => 1,
                "client_id" => 1,
                "customer_id" => 1,
                "phase_id" => 8,
                "project_type_id" => 2,
                "user_id" => 22,
                "property_id" => 1,
                "billing_address_id" => NULL,
                "current_roof_type_id" => 1,
                "preferred_roof_type_id" => 10,
                "name" => NULL,
                "description" => "adas",
                "customer_notes" => "adsad",
                "evaluation_date" => NULL,
                "sold_date" => NULL,
                "permit_applied_date" => NULL,
                "permit_approved_date" => NULL,
                "start_date_tearoff" => NULL,
                "start_date_install" => NULL,
                "completion_date" => NULL,
                "deleted_at" => NULL,
                "created_at" => "2020-04-10 15:48:27",
                "updated_at" => "2020-04-10 15:48:27",
            ),
        );

        foreach ($projects as $project)
        {
            \App\Project::create($project);
        }
    }
}
