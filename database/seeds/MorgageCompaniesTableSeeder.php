<?php

use Illuminate\Database\Seeder;

class MortgageCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mortgage_companies = array(
            array(
                "id" => 1,
                "name" => 'Third Federal',
            ),
            array(
                "id" => 1,
                "name" => 'US Bank Home Mortgage',
            ),
        );
        foreach ($mortgage_companies as $mortgage_company) {
            \App\MorgageCompany::create($mortgage_company);
        }
    }
}

