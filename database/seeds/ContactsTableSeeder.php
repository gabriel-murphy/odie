<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adjusters = array(
            array(
                "id" => 1,
                "first_name" => "Michael",
                "last_name" => "Miller",
                "work_phone" => "8444584300",
                "extension" => "3097632270",
                "fax_phone" => '8442363646',
                "insurance_carrier_id" => 1,
                "carrier_relationship_id" => 3,     // Field Adjuster
            ),
            array(
                "id" => 2,
                "first_name" => "April",
                "last_name" => "Crawford",
                "email" => "acrawford@upcinsurance.com",
                "work_phone" => "7278957737",
                "extension" => "5709",
                "insurance_carrier_id" => 2,
                "carrier_relationship_id" => 2,
            ),
            array(
                "id" => 3,
                "first_name" => "Janelle",
                "work_phone" => "3366631837",
                "insurance_carrier_id" => 3,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 4,
                "first_name" => "Niles",
                "last_name" => "Wood",
                "work_phone" => "3364469876",
                "insurance_carrier_id" => 3,
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 5,
                "first_name" => "Tobin",
                "last_name" => "Yager",
                "email" => "tkyager@comcast.net",
                "work_phone" => "2074501894",
                "insurance_carrier_id" => 5,
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 6,
                "first_name" => "Loretta",
                "last_name" => "James",
                "work_phone" => "8006750145",
                "extension" => "1336",
                "insurance_carrier_id" => 6,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 7,
                "first_name" => "Jacqueline",
                "last_name" => "Dimery",
                "email" => "cat@asicorp.org",
                "work_phone" => "8662745677",
                "extension" => "1329",
                "insurance_carrier_id" => 11,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 8,
                "first_name" => "Kevin",
                "last_name" => "Wisniewski",
                "email" => "claims.1500miles@gmail.com",
                "work_phone" => "3095321323",
                "insurance_carrier_id" => 2,
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 9,
                "first_name" => "Daniel",
                "last_name" => "Galdiano",
                "email" => "dgaldiano@alacritysolutions.com",
                "work_phone" => "8004110013",
                "extension" => "1006",
                "insurance_carrier_id" => 5,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 10,
                "first_name" => "Donald",
                "last_name" => "Rench",
                "work_phone" => "2392482003",
                "insurance_carrier_id" => 5,
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 11,
                "first_name" => "Ted",
                "last_name" => "Fiocati",
                "work_phone" => "2397770290",
                "email" => "tfiocati@upcinsurance.com",     // Guess based on other naming conventions used by UPC
                "insurance_carrier_id" => 2,                        // UPC
                "carrier_relationship_id" => 3,                     // Field Adjuster
            ),
            array(
                "id" => 12,
                "first_name" => "Jonathan",
                "last_name" => "Walker",
                "work_phone" => "8662745677",
                "extension" => "2056",
                "fax_phone" => '8662743299',
                "insurance_carrier_id" => 5,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 13,
                "first_name" => "Daniel",
                "last_name" => "Dessum",
                "email" => "ddessum@heritagepci.com",
                "work_phone" => "7273627200",
                "extension" => "7537",
                "insurance_carrier_id" => 4,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 14,
                "first_name" => "Ketari",
                "last_name" => "Cole",
                "work_phone" => "8777445224",
                "extension" => "8150",
                "insurance_carrier_id" => 6,            // Frontier
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 15,
                "first_name" => "Steve",
                "last_name" => "Winalis",
                "email" => "acmeadjusting@gmail.com",
                "work_phone" => "2399403200",
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 16,
                "first_name" => "Dave",
                "last_name" => "Marrs",
                "email" => "marrsadj@gmail.com",
                "work_phone" => "4699518976",
                "insurance_carrier_id" => 8,            // USAA
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 17,
                "first_name" => "Josephine",
                "last_name" => "Burdett",
                "email" => "jburdett@upcinsurance.com",
                "work_phone" => "8008614370",
                "extension" => "5835",
                "fax_phone" => "8003805053",
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 18,
                "first_name" => "Taylor",
                "last_name" => "Angell",
                "work_phone" => "7277736946",
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 19,
                "first_name" => "Robert",
                "last_name" => "Martin",
                "email" => "rmartin@thig.com",
                "work_phone" => "3523331736",
                "insurance_carrier_id" => 5,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 20,
                "first_name" => "Gary",
                "last_name" => "Widich",
                "email" => "gary.widich@fpclaimservices.com",
                "insurance_carrier_id" => 9,            // Florida Penninsula
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 21,
                "first_name" => "Antoine",
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 22,
                "first_name" => "Charlotte",
                "last_name" => "McGill-Colyer",
                "email" => "cat@asicorp.org",
                "work_phone" => "8662745677",
                "extension" => "1156",
                "fax_phone" => "8668401905",
                "insurance_carrier_id" => 5,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 23,
                "first_name" => "Jason",
                "last_name" => "Shelton",
                "company" => "EFI Global",
                "title" => "Senior Forensic Engineer",
                "email" => "jason.shelton@efiglobal.com",
                "work_phone" => "3212519091",
                "cell_phone" => "8137120247",
                "insurance_carrier_id" => 5,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 24,
                "first_name" => "Dave",
                "work_phone" => "6783162914",
                "insurance_carrier_id" => 13,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 25,
                "first_name" => "Kiarra",
                "last_name" => "Rainer",
                "work_phone" => "8037482830",
                "email" => "cat.exam144@seibels.com",
                "insurance_carrier_id" => 10,            // Saint John's
                "carrier_relationship_id" => 2,          // Desk Adjuster
            ),
            array(
                "id" => 26,
                "first_name" => "Robert",
                "last_name" => "Campbell",
                "insurance_carrier_id" => 10,            // Saint John's
                "carrier_relationship_id" => 3,          // Field Adjuster
            ),
            array(
                "id" => 27,
                "first_name" => "Mike",
                "last_name" => "Miller",
                "insurance_carrier_id" => 1,            // Saint John's
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 28,
                "first_name" => "Marina",
                "last_name" => "Purvis",
                "work_phone" => "7278957737",
                "extension" => "6292",
                "insurance_carrier_id" => 1,            // Saint John's
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 29,
                "first_name" => "Thomas",
                "last_name" => "Prator",
                "email" => "contractorst@aol.com",
                "work_phone" => "2398104047",
                "insurance_carrier_id" => 2,
                "carrier_relationship_id" => 3,          // Field Adjuster
            ),
            array(
                "id" => 31,
                "first_name" => "Steve",
                "last_name" => "Barbour",
                "email" => "jsbadjuster@gmail.com",
                "work_phone" => "9417561533",
                "insurance_carrier_id" => 2,
                "carrier_relationship_id" => 3,          // Field Adjuster
            ),
        );

        $escalators = array(
            array(
                "id" => 32,
                "first_name" => "Omari",
                "middle_name" => "S.",
                "last_name" => "Ruddock",
                "email" => "oruddock@vargasgonzalez.com",
                "company" => "Vargas Gonzales Hevia Baldwin",
                "work_phone" => "3056312528",
                "fax_phone" => '3056312741',
                "client_relationship_id" => 1,     // Attorney
            ),
            array(
                "id" => 33,
                "first_name" => "Carlos",
                "last_name" => "Infante",
                "email" => "claim@infanteadjustmentbureau.com",
                "company" => "Infante Adjustment Bureau",
                "work_phone" => "5619078584",
                "client_relationship_id" => 2,     // Public Adjuster
            ),
            array(
                "id" => 34,
                "first_name" => "Turd",
                "last_name" => "Furgeson",
                "work_phone" => "2399945596",
                "company" => "GCM",
                "email" => "gabriel@gabrielmurphy.com",
                "client_relationship_id" => 2,     // Public Adjuster
            ),
        );

        foreach ($escalators as $escalator) {
            DB::table('contacts')->insert($escalator);
        }

        foreach ($adjusters as $adjuster) {
            DB::table('contacts')->insert($adjuster);
        }
    }
}
