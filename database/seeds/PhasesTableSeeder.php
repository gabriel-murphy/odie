<?php

use Illuminate\Database\Seeder;

class PhasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phases = array(
            array(
                "id" => 1,
                "name" => "Leads",                          // Leads Phase Group
                "icon" => "fad fa-server",                  // from the first milestone up to scheduled opportunities
            ),
            array(
                "id" => 2,
                "name" => "Selling",                        // Selling Phase Group
                "icon" => "fad fa-server",                  // includes opportunities scheduled for estimations up through signed estimates
            ),
            array(
                "id" => 3,
                "name" => "Pre-Production",                 // Preparation Phase Group (between signature and permitting)
                "icon" => "fad fa-server",                  // includes waiting on customer / estimator for colors and NOC and gutter diagram as well as permitting
            ),
            array(
                "id" => 4,
                "name" => "Production",                     // Production Phase Group
                "icon" => "fad fa-server",                  // includes dry-in and and final workflow ending with final permit approved
            ),
            array(
                "id" => 5,
                "name" => "Closure",                        // Closure Phase Group
                "icon" => "fad fa-server",                  // Includes waste papers, wind mitigation reports, warranties and outstanding payments owed
            ),
            array(
                "id" => 6,
                "name" => "Complete",                       // Complete Phase Group
                "icon" => "fad fa-server",                  // Includes waste papers, wind mitigation reports, warranties and outstanding payments owed
            ),


            array(
                "id" => 7,                                  // ID = 7 is start of phases
                "name" => "Digital Leads",                  // Digitally dropped in from Website or third party lead generators (HomeAdvisor) which have API
                "phase_id" => 1,                            // Leads Phase Group
                "icon" => "fad fa-server",
            ),
            array(
                "id" => 8,
                "name" => "Opportunities",                  // Client (user of the Platform) speaks to opportunity
                "phase_id" => 1,                            // Likely will move to phase_id = 9 at same time below
                "comes_after_id" => 7,
            ),
            array(
                "id" => 9,
                "name" => "Scheduled",
                "phase_id" => 1,                            // Selling Phase Group
                "comes_after_id" => 8,
            ),
            array(
                "id" => 10,
                "name" => "Assessing",                     // Estimator checks-into Appointment via App, confirms appointment occurs
                "phase_id" => 1,
                "comes_after_id" => 8,
            ),
            array(
                "id" => 11,
                "name" => "Estimating",
                "phase_id" => 2,
                "comes_after_id" => 9,
            ),
            array(
                "id" => 12,
                "name" => "Deciding",
                "phase_id" => 2,                            // Selling Phase Group
                "comes_after_id" => 10,
            ),
            array(
                "id" => 13,
                "name" => "Won",
                "phase_id" => 2,                            // Selling Phase Group
                "comes_after_id" => 11,
            ),
            array(
                "id" => 14,
                "name" => "Lost",
                "phase_id" => 2,                            // Goes to Archive Phase after 30 days
                "comes_after_id" => 11,
            ),
            array(
                "id" => 15,
                "name" => "Internal Audit",
                "phase_id" => 3,                            // Pre-Build Phase Group
                "comes_after_id" => 12,
                "customer_viewable" => '1',                 // Viewable in Customer Portal
            ),
            array(
                "id" => 16,
                "name" => "Permitting",                     // Collector Module start (Collector is additional $$$)
                "phase_id" => 3,                            // Pre-Build
                "comes_after_id" => 13,
                "customer_viewable" => '1',                 // Viewable in Customer Portal
            ),
            array(
                "id" => 17,
                "name" => "Scheduled",
                "phase_id" => 4,                            // Production Group Phase
                "comes_after_id" => 14,
                "customer_viewable" => '1',                 // Viewable in Customer Portal
            ),
            array(
                "id" => 18,
                "name" => "Production",
                "phase_id" => 4,                            // Production Group Phase
                "comes_after_id" => 15,
                "customer_viewable" => '1',                 // Viewable in Customer Portal
            ),
            array(
                "id" => 19,
                "name" => "Completed",
                "phase_id" => 5,
                "comes_after_id" => 16,
                "customer_viewable" => '1',                 // Viewable in Customer Portal
            ),
            array(
                "id" => 20,
                "name" => "Invoicing",                      // System will automate everything here, but client (human) may want to review first
                "phase_id" => 5,
                "comes_after_id" => 17,
                "customer_viewable" => '1',                 // Viewable in Customer Portal
            ),
            array(
                "id" => 21,
                "name" => "Paid in Full",
                "phase_id" => 5,
                "comes_after_id" => 18,
                "customer_viewable" => '1',                 // Viewable in Customer Portal
            ),
            array(
                "id" => 22,
                "name" => "Quality Check",                  // Customer receives a survey, if favorable, asked to leave 5-star review on Google/etc.
                "phase_id" => 5,
                "comes_after_id" => 19,
            ),
            array(
                "id" => 23,
                "name" => "Recently Lost",
                "phase_id" => 6,
                "comes_after_id" => 20,
            ),
            array(
                "id" => 24,
                "name" => "Cancelled",
                "phase_id" => 6,
                "icon" => "fad fa-map-marker-slash",
                "comes_after_id" => 21,
            ),
            array(
                "id" => 25,
                "name" => "Collections",
                "phase_id" => 6,
                "icon" => "fad fa-angry",
                "comes_after_id" => 22,
            ),
        );

        foreach ($phases as $phase) {
            \App\Phase::create($phase);
        }
    }
}
