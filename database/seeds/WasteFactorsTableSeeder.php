<?php

use Illuminate\Database\Seeder;

class WasteFactorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* These default waste factors are NOT set to a specific client and therefore, are the default settings of the client */
    public function run()
    {
        $waste_factors = array(
            array(
                "id" => 1,
                "roof_type_id" => 5,            // Dimensional Shingle
                "material_factor" => 0.040,
                "labor_factor" => 0.060,
                "created_at" => NOW(),

            ),
            array(
                "id" => 2,
                "roof_type_id" => 6,            // Three-Tab Shingle
                "material_factor" => 0.170,
                "labor_factor" => 0.120,
                "created_at" => NOW(),

            ),
            array(
                "id" => 3,
                "roof_type_id" => 7,            // Concrete Tile
                "material_factor" => 0.100,
                "labor_factor" => 0.100,
                "created_at" => NOW(),

            ),
            array(
                "id" => 4,
                "roof_type_id" => 8,            // Clay Tile
                "material_factor" => 0.120,
                "labor_factor" => 0.130,
                "created_at" => NOW(),

            ),
            array(
                "id" => 5,
                "roof_type_id" => 9,            // Slate Tile
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
            array(
                "id" => 6,
                "roof_type_id" => 10,           // Aluminum Metal
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
            array(
                "id" => 7,
                "roof_type_id" => 11,           // Steel Metal
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
            array(
                "id" => 8,
                "roof_type_id" => 12,           // Stone-Coated Steel
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
            array(
                "id" => 9,
                "roof_type_id" => 13,           // Solar
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
            array(
                "id" => 10,
                "roof_type_id" => 14,
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
            array(
                "id" => 11,
                "roof_type_id" => 15,
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
            array(
                "id" => 12,
                "roof_type_id" => 16,               // TPO
                "material_factor" => 0.180,
                "labor_factor" => 0.150,
                "created_at" => NOW(),
            ),
        );
        foreach ($waste_factors as $waste_factor) {
            DB::table('waste_factors')->insert($waste_factor);
        }
    }
}
