<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* This is the address for the headquarters of Roman Roofing (client ID = 1) and any
    future locations of clients of the Platform */

    public function run()
    {
        $clients = array(
            array(
                "id" => 1,
                "company_name" => "Roman Roofing, Inc.",
                "short_name" => "Roman",
                "headquarters_address_id" => "1",
                "main_phone" => "2399945597",
                "trade_type_id" => "1",
                "logo_image" => "roman_logo.jpg",
                "appointment_minutes" => "120",
                "email_address" => "gabriel@romanroofing.com",
                "admin_user_id" => "1",
                "created_at" => NOW(),
            )
        );
        foreach ($clients as $client) {
            DB::table('clients')->insert($client);
        }
    }
}
