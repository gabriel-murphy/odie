<?php

use Illuminate\Database\Seeder;

class RoofManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*  This is a list of all of the twenty (20) manufacturers of roofing materials of various Roof Types (Shingle, Tile, Metal, ModBit, etc.) throughout North
        America presently supported by Odie.  The Manufacturers below may manufacture roofing materials across various roof_types.  Refer see the
        RoofManufacturerRoofTypeTableSeeder seeder to understand the existing polymorphic relationships. */

        $roof_manufacturers = array(
            array(
                "id" => 1,
                "short_name" => "GAF",
                "long_name" => "General Aniline & Film",
                "series_name" => "Product Line",
                "website" => "https://www.gaf.com/en-us",
                "logo_url" => "https://www.gaf.com/-/media/fact-panels/home/logo-with-text/header_logotext.png?w=3200&hash=E7189EC99D4FA7554350ABB486F9025F79EA5F1B",
                "created_at" => NOW(),
            ),
            array(
                "id" => 2,
                "short_name" => "OC",
                "long_name" => "Owens Corning",
                "series_name" => "Product Line",
                "website" => "https://www.owenscorning.com/roofing/shingles",
                "logo_url" => "https://newsroom.owenscorning.com/sites/owenscorning.newshq.businesswire.com/files/image/additional/OC_logo_CMYK_0c100m81y4k_300DPI.jpg",
                "created_at" => NOW(),
            ),
            array(
                "id" => 3,
                "short_name" => "IKO",
                "long_name" => "IKO Roofing",
                "series_name" => "Product Line",
                "website" => "https://www.iko.com/na/residential-roofing-shingles",
                "logo_url" => "https://3sdwfb1xa06f3y7ie9255ali-wpengine.netdna-ssl.com/na/wp-content/themes/ikoresidential/assets/images/logo-iko.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 4,
                "short_name" => "Tamko",
                "long_name" => "Tamko Roofing",
                "series_name" => "Shingle Line",
                "website" => "https://www.tamko.com",
                "logo_url" => "https://tamko.renoworks.com/images/logo.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 5,
                "short_name" => "CertainTeed",
                "long_name" => "CertainTeed",
                "series_name" => "Product Line",
                "website" => "https://www.certainteed.com/residential-roofing",
                "logo_url" => "https://nobleroof.com/wp-content/uploads/2017/12/Productslogocertainteed_4fe3923c682a0.jpg",
                "created_at" => NOW(),
            ),
            array(
                "id" => 6,
                "short_name" => "Boral",
                "long_name" => "Boral Roofing",
                "series_name" => "Lines",
                "website" => "https://www.boralroof.com",
                "logo_url" => "https://www.boralroof.com/wp-content/uploads/2018/08/boral_logo.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 7,
                "short_name" => "Eagle",
                "long_name" => "Eagle Roofing Products",
                "series_name" => "Material Types",
                "website" => "https://eagleroofing.com/",
                "logo_url" => "https://eagleroofing.com/wp-content/uploads/2019/07/30yr-integrated.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 8,
                "short_name" => "Crown",
                "long_name" => "Crown Roof Tiles",
                "series_name" => "Products",
                "website" => "https://www.crownrooftiles.com",
                "logo_url" => "https://www.crownrooftiles.com/wa_images/logo%20crown_2.png?v=1eiuliq",
                "created_at" => NOW(),
            ),
            array(
                "id" => 9,
                "short_name" => "Santafé",
                "long_name" => "Santafé - The Masters in Clay",
                "series_name" => "Styles",
                "website" => "https://www.santafetile.com/",
                "logo_url" => "https://santafetile.com/wp-content/uploads/2019/01/yoast.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 10,
                "short_name" => "Gulf Coast",
                "long_name" => "Gulf Coast Supply & Manufacturing",
                "series_name" => "Product Line",
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types",
                "logo_url" => "https://s24231.pcdn.co/wp-content/uploads/2020/01/gulf-coast-supply-metal-roofing-logo.jpg",
                "created_at" => NOW(),
            ),
            array(
                "id" => 11,
                "short_name" => "Roser",
                "long_name" => "Roser Roofing Systems",
                "series_name" => "Product Line",
                "website" => "https://www.roser-usa.com",
                "logo_url" => "https://i1.wp.com/promarkbuildingsolutions.com/wp-content/uploads/2019/02/Roser-Logo.png?resize=1377%2C677&ssl=1",
                "created_at" => NOW(),
            ),
            array(
                "id" => 12,
                "short_name" => "Tilcor",
                "long_name" => "Tilcor Roofing Systems",
                "series_name" => "Product Line",
                "website" => "https://www.tilcorroofingusa.com/",
                "logo_url" => "https://modii3d.com/wp-content/uploads/2018/08/Tilcor-Roofing-Logo.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 13,
                "short_name" => "Gerand",
                "long_name" => "Gerand Roofing",
                "series_name" => "Product Line",
                "website" => "https://gerardroofs.eu/en/",
                "logo_url" => "https://lh3.googleusercontent.com/proxy/spTX2HWsE1MAtEPv9el0mpaWljQcXJ8xzWy-hxyMBN_IoB-3KMIZSxJCFMrvws4gNtt-okEpzTTEFNI1xKnmRhP4ETA5flCOeD3KQC82If3PnfwhVR4",
                "created_at" => NOW(),
            ),
            array(
                "id" => 14,
                "short_name" => "Permatile",
                "long_name" => "Permatile Roofing",
                "series_name" => "Product Line",
                "website" => "http://www.perfexcrm.com",
                "logo_url" => "https://www.advaluminum.com/wp-content/themes/advaluminum/img/logo.jpg",
                "created_at" => NOW(),
            ),
            array(
                "id" => 15,
                "short_name" => "Tropical",
                "long_name" => "Tropical Roofing Products",
                "series_name" => "Product Line",
                "website" => "https://www.tropicalroofingproducts.com/",
                "logo_url" => "https://i.ytimg.com/vi/cFm3Rtre0Vo/maxresdefault.jpg",
                "created_at" => NOW(),
            ),
            array(
                "id" => 16,
                "short_name" => "PolyGlass",
                "long_name" => "PolyGlass®",
                "series_name" => "Products",
                "website" => "https://polyglass.us",
                "logo_url" => "https://cardinal-building.com/files/logo-3.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 17,
                "short_name" => "Firestone",
                "long_name" => "Firestone Building Products",
                "series_name" => "Products",
                "website" => "https://www.firestonebpco.com/us-en/roofing",
                "logo_url" => "https://www.bridgestoneamericas.com/content/dam/bscorpcomm-sites/bridgestone-americas/images/brand-assets/logos/firestone-logos/firestone-building-products-logos/firestone-building-products-logos-color/firestone-building-products-RGB.JPG",
                "created_at" => NOW(),
            ),
            array(
                "id" => 18,
                "short_name" => "Malarkey",
                "long_name" => "Malarkey Roofing Products",
                "series_name" => "Products",
                "website" => "https://malarkeyroofing.com/",
                "logo_url" => "https://malarkeyroofing.com/img/logo-new.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 19,
                "short_name" => "BP",
                "long_name" => "BP Canada",
                "series_name" => "Products",
                "website" => "https://https://bpcan.com/",
                "logo_url" => "http://www.toituresstephanebergeron.com/sites/default/files/Produit-BP.png",
                "created_at" => NOW(),
            ),
            array(
                "id" => 20,
                "short_name" => "Atlas",
                "long_name" => "Atlas Roofing Corporation",
                "series_name" => "Products",
                "website" => "https://https://www.atlasroofing.com/",
                "logo_url" => "https://cdn.imgbin.com/13/4/4/imgbin-roof-shingle-atlas-roofing-corporation-wood-shingle-building-gFEvgh4szn67wsRPpkHmzXsCK.jpg",
                "created_at" => NOW(),
            ),

        );
        foreach ($roof_manufacturers as $roof_manufacturer) {
            DB::table('roof_manufacturers')->insert($roof_manufacturer);
        }
    }
}
