<?php

use App\RoofType;
use Illuminate\Database\Seeder;

class RoofManufacturerRoofTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roofTypeRoofManufacturers = [
            5 => [1,2,3,4,5],           // Dimensional Shingle
            6 => [1,2,3,4,5],           // Three-Tab Shingle
            7 => [6,7,8],               // Concrete Tile
            8 => [6,9,8],               // Clay Tile
            9 => [1],                   // Slate Varieties
            10 => [10],                 // Aluminum
            11 => [4, 10],              // Steel
            12 => [6,11,12,13,14],      // Stone-Coated Steel
            13 => [1],                  // Solar
            14 => [1,15,16,17],         // Coatings
            15 => [1,16,17],            // Modified Bitumen
            16 => [1,17],               // TPO
        ];

        foreach ($roofTypeRoofManufacturers as $roof_type_id => $manufacturers) {
            $roofType = RoofType::find($roof_type_id);
            $roofType->manufacturers()->attach($manufacturers);
        }
    }
}
