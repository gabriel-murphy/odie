<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reviews = array(
            array(
                "name" => "Katherine M.",
                "rating" => 5,
                "comment" => "Excellent service offering a low cost solution for my roofing problems. A company with professionalism and integrity.",
                "created_at" => "2017-09-15 00:00:00",
            ),
            array(
                "name" => "Robert H.",
                "rating" => 1,
                "comment" => "A man from Roman Roofing named Nick P., made an appointment to give me an estimate on a " .
                    "reroof. He never showed and never called. First impressions are important. I'm done with Roman " .
                    "Roofing.",
                "created_at" => "2017-04-11 00:00:00",
            ),
            array(
                "name" => "Sherrill W.",
                "rating" => 5,
                "comment" => "The team at Roman Roofing are great! They were amazing and worked very hard. They " .
                    "completed the work as agreed and went above and beyond to address any concerns I had. I am " .
                    "very happy to have hired Roman Roofing Inc.",
                "created_at" => "2017-12-12 00:00:00",
            ),
            array(
                "name" => "Rick T.",
                "rating" => 5,
                "comment" => "They were prompt sand did a professional job.",
                "created_at" => "2018-09-01 00:00:00",
            ),
            array(
                "name" => "John C.",
                "rating" => 5,
                "comment" => "I never realized how much goes into installing a metal roof. Roman Roofing did a wonderful job " .
                    "protecting my house from the next hurricane. Not only is my roof solid, it's beautiful, too",
                "created_at" => "2018-02-16 00:00:00",
            ),
            array(
                "name" => "Dawn P.",
                "rating" => 5,
                "comment" => "Roman Roofing in Cape Coral did a great proffesional job at My house I would highly recommend " .
                    "this business to anyone great team of professionals they went up and beyond",
                "created_at" => "2018-03-21 00:00:00",
            ),
            array(
                "name" => "Shirley C.",
                "rating" => 5,
                "comment" => "Excellent company to work with. Job was done earlier than estimated and the materials used were excellent.",
                "created_at" => "2018-03-26 00:00:00",
            ),
            array(
                "name" => "Dana C.",
                "rating" => 5,
                "comment" => "Great company with the core values everyone is looking for in a for-hire contractor and their " .
                    "Team members: honesty, high integrity, responsiveness, \"doing what they say they will do\", " .
                    "courteous and prompt...oh and very cost competitive as well! You can tell this is a company with " .
                    "great leadership and commitment to excellence and a company that has great respect for their " .
                    "customers. We have used Roman Roofing for both repairs on our previous home and now a new " .
                    "roof install within the past month on our current home. The efficiency of their entire process from " .
                    "old roof tear-off, to roof repair and prep to the final re-roofing process was done like clock work " .
                    "and with high quality workmanship and attention to detail. Most impressive to me, because I am a " .
                    "business owner as well, was when they said the roof tear-off would take 3 days...it took 3 days, " .
                    "when they said the roof re-pair and prep would take 3 days...it took 3 days, when they said the " .
                    "shingles would be delivered on Thursday...they were delivered on Thursday, etc, etc.! If you are " .
                    "considering replacing your current roof or you need repairs on your roof, save yourself the " .
                    "process of trying to find the \"right guys for your job\" and give Roman Roofing a call - you will not " .
                    "believe the experience you will have with them!",
                "created_at" => "2018-03-04 00:00:00",
            ),
            array(
                "name" => "Mary H.",
                "rating" => 5,
                "comment" => "Norm and the entire Roman Roofing team went above and beyond to help my sellers on their " .
                    "roof. The tiles that needed repaired were no longer being made and Norm went out of his way to " .
                    "match the color for them and make sure he could get it done! I'm sure you are thinking he must of " .
                    "charged extra for that... NO he didn't!! I know I was shocked too!! There is no other company I " .
                    "would recommend. When you call the company you will get treated like family! Lindsey answered " .
                    "when I called and she was the sweetest!! Represents the company VERY WELL! Over all ð??¯!",
                "created_at" => "2018-04-18 00:00:00",
            ),
            array(
                "name" => "Gerald S.",
                "rating" => 4,
                "comment" => "Roman Roofing replaced my roof I called about a portion of roof that did not look right. They sent " .
                    "a tech out to check. Recognised an issue. Tech came out and removed shingles, underlayment " .
                    "plywood and found a warped truss. Repaired under warranty. Tech was excellent and company " .
                    "service was excellent",
                "created_at" => "2018-06-14 00:00:00",
            ),
            array(
                "name" => "TJ T.",
                "rating" => 5,
                "comment" => "Alex C. of Roman Roofing was very professional, very knowledgeable, and very thorough in his " .
                    "assessment of my roof. His evaluation was very honest, and his estimate was very fair. I was very " .
                    "happy with the service they performed, and would definitely recommend them.",
                "created_at" => "2018-02-08 00:00:00",
            ),
            array(
                "name" => "Raymond",
                "rating" => 5,
                "comment" => "Great company, excellent work. We would highly recommend them.",
                "created_at" => "2018-08-31 00:00:00",
            ),
            array(
                "name" => "Connie",
                "rating" => 5,
                "comment" => "We had damage from Hurricane Irma and had a couple estimates from different companies for a " .
                    "complete new roof. We chose Roman Roofing to do the job. Clint N. was our go to guy, he was " .
                    "extremely helpful and knowledgeable. Clint was always there to answer our phone calls no " .
                    "matter when we called him. Our roof turned out beautiful.......I highly recommend Roman Roofing.",
                "created_at" => "2018-01-09 00:00:00",
            ),
            array(
                "name" => "Walt Z",
                "rating" => 5,
                "comment" => "All seems well.My access is limited due to my age but after recent rains no interior on the second " .
                    "floor in areas of repairs are appearing distressed. Repair was promptly handled.",
                "created_at" => "2018-05-09 00:00:00",
            ),
            array(
                "name" => "Jon",
                "rating" => 5,
                "comment" => "Excellent in all matters",
                "created_at" => "2018-07-09 00:00:00",
            ),
            array(
                "name" => "Charlotte G",
                "rating" => 5,
                "comment" => "Great company. Dependable, respectful, professional. The representative went out of his way to " .
                    "assist me with the insurance company and to provide me with a new roof after Hurricane Irma. " .
                    "The job was done from start to finish with minimal clutter and friendly workers. There was " .
                    "communication from the job supervisor to keep me informed. They went above and beyond what " .
                    "we agreed upon. As a single woman with 3 children, that meant a lot to me. Thank you Roman " .
                    "Roofing.",
                "created_at" => "2018-03-10 00:00:00",
            ),
            array(
                "name" => "Linda S",
                "rating" => 5,
                "comment" => "Roman Roofing has an extraordinary management team that is focused on pleasing the customer " .
                    "and, in my case, exceeding my expectations. There were a few hiccups, but the team was there " .
                    "to make everything right! That is not always the case with service providers. I highly recommend " .
                    "Roman Roofing as an outstanding company.",
                "created_at" => "2018-04-10 00:00:00",
            ),
            array(
                "name" => "Bill A",
                "rating" => 5,
                "comment" => "They replaced my flat deck roof. Did a great job. Would highly recommend.",
                "created_at" => "2018-08-10 00:00:00",
            ),
            array(
                "name" => "Michael H",
                "rating" => 5,
                "comment" => "Roman Roofing was the best company I've dealt with all summer. Been remodeling with multiple " .
                    "contractors this year. Including a new roof. Roman Roofing kept me informed, did a professional, " .
                    "quick concise job. JR, the field manager, was very involved. Stopped by multiple times to make " .
                    "sure I was happy. And.... the roof looks GORGEOUS!",
                "created_at" => "2018-10-13 00:00:00",
            ),
            array(
                "name" => "Terry H",
                "rating" => 5,
                "comment" => "We were very please with this company. They kept their appointment when they said. The " .
                    "supplies were delivered and work start right away. They cleaned up and didn't request payment " .
                    "after the initial deposit. Excellent work and it looks great. Thank you Roman Roofing for making " .
                    "this project so easy.",
                "created_at" => "2018-11-15 00:00:00",
            ),
            array(
                "name" => "adrian w",
                "rating" => 5,
                "comment" => "I am a local real estate agent and have for the last 5 years always recommended Roman roofing " .
                    "for roof repair and replacement. I have even used them on my own house. Jake who I always call " .
                    "is always a major help and goes to great lengths to explain what they are doing and he is proud " .
                    "of the workmanship they do.No company is perfect but I would and will continue to use them and " .
                    "I gain nothing financially for doing so.A great company.people and service",
                "created_at" => "2018-11-28 00:00:00",
            ),
            array(
                "name" => "Steven B",
                "rating" => 5,
                "comment" => "We love our new roof. Roman was not on time, materials and crew showed up a month early! Fast " .
                    "and efficient removal and installation. Both crews were highly skilled and respectful. A couple of " .
                    "small issues were addressed no problem. Mark, Alex, Angel thank you.l. Alex even took it upon " .
                    "himself to paint the new vents to match roof color. If you need a new roof do not hesitate to hire " .
                    "Roman Roofing.",
                "created_at" => "2018-12-12 00:00:00",
            ),
            array(
                "name" => "Anthony P. Capobianco",
                "rating" => 5,
                "comment" => "Our affiliation with Roman Roofing began when we were living in our previous home @ Colonial " .
                    "Country Club & unfortunately, after Hurricane Irma, had minor roof damage. Not often did I ever " .
                    "think of having a roof checked regularly & afterwards, felt a bit of a panic. In our community, a few " .
                    "companies were there & we decided to use Roman's expertise. We received excellent service " .
                    "from them. They were efficient, responsible, knowledgeable, honest & truthful. Our second home " .
                    "that is presently in the process of being sold, was in need of a few roofing adjustments after the " .
                    "home inspection. Of course, we called your company. Might I add that the compliments begin " .
                    "with the receptionist, who, after first verbal impressions, was extremely helpful & courteous. Then, " .
                    "to our meeting with was absolutely formidable, in my opinion. I could adhere to his " .
                    "professionalism by the way he spoke on the phone & replied to my texts. After we met, it was " .
                    "friendship at best talking about family. Two days after your inspection, I received a very positive " .
                    "call from Mike indicating that a crew was able to be at our home the following day for the roofing " .
                    "repairs. He was professional & I could adhere that he was proficient with his craft. I was elated & " .
                    "after checking with you, the following day, the job was completed. I must say, I am deeply " .
                    "appreciative & grateful knowing that proficient companies still exist & promote excellence. I am " .
                    "now going to have Roman Roofing yearly inspect our roof; fulfilling whatever needs to be " .
                    "completed & simply having \"a piece of mind.\" Yes, it's \"a piece of mind\" working with Roman " .
                    "Roofing & I will gladly share our positive affiliation of the company with anyone of inquiry. It's " .
                    "impossible to please every individual but, in our opinion, the company does it superbly well! " .
                    "Thank you ******* ** ********** Fort Myers, FL 33966",
                "created_at" => "2018-12-21 00:00:00",
            ),
            array(
                "name" => "Bruce Q",
                "rating" => 5,
                "comment" => "I recently had Roman roofing redo the roof on my residence. The process started with " .
                    "comparable estimates, of which, they were very competitive, price wise. The estimator was very " .
                    "professional and answered all my questions and concerns. The company sent me a link to check " .
                    "up on progress through the process, making that part of the experience even easier. The crews " .
                    "that did the work were also very professional and did quality work. The results of the new " .
                    "galvalume roof were outstanding. All of my neighbors and friends commented that it looked very " .
                    "nice. I am completely satisfied with the results and would not hesitate to recommend Roman " .
                    "Roofing to any of my friends in need of a new roof. I am confident they would be happy, as I was, " .
                    "with their work.",
                "created_at" => "2018-12-20 00:00:00",
            ),
            array(
                "name" => "Jean B",
                "rating" => 5,
                "comment" => "Roman Roofing is the best. I needed a new roof due to Irma. Their salesman, Ralph, was terrific. " .
                    "He was knowledgeable and extremely professional. He presented all my roofing options and was " .
                    "even able to present a quote that my home insurance finally approved. Without Ralph's " .
                    "assistance, I know I would have had to pay for the entire invoice. Ralph was the only person that " .
                    "was willing to work with me and my insurance company. He took pictures and submitted all the " .
                    "required paperwork to obtain a positive result for me. My roof looks just great. All my friends say " .
                    "my home looks brand new and they love the color of my new GAF shingles. Alex and his team " .
                    "worked quickly and had my home weather tight in one day. Alison and the office team are " .
                    "professional and fast to respond to requests. The crew that installed the shingles did an excellent " .
                    "job. Tom and son did a quality job when installing the new brown facia perfectly. If you need a " .
                    "new roof, do not hesitate to call Roman Roofing. You will not be disappointed.",
                "created_at" => "2019-01-23 00:00:00",
            ),
            array(
                "name" => "Nancy E",
                "rating" => 5,
                "comment" => "A soft spot was discovered on our roof so I asked friends for recommendations....Roman Roofing " .
                    "came up 3 times! Ralph determined it was from Hurricane Irma and recommended filing a claim " .
                    "on our Homeowners Ins. When our insurance company sent a check for far less than was " .
                    "needed, Ralph went to bat and they paid the entire amount submitted! Can't say enough about " .
                    "Alex and his prompt responses whenever needed. Ralph, Alex and Allison made the whole " .
                    "experience go smoother than I ever thought possible! They need to provide a rating higher than " .
                    "5 Stars for this company.",
                "created_at" => "2019-01-25 00:00:00",
            ),
            array(
                "name" => "Rich",
                "rating" => 5,
                "comment" => "I had a very good experience from the beginning with Mark S. who carried us through the permit. " .
                    "king of shingles,and follow up throughout the job.Alex was the everyday person " .
                    "who handled the applying of the roof and expediting everything in a timely manner.We stayed in " .
                    "constant contact and I was happy with his professionalism.The roof looks good and the signoffs " .
                    "by the town are all done. Everything passed.The cleanup was good also. Nothing that I had to do " .
                    "once they were done. I highly recommend Roman Roofing and their workers. Rich I give them 5 " .
                    "stars.",
                "created_at" => "2019-01-02 00:00:00",
            ),
            array(
                "name" => "Colette S",
                "rating" => 5,
                "comment" => "Very professional. They did a great job on the roof and the gutters were done in a timely manner. " .
                    "Gabriela was a bit rude on the phone, but you can't have everything. Would recommend the " .
                    "company for any roofing needs.",
                "created_at" => "2019-02-08 00:00:00",
            ),
            array(
                "name" => "Jackie G",
                "rating" => 5,
                "comment" => "This company is the only one I will call for roof issues. I am a realtor and also refer clients to them. " .
                    "They are honest and knowledgeable and I won't use any one else at my home. I fully trust them " .
                    "with my roof.",
                "created_at" => "2019-02-14 00:00:00",
            ),
            array(
                "name" => "Jeffrey C",
                "rating" => 5,
                "comment" => "From start to finish a great job! Needed help working with my insurance company and they jumped right in. Roof looks great!",
                "created_at" => "2019-02-14 00:00:00",
            ),
            array(
                "name" => "Alice D",
                "rating" => 5,
                "comment" => "Roman Roofing is professional, highly skilled, and trustworthy. I was kept informed of all the steps " .
                    "along the way. All staff was courteous and made me feel like I was important. The quality of the " .
                    "work is second to none! The roofer paid great attention to detail, and the roof looks absolutely " .
                    "amazing. It took only a few weeks from the first contact to job completion. I was very happy with " .
                    "every phase of the project, and SO SO happy that I chose Roman Roofing!",
                "created_at" => "2019-02-14 00:00:00",
            ),
            array(
                "name" => "Ted Z",
                "rating" => 5,
                "comment" => "The roof on my 3 year old home developed two leaks. Frank inspected the problem and also " .
                    "pointed out some other issues with the roof caused buy the original roof installers. Dillon did a " .
                    "very thorough repair and cleanup job. I wish Roman Roofing had installed the roof when the " .
                    "house was built, but I will definitely call them if I have any future problems. TAZ",
                "created_at" => "2019-02-16 00:00:00",
            ),
            array(
                "name" => "Jeff S",
                "rating" => 5,
                "comment" => "The workmanship was great. Miguel took pride in his work and did a great job. The project was " .
                    "done ahead of the projected start date, and Mark in sales did a good job of communicating " .
                    "between me and the project supervisor. Thanks for a job well done.",
                "created_at" => "2019-02-18 00:00:00",
            ),
            array(
                "name" => "Jonah N",
                "rating" => 5,
                "comment" => "THANK GOD FOR ROMAN ROOFING!!!! ABSOLUTELY AMAZING PEOPLE AND AMAZING\r\nCOMPANY!!! They saved a Naples School Teacher and Costco Employees 1st home!!! And for\r\nthat, we are forever grateful & indebted to them for life. Please read my wife and I's full story.....\r\nMy wife and I had storm damage due to Hurricane Irma as many did. Our roof was leaking in 7\r\ndifferent locations and we desperately needed help. We didn't have enough buckets to catch the\r\nwater that was leaking from the ceiling and needed to get it started as soon as possible. We hired\r\na roofing/construction company out of Tampa to do the job. All looked great on paper but when it\r\ncame time to get the job done they did not have a clue. It was a train wreck from the beginning.\r\nWe provided a 50% deposit. We went over what we wanted and typed up a document of exactly\r\nwhat materials we wanted them to use for the job. They lied about everything from specifics of\r\nthe job to the work that they would be doing and how they would be doing it. They ordered the\r\nwrong wood. We asked for and paid for plywood, they ordered OSB which is cheaper and less\r\ndurable than plywood. We sent it back. When the correct wood was finally delivered they only\r\nhad paid for and delivered half of the wood. The wood was placed in their empty dumpster which\r\nwas sitting in our driveway. I asked them to please put it in our garage as it had been raining\r\nheavily for several days, they did not. The wood got saturated as the dumpster filled up with\r\nwater. I covered it with a tarp that I purchased from Home Depot because they would not and we\r\nwanted it covered in the meantime but the tarp started sagging like a pocket and holding water. I\r\nwas draining the water off of the tarp to try and prevent all of the plywood from getting damaged\r\nand leaning back on the dumpster pulling on the tarp, my hand slipped...I hit myself in the eye,\r\nlacerated my cornea and retina, fell off of the dumpster, herniated a disc in my lower back and\r\nended up in the hospital. All because the contractor was too lazy to place the plywood in the\r\ngarage (that I left open) to protect it from the rain. The plywood was now soaked, warped and\r\nuseless. But not to worry because my contractor had the nerve to ask me for more money to buy\r\nnew plywood. What we had was useless and had to be thrown out. We ordered and paid for 3\r\nglass impact skylights, they delivered plastic bubble skylights that were much cheaper and not\r\nimpact. We sent them back. We ordered stainless steel fasteners to fasten the plywood, they\r\ndelivered fasteners that were cheap and not stainless steel. We ordered tile and paid for it and\r\nnever received it. We ordered pressure treated 2x4's for the soffits they delivered 2x4's that were\r\nnot PT. We sent that back. When it came to the underlayment, we requested a secondary water\r\nbarrier. It was delivered but ended up being the wrong type and was for shingle roofs that were\r\nto be used for flat construction. Our roof is tile and the type of roof we have is a crossed gable.\r\nWhen we provided the information to the owner for the job, he stated that he would take care of\r\nand order the permit within a week's time. It never happened, he did the paperwork for the\r\npermit 6 months later. When the permit was finally submitted by our contractor and processed by\r\n the Collier County Building Department it was kicked back 3 separate times with 3 separate\r\nmistakes that our contractor made: #1) Wrong address/missing documents #2) Expired insurance\r\n#3) Incomplete materials list They were 6 months late in starting the job. When they did start the\r\njob, they would only work for 2 hours then they would leave and we would not see them again\r\nfor another 5-7 weeks. Then the pattern was repeated. They left our roof exposed, did not dry it\r\nin correctly with the tarp, water snuck under the tarp in many areas when we were not home\r\nbecause they did not drape the tarp over the ridges and our home flooded in 3 separate rooms\r\ncreating an additional $67k in damages including mold that was now spreading. If that was not\r\nenough, then I found out that they were stealing fruit from my fruit trees. Wait I am not finished: I\r\nthen received a phone call from my association president stating that on two separate occasions,\r\nthe roofers I hired were urinating in my neighbor's yard. Needless to say, we fired them,\r\ncancelled the permit and reported them. My wife and I were not going to make the same mistake\r\nagain. We were going to do a lot of research, talk to a lot of people, and hire the best. That is\r\nexactly what we did! The phone calls we made, our weeks of research focusing on the other\r\npiece of also reading reviews on every review site out there, our conversations we had with many\r\npeople over the course of several weeks every single day, all led us to the best, Roman Roofing.\r\nEveryone that has had work done by them, absolutely loved them and their work!!! We spent over\r\nan hour talking to Owners Norm and Lindsey Dopfer on the phone. We set up an appointment\r\nand asked if they could please come out to our house to meet with us instead of a sales guy or\r\nproject manager. They agreed and said in exact words \"We are so sorry that this happened to\r\nyou guys, don't worry we will take good care of you and your house, we will get a great roof on\r\nyour house for a great price.\" It didn't take my wife and I very long at all to figure out that we had\r\nthe right people for the job. We hired Roman Roofing to take the job over and that is exactly what\r\nthey did. The work they did was exactly as advertised. They were educated, had an answer for\r\nevery question/concern. They were incredibly organized in every single facet, very professional,\r\nthe paperwork was perfect, the permit process was perfect, the materials ordered was exactly\r\nwhat was received. They came when they said they would come, they ordered and delivered\r\nexactly what we requested they use for all of the materials, they started when they said they\r\nwould start, they finished when they said they would finish, attention to detail was amazing. They\r\nwere friendly and they were extremely clean. Everything that they touched \"from start to finish\"\r\nturned to gold. We have the nicest, neatest and best looking roof in the community. The workers\r\ncleaned up the jobsite every single day that they left. I am not an emotional man, but it is\r\nemotional for me to write this because of what my wife and I have been through. I will\r\nrecommend them forever! It is so nice to know that beautiful, honest, hardworking, dedicated\r\npeople like the owners of Roman Roofing Norm and Lindsey Dopfer and their entire crew from\r\nthe project managers, to the site managers, to the installers to the carpenter's to the tile crew, still\r\ndo exist!!! They were all a blessing and absolutely, amazing! Please excuse my passion for\r\ngreatness. This letter is just a drop in the bucket for what my wife and I feel like we owe Norm\r\nand Lindsey Dopfer & Roman Roofing for their honesty, amazing work, and for saving our home.\r\nJonah N. - Naples",
                "created_at" => "2019-02-19 00:00:00",
            ),
            array(
                "name" => "Adriana N",
                "rating" => 5,
                "comment" => "I have to start by saying that Roman roofing saved my house because when Hurricane Irma\r\npassed my house was destroyed, the roof was very damaged and there were many water leaks\r\nthroughout the house. The walls, windows and floor were getting damaged inside with so much\r\nwater that came in when it rained. At first we hired a company by the name of Moss Construction,\r\nthe owner's name was Dean Moss and this person worsened everything, and was a complete\r\ncompulsive liar. It was a total disaster. We thought we were going to lose our house. It was very\r\nfrustrating and sad to know that we were going to lose our house that we had bought with\r\neverything we had and put everything into it. Thank God a friend recommended Roman Roofing\r\nand my husband to contact directly the owners Norm and Lindsey Dopfer. What humane, humble\r\nand simple people. We immediately made an appointment and they personally came to see what\r\nthey could do to save our home. They started working as soon as possible, they recommended\r\nthe best materials, they explained the whole process step by step. They had answers for all our\r\nconcerns. Our house needed new structure in wood, edges in aluminum, soffits, framing, tiles.\r\nBasically everything. It was a big roofing job. First, they sent us one of their many crews\r\ndedicated to our home/project. They were a spectacular group of workers, led by Michael and his\r\nsupervisor was Tony. Great, punctual, happy always working, polished in every detail and\r\ndedicated all the time necessary to advance every day. The carpentry work was amazing and\r\nthen they went to put the adhesive that protects not enter the water. Wow super careful installing\r\nthe skylights and fans. All the work verified so that not even a drop of water will enter the house.\r\nFully sealed with multiple layers of underlayment. After they sent us a couple RamÃ³n and Lucia\r\nwho was in charge of installing the tiles. Super good people and super polished as well. They\r\nmade cement perfectly match with the tile. They also were incredible. For me our house now has\r\nthe most beautiful roof of the whole community. Not all roofs are the same, and we realized this\r\nvery quickly when they were done...Our roof just looks tighter, cleaner, more organized and\r\nsturdier. And all this thanks to the dedication, incredible attention, treatment and great price we\r\nreceived. Blessings and incredible work we received from Roman Roofing, amazing company.\r\nAdriana N. - Naples",
                "created_at" => "2019-02-19 00:00:00",
            ),
            array(
                "name" => "Bob H",
                "rating" => 5,
                "comment" => "Fantastic service! Not only quality workmanship, but fast service!",
                "created_at" => "2019-02-27 00:00:00",
            ),
            array(
                "name" => "Bob M",
                "rating" => 5,
                "comment" => "Roman roofing was professional and did a quality job with lots of feedback they used excellent " .
                    "materials and moved the job right along, when it was done the cost came out almost the same as " .
                    "the estimate. Yes I would recommend them. *****",
                "created_at" => "2019-03-07 00:00:00",
            ),
            array(
                "name" => "Edward R",
                "rating" => 5,
                "comment" => "ROMAN ROOFING IS THE BEST ROOFING COMPANY IN FLORIDAY!!! They have replaced " .
                    "several roofs for us and this company knows how to please the consumer!!! Our roof is the most " .
                    "beautiful roof we have ever owned thanks to Norm, the owner. He is honest and hard working " .
                    "and wants you to be very happy with the results!!! We can't thank him enough!!! We highly " .
                    "recommend Roman Roofing located in Cape Coral, FL",
                "created_at" => "2019-03-07 00:00:00",
            ),
            array(
                "name" => "Dennis P",
                "rating" => 5,
                "comment" => "Roman Roofing did a fantastic jib of repairing my roof leak. Knowing that the original tiles were no " .
                    "longer available the workers took extreme care to make sure that all tiles could be saved. " .
                    "Excellent work and fast and friendly throughout the entire process. Highly recommend them if " .
                    "you need roof work done.",
                "created_at" => "2019-03-08 00:00:00",
            ),
            array(
                "name" => "Bruce W",
                "rating" => 5,
                "comment" => "Very professional and completed the job I a timely manner",
                "created_at" => "2019-03-08 00:00:00",
            ),
            array(
                "name" => "Raul R",
                "rating" => 5,
                "comment" => "GREAT ROOFING COMPANY. FAST SERVICE, EASY INSTALL, VERY COURTEOUS. GREAT PRICE. THANKS AGAIN.",
                "created_at" => "2019-03-08 00:00:00",
            ),
            array(
                "name" => "rpfla01",
                "rating" => 5,
                "comment" => "Roman Roofing did an excellent job on our roof. The workers that removed the old roofing and " .
                    "prepared the dry-in as well as the two guys that installed the shingles did a phenomenal job. " .
                    "They were quick, but diligent in making sure the work was completed correctly. Our initial " .
                    "construction had ridge vents, but for some reason the original contractor did not cut them in. This " .
                    "resulted in the shingles over the last 15 years \"baking.\" Roman Roofing installed brand new ridge " .
                    "vents over the home and garage. The inspections were scheduled and passed without any " .
                    "problems from the city. No inspection failures. Our bid included new gutters as well and the " .
                    "company that installed them, completed in one day. The roof is beautiful and the gutters are " .
                    "brand new. The folks in the back office were pleasant and easy to work with as well. I can't give " .
                    "enough credit to the workers that are up on the roof doing the hard work. They are professional " .
                    "and do an great job! Many thanks to these guys that work so hard, in the South Florida sun to " .
                    "provide an exceptional experience. I am very happy with the roof and with the work of Roman " .
                    "Roofing. I recommend them highly.",
                "created_at" => "2019-03-11 00:00:00",
            ),
            array(
                "name" => "Willoughby B",
                "rating" => 5,
                "comment" => "Roman roofing was amazing. Their work was professional and on time. Unlike a lot of companies, Roman Roofing kept me in the loop and followed up to make sure I was completely satisfied. I highly recommend this company.",
                "created_at" => "2019-03-11 00:00:00",
            ),
            array(
                "name" => "Gary F",
                "rating" => 5,
                "comment" => "Roman roofing did an excellent job. The salesman did not try to sell me a new roof when I only " .
                    "needed some repair work. Salesman was professional and knowledgeable. Repairman did an " .
                    "excellent job also. Completed everything I asked for and it looked professional. Five star rating.",
                "created_at" => "2019-03-18 00:00:00",
            ),
            array(
                "name" => "Gerald N",
                "rating" => 5,
                "comment" => "Roman roofing did an excellent job. The salesman did not try to sell me a new roof when I only " .
                    "needed some repair work. Salesman was professional and knowledgeable. Repairman did an " .
                    "excellent job also. Completed everything I asked for and it looked professional. Five star rating.",
                "created_at" => "2019-03-18 00:00:00",
            ),
            array(
                "name" => "Brian A",
                "rating" => 5,
                "comment" => "I feel very lucky to have met Norm the owner of Roman Roofing. I had spent the past few months " .
                    "getting repair estimates from other roofing companies which only ended with wasted time and " .
                    "disappointment. I met the owner by chance and I told him I had been shopping for decent " .
                    "estimates for my roof repair. He said he would get someone out to my house for an estimate by " .
                    "the next week and he did. They gave me be far the best estimate and had the repair completed " .
                    "by the following week! They were high quality and completely professional along every step of " .
                    "the process. I am not very trusting of people working around my house but I felt completely " .
                    "comfortable with everyone that was sent to my house. The owner is also a huge supporter of his " .
                    "community which is a huge plus for me! Thanks again, and yes this will be the ONLY roofing " .
                    "company that I will use or refer to any of my friends in the future.",
                "created_at" => "2019-03-18 00:00:00",
            ),
            array(
                "name" => "Lew G.",
                "rating" => 5,
                "comment" => "Excellent job! Showed up when scheduled, work done on time and according to contract, no " .
                    "surprise charges, great workers who showed respect for the residence, and got the job done in a " .
                    "very reasonable time! Would definitely hire them again",
                "created_at" => "2019-03-19 00:00:00",
            ),
            array(
                "name" => "Dorothy S",
                "rating" => 5,
                "comment" => "Very professional and ethical company, very efficient and hardworking conscientious crews who " .
                    "did a great job in a timely manner. No problems with inspections, I would highly recommend " .
                    "them.",
                "created_at" => "2019-03-20 00:00:00",
            ),
            array(
                "name" => "Dan P",
                "rating" => 5,
                "comment" => "Roman Roofing Company in Cape Coral FL was excellent in customer service and quality. " .
                    "Everyone we dealt with from start to finish did what they said when they said they would do it. " .
                    "They responded immediately to any questions and/or concerns. Highly recommend this company " .
                    "to anyone with roofing repairs or replacement needs.",
                "created_at" => "2019-03-20 00:00:00",
            ),
            array(
                "name" => "GreggD",
                "rating" => 5,
                "comment" => "Professional and very helpful following a natural disaster (Hurricane Irma). Would work with them again if I have the need.",
                "created_at" => "2019-04-01 00:00:00",
            ),
            array(
                "name" => "David H",
                "rating" => 5,
                "comment" => "Great job! Right price. Excellent quality. Nice people.",
                "created_at" => "2019-04-02 00:00:00",
            ),
            array(
                "name" => "Michael",
                "rating" => 5,
                "comment" => "My roof repair was priced in line with three other quotes. Chris and his crew got to my job a week " .
                    "before it was scheduled and did a very professional job and perfect clean up. I am very pleased " .
                    "and will definitely call them in the future if I need any roof work done!",
                "created_at" => "2019-04-04 00:00:00",
            ),
            array(
                "name" => "Marilyn A",
                "rating" => 5,
                "comment" => "Excellent company and excellent job replacing a very large tile roof. Professional and polite " .
                    "employees that cleaned up the job site each and every day they were there. Company staff and " .
                    "owners were a delight to work with and recommend them to anyone looking for roof work.",
                "created_at" => "2019-04-05 00:00:00",
            ),
            array(
                "name" => "Michael T",
                "rating" => 5,
                "comment" => "Roman did very well by us. Excellent responsiveness, efficient, prompt service and cost effective " .
                    "in taking over for a defaulted roof contractor and replacing our single family asphalt roof. No " .
                    "hesitation in recommending that you are safe in contracting with Roman Roofing. Go for it.",
                "created_at" => "2019-04-06 00:00:00",
            ),
            array(
                "name" => "Sherry S",
                "rating" => 5,
                "comment" => "Roman Roofing is amazing. The office staff are responsive, efficient and professional. The roofers " .
                    "are hard workers who are extremely polite and professional. They have scheduling of product " .
                    "and removal of debris down to a science. Tile didn't sit on my driveway waiting to be installed. " .
                    "Pricing is affordable, reasonable with no hidden costs. I highly recommend Roman Roofing for " .
                    "replacement or repairs. You can't go wrong hiring Roman it's Lee County's smartest choice and " .
                    "only choice if you want a quality company.",
                "created_at" => "2019-04-11 00:00:00",
            ),
            array(
                "name" => "LAURENCE K",
                "rating" => 5,
                "comment" => "Roman Roofing has performed its work professionally, efficiently and in a timely manner. Norm, " .
                    "an owner, takes your call personally if you want him to. Chris has done good roof inspection and " .
                    "repair work in a professional, timely and polite manner Could not have asked for better service. I " .
                    "would recommend them to anyone. Their responsiveness has been outstanding. *****",
                "created_at" => "2019-04-16 00:00:00",
            ),
            array(
                "name" => "Karen G",
                "rating" => 5,
                "comment" => "Roman Roofing is very lucky to have one employee in particular. His name is Alex A. He was " .
                    "patient calm professional and knowledgeable. he communicated with me from start to finish even " .
                    "though it wasn't his job! Roman Roofing is very fortunate to have him. The roof looks beautiful " .
                    "now and we are satisfied with the company and their quality and performance.",
                "created_at" => "2019-04-25 00:00:00",
            ),
            array(
                "name" => "William W",
                "rating" => 5,
                "comment" => "Roman Roofing was very responsive to work with, they went the extra mile to get samples of " .
                    "wind rated GAF Timberline Ultra HD shingles not readily available. The job went quickly and " .
                    "efficiently with crews cleaning up every night. The result is a roof that is superior and has an " .
                    "excellent appearance. We had one concern that was quickly addressed to our satisfaction . We " .
                    "recommend Roman Roofing.",
                "created_at" => "2019-04-25 00:00:00",
            ),
            array(
                "name" => "Cecilia D",
                "rating" => 5,
                "comment" => "I highly recommend Roman Roofing! They installed a metal roof for us, and we couldn't be " .
                    "happier. Their price was much lower than another estimate, and Clint gave us a quote the same " .
                    "day! The entire process was efficient and painless. The Roman Roofing office staff (Allison, Allen, " .
                    "Devin and Jeffrey) and all of the workers were courteous and professional. They were on the job " .
                    "like clockwork and kept us informed every step of the way. They cleaned up at the end of each " .
                    "day and went above and beyond to make sure we have a quality roof that will last for years. I " .
                    "can't say enough good things about this company!",
                "created_at" => "2019-04-25 00:00:00",
            ),
            array(
                "name" => "Marc B",
                "rating" => 5,
                "comment" => "Roman Roofing is a professional commercial and residential roofer. I had my home reroofed. " .
                    "They did a great job! I was emailed as to the progress being made every step of the way. I spoke " .
                    "with the supervisor. He was very nice, explained everything to me. The whole thing took just two " .
                    "weeks to complete and most of that was waiting on city inspectors to complete their job. I highly " .
                    "recommend Roman Roofing.",
                "created_at" => "2019-04-30 00:00:00",
            ),
            array(
                "name" => "Kyle L",
                "rating" => 5,
                "comment" => "Roman Roofing has done several roofs for me include my parent's property and duplex as well as " .
                    "several other rental properties I manage. It is hard to find a roofer that you can trust but I trust " .
                    "them completely with their pricing and quality of work. All of the projects were completed on time " .
                    "with no issues. I recommend them anyone who is looking for roof to be complete.",
                "created_at" => "2019-05-07 00:00:00",
            ),
            array(
                "name" => "Ellen P",
                "rating" => 5,
                "comment" => "The folks at Roman Roofing are polite, attentive and they follow thru. Do a good job and will " .
                    "make a correction as needed. Had alot of confidence in them prior to the job and now after the " .
                    "job was done. They really seem to know what they are doing.",
                "created_at" => "2019-05-11 00:00:00",
            ),
            array(
                "name" => "Tracy H",
                "rating" => 5,
                "comment" => "Roof and Gutters look wonderful! The staff cleaned up after each step from the removal to the " .
                    "reroof process. After each step a manger came out and checked each step before the next step " .
                    "was done. I have already saved money on my 1st electric bill after having the new roof installed",
                "created_at" => "2019-05-16 00:00:00",
            ),
            array(
                "name" => "Joel T",
                "rating" => 5,
                "comment" => "I highly recommend Roman Roofing! The work was performed in an excellent, professional " .
                    "manner. All of the Roman Roofing workers were prompt, polite and wore clothing with the " .
                    "company name and logo. They located 3 places that needed to be repaired, but honored their " .
                    "estimate to repair only two places. I saw them do the repairs, but now it is impossible to identify " .
                    "the three places that were repaired!",
                "created_at" => "2019-05-20 00:00:00",
            ),
            array(
                "name" => "Jason O",
                "rating" => 5,
                "comment" => "Ask for Clint!!! Cant say enough good things about these guys. My wife and I are in the process of " .
                    "selling our home and were in a time crunch to get a roof leak fixed before selling. From the time I " .
                    "made the first phone call to the roof being fixed was less than a week total and for an incredible " .
                    "price to boot. Make sure you ask for Clint, he was great and very professional. Thank you guys so " .
                    "much!!",
                "created_at" => "2019-06-03 00:00:00",
            ),
            array(
                "name" => "Joy S",
                "rating" => 5,
                "comment" => "Work quickly done -- 13-hour days. Top quality at reasonable price for Owens-Corning.",
                "created_at" => "2019-06-02 00:00:00",
            ),
            array(
                "name" => "James D",
                "rating" => 1,
                "comment" => "Roman roofing was very had to communicate with and had numerous unexplained delays. They " .
                    "would make promises and not fulfill them. Damaged my driveway and were very quick to make it " .
                    "clear they would not help me fix it. Overall very poor customer service. Only thing they did quickly " .
                    "was collect money.",
                "created_at" => "2019-06-03 00:00:00",
            ),
            array(
                "name" => "J B",
                "rating" => 5,
                "comment" => "I had a pleasant experience with Roman Roofing. They put me at ease dealing with insurance and " .
                    "inspections And getting the job done in the allotted time frame. I got the shingles I waned and " .
                    "would recommend Roman and use them again. ***** ******",
                "created_at" => "2019-06-04 00:00:00",
            ),
            array(
                "name" => "Jim and Lorry P",
                "rating" => 5,
                "comment" => "After a bad experience with a local roofing company we needed a company that we could " .
                    "depend on to do what they said they would do, in the time that they said they would do it, and " .
                    "have the job completed satisfactorily when promised. Roman Roofing in Cape Coral was highly " .
                    "recommended to us. A family owned business, Roman Roofing went above and beyond our " .
                    "expectations. Each of the employees at Roman has a job to do that keeps the business running " .
                    "smoothly and efficiently and keeps their customers informed. Our salesman Clint was very " .
                    "professional and knowledgeable and he was key in our decision to select Roman Roofing to " .
                    "install our roof. Allen, in operations, answered our many questions and kept us informed of the " .
                    "timeline for the tearoff and the roof installation as well as when inspections from the city could be " .
                    "expected. His communication assured us that we made the right decision. Michael and his crew " .
                    "did a great job working hard in completing our 5100 sq foot tear off in only 1 day. Edwin and his " .
                    "crew who installed the shingles, worked efficiently and completed their job in 1 day as well. Both " .
                    "crews were very professional and polite. JR and Jamey's follow ups assured us that the work was " .
                    "done to code and ready for the city inspection, and that the cleanup was done to our satisfaction. " .
                    "The girls in the office also helped us navigate through an online portal, a communication tool that " .
                    "keeps the customer informed. Norm, the owner of Roman Roofing, who we also had the pleasure " .
                    "of meeting, is very personable and he is passionate about making sure that all of his customers " .
                    "have quality roofs installed correctly in a timely manner, as well as making sure each customer is " .
                    "completely satisfied with the job done. Roman Roofing is an accredited business with the Better " .
                    "Business Bureau having an A+ rating. We would highly recommend Roman for their reliability, " .
                    "honesty and superb workmanship. We are very pleased with our new roof. *** and ***** *****",
                "created_at" => "2019-06-06 00:00:00",
            ),
            array(
                "name" => "Bruce R",
                "rating" => 4,
                "comment" => "I recommend this company, and if i had to do it again I think I would choose Roman. The work " .
                    "seems to have been done well, the workers seemed competent and courteous. Everyone I had " .
                    "verbal interaction with seemed professional. The work took nearly 6 weeks from removing the " .
                    "old roofing to the installation of gutters and downspouts (by what I believe was a sub-contractor). " .
                    "However, there is a general lack of good communication skills with this company. One day, as I " .
                    "was getting ready for work, a small crew showed up to install the skylight tube. As there had " .
                    "been no warning this was going to happen on this, nor any particular day, I had to get my retired " .
                    "brother-in-law to come over to babysit the work so I would not have to miss work. Calls and " .
                    "emails made to various members of the company went ignored, so I was left in the dark about " .
                    "schedule and progress. The gutter and downspout installation was apparently done by a subcontractor " .
                    "(with a different company name than Roman on his vehicles). This was another " .
                    "surprise appearance....and the contractor was cursing my landscaping and the fence on the sides " .
                    "of my property for making the installation a challenge (how many homes do not have fencing and " .
                    "landscaping?!). When paying the final bill, I was surprised to find that a bunch of plywood was " .
                    "needed for replacement, hence a higher cost than the original price quote. .......I think mainly on " .
                    "the shed, as later I found the shed was infested with termites. Again with the communication! IF " .
                    "workers found they had to replace nearly the entire plywood sheathing on a 8x14 shed roof " .
                    "because of termite damage, wouldn't it make sense to communicate this to the homeowner?! " .
                    "rather than replacing the wood and saying nothing? So, again, I say the work was done well by " .
                    "Roman, but there's room for improvement in the communication aspect, on all levels of this " .
                    "company.",
                "created_at" => "2019-06-09 00:00:00",
            ),
            array(
                "name" => "Debbie C",
                "rating" => 5,
                "comment" => "Roman Roofing has been great from the first phone call to the last. Questions have always been " .
                    "answered promptly even when Frank was on vacation. The roof was finished in a timely manner " .
                    "and it looks great! Thank you Roman Roofing for an honest transaction and I would highly " .
                    "recommend your company.",
                "created_at" => "2019-06-11 00:00:00",
            ),
            array(
                "name" => "Geri O",
                "rating" => 5,
                "comment" => "We contracted with Roman Roofing to replace our roof. Frank S. was our sales representative. He " .
                    "was polite, professional and efficient. He stayed in contact with us thru the entire process. Alix " .
                    "was our foreman. He and his crew were amazingly diligent, regarding a work ethic. They were " .
                    "prompt and demonstrated expertise installing our roof. I would highly recommend Roman " .
                    "Roofing for all roofing needs. They are a reputable and honest company. *****",
                "created_at" => "2019-06-11 00:00:00",
            ),
            array(
                "name" => "Gayle K",
                "rating" => 5,
                "comment" => "Roman Roofing did an excellent job replacing my wind damaged roof. Frank and his associates " .
                    "were all very professional in all aspects of the job. He also handled many problems with my " .
                    "insurance claim.",
                "created_at" => "2019-06-25 00:00:00",
            ),
            array(
                "name" => "David H",
                "rating" => 5,
                "comment" => "These guys were great all the way. On time and job was done perfect. If I ever need anymore repairs these guys will get the call. 5 stars all the way.",
                "created_at" => "2019-06-28 00:00:00",
            ),
            array(
                "name" => "tariq h",
                "rating" => 5,
                "comment" => "very happy with their work done would surely give them work again when required my roof looks " .
                    "very nice job done on time every thing went smooth staff was very help full they keep you in " .
                    "touch about job via phone and email before the work start",
                "created_at" => "2019-08-06 00:00:00",
            ),
            array(
                "name" => "Jan R",
                "rating" => 5,
                "comment" => "Roofers were excellent, Salesman F. has walked Me through from start to finish timeline, " .
                    "everything went smoothly! Could not have asked for a more efficient Company! job was " .
                    "completed as promised & on time, definitely recommend",
                "created_at" => "2019-08-27 00:00:00",
            ),
            array(
                "name" => "KellyT",
                "rating" => 5,
                "comment" => "We can not say enough good things about Mark S., and Roman Roofing. Mark was professional " .
                    "from the moment we contacted him about our roof, right through to completion. He listened, and " .
                    "answered all of our questions, throughout the process. When he made a promise, he kept it. HE " .
                    "followed up with us, instead of us having to follow-up with him. He let us know what was going on " .
                    "at each stage of the re-roofing, and sent photos. We even received a nice note, thanking us for " .
                    "our business, after the job was completed. Of course, time will tell how well the roof holds up, but " .
                    "based on what we saw in the photos that were sent to us, and the customer service experience " .
                    "we had, we feel confident that it will be a long-lasting roof, and if we do happen to have any " .
                    "problems, they will be responsive. Needless to say, we would not hesitate to refer Roman " .
                    "Roofing to anyone looking for roof repairs and re-roofing.",
                "created_at" => "2019-10-07 00:00:00",
            ),
            array(
                "name" => "James V",
                "rating" => 5,
                "comment" => "What a great experience dealing with Roman Roofing. Fast and efficient, no surprises. Marcus " .
                    "was terrific and explained everything. I would highly recommend them to anyone needing their " .
                    "services ***********",
                "created_at" => "2019-10-23 00:00:00",
            ),
            array(
                "name" => "Frank C",
                "rating" => 5,
                "comment" => "******** Roman Roofing, Inc. was clearly the best choice of all the Roofing Companies that " .
                    "knocked on my door when a recent Tornado hit our home. Their Representative was " .
                    "professional, knowledgeable and a real, caring person. He listened! Imagine that............... He " .
                    "offered immediate help and a long term plan. A rare experience for me.",
                "created_at" => "2019-10-26 00:00:00",
            ),
            array(
                "name" => "Philip M",
                "rating" => 5,
                "comment" => "Even with all of my years experience, I don't see how things could have went any better. As a " .
                    "retired 34 year veteran of building subdivisions I can honestly say that my overall experience with " .
                    "Roman Roofing on my personal home was good enough in all areas to be quite surprising and " .
                    "even far better than I expected. They started and finished in 1 week from the start date. Even " .
                    "though there were significant issues discovered once the old roof was removed, the repairs were " .
                    "made quickly, without delay and at a price that was quite fair. I am well pleased.",
                "created_at" => "2019-11-08 00:00:00",
            ),
            array(
                "name" => "Dieter H",
                "rating" => 5,
                "comment" => "We are very satisfied, and impressed with the job Roman Roofing did for us. We've had poor, to " .
                    "okay experiences with contractors in the past and didn't know what we'd actually get from this " .
                    "company. Well....they delivered great service with great communication throughout the project. " .
                    "I'd use them again & we (my wife and I), recommend them to others. Nice work and thank you for " .
                    "a job well done! This was an uncommon experience for us with not having to debate or argue " .
                    "about timing, delays, quality of work, or extra charges & so on. A breath of fresh air! Thank you",
                "created_at" => "2019-11-09 00:00:00",
            ),
            array(
                "name" => "Douglas D",
                "rating" => 5,
                "comment" => "The roofing, material and sales are exactly as agreed on. Removal and install of our roof was " .
                    "done in a verry timely and professional manner with Alex the crews foreman going well above. " .
                    "We are very pleased that we chose Roman Roofing.",
                "created_at" => "2019-11-26 00:00:00",
            ),
            array(
                "name" => "Angie & Brian W",
                "rating" => 5,
                "comment" => "Mark S. was our sales guy. (fantastic person to deal with) Roman roofing's work was absolutely " .
                    "amazing from start to finish. No problems with any of the people connected to Roman Roofing. " .
                    "Good job guys!!!!!!!",
                "created_at" => "2019-11-27 00:00:00",
            ),
            array(
                "name" => "Carol A",
                "rating" => 5,
                "comment" => "We recently had our roof replaced by Roman Roofing. Ralph C. was with us throughout the whole " .
                    "process. From selecting the roof to working with our insurance company. Everything exceeded " .
                    "our expectations. My husband is a retired carpenter- so he is very picky. He was very impressed " .
                    "with the work that was done. We have a gorgeous roof and our neighbors are jealous. We highly " .
                    "recommend Ralph and Roman Roofing.",
                "created_at" => "2019-12-21 00:00:00",
            ),
            array(
                "name" => "Dennis P",
                "rating" => 5,
                "comment" => "Roman Roofing is one of the very best when it comes to roof repair. They are extremely " .
                    "professional in every detail of their work. Jamie the person who did our estimate actually fixed a " .
                    "small problem on our roof \"at no cost\" while doing his evaluation and detailed estimate. Jake " .
                    "Shoemaker is the one who makes this company shine when it comes to complete quality " .
                    "workmanship. Start to finish they were courteous - on time - and completed the work with care to " .
                    "make sure it was done correct. I would highly recommend Roman Roofing for all your roofing " .
                    "needs !!",
                "created_at" => "2020-02-10 00:00:00",
            ),
            array(
                "name" => "Kevin H.",
                "rating" => 5,
                "comment" => "Have used Roman Roofing for all my investment properties and they are extremely organized!! " .
                    "Great customer care and never have any issues. I recently hired them to do my personal home " .
                    "which was a larger tile roof. They finished in 13 days and all my neighbors were in shock!! Not to " .
                    "mention the crews they use are friendly and very polite! Other roofs in my neighborhood all took " .
                    "a month plus!! Highly recommend this company.",
                "created_at" => "2020-02-13 00:00:00",
            ),
            array(
                "name" => "Donna T",
                "rating" => 5,
                "comment" => "Roman Roofing is a wonderful company willing to go above and beyond for their customers. They are very prompt & pleasant to be around while they are working.",
                "created_at" => "2020-02-21 00:00:00",
            ),
            array(
                "name" => "Judy M",
                "rating" => 5,
                "comment" => "I chose Roman Roofing for a new roof on our home. They did a great job informing us of " .
                    "everything they would do, materials, schedules and price. Even when the job was completed they " .
                    "came back to help us with an unrelated solar pool heating situation. Roman Roofing is a great " .
                    "company to work with!",
                "created_at" => "2020-02-22 00:00:00",
            ),
            array(
                "name" => "Linda D",
                "rating" => 5,
                "comment" => "This company is great! Ralph the sales person is outstanding and really educates you. The entire experience was outstanding. I highly recommend Roman Roofing!",
                "created_at" => "2020-03-03 00:00:00",
            ),
            array(
                "name" => "Eric N",
                "rating" => 4,
                "comment" => "We did a lot of research and interviews of prospective roofing companies (12) and found Roman " .
                    "Roofing to be who we felt would give us the best roof that I could get with a great warranty and a " .
                    "company with a solid reputation. Marcus B. my salesman, he was very professional and " .
                    "knowledgeable of his entire product line. I appreciated the extra time that he took with me and " .
                    "would recommend him highly to anyone that would ask. As for the rest of the process with the " .
                    "main office and work crew; The final product did turn out great, but there were some " .
                    "unnecessary and preventable issues that I wish I did not have to deal with. One thing they never " .
                    "updated their website for our progress even after phone calls to fix the problem. Two, before " .
                    "they started shingling the roof I showed them pictures of rotten wood that was still a part of the " .
                    "roof in the attic, and they told me it wasn't. They finished the roof, then I proved to them that their " .
                    "crew did leave the rotten wood with more pictures, and a few days later they sent another crew " .
                    "to repair my NEW roof already. Like I said, the final product looks great, but the journey wasn't " .
                    "fun.",
                "created_at" => "2020-03-18 00:00:00",
            ),
            array(
                "name" => "Michael R",
                "rating" => 5,
                "comment" => "Their work was good quality, done as advertised. Easy to deal with. I would call again for any services needed.",
                "created_at" => "2020-03-26 00:00:00",
            ),
            array(
                "name" => "Kathy R",
                "rating" => 5,
                "comment" => "Fantastic company. Totally exceeded our expectations. A neighbor also used them on my advice, " .
                    "they are equally satisfied. Ralph was highly professional and helped us through a rough spot with " .
                    "our insurance company keeping us informed. This is a 5 star company from start to finish.",
                "created_at" => "2020-03-30 00:00:00",
            ),
            array(
                "name" => "Lisa C",
                "rating" => 5,
                "comment" => "I recently had a new roof put on my home and was very pleased with the work that was done by " .
                    "Roman Roofing. I searched for months and received many quotes for the job, and their pricing " .
                    "was competitive. Not the cheapest, but not the most expensive. I will say that through the entire " .
                    "process they were very patient and professional. I cannot be more pleased with the end product. " .
                    "Thank you to Marcus and everyone else with Roman that helped me. You guys represent what " .
                    "companies should be...a good company that does honest work. Would highly recommend them " .
                    "to anyone I know.",
                "created_at" => "2020-04-08 00:00:00",
            ),
            array(
                "name" => "Mark C",
                "rating" => 5,
                "comment" => "My experience is that it is very rare to get high quality work performed on schedule at a " .
                    "reasonable cost. I was skeptical when Roman Roofing's estimator, Marcus B., arrived at my house " .
                    "two days ahead of his scheduled appointment, provided me with an estimate for a new roof that " .
                    "was in the middle of the range of estimates I had already received, and then promised to " .
                    "complete the work within two weeks. But, incredibly, the work was done exactly as promised. It " .
                    "was like \"clockwork.\" The work was started within a couple days (not weeks or months) of my " .
                    "signing the contract. The old roof was torn off and sections of sheeting replaced within a day, the " .
                    "inspection was performed the next day, the new roofing material was delivered the following day, " .
                    "workers installed the new roof two days later, and the final inspection was performed a day or " .
                    "two after that. I should mention that the workers were generally careful to clean up the yard and " .
                    "flower beds when they finished removing the old roof. Except for gutter replacement, which was " .
                    "done a couple weeks later, the entire roof replacement was accomplished within two weeks of " .
                    "my signing the contract. I am still amazed by how much was accomplished in such a short period " .
                    "of time. And I am also amazed that there is a company that still honors every single one of its " .
                    "promises. To me, this is truly extraordinary. As a final comment, it's been roughly a month since " .
                    "the roof was replaced, and we have not had any rain since then. So, I don't know if the there are " .
                    "any leaks in the new roof. I am sure I will find out as we go into the rainy season. But I do feel that " .
                    "if there is a problem, Roman Roofing will honor their 5-year warranty just as they have honored " .
                    "their other promises.",
                "created_at" => "2020-04-15 00:00:00",
            ),
            array(
                "name" => "Brian J",
                "rating" => 5,
                "comment" => "Excellent work, great communication. I would highly recommend Roman for your roof project",
                "created_at" => "2020-04-15 00:00:00",
            ),
            array(
                "name" => "Susan C",
                "rating" => 5,
                "comment" => "I called Roman Roofing to have an inspection On a potential property for purchase. Marcus was " .
                    "prompt, professional and courteous. He explained the roof situation and answered My questions. " .
                    "As it turned out I am not purchasing the property and will call Roman Roofing again when I find " .
                    "another home I'm interested in buying.",
                "created_at" => "2020-04-16 00:00:00",
            ),
            array(
                "name" => "Laura R",
                "rating" => 5,
                "comment" => "Roman Roofing was fantastic to work with from start to finish. The estimate was accurate, the " .
                    "work was don in a timely manner and the end result was great. The office is wonderful to deal " .
                    "with as you always talk to a real person and they take care of any question or concern promptly " .
                    "and completely. I would highly recommend this company for roof replacement or repair. I would " .
                    "use them again in a minute.",
                "created_at" => "2020-04-29 00:00:00",
            ),
            array(
                "name" => "Rick C.",
                "rating" => 5,
                "comment" => "Roman Roofing was absolutely wonderful to work with from the first step. Mark Simpson was " .
                    "amazingfast estimate, superior knowledge of roofing products, very responsive, customer " .
                    "focused and honest. I was amazed at the customer service of Mark keeping me informed at every " .
                    "step. The product was delivered professionally, the tear-off crew was hardworking/meticulous, " .
                    "the install team was amazing/hard working, and the site manager Alex Alonso was outstanding. " .
                    "The crews for both tear-of and installation were amazingeach day they cleaned the entire " .
                    "worksite combing flower beds with magnets and picking up debris, tarps, etc. They were much " .
                    "quicker in completing the job than expected and always friendly. I am a believer in customer " .
                    "service and give them the highest rating possible. Kudos to Mark, Alex, and the professional " .
                    "crews for outstanding work/service!",
            ),
            array(
                "name" => "Alice S",
                "rating" => 5,
                "comment" => "I highly recommend this roofing company. The salesman, office staff and workers continuously " .
                    "worked together to get the job completed. The roof repair was done on time and within our " .
                    "budget. Dealing with them was a pleasure. They are professional, polite and their work was A+.",
                "created_at" => "2020-05-13 00:00:00",
            ),
            array(
                "name" => "Gerard L.",
                "rating" => 5,
                "comment" => "We met Dave at our community home Show in Pine Lakes CC and we are so thankful we did. " .
                    "From the beginning to end , he was the consummate professional. We had some roof damage " .
                    "caused by a storm in February, plus as it turns out, some residual damage from Hurricane Irma in " .
                    "2018. After talking with Dave and getting his estimate, we contacted our insurance company. " .
                    "Their brand new field adjuster spent about 15 or 20 minutes on the roof and took one picture. " .
                    "Dave spent over an hour and took over 40 pictures. The insurance company initially turned us " .
                    "down for a new roof, based on their adjuster's evaluation and report. Dave is a fighter and was " .
                    "not going to let go of this until we got what we wanted. A new roof. Our Insurance adjuster in the " .
                    "office even complimented to us that Dave was extremely professional and persistent in his followup. " .
                    "So after a couple of months, the insurance came through and funded the project. Once we " .
                    "gave Dave the go ahead, his crew stripped the old roof in less than a day (including cleaning up " .
                    "around the house) and the roofers finished the job in another day. The whole time, members of " .
                    "the Roman team kept us informed of every step in the process (delivery of the roll-off dumpster, " .
                    "delivery of the shingles, etc.). Dave is easy to talk to and very conscientious. I would not hesitate " .
                    "to call ROMAN ROOFING in the future if we ever need a new roof.",
                "created_at" => "2020-05-13 00:00:00",
            ),
            array(
                "name" => "charles j",
                "rating" => 5,
                "comment" => "Contacted Roman Roofing for an estimate to replace our twenty one year old roof. They came " .
                    "same day, and the salesman, Mr Frank S, was very thorough in explaining the roofing tare off, " .
                    "type of shingles they sold, provided samples, and explained the process once the contract was " .
                    "signed. Once we signed the contract, the job took less than two weeks to complete. We got new " .
                    "shingles, gutters, downspouts and the fascia covered in aluminum. They were very professional " .
                    "in protecting our plantings, lani, and the Air Conditioned. The job foreman, Mr Alex A. was on the " .
                    "job making sure everything was going as we had agreed to on the contract. He professionalism " .
                    "made us feel the job would turn out great. Would recommend Roman Roofing 100% for any " .
                    "roofing needs you may have.",
                "created_at" => "2020-05-15 00:00:00",
            ),
            array(
                "name" => "stephen l",
                "rating" => 5,
                "comment" => "i am involved with a contract dispute with Big C roofing over the new roof i had done by them. i " .
                    "had to get an independent roofing contractor's statement regarding the Unprofessional, Very " .
                    "Poor job they did. Roman Roofing was receommended to me by my daughters boss who has " .
                    "used them in the past. within a few hours of speaking to Ralph, he was at my house and going " .
                    "over my roof job with a Fine Tooth Comb. he was very prompt, very professional and went over " .
                    "everything with me and brought up suggestions as how to make my flat roof safer and more " .
                    "water resistent. needless to say i will be going with Roman Roofing to un-do what the first " .
                    "contractor did to my roof. based on the other reviews i've seen here, my first Great Experience " .
                    "with Roman Roofing will transfer to the GREAT RESULT i was looking for to start with.",
                "created_at" => "2020-05-23 00:00:00",
            ),
            array(
                "name" => "Manuel C",
                "rating" => 5,
                "comment" => "Roman Roofing says what they mean, and mean what they say. Learned about Roman Roofing " .
                    "from a neighbor in our community. It seems that the neighbor had fired two previous roofing " .
                    "companies for non-performance and hired Roman Roofing. The neighbor was very happy with " .
                    "the work done by Roman Roofing. This was after hurricane Irma. The recommendation was " .
                    "heeded by our board of directors at Sunset Cay Villas X at Port of the Islands. We also had " .
                    "hurricane damage from hurricane Irma and finally received a settlement allowing us to proceed " .
                    "with replacement of the roof on our condo building. The board interviewed several roofing " .
                    "contractors and found Roman Roofing to be very competitive price wise and Roman Roofing also " .
                    "made it quite clear that they wanted our job. During the process of interviewing contractors, " .
                    "Ralph C, Estimator and Norm D, Owner gave us a very impressive presentation outlining the work " .
                    "they would do, how they would do it, and when they would do it. They did everything they " .
                    "promised and also some extras at no additional cost. Work was performed on schedule, as " .
                    "promised. Work crews were respectful and polite, cleaned the work site after work every night. " .
                    "Ralph was a real pleasure to work with as well. He really wanted us to be satisfied and went " .
                    "above and beyond to make it so. Our association highly recommends Roman Roofing.",
                "created_at" => "2020-05-28 00:00:00",
            ),
            array(
                "name" => "John A",
                "rating" => 5,
                "comment" => "Very prompt and thorough on the 2 roof repairs I've had to have done. I have no problem " .
                    "recommending them. Marcus the estimator took us through what would be done, why it needed " .
                    "to be done and the cost. We will strongly consider them for our future roofing needs!",
                "created_at" => "2020-06-13 00:00:00",
            ),
            array(
                "name" => "Perri W",
                "rating" => 5,
                "comment" => "Punctuality, Quality, Price! Roman Roofing did a spectacular job on our standing seam roof. It " .
                    "upgraded the aesthetic of our home exponentially. The crew that tore off our old shingle roof in " .
                    "particular was amazing. I'm not sure if I found a single nail in the yard. We're really pleased in our " .
                    "lifetime investment. They even put standing seam roofing on my chicken coop that matches the " .
                    "house! Don't waste your time calling any of the other companies in town.",
                "created_at" => "2020-06-17 00:00:00",
            ),
            array(
                "name" => "Mable S",
                "rating" => 5,
                "comment" => "Roman Roofing is A+ in every way. Clint N, our sales person is local to our community in Charlotte " .
                    "County. He was so informative with product information and so kind in his handling of us even" .
                    "after the job was started. He made himself available to me and if I texted him with a question he" .
                    "got back within minutes. We were very well taken care of. The crew that put the roof on are all" .
                    "hard working men and did a beautiful job. I highly recommend Roman Roofing.",
                "created_at" => "2020-06-19 00:00:00",
            ),
            array(
                "name" => "DONNA C",
                "rating" => 5,
                "comment" => "Roman Roofing did a great job on our new roof. They were very professional and got the job " .
                    "done quickly. They were very responsive and helpful whenever we had questions. We were very " .
                    "impressed with the quality of the work, and the ease of dealing with everyone involved. I highly " .
                    "recommend them",
                "created_at" => "2020-06-19 00:00:00",
            ),
            array(
                "name" => "Steve B",
                "rating" => 5,
                "comment" => "When you began the roofing project, we thought it would take a week or so to complete. We " .
                    "were happily surprised that you finished in two days. You had done an excellent job. We are " .
                    "completely pleased with your work.",
                "created_at" => "2020-07-01 00:00:00",
            ),
            array(
                "name" => "William W",
                "rating" => 5,
                "comment" => "Our roof was 20 years old and while still in good condition we decided to be proactive and " .
                    "replace it. We have some experience with two other companies and had been watching a new " .
                    "roof install by Roman and liked what we saw. Ralph C. was our estimator and we connected very " .
                    "well, he educated us on quality, choices and the installation process. He spent extra time locating " .
                    "a specific sample we were interested in that we ultimately selected for our roof. the time from " .
                    "selection to initiation of work was about two weeks, the tear off crew were fast and cleaned up " .
                    "the yard in one day. The shingles were delivered the next day and work started shortly " .
                    "afterwards. The crew were excellent workers and the leader always responded to my questions " .
                    "and requests. they cleaned up every night and their work was outstanding, we have had many " .
                    "positive remarks on the appearance of our new roof. We recommend Roman Roofing 100%.",
                "created_at" => "2020-07-08 00:00:00",
            ),
            array(
                "name" => "Deanna C",
                "rating" => 5,
                "comment" => "Roman Roofing was prompt with responses and scheduling, they completed the job fairly quickly " .
                    "and did an amazing job! Pricing was actually better than several other quotes I received. I highly " .
                    "recommend them.",
                "created_at" => "2020-07-08 00:00:00",
            ),
            array(
                "name" => "Doug B",
                "rating" => 5,
                "comment" => "We were extremely happy with Roman Roofing. They did everything they said they would do and in a timely fashion",
                "created_at" => "2020-07-09 00:00:00",
            ),
            array(
                "name" => "Rosana H",
                "rating" => 5,
                "comment" => "Professional, quality and superb workmanship from quoting, reception, roofers and service people.",
                "created_at" => "2020-07-10 00:00:00",
            ),
            array(
                "name" => "Kevin M",
                "rating" => 5,
                "comment" => "Wow just Wow. My wife and I love our new premium copper colored steel roof. We chose Roman " .
                    "because of their experience and reputation. From Ralph our salesman to Devon and Alan the " .
                    "project managers Roman kept us apprised of the schedule and were on time every time. I was " .
                    "glad I chose the extra purlins and insulation underlayment since it has made a huge difference. " .
                    "My house is quieter in rainstorms, the AC guys love the cooler attic versus the standard steel roof " .
                    "installations, and my electric bills are about $50 less per month. As I mentioned to Norm the " .
                    "owner, this is the last roof I plan on installing, give me something we can BOTH be proud of. I feel " .
                    "Roman delivered on that promise and every time I see the sun gleam off my copper roof I am " .
                    "happy I made the choice I did. Put my wife and I down as happy customers and I would definitely " .
                    "recommend them to family and friends!",
                "created_at" => "2020-07-11 00:00:00",
            ),
            array(
                "name" => "Robert H",
                "rating" => 5,
                "comment" => "Did a great job & stopped by afterwards to correct & issues we may have had. I would highly recommend their services!",
                "created_at" => "2020-07-13 00:00:00",
            ),
        );

        foreach ($reviews as $review) {
            \App\Review::create($review);
        }

    }
}
