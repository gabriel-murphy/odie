<?php

use Illuminate\Database\Seeder;

class StructuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $structures = array(
            array(
                "id" => 1,
                "project_id" => 1,
                "name" => NULL,
                "measurements" => [
                    'square_feet' => '38592',
                    'stories' => '2',
                    'facets' => '8',
                    'pitch' => '8',
                    'ridges' => '104',
                    'ridge_vent' => '100',
                    'hips' => '172',
                    'valleys' => '81',
                    'rakes' => '49',
                    'eaves' => '302',
                    'flashing' => '0',
                    'step_flashing' => '0',
                ],
                "penetrations" => array(
                    'boot' => [
                        ['X' => '431', 'Y' => '164', 'material_id' => 4]
                    ],
                    'vent' => [
                        ['X' => '256', 'Y' => '146', 'material_id' => 278,],
                        ['X' => '574', 'Y' => '214', 'material_id' => 279]
                    ]
                ),
                "created_at" => "2020-04-10 17:03:16",
                "updated_at" => "2020-04-10 17:03:16",
            ),
        );

        foreach ($structures as $structure) {
            \App\Structure::create($structure);
        }
    }
}
