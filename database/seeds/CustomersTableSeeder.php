<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = array(
            array(
                "id" => 1,
                "client_id" => 1,
                "first_name" => "Maria",
                "last_name" => "Morales",
                "associate_type_id" => 1,
                "created_at" => "2020-02-06 09:23:00",          // Timestamp from GiddyUp
            ),
            array(
                "id" => 2,
                "client_id" => 1,
                "first_name" => "Donna",
                "last_name" => "Householder",
                "secondary_first_name" => "PJ",
                "secondary_last_name" => "Householder",
                "associate_type_id" => 1,
                "created_at" => "2020-06-02 05:18:00",          // Timestamp from GiddyUp
            ),
            array(
                "id" => 3,
                "client_id" => 1,
                "first_name" => "Michelle",
                "last_name" => "Rossman",
                "secondary_first_name" => "Dennis",
                "secondary_last_name" => "Rossman",
                "associate_type_id" => 1,
                "created_at" => "2020-01-18 09:29:00",
            ),
            array(
                "id" => 4,
                "client_id" => 1,
                "first_name" => "Linda",
                "last_name" => "Crawford",
                "associate_type_id" => 1,
                "created_at" => "2020-01-18 11:47:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 5,
                "client_id" => 1,
                "first_name" => "Denise",
                "last_name" => "Bruce",
                "associate_type_id" => 1,
                "created_at" => "2020-06-03 05:04:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 6,
                "client_id" => 1,
                "first_name" => "Craig ",
                "last_name" => "MacLean",
                "secondary_first_name" => "Abby",
                "secondary_last_name" => "MacLean",
                "associate_type_id" => 1,
                "created_at" => "2020-06-03 13:33:27",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 7,
                "client_id" => 1,
                "first_name" => "Millard",
                "last_name" => "Allen",
                "secondary_first_name" => "Cathy",
                "secondary_last_name" => "Allen",
                "associate_type_id" => 1,
                "created_at" => "2020-06-09 11:30:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 8,
                "client_id" => 1,
                "first_name" => "Bruce",
                "last_name" => "Kringlie",
                "secondary_first_name" => "Peggy",
                "secondary_last_name" => "Kringlie",
                "associate_type_id" => 1,
                "created_at" => "2020-06-08 11:40:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 9,
                "client_id" => 1,
                "first_name" => "Frank",
                "last_name" => "Brown",
                "secondary_first_name" => "Gwen",
                "secondary_last_name" => "Brown",
                "associate_type_id" => 1,
                "created_at" => "2020-06-08 09:41:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 10,         // *
                "client_id" => 1,
                "first_name" => "William",
                "last_name" => "Bockhorst",
                "associate_type_id" => 1,
                "created_at" => "2020-06-08 10:29:00",
            ),
            array(
                "id" => 11,
                "client_id" => 1,
                "first_name" => "Kevin",
                "last_name" => "Tully",
                "secondary_first_name" => "Sharon",
                "secondary_last_name" => "Tully",
                "associate_type_id" => 1,
                "created_at" => "2020-06-17 15:39:00",
            ),
            array(
                "id" => 12,
                "client_id" => 1,
                "first_name" => "Tachung",
                "last_name" => "Yih",
                "secondary_first_name" => "Debbie",
                "secondary_last_name" => "Yih",
                "associate_type_id" => 1,
                "created_at" => "2020-06-25 12:28:00",
            ),
            array(
                "id" => 13,
                "client_id" => 1,
                "first_name" => "Frances",
                "last_name" => "Turowetz",
                "secondary_first_name" => "Ronald",
                "secondary_last_name" => "Turowetz",
                "associate_type_id" => 1,
                "created_at" => "2020-07-14 17:32:00",
            ),
            array(
                "id" => 14,
                "client_id" => 1,
                "first_name" => "James",
                "last_name" => "Mitchell",
                "secondary_first_name" => "Maureen",
                "secondary_last_name" => "Mitchell",
                "associate_type_id" => 1,
                "created_at" => "2020-04-21 09:02:00",
            ),
            array(
                "id" => 15,
                "client_id" => 1,
                "first_name" => "Mike",
                "last_name" => "Dimond",
                "associate_type_id" => 1,
                "created_at" => "2020-06-11 09:35:00",
            ),
            array(
                "id" => 16,
                "client_id" => 1,
                "first_name" => "Susan",
                "last_name" => "Cortner",
                "secondary_first_name" => "Victor",
                "secondary_last_name" => "Cortner",
                "associate_type_id" => 1,
                "created_at" => "2020-07-01 03:36:00",
            ),
            array(
                "id" => 17,
                "client_id" => 1,
                "first_name" => "Robert",
                "last_name" => "Hiatt",
                "secondary_first_name" => "Jill",
                "secondary_last_name" => "Hiatt",
                "associate_type_id" => 1,
                "created_at" => "2020-06-19 11:26:00",          // Timestamp in GiddyUp for Customer Creation
            ),
            array(
                "id" => 18,
                "client_id" => 1,
                "first_name" => "Gerald",
                "middle_name" => "H",
                "last_name" => "Laatsch",
                "secondary_first_name" => "Kimberly",
                "secondary_last_name" => "Laatsch",
                "associate_type_id" => 1,
                "created_at" => "2020-07-09 05:20:00",          // Timestamp in GiddyUp for Customer Creation
            ),
            array(
                "id" => 19,
                "client_id" => 1,
                "first_name" => "Lynn",
                "middle_name" => "W",
                "last_name" => "Buskard",
                "associate_type_id" => 1,
                "created_at" => "2020-07-13 13:14:00",          // Timestamp in GiddyUp for Customer Creation
            ),
            array(
                "id" => 20,
                "client_id" => 1,
                "first_name" => "Joel",
                "last_name" => "Georges",
                "associate_type_id" => 1,
                "created_at" => "2020-07-08 14:47:00",          // Timestamp in GiddyUp for Customer Creation
            ),
            array(
                "id" => 21,
                "client_id" => 1,
                "first_name" => "Patrick",
                "last_name" => "Wassmann",
                "secondary_first_name" => "Connie",
                "secondary_last_name" => "Wassmann",
                "associate_type_id" => 1,
                "created_at" => "2020-06-03 13:33:27",
                "updated_at" => "2020-06-03 13:33:27",
            ),
            array(
                "id" => 22,
                "client_id" => 1,
                "first_name" => "John",
                "last_name" => "Brooks",
                "secondary_first_name" => "Richard",
                "secondary_last_name" => "Brooks",
                "associate_type_id" => 1,
                "created_at" => "2020-01-23 16:33:00",          // Timestamp in GiddyUp for Customer Creation
                "updated_at" => "2020-06-22 14:31:24",  // *
            ),
            array(
                "id" => 23,
                "client_id" => 1,
                "first_name" => "Michael",
                "last_name" => "Murphy ",
                "secondary_first_name" => "Lisa",
                "secondary_last_name" => "Murphy",
                "associate_type_id" => 1,
                "created_at" => "2020-06-24 08:58:00",          // Timestamp in GiddyUp for Customer Creation
            ),
            array(
                "id" => 24,
                "client_id" => 1,
                "first_name" => "Collene",
                "last_name" => "Davis",
                "associate_type_id" => 1,
                "created_at" => "2020-06-24 08:58:00",          // Timestamp in GiddyUp for Customer Creation
            ),
            array(
                "id" => 25,
                "client_id" => 1,
                "first_name" => "Marc",
                "last_name" => "Brenscheidt",
                "associate_type_id" => 1,
                "created_at" => "2020-06-17 8:32:00",          // Timestamp in GiddyUp for Customer Creation
            ),
            array(
                "id" => 26,
                "client_id" => 1,
                "first_name" => "Gerald",
                "last_name" => "Shaffner",
                "secondary_first_name" => "Patricia",
                "secondary_last_name" => "Shaffner",
                "associate_type_id" => 1,
                "created_at" => "2020-06-16 12:17:00",
            ),
            array(
                "id" => 27,
                "client_id" => 1,
                "first_name" => "Donald",
                "middle_name" => "A",
                "last_name" => "Watt",
                "secondary_first_name" => "Sharleen",
                "secondary_middle_name" => "H",
                "secondary_last_name" => "Watt",
                "associate_type_id" => 1,
                "created_at" => "2020-06-16 16:53:00",
            ),
            array(
                "id" => 28,
                "client_id" => 1,
                "first_name" => "Alan",
                "last_name" => "Hartstein",
                "secondary_first_name" => "Sandra",               // Deceased
                "secondary_last_name" => "Hartstein",
                "associate_type_id" => 1,
                "created_at" => "2020-06-18 08:55:00",
            ),
            array(
                "id" => 29,
                "client_id" => 1,
                "first_name" => "John",
                "last_name" => "Black",
                "secondary_first_name" => "Sandra",
                "secondary_last_name" => "Black",
                "associate_type_id" => 1,
                "created_at" => "2020-07-22 13:16:00",
            ),
            array(
                "id" => 30,  // *
                "client_id" => 1,
                "first_name" => "William",
                "last_name" => "Davis",
                "secondary_first_name" => "Patricia",
                "secondary_last_name" => "Davis",
                "associate_type_id" => 1,
                "created_at" => "2020-07-22 13:16:00",
            ),
            array(
                "id" => 31,
                "client_id" => 1,
                "first_name" => "Kenneth",
                "last_name" => "Shivel",
                "secondary_first_name" => "Marjorie",
                "secondary_last_name" => "Shivel",
                "associate_type_id" => 1,
                "created_at" => "2020-07-18 15:13:00",
            ),
            array(
                "id" => 32,
                "client_id" => 1,
                "first_name" => "Brianne",
                "middle_name" => "Lopez",
                "last_name" => "Romano",
                "secondary_first_name" => "Richard",
                "secondary_last_name" => "Romano",
                "associate_type_id" => 1,
                "created_at" => "2020-07-30 15:11:00",
            ),
            array(
                "id" => 33,
                "client_id" => 1,
                "first_name" => "Erika",
                "last_name" => "Patalano",
                "associate_type_id" => 1,
                "created_at" => "2020-06-11 15:54:00",
            ),
            array(
                "id" => 34,
                "client_id" => 1,
                "first_name" => "Ronald",
                "last_name" => "Anderson",
                "secondary_first_name" => "Deborah",
                "secondary_last_name" => "Johnson",
                "associate_type_id" => 1,
                "created_at" => "2020-08-05 06:38:00",
            ),
            array(
                "id" => 35,
                "client_id" => 1,
                "first_name" => "James",
                "middle_name" => "Lincoln",
                "last_name" => "Dilling",
                "secondary_first_name" => "Tamera",
                "secondary_last_name" => "Dilling",
                "associate_type_id" => 1,
                "created_at" => "2020-07-28 10:32:00",
            ),
        );

        foreach ($customers as $customer) {
            //$phones = array();

//            if (isset($customer['work_phone'])) {
//                $phones[] = ['type' => 'work', 'number' => $customer['work_phone'],'extension'=>isset($customer['extension'])?$customer['extension']:""];
//                unset($customer['work_phone']);
//                unset($customer['extension']);
//            }
//            if (isset($customer['home_phone'])) {
//                $phones[] = ['type' => 'home', 'number' => $customer['home_phone'],'extension'=>isset($customer['extension'])?$customer['extension']:""];
//                unset($customer['home_phone']);
//                unset($customer['extension']);
//            }
//            if (isset($customer['cell_phone'])) {
//                $phones[] = ['type' => 'cell', 'number' => $customer['cell_phone'],'extension'=>isset($customer['extension'])?$customer['extension']:""];
//                unset($customer['cell_phone']);
//                unset($customer['extension']);
//            }
//            if (isset($customer['fax_phone'])) {
//                $phones[] = ['type' => 'fax', 'number' => $customer['fax_phone'],'extension'=>isset($customer['extension'])?$customer['extension']:""];
//                unset($customer['fax_phone']);
//                unset($customer['extension']);
//            }
            unset($customer['secondary_first_name'], $customer['secondary_last_name'], $customer['secondary_middle_name']);

            //$customer['phones'] = $phones;

            \App\Customer::create($customer);
        }

        // Seeds Customer Emails

        $emails = array(
            array(
                "id" => 1,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "chipsaregood33@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 1,
                "is_primary" => 1,
            ), array(
                "id" => 2,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Donna.vt@pobox.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 2,
                "is_primary" => 1,
            ), array(
                "id" => 3,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "denisegb@embarqmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 5,
                "is_primary" => 1,
            ), array(
                "id" => 4,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "amaclean@ige.org",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 6,
                "is_primary" => 1,
            ), array(
                "id" => 5,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "whiffleallen@yahoo.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 7,
                "is_primary" => 1,
            ), array(
                "id" => 6,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Nursex2@comcast.net",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 9,
                "is_primary" => 1,
            ), array(
                "id" => 7,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "ktully@tullycreative.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 11,
                "is_primary" => 1,
            ), array(
                "id" => 8,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "tcyih@fgcu.edu",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 12,
                "is_primary" => 1,
            ), array(
                "id" => 9,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "fturowetz@comcast.net",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 13,
                "is_primary" => 1,
            ), array(
                "id" => 10,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "j.mitchell.mitch@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 14,
                "is_primary" => 1,
            ), array(
                "id" => 11,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "dimond@sympatico.ca",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 15,
                "is_primary" => 1,
            ), array(
                "id" => 12,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "victor.cortner@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 16,
                "is_primary" => 1,
            ), array(
                "id" => 13,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "jillhiatt69@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 17,
                "is_primary" => 1,
            ), array(
                "id" => 14,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Conniewassmann@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 21,
                "is_primary" => 1,
            ), array(
                "id" => 15,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "lynnbuskard@me.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 19,
                "is_primary" => 1,
            ), array(
                "id" => 16,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "goldengeorgy@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 20,
                "is_primary" => 1,
            ), array(
                "id" => 17,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Johnb@leeinspectionservices.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 22,
                "is_primary" => 1,
            ), array(
                "id" => 18,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "lcmurphy55@yahoo.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 23,
                "is_primary" => 1,
            ), array(
                "id" => 19,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "collenedavis@yahoo.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 24,
                "is_primary" => 1,
            ), array(
                "id" => 20,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Brecon@sunrise.ch",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 25,
                "is_primary" => 1,
            ), array(
                "id" => 21,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "sharleen2@comcast.net",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 27,
                "is_primary" => 1,
            ), array(
                "id" => 22,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "alanhartstein@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 28,
                "is_primary" => 1,
            ), array(
                "id" => 23,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Johnangusblack@hotmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 29,
                "is_primary" => 1,
            ), array(
                "id" => 24,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "pattida588@aol.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 30,
                "is_primary" => 1,
            ), array(
                "id" => 25,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Belinda_coles@icloud.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 31,
                "is_primary" => 1,
            ), array(
                "id" => 26,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "brianne519@yahoo.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 32,
                "is_primary" => 1,
            ), array(
                "id" => 27,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "Leepaeifel54@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 33,
                "is_primary" => 1,
            ), array(
                "id" => 28,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "r.k.a@comcast.net",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 34,
                "is_primary" => 1,
            ), array(
                "id" => 29,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "email" => "lincolndilling@yahoo.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 35,
                "is_primary" => 1,
            )
        );
        foreach ($emails as $email) {
            \App\Emails::create($email);
        }

        // Seeds Customer Phones

        // 366,367,368,369 (Cell,Home,Work,Fax)
        $phones = array(
            array(
                "id" => 1,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 1,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,//home phone
                "number"=>"7739208693",
            ), array(
                "id" => 2,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 2,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367, //home phone
                "number"=>"8025225243",
            ), array(
                "id" => 3,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 5,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2396770702",
            ), array(
                "id" => 4,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 6,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"5712711071",
            ), array(
                "id" => 5,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 6,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>366, //cell phone
                "number"=>"5712170580",
            ), array(
                "id" => 6,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 7,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"7066197179",
            ), array(
                "id" => 7,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 8,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"7012190045",
            ), array(
                "id" => 8,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 9,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"7742440663",
            ), array(
                "id" => 9,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 10,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"9414054150",
            ), array(
                "id" => 10,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 11,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"6107336219",
            ), array(
                "id" => 11,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 12,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2393219968",
            ), array(
                "id" => 12,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 13,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"5087768700",
            ), array(
                "id" => 13,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 14,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"6475672649",
            ), array(
                "id" => 14,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 15,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2267901711",
            ), array(
                "id" => 15,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 16,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"9014818252",
            ), array(
                "id" => 16,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 17,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"6512100051",
            ), array(
                "id" => 17,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 18,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"7035681770",
            ), array(
                "id" => 18,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 19,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"3055826566",
            ), array(
                "id" => 19,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 20,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2394409514",
            ), array(
                "id" => 20,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 21,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"8457426563",
            ), array(
                "id" => 21,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 22,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2392469534",
            ), array(
                "id" => 22,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 23,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2399452116",
            ), array(
                "id" => 23,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 24,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2392399194",
            ), array(
                "id" => 24,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 25,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"9412761801",
            ), array(
                "id" => 25,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 27,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2395610873",

            ), array(
                "id" => 26,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 27,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>366,
                "number"=>"2393575417",
            ), array(
                "id" => 27,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 28,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"3055826566",
            ), array(
                "id" => 28,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 29,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367, //home phone
                "number"=>"2395588168",
            ), array(
                "id" => 29,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 29,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>366, //cell phone
                "number"=>"5196617102",
            ), array(
                "id" => 30,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 29,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368, //work phone
                "number"=>"5198500307",
            ), array(
                "id" => 31,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 30,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"5618019106",
            ), array(
                "id" => 32,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" =>31,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"8042397838",
            ), array(
                "id" => 33,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" =>32,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2392145118",
            ), array(
                "id" => 34,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 33,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2392409421",
            ), array(
                "id" => 35,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 34,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2395602811",
            ), array(
                "id" => 36,
                "client_id" => 1,
                "associate_type_id" => 1,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 35,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>367,
                "number"=>"2392339139",
            )
        );

        foreach ($phones as $phone) {
            \App\Phones::create($phone);
        }
    }
}
