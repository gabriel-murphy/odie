<?php

use Illuminate\Database\Seeder;

class DumpFeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dump_fees = array(
            array(
                'client_id' => '1',
                'name' => 'Shingle Dump Fees',
                'roof_type_id' => 1,
                'cost_type' => 'Per Square',
                'cost_per_dump' => '500',
                'squares_per_dump' => '40',

            ),
            array(
                'client_id' => '1',
                'name' => 'Tile Dump Fees',
                'roof_type_id' => '2',
                'cost_type' => 'Per Square',
                'cost_per_dump' => '40',
                'squares_per_dump' => '1',
            ),
            array(
                'client_id' => '1',
                'name' => 'Flat Dump Fees',
                'roof_type_id' => '4',
                'cost_type' => 'Per Square',
                'cost_per_dump' => '500',
                'squares_per_dump' => '15',
            ),
        );
        foreach ($dump_fees as $dump_fee) {
            DB::table('dump_fees')->insert($dump_fee);
        }
    }
}
