<?php

use Illuminate\Database\Seeder;

class ClientTeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into client_teams (name) values (?)', ['Office Staff']);
        DB::insert('insert into client_teams (name) values (?)', ['Accounting']);
        DB::insert('insert into client_teams (name) values (?)', ['Permitting']);
        DB::insert('insert into client_teams (name) values (?)', ['Crews']);
        DB::insert('insert into client_teams (name) values (?)', ['Estimators']);
        DB::insert('insert into client_teams (name) values (?)', ['Operations']);
        DB::insert('insert into client_teams (name) values (?)', ['Company Wide']);
        DB::insert('insert into client_teams (name) values (?)', ['Owners']);
    }
}
