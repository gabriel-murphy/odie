<?php

use Illuminate\Database\Seeder;

class AssociatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adjusters = array(
            array(
                "id" => 36,
                "first_name" => "Michael",
                "last_name" => "Miller",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 1,
                "carrier_relationship_id" => 3,     // Field Adjuster
            ),
            array(
                "id" => 37,
                "first_name" => "April",
                "last_name" => "Crawford",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,
                "carrier_relationship_id" => 2,
            ),
            array(
                "id" => 38,
                "first_name" => "Janelle",
                "insurance_carrier_id" => 3,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 39,
                "first_name" => "Niles",
                "last_name" => "Wood",
                "insurance_carrier_id" => 3,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 40,
                "first_name" => "Tobin",
                "last_name" => "Yager",
                "insurance_carrier_id" => 5,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 41,
                "first_name" => "Loretta",
                "last_name" => "James",
                "insurance_carrier_id" => 6,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 42,
                "first_name" => "Jacqueline",
                "last_name" => "Dimery",
                "insurance_carrier_id" => 11,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 43,
                "first_name" => "Kevin",
                "last_name" => "Wisniewski",
                "insurance_carrier_id" => 2,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 44,
                "first_name" => "Daniel",
                "last_name" => "Galdiano",
                "insurance_carrier_id" => 5,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 45,
                "first_name" => "Donald",
                "last_name" => "Rench",
                "insurance_carrier_id" => 5,
                "associate_type_id" => 2, // 2 means this is adjuster
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 46,
                "first_name" => "Ted",
                "last_name" => "Fiocati",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,                        // UPC
                "carrier_relationship_id" => 3,                     // Field Adjuster
            ),
            array(
                "id" => 47,
                "first_name" => "Jonathan",
                "last_name" => "Walker",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 5,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 48,
                "first_name" => "Daniel",
                "last_name" => "Dessum",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 4,
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 49,
                "first_name" => "Ketari",
                "last_name" => "Cole",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 6,            // Frontier
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 50,
                "first_name" => "Steve",
                "last_name" => "Winalis",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 51,
                "first_name" => "Dave",
                "last_name" => "Marrs",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 8,            // USAA
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 52,
                "first_name" => "Josephine",
                "last_name" => "Burdett",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 53,
                "first_name" => "Taylor",
                "last_name" => "Angell",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 54,
                "first_name" => "Robert",
                "last_name" => "Martin",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 5,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 55,
                "first_name" => "Gary",
                "last_name" => "Widich",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 9,            // Florida Penninsula
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 56,
                "first_name" => "Antoine",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,            // UPC
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 57,
                "first_name" => "Charlotte",
                "last_name" => "McGill-Colyer",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 5,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 58,
                "first_name" => "Jason",
                "last_name" => "Shelton",
                "company" => "EFI Global",
                "title" => "Senior Forensic Engineer",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 5,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 59,
                "first_name" => "Dave",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 13,            // Tower Hill
                "carrier_relationship_id" => 3,         // Field Adjuster
            ),
            array(
                "id" => 60,
                "first_name" => "Kiarra",
                "last_name" => "Rainer",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 10,            // Saint John's
                "carrier_relationship_id" => 2,          // Desk Adjuster
            ),
            array(
                "id" => 61,
                "first_name" => "Robert",
                "last_name" => "Campbell",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 10,            // Saint John's
                "carrier_relationship_id" => 3,          // Field Adjuster
            ),
            array(
                "id" => 62,
                "first_name" => "Mike",
                "last_name" => "Miller",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 1,            // Saint John's
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 63,
                "first_name" => "Marina",
                "last_name" => "Purvis",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 1,            // Saint John's
                "carrier_relationship_id" => 2,         // Desk Adjuster
            ),
            array(
                "id" => 64,
                "first_name" => "Thomas",
                "last_name" => "Prator",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,
                "carrier_relationship_id" => 3,          // Field Adjuster
            ),
            array(
                "id" => 65,
                "first_name" => "Steve",
                "last_name" => "Barbour",
                "associate_type_id" => 2, // 2 means this is adjuster
                "insurance_carrier_id" => 2,
                "carrier_relationship_id" => 3,          // Field Adjuster
            ),
        );

        $escalators = array(
            array(
                "id" => 66,
                "first_name" => "Omari",
                "associate_type_id" => 3, // 3 means this is esculator
                "middle_name" => "S.",
                "last_name" => "Ruddock",
                "company" => "Vargas Gonzales Hevia Baldwin",
                "client_relationship_id" => 1,     // Attorney
            ),
            array(
                "id" => 67,
                "first_name" => "Carlos",
                "last_name" => "Infante",
                "associate_type_id" => 3, // 3 means this is esculator
                "company" => "Infante Adjustment Bureau",
                "client_relationship_id" => 2,     // Public Adjuster
            ),
            array(
                "id" => 68,
                "first_name" => "Turd",
                "last_name" => "Furgeson",
                "associate_type_id" => 3, // 3 means this is esculator
                "company" => "GCM",
                "client_relationship_id" => 2,     // Public Adjuster
            ),
        );

        foreach ($adjusters as $adjuster) {

            //DB::table('contacts')->insert($adjuster);
            \App\Customer::create($adjuster);
        }

        foreach ($escalators as $escalator) {

            //DB::table('contacts')->insert($escalator);
            \App\Customer::create($escalator);
        }

        $emails = array(
            array(
                "id" => 30,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "acrawford@upcinsurance.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 37,
                "is_primary" => 1,
            ), array(
                "id" => 31,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "tkyager@comcast.net",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 40,
                "is_primary" => 1,
            ), array(
                "id" => 32,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "cat@asicorp.org",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 42,
                "is_primary" => 1,
            ), array(
                "id" => 33,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "claims.1500miles@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 43,
                "is_primary" => 1,
            ), array(
                "id" => 34,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "dgaldiano@alacritysolutions.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 44,
                "is_primary" => 1,
            ), array(
                "id" => 35,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "tfiocati@upcinsurance.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 46,
                "is_primary" => 1,
            ), array(
                "id" => 36,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "ddessum@heritagepci.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 48,
                "is_primary" => 1,
            ), array(
                "id" => 37,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "acmeadjusting@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 50,
                "is_primary" => 1,
            ),array(
                "id" => 38,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "marrsadj@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 51,
                "is_primary" => 1,
            ), array(
                "id" => 39,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "jburdett@upcinsurance.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 52,
                "is_primary" => 1,
            ), array(
                "id" => 40,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "rmartin@thig.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 54,
                "is_primary" => 1,
            ), array(
                "id" => 41,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "gary.widich@fpclaimservices.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 55,
                "is_primary" => 1,
            )
        , array(
                "id" => 42,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "cat@asicorp.org",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 57,
                "is_primary" => 1,
            ) , array(
                "id" => 43,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "jason.shelton@efiglobal.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 58,
                "is_primary" => 1,
            ) , array(
                "id" => 44,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "cat.exam144@seibels.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 60,
                "is_primary" => 1,
            ) , array(
                "id" => 45,
                "client_id" => 1,
                "associate_type_id" => 2,//2 indicates this is adjuster
                "email" => "contractorst@aol.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 64,
                "is_primary" => 1,
            ), array(
                "id" => 46,
                "client_id" => 1,
                "associate_type_id" => 3,//3 indicates this is esculator
                "email" => "jsbadjuster@gmail.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 66,
                "is_primary" => 1,
            ), array(
                "id" => 47,
                "client_id" => 1,
                "associate_type_id" => 3,//3 indicates this is esculator
                "email" => "oruddock@vargasgonzalez.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 67,
                "is_primary" => 1,
            ), array(
                "id" => 48,
                "client_id" => 1,
                "associate_type_id" => 3,//3 indicates this is adjuster
                "email" => "gabriel@gabrielmurphy.com",
                "emailable_type" => "App\\Customer",
                "emailable_id" => 68,
                "is_primary" => 1,
            )
        );

        foreach ($emails as $email) {
            \App\Emails::create($email);
        }

        // Seeds Customer Phones

        // 366,367,368,369 (Cell,Home,Work,Fax)
        $phones = array(
            array(
                "id" => 37,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 36,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,//work phone
                "number"=>"8444584300",
                "extension" => "3097632270",

            ), array(
                "id" => 38,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 36,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>369,//fax phone
                "number"=>"8442363646",

            ), array(
                "id" => 39,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 37,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"7278957737",
                "extension" => "5709",
            ), array(
                "id" => 40,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 38,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368, //work phone
                "number"=>"3366631837",
            ), array(
                "id" => 41,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 39,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"3364469876",
            ), array(
                "id" => 42,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 40,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"2074501894",
            ), array(
                "id" => 43,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 41,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"8006750145",
                "extension" => "1336",
            ), array(
                "id" => 44,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 42,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"8662745677",
                "extension" => "1329",
            ), array(
                "id" => 45,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 43,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"3095321323",
            ), array(
                "id" => 46,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customerb
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 44,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"8004110013",
                "extension" => "1006",
            ), array(
                "id" => 47,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 45,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"2392482003",
            ), array(
                "id" => 48,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 46,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"2397770290",
            ), array(
                "id" => 49,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 47,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"8662745677",
                "extension" => "2056",
            ), array(
                "id" => 50,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 47,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>369, //fax
                "number"=>"8662743299",
            ), array(
                "id" => 51,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 48,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"7273627200",
                "extension" => "7537",
            ), array(
                "id" => 52,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 49,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"8777445224",
                "extension" => "8150",
            ), array(
                "id" => 53,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 50,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"2399403200",
            ), array(
                "id" => 54,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 51,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"4699518976",
            ), array(
                "id" => 55,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 52,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"8008614370",
                "extension" => "5835",

            ), array(
                "id" => 56,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 52,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>369,
                "number"=>"8003805053",
            ), array(
                "id" => 57,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 53,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"7277736946",
            ), array(
                "id" => 58,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 54,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"3523331736",
            ), array(
                "id" => 59,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 57,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"8662745677",
                "extension" => "1156",

            ), array(
                "id" => 60,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 57,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>369,
                "number"=>"8668401905",
            ), array(
                "id" => 61,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 58,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"3212519091",
            ), array(
                "id" => 62,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 58,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>366, //cell
                "number"=>"8137120247",
            ),array(
                "id" => 63,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 59,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"6783162914",
            ), array(
                "id" => 64,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 63,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"7278957737",
                "extension" => "6292",
            ), array(
                "id" => 65,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 64,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"2398104047",
            ), array(
                "id" => 66,
                "client_id" => 1,
                "associate_type_id" => 2,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 65,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"9417561533",
            ), array(
                "id" => 67,
                "client_id" => 1,
                "associate_type_id" => 3,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 66,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"3056312528",
            ), array(
                "id" => 68,
                "client_id" => 1,
                "associate_type_id" => 3,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 66,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>369,
                "number"=>"3056312741",
            ), array(
                "id" => 69,
                "client_id" => 1,
                "associate_type_id" => 3,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 67,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"5619078584",
            ), array(
                "id" => 70,
                "client_id" => 1,
                "associate_type_id" => 3,//1 indicates this is customer
                "phoneable_type" => "App\\Customer",
                "phoneable_id" => 68,
                "countries_id"=>348, // USA constant table id
                "phone_types_id"=>368,
                "number"=>"2399945596",
            )
        );

        foreach ($phones as $phone) {
            \App\Phones::create($phone);
        }
    }
}
