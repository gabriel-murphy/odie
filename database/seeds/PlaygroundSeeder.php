<?php

use Illuminate\Database\Seeder;

class PlaygroundSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

        /* Playground Seeders Only - to have this seeded, run the following command:
        php artisan db:seed --class=PlaygroundSeeder */

    public function run()
    {
        ## Likely to be used in the future
        $this->call(RoofWarrantiesTableSeeder::class);
        $this->call(CommissionStructureTableSeeder::class);
        $this->call(ProjectFeeTypesTableSeeder::class);
        $this->call(MilestonesTableSeeder::class);
        $this->call(ProfitMarginTableSeeder::class);
        $this->call(ClientTeamsTableSeeder::class);
        $this->call(CommissionStructureTableSeeder::class);
        $this->call(DefaultMaterialsTableSeeder::class);
        $this->call(CredentialsTableSeeder::class);
        $this->call(RoofProductColorsTableSeeder::class);                              ## colors are tied to profile or finish or line

        ## Likely garbage and old
        //$this->call(RoofManufacturerRoofTypeTableSeeder::class);
        //$this->call(RoofManufacturersSeriesTypesTableSeeder::class);
        //$this->call(RoofManufacturersProductsTableSeeder::class);           ## the marketing name of the line (Good, Better, Best)
        //$this->call(RoofManufacturersFinishTypesTableSeeder::class);        ## the roofing materials within the lines above
        //$this->call(RoofManufacturersProfileTypesTableSeeder::class);       ## only metal roofs really go this deep for pricing purposes
        //$this->call(RoofManufacturersMaterialTypesTableSeeder::class);      ## this material types can be applicable to lines

    }
}
