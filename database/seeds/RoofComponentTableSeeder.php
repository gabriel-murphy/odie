<?php

use Illuminate\Database\Seeder;

class RoofComponentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* Roof components in which quantities are derived from square feet or lineal feet of measurements of the roof, used by
    the Platform to auto-generate cost estimates for customer proposals.  Note this is NOT client specific but global across
    the Platform (because the math behind how to calcuate the cost is the same from roof-to-roof) */

    public function run()
    {
	    DB::insert('insert into roof_components (name, link_estimating, roof_measurements_column, divisor) values (?, ?, ?, ?)', ['squares', 'yes', 'square_feet', '100']);
	    DB::insert('insert into roof_components (name, link_estimating, roof_measurements_column) values (?, ?, ?)', ['valleys', 'yes', 'valleys']);
	    DB::insert('insert into roof_components (name, link_estimating, roof_measurements_column) values (?, ?, ?)', ['drip edge', 'yes', 'drip_edge']);
	    DB::insert('insert into roof_components (name, link_estimating, roof_measurements_column) values (?, ?, ?)', ['eaves', 'yes', 'eaves']);
	    DB::insert('insert into roof_components (name, link_estimating, roof_measurements_column) values (?, ?, ?)', ['caps', 'yes', 'ridges_hips_sum']);
    }
}
