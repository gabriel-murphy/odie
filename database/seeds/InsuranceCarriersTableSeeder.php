<?php

use Illuminate\Database\Seeder;

class InsuranceCarriersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insurance_carriers = array(
            array(
                "id" => 1,                                  // IDs shown are needed to ensure proper seeding of the MaximusClaimsTableSeeder
                "name" => 'State Farm Insurance',
                "email" => 'statefarmfireclaims@statefarm.com',
                "website" => 'https://www.statefarm.com',
                "phone" => '8007828332',
                "claims_phone" => '8007325246',
            ),
            array(
                "id" => 2,
                "name" => 'United Property & Casualty Insurance',
                "email" => 'claims@upcinsurance.com',
                "website" => 'http://www.upcinsurance.com',
                "phone" => '8002958016',
                "claims_phone" => '8882563378',
            ),
            array(
                "id" => 3,
                "name" => 'Old Dominion Insurance Company',
                "email" => 'claims@msagroup.com',
                "website" => 'https://www.msagroup.com/',
                "phone" => '8002585310',
                "claims_phone" => '8774252467',
            ),
            array(
                "id" => 4,
                "name" => "Heritage Property & Casualty Insurance Company",
                "short_name" => 'Heritage',
                "email" => 'assignmentofbenefits@heritagepci.com',
                "website" => 'https://www.heritagepci.com',
                "phone" => '8556209978',
                "claims_phone" => '8554157120',
            ),
            array(
                "id" => 5,
                "name" => 'Tower Hill Insurance ',
                "email" => 'claims@thig.com',
                "website" => 'https://www.thig.com/',
                "phone" => '8003423407',
                "claims_phone" => '8003423407',
            ),
            array(
                "id" => 6,
                "name" => 'Frontline Insurance',
                "website" => 'https://stjohnsinsurance.com',
                "claims_url" => 'https://claims.frontlineinsurance.com/#/documents/claim-select',
                "phone" => '8777445224',
                "claims_phone" => '8666730623',
            ),
            array(
                "id" => 7,
                "name" => 'American Integrity',
                "website" => 'https://stjohnsinsurance.com',
                "phone" => '8777445224',
                "claims_phone" => '8666730623',
            ),
            array(
                "id" => 8,
                "name" => 'USAA',
                "website" => 'https://www.usaa.com/',
                "email" => 'claims@usaa.com',
                "phone" => '8005318722',
            ),
            array(
                "id" => 9,
                "name" => 'Florida Peninsula Insurance Company ',
                "email" => 'csclaims@floridapeninsula.com',
                "website" => 'http://www.floridapeninsula.com',
                "phone" => '8772292244',
                "claims_phone" => '8665499672',
            ),
            array(
                "id" => 10,
                "name" => 'Saint John’s Insurance',
                "email" => 'dispatch@seibels.com',
                "website" => 'https://stjohnsinsurance.com',
                "phone" => '8007482030',
                "claims_phone" => '8777482059',
            ),
            array(
                "id" => 11,
                "name" => 'ASI / Progressive Insurance Company ',
                "email" => 'claims@asicorp.org',
                "website" => 'http://www.progressive.com/',
                "phone" => '8007764737',
                "claims_phone" => '8007764737',
            ),
            array(
                "id" => 12,
                "name" => 'The Main Street America Group',
                "email" => 'claimsmail@msagroup.com',
                "website" => 'https://www.msagroup.com/',
                "phone" => '8002585310',
                "claims_phone" => '8774252467',
            ),
            array(
                "id" => 13,
                "name" => 'Asi/Ark Royal Insurance Co',
                "email" => 'cat@asicorp.org',
                "website" => 'https://www.asicorp.org/',
                "phone" => '8662745677',
                "claims_phone" => '7274561673',
            ),
            array(
                "id" => 14,
                "name" => 'Security First Insurance',
                "email" => 'catclaims@securityfirstflorida.com',
                "website" => 'https://www.securityfirstflorida.com',
                "star_rating" => 1.8,
                "rating_url" => 'https://www.google.com/search?newwindow=1&rlz=1C1CHBF_enUS867US867&sxsrf=ALeKk00UrQ5ztziXYpoRVj4nedsCt6fZBw:1596227308875&q=Security+First+Insurance+Company&stick=H4sIAAAAAAAAAONgecR4gJFb4OWPe8JS2xgnrTl5jXE9IxdXcEZ-uWteSWZJpZAPFxuUpcDFL8Wtn65vWJKeYZFcUqDBIMXLhSwgpaDExbubu1RQNKxy03ktIU5fhdkbVW8_6xCc3aT84-bpv8-Vgo3cd12ado7NUZABCCp8Qh2kNLWEuNg9i33ykxNzBLfUbs36fmyDnZYwF0dIYkV-Xn5uJVgpA8MHeyVOTiB94HLoA3sthqZ9Kw6xsXAwCjDwLGJVCE5NLi0COlLBLbOouETBM6-4tCgxLzlVwTk_tyAxrxIAAv6c5ekAAAA&sa=X&ved=2ahUKEwi6neeNqvjqAhVSgK0KHbBdALMQ6RMwFHoECA0QBA&biw=2048&bih=962#',
                "phone" => '',
                "claims_phone" => '8775814862',
            ),
            array(
                "id" => 15,
                "name" => 'Gulfstream Property & Casualty Insurance Company',
            ),
        );
        foreach ($insurance_carriers as $insurance_carrier) {
            \App\InsuranceCarrier::create($insurance_carrier);
        }
    }
}
