<?php

use App\Constant;
use Illuminate\Database\Seeder;

class MaximusClaimsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $helper = app('constants');

        $constants = Constant::group('claim_statuses')->get();
        $send_to_carrier = $constants->where('key', 1)->first()->id;

        $searches = [
            'aob' => [
                'type_id' => $helper->getAttachmentTypeId('aob'),
                'keywords' => ['aob', ucfirst('aob'), strtoupper('aob')]
            ],
            'xactimate' => [
                'type_id' => $helper->getAttachmentTypeId('xactimate'),
                'keywords' => ['xactimate', ucfirst('xactimate'), strtoupper('xactimate'), 'item', 'Item']
            ],
            'ariel' => [
                'type_id' => $helper->getAttachmentTypeId('ariel'),
                'keywords' => ['eagle', ucfirst('eagle'), strtoupper('eagle')]
            ],
            'itel' => [
                'type_id' => $helper->getAttachmentTypeId('itel'),
                'keywords' => ['itel', ucfirst('itel'), strtoupper('itel')]
            ],
        ];

        $maximus_claims = array(
            array(
                "id" => 1,
                "user_id" => 2,                                     // Lindsey sent first AoB
                "estimator_id" => 10,
                "customer_id" => 1,                                 // Morales
                "aob_signed_on" => "2020-05-18",
                "insurance_carrier_id" => 1,
                "desk_adjuster_id" => 26,
                "field_adjuster_id" => 1,
                "policy_number" => '59BVJ859-5',
                "claim_number" => '5906V849N',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '103465.74',
                "deductible_amount" => '5274.00',
                "property_id" => 2,
                "description" => '
TEMP REPAIR COMPLETED BY ANGEL 6/16/20

** RR AOB***

Eray doing this job %50/%50 with Daniel
                ',
                "temp_repair_needed" => 1,
                "adjuster_meeting_at" => '2020-05-28 23:00:00',
                "adjuster_meeting_notes" => '
                Adjuster when up on the roof and did the inspection See Attached Pics. Everything went smooth. Waiting
                on the desk adjusters response. He did not have an access to inside the house to check the interior
                water damage areas but I will email to field adjuster the interior damaged pictures.
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 2,
                "user_id" => 2,
                "estimator_id" => 5,                    // Ralph
                    "customer_id" => 2,                 // Donna & Patrick Householder
                "aob_signed_on" => "2020-06-02",
                "insurance_carrier_id" => 2,
                "desk_adjuster_id" => 2,
                "policy_number" => 'UHV31741680701',
                "claim_number" => '2017FL039867',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '127101.16',
                "deductible_amount" => '12100.00',
                "property_id" => 3,
                "description" => '
Desk Adjuster is April Crawford 727 895-7737 Ext: 5709 acrawford@upcinsurance.com

PATRICK & DONNA HOUSEHOLDER - 1705 CORAL WAY CAPE CORAL FL 33917
UPC INSURANCE COMPANY - CLAIM #2017FL039867

NEEDS TEMPORARY REPAIR PERFORMED ASAP

NO ADJUSTER INFO, OR MEETING SCHEDULED  AT THIS POINT
Over 30 cracked/broken tiles
Loose tiles in field, and on hip & ridges
Sliding tiles throughout the roof
Lifted tiles over 2" throughout the roof
Broken mortar on 50% of hip & ridges
Pioneer Hacienda tile no longer manufactured
Previous repairs made on roof
Active leak over family room
Damage to interior ceiling and walls in family room

DA - April Crawford
                ',
                "minimum_damaged_units" => 30,
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 3,  // Perri questions - policy holders should technically be the company listed on AoB?
                "user_id" => 1,                                     // Gabriel Murphy first AoB Submission
                "estimator_id" => 10,                          // Eray
                "customer_id" => 3,                                 // Rossmans
                "aob_signed_on" => "2020-06-01",
                "insurance_carrier_id" => 3,
                "desk_adjuster_id" => 3,
                "field_adjuster_id" => 4,
                "policy_number" => 'BGP7884H',
                "claim_number" => 'BPG7884H01',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '200517.66',
                "property_id" => 4,
                "description" => '
min 30 loose ridge cap
lifted and lose tiles throughout roof
more than 25 broken tiles

INSURED - REALTY SERVICES OF CAPE CORAL LLC

TWO OWNER:

MICHELLE ROSSMAN: MICHROSS@YAHOO.COM - 239-841-1829

DENNIS ROSSMAN: DENNIS@ROSSMANREALTY.COM - 239-470-25-17


2017 TO 2018 INSURANCE COMPANY IS OLD DOMINION INSURANCE COMPANY

2020 TO 2021 INSURANCE COMPANY IS THE MAIN STREET AMERICA GROUP

SHE SAID THEY BOTH THE SAME COMPANY WITH SAME POLICE NUMBER: BPG7884H

CLAIM NUMBER: BPG7884H01

HER DEDUCTIBLE IS %5


marca@rossmanrealty.com

(239) 443-1304
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 4,
                "user_id" => 2,                                     // Lindsey sent
                "estimator_id" => 10,
                "customer_id" => 4,                                 // Crawford
                "aob_signed_on" => "2020-06-04",
                "insurance_carrier_id" => 2,
                "policy_number" => '1501-1203-1991',
                "claim_number" => 'FL20-0118481-E317',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '93051.83',
                "deductible_amount" => '5274.00',
                "property_id" => 5,
                "description" => '
ANGEL APPLIED PEEL AND STICK TARP LEAK AREAS.
Over a 100 lose Tiles
Over a 25 broken Tiles

Universal Property & Casualty Insurance Company

Policy# 1501-1203-1991

Claim# FL20-0118481-E317

Mortgage Com: Wells Fargo
                ',
                "minimum_damaged_units" => 100,
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,                    // Cancelled
            ),
            array(
                "id" => 5,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 5,
                "customer_id" => 5,                                 // Bruce
                "aob_signed_on" => "2020-06-04",
                "insurance_carrier_id" => 2,
                "policy_number" => 'UHF 119011800309',
                "claim_number" => '20FL00037041',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '105823.55',
                "deductible_amount" => '4800.00',
                "property_id" => 6,
                "description" => '
DENISE BRUCE - 2664 CLAIRFONT CT CAPE CORAL FL 33990
UPC INSURANCE COMPANY - CLAIM# 20FL00037041 / POLICY #UCF 119011800309

NO ADJUSTER INFO, OR MEETING SCHEDULED  AT THIS POINT
Over 30 cracked/broken tiles
Loose tiles in field, and on hip & ridges
Sliding tiles throughout the roof
Lifted tiles over 2" throughout the roof
Broken mortar on 50% of hip & ridges
Monier Lifetile tile no longer manufactured
                ',
                "minimum_damaged_units" => 30,
                "status_id" => $send_to_carrier,                                 // Decided - Denied
            ),
            array(
                "id" => 6,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 11,                               // Clint Nix
                "customer_id" => 6,                                 // Maclean
                "aob_signed_on" => "2020-06-09",
                "insurance_carrier_id" => 11,                       // Progressive
                "policy_number" => 'FLP231946',
                "claim_number" => '76632620103',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '105823.55',
                "deductible_amount" => '4800.00',
                "property_id" => 7,
                "description" => '
Engineer meeting 7/7/2020, was 1/2 hr late, arrived about 4:30
He spent about an hour on the backside of roof lifting tiles and taking oictures
no comment from him, he was very guarded,
Brett Grantham Donan Engineering 813-321-9800 bgrantham@donan.com

Daniel did Xactimate ~ $200 COMMISSION

temp repairs completed by angel 6/5/20

CCPA = MACLEAN CRAIG M & ABBY D
One Story, Tile Roof, Leak over garage.
amaclean@igc.org

Lifted tiles on entire roof, cement paddies over mod bit. There are about 50 broken tiles, mortar is all loose on ridge and hips. active leak in garage
                ',
                "temp_repair_needed" => 1,
                "minimum_damaged_units" => 50,
                "adjuster_meeting_at" => '2020-07-07 16:30',
                "adjuster_meeting_notes" => '
Engineer meeting 7/7/2020, was 1/2 hr late, arrived about 4:30
He spent about an hour on the backside of roof lifting tiles and taking oictures
no comment from him, he was very guarded,
Brett Grantham Donan Engineering 813-321-9800 bgrantham@donan.com
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 7,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 5,                                // Ralph
                "customer_id" => 7,                                 // Maclean
                "aob_signed_on" => "2020-06-09",
                "insurance_carrier_id" => 4,
                "policy_number" => 'HOH 271822 ',
                "claim_number" => 'HP 207715',
                "date_of_loss" => '2020-05-18',
                "xactimate_amount" => '89613.19',
                "deductible_amount" => '5000.00',
                "property_id" => 8,
                "description" => '
MILLARD TROY ALLEN - 14965 TOSCANA WAY NAPLES FL 34120
HERITAGE INSURANCE COMPANY POLICY# HOH 271822 / CLAIM# HP 207715
DEDUCTIBLE $5,000

TEMP REPAIR IS NEEDED - LEAKING IN FRONT VALLEY, CURRENTLY HAS A TARP ON IT, BUT IT IS NOT WORKING.
INSURANCE COMPANY INDICATED  THAT THE LEAK IS NOT CAUSE BY THE ROOF, AND THAT THERE ARE NO ISSUES WITH THE ROOF! HO WAS PAID FOR A SMALL REPAIR ONLY

NO ADJUSTER INFO, OR MEETING SCHEDULED  AT THIS POINT
Over 50 cracked/broken tiles
Loose tiles in field, and on hip & ridges
Sliding tiles throughout the roof
Lifted tiles over 2" throughout the roof
Broken mortar on 50% of hip & ridges
Monier Lifetile Capri tiles no longer manufacturered
Active leak over garage
Damage to interior ceiling and walls in garage


TEMP REPAIRS COMPLETED 6/11/20
ANGEL DID REPAIR.
                ',
                "minimum_damaged_units" => 25,
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,                    // Decision - Denied on 7/21
            ),
            array(
                "id" => 8,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 10,                               // Eray
                "customer_id" => 8,                                 // Kringle
                "aob_signed_on" => "2020-06-12",
                "insurance_carrier_id" => 5,
                "field_adjuster_id" => 5,
                "policy_number" => 'P000977857',
                "claim_number" => '3300341576',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '190597.16',
                "property_id" => 9,
                "description" => '
TEMP REPAIR COMPLETED BY RAMON ON 6/13
PHOTOS UPLOADED..

Eray is working this deal for Daniel. Split commission 50%/50%

BROKEN TILES MORE THAN 25

LIFTED RIDGE CAPS MORE THAN 25

WIND LIFT ON THREE SIDE OF THE ROOF
                ',
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 9,  // Perri Questions
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 10,                               // Eray
                "customer_id" => 9,                                 // Brown
                "aob_signed_on" => "2020-06-12",
                "insurance_carrier_id" => 2,
                "desk_adjuster_id" => 17,
                "field_adjuster_id" => 8,
                "policy_number" => 'UHF 12513760209',
                "claim_number" => '20FL00038905',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '414634.43',
                "property_id" => 10,
                "description" => '
                                                                                       *****ROMAN ROOFING AOB*****

Tile Re-Roof Insurance claim

The roof has 90% of the tile loose and i have fixed 4 leaks
                ',
                "minimum_damaged_units" => 30,
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 10,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 11,                               // Clint
                "customer_id" => 10,                                // Bockhorst
                "aob_signed_on" => "2020-06-19",
                "insurance_carrier_id" => 10,                       // Saint John's
                "policy_number" => 'SJ30318415',
                "claim_number" => 'ST17230129',
                "deductible_amount" => '6778.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '187280.68',
                "property_id" => 11,
                "description" => '
*Daniel Stamps is doing Xactimate. $200

MCPA = BOCKHORST, WILLIAM B
referred by Matt Whalen @ no walk roof cleaning owe him $1000
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 11,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 5,                                // Ralph
                "customer_id" => 11,                                // Tully
                "insurance_carrier_id" => 6,                        // Frontline
                "desk_adjuster_id" => 6,
                "policy_number" => '4085364734',
                "claim_number" => '01000037623',
                "deductible_amount" => '1000.00',
                "date_of_loss" => '2020-06-06',
                "aob_signed_on" => '2020-06-16',
                "xactimate_amount" => '229384.05',
                "property_id" => 12,
                "description" => '
ANGEL DID TEMP REPAIR 6/17
KEVIN & SHARON TULLY - FRONTLINE INSURANCE - CLAIM# 01000031623 / POLICY# 4085364734

TEMPORARY REPAIR IS NEEDED

DEDUCTIBLE - $1,000 FOR OTHER PERILS / $16,000 for Hurricane
NO ADJUSTER INFO, OR MEETING SCHEDULED  AT THIS POINT
Over 60 cracked/broken tiles
Loose tiles in field, and on hip & ridges
Sliding tiles throughout the roof
Lifted tiles over 2" throughout the roof
Broken mortar on 50% of hip & ridges
LifeTile - Ceetile  no longer manufactured
Active leak over great room ceiling, between kitchen and living room
Damage to interior ceiling
                ',
                "minimum_damaged_units" => 60,
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 12,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 10,                               // Eray
                "customer_id" => 12,                                // Yih
                "aob_signed_on" => "2020-06-29",
                "insurance_carrier_id" => 8,
                "desk_adjuster_id" => 10,
                "field_adjuster_id" => 9,
                "policy_number" => 'AGH0017901',
                "claim_number" => 'CHO97652',
                "deductible_amount" => '16000.00',
                "date_of_loss" => '2020-06-06',
                "xactimate_amount" => '252439.67',
                "property_id" => 13,
                "description" => '
More than 25 broken tiles

More than 25 lose ridge caps

Wind lift damage

DANIEL DID XACTIMATE. $200 COMMISSION
                ',
                "minimum_damaged_units" => 25,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 13,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 25,                               // Gomez
                "customer_id" => 13,                                // Turowetz
                "aob_signed_on" => "2020-06-14",
                "insurance_carrier_id" => 11,                       // ASI / Progressive
                "desk_adjuster_id" => 12,
                "policy_number" => 'FSA98284',
                "claim_number" => '756050-201003',
                "deductible_amount" => '7580.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '141729.33',
                "property_id" => 14,
                "description" => '
DANIEL DID XACTIMATE ~ $200 COMMISSION

(15) Broken tiles through out the roof
Substantial wind uplift through out the roof
(41) Loose hip & Ridge
Initial Adjuster and engineer inspections already performed
                ',
                "minimum_damaged_units" => 15,
                "status_id" => $send_to_carrier,
                "adjuster_meeting_notes" => '
    Already took place in May unsure on the name of Field adjuster. However she did mention she saw all the markings I made on the roof detailing the wind uplift, broken tiles and loose caps
                    ',
            ),
            array(
                "id" => 14,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 25,                               // Gomez
                "customer_id" => 14,                                // Mitchell
                "aob_signed_on" => "2020-07-13",
                "insurance_carrier_id" => 4,
                "desk_adjuster_id" => 13,
                "policy_number" => 'HOH213270',
                "claim_number" => 'HP207625',
                "deductible_amount" => '10858.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '101557.78',
                "property_id" => 15,
                "description" => '
DANIEL DID XACTIMATE ~ $200 COMMISSION

Mitchell Roof Findings:

Broken Tiles - 15 Previously Repaired - Roman Roofing
Loose Hip & Ridge Caps - 50 Previously Repaired- Roman Roofing
Wind Uplift upper and lower roof sections - New Discovery
                ',
                "minimum_damaged_units" => 15,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 15,
                "user_id" => 1,                                     // Gabriel submitted form
                "estimator_id" => 26,                               // Critter
                "customer_id" => 15,                                // Diamond
                "aob_signed_on" => "2020-06-22",
                "insurance_carrier_id" => 6,                        // Frontline (no email)
                "desk_adjuster_id" => 14,
                "policy_number" => 'FPH3-163604',
                "claim_number" => '01-31795',
                "deductible_amount" => '6000.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '181990.05',
                "property_id" => 16,
                "description" => '
Daniel did Xactimate. $200 Commission

previous customer with hurricane damage

Roman Roofing AOB
                ',
                "status_id" => $send_to_carrier,
                "adjuster_meeting_at" => '2020-06-19 08:00:00',
                "adjuster_meeting_notes" => '
Adjuster said that the wind was directional and also said that it is definitely hurricane damage.
                ',
            ),
            array(
                "id" => 16,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 5,                                // Ralph
                "customer_id" => 16,                                // Cortner
                "aob_signed_on" => "2020-07-01",
                "insurance_carrier_id" => 2,                        // UPC
                "desk_adjuster_id" => 15,
                "policy_number" => 'UHV 2934833 07',
                "claim_number" => '20FL00042196',
                "deductible_amount" => '8760.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '181990.05',
                "property_id" => 17,
                "description" => '
VICTOR & SUSAN CORTNER - UPC INSURANCE - CLAIM# 20FL00042196 / POLICY# UHV 2934833 07

DEDUCTIBLE - $8,760.00
MET WITH FIELD ADJUSTER ALREADY
Over 50  cracked/broken tiles
Loose tiles in field, and on hip & ridges
Sliding tiles throughout the roof
Lifted tiles over 2" throughout the roof
Broken mortar on 50% of hip & ridges
Pioneer tile no longer manufactured
Damage to interior ceiling
                ',
                "minimum_damaged_units" => 50,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 17,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 10,                               // Eray
                "customer_id" => 17,                                // Hiatt
                "claim_type_id" => 1,                               // For Insurance Proceeds and NOT an AoB
                "aob_signed_on" => "2020-06-18",
                "insurance_carrier_id" => 2,                        // UPC
                "desk_adjuster_id" => 28,
                "field_adjuster_id" => 29,
                "policy_number" => 'UHV 28622980701',
                "claim_number" => '20FL00039653',
                "deductible_amount" => '8760.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '43766.04',
                "property_id" => 18,
                "minimum_damaged_units" => 50,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 18,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 5,                                // Ralph
                "customer_id" => 18,                                // Laatsch
                "aob_signed_on" => "2020-07-09",
                "insurance_carrier_id" => 2,                        // UPC
                "desk_adjuster_id" => 15,
                "policy_number" => '00305 29 43 92A',
                "claim_number" => '305 2943-017',
                "deductible_amount" => '10920.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '200518.91',
                "property_id" => 19,
                "description" => '
NO ADJUSTER INFO, OR MEETING SCHEDULED  AT THIS POINT
Over 40 cracked/broken tiles
Loose tiles in field, and on hip & ridges
Sliding tiles throughout the roof
Lifted tiles over 2" throughout the roof
Broken mortar on 50% of hip & ridges
Pioneer tile no longer manufactured

Email below is from homeowner on July 23rd, 2020

Hello Ralph,

As discussed, I\'ve attached (2) documents from USAA.
    The first document dated 7/20/2020 states that their Adjuster found no storm damage for this date of loss.
    The second document dated 7/8/2020  outlines FL homeowners policy rights. That document states that I have the right in FL to request mediation, but such request must be made within 21 days of receipt of the letter.
    I\'m assuming that we\'ll want to request mediation, in which case we have until 7/29 which doesn\'t give us much time.  Also, the letter states that if we\'re going to have an attorney present, that we notify the mediator at least (14) days in advance.  Both letters are attached so you can review in full.

    Pls. review and let me know next steps.  If going mediation, let me know if you want me to make the request, or if you\'ll do that.  Again, pls. note that we have a looming deadline in which to make that request.

Thanks so much and pls. don\'t hesitate to contact me if you have any questions or concerns.

    Best regards,

Jerry Laatsch
jlaatsch@gmail.com
c. 703.568.1770

After further reading, the USAA letter states that I can request in writing from USAA a formal confirmation as to my claim status, i.e. covered in full, partially covered, or denied.  There is another timeline, in that I have to make this request within (30) days of submitting a complete proof-of-loss statement to USAA.

    So two questions:
Have we submitted a proof-of-loss statement to USAA?  I\'ve submitted a claim, but I get the sense a proof-of-loss statement is a separate document.  Seeing that they just came back on 7/20 informing us that no damage was found, I would think the next step is that we\'d want to formally refute their findings by submitting our own documentation and stating your findings regarding the damage you identified.
If we do need to submit a proof-of-loss statement, I would think that we\'d then want to also immediately request a written confirmation of their claim denial so that we can put a clock on USAA foot dragging. I doubt a mediator will schedule a meeting until the parties have at least attempted to reconcile and the claim is formally denied.
I noticed in their letter that they were very careful to just state that they found no storm damage -- they make no mention that the claim is denied.

Presuming we\'re going to mediation, I would think that we\'d want to document the damage, submit the documentation and findings to USAA, and then request a formal written disposition on the claim.  At the same time, per my earlier email, we should request remediation before that deadline expires on 7/29  (we should file earlier to be safe).  I would expect that the assigned mediator will first want to see that we made every effort with USAA to settle the claim, and then get their formal denial, before they would schedule a meeting.

However, you folks know these waters better than I, so I\'ll wait for your guidance.

    Best regards,

                                                                     Jerry
                ',
                "minimum_damaged_units" => 50,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 19,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 25,                          // Gomez
                "customer_id" => 19,                                // Buskard
                "aob_signed_on" => "2020-07-14",
                "insurance_carrier_id" => 2,                        // UPC
                "desk_adjuster_id" => 17,
                "field_adjuster_id" => 18,
                "policy_number" => 'UHV 4670763 00',
                "claim_number" => '20FL00034798',
                "deductible_amount" => '10000.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '202897.68',
                "property_id" => 20,
                "description" => '
DANIEL DID XACTIMATE ~ $200 COMMISSION

(37) Broken Tiles throughout the roof
Wind uplift throughout the roof.
Field Adjuster already inspected the roof on 05/26
                ',
                "minimum_damaged_units" => 37,
                "adjuster_meeting_at" => '2020-05-26',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 20,  // NEEDS AUDIT
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 26,                          // Scott Morgan (first AoB)
                "customer_id" => 20,                                // Georges
                "claim_type_id" => 1,                               // Not an AoB
                "aob_signed_on" => NULL,
                "insurance_carrier_id" => 2,                        // UPC
                "policy_number" => 'UHV 4670763 00',
                "claim_number" => '20FL00034798',
                "deductible_amount" => '10000.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '243780.53',
                "property_id" => 21,
                "description" => '
DANIEL DID XACTIMATE ~ $100 COMMISSION

Scott\'s job %100 Eray\'s is ONLY helping him to get it wrapped up

RR Insurance claim Irma
Wind damage and missing shingles and lose ridge caps.
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 21,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 10,                          // Eray
                "customer_id" => 21,                                // Wasserman
                "aob_signed_on" => "2020-07-14",                  // Not an AoB
                "insurance_carrier_id" => 15,                       // Gulfstream Property
                "policy_number" => 'UHV 4670763 00',
                "claim_number" => '20FL00034798',
                "deductible_amount" => '5270.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '42247.75',
                "property_id" => 22,
                "description" => '
Did and settle for retail roofing price.

34 SQ GAF HDZ TIMBERLINE WEATHERED WOOD COLOR
WHITE DRIP EDGE
6/12 PITCH
DRYER VENT ABOVE ROOF ON THE LEFT SIDE OF THE GARAGE AREA FACING THE HOUSE
REINSTALL GUTTERS
DISPOSE SATT\'S


Daniel did Xactimate. $1,158/sq Still working out shingle commission w/ Norm


Hurricane Irma  Shingle Reroof Claim

Wind lift damage and missing taps and damaged shingle taps

Insurance PA Agreement
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 22,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 8,                                // Frank
                "customer_id" => 22,                                // Brooks
                "aob_signed_on" => "2020-06-18",
                "insurance_carrier_id" => 5,                        // Tower Hill
                "policy_number" => 'E004907089',
                "claim_number" => '3300324995',
                "desk_adjuster_id" => 19,
                "deductible_amount" => '5270.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '68565.98',
                "property_id" => 23,
                "description" => '
REFERRED BY ERIC HENKE $1000

100+ Cracked Tiles due to wind damage
10 Brokedn Tiles
8 Sliding tiles
Multiple areas of lift
Dented Off Ridge Vent

Johnb@leeinspectionservices.com

Leeinspector@hotmail.com
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 23,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 26,                          // Critter
                "customer_id" => 23,                                // Murphy
                "aob_signed_on" => "2020-06-22",
                "insurance_carrier_id" => 10,                       // Saint John's
                "desk_adjuster_id" => 25,
                "desk_adjuster_id" => 26,
                "policy_number" => 'SJ30311197',
                "claim_number" => 'ST17230159',
                "date_of_loss" => '2017-09-10',
                "deductible_amount" => '70500.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '68565.98',
                "property_id" => 24,
                "description" => '
Daniel did Xactimate. $200 commission.


You did repairs in the past. Has a leak in a new area she would like you to look at.

Roman Roofing AOB
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 24,  // NO EXACTIMATE - NEVER SENT!!! & CHECK ADJUSTERS TO ENSURE CORRECT!
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 8,                                // Frank
                "customer_id" => 24,                                // Davis
                "aob_signed_on" => "2020-06-22",
                "insurance_carrier_id" => 7,                        // American Integrity
                "policy_number" => 'AGD340226-03',
                "claim_number" => 'CDP-00092436',
                "desk_adjuster_id" => 9,
                "field_adjuster_id" => NULL,
                "date_of_loss" => '2017-09-10',
                "property_id" => 25,
                "description" => '
100+ cracked or chipped tiles directly related to wind damage
Multiple areas of lift over 2"
Multiple areas of sliding tiles
Multiple areas of loose cap tiles
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 25,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 11,                               // Clint Nix
                "customer_id" => 25,                                // Brenscheidt
                "aob_signed_on" => "2020-06-19",
                "insurance_carrier_id" => 9,                        // Florida Penninsula
                "desk_adjuster_id" => 20,
                "field_adjuster_id" => 20,
                "policy_number" => 'FPH 4149794 02',
                "claim_number" => '214625',
                "deductible_amount" => '26340.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '219737.24',
                "property_id" => 26,
                "description" => '
ANGEL DID TEMP 6/20

Please see info for access, customer is overseas. Neighbor has key. AOB Signed.

Daniel is doing Xactimate ~ $200 COMMISSION

LEEPA = BRENSCHEIDT MARC
tile repair
leaking into his dining room
single story
Lifted tiles, slipped tiles. broken tiles, slipped tiles, pulled screws, active leak, and previous leak after Hurricane Irma

skype number in Switzerland (6 hours behind) 941-621-3991

The neighbor at 1100 Romano Key has keys and garage door opener for access to garage if needed- John Jett 804-724-4869
                ',
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 26,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 11,                          // Clint Nix
                "customer_id" => 26,                                // Shaffner
                "aob_signed_on" => "2020-06-19",
                "insurance_carrier_id" => 2,                        // UPC
                "policy_number" => 'UHV 37232880201',
                "claim_number" => '20FL00040062',
                "deductible_amount" => '6580.00',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '171219.50',
                "property_id" => 27,
                "description" => '
ANGEL DID TEMP 6/20

Please see info for access, customer is overseas. Neighbor has key. AOB Signed.

Daniel is doing Xactimate ~ $200 COMMISSION

LEEPA = BRENSCHEIDT MARC
tile repair
leaking into his dining room
single story
Lifted tiles, slipped tiles. broken tiles, slipped tiles, pulled screws, active leak, and previous leak after Hurricane Irma

skype number in Switzerland (6 hours behind) 941-621-3991

The neighbor at 1100 Romano Key has keys and garage door opener for access to garage if needed- John Jett 804-724-4869
                ',
                "temp_repair_needed" => 1,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 27,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 28,                               // Clayton's first deal
                "customer_id" => 27,                                // Watt
                "aob_signed_on" => "2020-06-22",
                "insurance_carrier_id" => 2,                        // UPC
                "policy_number" => 'UHV 3269481 061',
                "claim_number" => '20FL00039127',
                "desk_adjuster_id" => NULL,
                "field_adjuster_id" => NULL,
                "deductible_amount" => '0',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '141419.04',
                "property_id" => 28,
                "description" => '
Daniel is doing Xactimate. $200 commission

9990+ Cracked tiles directly related to wind damage
75% of roof is lifted.  (Mud Set)
Multiple areas of possible mud set tears on underlayment
40% of cap tiles are loose
Multiple sliding tiles
Multiple broken tiles
83 documented pictures

Call befor 4  at the home # to set up app

adjuster got on roof. Found ample lifting and loose tile. Found a few that would slide compleatly out
                ',
                "minimum_damaged_units" => 9990,
                "adjuster_meeting_at" => '2020-06-19 22:00:00',
                "adjuster_meeting_notes" => '
adjuster got on roof. Found ample lifting and loose tile. Found a few that would slide compleatly out
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 28,
                "user_id" => 1,                                     // Gabriel sent
                "estimator_id" => 8,                                // Frank
                "customer_id" => 28,                                // Hartstein
                "aob_signed_on" => "2020-06-19",
                "insurance_carrier_id" => 11,                       // Progressive
                "policy_number" => 'FLP 221042',
                "claim_number" => '637796-191013',
                "desk_adjuster_id" => 22,
                "deductible_amount" => '0',
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '102930.41',
                "previously_denied" => 1,
                "property_id" => 29,
                "description" => '
    150+ chipped or cracked tile related to wind lift and damage
Sliding Tiles
Multiple areas of lift
Loose Cap Tiles

tile repair
there is a leak in the corner of the overhang by front door
there is a water stain on the ceiling of the garage as well
                ',
                "minimum_damaged_units" => 150,
                "adjuster_meeting_at" => '2019-03-28 09:27:00',
                "status_id" => $send_to_carrier,
            ),

            array(
                "id" => 29,
                "user_id" => 1,                                 // Gabriel sent
                "estimator_id" => 5,                            // Ralph
                "customer_id" => 29,                            // Black
                "aob_signed_on" => "2020-07-31",
                "insurance_carrier_id" => 13,                   // Ark Royal
                "policy_number" => 'ARK 4413',
                "claim_number" => '7879015201011',
                "field_adjuster_id" => 24,
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '174033.76',
                "property_id" => 30,
                "description" => '
Over 40 cracked/broken tiles
Loose tiles in field, and on hip & ridges
Sliding tiles throughout the roof
Lifted tiles over 2" throughout the roof
Broken mortar on 50% of hip & ridges
Pioneer tile no longer manufactured
Active leaks - Damage to interior ceiling and walls

Customer Notes: Daniel was originally assigned to this customer, and he was the one that was able to see the interior damages, and documented them with pictures. However, I am meeting with the adjuster Dave today July 31st, 2020, and unfortunately, the HO is in canada at this time, and he will not be able to view the damages!
                ',
                "adjuster_meeting_at" => '2020-07-31 15:30:00',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 30,
                "user_id" => 2,                                     // Lindsey sent
                "estimator_id" => 11,                          // Clint Nix
                "customer_id" => 30,                                // Davis
                "aob_signed_on" => "2020-07-28",
                "insurance_carrier_id" => 5,                        // Tower Hill
                "policy_number" => 'E001717339',
                "claim_number" => '3300345405',
                "desk_adjuster_id" => NULL,
                "field_adjuster_id" => 23,
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '153363.19',
                "deductible_amount" => '6740.00',
                "property_id" => 31,
                "description" => '
CCPA = DAVIS WILLIAM O & PATRICIA A
Toni Tyrer Lead $1000 referral

Customer will not pay any deductible per Norm, his interior receipt for damages and advertising credit will cancel it out.
                ',
                "minimum_damaged_units" => 56,
                "adjuster_meeting_at" => '2020-07-27 09:00:00',
                "adjuster_meeting_notes" => '
Engineer with EFI Global,
Jason Shelton Senior Forensic Engineer
321-251-9091
Cell- 813-712-0247
Jason.shelton@efiglobal.com
Showed up at 9:45 AM 7/27/2020
Said after his 20 minute inspection that the tile is obsolete and he would testify the entire roof needs replaced
Said he is not allowed to make any decisions anymore, that he wouldn’t be long, and he was going to go up and take some pictures.
Homeowner told him he had recently had interior repair all over and has receipts. Engineer did not want to go inside.
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 31,
                "user_id" => 22,                                    // Perri sent
                "estimator_id" => 26,                          // Chris
                "customer_id" => 31,                                // Shivel
                "aob_signed_on" => "2020-07-30",
                "insurance_carrier_id" => 2,                        // UPC
                "policy_number" => 'UHC 38969900801',
                "claim_number" => '20FL00045472',
                "desk_adjuster_id" => 17,
                "field_adjuster_id" => 11,
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '110181.99',
                "deductible_amount" => '13050.00',
                "property_id" => 32,
                "description" => '
CCPA = DAVIS WILLIAM O & PATRICIA A
Toni Tyrer Lead $1000 referral


Customer will not pay any deductible per Norm, his interior receipt for damages and advertising credit will cancel it out.
                ',
                "temp_repair_needed" => 1,
                "adjuster_meeting_at" => '2020-07-22 09:00:00',
                "adjuster_meeting_notes" => '
Didn\'t say much.  He was pretty much neutral as I have ever seen.
                ',
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 32,
                "user_id" => 20,                                    // Perri sent
                "estimator_id" => 10,                               // Eray
                "customer_id" => 32,                                // Romano
                "aob_signed_on" => "2020-08-05",
                "insurance_carrier_id" => 2,                        // UPC
                "policy_number" => 'UHV 416180102',
                "claim_number" => '20FL00049283',
                "desk_adjuster_id" => 17,
                "field_adjuster_id" => NULL,
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '123204.39',                  // Net Claim from Exactimate
                "deductible_amount" => '6020.00',
                "property_id" => 33,
                "description" => '
More than 25 % of the roof tiles are wind damaged and lifted
Lose ridge caps and lose tiles.
More than 70 broken tiles.',
                "minimum_damaged_units" => 70,
                "temp_repair_needed" => 1,
                "adjuster_meeting_at" => '2020-08-07 08:30:00',
                "adjuster_meeting_notes" => NULL,
                "status_id" => $send_to_carrier,                             // WTF is this?? @ Shehzad
            ),
            array(
                "id" => 33,
                "user_id" => 20,                                    // Perri sent
                "estimator_id" => 26,                               // Critter
                "customer_id" => 33,                                // Patalano
                "aob_signed_on" => "2020-08-17",
                "insurance_carrier_id" => 10,                       // St. Johns
                "policy_number" => 'SJ30192804',
                "claim_number" => '720985095',
                "mortgage_company_id" => 1,
                "mortgage_account_number" => '720985095',
                "desk_adjuster_id" => NULL,
                "field_adjuster_id" => 30,
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '161489.53',                  // Net Claim from Exactimate
                "deductible_amount" => '10880.00',
                "property_id" => 34,
                "description" => '
Roman Roofing AOB


DANIEL DID XACTIMATE $200








The roof has over 100 damaged tile and most of the roof is loose.',
                "minimum_damaged_units" => 100,
                "temp_repair_needed" => 0,
                "adjuster_meeting_at" => '2020-06-16 10:32:00',
                "adjuster_meeting_notes" => NULL,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 34,
                "user_id" => 20,                                    // Perri sent
                "estimator_id" => 5,                                // Ralph
                "customer_id" => 34,                                // Anderson
                "aob_signed_on" => "2020-08-20",
                "insurance_carrier_id" => 4,                        // Heritage
                "policy_number" => 'HPH191264',
                "claim_number" => 'HP210240',
                "mortgage_company_id" => 1,
                "mortgage_account_number" => '0721127751',
                "desk_adjuster_id" => NULL,
                "field_adjuster_id" => 30,
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '153977.00',                  // Net Claim from Exactimate
                "deductible_amount" => '7458.00',
                "property_id" => 35,
                "description" => '






Over 40 cracked/broken tiles



Loose tiles in field, and on hip & ridges


Sliding tiles throughout the roof


Lifted tiles over 2" throughout the roof


Broken mortar on 50% of hip & ridges


Ceetile no longer manufactured





Adjuster Notes:


Adjuster has been to the house before HO\'s signed with us, I am waiting for info on the adjuster from HO
',
                "minimum_damaged_units" => 40,
                "temp_repair_needed" => 0,
                "adjuster_meeting_notes" => NULL,
                "status_id" => $send_to_carrier,
            ),
            array(
                "id" => 35,
                "user_id" => 20,                                    // Perri sent
                "estimator_id" => 8,                                // Frank
                "customer_id" => 35,                                // Regler
                "aob_signed_on" => "2020-08-18",
                "insurance_carrier_id" => 5,                        // Tower Hill
                "policy_number" => 'E004257245',
                "claim_number" => '3300347893',
                "mortgage_company_id" => 2,
                "mortgage_account_number" => '3300035036',
                "desk_adjuster_id" => NULL,
                "field_adjuster_id" => 30,
                "date_of_loss" => '2017-09-10',
                "xactimate_amount" => '178072.61',                  // Net Claim from Exactimate
                "deductible_amount" => '7458.00',
                "property_id" => 36,
                "description" => '
Lifted Tiles


40+ Chipped Tiles


5 Broken Tiles
',
                "minimum_damaged_units" => 40,
                "temp_repair_needed" => 0,
                "adjuster_meeting_at" => '2020-08-07 13:00:00',
                "adjuster_meeting_notes" => 'Steven was 30 Minutes Early. He did an internal inspection of attic for 8 minutes, then was inside the house for 6 minutes.  Went on the roof at 1241pm and was off at 1244pm.  He did not appear to take more than 10 pics or so.',
                "status_id" => $send_to_carrier,
            ),
        );

        foreach ($maximus_claims as $maximus_claim) {
            $claim = \App\MaximusClaim::create($maximus_claim);

            // Seeding Damage Photos
            $dir = public_path("storage/maximus/$claim->id/aob/uploads");
            $attachments = is_dir($dir) ? File::files($dir) : array();

            foreach ($attachments as $attachment) {
                $path = str_replace('\\', '/', explode('public' . DIRECTORY_SEPARATOR, $attachment->getPathname())[1]);
                $claim->addMedia(public_path($path))->preservingOriginal()->toMediaCollection('damage-photos');
            }


            // Seeding Docs
            $dir = public_path("storage/maximus/$claim->id/aob/email_attachments");
            $attachments = is_dir($dir) ? File::files($dir) : array();

            foreach ($attachments as $attachment) {
                $not_found = true;
                $path = str_replace('\\', '/', explode('public' . DIRECTORY_SEPARATOR, $attachment->getPathname())[1]);
                foreach ($searches as $search) {
                    if (Str::contains($attachment->getFilename(), $search['keywords'])) {
                        $claim->addMedia(public_path($path))
                            ->preservingOriginal()
                            ->withCustomProperties(['media_type_id' => $search['type_id']])
                            ->toMediaCollection('documents');
                        $not_found = false;
                        break;
                    }
                }

                if ($not_found) {
                    $claim->addMedia(public_path($path))
                        ->preservingOriginal()
                        ->toMediaCollection('documents');
                }
            }
        }

    }
}
