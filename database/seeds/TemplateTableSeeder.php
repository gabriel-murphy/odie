<?php

use App\Template;
use Illuminate\Database\Seeder;

class TemplateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $templates = array(
            array(
                "id" => 1,
                "type" => 'email',
                "key" => "appointment_confirmation",
                "name" => "Appointment Confirmation",
                "subject" => "Your Appointment Confirmation with %%Company_Name%%",
                "content" => "<b>Dear %%Homeowner%%:<br>\r\n        \t<br>\r\n        \tThis is a courtesy email notification of your upcoming appointment with %%Estimator_Full_Name%% on %%Appointment_Weekday%%, %%Appointment_Longdate%% at %%Appointment_Longtime%%. No doubt %%Company_Name%% will earn your business at that time!<br>\r\n        \t<br>\r\n        \tBy the way, here is a picture of %%Estimator_First_Name%%: %%User_Photo%% </b>",
            ),
            array(
                "id" => 2,
                "type" => 'email',
                "key" => "estimate",
                "name" => "Estimate",
                "subject" => "Your %%Roof_Type%% Estimate from %%Company_Name%%",
                "content" => "<b>Dear %%Homeowner%%:<br>\r\n            <br>\r\n            Attached is your estimate.  Please sign or we will send someone to make you sign it! </b>",
            ),
            array(
                "id" => 3,
                "type" => 'email',
                "key" => "new_aob_submitted",
                "name" => "Maximus New Claim",
                "subject" => "Claim #%%claim_number%%",
                "content" => "<div style='color:#262626; font-family: Corbel, sans-serif;'><p>To Whom It May Concern:</p> <p>We are the roofing contractor engaged by %%customer_name%% (the “Customer”), with an assignment of benefits (“AOB”) and who has submitted a claim to %%insurance_carrier%% as indicated in the subject of this email.  The loss occurred on %%date_of_loss%% at %%property_address%% (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Customer; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by RoofScope; and</li><li>Roman Roofing Inc’s state license (CCC1330707).</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Customer.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be discoveries that will impact the estimate but are not immediately evident. As we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><p>Best Regards, </p></div>",
            ),
            array(
                "id" => 4,
                "type" => 'email',
                "key" => "temp_repair_created",
                "name" => "Temp Repair for Maximus",
                "subject" => "Temp Repair for Claim #%%claim_number%%",
                "content" => "<div style='color:#262626; font-family: Corbel, sans-serif;'><p>To Whom It May Concern:</p><p>Roman Roofing (“Roman”) contacted you initially on %%claim_sent_date%%, to notify Tower Hill Insurance  that we are the roofing contractor engaged by our client, %%customer_name%%  (“%%customer_last_name%%”), who asserted a claim.  The date of loss occurred on September 10, 2017 at 338 East Washington Road, Bradford, NH (the “Property”).  This email is to follow-up and notify you that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p> <ol><li>1. Photographs of damage and the work performed by Roman; and</li> <li>An invoice for the roofing services completed by Roman.</li></ol> <p>Please update the claim to include the temporary repair performed by Roman Roofing, Inc. at the subject property and kindly submit the attached invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p></div>",
            ),
            array(
                "id" => 5,
                "type" => 'email',
                "key" => "maximus_pa_agreement",
                "name" => "Maximus PA Agreement",
                "subject" => "Claim #%%claim_number%%",
                "content" => "<div style='color:#262626; font-family: Corbel, sans-serif;'><p>To whom it may concern:</p><p>Greetings – this email is to submit documents for our client, CLIENT (“CLIENT”), who has submitted a claim to %%insurance_carrier%% as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at %%property_address%% (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please feel free to contact us regarding this claim at Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663 or the homeowner that you have on file.</p><p>Attached to this email you will find the following documents:</p><ol><li>Contract between CLIENT and Roman Roofing Inc. for Insurance Proceeds</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li></ol><p>We kindly ask that you email us an acknowledgement of your receipt of the above documents from CLIENT</p><p>The attached estimate is based upon the information known to us at the time the contract was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><br><br><p>We appreciate your assistance and look forward to working with you. </p></div>",
            ),
            array(
                "id" => 6,
                "type" => 'email',
                "key" => "resend_email",
                "name" => "Resend Email",
                "subject" => "RE: %%email_subject%%",
                "content" => "<div style='color:#262626; font-family: Corbel, sans-serif;'><p>To Whom It May Concern:</p><div style='border-left: 4px solid #008002!important;padding-left: 10px; '><p><strong>From:</strong> %%email_from%% <br><strong>Sent:</strong> %%email_sent_on%%<br>%%email_recipients%%<br><strong>Subject:</strong> %%email_subject%% <br></p><p>%%email_content%%</p></div></div>",
            ),
            array(
                "id" => 7,
                "type" => 'email',
                "key" => "maximus_info_submitted",
                "name" => "Info Submitted",
                "subject" => "Here is the Info",
                "content" => "<div style='color:#262626; font-family: Corbel, sans-serif;'><p>To Whom It May Concern:</p><p>Here is the info you requested</p></div>",
            ),
            array(
                "id" => 8,
                "type" => 'email',
                "key" => "maximus_claim_escalated",
                "name" => "Escalation Notice",
                "subject" => "Escalation of Claim #%%claim_number%%",
                "content" => "<div style='color:#262626; font-family: Corbel, sans-serif;'><p>Dear %%escalator%%:</p><p>Roman Roofing wishes to escalate claim %%claim_number%% to %%escalator_company%%, so that your firm will commence legal action against %%insurance_carrier%%, who on %%decision_date%% issued a decision <strong>%%decision%%</strong> the aforementioned claim.  The claim arises from wind-storm related damage sustained on %%date_of_loss%% at the subject property located at %%property_address%%.</p><p>Please note the following documents which are attached to this email communication:</p><ol><li>Assignment of Benefits (\"AoB\") between Roman Roofing, Inc. and %%customer_name%%; and</li><li>Photographs indicating windstorm-related damage the roofing structure on the Property;</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by QuickMeasure; and</li><li>All correspondence received from %%insurance_carrier%%.</li></ol><p>We ask that you carefully review all of the attached materials to ensure there are no technicalities or deficiencies in our process that would otherwise jeoppardize or legal position.  Assuming our claim file is in order, we wish to prepare and send a pre-suit notification and demand letter from your firm to %%insurance_carrier%%.</p><p>Should you have any questions about this notification, please email us back at this address.</p></div>",
            ),
            array(
                "id" => 9,
                "type" => 'email',
                "key" => "forward_email",
                "name" => "Forward Email",
                "subject" => "FWD: %%email_subject%%",
                "content" => "<div style='color:#262626; font-family: Corbel, sans-serif;'><p>To Whom It May Concern:</p><div style='border-left: 4px solid #008002!important;padding-left: 10px; '><p><strong>From:</strong> %%email_from%% <br><strong>Sent:</strong> %%email_sent_on%%<br>%%email_recipients%%<br><strong>Subject:</strong> %%email_subject%% <br></p><p>%%email_content%%</p></div></div>",
            ),
            array(
                "id" => 10,
                "type" => 'campaign',
                "key" => "irma_campaign",
                "name" => "Irma Claims",
                "subject" => "🚨 Attention %%name%% - Only 2 Days Remain! ",
                "content" => "<tr>
            <td>
                <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"background-color: #e5e5e5;padding: 20px;\">
                    <tr>
                        <td>
                            <p style=\"color: #ad1f23;font-size: 31px;font-weight: 900;\">Attention %%name%%,</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-size: 15px;\">
                                My name is <strong>%%estimator%%</strong> and you may recall that
                                Roman Roofing completed a repair of your roof at <strong>%%site address line 1%% in %%site city%%.
                                    <u>I wanted to write to you as this is a very important alert</u>
                                </strong> – it is likely that your roof may be eligible for a claim for
                                windstorm-related damage.
                                <strong>%%name%%</strong>, you must file a claim with your
                                insurance carrier by no later than this Thursday, September 10, 2020!
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-size: 15px;\">Three steps to ensure you protect your interest:</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-size: 22px;\">
                                <strong>Step 1:</strong>
                            </p>
                            <p style=\"font-size: 15px\">
                                Open a claim for damage to your roof with your insurance
                                carrier immediately!
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-size: 22px;\">
                                <strong>Step 2:</strong>
                            </p>
                            <p style=\"font-size: 15px\">
                                Call Roman Roofing at 239-420-ROOF so that we can schedule a time to assess
                                the damage; and
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-size: 22px;\">
                                <strong>Step 3:</strong>
                            </p>
                            <p style=\"font-size: 15px\">
                                Be available for the appointment so we can ensure your claim is handled properly.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=\"font-size: 15px;\">
                                Please know <strong>%%name%%</strong> I am your advocate and looking to protect your home’s value which
                                will otherwise suffer if a new roof is needed when you try to sell your home in the future.
                                If you have any questions
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding: 40px 15px;\">
                    <tr>
                        <td align=\"center\">
                            <p style=\"font-size: 25px;\">
                                Please do not hesitate to contact me at
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align=\"center\">
                            <p class=\"contact\" style=\"font-size: 74px;color: #ad1f23!important;text-decoration: none!important;margin: 0;font-weight: 900;\">239-420-ROOF</p>
                        </td>
                    </tr>
                    <tr>
                        <td align=\"center\">
                            <p style=\"font-size: 25px;margin: 0;font-weight: 900;margin-bottom: 25px;\">Your rooofing pro,</p>
                        </td>
                    </tr>
                    <tr>
                        <td align=\"center\">
                            <p style=\"font-size: 25px;margin: 0;font-weight: 900;\">
                                %%estimator%%
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>",
            ),
        );

        foreach ($templates as $template) {
            \App\Template::create($template);
        }

        Template::find(1)->placeholders()->attach([8, 9]);
        Template::find(3)->placeholders()->attach([20, 21, 22, 23, 31]);
        Template::find(4)->placeholders()->attach([19, 20, 21, 22, 23, 24]);
        Template::find(5)->placeholders()->attach([22, 23]);
        Template::find(6)->placeholders()->attach([25,26,27,28, 29]);
        Template::find(7)->placeholders()->attach([30]);
        Template::find(8)->placeholders()->attach([20, 21, 22, 23, 31, 32, 33, 34, 35]);
        Template::find(9)->placeholders()->attach([25,26,27,28, 29]);
    }
}
