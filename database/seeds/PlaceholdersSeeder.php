<?php

use Illuminate\Database\Seeder;

class PlaceholdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $placeholders = array(
            array(
                "model" => 'company',
                "attribute" => 'name',
                "name" => "Company_Name",
            ),
            array(
                "model" => 'company',
                "attribute" => 'short_name',
                "name" => "Company_Short_Name",
            ),
            array(
                "model" => 'company',
                "attribute" => 'address',
                "name" => "Company_Address",
            ),
            array(
                "model" => 'company',
                "attribute" => 'long_address',
                "name" => "Company_Long_Address",
            ),
            array(
                "model" => 'company',
                "attribute" => 'main_phone',
                "name" => "Company_Main_Phone",
            ),
            array(
                "model" => 'company',
                "attribute" => 'fax_phone',
                "name" => "Company_Fax_Phone",
            ),
            array(
                "model" => 'company',
                "attribute" => 'trade_type',
                "name" => "Company_Trade_Type",
            ),
            array(
                "model" => 'user',
                "attribute" => 'first_name',
                "name" => "User_First_Name",
            ),
            array(
                "model" => 'user',
                "attribute" => 'last_name',
                "name" => "User_Last_Name",
            ),
            array(
                "model" => 'user',
                "attribute" => 'name',
                "name" => "user_full_name",
            ),
            array(
                "model" => 'user',
                "attribute" => 'title',
                "name" => "User_Title",
            ),
            array(
                "model" => 'user',
                "attribute" => 'cell_phone',
                "name" => "User_Cell_Phone",
            ),
            array(
                "model" => 'user',
                "attribute" => 'work_phone',
                "name" => "User_Work_Phone",
            ),
            array(
                "model" => 'user',
                "attribute" => 'fax_phone',
                "name" => "User_Fax_Phone",
            ),
            array(
                "model" => 'user',
                "attribute" => 'email',
                "name" => "User_Email",
            ),
            array(
                "model" => 'user',
                "attribute" => 'avatar_path',
                "name" => "User_Profile_Image",
            ),
            array(
                "model" => 'user',
                "attribute" => 'signature',
                "name" => "User_Signature_Image",
            ),
            array(
                "model" => 'user',
                "attribute" => 'signature_text',
                "name" => "User_Signature_Text",
            ),
            array(
                "model" => 'customer',
                "attribute" => 'last_name',
                "name" => "customer_last_name",
            ),
            array(
                "model" => 'customer',
                "attribute" => 'name',
                "name" => "customer_name",
            ),
            array(
                "model" => 'maximus_claim',
                "attribute" => 'claim_number',
                "name" => "claim_number",
            ),
            array(
                "model" => 'address',
                "attribute" => 'full_address',
                "name" => "property_address",
            ),
            array(
                "model" => 'insurance_carrier',
                "attribute" => 'name',
                "name" => "insurance_carrier",
            ),
            array(
                "model" => 'maximus_claim',
                "attribute" => 'created_at',
                "name" => "claim_sent_date",
            ),
            array(
                "model" => 'odmail',
                "attribute" => 'body',
                "name" => "email_content",
            ),
            array(
                "model" => 'odmail',
                "attribute" => 'sent_at',
                "name" => "email_sent_on",
            ),
            array(
                "model" => 'odmail',
                "attribute" => 'from',
                "name" => "email_from",
            ),
            array(
                "model" => 'odmail',
                "attribute" => 'recipients_html',
                "name" => "email_recipients",
            ),
            array(
                "model" => 'odmail',
                "attribute" => 'subject',
                "name" => "email_subject",
            ),
            array(
                "model" => 'constant',
                "attribute" => 'name',
                "name" => "document_type",
            ),
            array(
                "model" => 'maximus_claim',
                "attribute" => 'date_of_loss',
                "name" => "date_of_loss",
            ),
            array(
                "model" => 'maximus_claim',
                "attribute" => 'decided_on',
                "name" => "decision_date",
            ),
            array(
                "model" => 'maximus_claim',
                "attribute" => 'decision',
                "name" => "decision",
            ),
            array(
                "model" => 'contact',
                "attribute" => 'name',
                "name" => "escalator",
            ),
            array(
                "model" => 'contact',
                "attribute" => 'company',
                "name" => "escalator_company",
            ),
        );

        foreach ($placeholders as $placeholder)
        {
            \App\Placeholder::create($placeholder);
        }
    }
}
