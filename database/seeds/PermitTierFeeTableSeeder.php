<?php

use Illuminate\Database\Seeder;

class PermitTierFeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* The Permit Fee auto-calculation by the Platform is a key selling point and thus, there will NOT be a CRUD for the
    Permitting, as it is the Plaform's operators responsibility to ensure the permit fees are updated as may be changed, so
    that clients (Roman) do NOT have to think about calculation of the permit fee - the Platform always does it for them.
    NO other CRM platform does auto-calculation of permitting fees regardless of jurisdiction! */

    /* Tier Fee Seeder Last Updated w/ Current Tiers and Fees: March 1, 2020
    Bonita Springs only jurisdiction presently using tiered permit pricing in Southwest Florida */

    public function run()
    {
    $x=0;
    $start = 1001;
    $increment = 999;

    DB::insert('insert into permit_tier_fee (permit_jurisdictions_id, tier_start_amount, tier_end_amount, tier_permit_fee, created_at) values (?, ?, ?, ?, ?)',
    ['1', '0', '2000', '81.37', NOW()]);

    while ($x < 99) {

        $end = $start + $increment;
        DB::insert('insert into permit_tier_fee (permit_jurisdictions_id, tier_start_amount, tier_end_amount, tier_permit_fee, created_at) values (?, ?, ?, ?, ?)',
        ['1', $start, $end, '100', NOW()]);         // $100 for now, have to adjust this shit to actual numbers for 100 records.  Someone else can do that.
        $start = $end + 1;

        $x++;
    }

    }
}
