<?php

use Illuminate\Database\Seeder;

class RoofTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roof_types = array(
            array(
                "id" => "1",
                "name" => "Shingle",
                "long_name" => "Shingle Varieties",
                "additional_data" => [
                    "icon" => "fad fa-dumpster"
                ]
            ),
            array(
                "id" => "2",
                "name" => "Tile",
                "long_name" => "Tile Varieties",
                "additional_data" => [
                    "icon" => "fad fa-home"
                ]
            ),
            array(
                "id" => "3",
                "name" => "Metal",
                "long_name" => "Metal Varieties",
                "additional_data" => [
                    "icon" => "fad fa-medal"
                ]
            ),
            array(
                "id" => "4",
                "name" => "Other",
                "long_name" => "Coatings, Low-Slope & Solar",
                "additional_data" => [
                    "icon" => "fad fa-sun"
                ]
            ),
            array(
                "id" => "5",
                "name" => "Dimensional",
                "long_name" => "Dimensional Shingle",
                "roof_type_id" => 1,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "6",
                "name" => "Three-Tab",
                "long_name" => "Three-Tab Shingle",         // The cheap stuff
                "roof_type_id" => 1,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "7",
                "name" => "Concrete",
                "long_name" => "Concrete Tile",
                "roof_type_id" => 2,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "8",
                "name" => "Clay",
                "long_name" => "Clay Tile",
                "roof_type_id" => 2,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "9",
                "name" => "Slate",
                "long_name" => "Slate Varieties",
                "roof_type_id" => 2,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "10",
                "name" => "Aluminum",
                "long_name" => "Aluminum Varieties",
                "roof_type_id" => 3,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "11",
                "name" => "Steel",
                "long_name" => "Steel Varieties",
                "roof_type_id" => 3,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "12",
                "name" => "Stone-Coated Steel",
                "long_name" => "Stone Coated Steel",
                "roof_type_id" => 3,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "13",
                "name" => "Solar",
                "long_name" => "Solar & Energy Efficient",
                "roof_type_id" => 4,
                "additional_data" => [
                    "estimator" => '1',
                ]
            ),
            array(
                "id" => "14",
                "name" => "Coatings",
                "long_name" => "Coatings",
                "roof_type_id" => 4,
            ),
            array(
                "id" => "15",
                "name" => "ModBit",
                "long_name" => "Modified Bitumen",
                "roof_type_id" => 4,
            ),
            array(
                "id" => "16",
                "name" => "TPO",
                "long_name" => "TPO",
                "roof_type_id" => 4,
            ),
        );

        foreach ($roof_types as $roof_type) {
            \App\RoofType::create($roof_type);
        }
    }
}
