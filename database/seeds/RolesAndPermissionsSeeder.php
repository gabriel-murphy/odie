<?php

use Illuminate\Database\Seeder;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /*
    The Platform shall support a variety of roles, each of which will include a set of permissions attached to the role,
    allowing users in that role to have CRUD access to certain modules of the Platform.
    */
    public function run()
    {
        $roles = [
            'Admin',		    // Can do anything
            'Manager',		    // Can do everything but client setting edits but can over-ride margin floors
            'Estimator',		// Ability to be scheduled for estimates
            'Scheduler',		// Ability to create/edit opportunities and edit/schedule/cancel appointments
            'Crew Member',		// Access strictly limited to uploading photos, searching customers
            'Crew Lead',		// Access strictly limited to uploading photos, searching customers
            'Foreman',			// Production access to update job status
            'Customer',			// Access to the customer connect portal only
            'Production',		// Ability to order roof materials from the supply houses & assign crews to projects
            'Supplier',			// Building material supplier (ABC Supply & SunCoast) - to review / confirm orders & update product catalog and pricing
            'Manufacturer',	    // Roofing manufacturer supported by Odie, permits CRUD of product line & pricing
            'Public Adjuster',	// Public adjuster for insurance AOB claims
            'Accounting',		// Access to financial data & commission module
        ];

        foreach ($roles as $role)
        {
            \Spatie\Permission\Models\Role::create(['name' => $role]);
        }
    }
}
