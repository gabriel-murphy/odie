<?php

use Illuminate\Database\Seeder;

class LeadSourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Better Business Bureau', 'Associations', NULL]);
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['HomeAdvisor', 'Lead Sources', NULL]);
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Critter', 'Referral', NULL]);
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Policy Holder', 'Referral', NULL]);
    /*DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Yard Sign', 'Job Site', NULL]);*/
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Networx', 'Lead Sources', NULL]);
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Pine Lakes Home & Garden Show', 'Events', NULL]);
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Online Reviews', 'Reputation', NULL]);
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Thumbtack', 'Lead Sources', NULL]);
    DB::insert('insert into lead_sources (name, subcategory, user_id) values (?, ?, ?)', ['Real Estate Agent', 'Referral', NULL]);
    }
}
