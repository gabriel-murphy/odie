<?php

use Illuminate\Database\Seeder;

class USCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    /* We are going to pull in the CSV file, parse and insert each row */
    $lines = file("database/us_cities.csv");
	foreach ($lines as $data) {
    	$values = explode(',', $data);
    	//$values = $data;
		// print_r($values);
		DB::insert('insert into us_cities (name, county, state_code, state, us_zip_codes, type, latitude, longitude, area_code, population, households, median_income, land_area, water_area, time_zone, created_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$values[1], $values[2], $values[3], $values[4], $values[5], $values[6], $values[7], $values[8], $values[9], $values[10], $values[11], $values[12], $values[13], $values[14], $values[15], NOW()]);
		}
	}
}