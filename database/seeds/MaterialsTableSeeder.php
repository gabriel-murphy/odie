<?php

use Illuminate\Database\Seeder;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /* This materials seeder includes actual costs for Roman Roofing as of March 1, 2020 and contains proprietary table structures, which permits Odie to dynamically generate quantities of materials to determine a materials cost breakdown.  The materials seeded includes only what is needed to generate an estimate for shingle re-roof projects.  */

        /* We are going to pull in the CSV file containing the materials, parse and insert each row */


        $rows = file("storage/app/clients/1/onboarding/material_list.txt");     // The same material list Roman provided to Giddy Up

        foreach ($rows as $line_number => $row) {
            $row_data = explode("\t", $row);

            if ($line_number > 1) {
                $items_per_unit = (int)$this->getValue('units_per_unit', $row_data);
                $units_purchased_at_once = (int)$this->getValue('units_purchased_at_once', $row_data);
                $factor_value = $this->getValue('factor_value', $row_data);
                $factor_unit_id = (int)$this->getValue('factor_unit_id', $row_data);
                $roof_component_id = (int)$this->getValue('roof_component_id', $row_data);
                $roof_type_id = (int)$this->getValue('roof_type_id', $row_data);
                $roof_manufacturer_id = (int)$this->getValue('roof_manufacturers_id', $row_data);
                $unit_id = (int)$this->getValue('unit_id', $row_data);
                $description = Str::of($this->getValue('name', $row_data))->after('--');

                    \App\Material::create([
                    'client_id' => 1,
                    'location_id' => 1,
                    'trade_type_id' => 1,
                    'roof_material_category' => $this->getValue('roof_material_categories.id', $row_data),
                    'code' => null,
                    'giddy_up_code' => $this->getValue("Giddy_Up_Code\r\n", $row_data),
                    'name' => Str::of($this->getValue('name', $row_data))->replace('"', '').'"',
                    'short_name' => null,
                    'description' => $description,
                    'comments' => '',
                    'cost' => (double)$this->getValue('cost', $row_data),
                    'unit' => $unit_id ? $unit_id : null,
                    'items_per_unit' => $items_per_unit ? $items_per_unit : 1,
                    'units_purchased_at_once' => $units_purchased_at_once ? $units_purchased_at_once : 1,
                    'coverage_value' => $factor_value ? $factor_value : null,
                    'coverage_unit' => $factor_unit_id ? $factor_unit_id : null,
                    'roof_component_id' => $roof_component_id ? $roof_component_id : null,
                    'roof_type_id' => $roof_type_id ? $roof_type_id : null,
                    'roof_manufacturer_id' => $roof_manufacturer_id ? $roof_manufacturer_id : null,
                    'properties' => [
                        'length' => $this->getValue('length', $row_data),
                        'width' => $this->getValue('width', $row_data)
                    ],
                    'active' => 1
                ]);
            }
        }
    }

    private function getValue($column, $row_data)
    {
        $columns = [
            0 => "roof_material_categories.id",
            1 => "name",
            2 => "cost",
            3 => "units_purchased_at_once",
            4 => "units_per_unit",
            5 => "unit_id",
            6 => "factor_value",
            7 => "factor_unit_id",
            8 => "roof_components_id",
            9 => "length",
            10 => "width",
            11 => "roof_manufacturers_id",
            12 => "roof_manufacturers_lines_id",
            13 => "colors",
            14 => "color_pallete.id",
            15 => "active",
            16 => "roof_types.id",
            17 => "roof_measurements_column",
            18 => "roof_warranty_id",
            19 => "Shingle",
            20 => "Tile",
            21 => "Metal",
            22 => "Tapered",
            23 => "Flat",
            24 => "TPO",
            25 => "Coating",
            26 => "Solar",
            27 => "Giddy_Up_Code\r\n"
        ];
        return $row_data[array_search($column, $columns)] != '' ? $row_data[array_search($column, $columns)] : null;
    }
}
