<?php

use Illuminate\Database\Seeder;

class PermitJurisdictionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* The Permit Fee Auto-Calcuation by the Platform is a key selling point and thus, there will NOT be a CRUD for the
    Permitting, as it is the Plaform's operators responsiblity to ensure the permit fees are updated as may be changed, so
    that clients (Roman) do NOT have to think about calcuation of the permit fee - the Platform always does it for them.
    NO other CRM platform does auto-calcuation of permitting fees regardless of jurisdiction! */

    /* This List is Hard-Coded and Must be Updated by the Operators of the Platform.  As of 03/01/2020, the Platform seeds and
    calculates permitting fees for roofing projectors in 9 jurisdictions throughout Southwest Florida, south to Collier County and up to Charlotte County, and covering all muncipalities sprinkled in between. */

    /* Seeder Last Updated w/ Current Permitting Fees: March 1, 2020 */

    public function run()
    {
        $permit_jurisdictions = array(
            array(
                "id" => 1,
                "name" => "Bonita Springs",
                "type" => "city",
                "us_cities_id" => 4372,
                "tier_fee" => 1,
                "document" => "4.pdf",
            ),
            array(
                "id" => 2,
                "name" => "Cape Coral",
                "type" => "city",
                "us_cities_id" => 4405,
                "fixed_fee" => 125.00,
                "document" => "3.pdf",
            ),
            array(
                "id" => 3,
                "name" => "Charlotte",
                "type" => "county",
                "fixed_fee" => 120.00,
            ),
            array(
                "id" => 4,
                "name" => "Naples",
                "type" => "city",
                "fixed_fee" => 65.00,
                "per_square_fee" => 5.00,
            ),
            array(
                "id" => 5,
                "name" => "Collier",
                "type" => "county",
                "fixed_fee" => 225.00,
            ),
            array(
                "id" => 6,
                "name" => "Estero",
                "type" => "city",
                "us_cities_id" => 4514,
                "fixed_fee" => 100.00,
            ),
            array(
                "id" => 7,
                "name" => "Fort Myers Beach",
                "type" => "city",
                "us_cities_id" => 4544,
                "fixed_fee" => 130.00,
            ),
            array(
                "id" => 8,
                "name" => "Lee",
                "type" => "county",
                "fixed_fee" => 100.00,
            ),
            array(
                "id" => 9,
                "name" => "Sanibel",
                "type" => "city",
                "us_cities_id" => 5012,
                "percentage_fee" => 0.01,
            ),
        );
        foreach ($permit_jurisdictions as $permit_jurisdiction) {
            \App\PermitJurisdiction::create($permit_jurisdiction);
        }
    }
}
