<?php

use Illuminate\Database\Seeder;

class TempRepairsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $helper = app('constants');

        $temp_repairs = array(
            array(
                'maximus_claim_id' => 1,                // Morales
                'invoice_date' => '2020-07-09',
                'invoice_number' => '10935',
                'invoice_amount' => '2995',
                'completed_on' => '2020-06-16',
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 4,                // Crawford
                'invoice_date' => '2020-07-09',
                'invoice_number' => '10937',
                'invoice_amount' => '2998',
                'completed_on' => NULL,
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 6,                // MacLean
                'invoice_date' => NULL,
                'invoice_number' => NULL,
                'invoice_amount' => NULL,
                'completed_on' => '2020-06-11',
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 7,                // Allen
                'invoice_date' => '2020-06-19',
                'invoice_number' => NULL,
                'invoice_amount' => '2500',
                'completed_on' => '2020-06-11',
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 8,                // Kringle
                'invoice_date' => '2020-07-09',
                'invoice_number' => '10934',
                'invoice_amount' => '2900',
                'completed_on' => '2020-06-13',
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 9,                // Brown
                'invoice_date' => '2020-06-22',
                'invoice_number' => '10789',
                'invoice_amount' => '2998',
                'completed_on' => '2020-06-15',
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 11,               // Tully
                'invoice_date' => '2020-07-09',
                'invoice_number' => '10936',
                'invoice_amount' => '2995',
                'completed_on' => '2020-06-17',
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 25,               // Brenscheidt
                'invoice_date' => '2020-07-09',
                'invoice_number' => '10938',
                'invoice_amount' => '2895',
                'completed_on' => NULL,
                'damage_photo_count' => NULL,
            ),
            array(
                'maximus_claim_id' => 26,               // Shaffner
                'invoice_date' => '2020-07-09',
                'invoice_number' => '10939',
                'invoice_amount' => '2895',
                'completed_on' => NULL,
                'damage_photo_count' => NULL,
            ),
        );

        foreach ($temp_repairs as $temp_repair)
        {
            $claim = \App\MaximusClaim::find($temp_repair['maximus_claim_id']);
            $temp_repair = $claim->tempRepair()->create($temp_repair);

            $dir = public_path("storage/maximus/$claim->id/temp_repair/email_attachments");
            $attachments = is_dir($dir) ? File::files($dir) : array();

            foreach ($attachments as $attachment) {
                $path = str_replace('\\', '/', explode('public'.DIRECTORY_SEPARATOR, $attachment->getPathname())[1]);
                $temp_repair->addMedia(public_path($path))->preservingOriginal()->toMediaCollection('documents');            }
        }
    }
}
