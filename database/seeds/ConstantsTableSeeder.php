<?php

use App\Constant;
use App\ConstantGroup;
use Illuminate\Database\Seeder;

class ConstantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $general = array(
            [
                'key' => 'no_record',
                'name' => 'No Record Found'
            ]
        );

        $classifications = ["Residential", "Commercial", "Governmental", "Other",];

        $phone_types = ["Cell", "Home", "Work", "Fax"];

        $project_types = array(
            array(
                "name" => "Reroof",
                "additional_data" => ["long_name" => "Tear-off and installation of new roofing materials"]
            ),
            array(
                "name" => "Repair",
                "additional_data" => ["long_name" => "Identification and remediation of roof leak or other roofing issue"]
            ),
            array(
                "name" => "New Construction",
                "additional_data" => ["long_name" => "Construction of new structure requiring a roof"]
            ),
            array(
                "name" => "Inspection",
                "additional_data" => ["long_name" => "Roofing inspection with report provided"]
            ),
            array(
                "name" => "Insurance Proceeds",             // This is the Maximus Module
                "additional_data" => ["long_name" => "Insurance claim assigned to %%Client%% by homeowner"]
            ),
        );

        $attachment_groups = [
            "Odie", "Requested Info"
        ];

        $attachment_types = [
            "Build Sheet", "Estimate", "AoB", "Permit", "Change Order", "Sign-Off", "Inspection Report",
            "Invoice", "Receipt", "Xactimate", "Ariel Measurements", "itel Report", "Declaration Page", "Carrier Letter",
            "Insurance Policy", "Affidavit of Loss", "Damage Photos",
            ["name" => "Acknowledgement", "additional_data" => ["response_required" => 0], 'description' => 'Acknowledgement of AOB'],
            ["name" => "Request", "additional_data" => ["response_required" => 1], 'description' => 'Request more information / documentation'],
            ["name" => "Decision", "additional_data" => ["response_required" => 1], 'description' => 'Carrier makes a coverage decision'],
            "Other"
        ];

        $photo_types = [
            "Signed Project", "Signed NoC", "Curbside", "Mailbox", "Access", "Boot", "Vent", "Cricket",
            "Other Protrusion", "Problem Area", "Loose Shingle(s)", "Damaged Shingle(s)", "Cracked / Broken Tile(s)",
            "Significant Damage", "Wood Rot", "Dry-In Materials", "Job Commencement", "Dry-In Complete", "Job Progress",
            "Final Materials", "Final Underway", "Job Completion", "Signed Sign-Off",
        ];

        $trade_types = array(
            array(
                "name" => "Roofing",
                "additional_data" => ["long_name" => "Residential & Commercial Roofing Services"],
            ),
            array(
                "name" => "Gutters",
                "additional_data" => ["long_name" => "Residential & Commercial Gutter Services"],
            ),
        );

        $roof_manufacturers_material_types = array(
            array(
                "name" => "Asphalt",
                "additional_data" => ["roof_types_id" => 1],
            ),
            array(
                "name" => "Concrete",
                "additional_data" => ["roof_types_id" => 2],
            ),
            array(
                "name" => "Clay",
                "additional_data" => ["roof_types_id" => 2],
            ),
            array(
                "name" => "Aluminum",
                "additional_data" => ["roof_types_id" => 3],
            ),
            array(
                "name" => "Steel",
                "additional_data" => ["roof_types_id" => 3],
            ),
            array(
                "name" => "Photoelectric",
                "additional_data" => ["roof_types_id" => 5],
            ),
        );

        $roof_manufacturers_finish_types = array(
            array(
                "name" => "Kynar500",
                "additional_data" => ["detail" => "PVDF Resin Technology", "roof_manufacturers_id" => 6]
            ),
            array(
                "name" => "SMP Paint",
                "additional_data" => ["detail" => "SMP Paint", "roof_manufacturers_id" => 6]
            ),
            array(
                "name" => "Standard Mill",
                "additional_data" => ["detail" => "Mill Finish", "roof_manufacturers_id" => 6]
            ),
        );

        $carrier_relationships = [
            "Policy Holder", "Desk Appraiser", "Field Appraiser", "Public Adjuster", "Engineer", "Attorney"
        ];

        $roof_material_categories = array(
            array(
                "name" => "Boots",
                "additional_data" => ["code" => "BT"],
            ),
            array(
                "name" => "Coatings",
                "additional_data" => ["code" => "CO"],
            ),
            array(
                "name" => "Fasteners",
                "additional_data" => ["code" => "FA"],
            ),
            array(
                "name" => "Flashings",
                "additional_data" => ["code" => "FL"],
            ),
            array(
                "name" => "Flat",
                "additional_data" => ["code" => "FT"],
            ),
            array(
                "name" => "Metal",
                "additional_data" => ["code" => "MT"],
            ),
            array(
                "name" => "Miscellaneous",
                "additional_data" => ["code" => "MS"],
            ),
            array(
                "name" => "Shingle",
                "additional_data" => ["code" => "SH"],
            ),
            array(
                "name" => "Tapered",
                "additional_data" => ["code" => "TP"],
            ),
            array(
                "name" => "Tile",
                "additional_data" => ["code" => "TL"],
            ),
            array(
                "name" => "TPO",
                "additional_data" => ["code" => "TP"],
            ),
            array(
                "name" => "Underlayments",
                "additional_data" => ["code" => "UL"],
            ),
            array(
                "name" => "Vents",
                "additional_data" => ["code" => "VT"],
            ),
            array(
                "name" => "Wood",
                "additional_data" => ["code" => "WD"],
            ),
        );

        $slope_types = ["Flat", "Low", "Mid", "Steep"];

        $roof_leak_types = [
            "Minor ceiling staining, surface not wet.",
            "Ceiling stain with wet or area with light mold.",
            "Slow drip of water from ceiling.",
            "Active and ongoing dripping of water from ceiling.",
            "Stream of water from ceiling infiltrating the home.",
        ];

        $pitch_types = array(
            array(
                "name" => "0",
                "additional_data" => ["angle" => "0.0", "factor" => "0.00", "slope_type_id" => "1"]
            ),
            array(
                "name" => "1",
                "additional_data" => ["angle" => "4.8", "factor" => "1.01", "slope_type_id" => "2"]
            ),
            array(
                "name" => "2",
                "additional_data" => ["angle" => "9.5", "factor" => "0.02", "slope_type_id" => "2"]
            ),
            array(
                "name" => "3",
                "additional_data" => ["angle" => "14.0", "factor" => "1.03", "slope_type_id" => "2"]
            ),
            array(
                "name" => "4",
                "additional_data" => ["angle" => "18.4", "factor" => "1.05", "slope_type_id" => "3"]
            ),
            array(
                "name" => "5",
                "additional_data" => ["angle" => "22.6", "factor" => "1.08", "slope_type_id" => "3"]
            ),
            array(
                "name" => "6",
                "additional_data" => ["angle" => "30.3", "factor" => "1.12", "slope_type_id" => "3"]
            ),
            array(
                "name" => "7",
                "additional_data" => ["angle" => "33.7", "factor" => "1.16", "slope_type_id" => "4"]
            ),
            array(
                "name" => "8+",
                "additional_data" => ["angle" => "36.9", "factor" => "1.20", "slope_type_id" => "4"]
            ),
        );

        $appointment_types = array(
            array(
                "name" => "Evaluation",
                "additional_data" => ["long_name" => "No Cost Evaluation"]
            ),
            array(
                "name" => "Close",
                "additional_data" => ["long_name" => "Attempt to Close"]
            ),
            array(
                "name" => "NOC Meeting",
                "additional_data" => ["long_name" => "Meeting to Get NOC Signed"]
            ),
            array(
                "name" => "Pre-Construction Meeting",
                "additional_data" => ["long_name" => "Follow Up on Initial Appointment"]
            ),
            array(
                "name" => "Dry-In Inspection",
                "additional_data" => ["long_name" => "Meeting to Get Approval to Install"]
            ),
            array(
                "name" => "Final Inspection",
                "additional_data" => ["long_name" => "Meeting to Get Final Approval on Project"]
            ),
            array(
                "name" => "Payment",
                "additional_data" => ["long_name" => "Meeting to Get Final Payment"]
            ),
            array(
                "name" => "Review",
                "additional_data" => ["long_name" => "Meeting to Get Online Review"]
            ),
        );

        $crew_types = ["Install", "Tear-Off", "All"];

        $property_relationships = array(
            array(
                "name" => "Homeowner",
                "additional_data" => ["visible" => 1]
            ),
            array(
                "name" => "Agent/Broker",
                "additional_data" => ["visible" => 1]
            ),
            array(
                "name" => "Property Manager",
                "additional_data" => ["visible" => 1]
            ),
            array(
                "name" => "Tenant",
                "additional_data" => ["visible" => 1]
            ),
            array(
                "name" => "Relative",
                "additional_data" => ["visible" => 1]
            ),
            array(
                "name" => "Policy Holder",
                "additional_data" => ["visible" => 0]
            ),
            array(
                "name" => "Other",
                "additional_data" => ["visible" => 1]
            ),
        );

        $story_types = array(
            array(
                "name" => "Ranch",
                "additional_data" => ["stories" => 1]
            ),
            array(
                "name" => "Two-Story",
                "additional_data" => ["stories" => 2]
            ),
            array(
                "name" => "Three-Story",
                "additional_data" => ["stories" => 3]
            ),
            array(
                "name" => "4+ Levels",
                "additional_data" => ["stories" => 4]
            ),
        );

        $units = array(
            [
                "name" => "Squares",
                "additional_data" => ["abbrev" => "SQ"]
            ],
            [
                "name" => "Square Feet",
                "additional_data" => ["abbrev" => "SF"]
            ],
            [
                "name" => "Lineal Feet",
                "additional_data" => ["abbrev" => "LF"]
            ],
            [
                "name" => "Buckets",
                "additional_data" => ["abbrev" => "BK"]
            ],
            [
                "name" => "Boxes",
                "additional_data" => ["abbrev" => "BX"]
            ],
            [
                "name" => "Bundles",
                "additional_data" => ["abbrev" => "BD"]
            ],
            [
                "name" => "Gallons",
                "additional_data" => ["abbrev" => "GL"]
            ],
            [
                "name" => "Each",
                "additional_data" => ["abbrev" => "EA"]
            ],
            [
                "name" => "Rolls",
                "additional_data" => ["abbrev" => "RL"]
            ],
            [
                "name" => "Facets",
                "additional_data" => ["abbrev" => "FC"]
            ],
            [
                "name" => "Roofs",
                "additional_data" => ["abbrev" => "RF"]
            ],
            [
                "name" => "Cans",
                "additional_data" => ["abbrev" => "CN"]
            ],
            [
                "name" => "Quarts",
                "additional_data" => ["abbrev" => "QT"]
            ],
            [
                "name" => "Tubes",
                "additional_data" => ["abbrev" => "TB"]
            ],
            [
                "name" => "Bags",
                "additional_data" => ["abbrev" => "BG"]
            ],
            [
                "name" => "Tanks",
                "additional_data" => ["abbrev" => "TK"]
            ],
            [
                "name" => "Sheet",
                "additional_data" => ["abbrev" => "SH"]
            ],
            [
                "name" => "Cases",
                "additional_data" => ["abbrev" => "CA"]
            ],
            [
                "name" => "Kits",
                "additional_data" => ["abbrev" => "KS"]
            ],
            [
                "name" => "Panels",
                "additional_data" => ["abbrev" => "PN"]
            ],
        );

        $template_types = ["E-mail", "Text Message", "Voice", "Push Notification"];

        $maximus_attachment_groups = [
            "Initial Submission",         // AoB or Insurance Proceeds,
            "Temp Repair",
            "Request for Info",           // What we have been calling a supplement, insurance carrier wants more information from the policy holder
            "Response to Decision",
        ];


        $claim_types = ["Assignment of Benefits", "Insurance Proceeds"];

        $claim_statuses = array(
            ['name' => 'Sent to Carrier', 'description' => 'Sent by Maximus to the carrier and acknowledged by MailGun'],
            ['name' => 'Cancelled', 'description' => 'Policy holder rescinded the AoB'],
            ['name' => 'Doc Sent & Reviewing', 'description' => 'Carrier has opened the email (not applicable to form submitted claims)'],
            ['name' => 'Acknowledged', 'description' => 'Carrier has officially acknowledged AOB'],
            ['name' => 'Inspecting', 'description' => 'Carrier has scheduled a date/time for inspecting of the property'],
            ['name' => 'Evaluating', 'description' => 'Status after inspection occurs but before decision is made'],
            ['name' => 'Waiting', 'description' => 'Carrier awaiting additional information (supplement) to process claim'],
            ['name' => 'Decided', 'description' => 'Carrier made a decision - now Maximus needs a response from the client'],
            ['name' => 'Negotiating', 'description' => 'Claim holder and insurance carrier are negotiating a figure to resolve'],
            ['name' => 'Escalating', 'description' => 'Claim escalated to legal counsel by client'],
            ['name' => 'Settled', 'description' => 'Claim is settled (partial payment)'],
            ['name' => 'Abandoned', 'description' => 'Claim is abandoned by client after denial decision was made by carrier.'],
        );

        $maximus_claim_phases = array(
            "Submitted",          // AoB Submitted to Carrier (1, 2, 3)
            "Acknowledged",       // Carrier has officially acknowledged AOB (4)
            "Pre-Inspection",     // Inspection by the carrier of the property has not yet to occur (5)
            "Evaluating",         // After inspection but before decision (6, 7)
            "Decided",            // Carrier made a decision, but client has not responded (8)
            "Negotiating",        // Claim holder and insurance carrier are negotiating a figure to resolve (9)
            "Litigating",         // Claim escalated to legal counsel (10)
            "Closed",             // Claims which are paid in full, settled or abandoned (11, 12, 13)
        );

        $storms = array(
            array(
                "name" => "Hurricane Irma",
                "additional_data" => ["storm_type_id" => 1, "datestamp" => "09/10/2017", "highest_wind_gust" => "142", "county_ids" => "", "data_url" => "https://en.wikipedia.org/wiki/Hurricane_Irma"]
            ),
            array(
                "name" => "Cape Coral Dec 2018 Storm",
                "additional_data" => ["storm_type_id" => 2, "datestamp" => "12/21/2018", "highest_wind_gust" => "74", "county_ids" => "", "data_url" => "https://www.winknews.com/2018/12/20/neighbors-in-cape-coral-experience-property-damages-from-storm/"]
            ),
            array(
                "name" => "Pinellas County February 2020 Tornado",
                "additional_data" => ["storm_type_id" => 3, "datestamp" => "02/06/2020", "highest_wind_gust" => "85", "county_ids" => "", "data_url" => "https://www.weather.gov/tbw/feb062020_tor"]
            ),

        );

        $storm_types = array("Hurricane", "Windstorm", "Tornado", "Hail");

        $claim_submission_types = array(
            array(
                "name" => "Submission",
                "additional_data" => ["template_id" => 0]           // Selected when Client (Roman) submits the AOB Notice to the Carrier
            ),
            array(
                "name" => "Temp Repair",                            // Invoice for a temp repair needed at insured property sent to Carrier
                "additional_data" => ["template_id" => 0]
            ),
            array(
                "name" => "Response",                               // Selected when Client (Roman) is submitting additional information requested by Carrier
                "additional_data" => ["template_id" => 0]
            ),
            array(
                "name" => "Acceptance",                             // Selected when Client (Roman) accepts Decision or Counter-Decision submitted by Carrier
                "additional_data" => ["template_id" => 0]
            ),
            array(
                "name" => "Counter-Offer",                          // Selected when Client (Roman) responds to Carrier with a counter-offer to their decision or counter-decision
                "additional_data" => ["template_id" => 0]
            ),
            array(
                "name" => "Supplement",                             // NEED TO CHECK ON THIS
                "additional_data" => ["template_id" => 0]
            ),
        );

        $insurance_coverage_types = array(
            "Open",                // Coverage open, no amount indicated
            "Partial",             // Coverage open and partial payment / settlement tendered with letter
            "Full",                // Carrier pays full amount of claim
            "Denied",              // Carrier denies the claim
        );

        $negotiation_outcomes = array(
            "Abandoned",            // Client (Roman) decides to quit pursuing the claim and gives up
            "Counter",              // When client submits a revised number to the carrier based on the Carrier's decision or counter-decision
            "Escalate",             // Client does not counter-offer the Client and instead escalates the claim to legal contact to start legal action against Carrier
        );

        $coverage_denial_reasons = array(
            "Lack of timeliness of assertion of the claim.",
            "Not storm related damage / ordinary wear and tear.",
            "Coverage not in effect at the time loss was sustained.",
            "Other",
        );

        $ariel_report_providers = array(
            array(
                "name" => "EagleView",
                "additional_data" => ["fee" => 50, "submission_url" => NULL],
            ),
            array(
                "name" => "RoofScope",
                "additional_data" => ["fee" => 20, "submission_url" => NULL],
            ),
            array(
                "name" => "QuickMeasure",
                "additional_data" => ["fee" => 11, "submission_url" => NULL],
            ),
            array(
                "name" => "e360",
                "additional_data" => ["fee" => 20, "submission_url" => NULL],
            ),
        );

        $review_sources = array(
            array(
                "name" => "Better Business Bureau",
                "additional_data" => ["abbreviation" => "BBB", "review_url" => "https://www.bbb.org/us/fl/cape-coral/profile/roofing-contractors/roman-roofing-inc-0653-90222736"],
            ),
            array(
                "name" => "Google My Business",
                "additional_data" => ["abbreviation" => "Google"],
            ),
            array(
                "name" => "Facebook",
                "additional_data" => ["abbreviation" => "Facebook"],
            ),
            array(
                "name" => "Yelp!",
                "additional_data" => ["abbreviation" => "Yelp!"],
            ),
            array(
                "name" => "Angie's List",
                "additional_data" => ["abbreviation" => "Angie's List"],
            ),
        );

        $crew_craft_types = ["Tear-Off", "Dry-In", "Finish"];

        $crew_compensation_types = ["Hourly", "Square"];

        $states = array(
            array(
                "name" => "Alabama",
                "additional_data" => ["abbrev" => "AL"]
            ),
            array(
                "name" => "Alaska",
                "additional_data" => ["abbrev" => "AK"]
            ),
            array(
                "name" => "Arizona",
                "additional_data" => ["abbrev" => "AZ"]
            ),
            array(
                "name" => "Arkansas",
                "additional_data" => ["abbrev" => "AR"]
            ),
            array(
                "name" => "Californa",
                "additional_data" => ["abbrev" => "CA"]
            ),
            array(
                "name" => "Colorado",
                "additional_data" => ["abbrev" => "CO"]
            ),
            array(
                "name" => "Connecticut",
                "additional_data" => ["abbrev" => "CT"]
            ),
            array(
                "name" => "Delaware",
                "additional_data" => ["abbrev" => "DE"]
            ),
            array(
                "name" => "District of Columbia",
                "additional_data" => ["abbrev" => "DC"]
            ),
            array(
                "name" => "Florida",
                "additional_data" => ["abbrev" => "FL"]
            ),
            array(
                "name" => "Georgia",
                "additional_data" => ["abbrev" => "GA"]
            ),
            array(
                "name" => "Hawaii",
                "additional_data" => ["abbrev" => "HI"]
            ),
            array(
                "name" => "Idaho",
                "additional_data" => ["abbrev" => "ID"]
            ),
            array(
                "name" => "Illinois",
                "additional_data" => ["abbrev" => "IL"]
            ),
            array(
                "name" => "Indiana",
                "additional_data" => ["abbrev" => "IN"]
            ),
            array(
                "name" => "Iowa",
                "additional_data" => ["abbrev" => "IA"]
            ),
            array(
                "name" => "Kansas",
                "additional_data" => ["abbrev" => "KS"]
            ),
            array(
                "name" => "Kentucky",
                "additional_data" => ["abbrev" => "KY"]
            ),
            array(
                "name" => "Louisiana",
                "additional_data" => ["abbrev" => "LA"]
            ),
            array(
                "name" => "Maine",
                "additional_data" => ["abbrev" => "ME"]
            ),
            array(
                "name" => "Montana",
                "additional_data" => ["abbrev" => "MT"]
            ),
            array(
                "name" => "Nebraska",
                "additional_data" => ["abbrev" => "NE"]
            ),
            array(
                "name" => "Nevada",
                "additional_data" => ["abbrev" => "NV"]
            ),
            array(
                "name" => "New Hampshire",
                "additional_data" => ["abbrev" => "NH"]
            ),
            array(
                "name" => "New Jersey",
                "additional_data" => ["abbrev" => "NJ"]
            ),
            array(
                "name" => "New Mexico",
                "additional_data" => ["abbrev" => "NM"]
            ),
            array(
                "name" => "New York",
                "additional_data" => ["abbrev" => "NY"]
            ),
            array(
                "name" => "North Carolina",
                "additional_data" => ["abbrev" => "NC"]
            ),
            array(
                "name" => "North Dakota",
                "additional_data" => ["abbrev" => "ND"]
            ),
            array(
                "name" => "Ohio",
                "additional_data" => ["abbrev" => "OH"]
            ),
            array(
                "name" => "Oklahoma",
                "additional_data" => ["abbrev" => "OK"]
            ),
            array(
                "name" => "Oregon",
                "additional_data" => ["abbrev" => "OR"]
            ),
            array(
                "name" => "Maryland",
                "additional_data" => ["abbrev" => "MD"]
            ),
            array(
                "name" => "Massachusetts",
                "additional_data" => ["abbrev" => "MA"]
            ),
            array(
                "name" => "Michigan",
                "additional_data" => ["abbrev" => "MI"]
            ),
            array(
                "name" => "Minnesota",
                "additional_data" => ["abbrev" => "MN"]
            ),
            array(
                "name" => "Mississippi",
                "additional_data" => ["abbrev" => "MI"]
            ),
            array(
                "name" => "Missouri",
                "additional_data" => ["abbrev" => "MO"]
            ),
            array(
                "name" => "Pennsylvania",
                "additional_data" => ["abbrev" => "PA"]
            ),
            array(
                "name" => "Rhode Island",
                "additional_data" => ["abbrev" => "RI"]
            ),
            array(
                "name" => "South Carolina",
                "additional_data" => ["abbrev" => "SC"]
            ),
            array(
                "name" => "South Dakota",
                "additional_data" => ["abbrev" => "SD"]
            ),
            array(
                "name" => "Tennessee",
                "additional_data" => ["abbrev" => "TN"]
            ),
            array(
                "name" => "Texas",
                "additional_data" => ["abbrev" => "TX"]
            ),
            array(
                "name" => "Utah",
                "additional_data" => ["abbrev" => "UT"]
            ),
            array(
                "name" => "Vermont",
                "additional_data" => ["abbrev" => "VT"]
            ),
            array(
                "name" => "Virginia",
                "additional_data" => ["abbrev" => "VA"]
            ),
            array(
                "name" => "Washington",
                "additional_data" => ["abbrev" => "WA"]
            ),
            array(
                "name" => "West Virgina",
                "additional_data" => ["abbrev" => "WV"]
            ),
            array(
                "name" => "Wisconsin",
                "additional_data" => ["abbrev" => "WI"]
            ),
            array(
                "name" => "Wyoming",
                "additional_data" => ["abbrev" => "WY"]
            ),
        );

        $countries = array(
            array(
                "name" => "Afghanistan",
                "additional_data" => ["abbrev" => "AF"],
            ),
            array(
                "name" => "Albania",
                "additional_data" => ["abbrev" => "AL"],
            ),
            array(
                "name" => "Algeria",
                "additional_data" => ["abbrev" => "DZ"],
            ),
            array(
                "name" => "American Samoa",
                "additional_data" => ["abbrev" => "AS"],
            ),
            array(
                "name" => "Andorra",
                "additional_data" => ["abbrev" => "AD"],
            ),
            array(
                "name" => "Angola",
                "additional_data" => ["abbrev" => "AO"],
            ),
            array(
                "name" => "Anguilla",
                "additional_data" => ["abbrev" => "AI"],
            ),
            array(
                "name" => "Antarctica",
                "additional_data" => ["abbrev" => "AQ"],
            ),
            array(
                "name" => "Antigua And Barbuda",
                "additional_data" => ["abbrev" => "AG"],
            ),
            array(
                "name" => "Argentina",
                "additional_data" => ["abbrev" => "AR"],
            ),
            array(
                "name" => "Armenia",
                "additional_data" => ["abbrev" => "AM"],
            ),
            array(
                "name" => "Aruba",
                "additional_data" => ["abbrev" => "AW"],
            ),
            array(
                "name" => "Australia",
                "additional_data" => ["abbrev" => "AU"],
            ),
            array(
                "name" => "Austria",
                "additional_data" => ["abbrev" => "AT"],
            ),
            array(
                "name" => "Azerbaijan",
                "additional_data" => ["abbrev" => "AZ"],
            ),
            array(
                "name" => "Bahamas",
                "additional_data" => ["abbrev" => "BS"],
            ),
            array(
                "name" => "Bahrain",
                "additional_data" => ["abbrev" => "BH"],
            ),
            array(
                "name" => "Bangladesh",
                "additional_data" => ["abbrev" => "BD"],
            ),
            array(
                "name" => "Barbados",
                "additional_data" => ["abbrev" => "BB"],
            ),
            array(
                "name" => "Belarus",
                "additional_data" => ["abbrev" => "BY"],
            ),
            array(
                "name" => "Belgium",
                "additional_data" => ["abbrev" => "BE"],
            ),
            array(
                "name" => "Belize",
                "additional_data" => ["abbrev" => "BZ"],
            ),
            array(
                "name" => "Benin",
                "additional_data" => ["abbrev" => "BJ"],
            ),
            array(
                "name" => "Bermuda",
                "additional_data" => ["abbrev" => "BM"],
            ),
            array(
                "name" => "Bhutan",
                "additional_data" => ["abbrev" => "BT"],
            ),
            array(
                "name" => "Bolivia",
                "additional_data" => ["abbrev" => "BO"],
            ),
            array(
                "name" => "Bosnia And Herzegovina",
                "additional_data" => ["abbrev" => "BA"],
            ),
            array(
                "name" => "Botswana",
                "additional_data" => ["abbrev" => "BW"],
            ),
            array(
                "name" => "Bouvet Island",
                "additional_data" => ["abbrev" => "BV"],
            ),
            array(
                "name" => "Brazil",
                "additional_data" => ["abbrev" => "BR"],
            ),
            array(
                "name" => "British Indian Ocean Territory",
                "additional_data" => ["abbrev" => "IO"],
            ),
            array(
                "name" => "Brunei Darussalam",
                "additional_data" => ["abbrev" => "BN"],
            ),
            array(
                "name" => "Bulgaria",
                "additional_data" => ["abbrev" => "BG"],
            ),
            array(
                "name" => "Burkina Faso",
                "additional_data" => ["abbrev" => "BF"],
            ),
            array(
                "name" => "Burundi",
                "additional_data" => ["abbrev" => "BI"],
            ),
            array(
                "name" => "Cambodia",
                "additional_data" => ["abbrev" => "KH"],
            ),
            array(
                "name" => "Cameroon",
                "additional_data" => ["abbrev" => "CM"],
            ),
            array(
                "name" => "Canada",
                "additional_data" => ["abbrev" => "CA"],
            ),
            array(
                "name" => "Cape Verde",
                "additional_data" => ["abbrev" => "CV"],
            ),
            array(
                "name" => "Cayman Islands",
                "additional_data" => ["abbrev" => "KY"],
            ),
            array(
                "name" => "Central African Republic",
                "additional_data" => ["abbrev" => "CF"],
            ),
            array(
                "name" => "Chad",
                "additional_data" => ["abbrev" => "TD"],
            ),
            array(
                "name" => "Chile",
                "additional_data" => ["abbrev" => "CL"],
            ),
            array(
                "name" => "China",
                "additional_data" => ["abbrev" => "CN"],
            ),
            array(
                "name" => "Christmas Island",
                "additional_data" => ["abbrev" => "CX"],
            ),
            array(
                "name" => "Cocos (keeling) Islands",
                "additional_data" => ["abbrev" => "CC"],
            ),
            array(
                "name" => "Colombia",
                "additional_data" => ["abbrev" => "CO"],
            ),
            array(
                "name" => "Comoros",
                "additional_data" => ["abbrev" => "KM"],
            ),
            array(
                "name" => "Congo",
                "additional_data" => ["abbrev" => "CG"],
            ),
            array(
                "name" => "Congo, The Democratic Republic Of The",
                "additional_data" => ["abbrev" => "CD"],
            ),
            array(
                "name" => "Cook Islands",
                "additional_data" => ["abbrev" => "CK"],
            ),
            array(
                "name" => "Costa Rica",
                "additional_data" => ["abbrev" => "CR"],
            ),
            array(
                "name" => "Cote D'ivoire",
                "additional_data" => ["abbrev" => "CI"],
            ),
            array(
                "name" => "Croatia",
                "additional_data" => ["abbrev" => "HR"],
            ),
            array(
                "name" => "Cuba",
                "additional_data" => ["abbrev" => "CU"],
            ),
            array(
                "name" => "Cyprus",
                "additional_data" => ["abbrev" => "CY"],
            ),
            array(
                "name" => "Czech Republic",
                "additional_data" => ["abbrev" => "CZ"],
            ),
            array(
                "name" => "Denmark",
                "additional_data" => ["abbrev" => "DK"],
            ),
            array(
                "name" => "Djibouti",
                "additional_data" => ["abbrev" => "DJ"],
            ),
            array(
                "name" => "Dominica",
                "additional_data" => ["abbrev" => "DM"],
            ),
            array(
                "name" => "Dominican Republic",
                "additional_data" => ["abbrev" => "DO"],
            ),
            array(
                "name" => "East Timor",
                "additional_data" => ["abbrev" => "TP"],
            ),
            array(
                "name" => "Ecuador",
                "additional_data" => ["abbrev" => "EC"],
            ),
            array(
                "name" => "Egypt",
                "additional_data" => ["abbrev" => "EG"],
            ),
            array(
                "name" => "El Salvador",
                "additional_data" => ["abbrev" => "SV"],
            ),
            array(
                "name" => "Equatorial Guinea",
                "additional_data" => ["abbrev" => "GQ"],
            ),
            array(
                "name" => "Eritrea",
                "additional_data" => ["abbrev" => "ER"],
            ),
            array(
                "name" => "Estonia",
                "additional_data" => ["abbrev" => "EE"],
            ),
            array(
                "name" => "Ethiopia",
                "additional_data" => ["abbrev" => "ET"],
            ),
            array(
                "name" => "Falkland Islands (malvinas)",
                "additional_data" => ["abbrev" => "FK"],
            ),
            array(
                "name" => "Faroe Islands",
                "additional_data" => ["abbrev" => "FO"],
            ),
            array(
                "name" => "Fiji",
                "additional_data" => ["abbrev" => "FJ"],
            ),
            array(
                "name" => "Finland",
                "additional_data" => ["abbrev" => "FI"],
            ),
            array(
                "name" => "France",
                "additional_data" => ["abbrev" => "FR"],
            ),
            array(
                "name" => "French Guiana",
                "additional_data" => ["abbrev" => "GF"],
            ),
            array(
                "name" => "French Polynesia",
                "additional_data" => ["abbrev" => "PF"],
            ),
            array(
                "name" => "French Southern Territories",
                "additional_data" => ["abbrev" => "TF"],
            ),
            array(
                "name" => "Gabon",
                "additional_data" => ["abbrev" => "GA"],
            ),
            array(
                "name" => "Gambia",
                "additional_data" => ["abbrev" => "GM"],
            ),
            array(
                "name" => "Georgia",
                "additional_data" => ["abbrev" => "GE"],
            ),
            array(
                "name" => "Germany",
                "additional_data" => ["abbrev" => "DE"],
            ),
            array(
                "name" => "Ghana",
                "additional_data" => ["abbrev" => "GH"],
            ),
            array(
                "name" => "Gibraltar",
                "additional_data" => ["abbrev" => "GI"],
            ),
            array(
                "name" => "Greece",
                "additional_data" => ["abbrev" => "GR"],
            ),
            array(
                "name" => "Greenland",
                "additional_data" => ["abbrev" => "GL"],
            ),
            array(
                "name" => "Grenada",
                "additional_data" => ["abbrev" => "GD"],
            ),
            array(
                "name" => "Guadeloupe",
                "additional_data" => ["abbrev" => "GP"],
            ),
            array(
                "name" => "Guam",
                "additional_data" => ["abbrev" => "GU"],
            ),
            array(
                "name" => "Guatemala",
                "additional_data" => ["abbrev" => "GT"],
            ),
            array(
                "name" => "Guinea",
                "additional_data" => ["abbrev" => "GN"],
            ),
            array(
                "name" => "Guinea-bissau",
                "additional_data" => ["abbrev" => "GW"],
            ),
            array(
                "name" => "Guyana",
                "additional_data" => ["abbrev" => "GY"],
            ),
            array(
                "name" => "Haiti",
                "additional_data" => ["abbrev" => "HT"],
            ),
            array(
                "name" => "Heard Island and Mcdonald Islands",
                "additional_data" => ["abbrev" => "HM"],
            ),
            array(
                "name" => "Holy See (Vatican City State)",
                "additional_data" => ["abbrev" => "VA"],
            ),
            array(
                "name" => "Honduras",
                "additional_data" => ["abbrev" => "HN"],
            ),
            array(
                "name" => "Hong Kong",
                "additional_data" => ["abbrev" => "HK"],
            ),
            array(
                "name" => "Hungary",
                "additional_data" => ["abbrev" => "HU"],
            ),
            array(
                "name" => "Iceland",
                "additional_data" => ["abbrev" => "IS"],
            ),
            array(
                "name" => "India",
                "additional_data" => ["abbrev" => "IN"],
            ),
            array(
                "name" => "Indonesia",
            ),
            array(
                "name" => "Iran, Islamic Republic of",
                "additional_data" => ["abbrev" => "IR"],
            ),
            array(
                "name" => "Iraq",
                "additional_data" => ["abbrev" => "IQ"],
            ),
            array(
                "name" => "Ireland",
                "additional_data" => ["abbrev" => "IE"],
            ),
            array(
                "name" => "Israel",
                "additional_data" => ["abbrev" => "IL"],
            ),
            array(
                "name" => "Italy",
                "additional_data" => ["abbrev" => "IT"],
            ),
            array(
                "name" => "Jamaica",
                "additional_data" => ["abbrev" => "JM"],
            ),
            array(
                "name" => "Japan",
                "additional_data" => ["abbrev" => "JP"],
            ),
            array(
                "name" => "Jordan",
                "additional_data" => ["abbrev" => "JO"],
            ),
            array(
                "name" => "Kazakstan",
                "additional_data" => ["abbrev" => "KZ"],
            ),
            array(
                "name" => "Kenya",
                "additional_data" => ["abbrev" => "KE"],
            ),
            array(
                "name" => "Kiribati",
                "additional_data" => ["abbrev" => "KI"],
            ),
            array(
                "name" => "Korea, Democratic People's Republic of",
                "additional_data" => ["abbrev" => "KP"],
            ),
            array(
                "name" => "Korea, Republic Of",
                "additional_data" => ["abbrev" => "KR"],
            ),
            array(
                "name" => "Kosovo",
                "additional_data" => ["abbrev" => "KV"],
            ),
            array(
                "name" => "Kuwait",
                "additional_data" => ["abbrev" => "KW"],
            ),
            array(
                "name" => "Kyrgyzstan",
                "additional_data" => ["abbrev" => "KG"],
            ),
            array(
                "name" => "Lao People's Democratic Republic",
                "additional_data" => ["abbrev" => "LA"],
            ),
            array(
                "name" => "Latvia",
                "additional_data" => ["abbrev" => "LV"],
            ),
            array(
                "name" => "Lebanon",
                "additional_data" => ["abbrev" => "LB"],
            ),
            array(
                "name" => "Lesotho",
                "additional_data" => ["abbrev" => "LS"],
            ),
            array(
                "name" => "Liberia",
                "additional_data" => ["abbrev" => "LR"],
            ),
            array(
                "name" => "Libyan Arab Jamahiriya",
                "additional_data" => ["abbrev" => "LY"],
            ),
            array(
                "name" => "Liechtenstein",
                "additional_data" => ["abbrev" => "LI"],
            ),
            array(
                "name" => "Lithuania",
                "additional_data" => ["abbrev" => "LT"],
            ),
            array(
                "name" => "Luxembourg",
                "additional_data" => ["abbrev" => "LU"],
            ),
            array(
                "name" => "Macau",
                "additional_data" => ["abbrev" => "MO"],
            ),
            array(
                "name" => "Macedonia, The Former Yugoslav Republic of",
                "additional_data" => ["abbrev" => "MK"],
            ),
            array(
                "name" => "Madagascar",
                "additional_data" => ["abbrev" => "MG"],
            ),
            array(
                "name" => "Malawi",
                "additional_data" => ["abbrev" => "MW"],
            ),
            array(
                "name" => "Malaysia",
                "additional_data" => ["abbrev" => "MY"],
            ),
            array(
                "name" => "Maldives",
                "additional_data" => ["abbrev" => "MV"],
            ),
            array(
                "name" => "Mali",
                "additional_data" => ["abbrev" => "ML"],
            ),
            array(
                "name" => "Malta",
                "additional_data" => ["abbrev" => "MT"],
            ),
            array(
                "name" => "Marshall Islands",
                "additional_data" => ["abbrev" => "MH"],
            ),
            array(
                "name" => "Martinique",
                "additional_data" => ["abbrev" => "MQ"],
            ),
            array(
                "name" => "Mauritania",
                "additional_data" => ["abbrev" => "MR"],
            ),
            array(
                "name" => "Mauritius",
                "additional_data" => ["abbrev" => "MU"],
            ),
            array(
                "name" => "Mayotte",
                "additional_data" => ["abbrev" => "YT"],
            ),
            array(
                "name" => "Mexico",
                "additional_data" => ["abbrev" => "MX"],
            ),
            array(
                "name" => "Micronesia, Federated States of",
                "additional_data" => ["abbrev" => "FM"],
            ),
            array(
                "name" => "Moldova, Republic Of",
                "additional_data" => ["abbrev" => "MD"],
            ),
            array(
                "name" => "Monaco",
                "additional_data" => ["abbrev" => "MC"],
            ),
            array(
                "name" => "Mongolia",
                "additional_data" => ["abbrev" => "MN"],
            ),
            array(
                "name" => "Montserrat",
                "additional_data" => ["abbrev" => "MS"],
            ),
            array(
                "name" => "Montenegro",
                "additional_data" => ["abbrev" => "ME"],
            ),
            array(
                "name" => "Morocco",
                "additional_data" => ["abbrev" => "MA"],
            ),
            array(
                "name" => "Mozambique",
                "additional_data" => ["abbrev" => "MZ"],
            ),
            array(
                "name" => "Myanmar",
                "additional_data" => ["abbrev" => "MM"],
            ),
            array(
                "name" => "Namibia",
                "additional_data" => ["abbrev" => "NA"],
            ),
            array(
                "name" => "Nauru",
                "additional_data" => ["abbrev" => "NR"],
            ),
            array(
                "name" => "Nepal",
                "additional_data" => ["abbrev" => "NP"],
            ),
            array(
                "name" => "Netherlands",
                "additional_data" => ["abbrev" => "NL"],
            ),
            array(
                "name" => "Netherlands Antilles",
                "additional_data" => ["abbrev" => "AN"],
            ),
            array(
                "name" => "New Caledonia",
                "additional_data" => ["abbrev" => "NC"],
            ),
            array(
                "name" => "New Zealand",
                "additional_data" => ["abbrev" => "NZ"],
            ),
            array(
                "name" => "Nicaragua",
                "additional_data" => ["abbrev" => "NI"],
            ),
            array(
                "name" => "Niger",
                "additional_data" => ["abbrev" => "NE"],
            ),
            array(
                "name" => "Nigeria",
                "additional_data" => ["abbrev" => "NG"],
            ),
            array(
                "name" => "Niue",
                "additional_data" => ["abbrev" => "NU"],
            ),
            array(
                "name" => "Norfolk Island",
                "additional_data" => ["abbrev" => "NF"],
            ),
            array(
                "name" => "Northern Mariana Islands",
                "additional_data" => ["abbrev" => "MP"],
            ),
            array(
                "name" => "Norway",
                "additional_data" => ["abbrev" => "NO"],
            ),
            array(
                "name" => "Oman",
                "additional_data" => ["abbrev" => "OM"],
            ),
            array(
                "name" => "Pakistan",
                "additional_data" => ["abbrev" => "PK"],
            ),
            array(
                "name" => "Palau",
                "additional_data" => ["abbrev" => "PW"],
            ),
            array(
                "name" => "Palestinian Territory, Occupied",
                "additional_data" => ["abbrev" => "PS"],
            ),
            array(
                "name" => "Panama",
                "additional_data" => ["abbrev" => "PA"],
            ),
            array(
                "name" => "Papua New Guinea",
                "additional_data" => ["abbrev" => "PG"],
            ),
            array(
                "name" => "Paraguay",
                "additional_data" => ["abbrev" => "PY"],
            ),
            array(
                "name" => "Peru",
                "additional_data" => ["abbrev" => "PE"],
            ),
            array(
                "name" => "Philippines",
                "additional_data" => ["abbrev" => "PH"],
            ),
            array(
                "name" => "Pitcairn",
                "additional_data" => ["abbrev" => "PN"],
            ),
            array(
                "name" => "Poland",
                "additional_data" => ["abbrev" => "PL"],
            ),
            array(
                "name" => "Portugal",
                "additional_data" => ["abbrev" => "PT"],
            ),
            array(
                "name" => "Puerto Rico",
                "additional_data" => ["abbrev" => "PR"],
            ),
            array(
                "name" => "Qatar",
                "additional_data" => ["abbrev" => "QA"],
            ),
            array(
                "name" => "Reunion",
                "additional_data" => ["abbrev" => "RE"],
            ),
            array(
                "name" => "Romania",
                "additional_data" => ["abbrev" => "RO"],
            ),
            array(
                "name" => "Russian Federation",
                "additional_data" => ["abbrev" => "RU"],
            ),
            array(
                "name" => "Rwanda",
                "additional_data" => ["abbrev" => "RW"],
            ),
            array(
                "name" => "Saint Helena",
                "additional_data" => ["abbrev" => "SH"],
            ),
            array(
                "name" => "Saint Kitts And Nevis",
                "additional_data" => ["abbrev" => "KN"],
            ),
            array(
                "name" => "Saint Lucia",
                "additional_data" => ["abbrev" => "LC"],
            ),
            array(
                "name" => "Saint Pierre And Miquelon",
                "additional_data" => ["abbrev" => "PM"],
            ),
            array(
                "name" => "Saint Vincent And The Grenadines",
                "additional_data" => ["abbrev" => "VC"],
            ),
            array(
                "name" => "Samoa",
                "additional_data" => ["abbrev" => "WS"],
            ),
            array(
                "name" => "San Marino",
                "additional_data" => ["abbrev" => "SM"],
            ),
            array(
                "name" => "Sao Tome And Principe",
                "additional_data" => ["abbrev" => "ST"],
            ),
            array(
                "name" => "Saudi Arabia",
                "additional_data" => ["abbrev" => "SA"],
            ),
            array(
                "name" => "Senegal",
                "additional_data" => ["abbrev" => "SN"],
            ),
            array(
                "name" => "Serbia",
                "additional_data" => ["abbrev" => "RS"],
            ),
            array(
                "name" => "Seychelles",
                "additional_data" => ["abbrev" => "SC"],
            ),
            array(
                "name" => "Sierra Leone",
                "additional_data" => ["abbrev" => "SL"],
            ),
            array(
                "name" => "Singapore",
                "additional_data" => ["abbrev" => "SG"],
            ),
            array(
                "name" => "Slovakia",
                "additional_data" => ["abbrev" => "SK"],
            ),
            array(
                "name" => "Slovenia",
                "additional_data" => ["abbrev" => "SI"],
            ),
            array(
                "name" => "Solomon Islands",
                "additional_data" => ["abbrev" => "SB"],
            ),
            array(
                "name" => "Somalia",
                "additional_data" => ["abbrev" => "SO"],
            ),
            array(
                "name" => "South Africa",
                "additional_data" => ["abbrev" => "ZA"],
            ),
            array(
                "name" => "South Georgia And The South Sandwich Islands",
                "additional_data" => ["abbrev" => "GS"],
            ),
            array(
                "name" => "Spain",
                "additional_data" => ["abbrev" => "ES"],
            ),
            array(
                "name" => "Sri Lanka",
                "additional_data" => ["abbrev" => "LK"],
            ),
            array(
                "name" => "Sudan",
                "additional_data" => ["abbrev" => "SD"],
            ),
            array(
                "name" => "Suriname",
                "additional_data" => ["abbrev" => "SR"],
            ),
            array(
                "name" => "Svalbard And Jan Mayen",
                "additional_data" => ["abbrev" => "SJ"],
            ),
            array(
                "name" => "Swaziland",
                "additional_data" => ["abbrev" => "SZ"],
            ),
            array(
                "name" => "Sweden",
                "additional_data" => ["abbrev" => "SE"],
            ),
            array(
                "name" => "Switzerland",
                "additional_data" => ["abbrev" => "CH"],
            ),
            array(
                "name" => "Syrian Arab Republic",
                "additional_data" => ["abbrev" => "SY"],
            ),
            array(
                "name" => "Taiwan, Province Of China",
                "additional_data" => ["abbrev" => "TW"],
            ),
            array(
                "name" => "Tajikistan",
                "additional_data" => ["abbrev" => "TJ"],
            ),
            array(
                "name" => "Tanzania, United Republic Of",
                "additional_data" => ["abbrev" => "TZ"],
            ),
            array(
                "name" => "Thailand",
                "additional_data" => ["abbrev" => "TH"],
            ),
            array(
                "name" => "Togo",
                "additional_data" => ["abbrev" => "TG"],
            ),
            array(
                "name" => "Tokelau",
                "additional_data" => ["abbrev" => "TK"],
            ),
            array(
                "name" => "Tonga",
                "additional_data" => ["abbrev" => "TO"],
            ),
            array(
                "name" => "Trinidad And Tobago",
                "additional_data" => ["abbrev" => "TT"],
            ),
            array(
                "name" => "Tunisia",
                "additional_data" => ["abbrev" => "TN"],
            ),
            array(
                "name" => "Turvalue",
                "additional_data" => ["abbrev" => "TR"],
            ),
            array(
                "name" => "Turkmenistan",
                "additional_data" => ["abbrev" => "TM"],
            ),
            array(
                "name" => "Turks And Caicos Islands",
                "additional_data" => ["abbrev" => "TC"],
            ),
            array(
                "name" => "Tuvalu",
                "additional_data" => ["abbrev" => "TV"],
            ),
            array(
                "name" => "Uganda",
                "additional_data" => ["abbrev" => "UG"],
            ),
            array(
                "name" => "Ukraine",
                "additional_data" => ["abbrev" => "UA"],
            ),
            array(
                "name" => "United Arab Emirates",
                "additional_data" => ["abbrev" => "AE"],
            ),
            array(
                "name" => "United Kingdom",
                "additional_data" => ["abbrev" => "GB"],
            ),
            array(
                "name" => "United States of America",
                "additional_data" => ["abbrev" => "US"],
            ),
            array(
                "name" => "United States Minor Outlying Islands",
                "additional_data" => ["abbrev" => "UM"],
            ),
            array(
                "name" => "Uruguay",
                "additional_data" => ["abbrev" => "UY"],
            ),
            array(
                "name" => "Uzbekistan",
                "additional_data" => ["abbrev" => "UZ"],
            ),
            array(
                "name" => "Vanuatu",
                "additional_data" => ["abbrev" => "VU"],
            ),
            array(
                "name" => "Venezuela",
                "additional_data" => ["abbrev" => "VE"],
            ),
            array(
                "name" => "Viet Nam",
                "additional_data" => ["abbrev" => "VN"],
            ),
            array(
                "name" => "Virgin Islands, British",
                "additional_data" => ["abbrev" => "VG"],
            ),
            array(
                "name" => "Virgin Islands, U.s.",
                "additional_data" => ["abbrev" => "VI"],
            ),
            array(
                "name" => "Wallis And Futuna",
                "additional_data" => ["abbrev" => "WF"],
            ),
            array(
                "name" => "Western Sahara",
                "additional_data" => ["abbrev" => "EH"],
            ),
            array(
                "name" => "Yemen",
                "additional_data" => ["abbrev" => "YE"],
            ),
            array(
                "name" => "Zambia",
                "additional_data" => ["abbrev" => "ZM"],
            ),
            array(
                "name" => "Zimbabwe",
                "additional_data" => ["abbrev" => "ZW"],
            ),
        );

        $associate_types = ["Customer", "Associate" , "Policy Holder", "Attorney", "Adjuster"];

        $constant_groups = ConstantGroup::all();
        foreach ($constant_groups as $constant_group) {
            $group = $constant_group->key;
            $constants = isset($$group) ? $$group : array();
            foreach ($constants as $key => $constant) {
                $constant = is_array($constant) ? $constant : (array)$constant;
                $constant['constant_group_id'] = $constant_group->id;
                $constant['key'] = isset($constant['key']) ? $constant['key'] : $key + 1;
                $constant['name'] = isset($constant['name']) ? $constant['name'] : $constant[0];
                unset($constant[0]);
                Constant::create($constant);
            }
        }

    }
}
