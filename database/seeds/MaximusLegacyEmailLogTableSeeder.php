<?php

use Illuminate\Database\Seeder;

class MaximusLegacyEmailLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emails = array(
            array(
                'claim_id' => 1,
                'claim_submission_type_id' => 1,
                "message_id" => "<EC73B86C-30BD-4200-9913-1A87A218DE7F@romanroofinginc.com>",
                'to_recipients' => 'statefarmfireclaims@statefarm.com',
                'cc_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'bcc_recipients' => 'gabriel@romanroofing.com',
                'message_subject' => '5906V849N',
                'message_body' => '<p>To Whom It May Concern:</p><p>We are the roofing contractor engaged by our client, Maria Morales, with an assignment of benefits (“AOB”) and who has submitted a claim to State Farm. &nbsp;The date of loss occurred on September 10, 2017 at 2734 Blue Cypress Lake Ct, Cape Coral, FL 33909 (the “Property”).<br /> &nbsp;To schedule inspections at the Property, please email us at claims@romanroofinginc.com. Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Maria Morales; and</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Written, itemized, per-unit cost estimate of the services to be performed; and</li><li>Ariel scope of measure for the Property; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W9 for Roman Roofing, Inc.; and</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Maria Morales.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed. &nbsp;While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly. &nbsp;If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you!</p><p><br /> Best Wishes,<br /> <br /> <br /> Lindsey Dopfer<br /> Claims Department<br /> Roman Roofing, Inc<br /> 239-458-7663<br /> claims@romanroofinginc.com</p>',
                'type' => 'to',
                'created_at' => '2020-05-21 16:33:00',            // Timestamp is in EST
            ),

            array(
                'claim_id' => 2,
                'claim_submission_type_id' => 1,
                "message_id" => "<1457D0F6-0A5D-4B3A-8FFB-642B76253350@romanroofinginc.com>",
                'to_recipients' => 'Claims@upcinsurance.com',
                'cc_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'bcc_recipients' => NULL,
                'message_subject' => 'CLAIM #2017FL039867',
                'message_body' => '<p>To Whom It May Concern:</p><p>We are the roofing contractor engaged by our client, Donna Householder and J.P. Householder, with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance. The date of loss occurred on September 10, 2017 at 1705 Coral Way, North Fort Myers, FL 33917 (the “Property”). &nbsp;To schedule inspections at the Property, please email us at claims@romanroofinginc.com. &nbsp;Please direct all other communications regarding this claim to Roman Roofing, Inc. and not to our CLIENT.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Donna Householder and J.P. Householder; and</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Written, itemized, per-unit cost estimate of the services to be performed; and</li><li>Ariel scope of measure for the Property; and</li><li>ITEL Report; and</li>Roman Roofing Inc’s state license (CCC1330707); and<li>Signed W9 for Roman Roofing, Inc.; and</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from CUSTOMER.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed. &nbsp;While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly. &nbsp;If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you!</p><p><br /> Best Wishes,<br /> <br /> <br /> Lindsey Dopfer<br /> Claims Department<br /> Roman Roofing, Inc<br /> 239-458-7663<br /> claims@romanroofinginc.com</p>',
                'type' => 'to',
                'created_at' => '2020-06-03 13:59:00',
            ),

            array(
                'claim_id' => 3,
                'claim_submission_type_id' => 1,
                "message_id" => "<0c2c01d63acf$1d548d10$57fda730$@romanroofinginc.com>",
                'to_recipients' => 'claims@msagroup.com',
                'cc_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => 'CLAIM NUMBER: BPG7884H01',
                'message_body' => '<p>To Whom It May Concern:</p><p>We are the roofing contractor engaged by our client, Michelle and Dennis Rossman (the “Rossmans” or the “Client”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance. The date of loss occurred on September 10, 2017 at 1104 Southeast 46th Lane, Cape Coral, FL 33904 (the “Property”). &nbsp;To schedule inspections at the Property, please email us at claims@romanroofinginc.com. &nbsp;Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Rossmans; and</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Written, itemized, per-unit cost estimate of the services to be performed; and</li><li>Ariel scope of measure for the Property; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W9 for Roman Roofing, Inc.; and</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Maria Morales.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed. &nbsp;While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly. &nbsp;If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you!</p><p><br /> Best Wishes,<br /> <br /> <br /> Gabriel Murphy<br /> Claims Department<br /> Roman Roofing, Inc<br /> 239-458-7663<br /> claims@romanroofinginc.com</p>',
                'type' => 'to',
                'created_at' => '2020-06-04 20:20:00',
            ),

            array(
                'claim_id' => 4,
                'claim_submission_type_id' => 1,
                "message_id" => "<0c2c01d63acf$1d548d10$57fda730$@romanroofinginc.com>",
                'to_recipients' => 'claimshelp@universalproperty.com',
                'cc_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'bcc_recipients' => NULL,
                'message_subject' => 'CLAIM NUMBER: BPG7884H01',
                'message_body' => '<p><br /> </p><p>To Whom It May Concern:</p><p> </p><p>We are the roofing contractor engaged by our client, Linda Crawford, with an assignment of benefits (“AOB”) and who has submitted a claim to INSURANCE_CARRIER. The date of loss occurred on September 10, 2017 at 4727 Orange Grove Blvd, North Fort Myers, FL 33903 (the “Property”). &nbsp;To schedule inspections at the Property, please email us at claims@romanroofinginc.com. &nbsp;Please direct all other communications regarding this claim to Roman Roofing, Inc. and not to our CLIENT.</p><p><br /> </p><p>Attached to this email you will find the following documents:</p><p><br /> </p><p>Executed AOB between Roman Roofing, Inc. and Linda Crawford; and</p><p> </p><p>Notice of Deadline; and</p><p> </p><p>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</p><p> </p><p>Written, itemized, per-unit cost estimate of the services to be performed; and</p><p> </p><p>Ariel scope of measure for the Property; and</p><p> </p><p>ITEL Report; and</p><p> </p><p>Roman Roofing Inc’s state license (CCC1330707); and</p><p> </p><p>Signed W9 for Roman Roofing, Inc.; and</p><p><br /> </p><p>We kindly ask that you email us the following materials:</p><p><br /> </p><p>A copy of the declarations page for the date of loss; and</p><p> </p><p>The most recent estimate for damages at the property; and</p><p> </p><p>The deductible amount per the policy; and</p><p> </p><p>Acknowledgement of your receipt of the AOB from Linda Crawford.</p><p><br /> </p><p>The attached estimate is based upon the information known to us at the time the AOB was executed. &nbsp;While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly. &nbsp;If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p><br /> </p><p>We appreciate your assistance and look forward to working with you!</p><p><br /> <br /> </p><p>Best Wishes,</p><p><br /> </p><p>Lindsey Dopfer</p><p> </p><p>Claims Department</p><p> </p><p>Roman Roofing, Inc.</p><p> </p><p>239-458-7663</p><p> </p><p>claims@romanroofinginc.com</p><p> </p>',
                'type' => 'to',
                'created_at' => '2020-06-05 11:07:00',
            ),

            array(
                'claim_id' => 5,
                'claim_submission_type_id' => 1,
                "message_id" => "<057a01d63dec\$d4ef0a20$7ecd1e60$@romanroofinginc.com>",
                'to_recipients' => 'claimshelp@universalproperty.com',
                'cc_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'bcc_recipients' => 'lindsey@romanroofinginc.com;shehzad@cybersoph.com',
                'message_subject' => 'Claim #3300341576',
                'message_body' => '<p>To Whom It May Concern:</p><p>We are the roofing contractor engaged by our client, Denise Bruse  (“Bruse” or the “Client”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance. The date of loss occurred on September 10, 2017 at 2664 Clairfont Court, Cape Coral, Florida 33990 (the “Property”). &nbsp;To schedule inspections at the Property, please email us at claims@romanroofinginc.com. Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Bruse; and</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Written, itemized, per-unit cost estimate of the services to be performed; and</li><li>Ariel scope of measure for the Property; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W9 for Roman Roofing, Inc.; and</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Bruse.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed. While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly. &nbsp;If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you!</p><p><br /> Best Wishes,<br /> <br /> <br /> Gabriel Murphy<br /> Claims Department<br /> Roman Roofing, Inc<br /> 239-458-7663<br /> claims@romanroofinginc.com</p>',
                'type' => 'to',
                'created_at' => '2020-06-08 19:31:00',
            ),

            array(
                'claim_id' => 6,
                'claim_submission_type_id' => 1,
                "message_id" => "<057a01d63dec\$d4ef0a20$7ecd1e60$@romanroofinginc.com>",
                'to_recipients' => 'claims@asicorp.org',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => 'Claim #76632620103',
                'message_body' => '<p>To Whom It May Concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Craig and Abby Maclean (the “Macleans” or the “Client”), with an assignment of benefits (“AOB”) and who has submitted a claim  to Progressive Insurance Company, the claim number is indicated in the subject of this email.  The date of loss occurred  on September 10, 2017 at 8333 Creekview Lane, Englewood, Florida 34224 (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Macleans; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li></li>Written, itemized, per-unit cost estimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by EagleView; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.</li></ol><p>We kindly ask that you email us the following materials:</p><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Macleans.</li><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-11 12:42:00',
            ),

            array(
                'claim_id' => 7,
                'claim_submission_type_id' => 1,
                "message_id" => "<15de01d640f1$6d6f8750$484e95f0$@romanroofinginc.com>",
                'to_recipients' => 'AssignmentofBenefits@Heritagepci.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'lindsey@romanroofinginc.com;shehzad@cybersoph.com',
                'message_subject' => 'CLAIM# HP 207715',
                'message_body' => '<p>To Whom It May Concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Millard and Cathy Allen (the “Allens”), with an assignment of benefits (“AOB”) and who has submitted a claim to Heritage Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 14965 Toscana Way in Naples, Florida 34120 (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Allens; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by RoofScope; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Allens.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-12 15:41:00',
            ),

            array(
                'claim_id' => 8,
                'claim_submission_type_id' => 1,
                "message_id" => "<013801d6433d\$b8863ec0$2992bc40$@romanroofinginc.com>",
                'to_recipients' => 'claims@thig.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => 'Claim #3300341576',
                'message_body' => '<p>To Whom It May Concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Bruce and Peggy Kringle (the “Kringles”), with an assignment of benefits (“AOB”) and who has submitted a claim to Tower Hill Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 1302 Northeast 6th Avenue in Cape Coral, Florida 33909 (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofingin.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Kringles; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Kringles.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-15 13:52:00',
            ),

            array(
                'claim_id' => 9,
                'claim_submission_type_id' => 1,
                "message_id" => "<037901d644e3$393d9350\$abb8b9f0$@romanroofinginc.com>",
                'to_recipients' => 'jburdett@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => 'Claim #20FL00038905',
                'message_body' => '<p>Hello Ms. Burdett-</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Frank and Gwen Brown (the “Browns”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPS Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 19431 Lauzon Ave in Port Charlotte, Florida 33948 (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Browns; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Browns.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-17 16:10:00',
            ),

            array(
                'claim_id' => 10,
                'claim_submission_type_id' => 1,
                "message_id" => "<00ea01d64723\$d6d24420$8476cc60$@romanroofinginc.com>",
                'to_recipients' => 'dispatch@seibels.com',
                'cc_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => 'Claim #ST172230129',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, William Bockhorst (“Bockhorst”), with an assignment of benefits (“AOB”) and who has submitted a claim to Saint John’s Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 5104 97th St E in Bradenton, Florida 34211 (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Bockhorst; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Bockhorst.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-20 12:57:00',
            ),
            array(
                'claim_id' => 11,
                'claim_submission_type_id' => 1,
                'to_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => 'Claim #01-000031623',
                'message_body' => '<p>At 4:50 EST on 6/19/2020, I uploaded 11 documents to Frontline Insurance per the letter and the screen capture attached hereto.</p><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>
                ',
                'type' => 'to',
                'created_at' => '2020-06-19 16:53:00',
            ),
            array(
                'claim_id' => 12,
                'claim_submission_type_id' => 1,
                "message_id" => "<05e701d650eb$1511e800$3f35b800$@romanroofinginc.com>",
                'to_recipients' => 'dgaldiano@worleyco.com;danielle.moscinski@floridamic.org',
                'cc_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => 'Claim #CHO-00097652',
                'message_body' => '<p>Attention Daniel Galdiano:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Tachung Yih and Debbie Yih (the “Yihs”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 23212 Salinas Way in Bonita Springs, Florida 34135 (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.  You will note that the Yihs were previously and erroneously denied coverage (claim CHO-00048433).  We have copied our legal counsel on this communication.   We are subject matter experts at identifying and mitigating windstorm related damage on roofing structures and we are emphatic that this claim is legitimate and the damage a clear consequence of windstorm damage from Hurricane Irma.  In fact, we note over 50 cracked tiles and loose tiles all over the hip and ridges along with tiles with over 2” of lift throughout the field.  You can also observe broken mortar on 50% of the hip and ridge caps.  Accordingly, we encourage your firm reconsider its position to avoid litigating this claim.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Yihs; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by QuickEstimate; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Yihs.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-02 23:36:00',
            ),
            array(
                'claim_id' => 13,
                'claim_submission_type_id' => 1,
                "message_id" => "<05e701d650eb$1511e800$3f35b800$@romanroofinginc.com>",
                'to_recipients' => 'claims@asicorp.org',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'gabriel@romanroofinginc.com',
                'message_subject' => 'Claim #756050-201003',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Frances and Ronald P. Turowetz (“Turowetz”), with an assignment of benefits (“AOB”) and who has submitted a claim to Progressive Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 19022 Ridgepoint Drive in Estero, Florida 33928 (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Turkowetz; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Turkowetz.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                "created_at" => '2020-06-16 14:51:00',
            ),
            array(
                'claim_id' => 14,
                'claim_submission_type_id' => 1,
                "message_id" => "<05e701d650eb$1511e800$3f35b800$@romanroofinginc.com>",
                'to_recipients' => 'AssignmentofBenefits@Heritagepci.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'gabriel@romanroofinginc.com',
                'message_subject' => 'Claim #HP207625',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, James and Maureen Mitchell (the “Mitchells”), with an assignment of benefits (“AOB”) and who has submitted a claim to Heritage Property & Casualty Company as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 19029 Ridgepoint Drive in Estero, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Mitchells; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Mitchells.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-15 12:57:00',
            ),
            array(
                'claim_id' => 15,
                'claim_submission_type_id' => 1,
                'to_recipients' => 'oruddock@vargasgonzalez.com;louis@vargasgonzalez.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'gabriel@romanroofing.com',
                'message_subject' => 'Claim #01-31795',
                'message_body' => '<p>At 4:18 EST on 6/23/2020, I uploaded 11 documents to Frontline Insurance per the letter and the screen capture attached hereto.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-19 16:53:00',
            ),
            array(
                'claim_id' => 16,
                'claim_submission_type_id' => 1,
                "message_id" => "<05e701d650eb$1511e800$3f35b800$@romanroofinginc.com>",
                'to_recipients' => 'claims@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #20FL00042196',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Victor and Susan Cortner (the “Cortners”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 11684 LadyAnne Circle in Cape Coral, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Cortners; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Cortners.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                "created_at" => '2020-07-03 02:18:00',
            ),
            array(
                'claim_id' => 18,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'AssignmentofBenefits@Heritagepci.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #305-2943-017',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Gerald H and Kimberly A Laatsch (the “Laatschs”), with an assignment of benefits (“AOB”) and who has submitted a claim to Heritage Property & Casualty Company as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 19029 Ridgepoint Drive in Estero, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Laatschs; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Laatschs.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-10 04:38:00',
            ),
            array(
                'claim_id' => 19,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'claims@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #20FL00034798',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Lynn Buskard (“Buskard”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 12330 Village Way in Fort Myers, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Buskard; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Buskard.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-16 19:43:00',
            ),
            array(
                'claim_id' => 22,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'claims@thig.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #3300324995',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, John Brooks (“Brooks”), with an assignment of benefits (“AOB”) and who has submitted a claim to Tower Hill Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 2490 Verdmont Court in Cape Coral, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Brooks ; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Brooks.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-20 23:26:00',
            ),
            array(
                'claim_id' => 23,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'dispatch@seibels.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #ST17230159',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Michael and Lisa Murphy (the “Murphys”), with an assignment of benefits (“AOB”) and who has submitted a claim to Saint John\'s Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 1807 SW 29th Terrace in Cape Coral, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Murphys; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Murphys.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-29 14:07:00',
            ),
            array(
                'claim_id' => 25,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'csclaims@floridapeninsula.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #214625',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Marc Brenscheidt (“Brenscheidt”), with an assignment of benefits (“AOB”) and who has submitted a claim to Florida Penninsula Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 1807 SW 29th Terrace in Cape Coral, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Brenscheidt; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Brenscheidt.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-23 14:53:00',
            ),

            array(
                'claim_id' => 26,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'csclaims@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #20FL00040062',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Jerry Shaffner (“Shaffner”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 750 Northeast 5th Place in Cape Coral (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Shaffner; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Shaffner.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-24 04:26:00',
            ),
            array(
                'claim_id' => 27,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'csclaims@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #20FL00039127',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Donald A and Sharleen H Watt (the “Watts”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 9311 Old Hickory Court in Fort Myers, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and the Watts; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from the Watts.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-25 13:26:00',
            ),
            array(
                'claim_id' => 28,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'claims@asicorp.org',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #1857212414',
                'message_body' => '<p>To whom it may concern:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Alan Hartstein ("Hartstein”), with an assignment of benefits (“AOB”) and who has submitted a claim to ASI / Progressive as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 20018 Seadale Court in Estero, Florida (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><p>Attached to this email you will find the following documents:</p><ol><li>Executed AOB between Roman Roofing, Inc. and Hartstein; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the Property, as provided by GAF QuickMeasure; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.; and</li><li>Roman Roofing’s Certificate of Liability Insurance.</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from Hartstein.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-06-24 05:12:00',
            ),
            array(
                'claim_id' => 29,
                'claim_submission_type_id' => 1,
                "message_id" => "<99ECB88C-5B29-424C-3948-0D1579G7E9D1@romanroofinginc.com>",
                'to_recipients' => 'cat@asicorp.org',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'lindsey@romanroofinginc.com;perri@romanroofinginc.com;shehzad@cybersoph.com',
                'message_subject' => '7879015201011',
                'message_body' => '<p>Hello Ms. Jacqueline Dimery:</p><p>It was my pleasure speaking to you just now regarding this claim and I greatly appreciate your assistance in providing me with the email address for ASI / Ark Royal Insurance Company for notifications of assignments of benefits, which is why I am contacting you today.</p><p>Roman Roofing, Inc. (“Roman”) wishes to officially put ASI/Ark Royal Insurance Co. on notice that we are the roofing contractor engaged by our client, John Angus (the “Assignor”), with an assignment of benefits (“AoB”) and who has previously submitted a claim to ASI/Ark Royal Insurance Co. as indicated in the subject of this email.</p><p>Attached to this email you will find the following documents, as previously provided:</p><ol><li>Executed AoB between Roman Roofing, Inc. and the Assignor; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Sixteen (16) photographs indicating apparent windstorm-related damage through various facades of the roofing structure on the insured property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the insured property, as provided by GAF QuickMeasure; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.</li></ol><p>I kindly ask that you email me the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your timely receipt of the AOB from the Assignor.</li></ol><p>The attached estimate is based upon the information known to us at the time the AoB was executed.  We have documented over 40 cracked and broken tiles, as well as loose tiles in the field as well as on the hips and ridges, as shown in the photographs in the fifth attachment to this email.  We likewise documented broken mortar on about half of the hips and ridges.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>I look forward to working with you Ms. Dimery on resolving this claim to your policyholder’s satisfaction.  Should you have any questions, you can reach me directly at (239) 932-7200 or you may reply directly to this email as well.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-31 16:11:00',
            ),

            array(
                'claim_id' => 30,
                'claim_submission_type_id' => 1,
                "message_id" => "<286D402A-6132-4529-8640-C4DC512ED36D@romanroofinginc.com>",
                'to_recipients' => 'claims@thig.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim Number: 3300345405',
                'message_body' => '<p>To Whom It May Concern:</p><p>We are the roofing contractor engaged by our client, William and Patricia Davis, with an assignment of benefits (“AOB”) and who has submitted a claim to Tower Hill Insurance.  The date of loss occurred on September 10, 2017 at 945 Wal Harbor Blvd, Punta Gorda, FL 33950 (the “Property”).</p><p>To schedule inspections at the Property, please email us at claims@romanroofinginc.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. and not to our William and Patricia Davis.</p><p>Attached to this email you will find the following documents:</p><ol><li> Executed AOB between Roman Roofing, Inc. and William and Patricia Davis; and</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Insurer’s duties under Section 627.7013(5)(a), Fla Statute; and</li><li>Written, itemized, per-unit cost estimate of the services to be performed; and</li> <li>Ariel scope of measure for the Property; and</li><li>ITEL Report; and</li><li>Roman Roofing Inc.’s state license (CCC1330707); and</li><li>Signed W9 for Roman Roofing, Inc.; and</li></ol><p>We kindly ask that you email us the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your receipt of the AOB from CUSTOMER.</li></ol><p>The attached estimate is based upon the information known to us at the time the AOB was executed.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>We appreciate your assistance and look forward to working with you!</p><br><p>Best Wishes,<br>Lindsey Dopfer</p><p>Lindsey Dopfer<br>Claims Manager<br>Roman Roofing, Inc.<br>805 NE 7th Terrace<br>Cape Coral, FL 33909</p>',
                'type' => 'to',
                'created_at' => '2020-07-31 16:11:00',
            ),

            array(
                'claim_id' => 31,
                'claim_submission_type_id' => 1,
                "message_id" => "<09ECB43D-5B29-247A-8581-1D357C4B49D1@romanroofinginc.com>",
                'to_recipients' => 'claims@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'lindsey@romanroofinginc.com',
                'message_subject' => '20FL00045472',
                'message_body' => '<p>To Whom It May Concern:</p><p>Roman Roofing, Inc. (“Roman”) is the roofing contractor engaged by Kenneth C. Shivel and Marjorie Shivel (the “Assignor”), with an assignment of benefits (“AoB”). Below you will find the documents required for a claim—the claim number is indicated in the subject of this email.  Below is a list of attached documents:</p><ol><li>Executed AoB between Roman Roofing, Inc. and the Assignor; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Eight (8) photographs indicating apparent windstorm-related damage through various facades of the roofing structure on the insured property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the insured property, as provided by GAF QuickMeasure; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.</li></ol><p>Additionally there is a death certificate attached for the husband and a signed power of attorney for him as well.  They are document 10 and 11 respectively.</p><p>I kindly ask that you email me the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your timely receipt of the AOB from the Assignor.</li></ol><br><p>The attached estimate is based upon the information known to us at the time the AoB was executed.  We have documented over 40 cracked and broken tiles, as well as loose tiles in the field as well as on the hips and ridges, as shown in the photographs in the fifth attachment to this email.  We likewise documented broken mortar on about half of the hips and ridges.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</p><p>I look forward to working with you to resolve this claim to your policyholder’s satisfaction.  Please contact our office if you have further questions.  Our number is 239-458-7663.</p><br><p>Perri Williamson<br>Claims Specialist<br>Roman Roofing, Inc<br>805 NE 7th Terrace<br>Cape Coral, FL 33909<br>239.458.7663 Office</p>',
                'type' => 'to',
                'created_at' => '2020-08-04 11:50:00',
            ),
            array(
                'claim_id' => 32,
                'claim_submission_type_id' => 1,
                "message_id" => "<12FCB746-B0C5-4FD4-A957-32C683A14EFC@romanroofinginc.com>",
                'to_recipients' => 'claims@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'recipients' => 'claims@upcinsurance.com',
                'message_subject' => 'Claim #20FL00049283',
                'message_body' => '<p>To whom it may concern:</p><p>Roman Roofing, Inc. (“Roman”) wishes to officially let UPC Insurance know that we are the roofing contractor engaged by Brianne and Richard Romano (the “Assignor”), with an assignment of benefits (“AoB”) and who has previously submitted a claim to UPC Insurance as indicated in the subject of this email.  Attached to this email you will find the following documents:</p><ol><li>Executed AoB between Roman Roofing, Inc. and the Assignor; and</li><li>Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.</li><li>Notice of Deadline; and</li><li>Brochure from the Florida Department of Financial Services (regarding AOB contracts); and</li><li>Eight (8) photographs indicating apparent windstorm-related damage through various facades of the roofing structure on the insured property; and</li><li>Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and</li><li>Ariel scope of measure for the insured property, as provided by GAF QuickMeasure; and</li><li>Roman Roofing Inc’s state license (CCC1330707); and</li><li>Signed W-9 for Roman Roofing, Inc.</li></ol><p>I kindly ask that you email me the following materials:</p><ol><li>A copy of the declarations page for the date of loss; and</li><li>The most recent estimate for damages at the property; and</li><li>The deductible amount per the policy; and</li><li>Acknowledgement of your timely receipt of the AOB from the Assignor.</li></ol><P>The attached estimate is based upon the information known to us at the time the AoB was executed.  We have documented numerous broken and lifted tiles as shown in the photographs in the fifth attachment to this email.  We likewise documented broken mortar on about half of the hips and ridges.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.</P><p>We look forward to working with you to resolve this claim.  Should you have any questions, you can reach me our claims team (239) 458-7663 or you may reply directly to this email as well.</p><br><p>Best Regards,<br>Perri Williamson<br>Claims Specialist<br>Roman Roofing, Inc<br>805 NE 7th Terrace<br>Cape Coral, FL 33909<br>239.458.7663 Office</p> ',
                'type' => 'to',
                'created_at' => '2020-08-10 14:14:00',
            ),

            // Temp Repair Emails to Seed
            array(
                'claim_id' => 2,
                'claim_submission_type_id' => 2,
                "message_id" => "<12FCB746-B0C5-4FD4-A957-32C683A14EFC@romanroofinginc.com>",
                'to_recipients' => 'claims@upcinsurance.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim # 2017FL039867',
                'message_body' => '<p>To Whom It May Concern:</p><p>As you know, Roman Roofing (“Roman”) contacted you initially on June 3, 2020, to place UPC on notice that we are the roofing contractor engaged by our client, Donna Householder (the “Householder”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 1705 Coral Way, North Fort Myers, FL 33917 (the “Property”).  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol><p>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p><br><p>Best Regards,<br>Lindsey Dopfer</p><p>Lindsey Dopfer<br>Claims Manager<br>Roman Roofing, Inc.<br>805 NE 7th Terrace<br>Cape Coral, FL 33909</p>
                ',
                'type' => 'to',
                'created_at' => '2020-06-16 14:30:00',
            ),
            array(
                'claim_id' => 6,
                'claim_submission_type_id' => 2,
                "message_id" => "<1AABAFA88-3899-4281-84FB-8176DF8A72F2@romanroofinginc.com>",
                'to_recipients' => 'claims@asicorp.org',
                'cc_recipients' => NULL,
                'bcc_recipients' => 'gmurphy@romanroofinginc.com',
                'message_subject' => 'Claim # 766-326-201013',
                'message_body' => '<p>To Whom It May Concern:</p><p>As you know, Roman Roofing (“Roman”) contacted you initially on June 11, 2020, to place Progressive Insurance Company on notice that we are the roofing contractor engaged by our client, Craig and Abby Maclean (the “Macleans”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 8333 Creekview Ln, Englewood, FL 34224 (the “Property”).  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol><p>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p><br><p>Best Regards,<br>Lindsey Dopfer</p><p>Lindsey Dopfer<br>Claims Manager<br>Roman Roofing, Inc.<br>805 NE 7th Terrace<br>Cape Coral, FL 33909</p>',
                'type' => 'to',
                'created_at' => '2020-06-16 14:45:00',
            ),
            array(
                'claim_id' => 7,
                'claim_submission_type_id' => 2,
                "message_id" => "<1AABAFA88-3899-4281-84FB-8176DF8A72F2@romanroofinginc.com>",
                'to_recipients' => 'AssignmentofBenefits@Heritagepci.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim # HP 207715',
                'message_body' => '<p>To Whom It May Concern:</p><p>As you know, Roman Roofing (“Roman”) contacted you initially on 06/12/2020, to place Heritage on notice that we are the roofing contractor engaged by our client, Millard and Cathy Allen (the “Allens”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 14965 Toscana Way, Naples, FL 34120 (the “Property”).  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol><p>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p><p>Best Regards,<br>Lindsey Dopfer</p><p>Claims Manager<br>Roman Roofing, Inc.<br>805 NE 7th Terrace<br>Cape Coral, FL 33909</p>',
                'type' => 'to',
                'created_at' => '2020-06-19 14:24:00',
            ),
            array(
                'claim_id' => 8,
                'claim_submission_type_id' => 2,
                "message_id" => "<1AABAFA88-3899-4281-84FB-8176DF8A72F2@romanroofinginc.com>",
                'to_recipients' => 'claims@thig.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #3300341576',
                'message_body' => '<p>To Whom It May Concern:</p><p>As you know, Roman Roofing (“Roman”) contacted you initially on 06/15/2020, to place Tower Hill Insurance on notice that we are the roofing contractor engaged by our client, Bruce Kringle (“Kringle”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 1302 Northeast 6th Avenue in Cape Coral, Florida 33909 (the “Property”).  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol><p>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-10 04:03:00',
            ),
            array(
                'claim_id' => 1,
                'claim_submission_type_id' => 2,
                "message_id" => "<1AABAFA88-3899-4281-84FB-8176DF8A72F2@romanroofinginc.com>",
                'to_recipients' => 'claims@thig.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim #3300341576',
                'message_body' => '<p>To Whom It May Concern:</p><p>As you know, Roman Roofing (“Roman”) contacted you initially on 05/21/2020 to place State Farm on notice that we are the roofing contractor engaged by our client, Maria Morales (“Morales”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 2734 Blue Cypress Lake Ct, Cape Coral, FL 33909 (the "Property").  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment. If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.<br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-10 04:16:00',
            ),
            array(
                'claim_id' => 11,
                'claim_submission_type_id' => 2,
                "message_id" => "<1AABAFA88-3899-4281-84FB-8176DF8A72F2@romanroofinginc.com>",
                'to_recipients' => 'claims@thig.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => '01000031623',
                'message_body' => '<p>At 4:38 EST on 07/10/2020, I uploaded 2 documents to Frontline Insurance per the letter and the screen capture attached hereto.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p> ',
                'type' => 'to',
                'created_at' => '2020-07-10 04:16:00',
            ),
            array(
                'claim_id' => 4,
                'claim_submission_type_id' => 2,
                "message_id" => "<0c2c01d63acf$1d548d10$57fda730$@romanroofinginc.com>",
                'to_recipients' => 'claimshelp@universalproperty.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'CLAIM NUMBER: BPG7884H01',
                'message_body' => '<p>To Whom It May Concern:<p>As you know, Roman Roofing (“Roman”) contacted you initially on 05/21/2020 to place UPC Insurance on notice that we are the roofing contractor engaged by our client, Linda Crawford (“Crawford”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 4727 Orange Grove Blvd, North Fort Myers, FL 33903 (the “Property”).  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><p>Greetings – this email is to officially put you on notice that we are the roofing contractor engaged by our client, Jerry Shaffner (“Shaffner”), with an assignment of benefits (“AOB”) and who has submitted a claim to UPC Insurance as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 750 Northeast 5th Place in Cape Coral (the “Property”).  To schedule inspections at the Property, please email us at claims@romanroofing.com.  Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofing.com or 239-458-7663.</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol><p>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-10 04:27:00',
            ),
            array(
                'claim_id' => 26,
                'claim_submission_type_id' => 2,
                "message_id" => "<0c2c01d63acf$1d548d10$57fda730$@romanroofinginc.com>",
                'to_recipients' => 'claimshelp@universalproperty.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'CLAIM NUMBER: BPG7884H01',
                'message_body' => '<p>To Whom It May Concern:</p><p>As you know, Roman Roofing (“Roman”) contacted you initially on 06/24/2020 to place UPC Insurance on notice that we are the roofing contractor engaged by our client, Jerry Shaffner (“Shaffner”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 4727 Orange Grove Blvd, North Fort Myers, FL 33903 (the “Property”).  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol><p>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-10 04:39:00',
            ),
            array(
                'claim_id' => 25,
                'claim_submission_type_id' => 2,
                "message_id" => "<0c2c01d63acf$1d548d10$57fda730$@romanroofinginc.com>",
                'to_recipients' => 'claimshelp@universalproperty.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'CLAIM NUMBER: BPG7884H01',
                'message_body' => '<p>To Whom It May Concern:</p><p>As you know, Roman Roofing (“Roman”) contacted you initially on 06/23/2020 to place Florida Penninsula Insurance on notice that we are the roofing contractor engaged by our client, Marc Brenscheidt (“Brenscheidt”), who asserted a claim as indicated in the subject of this email.  The date of loss occurred on September 10, 2017 at 1807 SW 29th Terrace in Cape Coral, Florida (the “Property”).  This email is to follow-up and put you on further notice that Roman has completed temporary repairs to the roof of the Property that we deemed necessary to preserve the other elements of the structure as well as the contents therein.  We have documented the relevant portions of the Property with photographic evidence of the damage and the work that was performed by Roman.  Accordingly, please refer to the following two (2) attachments:</p><ol><li>Photographs of damage and the work performed by Roman; and</li><li>An invoice for the roofing services completed by Roman.</li></ol><p>At this time, we request you update the aforementioned claim to account for the temporary repairs performed by Roman at the Property and remit the invoice to your accounts payable department for prompt payment.   If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.  Thank you in advance for timely payment of the attached invoice.  Should you have any questions, please do not hesitate to contact us at 239-458-7663.</p><br><p>Best Regards,<br>Gabriel C. Murphy,</p><p>Gabriel C. Murphy,<br>Senior Claims Manager,<br>Roman Roofing, Inc.,<br>805 NE 7th Terrace,<br>Cape Coral, FL 33909,</p><p>This message may contain confidential and/or proprietary information. It is intended solely for the person/entity to whom it was originally addressed. The content of this message may contain private views and opinions which do not constitute a formal disclosure or commitment unless specifically stated. If you are not the intended recipient, you should delete this communication and are hereby notified any disclosure, copying, printing, retention or distribution of this message, or the taking of any action based on it, is strictly prohibited.</p><p> Please consider the environment before printing this email</p>',
                'type' => 'to',
                'created_at' => '2020-07-10 04:59:00',
            ),
            array(
                'claim_id' => 33,
                'claim_submission_type_id' => 1,
                "message_id" => "<2C4BFF5E-389F-4164-824F-536EEA4DFB1B@romanroofinginc.com>",
                'to_recipients' => 'dispatch@seibels.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'Claim Number: ST 17230133',
                'message_body' => '<p>
To Whom It May Concern:
We are the roofing contractor engaged by Erika Patalano (the “Customer”), with an assignment of benefits (“AOB”) and who has submitted a claim to St. John’s Insurance as indicated in the subject of this email. The loss occurred on September 10, 2017 at 1516 NW 28th Ave, Cape Coral, FL 33993 (the “Property”). To schedule inspections at the Property, please email us at claims@romanroofinginc.com. Please direct all other communications regarding this claim to Roman Roofing, Inc. at claims@romanroofinginc.com or 239-458-7663.
Attached to this email you will find the following documents:
1.	Executed AOB between Roman Roofing, Inc. and the Customer; and
2.	Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.
3.	Notice of Deadline; and
4.	Brochure from the Florida Department of Financial Services (regarding AOB contracts); and
5.	Photographs indicating apparent windstorm-related damage the roofing structure on the Property; and
6.	Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and
7.	Ariel scope of measure for the Property, as provided by RoofScope; and
8.	Roman Roofing Inc’s state license (CCC1330707)
We kindly ask that you email us the following materials:
1.	A copy of the declarations page for the date of loss; and
2.	The most recent estimate for damages at the property; and
3.	The deductible amount per the policy; and
4.	Acknowledgement of your receipt of the AOB from the Customer.
The attached estimate is based upon the information known to us at the time the AOB was executed. While we try to be thorough, there will always be discoveries that will impact the estimate but are not immediately evident. As we learn more about the loss, we will update the estimate accordingly. If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.
We appreciate your assistance and look forward to working with you.

Best Regards,
Perri Williamson
Claims Specialist
Roman Roofing, Inc
805 NE 7th Terrace
Cape Coral, FL 33909
239.458.7663 Office</p>',
                'type' => 'to',
                'created_at' => '2020-08-19 12:13:00',
            ),
            array(
                'claim_id' => 34,
                'claim_submission_type_id' => 1,
                "message_id" => " <DCD074E1-5143-46BC-A3B0-0B29E523D17E@romanroofinginc.com>",
                'to_recipients' => 'assignmentofbenefits@heritagepci.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'CLAIM# H266078',
                'message_body' => '<p>
To whom it may concern:

Roman Roofing, Inc. (“Roman”) wishes to officially let %%CARRIER%% know that we are the roofing contractor engaged, Ronald Anderson and Deborah Johnson (the “Assignor”), with an assignment of benefits (“AoB”) and who has previously submitted a claim to Heritage Insurance as indicated in the subject of this email.  Attached to this email you will find the following documents:
1.	Executed AoB between Roman Roofing, Inc. and the Assignor; and
2.	Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.
3.	Notice of Deadline; and
4.	Brochure from the Florida Department of Financial Services (regarding AOB contracts); and
5.	Eight (8) photographs indicating apparent windstorm-related damage through various facades of the roofing structure on the insured property; and
6.	Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and
7.	Ariel scope of measure for the insured property, as provided by GAF QuickMeasure; and
8.	Roman Roofing Inc’s state license (CCC1330707); and
9.	Signed W-9 for Roman Roofing, Inc.

I kindly ask that you email me the following materials:
1.	A copy of the declarations page for the date of loss; and
2.	The most recent estimate for damages at the property; and
3.	The deductible amount per the policy; and
4.	Acknowledgement of your timely receipt of the AOB from the Assignor.

The attached estimate is based upon the information known to us at the time the AoB was executed.  We have documented numerous broken and lifted tiles as shown in the photographs in the fifth attachment to this email.  We likewise documented broken mortar on about half of the hips and ridges.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.

We look forward to working with you to resolve this claim.  Should you have any questions, you can reach me our claims team (239) 458-7663 or you may reply directly to this email as well.


Best Regards,


Perri Williamson
Claims Specialist
Roman Roofing, Inc
805 NE 7th Terrace
Cape Coral, FL 33909
239.458.7663 Office

</p>',
                'type' => 'to',
                'created_at' => '2020-08-19 12:13:00',
            ),
            array(
                'claim_id' => 35,
                'claim_submission_type_id' => 1,
                "message_id" => " <DCD074E1-5143-46BC-A3B0-0B29E523D17E@romanroofinginc.com>",
                'to_recipients' => 'assignmentofbenefits@heritagepci.com',
                'cc_recipients' => NULL,
                'bcc_recipients' => NULL,
                'message_subject' => 'CLAIM# H266078',
                'message_body' => '<p>
To whom it may concern:

Roman Roofing, Inc. (“Roman”) wishes to officially let %%CARRIER%% know that we are the roofing contractor engaged, Ronald Anderson and Deborah Johnson (the “Assignor”), with an assignment of benefits (“AoB”) and who has previously submitted a claim to Heritage Insurance as indicated in the subject of this email.  Attached to this email you will find the following documents:
1.	Executed AoB between Roman Roofing, Inc. and the Assignor; and
2.	Duties you are obligated to follow pursuant to §627.70131(5)(a), Fla. Stat.
3.	Notice of Deadline; and
4.	Brochure from the Florida Department of Financial Services (regarding AOB contracts); and
5.	Eight (8) photographs indicating apparent windstorm-related damage through various facades of the roofing structure on the insured property; and
6.	Written, itemized, per-unit cost estimate via Xactimate of the services to be performed by Roman Roofing; and
7.	Ariel scope of measure for the insured property, as provided by GAF QuickMeasure; and
8.	Roman Roofing Inc’s state license (CCC1330707); and
9.	Signed W-9 for Roman Roofing, Inc.

I kindly ask that you email me the following materials:
1.	A copy of the declarations page for the date of loss; and
2.	The most recent estimate for damages at the property; and
3.	The deductible amount per the policy; and
4.	Acknowledgement of your timely receipt of the AOB from the Assignor.

The attached estimate is based upon the information known to us at the time the AoB was executed.  We have documented numerous broken and lifted tiles as shown in the photographs in the fifth attachment to this email.  We likewise documented broken mortar on about half of the hips and ridges.  While we try to be thorough, there will always be situations in which information that would impact the estimate is not immediately available to us, and as we learn more about the loss, we will update the estimate accordingly.  If you believe we have overlooked any aspect of the loss, we would appreciate if you would share that with us.

We look forward to working with you to resolve this claim.  Should you have any questions, you can reach me our claims team (239) 458-7663 or you may reply directly to this email as well.


Best Regards,


Perri Williamson
Claims Specialist
Roman Roofing, Inc
805 NE 7th Terrace
Cape Coral, FL 33909
239.458.7663 Office
</p>',
                'type' => 'to',
                'created_at' => '2020-08-21 16:17:00',
            ),
        );

        foreach ($emails as $email) {

            $odmail = \App\Odmail::create([
                'mailable_type' => \App\MaximusClaim::class,
                'mailable_id' => $email['claim_id'],
                'template_id' => $email['claim_submission_type_id'] == 1 ? 3 : 4,
                'from' => 'claims@romanroofing.com',
                'subject' => $email['message_subject'],
                'body' => $email['message_body'],
                'created_at' => $email['created_at'],
                'updated_at' => $email['created_at'],
            ]);

            $to_recipients = explode(';', $email['to_recipients']);
            $cc_recipients = explode(';', $email['cc_recipients']);
            $bcc_recipients = explode(';', $email['bcc_recipients']);

            foreach ($to_recipients as $recipient) {
                if ($recipient) $odmail->recipients()->create(['type' => 'to', 'email' => $recipient,]);
            }
            foreach ($cc_recipients as $recipient) {
                if ($recipient) $odmail->recipients()->create(['type' => 'cc', 'email' => $recipient,]);
            }
            foreach ($bcc_recipients as $recipient) {
                if ($recipient) $odmail->recipients()->create(['type' => 'bcc', 'email' => $recipient,]);
            }

            $claim_id = $email['claim_id'];

            if ($email['claim_submission_type_id'] == 1) {
                $dir = public_path("storage/maximus/$claim_id/aob/email_attachments");
            } else {
                $dir = public_path("storage/maximus/$claim_id/temp_repair/email_attachments");
            }

            $email_attachments = is_dir($dir) ? File::files($dir) : array();

            foreach ($email_attachments as $attachment) {
                $path = str_replace('\\', '/', explode('public' . DIRECTORY_SEPARATOR, $attachment->getPathname())[1]);

                $odmail->addMedia(public_path($path))->preservingOriginal()->toMediaCollection();
            }
        }
    }
}
