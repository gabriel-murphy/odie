<?php

use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* This is the address for the headquarters of Roman Roofing (client ID = 1) and any
    future locations of clients of the Platform */

    public function run()
    {
        $addresses = array(
            array(
                "id" => 1,
                "client_id" => 1,
                "addressable_type" => "App\\Client",
                "addressable_id" => 1,                                  // ??? about this
                "street" => "805 Northeast 7th Terrace",
                "city" => "Cape Coral",
                "zip_code" => "33909",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '37.86926',
                'longitude' => '-122.254811',
                "deleted_at" => NULL,
                "created_at" => "2020-01-01 12:00:00",
            ),
            array(
                "id" => 2,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 2,
                "street" => "2734 Blue Cypress Lake Court",             // MaximusClaimID = 1
                "city" => "Cape Coral",
                "zip_code" => "33909",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.711450',
                'longitude' => '-81.953280',
                "deleted_at" => NULL,
                "created_at" => "2020-02-06 09:23:00",
            ),
            array(
                "id" => 3,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 3,
                "street" => "1705 Coral Way",             // MaximusClaimID = 2
                "city" => "North Fort Myers",
                "zip_code" => "33917",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.698530',
                'longitude' => '-81.833328',
                "deleted_at" => NULL,
                "created_at" => "2020-06-02 05:18:00",
            ),
            array(
                "id" => 4,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 4,
                "street" => "1104 Southeast 46th Lane",             // MaximusClaimID = 3
                "city" => "Cape Coral",
                "zip_code" => "33904",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.565270',
                'longitude' => '-81.950920',
                "deleted_at" => NULL,
                "created_at" => NOW(),      // Not in GiddyUp
            ),
            array(
                "id" => 5,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 5,
                "street" => "4727 Orange Grove",             // MaximusClaimID = 4
                "city" => "North Fort Myers",
                "zip_code" => "33903",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.659650',
                'longitude' => '-81.915500',
                "deleted_at" => NULL,
                "created_at" => "2020-01-18 11:47:00",
            ),
            array(
                "id" => 6,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 6,
                "street" => "2664 Clairfont Court",             // MaximusClaimID = 5
                "city" => "Cape Coral",
                "zip_code" => "33991",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.624130',
                'longitude' => '-82.029400',
                "deleted_at" => NULL,
                "created_at" => "2020-01-18 11:47:00",
            ),
            array(
                "id" => 7,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 7,
                "street" => "8333 Creekview Lane",             // MaximusClaimID = 6
                "city" => "Englewood",
                "zip_code" => "34224",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.9288973',
                'longitude' => '-82.31014859999999',
                "deleted_at" => NULL,
                "created_at" => "2020-06-04 09:47:00",
            ),
            array(
                "id" => 8,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 8,
                "street" => "14965 Toscana Way",             // MaximusClaimID = 7
                "city" => "Naples",
                "zip_code" => "34120",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.269398',
                'longitude' => '-81.68645599999999',
                "deleted_at" => NULL,
                "created_at" => "2020-06-09 11:30:00",
            ),
            array(
                "id" => 9,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 9,
                "street" => "1302 Northeast 6th Avenue",             // MaximusClaimID = 8
                "city" => "Cape Coral",
                "zip_code" => "33909",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.676711',
                'longitude' => '-81.9612592',
                "deleted_at" => NULL,
                "created_at" => "2020-06-08 11:40:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 10,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 10,
                "street" => "19431 Lauzon Ave.",             // MaximusClaimID = 9
                "city" => "Port Charlotte",
                "zip_code" => "33948",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.969103',
                'longitude' => '-82.125902',
                "deleted_at" => NULL,
                "created_at" => "2020-06-08 09:41:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 11,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 11,
                "street" => "5104 97th St E",             // MaximusClaimID = 10
                "city" => "Bradenton",
                "zip_code" => "34211",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '27.4489392',
                'longitude' => '-82.4443835',
                "deleted_at" => NULL,
                "created_at" => "2020-06-08 10:29:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 12,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 12,
                "street" => "2720 SE 22nd Avenue",             // MaximusClaimID = 11
                "city" => "Cape Coral",
                "zip_code" => "33904",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6014578',
                'longitude' => '-81.9269247',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 13,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 13,
                "street" => "23212 Salinas Way",             // MaximusClaimID = 12
                "city" => "Bonita Springs",
                "zip_code" => "34135",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.3807747',
                'longitude' => '-81.7764378',
                "deleted_at" => NULL,
                "created_at" => "2020-06-25 12:28:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 14,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 14,
                "street" => "19022 Ridgepoint Drive",             // MaximusClaimID = 13
                "city" => "Estero",
                "zip_code" => "33928",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.4324612',
                'longitude' => '-81.8383365',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 15,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 15,
                "street" => "19029 Ridgepoint Drive",             // MaximusClaimID = 14
                "city" => "Estero",
                "zip_code" => "33928",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.4321723',
                'longitude' => '-81.8379345',
                "deleted_at" => NULL,
                "created_at" => "2020-04-21 09:02:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 16,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 16,
                "street" => "2314 SW 44th Terrace",             // MaximusClaimID = 15
                "city" => "Cape Coral",
                "zip_code" => "33914",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.5682176',
                'longitude' => '-82.02261279999999',
                "deleted_at" => NULL,
                "created_at" => "2020-06-11 09:35:00",          // Timestamp from GiddyUp
            ),
            array(
                "id" => 17,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 17,
                "street" => "11684 LadyAnne Circle",             // MaximusClaimID = 16
                "city" => "Cape Coral",
                "zip_code" => "33991",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6183485',
                'longitude' => '-82.0353286',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 18,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 18,
                "street" => "3827 Palm Tree Boulevard",             // MaximusClaimID = 17
                "city" => "Cape Coral",
                "zip_code" => "33904",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.5808039',
                'longitude' => '-81.96511319999999',
                "deleted_at" => NULL,
                "created_at" => "2020-06-19 11:26:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 19,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 19,
                "street" => "11764 LadyAnne Circle",             // MaximusClaimID = 18
                "city" => "Cape Coral",
                "zip_code" => "33991",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6203083',
                'longitude' => '-82.0337641',
                "deleted_at" => NULL,
                "created_at" => "2020-07-09 05:20:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 20,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 20,
                "street" => "1233 Villagio Way",             // MaximusClaimID = 19
                "city" => "Fort Myers",
                "zip_code" => "33912",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.5647064',
                'longitude' => '-81.800102',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 21,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 21,
                "street" => "4213 Garden Blvd",             // MaximusClaimID = 20
                "city" => "Cape Coral",
                "zip_code" => "33909",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.736797',
                'longitude' => '-81.9257525',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 22,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 22,
                "street" => "2427 NW 25th Street",             // MaximusClaimID = 21
                "city" => "Cape Coral",
                "zip_code" => "33993",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6990343',
                'longitude' => '-82.0248984',
                "deleted_at" => NULL,
                "created_at" => "2020-01-23 15:33:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 23,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 23,
                "street" => "2490 Verdmont Court",             // MaximusClaimID = 22
                "city" => "Cape Coral",
                "zip_code" => "33991",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6114228',
                'longitude' => '-82.02462039999999',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 24,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 24,
                "street" => "1807 SW 29th Terrace",             // MaximusClaimID = 23
                "city" => "Cape Coral",
                "zip_code" => "33914",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.5980356',
                'longitude' => '-82.0121448',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 25,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 25,
                "street" => "24924 Fairwinds Lane",           // MaximusClaimID = 24
                "city" => "Bonita Springs",
                "zip_code" => "34135",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.376673',
                'longitude' => '-81.7847256',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 26,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 26,
                "street" => "1051 Romano Key Circle",             // MaximusClaimID = 25
                "city" => "Punta Gorda",
                "zip_code" => "33955",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.7639437',
                'longitude' => '-82.04714109999999',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 27,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 27,
                "street" => "750 NE 5th Place",             // MaximusClaimID = 26
                "city" => "Cape Coral",
                "zip_code" => "33909",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6670963',
                'longitude' => '-81.9627081',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 28,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 28,
                "street" => "9311 Old Hickory Cr",          // MaximusClaimID = 27
                "city" => "Fort Myers",
                "zip_code" => "33912",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.53749239999999',
                'longitude' => '-81.79926979999999',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 29,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 29,
                "street" => "20018 Seadale Court",             // MaximusClaimID = 28
                "city" => "Estero",
                "zip_code" => "33928",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.4483989',
                'longitude' => '-81.770839',
                "deleted_at" => NULL,
                "created_at" => "2020-06-17 15:39:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 30,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 30,
                "street" => "11686 Royal Tee Circle",             // MaximusClaimID = 29
                "city" => "Cape Coral",
                "zip_code" => "33991",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6177333',
                'longitude' => '-82.03741459999999',
                "deleted_at" => NULL,
                "created_at" => "2020-07-14 17:32:00",      // Timestamp from GiddyUp
            ),
            array(
                "id" => 31,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 31,
                "street" => "945 Bal Harbor Boulevard",             // MaximusClaimID = 31
                "city" => "Punta Gorda",
                "zip_code" => "33950",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.9125985',
                'longitude' => '-82.0710795',
                "deleted_at" => NULL,
                "created_at" => "2020-07-22 13:16:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 32,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 32,
                "street" => "27482 Pelican Ridge Circle",       // MaximusClaimID = 32
                "city" => "Bonita Springs",
                "zip_code" => "34135",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.335731',
                'longitude' => '-81.7961519',
                "deleted_at" => NULL,
                "created_at" => "2020-07-18 15:13:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 33,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 33,
                "street" => "2661 Bellingham Court",       // MaximusClaimID = 33
                "city" => "Cape Coral",
                "zip_code" => "33991",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "latitude" => '26.6156251',
                'longitude' => '-82.030007',
                "deleted_at" => NULL,
                "created_at" => "2020-07-30 15:11:00",       // Timestamp from GiddyUp
            ),
            array(
                "id" => 34,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 34,
                "street" => "1516 NW 28th Avenue",       // MaximusClaimID = 34
                "city" => "Cape Coral",
                "zip_code" => "33993",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "deleted_at" => NULL,
                "created_at" => "2020-06-11 15:54:00",          // Timestamp from GiddyUp
            ),
            array(
                "id" => 35,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 35,
                "street" => "1925 SE 44th Avenue",
                "city" => "Cape Coral",
                "zip_code" => "33909",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "deleted_at" => NULL,
                "created_at" => "2020-07-28 10:32:00",          // Timestamp from GiddyUp
            ),
            array(
                "id" => 36,
                "client_id" => 1,
                "addressable_type" => "App\\Property",
                "addressable_id" => 36,
                "street" => "8355 Whispering Woods Court",      // MaximusClaimID = 36
                "city" => "Bradenton",
                "zip_code" => "34202",
                "county" => "Lee",
                "state" => "FL",
                "country" => "USA",
                "deleted_at" => NULL,
                "created_at" => "2020-07-28 10:32:00",          // Timestamp from GiddyUp
            ),
        );

        foreach ($addresses as $address) {
            \App\Address::create($address);
        }
    }
}
