<?php

use Illuminate\Database\Seeder;

class RoofProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roof_products = array(
            [
                "name" => "Timberline",                     // roof_product_id = 1
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles?Action=GetGrid&Filters_300=b433507505544e2ea82c863c51328202,500f9a5134f14c969813c0fbbcfa7059,21fe8b7172e24cdab37e728b7f00eaf2&ZipCode=33912",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Designer",
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles?Action=GetGrid&Filters_300=b9901c2e2cb54e92a1423698254fdcab,bd623fad509e4add909ac1562747c41c,e4e65061546743fd836f362cbeff2023&ZipCode=33912",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Three-Tab",                     // roof_product_id = 3
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles?Action=GetGrid&Filters_300=46bcc31dd796410ca32ea0655abc489d,9961280edbab4ba796203bf59689c9eb",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 6,                        // Three-Tab Shingle
            ],
            [
                "name" => "Architectural",
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Slate Look",                     // roof_product_id = 5
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Wood Shake Look",                     // roof_product_id = 5
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Three-Tab",
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 6,                        // Three-Tab Shingle
            ],
            [
                "name" => "Designer",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/designer/",
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Performance",                    // roof_product_id = 8
                "url" => "https://www.iko.com/na/residential-roofing-shingles/performance/",
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Architectural",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/architectural/",
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Traditional",                    // roof_product_id = 10
                "url" => "https://www.iko.com/na/residential-roofing-shingles/three-tab/",
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 6,                        // Three-Tab Shingle
            ],
            [
                "name" => "Heritage",
                "url" => "https://www.tamko.com/styles/heritage",
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 5,                        // Dimensional Shingle
            ],
            [
                "name" => "Three-Tab",
                "url" => "https://www.tamko.com/styles/elite-glass-seal",
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 6,                        // Three-Tab Shingle
            ],
            [
                "name" => "Shake",                          // roof_product_id = 13
                "url" => "https://www.certainteed.com/residential-roofing/products/?f[0]=field_residential_roofing_style_:634",
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Shingle
            ],
            [
                "name" => "Slate",
                "url" => "https://www.certainteed.com/residential-roofing/products/?f[0]=field_residential_roofing_style_:635",
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Shingle
            ],
            [
                "name" => "Three-Tab",                      // roof_product_id = 15
                "url" => "https://www.certainteed.com/residential-roofing/products/?f[0]=field_residential_roofing_style_:637",
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 6,                        // Three-Tab Shingle
            ],

            [
                "name" => "Boosted Barcelona Caps",
                "code" => "1BLCS0141K",
                "url" => "https://www.boralroof.com/product-profile/concrete/boosted-barcelona-caps/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/2018/10/1BECSS0141K-Boosted-Buckskin.jpeg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/Boosted-Barcelona-Caps-Buckskin-fla.pdf",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/Boosted-Barcelona-Caps-Buckskin-fls.pdf",
                ],
                "specs" => "https://www.boralroof.com/wp-content/uploads/2018/08/1BECSS0141K-Boosted-Buckskin.pdf",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Saxony 900 Split Slate",
                "code" => "1FCCS3425AA",
                "url" => "https://www.boralroof.com/product-profile/concrete/saxony-900-split-slate/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FCCS3425AA-SplitSlate900-BrownSugar-BA-2.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FCCW3425AA-SplitSlate900-BrownSugar-BA-str-2.jpg",
                ],
                "specs" => "https://www.boralroof.com/wp-content/uploads/2018/08/1BECSS0141K-Boosted-Buckskin.pdf",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Saxony 900 Slate",
                "code" => "1FCCS3425AA",
                "url" => "https://www.boralroof.com/product-profile/concrete/saxony-900-slate/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/2018/10/1FMCS0150-Slate900-AshenBlend.jpeg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FMCS0150-Slate-AshenBlend-str.jpg",
                ],
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Saxony 900 Shake",
                "code" => "1FNCF3233",
                "url" => "https://www.boralroof.com/product-profile/concrete/saxony-900-shake/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FNCF3233-Shake900-BrownBlend.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FNCF3233-Shake-BrownBlend_str.jpg",
                ],
                "specs" => "https://www.boralroof.com/wp-content/uploads/product-selector/1FNCF3233-Shake900-BrownBlend.pdf",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Saxony 900 Split Shake",         // roof_product_id = 20
                "code" => "1FPCV1132",
                "url" => "https://www.boralroof.com/product-profile/concrete/saxony-900-split-shake/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FPCV1132-SplitShake900-CharcoalBrownBlend.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FPCV1132-SplitShake-Charcoal-Brown-Blend_str.jpg",
                ],
                "specs" => "https://www.boralroof.com/wp-content/uploads/2018/08/1FPCV1132-SplitShake900-CharcoalBrownBlend.pdf",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Plantation Slate Like",
                "code" => "1GCCW3425AA",
                "url" => "https://www.boralroof.com/product-profile/concrete/plantation-slate-like/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GCCS3425AA-Plantation-SL-BrownSugar-BA.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GCCW3425AA-Plantation-SL-BrownSugar-BA-str.jpg",
                ],
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Bermuda Smooth",
                "code" => "1GKWU8006NN",
                "url" => "https://www.boralroof.com/product-profile/concrete/bermuda-smooth/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GKWU8006NN-Bermuda-SM-BocaWhite.jpg",
                ],
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Bermuda Broom Swept",
                "code" => "1GLCR0308NN",
                "url" => "https://www.boralroof.com/product-profile/concrete/bermuda-broom-swept/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GLCR0308NN-Bermuda-BS-CedarTan-1.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GLCR0308NN-Bermuda-BS-CedarTan-str-1.jpg",
                ],
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Bermuda Rustic Shake",
                "code" => "1GMC53442AA",
                "url" => "https://www.boralroof.com/product-profile/concrete/bermuda-rustic-shake/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GMC53442AA-Bermuda-Lignite-BA-RS.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GMC53442AA-Bermuda-Lignite-BA-RS-str.jpg",
                ],
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Estate",                         // roof_product_id = 25
                "code" => "1GOCS0301CD",
                "url" => "https://www.boralroof.com/product-profile/concrete/estate/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GOCS0301CD-Estate-Antigua-BBA.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GOCS0301CD-Estate-Antigua-BBA-str.jpg",
                ],
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Galena",
                "code" => "1GQCS6265",
                "url" => "https://www.boralroof.com/product-profile/concrete/galena/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1GQCS6265-Galena-Bayside-Blend.jpg",
                ],
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Barcelona 900",
                "code" => "1HBCS6259",
                "url" => "https://www.boralroof.com/product-profile/concrete/barcelona-900/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/2018/10/1HBCS6259-Barcelona900-AvalonBeachBlend.jpeg",
                    "https://www.boralroof.com/wp-content/uploads/2018/10/1HBCS6259-Barcelona900-AvalonBeach-str.jpeg",
                ],
                "specs" => "https://www.boralroof.com/wp-content/uploads/2018/08/1HBCS6259-Barcelona900-AvalonBeachBlend.pdf",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Madera 900",
                "code" => "1MDCL3002",
                "url" => "https://www.boralroof.com/product-profile/concrete/madera-900/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FNCF3233-Shake900-BrownBlend.jpg",
                    "https://www.boralroof.com/wp-content/uploads/product-selector/1FNCF3233-Shake-BrownBlend_str.jpg",
                ],
                "specs" => "https://www.boralroof.com/wp-content/uploads/product-selector/1FNCF3233-Shake900-BrownBlend.pdf",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Villa 900",
                "code" => "1MPSS0555",
                "url" => "https://www.boralroof.com/product-profile/concrete/villa-900/",
                "samples" => [
                    "https://www.boralroof.com/wp-content/uploads/2018/10/1MPSS0555-Villa900-AmberSand.jpeg",
                    "https://www.boralroof.com/wp-content/uploads/2018/10/1MPSS0555-Villa900-AmberSand_str.jpeg",
                ],
                "specs" => "https://www.boralroof.com/wp-content/uploads/2018/08/1MPSS0555-Villa900-AmberSand.pdf",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Bel Air",                        // roof_product_id = 30
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=bel-air&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Capistrano",
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=capistrano&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Double Eagle Bel Air",
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=double-eagle-bel-air&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Golden Eagle",
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Malibu",
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=malibu&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Ponderosa",                      // roof_product_id = 35
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=ponderosa&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Textured Slate",
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=textured-slate&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "Tapered Slate",
                "url" => "https://eagleroofing.com/browse-tile/tile-results/?region=florida&profile=tapered-slate&pg=1",
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
            ],

            [
                "name" => "Florida Signature Series",       // roof_product_id = 38
                "url" => "https://www.crownrooftiles.com/signature_series_fl.html",
                "roof_manufacturer_id" => 8,                // Crown Tile
                "roof_type_id" => 7,                        // Concrete Tile
            ],
            [
                "name" => "US Tile by Boral",
                "url" => "https://www.boralroof.com/product-profile/clay/",
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 8,                        // Clay Tile
            ],

            [
                "name" => 'Spanish "S"',                    // roof_product_id = 40
                "url" => "https://www.santafetile.com/clay-tile/spanish-s/",
                "roof_manufacturer_id" => 9,                // Santafé
                "roof_type_id" => 8,                        // Clay Tile
            ],
            [
                "name" => 'Báltica',
                "url" => "https://www.santafetile.com/clay-tile/baltica-collection/",
                "roof_manufacturer_id" => 9,                // Santafé
                "roof_type_id" => 8,                        // Clay Tile
            ],
            [
                "name" => "Imperial",
                "url" => "https://www.santafetile.com/clay-tile/imperial/",
                "roof_manufacturer_id" => 9,                // Santafé
                "roof_type_id" => 8,                        // Clay Tile
            ],
            [
                "name" => "Mission Barrel",
                "url" => "https://www.santafetile.com/clay-tile/mission-barrel/",
                "roof_manufacturer_id" => 9,                // Santafé
                "roof_type_id" => 8,                        // Clay Tile
            ],
            [
                "name" => "Artisan Blend",
                "url" => "https://www.santafetile.com/clay-tile/artisan/",
                "roof_manufacturer_id" => 9,                // Santafé
                "roof_type_id" => 8,                        // Clay Tile
            ],

            [
                "name" => "Florida Clay Series",            // roof_product_id = 45
                "url" => "https://www.crownrooftiles.com/fl_clay_series.html",
                "roof_manufacturer_id" => 8,                // Crown Tile
                "roof_type_id" => 8,                        // Clay Tile
            ],

            [
                "name" => "TruSlate®",
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/slate-roofing/truslate",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 9,                        // Slate Tile
            ],

            [
                "name" => "Hidden Fastener",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#hidden-fastener",
                "roof_manufacturer_id" => 10,               // Gulf Coast Media
                "roof_type_id" => 10,                       // Aluminum
            ],
            [
                "name" => "Exposed Fastener",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#exposed-fastener",
                "roof_manufacturer_id" => 10,               // Gulf Coast Media
                "roof_type_id" => 10,                       // Aluminum
            ],
            [
                "name" => "Stamped",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#stamped-metal",
                "roof_manufacturer_id" => 10,               // Gulf Coast Media
                "roof_type_id" => 10,                       // Aluminum
            ],

            [
                "name" => "MetalWorks",                     // roof_product_id = 50
                "url" => "https://www.tamko.com/docs/default-source/brochures-literature/TAMKO-metalworks-brochure.pdf?sfvrsn=a8b356a0_6",
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 11,                       // Steel
            ],

            [
                "name" => "Hidden Fastener",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#hidden-fastener",
                "roof_manufacturer_id" => 10,               // Gulf Coast Media
                "roof_type_id" => 11,                       // Steel
            ],
            [
                "name" => "Exposed Fastener",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#exposed-fastener",
                "roof_manufacturer_id" => 10,               // Gulf Coast Media
                "roof_type_id" => 11,                       // Steel
            ],
            [
                "name" => "Stamped",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#stamped-metal",
                "roof_manufacturer_id" => 10,               // Gulf Coast Media
                "roof_type_id" => 11,                       // Steel
            ],

            [
                "name" => "BoralSteel",                     // roof_product_id = 54
                "url" => "https://www.boralroof.com/product-profile/steel/",
                "roof_manufacturer_id" => 4,                // Boral
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Cleo - Tile",                    // masquerades as "barrel" tile
                "url" => "https://www.roser-usa.com/cleo.html",
                "roof_manufacturer_id" => 11,               // Roser
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Stonewood - Shake",              // masquerades as dimensional shingle
                "url" => "https://www.roser-usa.com/stonewood.html",
                "roof_manufacturer_id" => 11,               // Roser
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Piano - Shingle",                // masquerades as wood shake
                "url" => "https://www.roser-usa.com/piano.html",
                "roof_manufacturer_id" => 11,               // Roser
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],

            [
                "name" => "Antica",                         // roof_product_id = 55
                "url" => "https://www.tilcorroofingusa.com/products/gallery/antica",
                "specs" => "https://www.tilcorroofingusa.com/application/files/7515/4474/8245/Technical_Data_Sheet_Antica_Tile.pdf",
                "brochure" => "http://www.tilcorroofingusa.com/application/files/7915/1114/1419/Tilcor_USA_Antica_Textured.pdf",
                "roof_manufacturer_id" => 12,               // Tilcor
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Craftsman Shake",
                "url" => "https://www.tilcorroofingusa.com/products/gallery/craftsman-shake",
                "specs" => "https://www.tilcorroofingusa.com/application/files/2215/4474/8265/Technical_Data_Sheet_Craftsman_Shake_Tile.pdf",
                "brochure" => "https://www.tilcorroofingusa.com/application/files/2814/8581/2555/Tilcor_USA_Craftsman_Shake_Textured.pdf",
                "roof_manufacturer_id" => 12,               // Tilcor
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "CF Shingle",
                "url" => "https://www.tilcorroofingusa.com/products/gallery/cf-shingle",
                "specs" => "https://www.tilcorroofingusa.com/application/files/6115/4474/8823/Technical_Data_Sheet_CF_Shingle_Panel.pdf",
                "brochure" => "https://www.tilcorroofingusa.com/application/files/2715/0785/8936/Tilcor_USA_Shingle_CF_Textured.pdf",
                "roof_manufacturer_id" => 12,               // Tilcor
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "CF Shake",
                "url" => "https://www.tilcorroofingusa.com/products/gallery/cf-shake",
                "specs" => "https://www.tilcorroofingusa.com/application/files/9015/4379/9370/Technical_Data_Sheet_CF_Shake_Panel.pdf",
                "brochure" => "https://www.tilcorroofingusa.com/application/files/2615/4378/7937/Tilcor_USA_CF_Shake_Textured.pdf",
                "roof_manufacturer_id" => 12,               // Tilcor
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],

            [
                "name" => "Classic",
                "url" => "https://gerardroofs.eu/en/products/gerard-classic",
                "roof_manufacturer_id" => 13,               // Gerard
                "roof_type_id" => 12,                      // Stone-Coated Steel
            ],
            [
                "name" => "Diamant",                       // roof_product_id = 60
                "url" => "https://gerardroofs.eu/en/products/gerard-diamant",
                "roof_manufacturer_id" => 13,               // Gerard
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Heritage",
                "url" => "https://gerardroofs.eu/en/products/gerard-heritage",
                "roof_manufacturer_id" => 13,               // Gerard
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Milano",
                "url" => "https://gerardroofs.eu/en/products/gerard-milano",
                "roof_manufacturer_id" => 13,               // Gerard
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Corona",
                "url" => "https://gerardroofs.eu/en/products/gerard-corona",
                "roof_manufacturer_id" => 13,               // Gerard
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Senator",                        // masquerades as dimensional shingle
                "url" => "https://gerardroofs.eu/en/products/gerard-senator-shingle",
                "roof_manufacturer_id" => 13,               // Gerard
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Alphine",
                "url" => "https://gerardroofs.eu/en/products/gerard-alpine",
                "roof_manufacturer_id" => 13,               // Gerard
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],

            [
                "name" => "R-Panels",
                "url" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,               // Permatile
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "PBR-Panels",
                "url" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,               // Permatile
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Advantage Panel",
                "url" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,               // Permatile
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "Permatile Panel",
                "url" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,               // Permatile
                "roof_type_id" => 12,                       // Stone-Coated Steel
            ],
            [
                "name" => "DecoTech® Solar System",
                "url" => "https://www.gaf.com/en-us/residential-roofing/decotech/panels",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 13,                       // Solar
            ],
            [
                "name" => "Acrylic",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "PMMA",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "PVDF",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "SEBS",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Silicone",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Urethane",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Liquid Membrane Systems",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Cleaners & Primers",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Sealants",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Accessories",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Asphalt Roofing",
                "roof_manufacturer_id" => 15,               // Tropical
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Mastics & Cements",
                "roof_manufacturer_id" => 15,               // Tropical
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Primers & Adhesives",
                "roof_manufacturer_id" => 15,               // Tropical
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Reflective Products",
                "roof_manufacturer_id" => 15,               // Tropical
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Accessory Products",
                "roof_manufacturer_id" => 15,               // Tropical
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Spray Polyurethane Foam",
                "roof_manufacturer_id" => 15,               // Tropical
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Elastomeric",
                "roof_manufacturer_id" => 16,               // PolyGlass
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "Asphaltic",
                "roof_manufacturer_id" => 16,               // PolyGlass
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "EPDM",
                "roof_manufacturer_id" => 17,               // Firestone
                "roof_type_id" => 14,                       // Coatings
            ],
            [
                "name" => "APP Membranes",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "SBS Membranes",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "Insulation & Cover Boards",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "Adhesives, Fasteners & LRF",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "Asphaltic Primers, Sealants & Cement",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "Asphaltic Accessories",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "Perimeter Edge Metal",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "Compatible Built-Up Roofing",
                "url" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "APP Membranes",
                "roof_manufacturer_id" => 16,               // Polyglass
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "SBS Membranes",
                "roof_manufacturer_id" => 16,               // Polyglass
                "roof_type_id" => 15,                      // Modified Bitumen
            ],
            [
                "name" => "Base, Felt & Synthetic Membranes",
                "roof_manufacturer_id" => 16,               // Polyglass
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
            [
                "name" => "Accessories",
                "roof_manufacturer_id" => 16,               // Polyglass
                "roof_type_id" => 15,                       // Modified Bitumen
            ],
        );


        $roof_sub_products = array(
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 1,                     // Timberline Series
                "name" => "HD",
                "discontinued" => 1,                        // set true because particular product is unavailable by manufacturer
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/timberline/architectural/timberline-hd",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 1,                     // Timberline Series
                "name" => "HDZ",
                "discontinued" => 0,
                "brochure" => "/documents/brochure/Data_Sheet__Timberline_HDZ_Sell_Sheet_RESTZ102.pdf",
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/timberline/architectural/timberline-hdz",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 1,                     // Timberline Series
                "name" => "UHD",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/timberline/architectural/timberline-uhd",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 1,                     // Timberline Series
                "name" => "CS",
                "discontinued" => 0,
                "brochure" => "/documents/brochure/Data_Sheet__Timberline_HDZ_Sell_Sheet_RESTZ102.pdf",
                "url" => "https://www.gaf.com/en-us/products/timberline-cs",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 1,                     // Timberline Series
                "name" => "AS II",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/timberline/specialty-architectural/timberline-armorshield-ii",
            ),
            // Designer Series
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 2,                     // Designer Series
                "name" => "Camelot II",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/value-collection/camelot-ii",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 2,                     // Designer Series
                "name" => "Glenwood",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/ultra-premium/glenwood",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 2,                     // Designer Series
                "name" => "Grand Sequoia",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/value-collection/grand-sequoia",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 2,                     // Designer Series
                "name" => "Grand Sequoia AS",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/value-collection/grand-sequoia-amorshield",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 2,                     // Designer Series
                "name" => "Slateline",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/value-collection/slateline",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 2,                     // Designer Series
                "name" => "Woodland",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/value-collection/woodland",
            ),
                //Three tab
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 3,                     // Three-Tab Series
                "name" => "Marquis WeatherMax",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/3-tab/strip-shingles/marquis-weathermax",
            ),
            array(
                "roof_manufacturer_id" => 1,                // GAF
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 3,                     // Three-Tab Series
                "name" => "Royal Sovereign",
                "discontinued" => 0,
                "url" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/3-tab/strip-shingles/royal-sovereign",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 4,                     // Architectural Series
                "name" => "TruDefinition® Duration®",
                "url" => "https://www.owenscorning.com/roofing/shingles/trudefinition-duration/",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 4,                     // Architectural Series
                "name" => "TruDefinition® Duration® Designer",
                "url" => "https://www.owenscorning.com/roofing/shingles/trudefinition-duration-designer/",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 4,                     // Architectural Series
                "name" => "Duration® Premium COOL",
                "url" => "https://www.owenscorning.com/roofing/shingles/duration-premium-cool/",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 4,                     // Architectural Series
                "name" => "Oakridge®",
                "url" => "https://www.owenscorning.com/roofing/shingles/oakridge/",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 4,                     // Architectural Series
                "name" => "TruDefinition® Oakridge",
                "url" => "https://www.owenscorning.com/roofing/shingles/trudefinition-oakridge/",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 5,                     // Slate Look Series
                "discontinued" => 1,                         // true stats that particular product us unavailable by manufacturer
                "name" => "Cambridge Cool Colors",
                "url" => "https://www.iko.com/na/wp-content/uploads/sites/8/Cambridge-Brochure---Contractors-Edition-min.pdf",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 5,                     // Slate Look Series
                "discontinued" => 0,
                "name" => "Berkshire®",
                "url" => "https://www.iko.com/na/wp-content/uploads/sites/8/Cambridge-Brochure---Contractors-Edition-min.pdf",
            ),
            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 6,                     // Wood Shake Look Series
                "discontinued" => 0,
                "name" => "Woodcrest®",
                "url" => "https://www.owenscorning.com/en-us/roofing/shingles?names=Woodcrest%C2%AE",
            ),

            array(
                "roof_manufacturer_id" => 2,                // Owens Corning
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 6,                     // 3-Tab Series
                "name" => "Cambridge Cool Colors",
                "url" => "https://www.iko.com/na/wp-content/uploads/sites/8/Cambridge-Brochure---Contractors-Edition-min.pdf",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 7,                     // Designer Series
                "name" => "Armourshake",                    // masquerades as wood shake
                "url" => "https://www.iko.com/na/residential-roofing-shingles/designer/armourshake/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 7,                     // Designer Series
                "name" => "Crowne Slate",                   // masquerades as slate tile
                "url" => "https://www.iko.com/na/residential-roofing-shingles/designer/crowne-slate/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 7,                     // Designer Series
                "name" => "Royal Estate",                   // masquerades as slate tile
                "url" => "https://www.iko.com/na/residential-roofing-shingles/designer/royal-estate/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 8,                     // Performance Series
                "name" => "Dynasty",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/performance/dynasty/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 8,                     // Performance Series
                "name" => "Nordic",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/performance/nordic/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 9,                     // Architectural Series
                "name" => "Cambridge",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/architectural/cambridge/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 9,                     // Architectural Series
                "name" => "Cambridge Cool Colors",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/architectural/cambridge-cool-colors/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 10,                    // Traditional Series
                "name" => "Marathon Plus AR",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/three-tab/marathon-plus-ar/",
            ),
            array(
                "roof_manufacturer_id" => 3,                // IKO
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 10,                    // Traditional Series
                "name" => "Marathon Plus AR",
                "url" => "https://www.iko.com/na/residential-roofing-shingles/three-tab/marathon-plus-ar/",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 11,                    // Heritage Series
                "name" => "Heritage",
                "url" => "https://www.tamko.com/styles/heritage",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 11,                    // Heritage Series
                "name" => "Heritage Premium",
                "url" => "https://www.tamko.com/styles/heritage-premium",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 11,                    // Heritage Series
                "name" => "Heritage IR",
                "url" => "https://www.tamko.com/styles/heritage-ir",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 11,                    // Heritage Series
                "name" => "Heritage Woodgate",
                "url" => "https://www.tamko.com/styles/heritage-woodgate",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 11,                    // Heritage Series
                "name" => "Heritage Vintage",
                "url" => "https://www.tamko.com/styles/heritage-vintage",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 12,                    // 3-Tab Series
                "name" => "Elite Glass-Seal",
                "url" => "https://www.tamko.com/styles/elite-glass-seal",
            ),

            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 13,                    // Shake Series
                "name" => "Presidential Shake® TL",
                "url" => "https://www.certainteed.com/residential-roofing/products/presidential-shake-tl/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 13,                    // Shake Series
                "name" => "Presidential Shake®",
                "url" => "https://www.certainteed.com/residential-roofing/products/presidential-shake/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 13,                    // Shake Series
                "name" => "Landmark® Pro",
                "url" => "https://www.certainteed.com/residential-roofing/products/landmark-pro/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 13,                    // Shake Series
                "name" => "Landmark® Premium",
                "url" => "https://www.certainteed.com/residential-roofing/products/landmark-premium/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 13,                    // Shake Series
                "name" => "Landmark Solaris® Platinum",
                "url" => "https://www.certainteed.com/residential-roofing/products/landmark-solaris-platinum/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 13,                    // Shake Series
                "name" => "Landmark®",
                "url" => "https://www.certainteed.com/residential-roofing/products/landmark/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 14,                    // Slate Series
                "name" => "Grand Manor®",
                "url" => "https://www.certainteed.com/residential-roofing/products/grand-manor/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 14,                    // Slate Series
                "name" => "Carriage House®",
                "url" => "https://www.certainteed.com/residential-roofing/products/carriage-house/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 14,                    // Slate Series
                "name" => "Belmont®",
                "url" => "https://www.certainteed.com/residential-roofing/products/belmont/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 14,                    // Slate Series
                "name" => "Belmont® IR",
                "url" => "https://www.certainteed.com/residential-roofing/products/belmont-ir/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 5,                        // Dimensional Shingle
                "roof_product_id" => 14,                    // Slate Series
                "name" => "Highland Slate®",
                "url" => "https://www.certainteed.com/residential-roofing/products/highland-slate/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 15,                    // Slate Series
                "name" => "XT™ 30 IR",
                "url" => "https://www.certainteed.com/residential-roofing/products/xt-30-ir/",
            ),
            array(
                "roof_manufacturer_id" => 5,                // CertainTeed
                "roof_type_id" => 6,                        // Three-Tab Shingle
                "roof_product_id" => 15,                    // Slate Series
                "name" => "XT™ 25",
                "url" => "https://www.certainteed.com/residential-roofing/products/xt-25/",
            ),

            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Arrowhead Gray",
                "color_description" => "Hues of Gray, Smoke Gray Flashed",
                "code" => "4209",
                "url" => "https://eagleroofing.com/product/4209/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4209-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4209_skin-fl.jpg",
                ],
                "notes" => "Not recommended in locales subject to repeated freeze-thaw cycles",
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Vierra Blend",
                "color_description" => "Blend of Dark Brown, Tan",
                "code" => "4477",
                "url" => "https://eagleroofing.com/product/4477/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4477-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4477_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Arcadia Canyon Brown",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "4502",
                "url" => "https://eagleroofing.com/product/4502/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4502-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4502_skin-fl.jpg",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Arcadia Canyon Brown",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "4502",
                "url" => "https://eagleroofing.com/product/4502/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4502-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4502_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Sierra Madre",
                "color_description" => "Hues of Charcoal, Black Streaks",
                "code" => "4503",
                "url" => "https://eagleroofing.com/product/4503/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4503-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4503_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Boca Cream",
                "color_description" => "Hues of Mocha, Cream Streaks",
                "code" => "4507",
                "url" => "https://eagleroofing.com/product/4503/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4503-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4503_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Royal Palm",
                "color_description" => "Hues of Gray, White Streaks",
                "code" => "4516",
                "url" => "https://eagleroofing.com/product/4516/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4516-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4516_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Santa Paula",
                "color_description" => "Hues of Tan, Cream, Brown Streaks",
                "code" => "4549",
                "url" => "https://eagleroofing.com/product/4549/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4549-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4549_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Dark Gray Range",
                "color_description" => "Range of Dark Gray",
                "code" => "4591",
                "url" => "https://eagleroofing.com/product/4591/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4591-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4591_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Dark Charcoal",
                "color_description" => "Hues of Dark Charcoal, Black Streaks",
                "code" => "4595",
                "url" => "https://eagleroofing.com/product/4595/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4595-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4595_skin.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 30,                    // Bel Air
                "name" => "Concord Blend",
                "color_description" => "Blend of Charcoal, Tan",
                "code" => "4602",
                "url" => "https://eagleroofing.com/product/4602/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4602-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4602_skin.jpg",
                    "https://eagleroofing.com/eagle_images/homes/4602-concord_blend.jpg",
                    "https://eagleroofing.com/eagle_images/homes/4602-concord_blend-2.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),

            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Terracotta Gold",
                "color_description" => "Hues of Terracotta, Gold Flashed",
                "code" => "3118",
                "url" => "https://eagleroofing.com/product/3118/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3118-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3118_skin.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
                "notes" => "Not recommended in locales subject to repeated freeze-thaw cycles",
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Cayenne",
                "color_description" => "Range of Red Color Bonded over Red Through Color",
                "code" => "3233",
                "url" => "https://eagleroofing.com/product/3233/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3233-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3233_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
                "notes" => "Not recommended in locales subject to repeated freeze-thaw cycles",
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Sanibel",
                "color_description" => "Hues of Reddish Orange, Gold, Gray Flashed",
                "code" => "3283",
                "url" => "https://eagleroofing.com/product/3233/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3283-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3283_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
                "notes" => "Not recommended in locales subject to repeated freeze-thaw cycles",
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Viera Blend",
                "color_description" => "Blend of Dark Brown, Tan",
                "code" => "3477",
                "url" => "https://eagleroofing.com/product/3477/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3477-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3477_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Sierra Madre",
                "color_description" => "Hues of Charcoal, Black Streaks",
                "code" => "3503",
                "url" => "https://eagleroofing.com/product/3503/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3503-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3503_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/3503-sierra_madre-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Boca Cream",
                "color_description" => "Hues of Charcoal, Black Streaks",
                "code" => "3507",
                "url" => "https://eagleroofing.com/product/3507/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3507-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3507_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Weathered Terracotta Flashed",
                "color_description" => "Hues of Light Terracotta, Black, Red Streaks",
                "code" => "3520",
                "url" => "https://eagleroofing.com/product/3520/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3520-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3520_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Santa Paula",
                "color_description" => "Hues of Tan, Cream, Brown Streaks",
                "code" => "3549",
                "url" => "https://eagleroofing.com/product/3520/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3520-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3520_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Alhambra",
                "color_description" => "Hues of Tan, Cream, Brown Streaks",
                "code" => "3555",
                "url" => "https://eagleroofing.com/product/3520/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3520-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3520_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 31,                    // Capistrano
                "name" => "Arcadia Canyon Brown",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "3581",
                "url" => "https://eagleroofing.com/product/3581/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/3581-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/3581_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/3581-arcadia_canyon_brown-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),

            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 32,                    // Double Eagle Bel Air
                "name" => "Flingridge Gray",
                "color_description" => "Hues of Natural Gray, Black Streaks",
                "code" => "4011",
                "url" => "https://eagleroofing.com/product/4011/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4011-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4011_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/4011-flintridge_gray-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 32,                    // Double Eagle Bel Air
                "name" => "Rockledge",
                "color_description" => "Hues of Dark Charcoal, Rust, Red Streaks",
                "code" => "4046",
                "url" => "https://eagleroofing.com/product/4046/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/4046-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/4046_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/4046-rockledge-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 32,                    // Double Eagle Bel Air
                "name" => "Wildwood",
                "color_description" => "Hues of Tan, Maroon, Black Streaks",
                "code" => "40517",
                "url" => "https://eagleroofing.com/product/40517/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/40517-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/40517_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),

            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 33,                    // Golden Eagle
                "name" => "Arcadia Canyon Brown",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "1502",
                "url" => "https://eagleroofing.com/product/1502/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/1502-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/1502_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 33,                    // Golden Eagle
                "name" => "Brown Gray Range",
                "color_description" => "Range of Brown, Gray",
                "code" => "1687",
                "url" => "https://eagleroofing.com/product/1687/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/1687-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/1502_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/1687-brown_gray_range-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 33,                    // Golden Eagle
                "name" => "Charcoal Range",
                "color_description" => "Range of Gray",
                "code" => "1699",
                "url" => "https://eagleroofing.com/product/1699/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/1699-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/1699_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/1699-charcoal_range.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),

            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Terracotta Gold",
                "color_description" => "Hues of Terracotta, Gold Flashed",
                "code" => "2118",
                "url" => "https://eagleroofing.com/product/2118/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2118-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2118_skin.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
                "notes" => "Not recommended in locales subject to repeated freeze-thaw cycles",
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Riviera",
                "color_description" => "Hues of Terracotta, Yellow, Brown Flashed",
                "code" => "2282",
                "url" => "https://eagleroofing.com/product/2282/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2282-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2282_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/2282-riviera-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
                "notes" => "Not recommended in locales subject to repeated freeze-thaw cycles",
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Rosewood",
                "color_description" => "Hues of Reddish Orange, Black Streaks",
                "code" => "2424",
                "url" => "https://eagleroofing.com/product/2424/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2424-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2424_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Sierra Madra",
                "color_description" => "Hues of Charcoal, Black Streaks",
                "code" => "2503",
                "url" => "https://eagleroofing.com/product/2503/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2503-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2503_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Boca Cream",
                "color_description" => "Hues of Mocha, Cream Streaks",
                "code" => "2507",
                "url" => "https://eagleroofing.com/product/2507/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2507-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2507_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Royal Palm",
                "color_description" => "Hues of Gray, White Streaks",
                "code" => "2516",
                "url" => "https://eagleroofing.com/product/2516/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2516-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2516_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Sandy Bay",
                "color_description" => "Hues of Tan, Rust, Gray Streaks",
                "code" => "2518",
                "url" => "https://eagleroofing.com/product/2518/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2518-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2518_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Santa Paula",
                "color_description" => "Hues of Tan, Cream, Brown Streaks",
                "code" => "2549",
                "url" => "https://eagleroofing.com/product/2549/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2549-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2549_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/2549-santa_paula.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Alhambra",
                "color_description" => "Hues of Terracotta, Yellow Streaks",
                "code" => "2555",
                "url" => "https://eagleroofing.com/product/2555/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2555-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2555_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 34,                    // Malibu
                "name" => "Arcadia Canyon Brown",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "2581",
                "url" => "https://eagleroofing.com/product/2581/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/2581-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/2581_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/2581-arcadia_canyon_brown.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),

            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 35,                    // Ponderosa
                "name" => "Arcadia Canyon Brown",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "5502",                           // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/5502/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/5502-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/5502_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/5502-arcadia_canyon_brown.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 35,                    // Ponderosa
                "name" => "Sierra Madre",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "5503",                           // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/5503/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/5503-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/5503_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 35,                    // Ponderosa
                "name" => "Rockledge",
                "color_description" => "Hues of Dark Charcoal, Rust, Red Streaks",
                "code" => "56046",                           // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/56046/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/56046-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/56046_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 35,                    // Ponderosa
                "name" => "Brown Gray Range",
                "color_description" => "Range of Brown, Gray",
                "code" => "5687",                           // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/5687/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/5687-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/5687_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/5687-brown_gray_range-2.jpg",
                ],
                "details" => [
                    "category" => "Standard Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 36,                    // Textured Slate
                "name" => "Chatham Blend",
                "color_description" => "Range of Brown, Gray",
                "code" => "49101",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49101/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49101-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49101_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/49101-chatham_blend-3.jpg",
                    "https://eagleroofing.com/eagle_images/homes/49101-chatham_blend-1.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 36,                    // Textured Slate
                "name" => "Heathrow Blend",
                "color_description" => "Blend of Greens",
                "code" => "49102",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49102/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49102-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49102_skin.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 36,                    // Textured Slate
                "name" => "Amesbury Blend",
                "color_description" => "Blend of Medium Gray, Dark Gray, Maroon",
                "code" => "49103",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49103/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49103-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49103_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 36,                    // Textured Slate
                "name" => "Providence Blend",
                "color_description" => "Blend of Light Gray, Medium Gray, Maroon",
                "code" => "49104",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49104/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49104-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49104_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 36,                    // Textured Slate
                "name" => "Windermere Blend",
                "color_description" => "Blend of Dark Brown, Charcoal",
                "code" => "49105",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/eagle_images/panels/49105-fl.jpg",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels_skin/49105_skin-fl.jpg",
                    "https://eagleroofing.com/eagle_images/homes/49105-windermere_blend.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 36,                    // Textured Slate
                "name" => "Plymouth Blend",
                "color_description" => "Blend of Grays",
                "code" => "49106",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49106/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49106-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49106_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 37,                    // Tapered Slate
                "name" => "Arcadia Canyon Brown",
                "color_description" => "Hues of Dark Brown, Black Streaks",
                "code" => "49581",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49581/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49581-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49581_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 37,                    // Tapered Slate
                "name" => "Dark Charcoal",
                "color_description" => "Hues of Dark Charcoal, Black Streaks",
                "code" => "49595",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49595/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49595-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49595_skin.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 37,                    // Tapered Slate
                "name" => "Concord Blend",
                "color_description" => "Blend of Charcoal, Tan",
                "code" => "49602",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49602/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49602-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49602_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 37,                    // Tapered Slate
                "name" => "Kings Canyon Blend",
                "color_description" => "Blend of Dark Green, Tan, Red and Black Streaks",
                "code" => "49634",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49634/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49634-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49634_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 37,                    // Tapered Slate
                "name" => "Mount Dora Blend",
                "color_description" => "Blend of Gray, White, Black Streaks",
                "code" => "49655",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49655/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49655-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49655_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),
            array(
                "roof_manufacturer_id" => 7,                // Eagle
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 37,                    // Tapered Slate
                "name" => "Catskill",
                "color_description" => "Hues of Moss Green, Dark Green, Green, White Flashed",
                "code" => "49718",                          // Note the different code versus id above with same name field
                "url" => "https://eagleroofing.com/product/49718/?region=florida",
                "samples" => [
                    "https://eagleroofing.com/eagle_images/panels/49718-fl.jpg",
                    "https://eagleroofing.com/eagle_images/panels_skin/49718_skin-fl.jpg",
                ],
                "details" => [
                    "category" => "Designer Select",
                    "weight" => "Conventional",
                ],
            ),

            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 38,                    // Florida Signature Series
                "name" => "Sanibel",
                "url" => "https://www.crownrooftiles.com/sanibel_fl.html",
            ),
            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 38,                    // Florida Signature Series
                "name" => "Tuscany",
                "url" => "https://www.crownrooftiles.com/tuscany_fl.html",
            ),
            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 38,                    // Florida Signature Series
                "name" => "Windsor Shake",
                "url" => "https://www.crownrooftiles.com/windsor_shake_fl.html",
            ),
            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 38,                    // Florida Signature Series
                "name" => "Windsor Slate",
                "url" => "https://www.crownrooftiles.com/windsor_slate_fl.html",
            ),
            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 7,                        // Concrete Tile
                "roof_product_id" => 38,                    // Florida Signature Series
                "name" => "Windsor Split Shake",
                "url" => "https://www.crownrooftiles.com/windsor_split_shake_fl.html",
            ),

            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 39,                    // US Tile by Boral
                "name" => "Romano Pans",
                "url" => "https://www.crownrooftiles.com/windsor_split_shake_fl.html",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 39,                    // US Tile by Boral
                "name" => '1-Piece "S" Tile',
                "url" => "https://www.boralroof.com/product-profile/clay/1-piece-s-tile/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 39,                    // US Tile by Boral
                "name" => "Claymax",
                "url" => "https://www.boralroof.com/product-profile/clay/claymax/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 39,                    // US Tile by Boral
                "name" => "ClayLite",
                "url" => "https://www.boralroof.com/product-profile/clay/claylite/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 39,                    // US Tile by Boral
                "name" => "2-Piece Mission",
                "url" => "https://www.boralroof.com/product-profile/clay/2-piece-mission/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 39,                    // US Tile by Boral
                "name" => "2-Piece Monarch",
                "url" => "https://www.boralroof.com/product-profile/clay/2-piece-monarch/",
            ),

            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 45,                    // Florida Clay Series
                "name" => "Modena",
                "url" => "https://www.crownrooftiles.com/modena_fl.html",
            ),
            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 45,                    // Florida Clay Series
                "name" => "Toledo",
                "url" => "https://www.crownrooftiles.com/toledo_fl.html",
            ),
            array(
                "roof_manufacturer_id" => 8,                // Crown
                "roof_type_id" => 8,                        // Clay Tile
                "roof_product_id" => 45,                    // Florida Clay Series
                "name" => "Monaco",
                "url" => "https://www.crownrooftiles.com/monaco_fl.html",
            ),

            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 47,                    // Hidden Fastener Series
                "name" => "GulfLok™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulflok/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 47,                    // Hidden Fastener Series
                "name" => "GulfSeam™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfseam/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 47,                    // Hidden Fastener Series
                "name" => "VersaLoc™ & MegaLoc™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/versaloc-megaloc/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 48,                    // Exposed Fastener Series
                "name" => "GulfRib™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfrib/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 48,                    // Exposed Fastener Series
                "name" => "5V Crimp™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/5v-crimp/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 48,                    // Exposed Fastener Series
                "name" => "GulfPBR™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfpbr/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 48,                    // Exposed Fastener Series
                "name" => "GulfWave™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfwave/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 49,                    // Stamped
                "name" => "Centura Steel Shingle",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/centura-steel-shingle/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 49,                    // Stamped
                "name" => "Grande Tile",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/grandetile/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 49,                    // Stamped
                "name" => "Great American Shake",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/great-american-shake/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 10,                       // Aluminum
                "roof_product_id" => 49,                    // Stamped
                "name" => "Oxford Shingle",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/oxford-shingle/",
            ),

            array(
                "roof_manufacturer_id" => 4,                // Tamko (MetalWorks)
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 50,                    // Stamped
                "name" => "AstonWood",                      // masquerades as wood shingles
                "url" => "https://www.tamko.com/styles/metalworks-astonwood",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko (MetalWorks)
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 50,                    // Stamped
                "name" => "StoneCrest® Slate",              // masquerades as slate
                "url" => "https://www.tamko.com/styles/metalworks-stonecrest-slate",
            ),
            array(
                "roof_manufacturer_id" => 4,                // Tamko
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 50,                    // MetalWorks
                "name" => "StoneCrest® Tile",               // masquerades as tile
                "url" => "https://www.tamko.com/styles/metalworks-stonecrest-tile",
            ),

            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 51,                    // Hidden Fastener Series
                "name" => "GulfLok™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulflok/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 51,                    // Hidden Fastener Series
                "name" => "GulfSeam™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfseam/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 51,                    // Hidden Fastener Series
                "name" => "VersaLoc™ & MegaLoc™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/versaloc-megaloc/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 52,                    // Exposed Fastener Series
                "name" => "GulfRib™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfrib/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 52,                    // Exposed Fastener Series
                "name" => "5V Crimp™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/5v-crimp/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 52,                    // Exposed Fastener Series
                "name" => "GulfPBR™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfpbr/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 52,                    // Exposed Fastener Series
                "name" => "GulfWave™",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfwave/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 53,                    // Stamped
                "name" => "Centura Steel Shingle",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/centura-steel-shingle/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 53,                    // Stamped
                "name" => "Grande Tile",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/grandetile/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 53,                    // Stamped
                "name" => "Great American Shake",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/great-american-shake/",
            ),
            array(
                "roof_manufacturer_id" => 10,               // Gulf Coast Metals
                "roof_type_id" => 11,                       // Steel
                "roof_product_id" => 53,                    // Stamped
                "name" => "Oxford Shingle",
                "url" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/oxford-shingle/",
            ),

            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 12,                       // Stone-Coated Steel
                "roof_product_id" => 54,                    // BoralSteel
                "name" => "Pine Crest Shake",               // masquerades as wood shake
                "url" => "https://www.boralroof.com/product-profile/steel/pine-crest-shake/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 12,                       // Stone-Coated Steel
                "roof_product_id" => 54,                    // BoralSteel
                "name" => "Granite Ridge",                  // masquerades as three-tab shingle
                "url" => "https://www.boralroof.com/product-profile/steel/granite-ridge-shingle/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 12,                       // Stone-Coated Steel
                "roof_product_id" => 54,                    // BoralSteel
                "name" => "Cottage Shingle",                // masquerades as dimensional shingle
                "url" => "https://www.boralroof.com/product-profile/steel/cottage-shingle/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 12,                       // Stone-Coated Steel
                "roof_product_id" => 54,                    // BoralSteel
                "name" => "Barrel Vault Tile",              // masquerades as barrel tile
                "url" => "https://www.boralroof.com/product-profile/steel/barrel-vault-tile/",
            ),
            array(
                "roof_manufacturer_id" => 6,                // Boral
                "roof_type_id" => 12,                       // Stone-Coated Steel
                "roof_product_id" => 54,                    // BoralSteel
                "name" => "Pacific Tile",
                "url" => "https://www.boralroof.com/product-profile/steel/pacific-tile/",
            ),

        );

        foreach ($roof_products as $roof_product) {
            \App\RoofProduct::create($roof_product);
        }

        foreach ($roof_sub_products as $roof_sub_product) {
            \App\RoofProduct::create($roof_sub_product);
        }
    }
}
