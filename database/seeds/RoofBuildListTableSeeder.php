<?php

use Illuminate\Database\Seeder;

class RoofBuildListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roof_build_list = array(
            array(
                "id" => 1,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 1,
                "material_id" => 176,                   // Plywood sheets

            ),
            array(
                "id" => 2,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 1,
                "material_id" => 59,                    // 8d Coil nails
            ),
            array(
                "id" => 3,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 2,
                "material_id" => 1,                     // Boots
            ),
            array(
                "id" => 4,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 2,
                "material_id" => 278,                   // Vents
            ),
            array(
                "id" => 5,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 3,
                "material_id" => 271,                  // Underlayment - PolyGlass IR-XE 1/.7 SQ /Roll
            ),
            array(
                "id" => 6,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 3,
                "material_id" => 58,                    // Cap nails - 45 square per box F-003-21
            ),
            array(
                "id" => 7,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 4,
                "material_id" => 63,                    // Valley Metal Roll F-003-26
            ),
            array(
                "id" => 8,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 4,
                "material_id" => 66,                    // Drip Edge F-003-29
            ),
            array(
                "id" => 9,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 4,
                "material_id" => 283,                   // Roof cement MI-006-14
            ),
            array(
                "id" => 10,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 5,
                "material_id" => 190,                   // Main shingle component  (GAF, IKO, etc)
            ),
            array(
                "id" => 11,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 5,
                "material_id" => 59,                    // Nails
            ),
            array(
                "id" => 12,
                "client_id" => 1,
                "roof_type_id" => 1,
                "roof_layer_id" => 6,
                "material_id" => 8,                     // Hip and ridge caps
                "component" => 'ridge'
            ),
        );
        foreach ($roof_build_list as $list) {
            DB::table('roof_build_list')->insert($list);
        }
    }
}

