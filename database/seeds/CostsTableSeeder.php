<?php

use Illuminate\Database\Seeder;

class CostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $costs = [
            [
                "name" => "Shingle Off / Shingle On @ Standard Slope",
                "type" => "labor",
                "description" => "Shingle Group Off & On for Standard Slope",
                "criteria" => [
                    'tearoff_roof_type_id' => 1,
                    'install_roof_type_id' => 1
                ],
                "cost" => 109.00
            ],

            [
                "name" => "Remove Extra Layer of Shingle",
                "type" => "labor",
                "description" => "Remove Extra Layer of Shingle",
                "criteria" => ['tearoff_roof_type_id' => 1],
                "cost" => 45.00
            ],

            [
                "name" => "Shingle Off / Shingle On @ 7 Pitch",
                "type" => "labor",
                "description" => "Shingle Off / Shingle On @ 7 Pitch",
                "criteria" => [
                    'tearoff_roof_type_id' => 1,
                    'install_roof_type_id' => 1,
                    'pitch' => 7
                ],
                "cost" => 159.00
            ],

            [
                "name" => "Shingle Off / Shingle On @ 8 Pitch",
                "type" => "labor",
                "description" => "Shingle Off / Shingle On @ 8 Pitch",
                "criteria" => [
                    'tearoff_roof_type_id' => 1,
                    'install_roof_type_id' => 1,
                    'pitch' => 8
                ],
                "cost" => 159.00,
            ],

            [
                "name" => "Tile Off / Shingle On @ Standard Slope",
                "type" => "labor",
                "description" => "Tile Off / Shingle On @ Standard Slope",
                "criteria" => [
                    'tearoff_roof_type_id' => 2,
                    'install_roof_type_id' => 1
                ],
                "cost" => 154.70
            ],

            [
                "name" => "Tile Off / Shingle On @ 7 Pitch",
                "type" => "labor",
                "description" => "Tile Off / Shingle On @ 7 Pitch",
                "criteria" => [
                    'tearoff_roof_type_id' => 2,
                    'install_roof_type_id' => 1,
                    'pitch' => 7
                ],
                "cost" => 202.30
            ],

            [
                "name" => "Designer Shingle Extra Labor",
                "type" => "labor",
                "description" => "Designer Shingle Extra Labor",
                "criteria" => ['install_roof_type_id' => 6],
                "cost" => 23.80
            ],

            [
                "name" => "Shingle New Construction",
                "type" => "labor",
                "description" => "Designer Shingle Extra Labor",
                "criteria" => ['install_roof_type_id' => 1],
                "cost" => 23.80
            ],

            [
                "name" => "Management Fee",
                "type" => "other",
                "description" => "Odie Platform Job Management Fee",
                "cost" => 60,
                "additional_data" => ['customer_viewable' => 1],
            ],

            [
                "name" => "Marketing Fee",
                "type" => "other",
                "description" => "Lead Acquisition Fee",
                "cost" => 60,
                "additional_data" => ['customer_viewable' => 0],
            ],

            [
                "name" => "Fuel Surcharge",
                "type" => "other",
                "description" => "Fuel Surcharge & Wear & Tear",
                "cost" => 50.00,
                "additional_data" => ['customer_viewable' => 0],
            ],
        ];

        foreach ($costs as $cost) {
            \App\Cost::create($cost);
        }
    }
}
