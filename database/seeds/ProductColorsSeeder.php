<?php

use Illuminate\Database\Seeder;

class ProductColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product_colors = array(
            // hdz series
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 1,            // Barkwood
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 2,        //Birchwood
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 3,   //Charcoal
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 4,   // Driftwood
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 5,   // Hickory
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 6,    // Hunter Green
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 7, //  Mission Brown
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 8, //Oyster Grey
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 9,//Pewter Gray
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 10,//Shakewood
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 11,//Slate
            ),
            array(
                "roof_manufacturers_product_id" => 106, // HDZ
                "roof_product_color_id" => 13,//Weathered Wood
            ),
            // uhd series
            array(
                "roof_manufacturers_product_id" => 107, // UHD
                "roof_product_color_id" => 1,           // Barkwood
            ),
            array(
                "roof_manufacturers_product_id" => 107, // UHD
                "roof_product_color_id" => 2,        //Birchwood
            ),
            array(
                "roof_manufacturers_product_id" => 107, // UHD
                "roof_product_color_id" => 3,           // Charcoal
            ),
            array(
                "roof_manufacturers_product_id" => 107, // UHD
                "roof_product_color_id" => 9,        //Pewter Gray
            ),
            array(
                "roof_manufacturers_product_id" => 107, // UHD
                "roof_product_color_id" => 10,        //Shakewood
            ),
            array(
                "roof_manufacturers_product_id" => 107, // UHD
                "roof_product_color_id" => 11,        //slate
            ),
            array(
                "roof_manufacturers_product_id" => 107, // UHD
                "roof_product_color_id" => 13,       //Weathered Wood
            ),

            //
        );

        DB::table('product_colors')->insert($product_colors);
    }
}
