<?php

use Illuminate\Database\Seeder;

class ConstantGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = array(
            ['key' => 'general', 'name' => 'General'],
            ['key' => 'associate_types', 'name' => 'Associate Types'],
            ['key' => 'units', 'name' => 'Units'],
            ['key' => 'claim_statuses', 'name' => 'Claim Statuses'],
            ['key' => 'attachment_groups', 'name' => 'Attachment Groups'],
            ['key' => 'attachment_types', 'name' => 'Attachment Types'],
            ['key' => 'insurance_coverage_types', 'name' => 'Coverage Types'],
            ['key' => 'coverage_denial_reasons', 'name' => 'Denial Reasons'],

            /* Any New Groups Which are Actively In Use Goes Here*/

            /* All Unused Groups Here*/
            ['key' => 'states', 'name' => 'States'],
            ['key' => 'countries', 'name' => 'Countries'],
            ['key' => 'classifications', 'name' => 'Project Classification'],
            ['key' => 'phone_types', 'name' => 'Phone Types'],
            ['key' => 'project_types', 'name' => 'Project Types'],
            ['key' => 'photo_types', 'name' => 'Photo Types'],
            ['key' => 'trade_types', 'name' => 'Trade Types'],
            ['key' => 'roof_manufacturers_material_types', 'name' => 'Roof Manufacturers Material Types'],
            ['key' => 'roof_manufacturers_finish_types', 'name' => 'Roof Manufacturers Finish Types'],
            ['key' => 'carrier_relationships', 'name' => 'Relationships to an Insurance Carrier'],
            ['key' => 'roof_material_categories', 'name' => 'Roof Material Categories'],
            ['key' => 'slope_types', 'name' => 'Roof Slopes'],
            ['key' => 'roof_leak_types', 'name' => 'Roof Leak Types'],                      // Opportunity?
            ['key' => 'pitch_types', 'name' => 'Roof Pitches'],
            ['key' => 'appointment_types', 'name' => 'Appointment Types'],
            ['key' => 'crew_types', 'name' => 'Crew Types'],
            ['key' => 'property_relationships', 'name' => 'Property Relationships'],
            ['key' => 'story_types', 'name' => 'Story Types'],
            ['key' => 'template_types', 'name' => 'Templates'],
            ['key' => 'claim_types', 'name' => 'Claim Types'],                              // Maximus
            ['key' => 'storms', 'name' => 'Recent Storms'],                                 // Does not belong in constants table....
            ['key' => 'storm_types', 'name' => 'Types of Storms'],
            ['key' => 'claim_submission_types', 'name' => 'Info Sent to Carrier'],          // Maximus
            ['key' => 'negotiation_outcomes', 'name' => 'Negotiation Outcomes'],            // Maximus
            ['key' => 'ariel_measurement_firms', 'name' => 'Ariel Measurement Firms'],      // Opportunity, Estimator
            ['key' => 'review_sources', 'name' => 'Review Sources'],                        // Halo
            ['key' => 'crew_compensation_types', 'name' => 'Crew Compensation Types'],      // Production
            ['key' => 'crew_craft_types', 'name' => 'Crew Craft Types'],                    // Production
            ['key' => 'progress_boards', 'name' => 'Progress Boards'],
        );

        foreach ($groups as $group){
            \App\ConstantGroup::create($group);
        }
    }
}
