<?php

use Illuminate\Database\Seeder;

class RoofLayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roof_layers = [
            ["name" => "Deck Preparation"],
            ["name" => "Boots & Vents"],
            ["name" => "Underlayment"],
            ["name" => "Valley Prep & Flashings"],
            ["name" => "Main Component Work"],
            ["name" => "Hip & Ridge Caps"],
            ["name" => "Extras"],
        ];

        foreach ($roof_layers as $roof_layer) {
            \App\RoofLayer::create($roof_layer);
        }
    }
}
