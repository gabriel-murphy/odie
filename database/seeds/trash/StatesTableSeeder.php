<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Alabama', 'AL']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Alaska', 'AK']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Arizona', 'AZ']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Arkansas', 'AR']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Californa', 'CA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Colorado', 'CO']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Connecticut', 'CT']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Delaware', 'DE']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['District of Columbia', 'DC']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Florida', 'FL']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Georgia', 'GA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Hawaii', 'HI']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Idaho', 'ID']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Illinois', 'IL']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Indiana', 'IN']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Iowa', 'IA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Kansas', 'KS']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Kentucy', 'KY']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Louisiana', 'LA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Maine', 'ME']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Montana', 'MT']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Nebraska', 'NE']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Nevada', 'NV']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['New Hampshire', 'NH']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['New Jersey', 'NJ']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['New Mexico', 'NM']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['New York', 'NY']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['North Carolina', 'NC']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['North Dakota', 'ND']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Ohio', 'OH']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Oklahoma', 'OK']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Oregon', 'OR']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Maryland', 'MD']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Massachusetts', 'MA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Michigan', 'MI']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Minnesota', 'MN']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Mississippi', 'MI']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Missouri', 'MO']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Pennsylvania', 'PA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Rhode Island', 'RI']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['South Carolina', 'SC']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['South Dakota', 'SD']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Tennessee', 'TN']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Texas', 'TX']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Utah', 'UT']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Vermont', 'VT']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Virginia', 'VA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Washington', 'WA']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['West Virgina', 'WV']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Wisconsin', 'WI']);
        DB::insert('insert into states (name, abbrev) values (?, ?)', ['Wyoming', 'WY']);

    }
}
