<?php

use Illuminate\Database\Seeder;

class PopulationAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* The Platform seeds a population address table with all of the residential addresses in Collier, Lee and Charlotte Counties in Florida.  The seeding takes 35 minutes to 5 hours, depending on the system.  Note the dB: insert is commented out as is the drop() function on line 110 of the create_population_addresses migrations to avoid re-seeding the table when running 'artisan migrate:fresh -seed'  */

    public function run()
    {

        ini_set("memory_limit", "1024M");
        $lines = file("http://165.227.202.96/population_addresses.txt");
        foreach ($lines as $data) {
            $values = explode("\t", $data);
            DB::insert('insert into population_addresses (
			ConsumerDataID,
			AddressID,
			IndividualId,
			personfirstname,
			personmiddleinitial,
			personlastname,
			PersonSurnameSuffix,
			persontitleofrespect,
			primaryaddress,
			secondaryaddress,
			ZipCode,
			Zip_4,
			countyname,
			carrier_route,
			latitude,
			longitude,
			dwellingtype,
			TenDigitPhone,
			Cellindicator,
			Carrier,
			estimatedincomecode,
			homeownerprobabilitymodel,
			lengthofresidence,
			numberofpersonsinlivingunit,
			presenceofchildren,
			NumberOfChildren,
			persongender,
			persondateofbirthyear,
			persondateofbirthmonth,
			persondateofbirthday,
			PersonMaritalStatus,
			ethniccode,
			languagecode,
			ethnicgroup,
			religioncode,
			hispaniccountrycode,
			NumberOfAdults,
			CreditRating,
			Networth,
			golfenthusiasts,
			traveler,
			pets,
			cats,
			dogs,
			religiouscontributor,
			politicalcontributor,
			donatestoenvironmentalcauses,
			veteraninhousehold,
			Smoker,
			AnimalWelfareCharitableDonation,
			ArtsOrCulturalCharitableDonation,
			ChildrensCharitableDonation,
			_ENVIRONMENT_OR_WILDLIFE__CHARITABLE_DONATION,
			EnvironmentalIssuesCharitableDonation,
			InternationalAidCharitableDonation,
			PoliticalCharitableDonation,
			PoliticalConservativeCharitableDonation,
			PoliticalLiberalCharitableDonation,
			VoterParty,
			MatchTypeVoterParty,
			VeteransCharitableDonation,
			CharitableDonations_Other,
			CommunityCharities,
			SingleParent,
			ChristianFamilies,
			Equestrian,
			OtherPetOwner,
			AfricanAmericanProfessionals,
			GamingCasino,
			homepurchaseprice,
			homepurchasedateyear,
			homepurchasedatemonth,
			homepurchasedateday,
			homeyearbuilt,
			mortgageamountinthousands,
			mortgagelendername,
			LoanToValue,
			Name,
			Email,
			SecondEmail,
			ThirdEmail,
			CellPhone,
			SecondCellPhone,
			ThirdCellPhone,
			created_at) values 
		
	(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',

                [$values[0], $values[1], $values[2], ucfirst(strtolower($values[3])), $values[4], ucfirst(strtolower($values[5])), $values[6], ucfirst(strtolower($values[7])), $values[8], $values[9], $values[10], $values[11], $values[12], $values[13], $values[14], $values[15], $values[16], $values[17], $values[18], $values[19], ucfirst(strtolower($values[20])), $values[21], $values[22], $values[23], $values[24], $values[25], $values[26], $values[27], $values[28], $values[29], $values[30], $values[31], $values[32], $values[33], $values[34], $values[35], $values[36], $values[37], $values[38], $values[39], $values[40], $values[41], $values[42], $values[43], $values[44], $values[45], $values[46], $values[47], $values[48], $values[49], $values[50], $values[51], $values[52], $values[53], $values[54], $values[55], $values[56], $values[57], $values[58], $values[59], $values[60], $values[61], $values[62], $values[63], $values[64], $values[65], $values[66], $values[67], $values[68], $values[69], $values[70], $values[71], $values[72], $values[73], $values[74], ucwords(strtolower($values[75])), $values[76], ucwords(strtolower($values[77])), strtolower($values[78]), $values[79], $values[80], $values[81], $values[82], $values[83], NOW()]);

        }
    }
}
