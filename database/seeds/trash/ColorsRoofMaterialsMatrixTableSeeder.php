<?php

use Illuminate\Database\Seeder;

class ColorsRoofMaterialsMatrixTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	## Populate all 14 color options for GAF Timberline HD
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['1', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['2', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['3', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['4', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['5', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['6', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['7', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['8', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['9', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['10', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['11', 'roof_manufacturers_lines', '1']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['12', 'roof_manufacturers_lines', '1']); 
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['13', 'roof_manufacturers_lines', '1']); 
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['14', 'roof_manufacturers_lines', '1']); 

    	## Populate all 12 color options for GAF Timberline (Series) HDZ (Line) - which does not include ID 12 and 14
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['1', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['2', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['3', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['4', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['5', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['6', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['7', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['8', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['9', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['10', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['11', 'roof_manufacturers_lines', '2']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['13', 'roof_manufacturers_lines', '2']); 

    	## Populate all 6 color options for GAF Timberline (Series) UHD (Line)
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['1', 'roof_manufacturers_lines', '3']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['3', 'roof_manufacturers_lines', '3']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['9', 'roof_manufacturers_lines', '3']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['10', 'roof_manufacturers_lines', '3']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['11', 'roof_manufacturers_lines', '3']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['13', 'roof_manufacturers_lines', '3']);

    	## Populate all 4 color options for GAF Designer (Series) Slateline (Line) - which has only 4 color options
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['15', 'roof_manufacturers_lines', '4']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['16', 'roof_manufacturers_lines', '4']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['17', 'roof_manufacturers_lines', '4']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['18', 'roof_manufacturers_lines', '4']);

    	## Populate all 4 color options for GAF Designer (Series) Camelot (Line) - which has only 5 color options
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['15', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['1', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['3', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['19', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['20', 'roof_manufacturers_lines', '5']);

    	## Populate all 9 color options for GAF 3-Tab (Series) Royal Sovereign® (Line)
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['21', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['22', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['23', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['24', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['25', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['26', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['27', 'roof_manufacturers_lines', '5']);
        DB::insert('insert into colors_roof_materials_matrix (color_id, dB_location, dB_id) values (?, ?, ?)', ['14', 'roof_manufacturers_lines', '5']);



    }
}
