<?php

use Illuminate\Database\Seeder;

class RoofManufacturersSeriesTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roof_manufacturers_series_types = array(
            array(
                "id" => 1,                                          // Start w/ Shingle
                "name" => "Timberline",
                "roof_manufacturer_id" => 1,                        // GAF
                "roof_types_roof_manufacturers_matrix_id" => 1,     // Shingle - GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 2,
                "name" => "Designer",
                "roof_manufacturer_id" => 1,                        // GAF
                "roof_types_roof_manufacturers_matrix_id" => 1,     // Shingle - GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 3,
                "name" => "3-Tab",
                "roof_manufacturer_id" => 1,                        // GAF
                "roof_types_roof_manufacturers_matrix_id" => 1,     // Shingle - GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 4,
                "name" => "Architectural",
                "roof_manufacturer_id" => 2,                        // Owens Corning
                "roof_types_roof_manufacturers_matrix_id" => 2,     // Shingle - OC
                "created_at" => NOW(),
            ),
            array(
                "id" => 5,
                "name" => "Slate Look",
                "roof_manufacturer_id" => 2,                        // Owens Corning
                "roof_types_roof_manufacturers_matrix_id" => 2,     // Shingle - OC
                "created_at" => NOW(),
            ),
            array(
                "id" => 6,
                "name" => "3-Tab",
                "roof_manufacturer_id" => 2,                        // Owens Corning
                "roof_types_roof_manufacturers_matrix_id" => 2,     // Shingle - OC
                "created_at" => NOW(),
            ),
            array(
                "id" => 7,
                "name" => "Designer",
                "roof_manufacturer_id" => 3,                        // IKO
                "roof_types_roof_manufacturers_matrix_id" => 3,     // Shingle - IKO
                "created_at" => NOW(),
            ),
            array(
                "id" => 8,
                "name" => "Performance",
                "roof_manufacturer_id" => 3,                        // IKO
                "roof_types_roof_manufacturers_matrix_id" => 3,     // Shingle - IKO
                "created_at" => NOW(),
            ),
            array(
                "id" => 9,
                "name" => "Architectural",
                "roof_manufacturer_id" => 3,                        // IKO
                "roof_types_roof_manufacturers_matrix_id" => 3,     // Shingle - IKO
                "created_at" => NOW(),
            ),
            array(
                "id" => 10,
                "name" => "Traditional",
                "roof_manufacturer_id" => 3,                        // IKO
                "roof_types_roof_manufacturers_matrix_id" => 3,     // Shingle - IKO
                "created_at" => NOW(),
            ),
            array(
                "id" => 11,
                "name" => "Heritage",
                "roof_manufacturer_id" => 4,                        // Tamko
                "roof_types_roof_manufacturers_matrix_id" => 4,     // Shingle - Tamko (Heritage & Elite Glass)
                "created_at" => NOW(),
            ),
            array(
                "id" => 12,
                "name" => "3-Tab",
                "roof_manufacturer_id" => 4,                        // Tamko
                "roof_types_roof_manufacturers_matrix_id" => 4,     // Shingle - Tamko
                "created_at" => NOW(),
            ),


            array(
                "id" => 13,
                "name" => "Shake",
                "roof_manufacturer_id" => 5,                        // CertainTeed
                "roof_types_roof_manufacturers_matrix_id" => 5,     // Shingle - CertainTeed
                "created_at" => NOW(),
            ),
            array(
                "id" => 14,
                "name" => "Slate",
                "roof_manufacturer_id" => 5,                        // CertainTeed
                "roof_types_roof_manufacturers_matrix_id" => 5,     // Shingle - CertainTeed
                "created_at" => NOW(),
            ),
            array(
                "id" => 15,
                "name" => "3-Tab",
                "roof_manufacturer_id" => 5,                        // CertainTeed
                "roof_types_roof_manufacturers_matrix_id" => 5,     // Shingle - CertainTeed
                "created_at" => NOW(),
            ),
            array(
                "id" => 16,                                         // Start of concrete tile series
                "name" => "Boosted Barcelona Caps",
                "roof_manufacturer_id" => 6,                        // Boral
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 17,
                "name" => "Saxony 900 Split Slate",
                "roof_manufacturer_id" => 6,                        // Boral
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 18,
                "name" => "Saxony 900 Slate",
                "roof_manufacturer_id" => 6,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),

            array(
                "id" => 19,
                "name" => "Saxony 900 Shake",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 20,
                "name" => "Saxony 900 Split Shake",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 21,
                "name" => "Plantation Slate Like",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 22,
                "name" => "Bermuda Smooth",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 23,
                "name" => "Bermuda Broom Swept",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 24,
                "name" => "Bermuda Rustic Shake",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 25,
                "name" => "Estate",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 26,
                "name" => "Galena",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 27,
                "name" => "Barcelona 900",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 28,
                "name" => "Madera 900",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 29,
                "name" => "Villa 900",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 6,
                "created_at" => NOW(),
            ),
            array(
                "id" => 30,
                "name" => "Bel Air",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,         // Eagle (last option before colors)
                "created_at" => NOW(),
            ),
            array(
                "id" => 31,
                "name" => "Capistrano",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,
                "created_at" => NOW(),
            ),
            array(
                "id" => 32,
                "name" => "Double Eagle Bel Air",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,
                "created_at" => NOW(),
            ),
            array(
                "id" => 33,
                "name" => "Golden Eagle",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,
                "created_at" => NOW(),
            ),
            array(
                "id" => 34,
                "name" => "Malibu",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,
                "created_at" => NOW(),
            ),
            array(
                "id" => 35,
                "name" => "Ponderosa",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,
                "created_at" => NOW(),
            ),
            array(
                "id" => 36,
                "name" => "Textured Slate",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,
                "created_at" => NOW(),
            ),
            array(
                "id" => 37,
                "name" => "Tapered Slate",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 7,
                "created_at" => NOW(),
            ),
            array(
                "id" => 38,
                "name" => "Sanibel",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 8,         // Crown Tile (last option before colors)
                "created_at" => NOW(),
            ),
            array(
                "id" => 39,
                "name" => "Tuscany",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 8,
                "created_at" => NOW(),
            ),
            array(
                "id" => 40,
                "name" => "Windsor Slate",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 8,
                "created_at" => NOW(),
            ),
            array(
                "id" => 41,
                "name" => "Windsor Shake",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 8,
                "created_at" => NOW(),
            ),
            array(
                "id" => 42,
                "name" => "Windsor Split Shake",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 8,
                "created_at" => NOW(),
            ),

            array(
                "id" => 43,                                             // Start of clay tile series
                "name" => "Romano Pans",
                "website" => "https://www.boralroof.com/product-profile/clay/romano-pans/1URDU6073/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 9,         // Boral - clay
                "created_at" => NOW(),
            ),
            array(
                "id" => 44,
                "name" => '1-Piece "S" Tile',
                "website" => "https://www.boralroof.com/product-profile/clay/1-piece-s-tile/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 9,
                "created_at" => NOW(),
            ),
            array(
                "id" => 45,
                "name" => "ClayMax",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 9,
                "created_at" => NOW(),
            ),
            array(
                "id" => 46,
                "name" => "ClayLite",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 9,
                "created_at" => NOW(),
            ),
            array(
                "id" => 47,
                "name" => "2-Piece Mission",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 9,
                "created_at" => NOW(),
            ),
            array(
                "id" => 48,
                "name" => "2-Piece Monarch",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 9,
                "created_at" => NOW(),
            ),

            array(
                "id" => 49,
                "name" => 'Spanish "S"',
                "website" => "https://www.santafetile.com/clay-tile/spanish-s/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 10,        // Santafé series
                "created_at" => NOW(),
            ),
            array(
                "id" => 50,
                "name" => 'Báltica',
                "website" => "https://www.santafetile.com/clay-tile/baltica-collection/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 10,
                "created_at" => NOW(),
            ),
            array(
                "id" => 51,
                "name" => "Imperial",
                "website" => "https://www.santafetile.com/clay-tile/imperial/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 10,
                "created_at" => NOW(),
            ),
            array(
                "id" => 52,
                "name" => "Mission Barrel",
                "website" => "https://www.santafetile.com/clay-tile/mission-barrel/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 10,
                "created_at" => NOW(),
            ),
            array(
                "id" => 53,
                "name" => "Mission Barrel",
                "website" => "https://www.santafetile.com/clay-tile/mission-barrel/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 10,
                "created_at" => NOW(),
            ),
            array(
                "id" => 54,
                "name" => "Artisan Blend",
                "website" => "https://www.santafetile.com/clay-tile/artisan/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 10,
                "created_at" => NOW(),
            ),
            array(
                "id" => 55,
                "name" => "Modena",
                "website" => "https://www.crownrooftiles.com/modena_fl.html",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 11,        // Crown clay
                "created_at" => NOW(),
            ),
            array(
                "id" => 56,
                "name" => "Toledo",
                "website" => "https://www.crownrooftiles.com/toledo_fl.html",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 11,
                "created_at" => NOW(),
            ),
            array(
                "id" => 57,
                "name" => "Monaco",
                "website" => "https://www.crownrooftiles.com/monaco_fl.html",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 11,
                "created_at" => NOW(),
            ),

            array(
                "id" => 58,
                "name" => "Hidden Fastener",
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#hidden-fastener",
                "roof_manufacturer_id" => 10,                           // Gulf Coast Metals
                "roof_types_roof_manufacturers_matrix_id" => NULL,
                "created_at" => NOW(),
            ),
            array(
                "id" => 59,
                "name" => "Exposed Fastener",
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#exposed-fastener",
                "roof_manufacturer_id" => 10,
                "roof_types_roof_manufacturers_matrix_id" => NULL,
                "created_at" => NOW(),
            ),
            array(
                "id" => 60,
                "name" => "Stamped",
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/#stamped-metal",
                "roof_manufacturer_id" => 10,
                "roof_types_roof_manufacturers_matrix_id" => NULL,
                "created_at" => NOW(),
            ),

            array(
                "id" => 61,
                "name" => "MetalWorks",
                "website" => "https://www.tamko.com/docs/default-source/brochures-literature/TAMKO-metalworks-brochure.pdf?sfvrsn=a8b356a0_6",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 14,        // Tamko
                "created_at" => NOW(),
            ),
            array(
                "id" => 62,                                             // Start of Stone-Coated Steel
                "name" => "Boral Steel",
                "website" => "https://www.boralroof.com/product-profile/steel/",
                "roof_manufacturer_id" => NULL,
                "roof_types_roof_manufacturers_matrix_id" => 15,        // Boral
                "created_at" => NOW(),
            ),
            array(
                "id" => 63,
                "name" => "Cleo - Tile",                                // masquerades as "barrel" tile
                "website" => "https://www.roser-usa.com/cleo.html",
                "roof_manufacturer_id" => 11,                           // Roser
                "roof_types_roof_manufacturers_matrix_id" => 16,
                "created_at" => NOW(),
            ),
            array(
                "id" => 64,
                "name" => "Stonewood - Shake",                          // masquerades as dimensional shingle
                "roof_manufacturer_id" => 11,                           // Roser
                "roof_types_roof_manufacturers_matrix_id" => 16,
                "created_at" => NOW(),
            ),
            array(
                "id" => 65,
                "name" => "Piano - Shingle",                            // masquerades as wood shake
                "roof_manufacturer_id" => 11,                           // Roser
                "roof_types_roof_manufacturers_matrix_id" => 16,
                "created_at" => NOW(),
            ),

            array(
                "id" => 66,
                "name" => "Antica",
                "website" => "https://www.tilcorroofingusa.com/products/gallery/antica",
                "roof_manufacturer_id" => 12,                           // Tilcor
                "roof_types_roof_manufacturers_matrix_id" => 17,
                "created_at" => NOW(),
            ),
            array(
                "id" => 67,
                "name" => "Craftsman Shake",
                "website" => "https://www.tilcorroofingusa.com/products/gallery/craftsman-shake",
                "roof_manufacturer_id" => 12,                           // Tilcor
                "roof_types_roof_manufacturers_matrix_id" => 17,
                "created_at" => NOW(),
            ),
            array(
                "id" => 68,
                "name" => "CF Shingle",
                "website" => "https://www.tilcorroofingusa.com/products/gallery/cf-shingle",
                "roof_manufacturer_id" => 12,                           // Tilcor
                "roof_types_roof_manufacturers_matrix_id" => 17,
                "created_at" => NOW(),
            ),
            array(
                "id" => 69,
                "name" => "CF Shake",
                "website" => "https://www.tilcorroofingusa.com/products/gallery/cf-shake",
                "roof_manufacturer_id" => 12,                           // Tilcor
                "roof_types_roof_manufacturers_matrix_id" => 17,
                "created_at" => NOW(),
            ),

            array(
                "id" => 70,
                "name" => "Classic",
                "website" => "https://gerardroofs.eu/en/products/gerard-classic",
                "roof_manufacturer_id" => 13,                           // Gerard
                "roof_types_roof_manufacturers_matrix_id" => 18,
                "created_at" => NOW(),
            ),
            array(
                "id" => 71,
                "name" => "Diamant",
                "website" => "https://gerardroofs.eu/en/products/gerard-diamant",
                "roof_manufacturer_id" => 13,                           // Gerard
                "roof_types_roof_manufacturers_matrix_id" => 18,
                "created_at" => NOW(),
            ),
            array(
                "id" => 72,
                "name" => "Heritage",
                "website" => "https://gerardroofs.eu/en/products/gerard-heritage",
                "roof_manufacturer_id" => 13,                           // Gerard
                "roof_types_roof_manufacturers_matrix_id" => 18,
                "created_at" => NOW(),
            ),
            array(
                "id" => 73,
                "name" => "Milano",
                "website" => "https://gerardroofs.eu/en/products/gerard-milano",
                "roof_manufacturer_id" => 13,                           // Gerard
                "roof_types_roof_manufacturers_matrix_id" => 18,
                "created_at" => NOW(),
            ),
            array(
                "id" => 74,
                "name" => "Corona",
                "website" => "https://gerardroofs.eu/en/products/gerard-corona",
                "roof_manufacturer_id" => 13,                           // Gerard
                "roof_types_roof_manufacturers_matrix_id" => 18,
                "created_at" => NOW(),
            ),
            array(
                "id" => 75,
                "name" => "Senator",                                    // masquerades as dimensional shingle
                "website" => "https://gerardroofs.eu/en/products/gerard-senator-shingle",
                "roof_manufacturer_id" => 13,                           // Gerard
                "roof_types_roof_manufacturers_matrix_id" => 18,
                "created_at" => NOW(),
            ),
            array(
                "id" => 76,
                "name" => "Alphine",
                "website" => "https://gerardroofs.eu/en/products/gerard-alpine",
                "roof_manufacturer_id" => 13,                           // Gerard
                "roof_types_roof_manufacturers_matrix_id" => 18,
                "created_at" => NOW(),
            ),

            array(
                "id" => 77,
                "name" => "R-Panels",
                "website" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,                           // Permatile
                "roof_types_roof_manufacturers_matrix_id" => 19,
                "created_at" => NOW(),
            ),
            array(
                "id" => 78,
                "name" => "PBR-Panels",
                "website" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,                           // Permatile
                "roof_types_roof_manufacturers_matrix_id" => 19,
                "created_at" => NOW(),
            ),
            array(
                "id" => 79,
                "name" => "Advantage Panel",
                "website" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,                           // Permatile
                "roof_types_roof_manufacturers_matrix_id" => 19,
                "created_at" => NOW(),
            ),
            array(
                "id" => 80,
                "name" => "Permatile Panel",
                "website" => "https://www.advaluminum.com/roofing-panel-types/",
                "roof_manufacturer_id" => 14,                           // Permatile
                "roof_types_roof_manufacturers_matrix_id" => 19,
                "created_at" => NOW(),
            ),
            array(
                "id" => 81,
                "name" => "DecoTech® Solar System",
                "website" => "https://www.gaf.com/en-us/residential-roofing/decotech/panels",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 20,
                "created_at" => NOW(),
            ),
            array(
                "id" => 82,                                             // Start of Coatings series
                "name" => "Acrylic",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 83,
                "name" => "PMMA",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 84,
                "name" => "PVDF",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 85,
                "name" => "SEBS",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 86,
                "name" => "Silicone",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 87,
                "name" => "Urethane",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 88,
                "name" => "Liquid Membrane Systems",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 89,
                "name" => "Cleaners & Primers",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 90,
                "name" => "Sealants",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 91,
                "name" => "Accessories",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 21,
                "created_at" => NOW(),
            ),
            array(
                "id" => 92,
                "name" => "Asphalt Roofing",
                "roof_manufacturer_id" => 15,                           // Tropical
                "roof_types_roof_manufacturers_matrix_id" => 22,
                "created_at" => NOW(),
            ),
            array(
                "id" => 93,
                "name" => "Mastics & Cements",
                "roof_manufacturer_id" => 15,                           // Tropical
                "roof_types_roof_manufacturers_matrix_id" => 22,
                "created_at" => NOW(),
            ),
            array(
                "id" => 94,
                "name" => "Primers & Adhesives",
                "roof_manufacturer_id" => 15,                           // Tropical
                "roof_types_roof_manufacturers_matrix_id" => 22,
                "created_at" => NOW(),
            ),
            array(
                "id" => 95,
                "name" => "Reflective Products",
                "roof_manufacturer_id" => 15,                           // Tropical
                "roof_types_roof_manufacturers_matrix_id" => 22,
                "created_at" => NOW(),
            ),
            array(
                "id" => 96,
                "name" => "Accessory Products",
                "roof_manufacturer_id" => 15,                           // Tropical
                "roof_types_roof_manufacturers_matrix_id" => 22,
                "created_at" => NOW(),
            ),
            array(
                "id" => 97,
                "name" => "Spray Polyurethane Foam",
                "roof_manufacturer_id" => 15,                           // Tropical
                "roof_types_roof_manufacturers_matrix_id" => 22,
                "created_at" => NOW(),
            ),
            array(
                "id" => 98,
                "name" => "Elastomeric",
                "roof_manufacturer_id" => 16,                           // Polyglass
                "roof_types_roof_manufacturers_matrix_id" => 23,
                "created_at" => NOW(),
            ),
            array(
                "id" => 99,
                "name" => "Asphaltic",
                "roof_manufacturer_id" => 16,                           // Polyglass
                "roof_types_roof_manufacturers_matrix_id" => 23,
                "created_at" => NOW(),
            ),
            array(
                "id" => 100,
                "name" => "EPDM",
                "roof_manufacturer_id" => 17,                           // Firestone
                "roof_types_roof_manufacturers_matrix_id" => 24,
                "created_at" => NOW(),
            ),
            array(
                "id" => 101,                                            // Start of Modified Bitumen
                "name" => "APP Membranes",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 102,
                "name" => "SBS Membranes",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 103,
                "name" => "Insulation & Cover Boards",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 104,
                "name" => "Adhesives, Fasteners & LRF",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 105,
                "name" => "Asphaltic Primers, Sealants & Cement",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 106,
                "name" => "Asphaltic Accessories",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 107,
                "name" => "Perimeter Edge Metal",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 108,
                "name" => "Compatible Built-Up Roofing",
                "website" => "https://www.gaf.com/en-us/roofing-products/commercial-roofing-products/modified-bitumen-app-roofing-systems?Action=GetGrid",
                "roof_manufacturer_id" => 1,                            // GAF
                "roof_types_roof_manufacturers_matrix_id" => 25,
                "created_at" => NOW(),
            ),
            array(
                "id" => 109,
                "name" => "APP Membranes",
                "roof_manufacturer_id" => 16,                           // Polyglass
                "roof_types_roof_manufacturers_matrix_id" => 26,
                "created_at" => NOW(),
            ),
            array(
                "id" => 110,
                "name" => "SBS Membranes",
                "roof_manufacturer_id" => 16,                           // Polyglass
                "roof_types_roof_manufacturers_matrix_id" => 26,
                "created_at" => NOW(),
            ),
            array(
                "id" => 111,
                "name" => "Base, Felt & Synthetic Membranes",
                "roof_manufacturer_id" => 16,                           // Polyglass
                "roof_types_roof_manufacturers_matrix_id" => 26,
                "created_at" => NOW(),
            ),
            array(
                "id" => 112,
                "name" => "Accessories",
                "roof_manufacturer_id" => 16,                           // Polyglass
                "roof_types_roof_manufacturers_matrix_id" => 26,
                "created_at" => NOW(),
            ),
            array(
                "id" => 113,
                "name" => "Accessories",
                "roof_manufacturer_id" => 17,                           // Firestone
                "roof_types_roof_manufacturers_matrix_id" => 27,
                "created_at" => NOW(),
            ),
        );

        foreach ($roof_manufacturers_series_types as $series) {
            DB::table('roof_manufacturers_series_types')->insert($series);
        }
    }
}
