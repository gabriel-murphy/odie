<?php

use Illuminate\Database\Seeder;

class ClientLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /* This is the address for the headquarters of Roman Roofing (client ID = 1) and any
    future locations of clients of the Platform */

    public function run()
    {
        $locations = array(
            array(
                "id" => 1,
                "client_id" => "1",
                "name" => "Headquarters",
                "address_id" => "1",
                "created_at" => NOW(),
            )
        );
        foreach ($locations as $location) {
            DB::table("client_locations")->insert($location);
        }
    }
}
