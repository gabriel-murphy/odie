<?php

use Illuminate\Database\Seeder;

class RoofTypeManufacturersMatrixTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roof_types_roof_manufacturers_matrix = array(
            array(
                "id" => 1,
                "roof_types_id" => 1,                       // Start of manufacturers of Asphalt Shingle
                "roof_manufacturers_id" => 1,               // GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 2,
                "roof_types_id" => 1,
                "roof_manufacturers_id" => 2,               // OC
                "created_at" => NOW(),
            ),
            array(
                "id" => 3,
                "roof_types_id" => 1,
                "roof_manufacturers_id" => 3,               // IKO
                "created_at" => NOW(),
            ),
            array(
                "id" => 4,
                "roof_types_id" => 1,
                "roof_manufacturers_id" => 4,              // Tamko
                "created_at" => NOW(),
            ),
            array(
                "id" => 5,
                "roof_types_id" => 1,
                "roof_manufacturers_id" => 5,              // CertainTeed
                "created_at" => NOW(),
            ),
            array(
                "id" => 6,
                "roof_types_id" => 2,                       // Start of manufacturers of Concrete Tile
                "roof_manufacturers_id" => 6,               // Boral Roofing
                "name" => "Boral",
                "website" => "https://www.boralroof.com/product-profile/concrete/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 7,
                "roof_types_id" => 2,
                "roof_manufacturers_id" => 7,               // Eagle
                "created_at" => NOW(),
            ),
            array(
                "id" => 8,
                "roof_types_id" => 2,
                "roof_manufacturers_id" => 8,               // Crown
                "created_at" => NOW(),
            ),
            array(
                "id" => 9,
                "roof_types_id" => 3,                       // Start of manufacturers of Clay Tile
                "roof_manufacturers_id" => 6,
                "name" => "US Tile by Boral",
                "website" => "https://www.boralroof.com/product-profile/clay/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 10,
                "roof_types_id" => 3,
                "roof_manufacturers_id" => 9,               // Santafé
                "created_at" => NOW(),
            ),
            array(
                "id" => 11,
                "roof_types_id" => 3,
                "roof_manufacturers_id" => 8,               // Crown
                "created_at" => NOW(),
            ),
            array(
                "id" => 12,
                "roof_types_id" => 4,                       // Start of manufacturers of aluminum
                "roof_manufacturers_id" => 10,              // Gulf Coast
                "created_at" => NOW(),
            ),
            array(
                "id" => 13,
                "roof_types_id" => 5,                       // Start of manufacturers of steel
                "roof_manufacturers_id" => 10,              // Gulf Coast
                "created_at" => NOW(),
            ),
            array(
                "id" => 14,
                "roof_types_id" => 5,
                "roof_manufacturers_id" => 4,              // Tamko - Metalworks
                "name" => "MetalWorks",
                "website" => "https://www.tamko.com/docs/default-source/brochures-literature/TAMKO-metalworks-brochure.pdf?sfvrsn=a8b356a0_6",
                "created_at" => NOW(),
            ),
            array(
                "id" => 15,
                "roof_types_id" => 6,                      // Start of manufacturers of stone-coated steel
                "roof_manufacturers_id" => 6,              // BoralSteel
                "name" => "BoralSteel",
                "website" => "https://www.boralroof.com/product-profile/steel/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 16,
                "roof_types_id" => 6,                       // Start of manufacturers of stone-coated steel
                "roof_manufacturers_id" => 11,              // Roser
                "created_at" => NOW(),
            ),
            array(
                "id" => 17,
                "roof_types_id" => 6,
                "roof_manufacturers_id" => 12,              // Tilcor
                "created_at" => NOW(),
            ),
            array(
                "id" => 18,
                "roof_types_id" => 6,
                "roof_manufacturers_id" => 13,              // Gerand
                "created_at" => NOW(),
            ),
            array(
                "id" => 19,
                "roof_types_id" => 6,
                "roof_manufacturers_id" => 14,              // Permatile
                "created_at" => NOW(),
            ),
            array(
                "id" => 20,
                "roof_types_id" => 7,                       // Start of manufacturers of Solar
                "roof_manufacturers_id" => 1,               // GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 21,
                "roof_types_id" => 8,                       // Start of manufacturers of Coatings
                "roof_manufacturers_id" => 1,               // GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 22,
                "roof_types_id" => 8,
                "roof_manufacturers_id" => 15,              // Tropical
                "created_at" => NOW(),
            ),
            array(
                "id" => 23,
                "roof_types_id" => 8,
                "roof_manufacturers_id" => 16,              // Polyglass
                "created_at" => NOW(),
            ),
            array(
                "id" => 24,
                "roof_types_id" => 8,
                "roof_manufacturers_id" => 17,               // Firestone
                "created_at" => NOW(),
            ),
            array(
                "id" => 25,
                "roof_types_id" => 9,                       // Start of manufacturers of Modified Bitumen
                "roof_manufacturers_id" => 1,               // GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 26,
                "roof_types_id" => 9,
                "roof_manufacturers_id" => 16,              // Polyglass
                "created_at" => NOW(),
            ),
            array(
                "id" => 27,
                "roof_types_id" => 9,
                "roof_manufacturers_id" => 17,              // Firestone
                "created_at" => NOW(),
            ),
            array(
                "id" => 28,
                "roof_types_id" => 10,                      // Manufacturers of TPO
                "roof_manufacturers_id" => 1,               // GAF
                "created_at" => NOW(),
            ),
            array(
                "id" => 29,
                "roof_types_id" => 10,
                "roof_manufacturers_id" => 17,              // Firestone
                "created_at" => NOW(),
            ),
        );
        foreach ($roof_types_roof_manufacturers_matrix as $line) {
            DB::table('roof_types_roof_manufacturers_matrix')->insert($line);
        }
    }
}
