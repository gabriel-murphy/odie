<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MaterialsTableSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ConstantGroupsTableSeeder::class);
        $this->call(ConstantsTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(LeadSourcesTableSeeder::class);
        $this->call(AddressTableSeeder::class);
        $this->call(RoofManufacturersTableSeeder::class);
        $this->call(RoofTypesTableSeeder::class);
        $this->call(RoofManufacturerRoofTypeTableSeeder::class);
        $this->call(RoofProductsTableSeeder::class);
        $this->call(RoofProductColorsTableSeeder::class);
//        $this->call(ProductColorsSeeder::class);
        $this->call(PhasesTableSeeder::class);
        $this->call(TemplateTableSeeder::class);
        $this->call(PlaceholdersSeeder::class);
        $this->call(LaborTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);
        $this->call(StructuresTableSeeder::class);
        $this->call(WasteFactorsTableSeeder::class);
        $this->call(PermitJurisdictionsTableSeeder::class);
        $this->call(PermitTierFeeTableSeeder::class);
        $this->call(RoofLayersTableSeeder::class);
        $this->call(RoofComponentTableSeeder::class);
        $this->call(DumpFeesTableSeeder::class);
        $this->call(RoofBuildListTableSeeder::class);
        $this->call(CostsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);
        $this->call(InsuranceCarriersTableSeeder::class);
        $this->call(MaximusClaimsTableSeeder::class);
        $this->call(TempRepairsTableSeeder::class);
        $this->call(MaximusLegacyEmailLogTableSeeder::class);
        /*$this->call(USCitiesTableSeeder::class);*/
    }
}
