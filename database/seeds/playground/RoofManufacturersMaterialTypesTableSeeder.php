<?php

use Illuminate\Database\Seeder;

class RoofManufacturersMaterialTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into roof_manufacturers_material_types (name, roof_types_id) values (?, ?)', ['Asphalt', '1']);
        DB::insert('insert into roof_manufacturers_material_types (name, roof_types_id) values (?, ?)', ['Concrete', '2']);
        DB::insert('insert into roof_manufacturers_material_types (name, roof_types_id) values (?, ?)', ['Clay', '2']);
        DB::insert('insert into roof_manufacturers_material_types (name, roof_types_id) values (?, ?)', ['Aluminum', '3']);
        DB::insert('insert into roof_manufacturers_material_types (name, roof_types_id) values (?, ?)', ['Steel', '3']);
        DB::insert('insert into roof_manufacturers_material_types (name, roof_types_id) values (?, ?)', ['Photoelectric', '5']);
    }
}
