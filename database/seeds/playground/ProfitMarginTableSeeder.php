<?php

use Illuminate\Database\Seeder;

class ProfitMarginTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profit_margins = array(
            array(
                "id" => 1,
                "roof_types_id" => 1,
                "materials_markup" => 0.25,
                "labor_markup" => 0.10,
                "created_at" => NOW(),
            ),
            array(
                "id" => 2,
                "roof_types_id" => 2,
                "materials_markup" => 0.90,
                "labor_markup" => 0.20,
                "created_at" => NOW(),
            ),
            array(
                "id" => 3,
                "roof_types_id" => 3,
                "materials_markup" => 0.40,
                "labor_markup" => 0.10,
                "created_at" => NOW(),
            ),
            array(
                "id" => 4,
                "roof_types_id" => 4,
                "materials_markup" => 0.15,
                "labor_markup" => 0.30,
                "created_at" => NOW(),
            ),
            array(
                "id" => 5,
                "roof_types_id" => 5,
                "materials_markup" => 0.60,
                "labor_markup" => 0.30,
                "created_at" => NOW(),
            ),
        );
        foreach ($profit_margins as $profit_margin) {
            DB::table('profit_margins')->insert($profit_margin);
        }
    }
}
