<?php

use Illuminate\Database\Seeder;

class RoofManufacturersProfileTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.032', '30', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.040', '30', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '30', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '30', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '30', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '30', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['29 gauge', '30', '2', '5']);

        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.032', '31', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.040', '31', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '31', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '31', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '31', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '31', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['29 gauge', '31', '2', '5']);

        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.032', '32', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.040', '32', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '32', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '32', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '32', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '32', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['29 gauge', '32', '2', '5']);

        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.032', '33', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.040', '33', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '33', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '33', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '33', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '33', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['29 gauge', '33', '2', '5']);

        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.032', '34', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.040', '34', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '34', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '34', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '34', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '34', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['29 gauge', '34', '2', '5']);

        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.032', '35', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['0.040', '35', '1', '4']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '35', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '35', '1', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['24 gauge', '35', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['26 gauge', '35', '2', '5']);
        DB::insert('insert into roof_manufacturers_profile_types (name, roof_manufacturers_lines_id, roof_manufactures_finish_types_id, roof_manufacturers_material_types_id) values (?, ?, ?, ?)', ['29 gauge', '35', '2', '5']);

    }
}
