<?php

use Illuminate\Database\Seeder;

class CommissionStructureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $commissions = array(
            array(
                'client_id' => '1',
                'percent_sale' => 'Yes',
                'sale_percentage' => '0.05',
                'claim_bonus' => 'Yes',
                'claim_bonus_amount' => '1000',
            )
        );
        foreach ($commissions as $commission) {
            DB::table('commission_structure')->insert($commission);
        }
    }
}
