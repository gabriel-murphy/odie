<?php

use Illuminate\Database\Seeder;

class DefaultMaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	## Default material for deck prep (Roof Layer #1)
        DB::insert('insert into default_materials (client_id, roof_layers_id, material_id) values (?, ?, ?)', ['1', '1', '176']);			// Default of plywood for deck prep (roof layer 1) on all types of roofs
        DB::insert('insert into default_materials (client_id, roof_layers_id, material_id) values (?, ?, ?)', ['1', '1', '59']);

        ## Default material for underlayment (Roof Layer #3)
        DB::insert('insert into default_materials (client_id, roof_layers_id, roof_types_id, material_id) values (?, ?, ?, ?)', ['1', '3', '1', '271']);
        DB::insert('insert into default_materials (client_id, roof_layers_id, roof_types_id, material_id) values (?, ?, ?, ?)', ['1', '3', '2', '270']);
        DB::insert('insert into default_materials (client_id, roof_layers_id, roof_types_id, material_id) values (?, ?, ?, ?)', ['1', '3', '3', '269']);

    }
}


#            $table->bigInteger('client_id');
#            $table->bigInteger('roof_layers_id');                   //  The layer of the roof to which the default applies 
#            $table->bigInteger('material_id');                      // The material.id which is the default for something below.
#            $table->bigInteger('roof_types_id')->nullable();        
#            $table->bigInteger('roof_components_id')->nullable();
#            $table->bigInteger('roof_material_categories_id')->nullable();