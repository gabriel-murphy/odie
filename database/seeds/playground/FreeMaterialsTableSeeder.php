<?php

use Illuminate\Database\Seeder;

class FreeMaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $free_materials = array(
            array(
                'client_id' => '1',
                'roof_layers_id' => '1',
                'material_id' => '176',
                'quantity' => '2',
            ),
        );
        foreach ($free_materials as $free_material) {
            DB::table('free_materials')->insert($free_material);
        }
    }
}
