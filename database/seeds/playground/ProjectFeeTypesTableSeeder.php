<?php

use Illuminate\Database\Seeder;

class ProjectFeeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into project_fee_types (client_id, name, customer_view, active) values (?, ?, ?, ?)', ['1', 'Permit Fee', 1, 1]);
        DB::insert('insert into project_fee_types (client_id, name, customer_view, active, fee_per_project) values (?, ?, ?, ?, ?)', ['1', 'Odie Project Management Fee', 1, 1, '60.00']);
        DB::insert('insert into project_fee_types (client_id, name, customer_view, active, fee_per_project) values (?, ?, ?, ?, ?)', ['1', 'Fuel Surcharge', 1, 1, '86.00']);
        DB::insert('insert into project_fee_types (client_id, name, customer_view, active, fee_per_project) values (?, ?, ?, ?, ?)', ['1', 'Marketing Fee', 1, 1, '100.00']);
        DB::insert('insert into project_fee_types (client_id, name, customer_view, active) values (?, ?, ?, ?)', ['1', 'Commission', 0, 1]);
        DB::insert('insert into project_fee_types (client_id, name, customer_view, active) values (?, ?, ?, ?)', ['1', 'Dump Fees', 1, 1]);
    }
}
