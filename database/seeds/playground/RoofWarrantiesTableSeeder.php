<?php

use Illuminate\Database\Seeder;

class RoofWarrantiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roof_warranties = array(
            array(
                "id" => 1,
                "roof_manufacturers_id" => 1,
                "roof_types_id" => 1,
                "short_name" => "GAF Golden Pledge",
                "priced_per_square" => "Yes",
                "cost_per_square" => 9.00,
                "created_at" => NOW(),

            ),
        );
        foreach ($roof_warranties as $roof_warranty) {
            DB::table('roof_warranties')->insert($roof_warranty);
        }
    }
}
