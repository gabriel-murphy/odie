<?php

use Illuminate\Database\Seeder;

class RoofManufacturersProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    ## This is complete to coatings as of March 27, 2020

    public function run()
    {

        $roof_manufacturers_products = array(
            array(
                "id" => 1,
                "roof_manufacturers_series_types_id" => 1,                      // GAF Shingle - Timberline Series
                "name" => "HD",
                "code" => NULL,
                "website" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/timberline/architectural/timberline-hd",
                "created_at" => NOW(),
            ),
            array(
                "id" => 2,
                "roof_manufacturers_series_types_id" => 1,                      // GAF Shingle - Timberline Series
                "name" => "HDZ",
                "code" => NULL,
                "website" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/timberline/architectural/timberline-hdz",
                "created_at" => NOW(),
            ),
            array(
                "id" => 3,
                "roof_manufacturers_series_types_id" => 1,                      // GAF Shingle - Timberline Series
                "name" => "UHD",
                "code" => NULL,
                "website" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/timberline/architectural/timberline-uhd",
                "created_at" => NOW(),
            ),
            array(
                "id" => 4,
                "roof_manufacturers_series_types_id" => 2,                      // GAF Shingle - Designer Series
                "name" => "Slateline",
                "code" => NULL,
                "website" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/value-collection/slateline",
                "created_at" => NOW(),
            ),
            array(
                "id" => 5,
                "roof_manufacturers_series_types_id" => 2,                      // GAF Shingle - Designer Series
                "name" => "Camelot II",
                "code" => NULL,
                "website" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/designer/value-collection/camelot-ii",
                "created_at" => NOW(),
            ),
            array(
                "id" => 6,
                "roof_manufacturers_series_types_id" => 3,                      // GAF Shingle - 3-Tab Series
                "name" => "Royal Sovereign",
                "code" => NULL,
                "website" => "https://www.gaf.com/en-us/roofing-products/residential-roofing-products/shingles/3-tab/strip-shingles/royal-sovereign",
                "created_at" => NOW(),
            ),
            array(
                "id" => 7,
                "roof_manufacturers_series_types_id" => 4,                      // Owens Corning - Architectural Series
                "name" => "TruDefinition® Duration®",
                "code" => NULL,
                "website" => "https://www.owenscorning.com/roofing/shingles/trudefinition-duration/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 8,
                "roof_manufacturers_series_types_id" => 4,                      // Owens Corning - Architectural Series
                "name" => "TruDefinition® Duration® Designer",
                "code" => NULL,
                "website" => "https://www.owenscorning.com/roofing/shingles/trudefinition-duration-designer/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 9,
                "roof_manufacturers_series_types_id" => 4,                      // Shingle - Owens Corning - Architectural Series
                "name" => "Duration® Premium Cool",
                "code" => NULL,
                "website" => "https://www.owenscorning.com/roofing/shingles/duration-premium-cool/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 10,
                "roof_manufacturers_series_types_id" => 4,                      // Shingle - Owens Corning - Architectural Series
                "name" => "Oakridge®",
                "code" => NULL,
                "website" => "https://www.owenscorning.com/roofing/shingles/oakridge/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 11,
                "roof_manufacturers_series_types_id" => 4,                      // Shingle - Owens Corning - Architectural Series
                "name" => "TruDefinition® Oakridge",
                "code" => NULL,
                "website" => "https://www.owenscorning.com/roofing/shingles/trudefinition-oakridge/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 12,
                "roof_manufacturers_series_types_id" => 5,                      // Shingle - Owens Corning - Slate Look Series
                "name" => "Cambridge Cool Colors",
                "code" => NULL,
                "website" => "https://www.iko.com/na/wp-content/uploads/sites/8/Cambridge-Brochure---Contractors-Edition-min.pdf",
                "created_at" => NOW(),
            ),

            array(
                "id" => 13,
                "roof_manufacturers_series_types_id" => 6,                      // Shingle - Owens Corning - 3-Tab Series
                "name" => "Cambridge Cool Colors",
                "code" => NULL,
                "website" => "https://www.iko.com/na/wp-content/uploads/sites/8/Cambridge-Brochure---Contractors-Edition-min.pdf",
                "created_at" => NOW(),
            ),
            array(
                "id" => 14,
                "roof_manufacturers_series_types_id" => 7,                      // IKO - Shingle - Designer Series
                "name" => "Armourshake",                                        // masquerades as wood shake
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/designer/armourshake/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 15,
                "roof_manufacturers_series_types_id" => 7,                      // IKO - Shingle - Designer Series
                "name" => "Crowne Slate",                                       // masquerades as slate
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/designer/crowne-slate/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 16,
                "roof_manufacturers_series_types_id" => 7,                      // IKO - Shingle - Designer Series
                "name" => "Royal Estate",                                       // masquerades as tile (poorly!)
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/designer/royal-estate/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 17,
                "roof_manufacturers_series_types_id" => 8,                      // IKO - Shingle - Performance Series
                "name" => "Dynasty",
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/performance/dynasty/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 18,
                "roof_manufacturers_series_types_id" => 8,                      // IKO - Shingle - Performance Series
                "name" => "Nordic",
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/performance/nordic/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 19,
                "roof_manufacturers_series_types_id" => 9,                      // IKO - Shingle - Architectural Series
                "name" => "Cambridge",
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/architectural/cambridge/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 20,
                "roof_manufacturers_series_types_id" => 9,                      // IKO - Shingle - Architectural Series
                "name" => "Cambridge Cool Colors",
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/architectural/cambridge-cool-colors/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 21,
                "roof_manufacturers_series_types_id" => 10,                      // IKO - Shingle - Traditional Series
                "name" => "Marathon Plus AR",
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/three-tab/marathon-plus-ar/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 22,
                "roof_manufacturers_series_types_id" => 10,                      // IKO - Shingle - Traditional Series
                "name" => "Marathon Plus AR",
                "code" => NULL,
                "website" => "https://www.iko.com/na/residential-roofing-shingles/three-tab/marathon-plus-ar/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 23,
                "roof_manufacturers_series_types_id" => 11,                     // Shingle - Tamko - Heritage Series
                "name" => "Heritage",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/heritage",
                "created_at" => NOW(),
            ),
            array(
                "id" => 24,
                "roof_manufacturers_series_types_id" => 11,                     // Shingle - Tamko - Heritage Series
                "name" => "Heritage Premium",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/heritage-premium",
                "created_at" => NOW(),
            ),
            array(
                "id" => 25,
                "roof_manufacturers_series_types_id" => 11,                     // Shingle - Tamko - Heritage Series
                "name" => "Heritage IR",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/heritage-ir",
                "created_at" => NOW(),
            ),
            array(
                "id" => 26,
                "roof_manufacturers_series_types_id" => 11,                     // Shingle - Tamko - Heritage Series
                "name" => "Heritage Woodgate",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/heritage-woodgate",
                "created_at" => NOW(),
            ),
            array(
                "id" => 27,
                "roof_manufacturers_series_types_id" => 11,                     // Shingle - Tamko - Heritage Series
                "name" => "Heritage Vintage",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/heritage-vintage",
                "created_at" => NOW(),
            ),
            array(
                "id" => 28,
                "roof_manufacturers_series_types_id" => 12,                     // Shingle - Tamko - 3-Tab Series
                "name" => "Elite Glass-Seal",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/elite-glass-seal",
                "created_at" => NOW(),
            ),

            array(
                "id" => 29,
                "roof_manufacturers_series_types_id" => 13,                     // Shingle - CertainTeed - Shake Series
                "name" => "Presidential Shake® TL",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/presidential-shake-tl/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 30,
                "roof_manufacturers_series_types_id" => 13,                     // Shingle - CertainTeed - Shake Series
                "name" => "Presidential Shake®",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/presidential-shake/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 31,
                "roof_manufacturers_series_types_id" => 13,                     // Shingle - CertainTeed - Shake Series
                "name" => "Landmark® Pro",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/landmark-pro/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 32,
                "roof_manufacturers_series_types_id" => 13,                     // Shingle - CertainTeed - Shake Series
                "name" => "Landmark® Premium",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/landmark-premium/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 33,
                "roof_manufacturers_series_types_id" => 13,                     // Shingle - CertainTeed - Shake Series
                "name" => "Landmark Solaris® Platinum",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/landmark-solaris-platinum/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 34,
                "roof_manufacturers_series_types_id" => 13,                     // Shingle - CertainTeed - Shake Series
                "name" => "Landmark®",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/landmark/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 35,
                "roof_manufacturers_series_types_id" => 14,                     // Shingle - CertainTeed - Slate Series
                "name" => "Grand Manor®",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/grand-manor/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 36,
                "roof_manufacturers_series_types_id" => 14,                     // Shingle - CertainTeed - Slate Series
                "name" => "Carriage House®",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/carriage-house/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 37,
                "roof_manufacturers_series_types_id" => 14,                     // Shingle - CertainTeed - Slate Series
                "name" => "Belmont®",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/belmont/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 38,
                "roof_manufacturers_series_types_id" => 14,                     // Shingle - CertainTeed - Slate Series
                "name" => "Belmont® IR",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/belmont-ir/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 39,
                "roof_manufacturers_series_types_id" => 14,                     // Shingle - CertainTeed - Slate Series
                "name" => "Highland Slate®",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/highland-slate/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 40,
                "roof_manufacturers_series_types_id" => 15,                     // Shingle - CertainTeed - 3-Tab Series
                "name" => "XT™ 30 IR",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/xt-30-ir/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 41,
                "roof_manufacturers_series_types_id" => 15,                     // Shingle - CertainTeed - 3-Tab Series
                "name" => "XT™ 25",
                "code" => NULL,
                "website" => "https://www.certainteed.com/residential-roofing/products/xt-25/",
                "created_at" => NOW(),
            ),

            array(
                "id" => 42,
                "roof_manufacturers_series_types_id" => 58,                     // Metal (Aluminum) - Gulf Coast - Hidden Fastener Series
                "name" => "GulfLok™",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulflok/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 43,
                "roof_manufacturers_series_types_id" => 58,                     // Metal (Aluminum) - Gulf Coast - Hidden Fastener Series
                "name" => "GulfSeam™",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfseam/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 44,
                "roof_manufacturers_series_types_id" => 58,                     // Metal - Gulf Coast - Hidden Fastener Series
                "name" => "VersaLoc™ & MegaLoc™",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/versaloc-megaloc/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 45,
                "roof_manufacturers_series_types_id" => 59,                     // Metal - Gulf Coast - Exposed Fastener Series
                "name" => "GulfRib™",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfrib/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 46,
                "roof_manufacturers_series_types_id" => 59,                     // Metal - Gulf Coast - Exposed Fastener Series
                "name" => "5V Crimp™",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/5v-crimp/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 47,
                "roof_manufacturers_series_types_id" => 59,                     // Metal - Gulf Coast - Exposed Fastener Series
                "name" => "GulfPBR™",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfpbr/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 48,
                "roof_manufacturers_series_types_id" => 59,                     // Metal - Gulf Coast - Hidden Fastener Series
                "name" => "GulfWave™",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/gulfwave/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 49,
                "roof_manufacturers_series_types_id" => 60,                     // Metal - Gulf Coast - Stamped Series
                "name" => "Centura Steel Shingle",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/centura-steel-shingle/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 50,
                "roof_manufacturers_series_types_id" => 60,                     // Metal - Gulf Coast - Stamped Series
                "name" => "Grande Tile",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/grandetile/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 51,
                "roof_manufacturers_series_types_id" => 60,                     // Metal - Gulf Coast - Stamped Series
                "name" => "Great American Shake",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/great-american-shake/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 52,
                "roof_manufacturers_series_types_id" => 60,                     // Metal - Gulf Coast - Stamped Series
                "name" => "Oxford Shingle",
                "code" => NULL,
                "website" => "https://www.gulfcoastsupply.com/metal-roofing/metal-roof-types/oxford-shingle/",
                "created_at" => NOW(),
            ),

            array(
                "id" => 53,
                "roof_manufacturers_series_types_id" => 61,                     // Metal - Tamko - MetalWorks
                "name" => "StoneCrest Slate",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/metalworks-stonecrest-slate",
                "created_at" => NOW(),
            ),
            array(
                "id" => 54,
                "roof_manufacturers_series_types_id" => 61,                     // Metal - Tamko - MetalWorks
                "name" => "StoneCrest Tile",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/metalworks-stonecrest-tile",
                "created_at" => NOW(),
            ),
            array(
                "id" => 55,
                "roof_manufacturers_series_types_id" => 61,                     // Metal - Tamko - MetalWorks
                "name" => "Aston Wood",
                "code" => NULL,
                "website" => "https://www.tamko.com/styles/metalworks-astonwood",
                "created_at" => NOW(),
            ),

            array(
                "id" => 56,
                "roof_manufacturers_series_types_id" => 62,                     // Metal - Boral - BoralSteel
                "name" => "Pine Crest Shake",                                   // masquerades as wood shake
                "code" => NULL,
                "website" => "https://www.boralroof.com/product-profile/steel/pine-crest-shake/4DAP93185SF/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 57,
                "roof_manufacturers_series_types_id" => 62,                     // Metal - Boral - BoralSteel
                "name" => "Granite Ridge",                                      // masquerades as 3-tab shingle
                "code" => NULL,
                "website" => "https://www.boralroof.com/product-profile/steel/granite-ridge-shingle/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 58,
                "roof_manufacturers_series_types_id" => 62,                     // Metal - Boral - BoralSteel
                "name" => "Cottage Shingle",                                    // masquerades as dimensional shingle
                "code" => NULL,
                "website" => "https://www.boralroof.com/product-profile/steel/cottage-shingle/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 59,
                "roof_manufacturers_series_types_id" => 62,                     // Metal - Boral - BoralSteel
                "name" => "Barrel Vault Tile",                                  // masquerades as tile
                "code" => NULL,
                "website" => "https://www.boralroof.com/product-profile/steel/barrel-vault-tile/",
                "created_at" => NOW(),
            ),
            array(
                "id" => 60,
                "roof_manufacturers_series_types_id" => 62,                     // Metal - Boral - BoralSteel
                "name" => "Pacific Tile",
                "code" => NULL,
                "website" => "https://www.boralroof.com/product-profile/steel/pacific-tile/",
                "created_at" => NOW(),
            ),


        );

        foreach ($roof_manufacturers_products as $products) {
            DB::table('roof_manufacturers_products')->insert($products);
        }

    }
}
