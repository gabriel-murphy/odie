<?php

use Illuminate\Database\Seeder;

class RoofManufacturersFinishTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('insert into roof_manufacturers_finish_types (name, detail, roof_manufacturers_id) values (?, ?, ?)', ['Kynar500', 'PVDF Resin Technology', '6']);
        DB::insert('insert into roof_manufacturers_finish_types (name, detail, roof_manufacturers_id) values (?, ?, ?)', ['SMP Paint', 'SMP Paint', '6']);
        DB::insert('insert into roof_manufacturers_finish_types (name, detail, roof_manufacturers_id) values (?, ?, ?)', ['Standard Mill', 'Mill', '6']);
    }
}

