<?php

use Illuminate\Database\Seeder;

class CredentialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credentials = array(
            array(
                "id" => 1,
                "service_name" => "MailGun",
                "service_url" => "mailgun.org",
                "service_username" => "gabriel@gabrielmurphy.com",
                "service_password" => "Gabriel2020!",
                "api_key" => "71857126b7eebc8fc23a87751da10f5d-c322068c-5525acb8",
                "api_secret" => "pubkey-b8c694834543be83b1914a2fe1e649a9",
                "validation_key" => "71857126b7eebc8fc23a87751da10f5d-c322068c-5525acb8",
            ),
            array(
                "id" => 2,
                "service_name" => "Google Cloud Platform",
                "service_url" => "console.cloud.google.com",
                "service_username" => "gabriel@gabrielmurphy.com",
                "service_password" => "Gabriel2020!",
                "api_key" => "AIzaSyDtsoWEUduR1G8IylC7M1YefQO1b4REHNc",
            ),
            array(
                "id" => 3,
                "service_name" => "Nexmo",
                "service_url" => "nexmo.com",
                "service_username" => "gabriel@gabrielmurphy.com",
                "service_password" => "Gabriel2020!",
                "api_key" => "55bdc272",
                "api_secret" => "3OILM4uRsTYGwoXk",
            ),
            array(
                "id" => 4,
                "service_name" => "People Data Labs",
                "service_url" => "peopledatalabs.com",
                "service_username" => "gabriel@odie.pro",
                "service_password" => "Gabriel2020!",
                "api_key" => "00589398e2476e8bd3b0d292d42dfe2b683f913ea7232f93dfca5f87693b9775",
            ),
            array(
                "id" => 5,
                "service_name" => "Google Cloud Platform",
                "service_url" => "cloud.google.com",
                "api_key" => "AIzaSyDtsoWEUduR1G8IylC7M1YefQO1b4REHNc",
            ),
            array(
                "id" => 6,
                "service_name" => "Estated",
                "service_url" => "estated.com",
                "service_username" => "gabriel@romanroofing.com",
                "service_password" => "Gabriel2020!",
                "service_endpoint" => "https://apis.estated.com/v4/property",
                "api_key" => "y5XMUqFDryVGdZ2QTdxzbTk76pWRdJ",
            ),
        );

        foreach ($credentials as $credential) {
            DB::table('credentials')->insert($credential);
        }
    }
}
