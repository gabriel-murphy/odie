<?php

use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $colors = array(
            array(
                "id" => 1,
                "name" => "Barkwood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/1440swatches/timberline_hd-barkwood.jpg",
            ),
            array(
                "id" => 2,
                "name" => "Birchwood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-birchwood.jpg",
            ),
            array(
                "id" => 3,
                "name" => "Charcoal",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-charcoal.jpg",
            ),
            array(
                "id" => 4,
                "name" => "Driftwood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-driftwood.jpg",
            ),
            array(
                "id" => 5,
                "name" => "Hickory",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-hickory.jpg",
            ),
            array(
                "id" => 6,
                "name" => "Hunter Green",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-hunter_green.jpg",
            ),
            array(
                "id" => 7,
                "name" => "Mission Brown",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-mission-brown.jpg",
            ),
            array(
                "id" => 8,
                "name" => "Oyster Grey",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-oyster_gray.jpg",
            ),
            array(
                "id" => 9,
                "name" => "Pewter Gray",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-pewter_gray.jpg",
            ),
            array(
                "id" => 10,
                "name" => "Shakewood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-shakewood.jpg",
            ),
            array(
                "id" => 11,
                "name" => "Slate",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-slate.jpg",
            ),
            array(
                "id" => 12,
                "name" => "Sunset Brick",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-sunset_brick.jpg",
            ),
            array(
                "id" => 13,
                "name" => "Weathered Wood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-weathered_wood.jpg",
            ),
            array(
                "id" => 14,
                "name" => "White",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline_hd-white.jpg",
            ),
            array(
                "id" => 15,
                "name" => "Barkwood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline-uhd/timberline-ultra-hd_barkwood_720x720.jpg",
            ),
            array(
                "id" => 16,
                "name" => "Charcoal",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline-uhd/timberline-ultra-hd_charcoal_720x720.jpg",
            ),
            array(
                "id" => 17,
                "name" => "Pewter Gray",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline-uhd/timberline-ultra-hd_pewter-gray_720x720.jpg",
            ),
            array(
                "id" => 18,
                "name" => "Shakewood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline-uhd/tl-uhd_shakewood_720.jpg",
            ),
            array(
                "id" => 19,
                "name" => "Slate",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline-uhd/timberline_ultra_hd-slate.jpg",
            ),
            array(
                "id" => 20,
                "name" => "Weathered Wood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/timberline-uhd/timberline_ultra_hd-weathered_wood.jpg",
            ),
            array(
                "id" => 21,
                "name" => "Antique Slate",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/slateline-antique_slate.jpg",
            ),
            array(
                "id" => 22,
                "name" => "English Gray",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/slateline-english_gray.jpg",
            ),
            array(
                "id" => 23,
                "name" => "Royal Slate",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/slateline-royal_slate.jpg",
            ),
            array(
                "id" => 24,
                "name" => "Weathered Slate",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/slateline-weathered_slate.jpg",
            ),
            array(
                "id" => 25,
                "name" => "Antique Slate",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/camelot_ii-antique_slate.jpg",
            ),
            array(
                "id" => 26,
                "name" => "Barkwood",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/camelot_ii-barkwood.jpg",
            ),
            array(
                "id" => 27,
                "name" => "Charcoal",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/camelot_ii-charcoal.jpg",
            ),
            array(
                "id" => 28,
                "name" => "Royal Slate",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/camelot_ii-royal_slate.jpg",
            ),
            array(
                "id" => 29,
                "name" => "Weathered Timber",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/camelot_ii-weathered_timber.jpg",
            ),
            array(
                "id" => 30,
                "name" => "Autumn Brown",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-autumn_brown.jpg",
            ),
            array(
                "id" => 31,
                "name" => "Autumn Brown",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-autumn_brown.jpg",
            ),
            array(
                "id" => 32,
                "name" => "Charcoal",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-charcoal.jpg",
            ),
            array(
                "id" => 33,
                "name" => "Cypress Tan",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-cypress_tan.jpg",
            ),
            array(
                "id" => 34,
                "name" => "Golden Cedar",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-golden_cedar.jpg",
            ),
            array(
                "id" => 35,
                "name" => "Russet Red",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-russet_red.jpg",
            ),
            array(
                "id" => 36,
                "name" => "Sandrift",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-sandrift.jpg",
            ),
            array(
                "id" => 37,
                "name" => "Silver Lining",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-silver_lining.jpg",
            ),
            array(
                "id" => 38,
                "name" => "Weathered Gray",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-weathered_gray.jpg",
            ),
            array(
                "id" => 39,
                "name" => "White",
                "image_url" => "https://www.gaf.com/-/media/shingleswatches/720x720swatches/royal_sovereign-white.jpg",
            ),
            array(
                "id" => 40,
                "name" => "Amber",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/203/original/PYS_TruDefDur_Amber_768x768_72dpi.jpg?1503324904",
            ),
            array(
                "id" => 41,
                "name" => "Antique Silver",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/204/original/PYS_TruDefDur_AntiqueSilver_768x768_72dpi.jpg?1503324905",
            ),
            array(
                "id" => 42,
                "name" => "Brownwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/205/original/PYS_TruDefDur_Brownwood_768x768_72dpi.jpg?1503324906",
            ),
            array(
                "id" => 43,
                "name" => "Chateau Green",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/206/original/PYS_TruDefDur_ChateauGreen_768x768_72dpi.jpg?1503324907",
            ),
            array(
                "id" => 44,
                "name" => "Desert Tan",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/208/original/PYS_TruDefDur_DesertTan_768x768_72dpi.jpg?1503324909",
            ),
            array(
                "id" => 45,
                "name" => "Driftwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/209/original/PYS_TruDefDur_Driftwood_768x768_72dpi.jpg?1503324911",
            ),
            array(
                "id" => 46,
                "name" => "Estate Gray",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/210/original/PYS_TruDefDur_EstateGray_768x768_72dpi.jpg?1503324912",
            ),
            array(
                "id" => 47,
                "name" => "Harbor Blue",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/211/original/PYS_TruDefDur_HarborBlue_768x768_72dpi.jpg?1503324913",
            ),
            array(
                "id" => 48,
                "name" => "Onyx Black",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/212/original/PYS_TruDefDur_OnyxBlack_768x768_72dpi.jpg?1503324914",
            ),
            array(
                "id" => 49,
                "name" => "Quarry Gray",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/214/original/PYS_TruDefDur_QuarryGray_768x768_72dpi.jpg?1503324916",
            ),
            array(
                "id" => 50,
                "name" => "Shasta White",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/215/original/PYS_TruDefDur_ShastaWhite_768x768_72dpi.jpg?1503324917",
            ),
            array(
                "id" => 51,
                "name" => "Terra Cotta",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/219/original/PYS_TruDefDur_TerraCotta_768x768_72dpi.jpg?1503324921",
            ),

            array(
                "id" => 52,
                "name" => "Aged Copper",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/228/original/PYS_TruDefDesigner_AgedCopper_768x768_72dpi.jpg?1503324928",
            ),
            array(
                "id" => 53,
                "name" => "Black Sable",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/294/original/PYS_TruDefDesigner_BlackSable_768x768_72dpi.jpg?1540219115",
            ),
            array(
                "id" => 54,
                "name" => "Merlot",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/229/original/PYS_TruDefDesigner_Merlot_768x768_72dpi.jpg?1503324929",
            ),
            array(
                "id" => 55,
                "name" => "Pacific Wave",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/230/original/PYS_TruDefDesigner_PacificWave_768x768_72dpi.jpg?1503324930",
            ),
            array(
                "id" => 56,
                "name" => "Sand Dune",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/231/original/PYS_TruDefDesigner_SandDune_768x768_72dpi.jpg?1503324932",
            ),
            array(
                "id" => 57,
                "name" => "Sedona Canyon",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/232/original/PYS_TruDefDesigner_SedonaCanyon_768x768_72dpi.jpg?1503324933",
            ),
            array(
                "id" => 58,
                "name" => "Storm Cloud",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/233/original/PYS_TruDefDesigner_StormCloud_768x768_72dpi.jpg?1503324935",
            ),
            array(
                "id" => 59,
                "name" => "Summer Harvest",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/234/original/PYS_TruDefDesigner_SummerHarvest_768x768_72dpi.jpg?1503324936",
            ),
            array(
                "id" => 60,
                "name" => "Frosted Oak",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/247/original/PYS_DPCool_FrostedOak_768x768_72dpi.jpg?1503324944",
            ),
            array(
                "id" => 61,
                "name" => "Harbor Fog",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/248/original/PYS_DPCool_HarborFot_768x768_72dpi.jpg?1503324945",
            ),
            array(
                "id" => 62,
                "name" => "Sage",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/249/original/PYS_DPCool_Sage_768x768_72dpi.jpg?1503324946",
            ),
            array(
                "id" => 63,
                "name" => "Sunrise",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/250/original/PYS_DPCool_Sunrise_768x768_72dpi.jpg?1503324947",
            ),
            array(
                "id" => 64,
                "name" => "Antique Silver",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/184/original/PYS_Oakridge_AntiqueSilver_768x768_72dpi.jpg?1503324888",
            ),
            array(
                "id" => 65,
                "name" => "Beachwood Sand",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/185/original/PYS_Oakridge_BeachwoodSand_768x768_72dpi.jpg?1503324888",
            ),
            array(
                "id" => 66,
                "name" => "Brownwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/187/original/PYS_Oakridge_Brownwood_768x768_72dpi.jpg?1503324889",
            ),
            array(
                "id" => 67,
                "name" => "Desert Tan",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/189/original/PYS_Oakridge_DesertTan_768x768_72dpi.jpg?1503324891",
            ),
            array(
                "id" => 68,
                "name" => "Driftwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/190/original/PYS_Oakridge_Driftwood_768x768_72dpi.jpg?1503324892",
            ),
            array(
                "id" => 69,
                "name" => "Estate Gray",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/191/original/PYS_Oakridge_EstateGray_768x768_72dpi.jpg?1503324893",
            ),
            array(
                "id" => 70,
                "name" => "Onyx Black",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/191/original/PYS_Oakridge_EstateGray_768x768_72dpi.jpg?1503324893",
            ),

            array(
                "id" => 70,
                "name" => "Brownwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/170/original/open-uri20190702-8119-1r00xv0?1562072333",
            ),
            array(
                "id" => 71,
                "name" => "Desert Tan",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/172/original/open-uri20190702-8119-1sqrz97?1562072336",
            ),
            array(
                "id" => 72,
                "name" => "Driftwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/173/original/open-uri20190702-8119-1jlgbku?1562072338",
            ),
            array(
                "id" => 73,
                "name" => "Estate Gray",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/174/original/open-uri20190702-8119-rtnlnl?1562072340",
            ),
            array(
                "id" => 74,
                "name" => "Onyx Black",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/176/original/open-uri20190702-8119-14ohvt9?1562072342",
            ),
            array(
                "id" => 75,
                "name" => "Shasta White",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/178/original/open-uri20190702-8119-12gwryv?1562072345",
            ),
            array(
                "id" => 76,
                "name" => "Canterbury Black",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/136/original/PYS_Berk_CanterburyBlack_768x768_72dpi.jpg?1503324851",
            ),
            array(
                "id" => 77,
                "name" => "Colonial",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/137/original/PYS_Berk_Colonial_768x768_72dpi.jpg?1503324853",
            ),
            array(
                "id" => 78,
                "name" => "Concord",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/138/original/PYS_Berk_Concord_768x768_72dpi.jpg?1503324854",
            ),
            array(
                "id" => 79,
                "name" => "Manchster Gray",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/139/original/PYS_Berk_ManchesterGREY_768x768_72dpi.jpg?1503324855",
            ),
            array(
                "id" => 80,
                "name" => "Sherwood Belge",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/140/original/PYS_Berk_SherwoodBeige_768x768_72dpi.jpg?1503324856",
            ),
            array(
                "id" => 81,
                "name" => "Antique Silver",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/252/original/PYS_Supreme_AntiqueSilver_768x768_72dpi.jpg?1503324949",
            ),
            array(
                "id" => 82,
                "name" => "Aspen Gray",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/253/original/PYS_Supreme_AspenGray_768x768_72dpi.jpg?1503324950",
            ),
            array(
                "id" => 83,
                "name" => "Autumn Brown",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/254/original/PYS_Supreme_AutumnBrown_768x768_72dpi.jpg?1503324951",
            ),
            array(
                "id" => 84,
                "name" => "Beachwood Sand",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/256/original/PYS_Supreme_BeachwoodSand_768x768_72dpi.jpg?1503324953",
            ),
            array(
                "id" => 85,
                "name" => "Brownwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/257/original/PYS_Supreme_Brownwood_768x768_72dpi.jpg?1503324953",
            ),
            array(
                "id" => 86,
                "name" => "Chateau Green",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/259/original/PYS_Supreme_ChateauGreen_768x768_72dpi.jpg?1503324955",
            ),
            array(
                "id" => 87,
                "name" => "Desert Tan",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/260/original/PYS_Supreme_DesertTan_768x768_72dpi.jpg?1503324956",
            ),
            array(
                "id" => 88,
                "name" => "Driftwood",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/261/original/PYS_Supreme_Driftwood_768x768_72dpi.jpg?1503324957",
            ),
            array(
                "id" => 89,
                "name" => "Estate Gray",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/262/original/PYS_Supreme_EstateGray_768x768_72dpi.jpg?1503324958",
            ),
            array(
                "id" => 90,
                "name" => "Onyx Black",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/264/original/PYS_Supreme_OnyxBlack_768x768_72dpi.jpg?1503324960",
            ),
            array(
                "id" => 91,
                "name" => "Shasta White",
                "image_url" => "https://dcpd6wotaa0mb.cloudfront.net/mdms/uploads/shingle_colors/swatch_768x768s/000/000/266/original/PYS_Supreme_ShastaWhite_768x768_72dpi.jpg?1503324962",
            ),


/*
            array(
                "id" => 28,
                "name" => "Amber",
            ),
            array(
                "id" => 29,
                "name" => "Antique Silver",
            ),
            array(
                "id" => 30,
                "name" => "Brownwood",
            ),
            array(
                "id" => 31,
                "name" => "Chateau Green",
            ),
            array(
                "id" => 32,
                "name" => "Desert Tan",
            ),
            array(
                "id" => 33,
                "name" => "Driftwood",
            ),
            array(
                "id" => 34,
                "name" => "Estate Gray",
            ),
            array(
                "id" => 35,
                "name" => "Harbor Blue",
            ),
            array(
                "id" => 36,
                "name" => "Onyx Black",
            ),
            array(
                "id" => 37,
                "name" => "Quarry Gray",
            ),
            array(
                "id" => 38,
                "name" => "Shasta White",
            ),
            array(
                "id" => 39,
                "name" => "Terra Cotta",
            ),
            array(
                "id" => 40,
                "name" => "Arrowhead Gray",
            ),
            array(
                "id" => 41,
                "name" => "Viera Blend",
            ),
            array(
                "id" => 42,
                "name" => "Arcadia Canyon Brown",
            ),
            array(
                "id" => 43,
                "name" => "Sierra Madre",
            ),
            array(
                "id" => 44,
                "name" => "Boca Cream",
            ),
            array(
                "id" => 45,
                "name" => "Royal Palm",
            ),
            array(
                "id" => 46,
                "name" => "Santa Paula",
            ),
            array(
                "id" => 47,
                "name" => "Dark Gray Range",
            ),
            array(
                "id" => 48,
                "name" => "Dark Charcoal",
            ),
            array(
                "id" => 49,
                "name" => "Concord Blend",
            ),
            array(
                "id" => 50,
                "name" => "Sanborn Blend",
            ),
            array(
                "id" => 51,
                "name" => "Bloomingdale Blend",
            ),
            array(
                "id" => 52,
                "name" => "Floridian Blend",
            ),
            array(
                "id" => 53,
                "name" => "Mount Dora Blend",
            ),
            array(
                "id" => 54,
                "name" => "Terracambra Range",
            ),
            array(
                "id" => 55,
                "name" => "Light Gray Range",
            ),
            array(
                "id" => 56,
                "name" => "Brown Gray Range",
            ),
            array(
                "id" => 57,
                "name" => "Kona Red Range",
            ),
            array(
                "id" => 58,
                "name" => "Cocoa Range",
            ),
            array(
                "id" => 59,
                "name" => "Walnut Creek Blend",
            ),
            array(
                "id" => 60,
                "name" => "White on White",
            ),
            array(
                "id" => 61,
                "name" => "Rocklin Blend",
            ),
            array(
                "id" => 62,
                "name" => "Hillsborough Blend",
            ),
            array(
                "id" => 63,
                "name" => "Chatham Blend",
            ),
            array(
                "id" => 64,
                "name" => "Heathrow Blend",
            ),
            array(
                "id" => 65,
                "name" => "Amesbury Blend",
            ),
            array(
                "id" => 66,
                "name" => "Providence Blend",
            ),
            array(
                "id" => 67,
                "name" => "Windermere Blend",
            ),
            array(
                "id" => 68,
                "name" => "Plymouth Blend",
            ),
            array(
                "id" => 69,
                "name" => "Moss Creek",
            ),
            array(
                "id" => 70,
                "name" => "Arcadia Canyon Brown",
            ),
            array(
                "id" => 71,
                "name" => "Sierra Madre",
            ),
            array(
                "id" => 72,
                "name" => "Rockledge",
            ),
            array(
                "id" => 73,
                "name" => "Brown Gray Range",
            ),
            array(
                "id" => 74,
                "name" => "Terracotta Gold",
            ),
            array(
                "id" => 75,
                "name" => "Cayenne",
            ),
            array(
                "id" => 76,
                "name" => "Sanibel",
            ),
            array(
                "id" => 77,
                "name" => "Viera Blend",
            ),
            array(
                "id" => 78,
                "name" => "Sierra Madre",
            ),
            array(
                "id" => 79,
                "name" => "Boca Cream",
            ),
            array(
                "id" => 80,
                "name" => "Weathered Terracotta Flashed",
            ),
            array(
                "id" => 81,
                "name" => "Santa Paula",
            ),
            array(
                "id" => 82,
                "name" => "Alhambra",
            ),
            array(
                "id" => 83,
                "name" => "Arcadia Canyon Brown",
            ),
            array(
                "id" => 84,
                "name" => "Concord Blend",
            ),
            array(
                "id" => 85,
                "name" => "Sanborn Blend",
            ),
            array(
                "id" => 86,
                "name" => "Carlsbad Blend",
            ),
            array(
                "id" => 87,
                "name" => "Bloomingdale Blend",
            ),
            array(
                "id" => 88,
                "name" => "Rancho Cordova Blend",
            ),
            array(
                "id" => 89,
                "name" => "Floridian Blend",
            ),
            array(
                "id" => 90,
                "name" => "Sunrise Blend",
            ),
            array(
                "id" => 91,
                "name" => "Juno Blend",
            ),
            array(
                "id" => 92,
                "name" => "Terracambra Range",
            ),
            array(
                "id" => 93,
                "name" => "Light Gray Range",
            ),
            array(
                "id" => 94,
                "name" => "San Rafael Blend",
            ),
            array(
                "id" => 95,
                "name" => "Brown Gray Range",
            ),
            array(
                "id" => 96,
                "name" => "Buena Vista Blend",
            ),
            array(
                "id" => 97,
                "name" => "Kona Red Range",
            ),
            array(
                "id" => 98,
                "name" => "Northdale Blend",
            ),
            array(
                "id" => 99,
                "name" => "Coral Springs Blend",
            ),
            array(
                "id" => 100,
                "name" => "Adobe Blend",
            ),
            array(
                "id" => 101,
                "name" => "Palm Beach Blend",
            ),
            array(
                "id" => 102,
                "name" => "Cocoa Range",
            ),
            array(
                "id" => 103,
                "name" => "Walnut Creek Blend",
            ),
            array(
                "id" => 104,
                "name" => "Flintridge Gray",
            ),
            array(
                "id" => 105,
                "name" => "Rockledge",
            ),
            array(
                "id" => 106,
                "name" => "Wildwood",
            ),
            array(
                "id" => 107,
                "name" => "Arcadia Canyon Brown",
            ),
            array(
                "id" => 108,
                "name" => "Brown Gray Range",
            ),
            array(
                "id" => 109,
                "name" => "Charcoal Range",
            ),
            array(
                "id" => 110,
                "name" => "Terracotta Gold",
            ),
            array(
                "id" => 111,
                "name" => "Riviera",
            ),
            array(
                "id" => 112,
                "name" => "Rosewood",
            ),
            array(
                "id" => 113,
                "name" => "Sierra Madre",
            ),
            array(
                "id" => 114,
                "name" => "Boca Cream",
            ),
            array(
                "id" => 115,
                "name" => "Royal Palm",
            ),
            array(
                "id" => 116,
                "name" => "Sandy Bay",
            ),
            array(
                "id" => 117,
                "name" => "Santa Paula",
            ),
            array(
                "id" => 118,
                "name" => "Alhambra",
            ),
            array(
                "id" => 119,
                "name" => "Arcadia Canyon Brown",
            ),
            array(
                "id" => 120,
                "name" => "Sanborn Blend",
            ),
            array(
                "id" => 121,
                "name" => "Floridian Blend",
            ),
            array(
                "id" => 122,
                "name" => "Terracambra Range",
            ),
            array(
                "id" => 123,
                "name" => "Kona Red Range",
            ),
            array(
                "id" => 124,
                "name" => "Palm Beach Blend",
            ),
            array(
                "id" => 125,
                "name" => "Cocoa Range",
            ),
            array(
                "id" => 126,
                "name" => "Cocoa Range",
            ),
            array(
                "id" => 127,
                "name" => "White on White",
            ),
            array(
                "id" => 128,
                "name" => "Maple Forge",
            ),
            array(
                "id" => 129,
                "name" => "Moss Creek",
            ),
            array(
                "id" => 130,
                "name" => "Arcadia Canyon Brown",
            ),
            array(
                "id" => 131,
                "name" => "Sierra Madre",
            ),
            array(
                "id" => 132,
                "name" => "Rockledge",
            ),
            array(
                "id" => 133,
                "name" => "Brown Gray Range",
            ),
            array(
                "id" => 134,
                "name" => "Chatham Blend",
            ),
            array(
                "id" => 135,
                "name" => "Heathrow Blend",
            ),
            array(
                "id" => 136,
                "name" => "Amesbury Blend",
            ),
            array(
                "id" => 137,
                "name" => "Providence Blend",
            ),
            array(
                "id" => 138,
                "name" => "Windermere Blend",
            ),
            array(
                "id" => 139,
                "name" => "Plymouth Blend",
            ),
            array(
                "id" => 140,
                "name" => "Arcadia Canyon Brown",
            ),
            array(
                "id" => 141,
                "name" => "Dark Charcoal",
            ),
            array(
                "id" => 142,
                "name" => "Concord Blend",
            ),
            array(
                "id" => 143,
                "name" => "Kings Canyon Blend",
            ),
            array(
                "id" => 144,
                "name" => "Mount Dora Blend",
            ),
            array(
                "id" => 145,
                "name" => "Catskill",
            ),
            array(
                "id" => 146,
                "name" => "Aged Copper",
            ),
            array(
                "id" => 147,
                "name" => "Ash Grey",
            ),
            array(
                "id" => 148,
                "name" => "Brook Blue",
            ),
            array(
                "id" => 149,
                "name" => "Charcoal Gray",
            ),
            array(
                "id" => 150,
                "name" => "Cobalt Sone",
            ),
            array(
                "id" => 151,
                "name" => "Colonial Red",
            ),
            array(
                "id" => 152,
                "name" => "Copper",
            ),
            array(
                "id" => 153,
                "name" => "Dark Bronze",
            ),
            array(
                "id" => 154,
                "name" => "Evergreen",
            ),
            array(
                "id" => 155,
                "name" => "Mansard Brown",
            ),
            array(
                "id" => 156,
                "name" => "Matte Black",
            ),
            array(
                "id" => 157,
                "name" => "Medium Bronze",
            ),
            array(
                "id" => 158,
                "name" => "Napa Champagne",
            ),
            array(
                "id" => 159,
                "name" => "Nevada Silver",
            ),
            array(
                "id" => 160,
                "name" => "Pre-Weathered",
            ),
            array(
                "id" => 161,
                "name" => "Regal Red",
            ),
            array(
                "id" => 162,
                "name" => "Regal White",
            ),
            array(
                "id" => 163,
                "name" => "Sandstone",
            ),
            array(
                "id" => 164,
                "name" => "Sierra Tan",
            ),
            array(
                "id" => 165,
                "name" => "Slate Gray",
            ),
            array(
                "id" => 166,
                "name" => "Solar White",
            ),
            array(
                "id" => 167,
                "name" => "Terra Cotta",
            ),
            array(
                "id" => 168,
                "name" => "Barn Red",
            ),
            array(
                "id" => 169,
                "name" => "Black",
            ),
            array(
                "id" => 170,
                "name" => "Bronze",
            ),
            array(
                "id" => 171,
                "name" => "Burgundy",
            ),
            array(
                "id" => 172,
                "name" => "Charcoal Gray",
            ),
            array(
                "id" => 173,
                "name" => "Clay",
            ),
            array(
                "id" => 174,
                "name" => "Cocoa Brown",
            ),
            array(
                "id" => 175,
                "name" => "Evergreen",
            ),
            array(
                "id" => 176,
                "name" => "Forest Green",
            ),
            array(
                "id" => 177,
                "name" => "Gallery Blue",
            ),
            array(
                "id" => 178,
                "name" => "Hawaiian Blue",
            ),
            array(
                "id" => 179,
                "name" => "Ivory",
            ),
            array(
                "id" => 180,
                "name" => "Light Gray",
            ),
            array(
                "id" => 181,
                "name" => "Light Stone",
            ),
            array(
                "id" => 182,
                "name" => "Marine Green",
            ),
            array(
                "id" => 183,
                "name" => "Mocha Tan",
            ),
            array(
                "id" => 184,
                "name" => "Patina Green",
            ),
            array(
                "id" => 185,
                "name" => "Patriot Red",
            ),
            array(
                "id" => 186,
                "name" => "Patriot White",
            ),
            array(
                "id" => 187,
                "name" => "Pure White",
            ),
            array(
                "id" => 188,
                "name" => "Mill Finish",
            ),
            array(
                "id" => 189,
                "name" => "Charcoal",
            ),
            array(
                "id" => 190,
                "name" => "Garnet",
            ),
            array(
                "id" => 191,
                "name" => "Mission Gold",
            ),
            array(
                "id" => 192,
                "name" => "Mission Red",
            ),
            array(
                "id" => 193,
                "name" => "Seafoam Green",
            ),
            array(
                "id" => 194,
                "name" => "Spanish Orange",
            ),
            array(
                "id" => 195,
                "name" => "Sunrise",
            ),
            array(
                "id" => 196,
                "name" => "Timberwood",
            ),
            array(
                "id" => 197,
                "name" => "Vista Cotta",
            ),
            array(
                "id" => 198,
                "name" => "Walnut",
            ),
            array(
                "id" => 199,
                "name" => "Birch",
            ),
            array(
                "id" => 200,
                "name" => "Cedar",
            ),
            array(
                "id" => 201,
                "name" => "Charcoal",
            ),
            array(
                "id" => 202,
                "name" => "Coffee Brown",
            ),
            array(
                "id" => 203,
                "name" => "Shadow Wood",
            ),
            array(
                "id" => 204,
                "name" => "Sun Rise",
            ),
            array(
                "id" => 205,
                "name" => "Terra Cotta",
            ),
            array(
                "id" => 206,
                "name" => "Walnut",
            ),
            array(
                "id" => 207,
                "name" => "Weathered Timber",
            ),*/
        );

        foreach ($colors as $color) {
#            DB::table('colors')->insert($color);
             DB::insert('insert into colors (name) values (?)', [$color["name"]]);
        }
    }
}


        /*
        ## GAF first Palette for 14 Timberline HD colors
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Barkwood']);        #1
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Birchwood']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Charcoal']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Driftwood']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Hickory']);         #5
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Hunter Green']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Mission Brown']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Oyster Grey']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Pewter Gray']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Shakewood']);       #10
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Slate']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Sunset Brick']);    // Not avail in HTZ
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Weathered Wood']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'White']);           #14  // Not avail in HTZ

        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Antique Slate']);   #15
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'English Gray']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Royal Slate']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Weathered Slate']); #18

        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Royal Slate']); #19 (first new color in Camelot II Line)
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Weathered Timber']);

        ## All new colors from 3-Tab
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Autumn Brown']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Cypress Tan']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Golden Cedar']);  #23
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Russet Red']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Sandrift']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Silver Lining']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['1', 'Weathered Gray']); #27

        # Done with GAF, onto OC and their color palette - 12 colors in Driftwood
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Amber']);        #8
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Antique Silver']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Brownwood']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Chateau Green']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Desert Tan']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Driftwood']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Estate Gray']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Harbor Blue']);     #35
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Onyx Black']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Quarry Gray']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Shasta White']);
        DB::insert('insert into colors (roof_manufacturers_id, name) values (?, ?)', ['2', 'Terra Cotta']);     #39

        ## Eagle - Bel Air color options
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Arrowhead Gray', '4209', 'Hues of Gray, Smoke Gray Flashed']);     #40
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Viera Blend', '4477', 'Blend of Dark Brown, Tan']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Arcadia Canyon Brown', '4502', ' Hues of Dark Brown, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Sierra Madre', '4503', ' Hues of Dark Brown, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Boca Cream', '4507', ' Hues of Mocha, Cream Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Royal Palm', '4516', ' Hues of Gray, White Streaks']); # 45
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Santa Paula', '4549', ' Hues of Tan, Cream, Brown Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Dark Gray Range', '4591', 'Range of Dark Gray']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Dark Charcoal', '4595', 'Hues of Dark Charcoal, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Concord Blend', '4602', 'Blend of Charcoal, Tan']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Sanborn Blend', '4603', 'Blend of Terracotta, Brownish Tan, Black Streaks']);  #50
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Bloomingdale Blend', '4620', 'Blend of Gray, Eggplant, Gold']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Floridian Blend', '4629', 'Blend of Terracotta, Brown Red, Light Gray, Black Streaking']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Mount Dora Blend', '4655', 'Blend of Gray, White, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Terracambra Range', '4664', 'Range of Terracotta']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Light Gray Range', '4679', 'Range of Light Gray']); #55
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Brown Gray Range', '4687', 'Range of Brown, Gray']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Kona Red Range', '4698', 'Range of Red']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Cocoa Range', '4743', 'Range of Dark Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Walnut Creek Blend', '4773', 'Blend of Tan, Orange, Dark Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'White on White', '4800', 'Hues of White on White']); #60
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Rocklin Blend', '4804', 'Blend of Brown, Light Brown, Mocha']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Hillsborough Blend', '4883', 'Blend of Brown, Natural Gray, Light Brown, W/Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Chatham Blend', '49101', 'Blend of Tan, Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Heathrow Blend', '49102', 'Blend of Brown, Light Brown, Mocha']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Amesbury Blend', '49103', 'Blend of Brown, Natural Gray, Light Brown, W/Streaks']); #65
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Providence Blend', '49104', 'Blend of Tan, Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Windermere Blend', '49105', 'Blend of Dark Brown, Charcoal']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Plymouth Blend', '49106', 'Blend of Grays']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Moss Creek', '49507', 'Hues of Green, Dark Green Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Arcadia Canyon Brown', '5502', 'Hues of Dark Brown, Black Streaks']); #70
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Sierra Madre', '5503', 'Hues of Charcoal, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Rockledge', '56046', 'Hues of Dark Charcoal, Rust, Red Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['8', 'Brown Gray Range', '5687', 'Range of Brown, Gray']);

        ## Eagle (Manufacturer) - Capistrano ()
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Terracotta Gold', '3118', 'Hues of Terracotta, Gold Flashed']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Cayenne', '3233', 'Range of Red Color Bonded over Red Through Color']);  #75
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Sanibel', '383', 'Hues of Reddish Orange, Gold, Gray Flashed']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Viera Blend', '3477', 'Blend of Dark Brown, Tan']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Sierra Madre', '3503', 'Hues of Charcoal, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Boca Cream', '3507', 'Hues of Mocha, Cream Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Weathered Terracotta Flashed', '3520', 'Hues of Light Terracotta, Black, Red Streaks']); #80
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Santa Paula', '3549', 'Hues of Light Terracotta, Black, Red Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Alhambra', '3555', 'Hues of Terracotta, Yellow Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Arcadia Canyon Brown', '3581', 'Hues of Dark Brown, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Concord Blend', '3602', 'Blend of Charcoal, Tan']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Sanborn Blend', '3603', 'Blend of Terracotta, Brownish Tan, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Carlsbad Blend', '3604', 'Blend of Terracotta, Brown, Yellow Streaking']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Bloomingdale Blend', '3620', 'Blend of Gray, Eggplant, Gold']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Rancho Cordova Blend', '3626', 'Blend of Green, Gold, Purple']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Floridian Blend', '3629', 'Blend of Terracotta, Brown Red, Light Gray, Black Streaking']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Sunrise Blend', '3645', 'Blend of Terracotta, Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Juno Blend', '3661', 'Blend of Terracotta, Brown, Red, Light Gray']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Terracambra Range', '3664', 'Range of Terracotta']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Light Gray Range', '3679', 'Range of Light Gray']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'San Rafael Blend', '3684', 'Blend of Purple, Tan']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Brown Gray Range', '3687', 'Range of Brown, Gray']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Buena Vista Blend', '3688', 'Blend of Light Gray, Terracotta, Tan']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Kona Red Range', '3698', 'Range of Red']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Northdale Blend', '3713', 'Blend of Reddish Brown, Gold, Gray, Brown Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Coral Springs Blend', '3714', 'Blend of Coral, Periwinkle, Orange, Yellow']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Adobe Blend', '3723', 'Blend of Maroon, Terracotta, Gray, Gold']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Palm Beach Blend', '3725', 'Blend of Eggplant, Gold, Coral']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Cocoa Range', '3743', 'Range of Dark Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['9', 'Walnut Creek Blend', '3773', 'Blend of Tan, Orange, Dark Brown']);

        ## Eagle (Manufacturer) - Double Eagle ()
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['10', 'Flintridge Gray', '4011', 'Hues of Natural Gray, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['10', 'Rockledge', '4046', 'Hues of Dark Charcoal, Rust, Red Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['10', 'Wildwood', '40517', 'Hues of Tan, Maroon, Black Streaks']);

        ## Eagle (Manufacturer) - Golden Eagle ()
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['11', 'Arcadia Canyon Brown', '1502', 'Hues of Dark Brown, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['11', 'Brown Gray Range', '1687', 'Range of Brown, Gray']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['11', 'Charcoal Range', '1699', 'Range of Gray']);

        ## Eagle (Manufacturer) - Malibu ()
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Terracotta Gold', '2118', 'Hues of Terracotta, Gold Flashed']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Riviera', '2282', 'Hues of Terracotta, Yellow, Brown Flashed']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Rosewood', '2424', 'Hues of Reddish Orange, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Sierra Madre', '2503', 'Hues of Charcoal, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Boca Cream', '2507', 'Hues of Mocha, Cream Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Royal Palm', '2516', 'Hues of Gray, White Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Sandy Bay', '2518', 'Hues of Tan, Rust, Gray Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Santa Paula', '2549', 'Hues of Tan, Cream, Brown Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Alhambra', '2555', 'Hues of Terracotta, Yellow Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Arcadia Canyon Brown', '2581', 'Hues of Dark Brown, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Sanborn Blend', '2603', 'Blend of Terracotta, Brownish Tan, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Floridian Blend', '2629', 'Blend of Terracotta, Brown Red, Light Gray, Black Streaking']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Terracambra Range', '2664', 'Range of Terracotta']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Kona Red Range', '2698', 'Range of Red']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Palm Beach Blend', '2725', 'Blend of Eggplant, Gold, Coral']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Cocoa Range', '2643', 'Range of Dark Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Cocoa Range', '2773', 'Blend of Tan, Orange, Dark Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'White on White', '2800', 'Hues of White on White']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Maple Forge', '29501', 'Hues of Red Orange, Maroon Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['12', 'Moss Creek', '29507', 'Hues of Green, Dark Green Streaks']);

        ## Eagle (Manufacturer) - Ponderosa ()
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['13', 'Arcadia Canyon Brown', '5502', 'Hues of Dark Brown, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['13', 'Sierra Madre', '5503', 'Hues of Charcoal, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['13', 'Rockledge', '56046', 'Hues of Dark Charcoal, Rust, Red Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['13', 'Brown Gray Range', '5687', 'Range of Brown, Gray']);

        ## Eagle (Manufacturer) - Textured Slate ()
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['14', 'Chatham Blend', '49101', 'Blend of Tan, Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['14', 'Heathrow Blend', '49102', 'Blend of Greens']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['14', 'Amesbury Blend', '49103', 'Blend of Medium Gray, Dark Gray, Maroon']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['14', 'Providence Blend', '49104', 'Blend of Light Gray, Medium Gray, Maroon']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['14', 'Windermere Blend', '49105', 'Blend of Dark Brown, Charcoal']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['14', 'Plymouth Blend', '49106', 'Blend of Grays']);

        ## Eagle (Manufacturer) - Tapered Slate ()
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['15', 'Arcadia Canyon Brown', '49581', 'Hues of Dark Brown, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['15', 'Dark Charcoal', '49595', 'Hues of Dark Charcoal, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['15', 'Concord Blend', '49602', 'Blend of Charcoal, Tan']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['15', 'Kings Canyon Blend', '49634', 'Blend of Dark Green, Tan, Red and Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['15', 'Mount Dora Blend', '49655', 'Blend of Gray, White, Black Streaks']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name, code, description) values (?, ?, ?, ?)', ['15', 'Catskill', '49718', 'Hues of Moss Green, Dark Green, Green, White Flashed']);

        ## Gulf Coast Metails (colors tied to finish type - Kynar)
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Aged Copper', '0.47', '0.85', '53']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Ash Grey', '0.39', '0.84', '41']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Brook Blue', '0.29', '0.85', '28']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Charcoal Gray', '0.29', '0.84', '28']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Cobalt Sone', '0.26', '0.85', '24']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Colonial Red', '0.33', '0.85', '34']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Copper', '0.49', '0.85', '55']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Dark Bronze', '0.26', '0.84', '24']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Evergreen', '0.27', '0.86', '26']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Mansard Brown', '0.27', '0.86', '26']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Matte Black', '0.27', '0.86', '26']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Medium Bronze', '0.30', '0.87', '31']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Napa Champagne', '0.37', '0.80', '37']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Nevada Silver', '0.60', '0.77', '68']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Pre-Weathered', '0.30', '0.79', '27']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Regal Red', '0.42', '0.84', '45']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Regal White', '0.68', '0.86', '82']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Sandstone', '0.54', '0.86', '63']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Sierra Tan', '0.35', '0.86', '37']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Slate Gray', '0.36', '0.84', '37']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Solar White', '0.68', '0.85', '82']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['1', 'Terra Cotta', '0.35', '0.87', '37']);

        ## Gulf Coast Metals (colors tied to finish type - SMP)
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Barn Red', '0.36', '0.84', '37']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Black', '0.25', '0.84', '23']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Bronze', '0.30', '0.85', '30']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Burgundy', '0.24', '0.83', '21']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Charcoal Gray', '0.86', '0.85', '24']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Clay', '0.34', '0.86', '35']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Cocoa Brown', '0.32', '0.85', '32']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Evergreen', '0.27', '0.86', '26']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Forest Green', '0.31', '0.85', '31']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Gallery Blue', '0.25', '0.86', '24']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Hawaiian Blue', '0.32', '0.85', '32']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Ivory', '0.60', '0.83', '70']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Light Gray', '0.34', '0.85', '35']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Light Stone', '0.55', '0.85', '64']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Marine Green', '0.36', '0.86', '38']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Mocha Tan', '0.44', '0.84', '48']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Patina Green', '0.29', '0.87', '29']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Patriot Red', '0.40', '0.83', '42']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Patriot White', '0.63', '0.85', '75']);
        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['2', 'Pure White', '0.68', '0.84', '82']);

        DB::insert('insert into colors (roof_manufacturers_finish_types_id, name, reflectivity, emissivity, sri) values (?, ?, ?, ?, ?)', ['3', 'Mill Finish', '0.69', '0.06', '55']);

        ## Roser - Tile
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Charcoal']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Garnet']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Mission Gold']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Mission Red']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Seafoam Green']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Spanish Orange']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Sunrise']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Timberwood']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Vista Cotta']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['19', 'Walnut']);

        ## Roser - Shake
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Birch']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Cedar']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Charcoal']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Coffee Brown']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Shadow Wood']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Sun Rise']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Terra Cotta']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Walnut']);
        DB::insert('insert into colors (roof_manufacturers_series_types_id, name) values (?, ?)', ['20', 'Weathered Timber']);
    */
