@props(['id' => 'node', 'icon' => asset('images/file_icons/folder.png'), 'title' => 'node'])
<li>
    <a href="javascript:void(0)" class="plus"><i class="fal fa-plus-square mr-2"></i></a>
    <input id="{{ $id }}" type="checkbox"/>
    <label for="{{ $id }}">
        <img src="{{ $icon }}" width="12" height="12" class="mr-1" alt="icon">
        {{ $title }}
    </label>
    <ul>{{ $slot }}</ul>
</li>
