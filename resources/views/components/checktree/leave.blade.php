@props([
'id' => $value, 'icon' => asset('images/file_icons/file.png'),
'name' => 'options', 'title' => 'leave', 'value' => 1
])

<li>
    <input id="{{ $id }}" type="checkbox" name="{{ $name }}[]" value="{{ $value }}"/>
    <label for="{{ $id }}">
        <img src="{{ $icon }}" width="12" height="12" class="mr-1" alt="icon">{{ $title }}
    </label>
</li>
