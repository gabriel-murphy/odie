<div class="row items-push">
    @foreach($attachments as $attachment)
        <div class="col-2">
            <div class="img-container preview-img mb-4 file-viewer" style="cursor: pointer"
                 data-path="{{ $attachment->getUrl() }}">
                @if($attachment->isMissing('thumb'))
                    <img class="preview-img" src="{{ $attachment->getFallbackImage() }}" alt="">
                @else
                    <img class="preview-img" src="{{ $attachment->getUrl('thumb') }}" alt="">
                @endif

                <div class="img-container-layer">
                    <div class="details">
                        <img class="mr-2" src="{{ $attachment->icon }}" alt=""
                             width="12" height="12">
                        <div class="name">
                            <p >{{ $attachment->name_with_extension }}</p>
                            <p class="mt-2 text-muted">{{ $attachment->human_readable_size }}</p>
                        </div>
                    </div>

                </div>
                <div class="image-flex-wrapper">
                    <div class="image-flex">
                        <div class="flex-details">
                            <img src="{{ $attachment->icon }}" alt="" width="12"
                                 height="12">
                            <p class="mb-0 ml-1 text-truncate" style="font-size: 12px">{{ $attachment->name }}</p>
                        </div>
                        <div class="shape-wrapper">
                            <div class="shape">
                                <div class="top-triangle"></div>
                                <div class="bottom-triangle"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endforeach
</div>


{{--<div class="btn btn-light file-viewer m-2" style="height: 170px; min-width: 140px" data-path="{{ $attachment->getUrl() }}">
    @if($attachment->isMissing('thumb'))
        <img src="{{ $attachment->getFallbackImage() }}" alt="">
    @else
        <img src="{{ $attachment->getUrl('thumb') }}" alt="">
    @endif
</div>--}}
