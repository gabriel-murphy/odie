<div class="modal fade border-0" id="review_email_{{$template->id}}">
    <div class="modal-dialog modal-lg text-left">
        <div class="modal-content">
            <div class="modal-body">

                <div class="form-group">
                    <label for="subject">Subject:</label>
                    <input id="subject" type="text" name="template[subject]"
                           class="form-control @error('template.subject') is-invalid @enderror"
                           value="{{ old('template.subject', isset($template) ? $template->subject : '') }}">
                    @include('includes.partials.error', ['field' => 'template.subject'])
                </div>
                <div class="form-group">
                    <label for="content">Email Content: </label>
                    <textarea name="template[content]" class="js-summernote @error('template.content') is-invalid @enderror">{{ old('content', isset($template) ? $template->content : '') }}</textarea>
                    @include('includes.partials.error', ['field' => 'template.content'])
                </div>

                <div class="form-group">
                    <label>Available Placeholders:</label>
                    <p>
                        @forelse($template->placeholders as $placeholder)
                            <span class="mr-4 text-primary">%%{{ $placeholder['name'] }}%%</span>
                        @empty
                            <span class="mr-4 ">no place holder available</span>
                        @endforelse
                    </p>
                </div>

                <div class="float-right">
                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Update and Stay</button>
                    <button type="submit" class="btn btn-sm btn-success">Update and Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
