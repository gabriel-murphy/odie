<button type="button"
        class="btn btn-primary btn-sm"
        data-toggle="modal"
        data-target="#{{$target}}">{{ $text }}
</button>
