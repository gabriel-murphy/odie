<div class="js-pie-chart pie-chart" data-percent="{{ $percentage }}"
     data-line-width="8" data-size="150" data-bar-color="{{ $color }}"
     title="{{ $title }}: {{ $count }}" data-toggle="tooltip" data-placement="bottom">
    <span>
        {{ $title }}<br>
        <small class="text-muted">{{ $percentage }}</small>
    </span>
</div>
