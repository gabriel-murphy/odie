<div class="progress push">
    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
         role="progressbar" style="width: {{ $percentage }};" >
        <span class="progress-bar-label">{{ $percentage }}</span>
    </div>
</div>
