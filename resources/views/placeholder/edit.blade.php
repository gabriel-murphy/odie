@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Placeholder</a>
@endsection
@section('breadcrumb-active', 'Edit')

@section('content')
    @include('includes.partials.errors')
    <div class="block">
        <div class="block-header block-header-default">Edit Placeholder</div>
        <div class="block-content">
            @include('includes.partials.errors')
            <form action="{{ route('placeholders.update', $placeholder->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                @include('placeholder.fields')
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">Edit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
