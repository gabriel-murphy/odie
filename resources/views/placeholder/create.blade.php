@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Placeholder</a>
@endsection
@section('breadcrumb-active', 'Create')


@section('content')
    <div class="block">
        <div class="block-header block-header-default">Add Placeholder</div>
        <div class="block-content">
            @include('includes.partials.errors')
            <form action="{{ route('placeholders.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @include('placeholder.fields')
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

