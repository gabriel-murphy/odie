<div class="form-group">
    <label for="model">Model:</label>
    <select name="model" id="model"
            class="js-select2 form-control @error('model') is-invalid @enderror">
        <option value="">Select Option</option>
        @forelse($placeholder->allowable_models as $model)
            <option value="{{ $model }}"
                {{isset($placeholder) && ucfirst($placeholder->model) == $model ? 'selected' : ''}}
                {{ (collect(old('model'))->contains($model)) ? 'selected':'' }}>
                {{$model }}
            </option>
        @empty
        @endforelse
    </select>
    @include('includes.partials.error', ['field' => 'model'])
</div>
<div class="form-group">
    <label for="attribute">Attribute:</label>
    <input id="attribute" type="text" name="attribute"
           class="form-control @error('attribute') is-invalid @enderror"
           value="{{ old('attribute', isset($placeholder) ? $placeholder->attribute : '') }}">
    @include('includes.partials.error', ['field' => 'attribute'])

</div>
<div class="form-group ">
    <label for="name">Name:</label>
    <input id="name" type="text" name="name"
           class="form-control @error('name') is-invalid @enderror"
           value="{{ old('name', isset($placeholder) ? $placeholder->name : '') }}">
    @include('includes.partials.error', ['field' => 'name'])
</div>
