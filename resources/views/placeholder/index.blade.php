@extends('layouts.backend')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Placeholders List</h3>
            <div class="block-options">
                @include('includes.common.search', ['route' => route(Route::currentRouteName())])
            </div>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr class="text-uppercase">
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Attribute</th>
                        <th>Model</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($placeholders as $placeholder)
                        <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="font-w600">{{ $placeholder->name }}</td>
                        <td>{{ $placeholder->attribute }}</td>
                        <td>{{ $placeholder->model }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ route('placeholders.edit', $placeholder->id ) }}"
                                   class="btn btn-sm btn-secondary"
                                   data-toggle="tooltip" title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <button data-toggle="modal" data-target="#confirm_placeholder_{{ $placeholder->id }}"
                                        class="btn btn-sm btn-outline-danger" title="">
                                    <i class="fa fa-trash"></i></button>
                                 @include('includes.modals.confirm', ['model' => 'placeholder', 'route' => route('placeholders.destroy', $placeholder->id), 'form' => true])
                            </div>
                        </td>
                    </tr>
                    @empty
                        <td colspan="5" class="text-center text-secondary">no record found</td>
                    @endforelse
                    </tbody>
                </table>

                  <div class="pagination justify-content-center">
                      {{ $placeholders->links() }}
                  </div>
            </div>
        </div>
    </div>
@endsection
