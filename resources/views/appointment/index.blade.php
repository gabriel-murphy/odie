@extends('layouts.backend')

@section('styles')
    <link href="{{ asset('js/plugins/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet'>
    <style>
        .fc-content{
            text-align: center;
            padding: 10px;
            color: white;
        }
    </style>
@endsection

@section('css_before')
    <link href="{{ asset('js/plugins/flatpickr/flatpickr.css') }}" rel='stylesheet'>
    <link href="{{ asset('js/plugins/select2/css/select2.min.css') }}" rel='stylesheet'>
@endsection

@section('content-heading', 'Step 2 - Schedule Appointment for %%CUSTOMER%%')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Opportunity</a>
@endsection
@section('breadcrumb-active', 'Schedule')

@section('content-header-action')
    <button  class="btn btn-sm btn-primary" onclick="openAppointmentForm(true)">
        <span class="btn-label">
            <i class="fa fa-plus-circle"></i>
        </span>Add Appointment
    </button>
@endsection

@section('content')
    <div class="block">
        <div class="block-content">
            <div id="calendar" class="mb-5"></div>
        </div>
    </div>

    <div class="modal fade come-from-modal right " id="appointmentFormModal" role="dialog">
        <div class="modal-dialog" role="document" style="min-width:526px;">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h5 class="block-title" id="modal-header-title">Appointment</h5>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" >
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <form action="{{ route('appointments.store') }}" method="post" id="form">
                        @csrf
                            <!-- project select -->
                            <input type="hidden" name="appointment_id">
                            <div class="form-group">
                                <label for="project_id">Customer/Project <span class="text-danger">*</span></label>
                                <select id="project_id" class="form-control" name="project_id" required>
                                    <option value="">Select Project</option>
                                    @foreach ($projects as $_project)
                                        <option
                                            value="{{ $_project->id }}" {{ $project && $project->id == $_project->id ? 'selected' : '' }}>{{ $_project->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="appointment_type_id">Appointment Type <span class="text-danger">*</span></label>
                                <select id="appointment_type_id" class="form-control"
                                        name="appointment_type_id" required>
                                    <option value="">Select Appointment Type</option>
                                    @foreach ($appointments_types as $type)
                                        <option value="{{ $type->id }}">{{ $type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="estimator_id">Appointment For <span class="text-danger">*</span></label>
                                <select id="estimator_id" class="form-control" name="estimator_id" required>
                                    <option value="">Select Estimator</option>
                                    @foreach ($estimators as $estimator)
                                        <option value="{{ $estimator->id }}">{{ $estimator->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="attendees-multi">Attendees</label>
                                <select class="js-select2 form-control" style="width: 100%" multiple name="attendees[]">
                                    @foreach ($estimators as $estimator)
                                        <option value="{{ $estimator->id }}">{{ $estimator->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="date">Date</label>
                                    <input type='text' id="date" name="date" class="flatpickr form-control">
                                </div>
                                <div class="form-group col-4">
                                    <label for="start_time">Start Time</label>
                                    <input type='text' id="start_time" name="start_time" class="flatpickr-time form-control">
                                </div>
                                <div class="form-group col-4">
                                    <label for="end_time">End Time</label>
                                    <input type='text' id="end_time" name="end_time" class="flatpickr-time form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="notes">Note <span class="text-danger">*</span></label>
                                <textarea id="notes" rows="5" class="form-control" name="notes"></textarea>
                            </div>

                            <div class="modal-footer">
                                <div id="delete_area">
                                    <button class="btn btn-danger">Delete</button>
                                </div>
                                <button class="btn btn-primary" id="save_appointment">Save</button>
                                <button class="btn btn-dark" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('appointment.includes.scripts')
@endsection
