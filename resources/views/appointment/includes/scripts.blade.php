<script src="{{ asset('js/plugins/moment/moment.min.js') }}"></script>

<script src="{{ asset('js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/plugins/flatpickr/flatpickr.js') }}"></script>
<script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>

<script type="text/javascript">

    let projects = <?php echo $projects ?>;
    let estimators = <?php echo $estimators ?>;
    let edit_selected_attendees = [];
    let appointments = <?php echo $appointments ?>;
    let events = <?php echo $events ?>;
    let appointments_without_project = <?php echo $appointments_without_project ?>;
    let editing_appointmentid;
    let firstloadstate = true;
    let $calendar = $('#calendar');
    let $saveAppointmentBtn = $('#save_appointment');
    let $appointmentForm = $('#form');
    let $appointmentId = $( `[name*='appointment_id']` );

    function openAppointmentForm(reset) {
        if (reset){
            resetForm();
            $('#modal-header-title').html('Add Appointment');
        }
        else {
            $('#modal-header-title').html('Edit Appointment');
        }
        $('#appointmentFormModal').modal('show');
        /* $('.modal-backdrop.show').css('opacity', '0');
         $('.modal-open').css('padding-right', '0');*/
    }

    function closeAppointmentForm() {
    }

    function twoDigits(d) {
        if (0 <= d && d < 10) return "0" + d.toString();
        if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
        return d.toString();
    }

    $(function () {
        Codebase.helper('flatpickr', {
            el: '.flatpickr',
        });

        Codebase.helper('flatpickr', {
            el: '.flatpickr-time',
            enableTime: true,
            noCalendar: true,
        });

        Codebase.helper('select2');

        initCalendar($calendar, events);

        $saveAppointmentBtn.on('click', function (e) {
            e.preventDefault();
            if ($appointmentId.val()) updateAppointment($appointmentId.val());
            else createAppointment();
        });

        function initCalendar(calElement, events) {
            // check to remove elements from the list
            let removeAfterDrop = $('#remove-after-drop');
            let $start_date = $("#start_Date input");
            let defaultAgenda = {
                type: 'agenda',
                slotLabelFormat: 'HH:mm',
                groupByResource: true,
                duration: {
                    days: 14
                },
                columnHeaderFormat: 'M/D \n ddd',
                visibleRange: {
                    start: moment($start_date.val(), 'MM/DD/YYYY HH:mm').format('YYYY-MM-DD'),
                    end: moment($start_date.val(), 'MM/DD/YYYY HH:mm').add(1, 'days').format('YYYY-MM-DD')
                }
            };

            calElement.fullCalendar({
                header: {
                    left: 'prev,next',
                    center: 'title',
                    right: 'agendaWeek,agendaDay'
                },
                defaultView: 'agendaDays',
                eventOrder: "title, start,-duration,allDay",
                views: {
                    agendaDays: {...defaultAgenda, ...{weekends:false} },
                    agendaDaysNoWeekends: {...defaultAgenda, ...{weekends:true} },
                },
                height: "auto",
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar
                drop: function (date, allDay) {
                    // this function is called when something is dropped

                    let $this = $(this),
                        // retrieve the dropped element's stored Event Object
                        originalEventObject = $this.data('calendarEventObject');

                    // if something went wrong, abort
                    if (!originalEventObject) return;

                    // clone the object to avoid multiple events with reference to the same object
                    let clonedEventObject = $.extend({}, originalEventObject);

                    // assign the reported date
                    clonedEventObject.start = date;

                    clonedEventObject.allDay = allDay;
                    clonedEventObject.backgroundColor = $this.css('background-color');
                    clonedEventObject.borderColor = $this.css('border-color');

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks"
                    // (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    calElement.fullCalendar('renderEvent', clonedEventObject, true);

                    // if necessary remove the element from the list
                    if (removeAfterDrop.is(':checked')) {
                        $this.remove();
                    }
                },
                eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                    if (event.allDay) {
                        //handle all day
                        console.log("allday");
                    } else {
                        let delta_day = delta._days;
                        let delta_mins = delta._milliseconds / 1000 / 60;
                        let appointment_id = event.appointment_id;
                        for (let i in appointments) {
                            if (appointments[i].id == appointment_id) {

                                let diff = (new Date(appointments[i].date + " " + appointments[i].start_time)).getTime() - (new Date(appointments[i].date + " " + appointments[i].end_time)).getTime();
                                var start_date = new Date(appointments[i].date + " " + appointments[i].start_time);
                                start_date = moment(start_date).subtract(-delta_day, 'days').toDate();
                                start_date = moment(start_date).subtract(-delta_mins, 'minutes').toDate();
                                let end_date = new Date(start_date.getTime() - diff);
                                end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());
                                start_date = start_date.getFullYear() + "-" + twoDigits(1 + start_date.getMonth()) + "-" + twoDigits(start_date.getDate()) + " " + twoDigits(start_date.getHours()) + ":" + twoDigits(start_date.getMinutes()) + ":" + twoDigits(start_date.getSeconds());
                                ChangedTimetoBackend(appointments[i].id, start_date, end_date);

                                appointments[i].start_time = start_date;
                                appointments[i].end_time = end_date;
                            }
                        }
                    }

                },
                eventResize: function (event, delta) {
                    // console.log(event);
                    let delta_day = delta._days;
                    let delta_mins = delta._milliseconds / 1000 / 60;
                    let appointment_id = event.appointment_id;
                    for (let i in appointments) {
                        if (appointments[i].id == appointment_id) {
                            var end_date = new Date(appointments[i].date + " " + appointments[i].end_time);
                            end_date = moment(end_date).subtract(-delta_day, 'days').toDate();
                            end_date = moment(end_date).subtract(-delta_mins, 'minutes').toDate();
                            //ajax call
                            end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());
                            ChangedTimetoBackend(appointments[i].id, appointments[i].date + " " + appointments[i].start_time, end_date);
                            //show to ui
                            appointments[i].end_time = end_date;
                        }
                    }
                },
                // This array is the events sources
                events: events,
                timeFormat: 'H(:mm)',
                minTime: '06:00',
                maxTime: '19:00',
                eventClick: (info) => {
                    getAppointment(info.appointment_id);
                },
                viewRender: function (view, element) {
                    if (firstloadstate) {
                        $calendar.find('.fc-toolbar > .fc-right').html(
                            '<p class="pt-2">Weekends Show:</p>' +
                            '<label class="css-control css-control-primary css-switch mr-2">' +
                            '<input class="css-control-input" type="checkbox" id="weekends_show" name="weekends_show">' +
                            '<span class="css-control-indicator"></span>' +
                            '</label>' +
                            $calendar.find('.fc-toolbar > .fc-right').html()
                        );

                        $('#weekends_show').change(function () {
                            if (this.checked) {
                                $calendar.fullCalendar('changeView', 'agendaDaysNoWeekends');
                            } else {
                                //agendaDaysNoWeekends
                                $calendar.fullCalendar('changeView', 'agendaDays');
                            }
                        })
                    }
                    firstloadstate = false;
                }
            });
        }

    });

    function renderErrors(response) {
        if (response.status === 422)
        {
            let errors = response.responseJSON.errors;
            $('.invalid-feedback').remove();
            $.each(errors, function (field, error) {
                let $input = $( `[name*='${field}']` );
                $input.addClass( "is-invalid" );
                $input.after(
                    '<span class="invalid-feedback" role="alert">\n' +
                    '    <strong>'+ error[0]+'</strong>\n' +
                    '</span>'
                )
            })
        }
    }

    function getAppointment(id) {
        $.ajax({
            type: 'GET',
            url: `/appointments/${id}`,
            success: function (appointment) {
                renderAppointmentValues(appointment);
                openAppointmentForm(false);
            }
        });
    }

    function createAppointment() {
        let form = $appointmentForm.serialize();
        $.ajax({
            type: 'POST',
            url: "{{ route('appointments.store') }}",
            data: form,
            success: function (response) {
                if (response.success) window.location.replace('/appointments');
            },
            error: function (response) {
                renderErrors(response);
            }
        });
    }

    function updateAppointment(id) {
        let form = $appointmentForm.serialize() + '&_method=' + 'put';

        $.ajax({
            type: 'post',
            url: `/appointments/${id}`,
            data: form,
            success: function (response) {
                if (response.success) window.location.replace('/appointments');
            },
            error: function (response) {
                renderErrors(response);
            }
        });
    }

    function ChangedTimetoBackend(appointment_id, start_date, end_date) {
        $.ajax({
            type: 'POST',
            url: "/editappointment/date",
            data: {
                "_token": "{{ csrf_token() }}",
                appointment_id: appointment_id,
                start_date: start_date,
                end_date: end_date
            },
            success: function (res) {
                if (res && res != 'success') {
                    alert('Error occurs!');
                }
            }
        });
    }

    function updateTime() {
        $.ajax({
            type: 'post',
            url: `/appointments/${id}/update-time`,
            data: {

            },
            success: function () {
                //
            },
            error: function (response) {
                //
            }
        });
    }

    function deleteAppointment(id) {
        $.ajax({
            type: 'post',
            url: "appointments/"+ id,
            data: {
                "_method": "delete",
                "_token": "{{ csrf_token() }}",
            },
            success: function (res) {
                window.location.reload();
            }
        });
    }

    function renderAppointmentValues(appointment) {
        $('#delete_area').html(`<button class="btn btn-danger" type="button" onclick="deleteAppointment(${appointment.id})">Delete</button>`);

        $appointmentId.val(appointment.id);
        $.each(appointment, function (field, value) {
            let $input = $( `[name*='${field}']` );
            if ($input.length){
                $input.val(value);
            }
        })
    }

    function resetForm() {
        let inputs = ['appointment_id', 'notes', 'date', 'start_time', 'end_time', 'attendees', 'estimator_id', 'project_id', 'appointment_type_id'];
        $('#delete_area').html('');
        $('.invalid-feedback').remove();

        $.each(inputs, function (index, field) {
            let $input = $( `[name*='${field}']` );
            if ($input.length){
                $input.val('');
            }
        });
        $('#date').flatpickr({
            defaultDate: '<?php echo date('Y-m-d'); ?>'
        });
        $('#start_time').flatpickr({
            dateFormat: "H:i",
            minTime: "09:00",
            maxTime: "18:00",
            defaultDate: "09:00"
        });
        $('#end_time').flatpickr({
            dateFormat: "H:i",
            minTime: "09:00",
            maxTime: "18:00",
            defaultDate: "11:00"
        });
    }

</script>
