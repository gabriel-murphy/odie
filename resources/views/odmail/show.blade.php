@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Odmail</a>
@endsection
@section('breadcrumb-active', 'Show')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Email Details</h3>
            <div class="block-options">
                <button data-toggle="modal" data-target="#forward_email_to_{{$odmail->id}}"
                        class="btn-block-option" title="Forward To">
                    <i class="fal fa-share"></i></button>
                <a onclick="return confirm('Are you sure you want to resend this email ?')"
                   href="{{ route('emails.odmails.resend', $odmail ) }}"
                   class="btn-block-option"
                   data-toggle="tooltip" title="Send Again">
                    <i class="fal fa-redo"></i>
                </a>
                <button data-toggle="modal" data-target="#delivery_report_{{$odmail->id}}"
                        class="btn-block-option" title="Info">
                    <i class="fal fa-info-circle"></i></button>
            </div>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <strong>From:</strong> {{ $odmail->from }}<br>
                        <strong>Sent:</strong> {{ $odmail->sent_at }}<br>
                        {!! $odmail->getRecipientsHtml() !!}
                    </p>
                    <p><strong>Subject: </strong>{{ $odmail->subject }}</p>
                    <p>{!! $odmail->body !!}</p>
                </div>
                @if($odmail->getMedia()->count())
                    <div class="col-md-12 mb-4">
                        <hr>
                        <h6>{{ $odmail->getMedia()->count() }} Attachments:</h6>
                        <x-attachments :attachments="$odmail->getMedia()"/>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @include('includes.modals.email_forward', ['odmail_id' => $odmail->id])
    @include('odmail.includes.delivery_details')
@endsection

