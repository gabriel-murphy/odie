<div class="modal fade border-0" id="delivery_report_{{$odmail->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="font-w400">Delivery Details</h5>
                <ul class="list list-activity">
                    @foreach($odmail->recipients as $recipient)
                        <li>
                            <i class="{{ $recipient->icon }}" title=""></i>
                            <div class="font-w600">{{ $recipient->email }}</div>
                            @if(!$recipient->status)
                                <div class="font-size-xs text-muted" title="">{{ $recipient->status_text }}</div>
                            @endif
                            @if($recipient->delivered_at)
                                <div class="font-size-xs text-muted" title="{{ $recipient->delivered_at->format('M d, Y h:i A') }}">
                                    Delivered {{ $recipient->delivered_at->diffForHumans() }}
                                </div>
                            @endif
                            @if($recipient->opened_at)
                                <div class="font-size-xs text-muted" title="{{ $recipient->opened_at->format('M d, Y h:i A') }}">
                                    Opened {{ $recipient->opened_at->diffForHumans() }}
                                </div>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
