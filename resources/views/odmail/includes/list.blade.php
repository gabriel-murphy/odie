<table class="table table-striped table-vcenter">
    <thead>
    <tr class="text-uppercase">
        <th style="width: 10px">#</th>
        <th>Email Title</th>
        <th></th>
        <th>Subject</th>
        <th>Recipients</th>
        <th class="text-center" style="width: 100px;">Actions</th>
    </tr>
    </thead>
    <tbody>
    @forelse($odmails as $odmail)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $odmail->template->name }}</td>
            <td>
                @if($odmail->forwarded) <i class="fad fa-share mr-2 text-info"></i> @endif
                @if($odmail->resent) <i class="fad fa-redo mr-2 text-info"></i> @endif
            </td>
            <td>{{ $odmail->subject }}</td>
            <td>
                {!!  $odmail->getRecipientsHtml() !!}
            </td>
            <td class="text-center">
                <div class="btn-group">
                    <button data-toggle="modal" data-target="#forward_email_to_{{$odmail->id}}"
                            class="btn btn-sm btn-secondary" title="Forward To">
                        <i class="fal fa-share"></i></button>

                    <a href="{{ route('emails.odmails.resend', $odmail ) }}"
                       class="btn btn-sm btn-secondary"
                       data-toggle="tooltip" title="Send Again">
                        <i class="fal fa-redo"></i>
                    </a>

                    <a href="{{ route('emails.odmails.show', $odmail ) }}"
                       class="btn btn-sm btn-secondary"
                       data-toggle="tooltip" title="View">
                        <i class="fad fa-eye"></i>
                    </a>
                    <button data-toggle="modal" data-target="#delivery_report_{{$odmail->id}}"
                            class="btn btn-sm btn-secondary" title="Info">
                        <i class="fal fa-info-circle"></i></button>
                </div>
            </td>
            @include('includes.modals.email_forward', ['odmail_id' => $odmail->id])
            @include('odmail.includes.delivery_details')
        </tr>

    @empty
    @endforelse
    </tbody>
</table>
