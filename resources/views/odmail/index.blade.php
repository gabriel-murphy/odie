@extends('layouts.backend')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Odie Emails List</h3>
            <div class="block-options">
                @include('includes.common.search', ['route' => route(Route::currentRouteName())])
            </div>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                @include('odmail.includes.list')

                <div class="pagination justify-content-center">
                    {{ $odmails->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
