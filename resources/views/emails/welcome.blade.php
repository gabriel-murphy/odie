@component('mail::message')
    # Introduction

    Thanks {{ Session::get('first_name') }}

    @component('mail::button', ['url' => '/'])
        Setup Now!
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
