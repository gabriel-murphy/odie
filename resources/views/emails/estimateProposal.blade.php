@component('mail::message')

# Dear sir

{{$mailText}}

<br>
<br>
Thanks,<br>

{{ config('app.name') }}

<div style='padding-top:50px;text-align:center;margin-bottom: 30px;'> 
    <a style='background: #3490dc;color: white;padding: 13px 42px;font-size: 13pt;cursor: pointer; text-decoration: none;' href="{{$signlink}}">
        Sign
    </a> 
</div>

@endcomponent
