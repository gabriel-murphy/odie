<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/e02be562b0.js" crossorigin="anonymous"></script>
    <title>Claim Reminder</title>

    <style type="text/css">
        @media screen and (max-width: 515px) {
            .contact{
                font-size: 55px !important;
            }
        }
        @media screen and (max-width: 400px) {
            .contact{
                font-size: 45px !important;
            }
        }
    </style>
</head>
<body style="margin: 0; padding: 0;font-family: 'Myriad Pro';">

<div class="wrapper" style="background-color: #EAEAEA;padding-top: 40px;padding-bottom: 40px;">
    <table class="outer-table" cellpadding="0" cellspacing="0" width="100%"
           style="padding-top:40px;max-width: 600px;margin: 0 auto;background-color: #FFFFFF;">
        <tr>
            <td align="center">
                <img width="175" src="{{asset('images/campaigns/irma/named-logo.png')}}" alt="named-logo.png">
            </td>
        </tr>
        <tr>
            <td style="padding-top: 20px;">
                <img width="100%" style="max-width: 100%;" src="{{asset('images/campaigns/irma/banner.jpg')}}"
                     alt="banner.jpg">
            </td>
        </tr>
        <tr>
            <td>
                <table style="padding: 30px;" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img style="margin-bottom: 15px" width="100%" src="{{asset('images/campaigns/irma/deadline.png')}}" alt="deadline.png">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="color: #ad1f23;font-size: 15px;padding: 0 25px;margin:0">
                                Only <strong><u>3 Days</u></strong> Remain Before Time Runs Out to Get Your Roof Replaced
                                <strong><u>Call Your Insurance Carrier Today</u></strong> and Make a Windstorm-Claim on Your
                                Roof, Then Call Roman to Assist You with the Claim!
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" cellspacing="10" cellpadding="0">
                    <tr>
                        <td>
                            <img width="100%" src="{{asset('images/campaigns/irma/roofs.png')}}" alt="">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        {!! $content !!}
        <tr>
            <td>
                <table width="100%" cellspacing="0" cellpadding="0" style="background:#ad1f23;padding: 40px 0;" >
                    <tr>
                        <td align="center">
                            <a style="display: inline-block" href="#">
                                <img src="{{asset('images/campaigns/irma/linkedin.png')}}" alt="">
                            </a>
                            <a style="display: inline-block" href="#">
                                <img src="{{asset('images/campaigns/irma/youtube.png')}}" alt="">
                            </a>
                            <a style="display: inline-block" href="#">
                                <img src="{{asset('images/campaigns/irma/facebook.png')}}" alt="">
                            </a>
                            <a style="display: inline-block" href="#">
                                <img src="{{asset('images/campaigns/irma/twitter.png')}}" alt="">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <div style="height: 1px;background: #FFFFFF;width: 60%;margin: 20px 0;"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="margin: 0;font-size: 15px;color: #FFFFFF!important;text-decoration: none!important;">
                                info@romanroofing.com
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="margin: 0;font-size: 15px;color: #FFFFFF;">
                                Unsubscribe
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
