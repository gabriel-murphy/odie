@component('mail::message')

# Dear sir 
# Please enter the following verification code to verify the sign attempt

{{$pincode}}

<br>
<br>
<br>

# Thanks

# {{ config('app.name') }}

@endcomponent