<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('frontends/css/input-material.css')}}">
    <link rel="stylesheet" href="{{asset('frontends/css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('frontends/css/common.css')}}">
    <link rel="stylesheet" href="{{asset('frontends/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontends/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontends/css/main-frontend.css')}}">
    <title>Odie</title>
</head>
<body>

<header>
    <div class="main-menu">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light py-3">
                <a class="navbar-brand" href="#">
                    <img class="website-logo" src="{{asset('frontends/images/logos/odie.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuNavbar"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="menuNavbar">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Services</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link sign-in-btn" href="#">Sign In</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

<main>
    <div class="banner" style="background-image: url({{asset('frontends/images/backgrounds/bg-banner.jpg')}})">
        <div class="banner_content primary-background py-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <h1 class="sub-heading-white mb-1">ROOFING SALES BOOSTER & PROJECT</h1>
                        <p class="heading-description-white mb-3">MANAGEMENT PLATFORM-RAISING THE ROOF ON EFFICIENCY &
                            PROFITABILITY</p>
                        <p class="tagline">Built by roofers and web artisans, Odie is the successfull roofer’s trade
                            secret. Make it yours as well.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="lowest-order-shape"></div>
        <div class="middle-order-shape">
            <div class="shape-content-wrapper text-center">
                <p class="small-detail mb-3">Want to know how it works?</p>
                <a class="primary-button" href="#">Request Demo Now!</a>
            </div>
        </div>
        <div class="highest-order-shape"></div>
    </div>
    <div class="signup-form secondary-background py-5">
        <div class="container">
            <div class="col-md-10 mx-auto">
                <form class="form-inline">
                    <input type="text" class="form-control mr-sm-2" placeholder="Your Name">
                    <input type="email" class="form-control mr-sm-2" placeholder="Email Address">
                    <input type="text" class="form-control mr-sm-2" placeholder="Phone Number">
                    <button type="submit" class="btn btn-primary primary-background">Sign Up</button>
                </form>
            </div>
        </div>
    </div>
    <div class="unique section-bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-7 ml-auto">
                    <div class="sub-section">
                        <h1 class="section-heading font-weight-light mb-4">This is what <strong>makes us Different</strong></h1>
                        <h2 class="sub-heading-black mb-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis</h2>
                        @for($i = 0;$i < 2; $i++)
                            <p class="section-description font-weight-light mb-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
        <div class="primary-background py-5">
            <div class="d-flex justify-content-center align-items-center">
                <h1 class="large-heading mr-2">Lorem ipsum</h1>
                <a href="#" class="arrow-btn mt-1"><i class="fas fa-chevron-right"></i></a>
            </div>
            <img src="{{asset('frontends/images/backgrounds/mobile.png')}}" alt="">
        </div>
    </div>
    <div class="profit section">
        <div class="container">
            <div class="section-header">
                <h1 class="section-heading text-center">Odie Makes Roofers Professional & Profitable.</h1>
            </div>
            <p class="section-description text-center">
                Any roofer with a ladder and some common sense should employ Odie- a unique, software-as-a-service platform built on best-of-breed technologies.
                Odie optimizes your return on investment on lead sources while managing your roofing business workflows. Aimed with your cost of materials and labor,
                Odie compiles roof take-offs and empowers your sales force with automated calculation of estimates.  Odie's AI engine considers roof pitch,
                permitting costs and other factors in calculating your project cost - right down to commissions. Odie also considers your customized waste factors
                and profit margins to prepare a professional, turnkey estimate for e-signature akin to professional e-signature platforms.
            </p>
        </div>
    </div>
    <div class="feedback section" style="background-image: url({{asset('frontends/images/backgrounds/bg-feedback.png')}})">
        <div class="container">
            <div class="section-header text-center">
                <h1 class="section-heading mb-3">Happy Customers</h1>
                <p class="section-description-sm text-center">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum
                    iriure dolor in hendrerit in vulputate
                </p>
            </div>
            <div class="feedback-slider owl-carousel owl-theme">
                <div class="item">
                    <div class="card border-0 rounded-0">
                        <img class="card-img-top mx-auto tag-img" src="{{asset('frontends/images/tags/tag-jquery.png')}}" alt="tag-jquery.png">
                        <div class="card-body">
                            <p class="card-text text-center card-detail mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                            <div class="media flex-column align-items-center">
                                <img src="{{asset('frontends/images/person.jpg')}}" class="align-self-start reviewer-img mx-auto">
                                <div class="media-body text-center">
                                    <h4 class="card-heading">Customer Name</h4>
                                    <p class="card-detail">Lorem ipsum dolor sit amet, </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card border-0 rounded-0">
                        <img class="card-img-top mx-auto tag-img" src="{{asset('frontends/images/tags/tag-node.png')}}" alt="tag-node.png">
                        <div class="card-body">
                            <p class="card-text text-center card-detail mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                            <div class="media flex-column align-items-center">
                                <img src="{{asset('frontends/images/person.jpg')}}" class="align-self-start reviewer-img mx-auto">
                                <div class="media-body text-center">
                                    <h4 class="card-heading">Customer Name</h4>
                                    <p class="card-detail">Lorem ipsum dolor sit amet, </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card border-0 rounded-0">
                        <img class="card-img-top mx-auto tag-img" src="{{asset('frontends/images/tags/tag-gulp.png')}}" alt="tag-gulp.png">
                        <div class="card-body">
                            <p class="card-text text-center card-detail mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                            <div class="media flex-column align-items-center">
                                <img src="{{asset('frontends/images/person.jpg')}}" class="align-self-start reviewer-img mx-auto">
                                <div class="media-body text-center">
                                    <h4 class="card-heading">Customer Name</h4>
                                    <p class="card-detail">Lorem ipsum dolor sit amet, </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card border-0 rounded-0">
                        <img class="card-img-top mx-auto tag-img" src="{{asset('frontends/images/tags/tag-gulp.png')}}" alt="tag-gulp.png">
                        <div class="card-body">
                            <p class="card-text text-center card-detail mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                            <div class="media flex-column align-items-center">
                                <img src="{{asset('frontends/images/person.jpg')}}" class="align-self-start reviewer-img mx-auto">
                                <div class="media-body text-center">
                                    <h4 class="card-heading">Customer Name</h4>
                                    <p class="card-detail">Lorem ipsum dolor sit amet, </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="overlay"></div>
        <div class="triangle-gray"></div>
    </div>
    {{--   <div class="showcase section">
           <div class="container">
               <div class="section-header text-center">
                   <h1 class="section-heading">Show Cases</h1>
               </div>

               <div class="showcase-slider owl-carousel owl-theme">
                   <div class="item">
                       <img src="{{asset('frontends/images/slider/slider-1.jpg')}}" alt="slider-1.jpg">
                   </div>
                   <div class="item">
                       <img src="{{asset('frontends/images/slider/slider-2.jpg')}}" alt="slider-2.jpg">
                   </div>
                   <div class="item">
                       <img src="{{asset('frontends/images/slider/slider-3.jpg')}}" alt="slider-3.jpg">
                   </div>
                   <div class="item">
                       <img src="{{asset('frontends/images/slider/slider-3.jpg')}}" alt="slider-3.jpg">
                   </div>
               </div>
           </div>
       </div>--}}

    {{-- Gallery Plugin Start--}}

    <div class="showcase-gallery section">
        <div class="container fix-container-height">
            <div class="section-header">
                <h1 class="section-heading text-center">Show Cases</h1>
                <p class="section-description-sm text-center">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum
                    iriure dolor in hendrerit in vulputate
                </p>
            </div>
            {{--<div id="frame">--}}
            <div class="gallery js-gallery">
                <section>
                    <div>
                        <a href="{{asset('frontends/images/gallery/house-1.jpg')}}" rel="houses" title="House 1">
                            <img src="{{asset('frontends/images/gallery/house-1.jpg')}}" alt="house-1.jpg">
                        </a>
                    </div>
                    <div>
                        <a href="{{asset('frontends/images/gallery/house-2.jpg')}}" rel="houses" title="House 2">
                            <img src="{{asset('frontends/images/gallery/house-2.jpg')}}" alt="house-2.jpg">
                        </a>
                    </div>
                    <div>
                        <a href="{{asset('frontends/images/gallery/house-3.jpg')}}" rel="houses" title="House 3">
                            <img src="{{asset('frontends/images/gallery/house-3.jpg')}}" alt="house-3.jpg">
                        </a>
                    </div>
                    <h1 class="sub-heading-black text-center mt-3">Houses</h1>
                </section>

                <section>
                    <div>
                        <a href="{{asset('frontends/images/gallery/car-1.jpg')}}" rel="cars" title="Car 1">
                            <img src="{{asset('frontends/images/gallery/car-1.jpg')}}" alt="car-1.jpg">
                        </a>
                    </div>
                    <div>
                        <a href="{{asset('frontends/images/gallery/car-2.jpg')}}" rel="cars" title="Car 2">
                            <img src="{{asset('frontends/images/gallery/car-2.jpg')}}" alt="car-2.jpg">
                        </a>
                    </div>
                    <div>
                        <a href="{{asset('frontends/images/gallery/car-3.jpg')}}" rel="cars" title="Car 3">
                            <img src="{{asset('frontends/images/gallery/car-3.jpg')}}" alt="car-3.jpg">
                        </a>
                    </div>
                    <h1 class="sub-heading-black text-center mt-3">Cars</h1>
                </section>

                <section>
                    <div>
                        <a href="{{asset('frontends/images/gallery/mountain-1.jpg')}}" rel="mountains" title="Mountain 1">
                            <img src="{{asset('frontends/images/gallery/mountain-1.jpg')}}" alt="mountain-1.jpg">
                        </a>
                    </div>
                    <div>
                        <a href="{{asset('frontends/images/gallery/mountain-2.jpg')}}" rel="mountains" title="Mountain 2">
                            <img src="{{asset('frontends/images/gallery/mountain-2.jpg')}}" alt="mountain-2.jpg">
                        </a>
                    </div>
                    <div>
                        <a href="{{asset('frontends/images/gallery/mountain-3.jpg')}}" rel="mountains" title="Mountain 3">
                            <img src="{{asset('frontends/images/gallery/mountain-3.jpg')}}" alt="mountain-3.jpg">
                        </a>
                    </div>
                    <h1 class="sub-heading-black text-center mt-3">Mountains</h1>
                </section>
            </div>
            {{--</div>--}}
        </div>
    </div>

    {{--Gallery Plugin End--}}


    <div class="packages section secondary-background">
        <div class="container">
            <h1 class="sub-heading-black text-center">Not Just Another CRM - A Feature-Rich Roofing Business Booster</h1>
            <p class="section-description-black text-center">
                Odie comes packaged with feature-rich modules, all of which can be heavily configured, including:
            </p>
        </div>
        <div class="triangle-maroon"></div>
    </div>

    <div class="services section">
        <div class="container">
            <div class="row">
                {{--Engineers Top Cards Start--}}

                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-arrows.png')}}" alt="icon-arrows.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Opportunities</h4>
                            <p class="card-text card-detail">
                                Odie turns opportunties from your lead sources into won projects through proprietary sources and methods.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-building.png')}}" alt="icon-building.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Estimator</h4>
                            <p class="card-text card-detail">
                                Odie's algorthm emulates the way you calculate project cost & profit - considering waste, pitch, commissions, etc.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>

                {{--Engineers Top Cards End--}}

                {{--Engineers Left Cards Start--}}

                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-calender.png')}}" alt="icon-calender.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Scheduler</h4>
                            <p class="card-text card-detail">
                                Odie makes it simple to schedule various appointments for your estimators complete with notifications.
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-page-tree.png')}}" alt="icon-page-tree.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Workflows</h4>
                            <p class="card-text card-detail">
                                Odie makes decisions based on your workflows, then logs the process which you can track with ease.
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-bell.png')}}" alt="icon-bell.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Notifications</h4>
                            <p class="card-text card-detail">
                                Odie is constantly communicating with your estimators, customers, vendors and crews via common channels.
                            </p>
                        </div>
                    </div>
                </div>

                {{--Engineers Left Cards End--}}

                <div class="col-md-4 my-auto">
                    <div class="text-center">
                        <img class="img-fluid" src="{{asset('frontends/images/engineer.png')}}" alt="">
                    </div>
                </div>


                {{--Engineers Right Cards Start--}}

                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-wallclock.png')}}" alt="icon-wallclock.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Presenter</h4>
                            <p class="card-text card-detail">
                                Odie turns raw numbers into professional estimates, custom to your payment and project terms.
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-treehouse.png')}}" alt="icon-treehouse.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Connector</h4>
                            <p class="card-text card-detail">
                                Odie creates a web-portal for each customer - they can view photos, pay their invoice and leave a review!
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-pages.png')}}" alt="icon-pages.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Signer</h4>
                            <p class="card-text card-detail">
                                Odie's last name is Acrobat, as he then presents your prospect with an e-Sign request akin to Adobe Sign.
                            </p>
                        </div>
                    </div>
                </div>

                {{--Engineers Right Cards End--}}

                {{--Engineers Bottom Start--}}

                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-setting.png')}}" alt="icon-setting.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Integrations</h4>
                            <p class="card-text card-detail">
                                Since Odie loves API, it obtains hip, ridge, valley, eave and other measurements error-free and in hours.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top icon-w-49 mx-auto" src="{{asset('frontends/images/icons/icon-tractor.png')}}" alt="icon-tractor.png">
                        <div class="card-body text-center">
                            <h4 class="card-title regular-heading">Harvesters</h4>
                            <p class="card-text card-detail">
                                Odie identifies homeowners in your service area ripe to re-roof their home!  Do you love Odie now?
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>

                {{--Engineers Bottom Cards End--}}
            </div>
        </div>
    </div>

    <div class="team section secondary-background">
        <div class="container">
            <div class="section-header">
                <h1 class="section-heading primary-color text-center">Meet Our Team</h1>
                <p class="section-description-sm text-center text-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                    tincidunt ut laoreet dolore magna </p>
            </div>
            <div class="team-slider owl-carousel owl-theme">
                <div class="item">
                    <div class="card border-0 bg-transparent">
                        <img class="card-img-top team-img" src="{{asset('frontends/images/person.jpg')}}" alt="person.jpg">
                        <div class="card-body text-center">
                            <h4 class="card-title sub-heading-black primary-color">Lorem ipsum</h4>
                            <p class="card-text card-detail text-white font-s-14">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card border-0 bg-transparent">
                        <img class="card-img-top team-img" src="{{asset('frontends/images/person.jpg')}}" alt="person.jpg">
                        <div class="card-body text-center">
                            <h4 class="card-title sub-heading-black primary-color">Lorem ipsum</h4>
                            <p class="card-text card-detail text-white font-s-14">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card border-0 bg-transparent">
                        <img class="card-img-top team-img" src="{{asset('frontends/images/person.jpg')}}" alt="person.jpg">
                        <div class="card-body text-center">
                            <h4 class="card-title sub-heading-black primary-color">Lorem ipsum</h4>
                            <p class="card-text card-detail text-white font-s-14">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="card border-0 bg-transparent">
                        <img class="card-img-top team-img" src="{{asset('frontends/images/person.jpg')}}" alt="person.jpg">
                        <div class="card-body text-center">
                            <h4 class="card-title sub-heading-black primary-color">Lorem ipsum</h4>
                            <p class="card-text card-detail text-white font-s-14">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="awesome-app">
        <div class="primary-background py-5">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-5">
                        <h1 class="sub-heading-black text-white">This is Awesome app</h1>
                        <p class="section-description-sm text-white font-s-14">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-5">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-5">
                        <h1 class="sub-heading-black mb-4">This is Awesome app</h1>
                        <p class="section-description-sm font-s-14 mb-4">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                        </p>
                        <p class="section-description-sm font-s-14">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <a class="secondary-button" href="#">Download Now</a>
        </div>
        <img class="laptop" src="{{asset('frontends/images/laptop.png')}}" alt="laptop.png">
    </div>
    <div class="section">
        <div class="container">
            <div class="section-header">
                <h1 class="section-heading text-center pt-5">About Us</h1>
            </div>
            <p class="section-description text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et
                accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
            <div class="text-center mt-5">
                <a class="primary-button" href="#">Learn More</a>
            </div>
        </div>
    </div>

    <div class="demo overlay-maroon py-5" style="background-image: url({{asset('frontends/images/backgrounds/bg-demo.jpg')}})">
        <div class="content-on-overlay">
            <div class="container">
                <div class="col-md-8 offset-md-2">
                    <div class="text-center">
                        <p class="section-description text-white font-s-30">
                            Ready to make the right decision today? Request a demo to get started on the path to enhancing your roofing profits!
                        </p>
                        <a class="transparent-button mt-4" href="#">Request Demo Now!</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="overlay-maroon"></div>
    </div>
</main>

<footer>
    <div class="section padding-t-60">
        <div class="container">
            <div class="remove-list-style">
                <ul class="footer-social-icons margin-b-60">
                    <li>
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fab fa-youtube"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="remove-list-style">
                        <h5 class="footer-list-heading">PRODUCT</h5>
                        <ul>
                            <li>
                                <a class="footer-list-item" href="#">API Documentation</a>
                            </li>
                            <li>
                                <a class="footer-list-item" href="#">Customers</a>
                            </li>
                            <li>
                                <a class="footer-list-item" href="#">Tour</a>
                            </li>
                            <li>
                                <a class="footer-list-item" href="#">Pricing Model</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="remove-list-style">
                        <h5 class="footer-list-heading">CONTACT</h5>
                        <ul>
                            <li>
                                <a class="footer-list-item" href="#">Support</a>
                            </li>
                            <li>
                                <a class="footer-list-item" href="#">Sales</a>
                            </li>
                            <li>
                                <a class="footer-list-item" href="#">Forum</a>
                            </li>
                            <li>
                                <a class="footer-list-item" href="#">Blog</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <h5 class="footer-list-heading text-left text-md-center">DOWNLOAD THE APP</h5>

                    <div class="margin-t-65">
                        <div class="text-left text-md-center">
                            <p class="footer-list-item mb-3">Available on Android and iOS</p>
                            <a class="d-inline-block margin-r-15" href="#">
                                <img src="{{asset('frontends/images/icons/icon-apple.png')}}" alt="icon-apple.png">
                            </a>
                            <a class="d-inline-block" href="#">
                                <img src="{{asset('frontends/images/icons/icon-android.png')}}" alt="icon-android.png">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 offset-md-1">
                    <h5 class="footer-list-heading">SUBSCRIBE TO OUT NEWSLETTER!</h5>
                    <form class="footer-form">
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" placeholder="mail@example.com">
                            <div class="input-group-append">
                                <button class="btn join-btn" type="submit">Join</button>
                            </div>
                        </div>
                        <p class="footer-list-item">We will never send spam and you can unsubscribe anytime</p>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom remove-list-style">
        <div class="container">
            <ul class="d-flex">
                <li>
                    <a class="font-s-12 text-white" href="#">Copyright 2020</a>
                </li>
                <li>
                    <a class="font-s-12 text-white" href="#">Terms of Service</a>
                </li>
                <li>
                    <a class="font-s-12 text-white" href="#">Privacy Policy</a>
                </li>
            </ul>
        </div>
    </div>
</footer>



<script src="{{ mix('js/codebase.app.js') }}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>

<!-- Laravel Scaffolding JS -->
<script src="{{ mix('js/laravel.app.js') }}"></script>

<script>
    @php($notifications = array('error', 'success', 'warning', 'info'))
    @foreach($notifications as $notification)
        @if(session()->has($notification))
            {{ "toastr." . $notification }}{!! "('" !!}{{session()->get($notification)}}{!! "')" !!}
        @endif
    @endforeach
</script>
<script src="{{asset('frontends/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontends/js/gallery.js')}}"></script>
<script src="{{asset('frontends/js/main.js')}}"></script>
</body>
</html>
