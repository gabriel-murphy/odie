@extends('layouts.backend')

@section('content')
    <constant></constant>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script>jQuery(function(){ Codebase.helpers(['slimscroll']); });</script>
@endsection
