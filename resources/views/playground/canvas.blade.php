@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Maximus</a>
@endsection
@section('breadcrumb-active', 'AOB')

@section('content')
    <div class="container">
        <div class="row">
            @for($i = 0; $i < 10; $i++)
                <div class="col-md-2">
                    <div class="img-container preview-img mb-4">
                        <img class="preview-img" src="{{asset('images/landing/background.jpg')}}" alt="">
                        <div class="img-container-layer">
                            <div class="details">
                                <i class="fas fa-file-pdf mr-2"></i>
                                <div class="name">
                                    <p>Document</p>
                                    <p class="mt-2">28.5kb</p>
                                </div>
                            </div>
                        </div>
                        <div class="image-flex-wrapper">
                            <div class="image-flex">
                                <div class="flex-details">
                                    <i class="fas fa-file-pdf mr-2"></i>
                                    <p class="text-truncate mb-0">Document</p>
                                </div>
                                <div class="shape-wrapper">
                                    <div class="shape">
                                        <div class="top-triangle"></div>
                                        <div class="bottom-triangle"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
@endsection
