@extends('layouts.backend')

@section('content')
    <div class="block">
        <div class="block-content">
            <form action="{{ route('playground.sms') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="">Enter Number</label>
                    <input type="text" name="number" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Enter Message</label>
                    <textarea class="form-control" name="message"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success">Send</button>
                </div>
            </form>
        </div>
    </div>
@endsection
