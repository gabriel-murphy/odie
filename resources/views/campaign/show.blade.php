@extends('layouts.backend')

@section('styles')
@endsection

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Campaigns Manager</a>
@endsection

@section('breadcrumb-active', 'Details')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ $campaign->name }}</h3>
            <div class="block-options">
                <a href="{{ route('campaigns.recipients', $campaign ) }}"
                   class="btn-block-option"
                   data-toggle="tooltip" title="See Recipients">
                    <i class="fal fa-list"></i>
                </a>
            </div>
        </div>
        <div class="block-content">
            <x-utils.progressbar :percentage="$campaign->getStats('sent', true)"/>
            <br>
            <div class="row items-push-2x text-center invisible" data-toggle="appear">
                <div class="col-6 col-md-3">
                    <x-utils.pie-chart
                        title="Emails Sent" :percentage="$campaign->getStats('sent', true)"
                        :count="$campaign->getStats('sent')" color="#ffca28">
                    </x-utils.pie-chart>
                </div>
                <div class="col-6 col-md-3">
                    <x-utils.pie-chart
                        title="Emails Delivered" :percentage="$campaign->getStats('delivered', true)"
                        :count="$campaign->getStats('delivered')" color="#3f9ce8">
                    </x-utils.pie-chart>
                </div>
                <div class="col-6 col-md-3">
                    <x-utils.pie-chart
                        title="Emails Opened" :percentage="$campaign->getStats('opened', true)"
                        :count="$campaign->getStats('opened')" color="#9ccc65">
                    </x-utils.pie-chart>
                </div>
                <div class="col-6 col-md-3">
                    <x-utils.pie-chart
                        title="Emails Failed" :percentage="$campaign->getStats('failed', true)"
                        :count="$campaign->getStats('failed')" color="#ef5350">
                    </x-utils.pie-chart>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    {{--<script src="{{ asset('js/plugins/parkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('js/plugins/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.stack.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flot/jquery.flot.resize.min.js') }}"></script>--}}
    <script>
        jQuery(function () {
            Codebase.helpers(['easy-pie-chart']);
        });
    </script>
@endsection
