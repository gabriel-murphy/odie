@extends('layouts.backend')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Campaign Recipients</h3>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr class="text-uppercase">
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($recipients as $recipient)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $recipient->name }}</td>
                            <td>{{ $recipient->email }}</td>
                            <td>{{ $recipient->status_text }}</td>
                        </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>

                <div class="pagination justify-content-center">
                    {{ $recipients->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
