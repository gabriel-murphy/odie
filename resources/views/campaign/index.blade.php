@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Campaigns Manager</a>
@endsection
@section('breadcrumb-active', 'Index')

@section('styles')
@endsection

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Campaigns List</h3>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Total Recipients</th>
                        <th>Emails Sent</th>
                        <th>Emails Delivered</th>
                        <th>Emails Opened</th>
                        <th>Emails Failed</th>
                        <th>Status</th>
                        <th style="width: 50px">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($campaigns as $campaign)
                        <tr>
                            <td>{{ $campaign->id }}</td>
                            <td>{{ $campaign->name }}</td>
                            <td>{{ $campaign->recipients_count }}</td>
                            <td>{{ $campaign->getStats('sent') }}</td>
                            <td>{{ $campaign->getStats('delivered') }}</td>
                            <td>{{ $campaign->getStats('opened') }}</td>
                            <td>{{ $campaign->getStats('failed') }}</td>
                            <td>
                                @if($campaign->status == 1)
                                    <span class="text-success">Finished</span>
                                @else
                                    <span class="text-info">Running</span>
                                @endif
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('campaigns.show', $campaign ) }}"
                                       class="btn btn-sm btn-secondary"
                                       data-toggle="tooltip" title="Show">
                                        <i class="fad fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="999" class="text-center">{{ app('constants')->constant('no_record') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                {{--<div class="pagination justify-content-center">
                    {{ $claims->links() }}
                </div>--}}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
