@extends('layouts.backend')

@section('styles')
    <link href="{{ asset('js/plugins/bootstrap-fileinput/css/fileinput.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('js/plugins/bootstrap-fileinput//themes/explorer-fas/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('js/plugins/bs-tagsinput/tagsinput.css')}}">
@endsection

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Campaigns Manager</a>
@endsection

@section('breadcrumb-active', 'New Campaign')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">New Campaign</div>
        <div class="block-content">
            @include('includes.partials.errors')
            <form action="{{ route('campaigns.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name"
                               class="form-control @error('template_id') is-invalid @enderror"
                               value="{{ old('name') }}">
                        @include('includes.partials.error', ['field' => 'name'])
                    </div>
                    <div class="form-group col-6">
                        <label for="template_id">Campaign Template: </label>
                        <select class="form-control @error('template_id') is-invalid @enderror"
                                id="template_id" name="template_id">
                            <option value="">Select</option>
                            @foreach($templates as $template)
                                <option value="{{ $template->id }}"
                                    {{ old('template_id') == $template->id ? 'selected':'' }}>
                                    {{ $template->name }}
                                </option>
                            @endforeach
                        </select>
                        @include('includes.partials.error', ['field' => 'template_id'])
                    </div>
                    <div class="form-group col-12">
                        <label for="columns">Columns:</label>
                        <select id="columns"  name="columns[]" data-role="tagsinput" multiple
                               class="form-control @error('columns') is-invalid @enderror">
                            @if(old('columns'))
                                @foreach(old('columns') as $column)
                                    <option value="{{ $column }}">{{ $column }}</option>
                                @endforeach
                            @endif
                        </select>
                        @include('includes.partials.error', ['field' => 'columns'])
                    </div>
                    <div class="form-group col-12">
                        <label class="font-italic font-size-xs text-muted">Upload Excel File</label>
                        <input type="file" id="recipients" class="file-input form-control @error('aob') is-invalid @enderror"
                               name="recipients"  data-allowed-file-extensions='["csv", "pdf"]'>
                        @include('includes.partials.error', ['field' => 'recipients', 'class' => 'd-block'])
                    </div>
                </div>
                <div class="form-group col-12 text-right px-0">
                    <button type="submit" class="btn btn-success btn-sm">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/bs-tagsinput/tagsinput.js') }}"></script>

    <script>
        jQuery(function () {
            Codebase.helper(['summernote', 'flatpickr', 'select2', 'tags-inputs']);
        });
    </script>

    @include('includes.common.bootstrap-fileinput')

@endsection
