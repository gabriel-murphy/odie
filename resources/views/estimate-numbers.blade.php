@extends('layouts.backend')

@section('content')

    <!-- Main section-->
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="content-title">Estimator :: Review Costs & Margins
                    <small>Step 4 - Verify Numbers & Build Sheet</small>
                </div>
            </div>
            <div class="container-fluid">

                <!-- START card-->
                <div class="card card-default">
                    <div class="block-header block-header-default">Estimate #2 - Project: _______</div>
                    <div class="block-content">
                        <!-- START table-responsive-->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Total Cost</th>
                                    <th>Materials</th>
                                    <th>Labor</th>
                                    <th>Fees</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Base Cost</td>
                                    <td>$58,382.43</td>
                                    <td>$38,381.43</td>
                                    <td>$19.392.23</td>
                                    <td>$383.40</td>
                                </tr>
                                <tr>
                                    <td>Retail Price</td>
                                    <td>$38,381.43</td>
                                    <td>$19.392.23</td>
                                    <td>$383.40</td>
                                    <td>$58,382.43</td>
                                </tr>
                                <tr>
                                    <td>Gross Margin</td>
                                    <td>$38,381.43</td>
                                    <td>$19.392.23</td>
                                    <td>$383.40</td>
                                    <td>$58,382.43</td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- END table-responsive-->
                    </div>
                </div><!-- END card-->

            </div>
    </section>

@endsection
