@php($project = $project ?? null)
@php($mode = $project ? 'display' : 'create')
@php($col_class = $mode == 'display' ? 'col-12' : 'col-6')
<div class="row">
    @if($mode == 'display')
        <div class="{{ $col_class }}">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Project Information</h3>
                </div>
                <div class="block-content">
                    <div class="form-group">
                        <label for="phase_id">Phase</label>
                        <select id="phase_id" class="form-control" name="phase_id">
                            @foreach($phases as $phase)
                                <option
                                    {{  old('phase_id', $project ? $project->phase_id : '' ) == $phase->id ? 'selected' : '' }}
                                    value="{{$phase->id}}">{{$phase->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="{{ $col_class }}">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Customer Information</h3>
            </div>
            <div class="block-content">
                @if($mode == 'create')
                    <div class="form-group">
                        <label for="customer_search">Customer Search</label>
                        <select class="form-control">
                            <option>Search for Existing Customer</option>
                        </select>
                    </div>
                    <hr>
                @endif
                <div class="form-group">
                    <label for="customer_type">Customer Classification</label>
                    <div class="col-12 pl-0">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" name="customer[classification]"
                                   id="customer_type1" value="0" checked>
                            <label class="custom-control-label" for="customer_type1">Individual</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" name="customer[classification]"
                                   id="customer_type2" value="1">
                            <label class="custom-control-label" for="customer_type2">Company</label>
                        </div>
                    </div>
                </div>
                <!-- will insert customer's company name and billing address element -->

                <div class="form-row">
                    <div class="form-group col-8">
                        <label for="lead_source">How did you find us?</label>
                        <select id="lead_source" class="form-control @error('project.lead_source') is-invalid @enderror"
                                name="project[lead_source]" required>
                            <option value="">Select Lead Source</option>
                            @foreach ($lead_sources as $lead_source)
                                <option value="{{ $lead_source->id }}"
                                    {{  old('project.lead_source', $project ? $project->lead_source : '' ) == $lead_source->id ? 'selected' : '' }}>{{ $lead_source->name }}</option>
                            @endforeach
                        </select>
                        @include('includes.partials.error', ['field' => 'project.lead_source'])
                    </div>
                    <div class="form-group col-4">
                        <label for="referral_code">{{ __('Referral Code:') }}</label>
                        <input id="referral_code" type="text"
                               class="form-control @error('customer.referral_code') is-invalid @enderror"
                               name="project[referral_code]" value="{{ old('project.referral_code') }}">
                        @include('includes.partials.error', ['field' => 'project.referral_code'])
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="first_name">{{ __('First Name:') }}</label>
                        <input id="first_name" type="text"
                               class="form-control @error('customer.first_name') is-invalid @enderror"
                               name="customer[first_name]" value="{{ old('customer.first_name') }}" required autofocus>
                        @include('includes.partials.error', ['field' => 'customer.first_name'])
                    </div>
                    <div class="form-group col-6">
                        <label for="last_name">{{ __('Last Name:') }}</label>
                        <input id="last_name" type="text"
                               class="form-control @error('customer.last_name') is-invalid @enderror"
                               name="customer[last_name]" value="{{ old('customer.last_name') }}" required>
                        @include('includes.partials.error', ['field' => 'customer.last_name'])
                    </div>
                </div>

                <div class="form-group">
                    <label for="email">{{ __('E-Mail Address:') }}</label>
                    <input id="email" type="email" class="form-control @error('customer.email') is-invalid @enderror"
                           name="customer[email]" value="{{ old('customer.email') }}" required>
                    @include('includes.partials.error', ['field' => 'customer.email'])
                </div>

                <contact :old_contacts="{{ json_encode([]) }}"></contact>

                <div class="form-group">
                    <label for="cell_phone">Cell Phone:</label>
                    <input id="cell_phone" type="text"
                           class="form-control @error('customer.cell_phone') is-invalid @enderror"
                           name="customer[cell_phone]" value="{{ old('customer.cell_phone') }}" required>
                    @include('includes.partials.error', ['field' => 'customer.cell_phone'])
                </div>

                <div class="form-group">
                    <label for="relationship">Connection to Property:</label>
                    <select id="relationship"
                            class="form-control @error('property.relationship') is-invalid @enderror"
                            name="property[relationship_id]" required>
                        <option value="">Customer Relationship with Property</option>
                        @foreach ($relationships as $relationship)
                            <option value="{{ $relationship->id }}"
                                {{  old('property.relationship', $project ? $project->relationship : '' ) == $relationship->id ? 'selected' : '' }}>{{ $relationship->name }}</option>
                        @endforeach
                    </select>
                    @include('includes.partials.error', ['field' => 'property.relationship'])
                </div>

                <div class="form-group">
                    <label for="customer_note">Customer Notes:</label>
                    <textarea id="customer_note" style="margin-bottom:4px;" rows="9" name="project[customer_notes]"
                              class="form-control @error('project.customer_notes') is-invalid @enderror">{{ old('project.customer_notes') }}</textarea>
                    @include('includes.partials.error', ['field' => 'project.customer_notes'])
                </div>
            </div>
        </div>
    </div>
    <div class="{{ $col_class }}">
        <div class="block {{ $mode == 'display' ? 'mb-0' : '' }}">
            <div class="block-header block-header-default">
                <h3 class="block-title">Property Details</h3>
            </div>
            <div class="block-content">
                <div class="form-group">
                    <label for="first_name">Project Classification</label>
                    <div class="col-12 pl-0">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" name="property[classification_id]"
                                   id="project_type1" value="0" checked>
                            <label class="custom-control-label" for="project_type1">Residential</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" name="property[classification_id]"
                                   id="project_type2" value="1">
                            <label class="custom-control-label" for="project_type2">Commercial</label>
                        </div>
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <label for="project_type">Type of Project</label>
                    <select id="project_type" class="form-control @error('project.project_type') is-invalid @enderror"
                            name="project[project_type]" required>
                        <option>Select Type of Project</option>
                        @foreach ($project_types as $project_type)
                            <option value="{{ $project_type->key }}"
                                {{  old('project.project_type', $project ? $project->project_type : '' ) == $project_type->key ? 'selected' : '' }}>{{ $project_type->name }}</option>
                        @endforeach
                    </select>
                    @include('includes.partials.error', ['field' => 'project.project_type'])
                </div>
                <div class="form-group">
                    <address-component :_old="{{ json_encode(Session::getOldInput()) }}"></address-component>
                    @include('includes.partials.error', ['field' => 'address'])
                </div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="current_roof_type_id">Current Material:</label>
                        <select id="current_roof_type_id"
                                class="form-control @error('project.current_roof_type_id') is-invalid @enderror"
                                name="project[current_roof_type_id]" required>
                            <option value="">Choose Current Roofing Material</option>
                            @foreach ($roof_types as $roof_type)
                                <option value="{{ $roof_type->id }}"
                                    {{  old('project.current_roof_type_id', $project ?  $project->current_roof_type_id : '' ) == $roof_type->id ? 'selected' : '' }}>{{ $roof_type->long_name }}</option>
                            @endforeach
                        </select>
                        @include('includes.partials.error', ['field' => 'project.current_roof_type_id'])
                    </div>
                    <div class="form-group col-6">
                        <label for="preferred_roof_type_id">Preferred Material:</label>
                        <select id="preferred_roof_type_id"
                                class="form-control @error('project.preferred_roof_type_id') is-invalid @enderror"
                                name="project[preferred_roof_type_id]" required>
                            <option value="">Choose Preferred Roofing Material</option>
                            @foreach ($roof_types as $roof_type)
                                <option value="{{ $roof_type->id }}"
                                    {{  old('project.preferred_roof_type_id', $project ? $project->preferred_roof_type_id : '' ) == $roof_type->id ? 'selected' : '' }}>{{ $roof_type->long_name }}</option>
                            @endforeach
                        </select>
                        @include('includes.partials.error', ['field' => 'project.preferred_roof_type_id'])
                    </div>
                </div>

                <div class="form-group">
                    <label for="project_description">Project Description:</label>
                    <textarea id="project_description" rows="9"
                              class="form-control @error('project.description') is-invalid @enderror"
                              name="project[description]">{{ old('project.description') }}</textarea>
                    @include('includes.partials.error', ['field' => 'project.description'])
                </div>
            </div>
        </div>
    </div>
</div>
