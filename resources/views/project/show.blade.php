@extends('layouts.backend')

@section('content-heading', 'Project Details')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Opportunity</a>
@endsection
@section('breadcrumb-active', 'Details')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <form method="post" action="{{ route('projects.update', $project->id) }}" enctype="multipart/form-data">
                @csrf
                @method('put')
                @include('project.form')
                <div class="block">
                    <div class="block-content">
                        <div class="row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success btn-block">Save Changes</button>
                            </div>
                            <div class="col-6">
                                <a href="/project/delete/{{ $project->id }}" data-toggle="modal"
                                   class="btn btn-danger btn-block">Delete Project</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-6">
            <div class="block">
                <div class="block-content project-appointments-list">
                    <h4>Appointments</h4>
                    <hr>
                    @if(isset($appointments) && count($appointments) > 0)
                        @foreach($appointments as $appointment)
                            <span>{{$appointment->last_name}}</span><span
                                style="margin-left:10px;margin-right:10px">{{$appointment->city}}</span>scheduled
                            from<span
                                style="margin-left:10px;margin-right:10px">{{date("Y-m-d h:i A", strtotime($appointment->start_timestamp))}}</span>
                            to<span
                                style="margin-left:10px">{{date("Y-m-d h:i A", strtotime($appointment->end_timestamp))}}</span>
                        @endforeach
                    @else
                        <p>No Appointments Found</p>
                    @endif
                    <a href="/calendar/scheduler/{{ $project->id }}" style="margin-top:10px"
                       class="btn btn-primary btn-block">Schedule New Appointment</a>
                    <a href="/calendar" class="btn btn-dark btn-small btn-block">View Calendar</a>
                </div>
            </div>
            <div class="block">
                <div class="block-content">
                    <h4>Measurements</h4>
                    <hr>
                    @if(isset($measurements))
                        <p>show measurements here</p>
                    @else
                        <p>No Measurements Found</p>
                        <a href="/estimator/properties/{{ $project->id }}" class="btn btn-primary btn-block">Enter
                            Measurements</a>
                        <a href="/eagleview" class="btn btn-dark btn-small btn-block">Order EagleView</a>
                    @endif
                </div>
            </div>
            <div class="block">
                <div class="block-content">
                    <h4>Estimate</h4>
                    <hr>
                    @if(isset($estimates))
                        <p>show estimate here</p>
                    @else
                        <p>No Estimates Found</p>
                        <a href="/estimate/create/{{ $project->id }}" class="btn btn-primary btn-block">Enter
                            Estimate</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
