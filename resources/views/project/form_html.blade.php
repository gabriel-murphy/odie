@php($project = $project ?? null)
@php($mode = $project ? 'display' : 'create')
@php($col_class = $mode == 'display' ? 'col-12' : 'col-6')
<div class="row">
    @if($mode == 'display')
        <div class="{{ $col_class }}">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Project Information</h3>
                </div>
                <div class="block-content">
                    <div class="form-group">
                        <label for="phase_id">Phase</label>
                        <select id="phase_id" class="form-control" name="phase_id">
                            @foreach($phases as $phase)
                                <option
                                    {{  old('phase_id', $project ? $project->phase_id : '' ) == $phase->id ? 'selected' : '' }}
                                    value="{{$phase->id}}">{{$phase->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    @endif

        <div class="col-md-12">
            <div class="js-wizard-simple block">
                <!-- Wizard Progress Bar -->
                <div class="progress rounded-0" data-wizard="progress" style="height: 8px;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                         style="width: 30%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <!-- END Wizard Progress Bar -->

                <!-- Step Tabs -->
                <ul class="nav nav-tabs nav-tabs-alt nav-fill" data-toggle="tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ _active_tab('step', 'contact_info_tab', true) }}" href="#contact_info_tab">1. Contact Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ _active_tab('step', 'property_info_tab') }}" href="#property_info_tab">2. Property Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  {{ _active_tab('step', 'appointment_tab') }}" href="#appointment_tab">3. Appointment</a>
                    </li>
                </ul>
                <!-- END Step Tabs -->

                <!-- Form -->

                <!-- Steps Content -->
                <div class="block-content block-content-full tab-content" style="min-height: 274px;">
                    <!-- Step 1 -->
                    <div class="tab-pane {{ _active_tab('step', 'contact_info_tab', true) }}" id="contact_info_tab" role="tabpanel">
                        <form action="{{ route('projects.steps.store','contact_info_tab') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <div class="d-flex">
                                    <label class="mr-4" for="customer_type">Customer Classification</label>
                                    <div class="col-6 pl-0">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio" name="customer[classification]"
                                                   id="customer_type_individual" value="individual" checked>
                                            <label class="custom-control-label"
                                                   for="customer_type_individual">Individual</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio" name="customer[classification]"
                                                   id="customer_type_company" value="company">
                                            <label class="custom-control-label" for="customer_type_company">Company</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="first_name">{{ __('First Name') }}</label>
                                    <input id="first_name" type="text"
                                           class="form-control @error('customer.first_name') is-invalid @enderror"
                                           name="customer[first_name]" value="{{ old('customer.first_name') }}"
                                           autofocus>
                                    @include('includes.partials.error', ['field' => 'customer.first_name'])
                                </div>
                                <div class="form-group col-6">
                                    <label for="last_name">{{ __('Last Name') }}</label>
                                    <input id="last_name" type="text"
                                           class="form-control @error('customer.last_name') is-invalid @enderror"
                                           name="customer[last_name]" value="{{ old('customer.last_name') }}" >
                                    @include('includes.partials.error', ['field' => 'customer.last_name'])
                                </div>
                            </div>

                            <div class="form-group" id="company_input" style="display: none">
                                <label for="company_name">Company Name</label>
                                <input id="company_name" type="text" name="customer[company]"
                                       class="form-control" value="{{ old('customer.company') }}">
                                @include('includes.partials.error', ['field' => 'customer.company'])
                            </div>

                            <div class="form-group">
                                <address-component :_old="{{ json_encode(Session::getOldInput()) }}" prefix="customer"></address-component>
                                @include('includes.partials.error', ['field' => 'address'])
                            </div>

{{--                            <div class="form-group">--}}
{{--                                <label for="email">{{ __('E-Mail Address:') }}</label>--}}
{{--                                <input id="email" type="email"--}}
{{--                                       class="form-control @error('customer.email') is-invalid @enderror"--}}
{{--                                       name="customer[emails][0]" value="{{ old('customer.email') }}" >--}}
{{--                                @include('includes.partials.error', ['field' => 'customer.email'])--}}
{{--                            </div>--}}

                           <opportunity-email old_emails="{{ json_encode([]) }}" prefix="customer"></opportunity-email>

                            <contact :old_contacts="{{ json_encode([]) }}" prefix="customer"></contact>

                            <div class="form-group">
                                <label for="relationship">Connection to Property:</label>
                                <select id="relationship"
                                        class="form-control @error('customer.relationship_id') is-invalid @enderror"
                                        name="customer[relationship_id]" required>
                                    <option value="">Customer Relationship with Property</option>
                                    @foreach ($relationships as $relationship)
                                        <option value="{{ $relationship->id }}"
                                            {{  old('property.relationship', $project ? $project->relationship : '' ) == $relationship->key ? 'selected' : '' }}>{{ $relationship->name }}</option>
                                    @endforeach
                                </select>
                                @include('includes.partials.error', ['field' => 'property.relationship'])
                            </div>
                            <div class="form-group">
                                <label for="customer_note">Customer Notes:</label>
                                <textarea id="customer_note" style="margin-bottom:4px;" rows="5"
                                          name="customer[customer_notes]"
                                          class="form-control resize-none @error('customer.customer_notes') is-invalid @enderror">{{ old('customer.customer_notes') }}</textarea>
                                @include('includes.partials.error', ['field' => 'customer.customer_notes'])
                            </div>

                            <div class="form-row">
{{--                                <div class="col-7">--}}
{{--                                    <div class="add-another-contact">--}}
{{--                                        <label class="wd-45P" for="relationship">Add Another Contact</label>--}}
{{--                                        <select id="relationship"--}}
{{--                                                class="form-control @error('property.relationship') is-invalid @enderror"--}}
{{--                                                name="property[relationship_id]" required>--}}
{{--                                            <option value="">Customer Relationship with Property</option>--}}
{{--                                            @foreach ($relationships as $relationship)--}}
{{--                                                <option value="{{ $relationship->id }}"--}}
{{--                                                    {{  old('property.relationship', $project ? $project->relationship : '' ) == $relationship->key ? 'selected' : '' }}>--}}
{{--                                                    {{ $relationship->name }}--}}
{{--                                                </option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                        @include('includes.partials.error', ['field' => 'property.relationship'])--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-5">
                                    <div class="text-right">
                                        <a class="btn btn-primary mr-3" href="#">Add Another Contact</a>
                                        <button type="submit" class="btn btn-primary">Enter Property Details
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Step 1 -->

                    <!-- Step 2 -->
                    <div class="tab-pane {{ _active_tab('step', 'property_info_tab') }}" id="property_info_tab" role="tabpanel">
                        <form action="{{ route('projects.steps.store','property_info_tab') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <div class="d-flex">
                                    <label class="mr-4">Project Classification</label>
                                    <div class="col-6 pl-0">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio"
                                                   name="property[classification_id]"
                                                   id="residential" value="0" checked>
                                            <label class="custom-control-label" for="residential">Residential</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio"
                                                   name="property[classification_id]"
                                                   id="commercial" value="1">
                                            <label class="custom-control-label" for="commercial">Commercial</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="css-control css-control-primary css-checkbox" for="same_address">
                                    <input type="checkbox" class="css-control-input" id="same_address" name="same_address"
                                           value="1" checked>
                                    <span class="css-control-indicator"></span> Same as the Primary Contact Address
                                </label>
                            </div>

                            <div class="form-group" id="company_address_input" style="display: none;">
                                <address-component :_old="{{ json_encode(Session::getOldInput()) }}" prefix="property"></address-component>
                                @include('includes.partials.error', ['field' => 'address'])
                            </div>

                            <div class="form-group">
                                <label for="project_type">Type of Project</label>
                                <select id="project_type"
                                        class="form-control @error('project.project_type') is-invalid @enderror"
                                        name="project[project_type]">
                                    <option>Select Type of Project</option>
                                    @foreach ($project_types as $project_type)
                                        <option value="{{ $project_type->key }}"
                                            {{  old('project.project_type', $project ? $project->project_type : '' ) == $project_type->key ? 'selected' : '' }}>
                                            {{ $project_type->name }}</option>
                                    @endforeach
                                </select>
                                @include('includes.partials.error', ['field' => 'project.project_type'])
                            </div>

                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="current_roof_type_id">Current Material:</label>
                                    <select id="current_roof_type_id"
                                            class="form-control @error('project.current_roof_type_id') is-invalid @enderror"
                                            name="project[current_roof_type_id]" required>
                                        <option value="">Choose Current Roofing Material</option>
                                        @foreach ($roof_types as $roof_type)
                                            <option value="{{ $roof_type->id }}"
                                                {{  old('project.current_roof_type_id', $project ?  $project->current_roof_type_id : '' ) == $roof_type->id ? 'selected' : '' }}>{{ $roof_type->long_name }}</option>
                                        @endforeach
                                    </select>
                                    @include('includes.partials.error', ['field' => 'project.current_roof_type_id'])
                                </div>
                                <div class="form-group col-6">
                                    <label for="preferred_roof_type_id">Preferred Material:</label>
                                    <select id="preferred_roof_type_id"
                                            class="form-control @error('project.preferred_roof_type_id') is-invalid @enderror"
                                            name="project[preferred_roof_type_id]" required>
                                        <option value="">Choose Preferred Roofing Material</option>
                                        @foreach ($roof_types as $roof_type)
                                            <option value="{{ $roof_type->id }}"
                                                {{  old('project.preferred_roof_type_id', $project ? $project->preferred_roof_type_id : '' ) == $roof_type->id ? 'selected' : '' }}>{{ $roof_type->long_name }}</option>
                                        @endforeach
                                    </select>
                                    @include('includes.partials.error', ['field' => 'project.preferred_roof_type_id'])
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="project_description">Project Description:</label>
                                <textarea id="project_description" rows="5"
                                          class="form-control resize-none @error('project.description') is-invalid @enderror"
                                          name="project[description]">{{ old('project.description') }}</textarea>
                                @include('includes.partials.error', ['field' => 'project.description'])
                            </div>

                            <div class="form-group">
                                <label for="arial_report">Order Arial Report:</label>
                                <select id="arial_report"
                                        class="form-control @error('project.arial_report_provider') is-invalid @enderror"
                                        name="project[arial_report_provider]">
                                    <option value="">Select Company</option>
                                    @foreach (app('constants')->findByGroup('ariel_report_providers') as $provider)
                                        <option value="{{ $provider->key }}"
                                            {{  old('project.arial_report_provider', $project ? $project->arial_report_provider : '' ) == $provider->key ? 'selected' : '' }}>
                                            {{ $provider->name }}</option>
                                    @endforeach
                                </select>
                                @include('includes.partials.error', ['field' => 'property.arial_report_provider'])
                            </div>

                            <div class="form-row">
                                <div class="col-7">
                                    <div class="add-another-project">
                                        <label for="another-project" class="wd-45P">Add Another Project</label>
                                        <select id="another-project" class="form-control">
                                            <option>Select Type of Project</option>
                                            @foreach ($project_types as $project_type)
                                                <option value="{{ $project_type->key }}">
                                                    {{ $project_type->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Enter Schedule
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Step 2 -->

                    <!-- Step 3 -->
                    <div class="tab-pane {{ _active_tab('step', 'appointment_tab') }}" id="appointment_tab" role="tabpanel">
                        <form action="{{ route('projects.steps.store','appointment_tab') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="estimator_id">Appointment For <span class="text-danger">*</span></label>
                                <select id="estimator_id" name="appointment[estimator_id]" required="required" class="form-control @error('appointment.estimator_id') is-invalid @enderror">
                                    <option value="">Select Estimator</option>
                                    @foreach($estimators as $estimator)
                                        <option value="{{ $estimator->id }}">{{ $estimator->name }}</option>
                                    @endforeach
                                </select>
                                @include('includes.partials.error', ['field' => 'appointment.estimator_id'])
                            </div>


                            <div class="form-group">
                                <label for="attendees-multi">Attendees</label>
                                <select class="js-select2 form-control" style="width: 100%" multiple name="appointment[attendees][]">
                                    @foreach ($estimators as $estimator)
                                        <option value="{{ $estimator->id }}">{{ $estimator->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="lead_sources_id">Lead Sources</label>
                                <select class="js-select2 form-control" style="width: 100%" id="lead_sources_id" multiple name="project[lead_source_id][]" required="required" >
{{--                                    <option value="">Select lead source</option>--}}
                                    @foreach($lead_sources as $lead_source)
                                        <option value="{{ $lead_source->id }}">{{ $lead_source->name }} ({{ $lead_source->subcategory }})</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group" id="referall_div" style="display:none">
                                <label for="referall_id">Referall</label>
                                <select id="referall_id" name="project[referall_id]" required="required" class="form-control">
                                    <option value="">Select Estimator</option>
                                    @foreach($estimators as $estimator)
                                        <option value="{{ $estimator->id }}">{{ $estimator->name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-4">
                                    <label for="date">Date</label>
                                    <input type='text' id="date" name="appointment[date]" class="js-flatpickr form-control">
                                </div>
                                <div class="form-group col-4">
                                    <label for="start_time">Start Time</label>
                                    <input type='text' id="start_time" name="appointment[start_time]" class="js-flatpickr form-control">
                                </div>
                                <div class="form-group col-4">
                                    <label for="end_time">End Time</label>
                                    <input type='text' id="end_time" name="appointment[end_time]" class="js-flatpickr form-control">
                                </div>
                                <div class="form-group col-12">
                                    <label for="notes">Note</label>
                                    <textarea rows="5" class="form-control resize-none" name="notes"></textarea>
                                </div>
                                <div class="text-right w-100">
                                    <button type="submit" class="js-swal-success btn btn-primary">Finish</button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- END Step 3 -->
                </div>
                <!-- END Steps Content -->

                <!-- END Form -->
            </div>
        </div>
</div>
