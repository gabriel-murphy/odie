@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Opportunities</a>
@endsection

@php($phase_name = $phase ? $phase->name : 'All')
@section('breadcrumb-active', $phase_name)

@section('content-heading', 'Opportunities :: '. $phase_name)

@section('content-header-action')
    <a class="btn btn-sm btn-primary" href="{{ route('projects.create') }}">
        <i class="fa fa-plus-circle"></i> Create New Opportunity
    </a>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Projects by Phase</h3>
                    <div class="block-options">
                        <a href="{{ route('projects.index') }}" class="btn-block-option"><i class="fa fa-refresh"></i></a>
                    </div>
                </div>
                <div class="block-content pb-4">
                    @foreach($phases as $_phase)
                        <a class="btn border btn-rounded mb-2 {{ ($phase && $phase->id == $loop->iteration) ? 'btn-primary' : 'shadow-sm' }}"
                           href="{{ route('projects.index', ['phase' => $_phase->id]) }}">{{ $_phase->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header">
                    <h3 class="block-title">Projects List</h3>
                    <div class="block-options">
                        @include('includes.common.search', ['route' => route('projects.index')])
                    </div>
                </div>
                <div class="block-content">
                    <table class="table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Classification</th>
                            <th>Phase</th>
                            <th>Address</th>
                            <th>Roof age</th>
                            <th>User</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($projects as $project)
                            <tr>
                                <td>
                                    <a href="/customer/">{{ ucfirst($project->first_name) }} {{ ucfirst($project->last_name) }}</a>
                                </td>
                                <td>{{ $project->clas }}</td>
                                <td>{{ $project->city }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $project->created_at->format('D M j, Y g:i a') }}</td>
                                <td>{{ $project->updated_at->format('D M j, Y g:i a') }}</td>
                                <td><a href="{{ route('projects.show', $project->id) }}" class="btn btn-sm btn-primary">View Project</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="999" class="text-center">{{ _constant('no_record') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
