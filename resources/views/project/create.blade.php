@extends('layouts.backend')

@section('styles')
    <link href="{{ asset('js/plugins/flatpickr/flatpickr.css') }}" rel='stylesheet'>
    <link rel="stylesheet" href=" {{ asset( 'js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link href="{{ asset('js/plugins/select2/css/select2.min.css') }}" rel='stylesheet'>
@endsection

@section('content-heading', 'Opportunity Creation Wizard')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Opportunity</a>
@endsection
@section('breadcrumb-active', 'Create')

@section('content')
    @include('includes.partials.errors')
{{--    <form method="post" action="{{ route('projects.store') }}" enctype="multipart/form-data">--}}
{{--        @csrf--}}
        @include('project.form_html')
    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('js/plugins/bootstrap-wizard/be_forms_wizard.min.js')}}"></script>
    <script src="{{ asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        jQuery(function () {
            Codebase.helper(['flatpickr', 'select2']);
            $('#date').flatpickr({
                defaultDate: '<?php echo date('Y-m-d'); ?>'
            });
            $("#start_time").flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                defaultDate: "<?php echo \Carbon\Carbon::now()->toTimeString();  ?>"
            });
            $("#end_time").flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                defaultDate: "<?php echo \Carbon\Carbon::now()->addHours(2); ?>"
            });
        });
    </script>
    <script>
        $(function () {
            $("input[name='customer[classification]']").change(function () {
                if($(this).val() === 'company'){
                    $('#company_input').show()
                } else {
                    $('#company_input').hide()
                }
            });

            $('#same_address').change(function () {
                if (!$(this).is(':checked')){
                    $('#company_address_input').show();
                } else {
                    $('#company_address_input').hide();
                }
            });

            $('.next-tab').click(function () {
                let tab = $(this).data('tab-name');
                $("a[href='"+tab+"']").click();
            });

            $("#lead_sources_id").change(function () {
                if($(this).val().includes('3') || $(this).val().includes('4') || $(this).val().includes('9')){ //all belongs to referal in constants table
                    $("#referall_div").show();
                }
                else {
                    $("#referall_div").hide();
                }
            });
        })
    </script>
@endsection
