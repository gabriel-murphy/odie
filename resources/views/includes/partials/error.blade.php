@error($field)
<span class="invalid-feedback {{ $class ?? '' }}" role="alert" >
    <strong>{{ $message }}</strong>
</span>
@enderror
