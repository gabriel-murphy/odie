<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                    <img src="{{ asset('images/logo.png') }}" alt="">
                </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout"
                        data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->
                <div class="content-header-item">
                    <a class="font-w700" href="/">
                        <img src="{{ asset('images/logo.png') }}"  alt="">
                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Side User -->
        <div class="content-side content-side-full content-side-user px-10 align-parent mt-2 pb-150">
            <!-- Visible only in mini mode -->
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
            </div>
            <!-- END Visible only in mini mode -->

            <!-- Visible only in normal mode -->
            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="javascript:void(0)">
                    <img class="img-avatar" src="{{ auth()->user()->avatar_path }}" alt="">
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-sm font-w600 text-uppercase"
                           href="javascript:void(0)">{{ auth()->user()->name }}</a>
                    </li>
                    <li class="list-inline-item">
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <a class="link-effect text-dual-primary-dark" data-toggle="layout"
                           data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                            <i class="si si-drop"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" href="javascript:void(0)" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="si si-logout"></i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-sm font-w600 text-uppercase"
                           href="javascript:void(0)">{{ auth()->user()->client->name }}</a>
                    </li>

                </ul>
            </div>
            <!-- END Visible only in normal mode -->
        </div>
        <!-- END Side User -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li>
                    <a class="{{ _active_menu('dashboard') }}" href="{{ route('dashboard') }}">
                        <i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span>
                    </a>
                </li>

                <li class="{{ _active_menu('project', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-calendar-check"></i><span class="sidebar-mini-hide">Scheduler</span>
                    </a>
                    <ul>
                        <li><a class="{{ _active_menu('projects.create') }}"
                               href="{{ route('projects.create') }}"><span>Create Opportunity</span></a></li>
                        <li><a class="{{ _active_menu('projects/*') }}" href="{{ route('projects.index') }}"><span>Schedule New Leads</span></a>
                        </li>
                    </ul>
                </li>

                <li class="{{ _active_menu('maximus/*', true) }}">
                    {!! _sidebar_menu_group('Maximus', 'fad fa-cabinet-filing') !!}
                    <ul>
                        {!! _sidebar_menu('Submit AOB', route('maximus.claims.create')) !!}
                        {!! _sidebar_menu('Manage Claims', route('maximus.claims.index')) !!}
                        {!! _sidebar_menu('Financials') !!}
                    </ul>
                </li>

                <li class="{{ _active_menu('campaigns/*', true) }}">
                    {!! _sidebar_menu_group('Campaigns Manager', 'fad fa-bullhorn') !!}
                    <ul>
                        {!! _sidebar_menu('Campaigns', route('campaigns.index')) !!}
                        {!! _sidebar_menu('New Campaign', route('campaigns.create')) !!}
                    </ul>
                </li>

                <li class="{{ _active_menu('estimator/*', true) }}">
                    {!! _sidebar_menu_group('Estimator', 'si si-calculator') !!}
                    <ul>
                        {!! _sidebar_menu('Structure Details', route('estimator.index', 'structure-details')) !!}
                        {!! _sidebar_menu('Roof Selector Utility', route('estimator.index', 'roof-selector')) !!}
                        {!! _sidebar_menu('Jobs Awaiting Estimates') !!}
                        {!! _sidebar_menu('New Construction') !!}
                        {!! _sidebar_menu('Roof Buildlists', route('estimator.buildlists')) !!}
                    </ul>
                </li>

                <li class="{{ _active_menu('signer/*', true) }}">
                    {!! _sidebar_menu_group('e-Signer', 'fad fa-file-signature') !!}
                    <ul>
                        {!! _sidebar_menu('Obtain e-Signature') !!}
                        {!! _sidebar_menu('Signature Log') !!}
                    </ul>
                </li>

                <li class="{{ _active_menu('harvester/*', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-seedling"></i><span class="sidebar-mini-hide">Harvester</span>
                    </a>
                    <ul>
                        <li><a href="/harvester/market">Addressable Market</a></li>
                        <li><a href="/harvester/candidates">Roofing Candidates</a></li>
                    </ul>
                </li>

                <li>
                    <a class="{{ _active_menu('customers') }}" href="{{ route('customers.index') }}">
                        <i class="fad fa-house-return"></i><span class="sidebar-mini-hide">Leads</span>
                    </a>
                </li>

                <li class="{{ _active_menu('supervisor/*', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-seedling"></i><span class="sidebar-mini-hide">Supervisor</span>
                    </a>
                    <ul>
                        <li><a href="/harvester/market">Permitting</a></li>
                        <li><a href="/harvester/candidates">Production Board</a></li>
                        <li><a href="#">Crew Assignments & Calendars</a></li>
                        <li><a href="/harvester/candidates">Production Board</a></li>
                    </ul>
                </li>

                <li class="{{ _active_menu('calendars/*', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-calendar-star"></i><span class="sidebar-mini-hide">Calendars</span>
                    </a>
                    <ul>
                        <li><a href="{{ route('appointments.index') }}">Upcoming Appointments</a></li>
                        <li><a href="#">Tear-Offs Scheduled</a></li>
                        <li><a href="#">Installations Scheduled</a></li>
                        <li><a href="#">Dry-Ins Scheduled</a></li>
                        <li><a href="#">Final Inspections</a></li>
                        <li><a href="#">Repairs & Warranty Work</a></li>
                        <li><a href="#">Other Events</a></li>
                    </ul>
                </li>

                <li class="{{ _active_menu('marketing/*', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-handshake"></i> <span class="sidebar-mini-hide">Sales & Marketing</span>
                    </a>
                    <ul>
                        <li><a href="/marketing/leads">Website Leads</a></li>
                        <li><a href="/marketing/leads">Lead Source Manager</a></li>
                        <li><a href="/marketing/reviews">Review Promoter</a></li>
                        <li><a href="/marketing/reviews">Postcard Campaigns</a></li>
                    </ul>
                </li>

                <li class="{{ _active_menu('finance/*', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fa fad fa-money-check-edit-alt"></i><span class="sidebar-mini-hide">Financial</span>
                    </a>
                    <ul>
                        <li><a href="/finance/sales-overrides">Pending Overrides</a></li>
                        <li><a href="/finance/projects-cost">Job Profitability</a></li>
                        <li><a href="/finance/payments">Payment Processing</a></li>
                    </ul>
                </li>

                <li class="{{ _active_menu('email/*', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-mail-bulk"></i><span class="sidebar-mini-hide">Communications</span>
                    </a>
                    <ul>
                        <li class="{{ _active_menu('templates/emails/*', true) }}">
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#">Templates</a>
                            <ul>
                                <li><a href="{{ route('templates.emails.create') }}">Add New</a></li>
                                <li><a href="{{ route('templates.emails.index') }}">View All</a></li>
                            </ul>
                        </li>
                        <li class="{{ _active_menu('placeholders/*', true) }}">
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#">Placeholders</a>      <!-- AKA "Tokens"-->
                            <ul>
                                <li><a href="{{ route('placeholders.create') }}">Add New</a></li>
                                <li><a href="{{ route('placeholders.index') }}">View All</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('emails.odmails') }}">OD E-mails</a></li>
                        <li><a href="{{ route('emails.odmails') }}">OD Texts</a></li>       <!-- Twillio for texting with API -->
                        <li><a href="{{ route('emails.odmails') }}">OD Voice</a></li>       <!-- Twillio for voice for voicemail drops -->
                    </ul>
                </li>

                <li class="{{ _active_menu('admin/*', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-users-cog"></i><span class="sidebar-mini-hide">Admin Settings</span>
                    </a>
                    <ul>
                        <li><a href="{{ route('settings.company.show') }}">Company Settings</a></li>
                        <li><a href="/admin/corp/locations">Location Management</a></li>
                        <li><a href="/admin/corp/templates">Forms and Templates</a></li>
                        <li><a href="/admin/corp/material-labor">Material & Labor</a></li>
                        <li><a href="/admin/corp/phases">Phases CRUD</a></li>
                        <li><a href="/admin/corp/milestones">Milestones CRUD</a></li>
                        <li><a href="/admin/corp/rules-notifications">Rules & Notifications</a></li>
                        <li><a href="/admin/corp/financial">Markup & Profits</a></li>
                        <li><a href="/admin/corp/commissions">Commissions</a></li>
                        <li><a href="/admin/corp/waste-factors">Waste Factors</a></li>
                        <li><a href="/admin/corp/users">User Management (CRUD)</a></li>
                        <li><a href="/admin/corp/database">Database View</a></li>
                        <li><a href="{{ route('constants.index') }}">Constant Group</a></li>
                    </ul>
                </li>
                <li class="{{ _active_menu('insurance_carriers/*', true) }}">
                    {!! _sidebar_menu_group('Insurance Carrier', 'fad fa-building') !!}
                    <ul>
                        {!! _sidebar_menu('Add New', route('insurance_carriers.create')) !!}
                        {!! _sidebar_menu('View All', route('insurance_carriers.index')) !!}
                    </ul>
                </li>

                <li class="{{ _active_menu('users', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-user-unlock"></i><span class="sidebar-mini-hide">User/Crew Management</span>
                    </a>
                    <ul>
                        <li><a class="{{ _active_menu('users/create') }}" href="{{ route('users.create') }}">Add New</a>
                        </li>
                        <li><a class="{{ _active_menu('users') }}" href="{{ route('users.index') }}">View All</a></li>
                    </ul>
                </li>

                <li class="{{ _active_menu('developer-tools', true) }}">
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        <i class="fad fa-tools"></i><span class="sidebar-mini-hide">Developer Tools</span>
                    </a>
                    <ul>
                        <li><a class="{{ _active_menu('developer-tools/logs') }}" href="{{ route('developer_tools.logs') }}">Logs</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- Sidebar Content -->
</nav>
