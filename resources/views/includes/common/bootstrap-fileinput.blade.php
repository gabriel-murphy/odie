<script src="{{ asset('js/plugins/bootstrap-fileinput/js/plugins/piexif.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/js/plugins/sortable.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/js/fileinput.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/js/locales/fr.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/js/locales/es.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/themes/fas/theme.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/themes/explorer-fas/theme.js') }}" type="text/javascript"></script>
<script>

    $(document).ready(function () {
        $(".bootstrap-fileinput").fileinput({
            uploadUrl: "{{env('APP_URL')}}/api/upload",
            fileActionSettings: {
                showUpload: true,
            },
            uploadExtraData: {
                'folderName': $('#uuid').val()
            },
            browseClass: "btn btn-secondary btn-sm",
            showUpload: false,
            showBrowse: false,
            showCaption: false,
            showRemove: false,
            overwriteInitial: false,
            initialPreviewAsData: true,
            minFileCount: 8,
            maxFileCount: 8,
            allowedFileExtensions: ["jpeg", "png", "jpg", "gif", "svg"],
            'theme': 'fas',
        }).on('fileloaded', function (event, file, previewId, index, reader) {
            let $uploadBtn = $(document.getElementById(previewId)).find('.kv-file-upload');
            setTimeout(
                function () {
                    $uploadBtn.trigger("click");
                }, 500
            );
        });
    });

    $(document).ready(function () {

        $(".bootstrap-pdf-fileinput").fileinput({
            uploadUrl: "{{env('APP_URL')}}/api/upload",
            fileActionSettings: {
                showUpload: true,
            },
            uploadExtraData: {
                'inputName': $(this)[0],
                'folderName': $('#uuid').val()
            },
            allowedFileExtensions: ["pdf"],
            initialPreviewAsData: true,
            overwriteInitial: false,
            showBrowse: false,
            showCaption: false,
            showUpload: false,
            showRemove: false,
            theme: 'fas',
        })
            .on('fileloaded', function (event, file, previewId, index, reader) {
                let $uploadBtn = $(document.getElementById(previewId)).find('.kv-file-upload');
                setTimeout(
                    function () {
                        $uploadBtn.trigger("click");
                    }, 500
                );
            });

        $(".file-input").fileinput({
            fileActionSettings: {
                showUpload: true,
                showRemove: true,
            },
            initialPreviewAsData: true,
            overwriteInitial: false,
            showBrowse: false,
            showCaption: false,
            showUpload: false,
            showRemove: false,
            browseOnZoneClick: true,
            theme: 'fas',
        });
    });

</script>
