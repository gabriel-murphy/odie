<div class="modal fade border-0" id="file_viewer_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="mb-3">
                    <iframe class="border-0" width="100%" height="500px" id="file_viewer"></iframe>
                </div>
            </div>
            <div class="modal-footer border-0">
                <div class="float-right">
                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
