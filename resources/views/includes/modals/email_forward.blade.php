<div class="modal fade border-0 text-left" id="forward_email_to_{{$odmail_id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form class="d-inline" action="{{ route('emails.odmails.forward', $odmail_id) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="recipients">Recipient</label>
                        <input type="text" name="recipients" class="form-control">
                    </div>
                    <div class="float-right">
                        <button type="submit" class="btn btn-sm btn-success">Send</button>
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
