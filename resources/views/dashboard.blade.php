@extends('layouts.backend')

@section('title','Dashboard!')

@section('content-heading', 'Welcome, '. auth()->user()->name)

@section('content')
    <div class="row" >
        <div class="col-12">
            <img src="{{ asset('images/dashboard-coming-soon.jpg') }}" class="w-100" >
        </div>
    </div>
@endsection
