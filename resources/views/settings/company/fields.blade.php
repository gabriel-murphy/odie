@php($project = $project ?? null)

<div class="form-row">
    <div class="form-group col-6">
        <label for="name">Company Name:</label>
        <input id="name" type="text" name="name"
               class="form-control @error('name') is-invalid @enderror"
               value="{{ old('name', isset($template) ? $template->name : '') }}">
        @include('includes.partials.error', ['field' => 'name'])
    </div>
    <div class="form-group col-6">
        <label for="license">Contractor License #</label>
        <input id="license" type="text" name="license"
               class="form-control @error('license') is-invalid @enderror"
               value="{{ old('license', isset($template) ? $template->license : '') }}">
        @include('includes.partials.error', ['field' => 'license'])
    </div>
</div>

<contact :old_contacts="{{ json_encode([]) }}"></contact>

<div class="form-row">
    <div class="form-group col-6">
        <label for="email">Email :</label>
        <input id="email" type="email" name="email"
               class="form-control @error('email') is-invalid @enderror"
               value="{{ old('email', isset($template) ? $template->email : '') }}">
        @include('includes.partials.error', ['field' => 'email'])
    </div>

    <div class="form-group col-6">
        <label for="address">Address :</label>
        <input id="address" type="text" name="address"
               class="form-control @error('address') is-invalid @enderror"
               value="{{ old('address', isset($template) ? $template->address : '') }}">
        @include('includes.partials.error', ['field' => 'address'])
    </div>
</div>

<div class="form-row">
    <div class="form-group col-3">
        <label for="city">City:</label>
        <input id="city" type="text" name="city"
               class="form-control @error('city') is-invalid @enderror"
               value="{{ old('city', isset($template) ? $template->city : '') }}">
        @include('includes.partials.error', ['field' => 'city'])
    </div>
    <div class="form-group col-3">
        <label for="state">State :</label>
        <input id="state" type="text" name="state"
               class="form-control @error('state') is-invalid @enderror"
               value="{{ old('state', isset($template) ? $template->state : '') }}">
        @include('includes.partials.error', ['field' => 'state'])
    </div>
    <div class="form-group col-3">
        <label for="zip">ZIP :</label>
        <input id="zip" type="text" name="zip"
               class="form-control @error('zip') is-invalid @enderror"
               value="{{ old('zip', isset($template) ? $template->zip : '') }}">
        @include('includes.partials.error', ['field' => 'zip'])
    </div>
    <div class="form-group col-3">
        <label for="country">Country :</label>
        <input id="country" type="text" name="country"
               class="form-control @error('country') is-invalid @enderror"
               value="{{ old('country', isset($template) ? $template->country : '') }}">
        @include('includes.partials.error', ['field' => 'country'])
    </div>
</div>

