@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Company Settings</a>
@endsection
@section('breadcrumb-active', 'Edit')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('js/plugins/summernote/summernote.css')}}">

@endsection

@section('content')
    @include('includes.partials.errors')
    <form method="post" action="{{ route('projects.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Edit Company Settings</h3>
                    </div>
                    <div class="block-content">
                        @include('settings.company.fields')
                    </div>
                    <div class="row justify-content-center pb-3">
                        <div class="col-md-2 offset-1">
                            <button type="submit" class="btn btn-primary btn-sm">
                                Update
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('scripts')
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script>
        jQuery(function () {
            Codebase.helper('summernote');
        });
    </script>
@endsection
