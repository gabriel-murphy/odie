@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Settings</a>
@endsection
@section('breadcrumb-active', 'Company Settings')

@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block block-link-shadow overflow-hidden">
                <div class="block-content block-content-full">
                    <div class="block-header p-0">
                        <h5 class="text-uppercase">COMPANY INFORMATION</h5>
                        <div class="block-options">
                            <a href="{{ route('settings.company.edit') }}" class="btn btn-primary btn-sm">
                                <i class="fa fa-pencil"></i> Edit Profile
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <table class="table-responsive table-borderless">
                                <tbody class="align-top">
                                <tr>
                                    <th>Address:</th>
                                    <td>805 North East 7th Terrace, Cape Coral, Florida, United States, 33909</td>
                                </tr>
                                <tr>
                                    <th>License #:</th>
                                    <td>CCC1330707</td>
                                </tr>
                                <tr>
                                    <th>Phone:</th>
                                    <td>(239) 458-7663</td>
                                </tr>
                                <tr>
                                    <th>Email:</th>
                                    <td><a href="javascript:void(0)">service@romanroofinginc.com</a></td>
                                </tr>
                                <tr>
                                    <th>Users:</th>
                                    <td>4</td>
                                </tr>
                                <tr>
                                    <th>Subscriber Id:</th>
                                    <td>303</td>
                                </tr>
                                <tr>
                                    <th>Subscriber Since:</th>
                                    <td>03/15/2018</td>
                                </tr>
                                <tr>
                                    <th>Subscription Type:</th>
                                    <td>JobProgress Multi</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('images/user/0.png') }}" class="rounded w-50">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="block block-link-shadow overflow-hidden">
                <div class="block-content block-content-full">
                    <h5 class="text-uppercase">OWNER(ADMIN) DETAILS</h5>
                    <div class="row ">
                        <div class="col-md-8">
                            <table class="table-responsive table-borderless">
                                <tbody class="align-top">
                                <tr>
                                    <th>Name:</th>
                                    <td>Lindsey Dopfer</td>
                                </tr>
                                <tr>
                                    <th>Address:</th>
                                    <td>805 North East 7th Terrace, Cape Coral, Florida,
                                        33909, United States
                                    </td>
                                </tr>
                                <tr>
                                    <th>Phone:</th>
                                    <td>(239) 458-7663</td>
                                </tr>
                                <tr>
                                    <th>Email:</th>
                                    <td><a href="javascript:void(0)">service@romanroofinginc.com</a></td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#" class="btn btn-danger btn-sm">unsubscribe</a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <img src="{{ asset('images/user/0.png') }}" class="rounded w-50">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
