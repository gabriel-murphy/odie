@extends('layouts.backend')

@section('content')
    <!-- Main section-->
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="content-title">Harvester :: Roofing Candidates
                    <small>Irma Homeowners, Home Value > $200K, Roof Age 15+ Years</small>
                </div>
                <div class="ml-auto"><a class="btn btn-labeled btn-primary float-right" href="/project"><span
                            class="btn-label"><i class="fa fa-plus-circle"></i></span>Select Addresses/People to Enhance
                        and Touch</a></div>
            </div>
            <div class="container-fluid">

                <div class="row justify-content-center">
                    <div class="col-md-12">


                        <div class="block">
                            <div class="block-header block-header-default">
                                <div class="card-title">Actual Data, Refreshed as of March 8, 2020</div>
                            </div>
                            <div class="block-content">
                                <div class="table-responsive">
                                    <table class="table table-striped my-4 w-100" id="datatable1">
                                        <thead>
                                        <tr>
                                            <th data-priority="1">Owner</th>
                                            <th>Main Image</th>
                                            <th>Site Address</th>
                                            <th>Roof Age</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($candidates as $candidate)
                                            <tr class="gradeX">
                                                <td> {{ $candidate->owner }} </td>
                                                <td><img src="{{ $candidate->image_structure }}"></td>
                                                <td> {{ $candidate->site_address }} </td>
                                                <td> {{ $candidate->roof_age }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
