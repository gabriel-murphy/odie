@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Insurance Carrier</a>
@endsection
@section('breadcrumb-active', 'Show')

@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block block-link-shadow overflow-hidden">
                <div class="block-content block-content-full">
                    <div class="block-header p-0">
                        <h5 class="text-uppercase" title="Insurance Carrier Subject">
                            Insurance Carrier (<a href="#">{{ $insuranceCarrier->name }}</a>)
                        </h5>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table-responsive table-borderless">
                                <tbody class="align-top">
                                <tr>
                                    <th>Name:</th>
                                    <td class="pl-3">{{ $insuranceCarrier->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email:</th>
                                    <td class="pl-3">{{ $insuranceCarrier->email }}</td>
                                </tr>
                                <tr>
                                    <th>Website:</th>
                                    <td class="pl-3">{{ $insuranceCarrier->website }}</td>
                                </tr>
                                <tr>
                                    <th>Claims URL:</th>
                                    <td class="pl-3">{{ $insuranceCarrier->claims_url }}</td>
                                </tr>
                                <tr>
                                    <th>Phone No:</th>
                                    <td class="pl-3">{{ $insuranceCarrier->phone }}</td>
                                </tr>
                                <tr>
                                    <th>Claims Phone No:</th>
                                    <td class="pl-3">{{ $insuranceCarrier->claims_phone }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
