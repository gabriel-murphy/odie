@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Insurance Carrier</a>
@endsection
@section('breadcrumb-active', 'Create')


@section('content')
    <div class="block">
        <div class="block-header block-header-default">Add Insurance Carrier</div>
        <div class="block-content">
            @include('includes.partials.errors')
            <form action="{{ route('insurance_carriers.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @include('insurance-carrier.fields')
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

