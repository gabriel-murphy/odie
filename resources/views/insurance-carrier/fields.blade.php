<div class="form-row">
    <div class="form-group col-6">
        <label for="name">Insurance Carrier Legal Name:</label>
        <input id="name" type="text" name="name"
               class="form-control @error('name') is-invalid @enderror"
               value="{{ old('name', isset($insuranceCarrier) ? $insuranceCarrier->name : '') }}">
        @include('includes.partials.error', ['field' => 'name'])
    </div>

    <div class="form-group col-6">
        <label for="email">General Email Address:</label>
        <input id="email" type="email" name="email"
               class="form-control @error('email') is-invalid @enderror"
               value="{{ old('email', isset($insuranceCarrier) ? $insuranceCarrier->email : '') }}">
        @include('includes.partials.error', ['field' => 'email'])
    </div>
</div>

<div class="form-row">
    <div class="form-group col-6">
        <label for="website">Website:</label>
        <input id="website" type="text" name="website"
               class="form-control @error('website') is-invalid @enderror"
               value="{{ old('website', isset($insuranceCarrier) ? $insuranceCarrier->website : '') }}">
        @include('includes.partials.error', ['field' => 'website'])
    </div>

    <div class="form-group col-6">
        <label for="claims_url">Claim Submission URL:</label>
        <input id="claims_url" type="text" name="claims_url"
               class="form-control @error('claims_url') is-invalid @enderror"
               value="{{ old('claims_url', isset($insuranceCarrier) ? $insuranceCarrier->claims_url : '') }}">
        @include('includes.partials.error', ['field' => 'claims_url'])
    </div>
</div>

<div class="form-row">
    <div class="form-group col-6">
        <label for="phone">General Phone Number:</label>
        <input id="phone" type="text" name="phone"
               class="form-control @error('phone') is-invalid @enderror"
               value="{{ old('phone', isset($insuranceCarrier) ? $insuranceCarrier->phone : '') }}">
        @include('includes.partials.error', ['field' => 'phone'])
    </div>

    <div class="form-group col-6">
        <label for="claims_phone">Claims Phone Number:</label>
        <input id="claims_phone" type="text" name="claims_phone"
               class="form-control @error('claims_phone') is-invalid @enderror"
               value="{{ old('claims_phone', isset($insuranceCarrier) ? $insuranceCarrier->claims_phone : '') }}">
        @include('includes.partials.error', ['field' => 'claims_phone'])
    </div>
</div>
