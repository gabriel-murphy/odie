@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Insurance Carrier</a>
@endsection
@section('breadcrumb-active', 'Edit')

@section('content')
    @include('includes.partials.errors')
    <div class="block">
        <div class="block-header block-header-default">Edit Insurance Carrier</div>
        <div class="block-content">
            @include('includes.partials.errors')
            <form action="{{ route('insurance_carriers.update', $insuranceCarrier->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                @include('insurance-carrier.fields')
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">Edit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
