@extends('layouts.backend')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Insurance Carriers List</h3>
            <div class="block-options">
                @include('includes.common.search', ['route' => route(Route::currentRouteName())])
            </div>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr class="text-uppercase">
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Website</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($insurance_carriers as $insurance)
                        <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="font-w600">{{ $insurance->name }}</td>
                        <td>{{ $insurance->email }}</td>
                        <td>{{ $insurance->website }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ route('insurance_carriers.show', $insurance->id ) }}"
                                   class="btn btn-sm btn-secondary"
                                   data-toggle="tooltip" title="Show">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a href="{{ route('insurance_carriers.edit', $insurance->id ) }}"
                                   class="btn btn-sm btn-secondary"
                                   data-toggle="tooltip" title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <button data-toggle="modal" data-target="#confirm_insurance_{{ $insurance->id }}"
                                        class="btn btn-sm btn-outline-danger" title="">
                                    <i class="fa fa-trash"></i></button>
                                 @include('includes.modals.confirm', ['model' => 'insurance', 'route' => route('insurance_carriers.destroy', $insurance->id), 'form' => true])
                            </div>
                        </td>
                    </tr>
                    @empty
                        <td colspan="5" class="text-center text-secondary">no record found</td>
                    @endforelse
                    </tbody>
                </table>

                  <div class="pagination justify-content-center">
                      {{ $insurance_carriers->links() }}
                  </div>
            </div>
        </div>
    </div>
@endsection
