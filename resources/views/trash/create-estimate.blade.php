@extends('layouts.backend')

@section('content')
<!-- Main section-->
    <section class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
            <div class="content-header">
               <div class="content-title">Estimator :: Create Estimates<small>Step 3 - Generate & Send Estimate</small></div>
            </div>
            <div class="container-fluid">
                <form method="post" action="/estimator/calculate" enctype="multipart/form-data">
                    @if(Session::has('message'))
                    <div class="alert alert-success" id="flash-message"><h3> {!! session('message') !!}</h3></div>
                    @endif
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            @csrf
                            <div class="block">
                                <div class="block-header block-header-default">Create New Estimate for Project #{!!session('project_info[id]')!!} (Project_Address)</div>
                                <div class="block-content">
                                    <div class="form-group row">
                                        <label for="roofing_material" class="col-md-4 col-form-label text-md-right">{{ __('Roofing Material:') }}</label>

                                        <div class="col-md-3">
                                            <select id="new_roofing_material" class="form-control @error('new_roofing_material') is-invalid @enderror" name="new_roofing_material" value="{{ old('new_roofing_material') }}" required>
                                                <option>Select Roofing Material</option>
                                                @foreach ($roofing_materials as $roofing_material)
                                                <option value="{{ $roofing_material->id }}">{{ $roofing_material->name }}</option>
                                                        @endforeach
                                            </select>
                                            @error('new_roofing_material')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="roof_product_manufacturer" class="col-md-4 col-form-label text-md-right">{{ __('Product Manufacturer:') }}</label>
                                        <div class="col-md-3">
                                            <select id="roof_product_manufacturer" class="form-control @error('roof_product_manufacturer') is-invalid @enderror" name="roof_product_manufacturer" value="{{ old('roof_manufacturer') }}" required>
                                                <option>Select Roofing Manufacturer</option>
                                                @foreach ($roof_manufacturers as $roof_manufacturer)
                                                <option value="{{ $roof_manufacturer->id }}">{{ $roof_manufacturer->short_name }}</option>
                                                        @endforeach
                                            </select>
                                            @error('roof_product_manufacturer')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="roof_product_line" class="col-md-4 col-form-label text-md-right">{{ __('Product Line:') }}</label>

                                        <div class="col-md-3">
                                            <select id="roof_product_line" class="form-control @error('product_line') is-invalid @enderror" name="roof_product_line" value="{{ old('roof_product_line') }}" required>
                                                <option>Select Product Line</option>
                                                @foreach ($roof_product_lines as $roof_product_line)
                                                <option value="{{ $roof_product_line->id }}">{{ $roof_product_line->name }}</option>
                                                        @endforeach
                                            </select>
                                            @error('product_line')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="warranty" class="col-md-4 col-form-label text-md-right">{{ __('Warranty:') }}</label>

                                        <div class="col-md-3">
                                            <select id="warranty" class="form-control @error('warranty') is-invalid @enderror" name="warranty" value="{{ old('warranty') }}" required>
                                                <option>Select Warranty</option>
                                                <option value="">No Warranty</option>
                                                @foreach ($roof_warranties as $warranty)
                                                <option value="{{ $warranty->id }}">{{ $warranty->short_name }}</option>
                                                        @endforeach
                                            </select>
                                            @error('warranty')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="insurance" class="col-md-4 col-form-label text-md-right">{{ __('For Insurance Proceeds:') }}</label>

                                        <div class="col-md-3">
                                            
                                            <label class="css-control css-control-primary css-switch mr-2" style="padding-top:3px;margin-right:10px">
                                                <input type="checkbox" id="insurance" name="insurance" class="css-control-input"/>
                                                    <span class="css-control-indicator"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="financing" class="col-md-4 col-form-label text-md-right">{{ __('Project') }}</label>

                                        <div class="col-md-3">
                                            
                                            <label class="css-control css-control-primary css-switch mr-2" style="padding-top:3px;margin-right:10px">
                                                <input type="checkbox" id="financing" name="financing" class="css-control-input"/>
                                                <span class="css-control-indicator"></span>
                                            </label>

                                        </div>
                                    </div>

                                    <div class="row justify-content-center">
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary btn-lg btn-block" style="width:300px;margin:auto">
                                                Create Cost Estimate
                                            </button>
                                        </div>
                                    </div>
                                    <input id = "insurance_value" name = "insurance_value" value="0" hidden/>
                                    <input id = "financing_value" name = "financing_value" value="0" hidden/>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script style="script/JavaScript">
        $('#insurance').on('change', () => {
            $('#insurance').is(':checked') == true ? $('#insurance_value').val(1) : $('#insurance_value').val(0);
        })
        $('#financing').on('change', () => {
            $('#financing').is(':checked') == true ? $('#financing_value').val(1) : $('#financing_value').val(0);
        })
    </script>
@endsection
