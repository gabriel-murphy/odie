@extends('layouts.backend')

@section('content')
<!-- Main section-->
      <section class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
            <div class="content-header">
               <div class="content-title">Materials :: Materials & Cost Database<small>Last Updated %%last_update_date%%</small></div>
               <div class="ml-auto"><a class="btn btn-labeled btn-primary float-right" href="/project"><span class="btn-label"><i class="fa fa-plus-circle"></i></span>Add New Material</a></div>
            </div>
<div class="container-fluid">

    <div class="row justify-content-center">
        <div class="col-md-12">
          <table class="table table-striped my-4 w-100" id="datatable1">
             <thead class="thead-dark">
                <tr>
                   <th>Code</th>
                   <th>GU Code</th>
                   <th data-priority="1">Material Name</th>
                   <th>Material Category</th>
                   <th>Cost per Unit</th>
                   <th>Units</th>
                   <th>Update</th>
                </tr>
             </thead>
             <tbody>
              @foreach($materials as $material)
                <tr class="gradeX">
                   <td>{{ $material->code }}</td>
                   <td><a href="/customer/">{{ $material->giddy_up_code }}</a></td>
                   <td>{{ $material->name }}</td>
                   <td>{{ $material->roof_material_categories_id }}</td>                   
                   <td>{{ '$' . $material->our_cost }}</td>
                   <td>{{ $material->unit_id }}</td>
                   <td><a href="/material/{{ $material->id }}" class="btn btn-sm btn-primary">Edit</td>
                </tr>
              @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</section>
@endsection
