@extends('layouts.backend')

@section('title', 'Scheduler')

@section('content')
    <!-- Main section-->
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="content-title">All Opportunties
                    <small>View and Manage Opportunities to be Scheduled</small>
                </div>
                <div class="ml-auto"><a class="btn btn-labeled btn-primary float-right" href="/project"><span
                            class="btn-label"><i class="fa fa-plus-circle"></i></span>Create New Opportunity</a></div>
            </div>
            <div class="container-fluid">
                @if(Session::has('message'))
                    <div class="alert alert-success" id="flash-message"><h3> {!! session('message') !!}</h3></div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <table class="table table-striped my-4 w-100" id="datatable1">
                            <thead class="thead-dark">
                            <tr>
                                <th data-priority="1">Last Name</th>
                                <th>Project Type</th>
                                <th>City</th>
                                <th>Category</th>
                                <th>Created</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr class="gradeX">
                                    <td>{{ $project->last_name }}</td>
                                    <td>{{ $project->clas }}</td>
                                    <td>{{ $project->city }}</td>
                                    <td></td>
                                    <td>{{ date('D M j, Y g:i a', strtotime($project->created_at)) }}</td>
                                    <td><a href="/project/{{ $project->id }}" class="btn btn-sm btn-primary">view project</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
