@extends('layouts.backend')

@section('content')

    <!-- Main section-->
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="content-title">Estimator :: Create Estimates
                    <small>Step 3 - Generate & Send Estimate</small>
                </div>
            </div>
            <div class="container-fluid">
                @if(Session::has('message'))
                    <div class="alert alert-success" id="flash-message"><h3> {!! session('message') !!}</h3></div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <table class="table table-striped my-4 w-100" id="datatable1">
                            <thead class="thead-dark">
                            <tr>
                                <th data-priority="1">Prospect</th>
                                <th>City</th>
                                <th>Phase</th>
                                <th>City</th>
                                <th>Roof age</th>
                                <th>User</th>
                                <th>Created</th>
                                <th>Modified</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr class="gradeX">
                                    <td></td>
                                    <td>{{ $project->clas }}</td>
                                    <td>{{ $project->city }}</td>
                                    <td></td>
                                    <td>{{ $project->roof_age }}</td>
                                    <td></td>
                                    <td>{{ date('D M j, Y g:i a', strtotime($project->created_at)) }}</td>
                                    <td>{{ date('D M j, Y g:i a', strtotime($project->updated_at)) }}</td>
                                    <td><a href="/project/{{ $project->id }}" class="btn btn-sm btn-primary">view project</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
