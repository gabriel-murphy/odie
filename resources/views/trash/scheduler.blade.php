@extends('layouts.backend')

@section('title', 'Scheduler')

@section('styles')
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <style>
        #locationField, #controls {
            position: relative;
            width: 480px;
        }

        #autocomplete {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 99%;
        }

        .label {
            text-align: right;
            font-weight: bold;
            width: 100px;
            color: #303030;
            font-family: "Roboto";
        }

        #address {
            border: 1px solid #000090;
            background-color: #f0f9ff;
            width: 480px;
            padding-right: 2px;
        }

        #address td {
            font-size: 10pt;
        }

        .field {
            width: 99%;
        }

        .slimField {
            width: 80px;
        }

        .wideField {
            width: 200px;
        }

        #locationField {
            height: 20px;
            margin-bottom: 2px;
        }
    </style>
@endsection

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="content-title">Add New Customer / Project to Odie
                <small>Complete details below to schedule an estimator to evaluate a roofing project.</small>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <p>"Thank you for calling Company Name, this is ______ speaking, how may I help you?"</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">Create Customer</h2>
            </div>
        </div>
    </div>
    <!--
        <div class="panel-body">
          <input id="autocomplete" placeholder="Enter your address" type="text" class="form-control">
          <div id="address">
            <div class="row">
              <div class="col-md-6">
                <label class="control-label">Street address</label>
                <input class="form-control" id="street_number" disabled="true">
              </div>
              <div class="col-md-6">
                <label class="control-label">Route</label>
                <input class="form-control" id="route" disabled="true">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label class="control-label">City</label>
                <input class="form-control field" id="locality" disabled="true">
              </div>
              <div class="col-md-6">
                <label class="control-label">State</label>
                <input class="form-control" id="administrative_area_level_1" disabled="true">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label class="control-label">Zip code</label>
                <input class="form-control" id="postal_code" disabled="true">
              </div>
              <div class="col-md-6">
                <label class="control-label">Country</label>
                <input class="form-control" id="country" disabled="true">
              </div>
            </div>
          </div>
        </div>
    -->


    <div id="locationField">
        <input id="autocomplete"
               placeholder="Enter your address"
               onFocus="geolocate()"
               type="text"/>
    </div>

    <!-- Note: The address components in this sample are typical. You might need to adjust them for
               the locations relevant to your app. For more information, see
         https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
    -->

    <table id="address">
        <tr>
            <td class="label">Street address</td>
            <td class="slimField"><input class="field" id="street_number" disabled="true"/></td>
            <td class="wideField" colspan="2"><input class="field" id="route" disabled="true"/></td>
        </tr>
        <tr>
            <td class="label">City</td>
            <td class="wideField" colspan="3"><input class="field" id="locality" disabled="true"/></td>
        </tr>
        <tr>
            <td class="label">State</td>
            <td class="slimField"><input class="field" id="administrative_area_level_1" disabled="true"/></td>
            <td class="label">Zip code</td>
            <td class="wideField"><input class="field" id="postal_code" disabled="true"/></td>
        </tr>
        <tr>
            <td class="label">Country</td>
            <td class="wideField" colspan="3"><input class="field" id="country" disabled="true"/></td>
        </tr>
    </table>
@endsection

@section('scripts')
    <!-- Replace the value of the key parameter with your own API key. -->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkUOdZ5y7hMm0yrcCQoCvLwzdM6M8s5qk&libraries=places&callback=initAutocomplete"></script>
@endsection
