@extends('layouts.backend')

@section('styles')
    <link href="{{ asset('js/plugins/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet'>
    <link
        href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css'
        rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
@endsection

@section('content-heading', 'Step 2 - Schedule Appointment for %%CUSTOMER%%')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Opportunity</a>
@endsection
@section('breadcrumb-active', 'Schedule')

@section('content-header-action')
    <button id="appointment_showbtn" class="btn btn-sm btn-primary ">
        <span class="btn-label">
            <i class="fa fa-plus-circle"></i>
        </span>Add Appointment
    </button>
@endsection

@section('content')
    <div class="block">
        <div class="block-content">
            <div id="calendar"></div>
        </div>
    </div>

    <div class="modal fade come-from-modal right " id="add_appointment" role="dialog">
        <div class="modal-dialog" role="document" style="min-width:526px;">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h5 class="block-title">
                            Add Appointment
                        </h5>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <!-- project select -->
                        <p style="margin-bottom:3px;">Customer/Project<span style="color:red">*</span></p>
                        <select id="project" class="form-control @error('project') is-invalid @enderror" name="project"
                                required>
                            <option value="-1">Select Project</option>
                            @foreach ($projects as $project)
                                <option
                                    value="{{ $project->project_id }}" <?php if ($latestproject && $project->project_id == $latestproject->project_id) echo "selected" ?>>{{ $project->first_name." ".$project->last_name." - ".$project->city." - ".$project->name }}</option>
                            @endforeach
                        </select>

                        <p style="margin-top:10px;margin-bottom:3px;">Appointment Type<span style="color:red">*</span>
                        </p>
                        <select id="appointment_type"
                                class="form-control @error('appointment_type') is-invalid @enderror"
                                name="appointment_type" required>
                            @foreach ($appointments_types as $type)
                                <option value="{{ $type->id }}">{{ $type->name}}</option>
                            @endforeach
                        </select>
                        <!-- title input -->
                        <p style="margin-top:10px; margin-bottom:3px;">Title<span style="color:red">*</span></p>
                        <input id="title" type="text"
                               value="{{$latestproject ? $latestproject->last_name.' - '.$latestproject->city : ''}}"
                               class="form-control @error('title') is-invalid @enderror" name="title" required
                               autofocus>
                        <p style="margin-top:10px; margin-bottom:3px;">Appointment For<span style="color:red">*</span>
                        </p>
                        <select id="estimator_modal" class="form-control @error('estimator_modal') is-invalid @enderror"
                                name="estimator_modal" required>
                            @foreach ($estimators as $estimator)
                                <option
                                    value="{{ $estimator->id }}" {{ $latestproject && $estimator->id == $latestproject->user_id ? 'selected' : '' }}>{{ $estimator->first_name }} {{ $estimator->last_name }}</option>
                            @endforeach
                        </select>

                        <p style="margin-top:10px; margin-bottom:3px;">Select Attendees</p>
                        <select class="chzn-select form-control" id="attendees-multi" multiple name="faculty">
                            @foreach ($estimators as $estimator)
                                <option id="chzn_select_{{$estimator->id}}"
                                        value="{{ $estimator->id }}">{{ $estimator->first_name }} {{ $estimator->last_name }}</option>
                            @endforeach
                        </select>
                        <p style="margin-top:10px; margin-bottom:3px;">Date/Time</p>
                        <div style="display: flex">
                            <input type='text' class="form-control" id='from-datepicker'
                                   style='width: 110px;margin-right:10px'>
                            <input type='text' class="form-control" id='from-timepicker' style='width: 110px;'>
                            <span style="margin-left:10px; margin-right:10px;margin-top:6px;">to</span>
                            <input type='text' class="form-control" id='to-datepicker'
                                   style='width: 110px;margin-right:10px'>
                            <input type='text' class="form-control" id='to-timepicker' style='width: 110px;'>
                        </div>
                        <div class="custom-control custom-checkbox" style="display: flex;margin-top:10px">
                            <input type="checkbox" class="custom-control-input" id="allday">
                            <label class="custom-control-label" for="allday" style="margin-top:10px">All day</label>
                            <select class="form-control" name="allday_dropdown" id="allday_dropdown" value="0" required
                                    style="width:200px;margin-left:10px" disabled>
                                <option value="0">Does not repeat</option>
                                <option value="1">10 Minute</option>
                                <option value="1">30 Minute</option>
                                <option value="1">1 Hour</option>
                                <option value="1">2 Hour</option>
                                <option value="1">8 Hour</option>
                            </select>
                        </div>
                        <p style="margin-top:10px; margin-bottom:3px;">Location<span style="color:red">*</span></p>
                        <select id="location" class="form-control" name="location" required disabled>
                            @foreach ($projects as $project)
                                <option
                                    value="{{ $project->project_address_id }}" <?php if ($latestproject && $project->project_id == $latestproject->project_id) echo "selected" ?>>{{ $latestproject->city }}</option>
                            @endforeach
                        </select>
                        <p style="margin-top:10px; margin-bottom:3px;">Note<span style="color:red">*</span></p>
                        <textarea id="note" rows="5" class="form-control @error('note') is-invalid @enderror"
                                  name="note"
                                  value="{{ old('note') }}"></textarea>
                        {{-- <p style="margin-top:10px; margin-bottom:3px;">Additional Recipients<span style="color:red">*</span></p>
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" required autofocus>
                        <p style="margin-top:10px; margin-bottom:3px;">User Notification(s)</p>
                        <div style="display: flex">
                           <select class="form-control" name="notification_type" id="notification_type" value="0" required style="width:100px;">
                              <option value="0">Email</option>
                              <option value="1">SMS</option>
                           </select>
                           <select class="form-control" name="notification_timeVal" id="notification_timeVal" value="0" required style="width:100px;margin-left:10px">
                              <option value="0">10</option>
                              <option value="1">20</option>
                              <option value="1">30</option>
                              <option value="1">40</option>
                              <option value="1">50</option>
                              <option value="1">60</option>
                           </select>
                           <select class="form-control" name="notification_time" id="notification_time" value="0" required style="width:100px;margin-left:10px">
                              <option value="0">Minute</option>
                              <option value="1">Hour</option>
                           </select>
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="add_appointmentOk" style="width:80px">SAVE</button>
                        <button class="btn btn-dark" data-dismiss="modal" style="width:80px">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade come-from-modal right" id="edit_appointment" role="dialog">
        <div class="modal-dialog" role="document" style="min-width:526px;">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h5 class="block-title">
                            Add Appointment
                        </h5>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <!-- project select  disabled-->
                        <p style="margin-bottom:3px;">Customer/Project<span style="color:red">*</span></p>
                        <select id="edit_project" class="form-control @error('project') is-invalid @enderror"
                                name="edit_project"
                                required disabled>
                            <option id="edit_project_content"></option>
                        </select>

                        <p style="margin-top:10px;margin-bottom:3px;">Appointment Type<span style="color:red">*</span>
                        </p>
                        <select id="edit_appointment_type"
                                class="form-control @error('edit_appointment_type') is-invalid @enderror"
                                name="edit_appointment_type" required>
                            @foreach ($appointments_types as $type)
                                <option value="{{ $type->id }}">{{ $type->name}}</option>
                            @endforeach
                        </select>

                        <!-- title input -->
                        <p style="margin-top:10px; margin-bottom:3px;">Title<span style="color:red">*</span></p>
                        <input id="edit_title" type="text" class="form-control @error('edit_title') is-invalid @enderror"
                            name="edit_title" required autofocus>

                        <p style="margin-top:10px; margin-bottom:3px;">Appointment For<span style="color:red">*</span></p>
                        <select id="edit_estimator_modal" class="form-control @error('edit_estimator_modal') is-invalid @enderror"
                                name="edit_estimator_modal" required>
                            @foreach ($estimators as $estimator)
                                <option
                                    value="{{ $estimator->id }}">{{ $estimator->first_name }} {{ $estimator->last_name }}</option>
                            @endforeach
                        </select>

                        <p style="margin-top:10px; margin-bottom:3px;">Select Attendees</p>
                        <select class="chzn-select form-control" id="edit-attendees-multi" data-placeholder="Edit Attendees" multiple name="edit-attendees-multi">
                            @foreach ($estimators as $estimator)
                                <option id="edit_chzn_select_{{$estimator->id}}"
                                        value="{{ $estimator->id }}">{{ $estimator->first_name }} {{ $estimator->last_name }}</option>
                            @endforeach
                        </select>
                        <p style="margin-top:10px; margin-bottom:3px;">Date/Time</p>
                        <div style="display: flex">
                            <input type='text' class="form-control" id='edit-from-datepicker'
                                style='width: 110px;margin-right:10px'>
                            <input type='text' class="form-control" id='edit-from-timepicker' style='width: 110px;'>
                            <span style="margin-left:10px; margin-right:10px;margin-top:6px;">to</span>
                            <input type='text' class="form-control" id='edit-to-datepicker'
                                style='width: 110px;margin-right:10px'>
                            <input type='text' class="form-control" id='edit-to-timepicker' style='width: 110px;'>
                        </div>
                        <div class="custom-control custom-checkbox" style="display: flex;margin-top:10px">
                            <input type="checkbox" class="custom-control-input" id="edit-allday">
                            <label class="custom-control-label" for="edit-allday" style="margin-top:10px">All day</label>
                            <select class="form-control" name="edit_allday_dropdown" id="edit_allday_dropdown" value="0"
                                    required style="width:200px;margin-left:10px" disabled>
                                <option value="0">Does not repeat</option>
                                <option value="1">10 Minute</option>
                                <option value="2">30 Minute</option>
                                <option value="3">1 Hour</option>
                                <option value="4">2 Hour</option>
                                <option value="5">8 Hour</option>
                            </select>
                        </div>
                        <p style="margin-top:10px; margin-bottom:3px;">Location<span style="color:red">*</span></p>
                        <select id="edit_location" class="form-control" name="edit_location" required disabled>
                            <option id="edit_location_id"></option>
                        </select>
                        <p style="margin-top:10px; margin-bottom:3px;">Note<span style="color:red">*</span></p>
                        <textarea id="edit_note" rows="5" class="form-control @error('edit_note') is-invalid @enderror"
                                name="edit_note"></textarea>
                        {{-- <p style="margin-top:10px; margin-bottom:3px;">Additional Recipients<span style="color:red">*</span></p>
                        <input id="edit_email" type="text" class="form-control @error('edit_email') is-invalid @enderror" name="edit_email" required autofocus>
                        <p style="margin-top:10px; margin-bottom:3px;">User Notification(s)</p>
                        <div style="display: flex">
                        <select class="form-control" name="edit_notification_type" id="edit_notification_type" required style="width:100px;">
                            <option value="0">Email</option>
                            <option value="1">SMS</option>
                        </select>
                        <select class="form-control" name="edit_notification_timeVal" id="edit_notification_timeVal" style="width:100px;margin-left:10px">
                            <option value="0">10</option>
                            <option value="1">20</option>
                            <option value="1">30</option>
                            <option value="1">40</option>
                            <option value="1">50</option>
                            <option value="1">60</option>
                        </select>
                        <select class="form-control" name="edit_notification_time" id="edit_notification_time" style="width:100px;margin-left:10px">
                            <option value="0">Minute</option>
                            <option value="1">Hour</option>
                        </select>
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" onclick="edit_appointmentOk()" style="width:80px">SAVE</button>
                        <button class="btn btn-danger" onclick="delete_appointmentOk()" style="width:80px">DELETE</button>
                        <button class="btn btn-dark" data-dismiss="modal" style="width:80px">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/plugins/moment/moment.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
    <script
        src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js'
        type='text/javascript'></script>
    <script src="{{ asset('js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

    <script type="text/javascript">
        let projects = <?php echo $projects ?>;
        let estimators = <?php echo $estimators ?>;
        let selected_attendees = [];
        let edit_selected_attendees = [];
        let appointments = <?php echo $appointments ?>;
        let appointments_without_project = <?php echo $appointments_without_project ?>;
        let editing_appointmentid;
        let firstloadstate = true;
        let $calendar = $('#calendar');
        function showmodal() {
            $('#add_appointment').modal('show');
            /* $('.modal-backdrop.show').css('opacity', '0');
             $('.modal-open').css('padding-right', '0');*/
        }

        function twoDigits(d) {
            if (0 <= d && d < 10) return "0" + d.toString();
            if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
            return d.toString();
        }

        $(function () {
            /*$('.js-select2').select2();*/
            function initAppointsOnCalendar() {
                //calendar part

                let demoEvents = createDemoEvents();

                // initExternalEvents(calendar);

                initCalendar($calendar, demoEvents);

            }

            $('#appointment_showbtn').on('click', function () {
                showmodal();
            });

            initAppointsOnCalendar();

            $("#attendees-multi").chosen({
                placeholder_text: "Add Attendees",
                width: "100%"
            }).change(function (event) {
                if (event.target == this) {
                    selected_attendees = $(this).val();
                }
            });

            $("#edit-attendees-multi").chosen({
            }).change(function (event) {
                if (event.target == this) {
                    edit_selected_attendees = $(this).val();
                }
            });

            let appointment_minutes = 0;
            for (let k in projects) {
                if (projects[k].project_id == $('#project').val())
                    appointment_minutes = projects[k].appointment_minutes;
            }
            let nowtime = new Date();
            let starttime = new Date(nowtime.getFullYear() + "-" + (nowtime.getMonth() + 1) + "-" + (nowtime.getDate() + 1) + " 09:00 AM");
            let endtime = new Date(nowtime.getFullYear() + "-" + (nowtime.getMonth() + 1) + "-" + (nowtime.getDate() + 1) + " 11:00 AM");
            endtime.setMinutes(starttime.getMinutes() + appointment_minutes);
            $('#from-datepicker').datetimepicker({
                format: "YYYY-MM-DD",
                defaultDate: starttime,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-clock-o",
                    clear: "fa fa-trash-o"
                }
            });

            $('#from-timepicker').datetimepicker({
                format: "hh:mm A",
                defaultDate: starttime,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-clock-o",
                    clear: "fa fa-trash-o"
                }
            });
            $('#to-datepicker').datetimepicker({
                format: "YYYY-MM-DD",
                defaultDate: endtime,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-clock-o",
                    clear: "fa fa-trash-o"
                }
            });

            $('#to-timepicker').datetimepicker({
                format: "hh:mm A",
                defaultDate: endtime,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-clock-o",
                    clear: "fa fa-trash-o"
                }
            });
            let allday_checkstate = true;

            $('#edit-allday').change(function () {
                allday_checkstate = !allday_checkstate;
                $('#edit_allday_dropdown').prop("disabled", allday_checkstate);
            })

            let allday_edit_checkstate = true;

            $('#allday').change(function () {
                allday_edit_checkstate = !allday_edit_checkstate;
                $('#allday_dropdown').prop("disabled", allday_edit_checkstate);
            })

            $('#project').change(function () {
                console.log($('#project').val());
                for (let i in projects) {
                    if (projects[i].project_id == $('#project').val()) {
                        $('#title').val(projects[i].last_name + "-" + projects[i].city);
                        console.log(projects[i]);
                        $('#estimator_modal').val(projects[i].user_id);
                        $('#location').val(projects[i].project_address_id);
                    }
                }
            })


            $('#add_appointmentOk').on('click', function () {
                //validation
                if ($('#project').val() == null || $('#project').val() == "" || $('#project').val().trim() == "") {
                    $('#project').addClass('is-invalid');
                    return;
                } else if ($('#project').hasClass('is-invalid')) {
                    $('#project').removeClass('is-invalid')
                }
                if ($('#title').val() == null || $('#title').val() == "" || $('#title').val().trim() == "") {
                    $('#title').addClass('is-invalid');
                    return;
                } else if ($('#title').hasClass('is-invalid')) {
                    $('#title').removeClass('is-invalid')
                }

                if ($('#estimator_modal').val() == null || $('#estimator_modal').val() == "" || $('#estimator_modal').val().trim() == "") {
                    $('#estimator_modal').addClass('is-invalid');
                    return;
                } else if ($('#estimator_modal').hasClass('is-invalid')) {
                    $('#estimator_modal').removeClass('is-invalid')
                }

                if ($('#note').val() == null || $('#note').val() == "" || $('#note').val().trim() == "") {
                    $('#note').addClass('is-invalid');
                    return;
                } else if ($('#note').hasClass('is-invalid')) {
                    $('#note').removeClass('is-invalid')
                }

                //send data to backend
                let fromdate = $('#from-datepicker').val() + " " + $('#from-timepicker').val();
                let todate = $('#to-datepicker').val() + " " + $('#to-timepicker').val();

                let start_date = new Date(fromdate);
                let end_date = new Date(todate);
                if (start_date.getTime() == end_date.getTime()) {
                    // end_date = end_date.getHours() + 2;
                    end_date.setHours(end_date.getHours() + 2);
                }
                start_date = start_date.getFullYear() + "-" + twoDigits(1 + start_date.getMonth()) + "-" + twoDigits(start_date.getDate()) + " " + twoDigits(start_date.getHours()) + ":" + twoDigits(start_date.getMinutes()) + ":" + twoDigits(start_date.getSeconds());
                end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());

                //pop estimator_user_id from secondary_user_id
                for (let j in selected_attendees) {
                    if (selected_attendees[j] == $('#estimator_modal').val()) {
                        selected_attendees.splice(j, 1);
                    }
                }

                $.ajax({
                    type: 'POST',
                    url: "/addappointment",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        project_id: $('#project').val(),
                        title: $('#title').val(),
                        appointment_type : $('#appointment_type').val(),
                        estimator_user_id: $('#estimator_modal').val(),
                        secondary_user_id: selected_attendees,
                        notes: $('#note').val(),
                        start_timestamp: start_date,
                        end_timestamp: end_date
                    },
                    success: function (res) {
                        if (res && res == 1) {
                            window.location.replace('/calendar');
                        }
                    }
                });

            });

            function initCalendar(calElement, events) {
                // check to remove elements from the list
                let removeAfterDrop = $('#remove-after-drop');

                calElement.fullCalendar({
                    header: {
                        left: 'prev,next',
                        center: 'title',
                        right: 'agendaWeek,agendaDay'
                    },
                    defaultView: 'agendaDays',
                    eventOrder: "title, start,-duration,allDay",
                    views: {
                        agendaDays: {
                            type: 'agenda',
                            slotLabelFormat: 'HH:mm',
                            groupByResource: true,
                            duration: {
                                days: 14
                            },
                            columnHeaderFormat: 'M/D \n ddd',
                            visibleRange: {
                                start: moment($("#start_Date input").val(), 'MM/DD/YYYY HH:mm').format('YYYY-MM-DD'),
                                end: moment($("#start_Date input").val(), 'MM/DD/YYYY HH:mm').add(1, 'days').format('YYYY-MM-DD')
                            },
                            weekends: false,
                        },
                        agendaDaysNoWeekends: {
                            type: 'agenda',
                            slotLabelFormat: 'HH:mm',
                            groupByResource: true,
                            duration: {
                                days: 14
                            },
                            columnHeaderFormat: 'M/D \n ddd',
                            visibleRange: {
                                start: moment($("#start_Date input").val(), 'MM/DD/YYYY HH:mm').format('YYYY-MM-DD'),
                                end: moment($("#start_Date input").val(), 'MM/DD/YYYY HH:mm').add(1, 'days').format('YYYY-MM-DD')
                            },
                            weekends: true,
                        },
                    },
                    buttonIcons: {
                        // note the space at the beginning
                        prev: '   fc-icon fc-icon-left-single-arrow',
                        next: '   fc-icon fc-icon-right-single-arrow'
                    },
                    buttonText: {
                        week: 'week',
                        day: 'day'
                    },
                    height: "auto",
                    editable: true,
                    droppable: true, // this allows things to be dropped onto the calendar
                    drop: function (date, allDay) {
                        // this function is called when something is dropped

                        let $this = $(this),
                            // retrieve the dropped element's stored Event Object
                            originalEventObject = $this.data('calendarEventObject');

                        // if something went wrong, abort
                        if (!originalEventObject) return;

                        // clone the object to avoid multiple events with reference to the same object
                        let clonedEventObject = $.extend({}, originalEventObject);

                        // assign the reported date
                        clonedEventObject.start = date;

                        clonedEventObject.allDay = allDay;
                        clonedEventObject.backgroundColor = $this.css('background-color');
                        clonedEventObject.borderColor = $this.css('border-color');

                        // render the event on the calendar
                        // the last `true` argument determines if the event "sticks"
                        // (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                        calElement.fullCalendar('renderEvent', clonedEventObject, true);

                        // if necessary remove the element from the list
                        if (removeAfterDrop.is(':checked')) {
                            $this.remove();
                        }
                    },
                    eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                        if (event.allDay) {
                            //handle all day
                            console.log("allday");
                        } else {
                            let delta_day = delta._days;
                            let delta_mins = delta._milliseconds / 1000 / 60;
                            let appointment_id = event.appointment_id;
                            for (let i in appointments) {
                                if (appointments[i].id == appointment_id) {
                                    let diff = (new Date(appointments[i].start_time)).getTime() - (new Date(appointments[i].end_time)).getTime();
                                    let start_date = appointments[i].start_time;
                                    start_date = new Date(start_date);
                                    start_date = moment(start_date).subtract(-delta_day, 'days').toDate();
                                    start_date = moment(start_date).subtract(-delta_mins, 'minutes').toDate();
                                    let end_date = new Date(start_date.getTime() - diff);
                                    end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());
                                    start_date = start_date.getFullYear() + "-" + twoDigits(1 + start_date.getMonth()) + "-" + twoDigits(start_date.getDate()) + " " + twoDigits(start_date.getHours()) + ":" + twoDigits(start_date.getMinutes()) + ":" + twoDigits(start_date.getSeconds());
                                    ChangedTimetoBackend(appointments[i].id, start_date, end_date);

                                    appointments[i].start_time = start_date;
                                    appointments[i].end_time = end_date;
                                }
                            }
                            for (let i in appointments_without_project) {
                                if (appointments_without_project[i].id == appointment_id) {
                                    let diff = (new Date(appointments_without_project[i].start_time)).getTime() - (new Date(appointments_without_project[i].end_time)).getTime();
                                    let start_date = appointments_without_project[i].start_time;
                                    start_date = new Date(start_date);
                                    start_date = moment(start_date).subtract(-delta_day, 'days').toDate();
                                    start_date = moment(start_date).subtract(-delta_mins, 'minutes').toDate();
                                    let end_date = new Date(start_date.getTime() - diff);
                                    end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());
                                    start_date = start_date.getFullYear() + "-" + twoDigits(1 + start_date.getMonth()) + "-" + twoDigits(start_date.getDate()) + " " + twoDigits(start_date.getHours()) + ":" + twoDigits(start_date.getMinutes()) + ":" + twoDigits(start_date.getSeconds());
                                    ChangedTimetoBackend(appointments_without_project[i].id, start_date, end_date);

                                    appointments_without_project[i].start_time = start_date;
                                    appointments_without_project[i].end_time = end_date;
                                }
                            }
                        }

                    },
                    eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                        // console.log(event);
                        let delta_day = delta._days;
                        let deleta_mins = delta._milliseconds / 1000 / 60;
                        let appointment_id = event.appointment_id;
                        for (let i in appointments) {
                            if (appointments[i].id == appointment_id) {
                                let end_date = appointments[i].end_time;
                                end_date = new Date(end_date);
                                end_date = moment(end_date).subtract(-delta_day, 'days').toDate();
                                end_date = moment(end_date).subtract(-deleta_mins, 'minutes').toDate();
                                //ajax call
                                end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());
                                ChangedTimetoBackend(appointments[i].id, appointments[i].start_time, end_date);
                                //show to ui
                                appointments[i].end_time = end_date;
                            }
                        }
                        for (let i in appointments_without_project) {
                            if (appointments_without_project[i].id == appointment_id) {
                                let end_date = appointments_without_project[i].end_time;
                                end_date = new Date(end_date);
                                end_date = moment(end_date).subtract(-delta_day, 'days').toDate();
                                end_date = moment(end_date).subtract(-deleta_mins, 'minutes').toDate();
                                //ajax call
                                end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());
                                ChangedTimetoBackend(appointments_without_project[i].id, appointments_without_project[i].start_time, end_date);
                                //show to ui
                                appointments_without_project[i].end_time = end_date;
                            }
                        }
                    },
                    // This array is the events sources
                    events: events,
                    timeFormat: 'H(:mm)',
                    minTime: '06:00',
                    maxTime: '19:00',
                    eventClick: (info) => {

                        for (let i in appointments) {
                            if (appointments[i].id == info.appointment_id) {
                                editing_appointmentid = appointments[i].id;
                                $('#edit_project_content').text(info.title);
                                $('#edit_title').val(appointments[i].title);
                                $('#edit_estimator_modal').val(appointments[i].estimator_user_id);
                                $('#edit_appointment_type').val(appointments[i].appointment_type_id)
                                //datetime set
                                $('#edit-from-datepicker').datetimepicker({
                                    format: "YYYY-MM-DD",
                                    defaultDate: new Date(appointments[i].start_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                $('#edit-from-timepicker').datetimepicker({
                                    format: "hh:mm A",
                                    defaultDate: new Date(appointments[i].start_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                $('#edit-to-datepicker').datetimepicker({
                                    format: "YYYY-MM-DD",
                                    defaultDate: new Date(appointments[i].end_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                $('#edit-to-timepicker').datetimepicker({
                                    format: "hh:mm A",
                                    defaultDate: new Date(appointments[i].end_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                let tmp = new Date(appointments[i].start_time);

                                let start_datetime_date = tmp.getFullYear() + "-" + twoDigits(1 + tmp.getMonth()) + "-" + twoDigits(tmp.getDate());
                                $('#edit-from-datepicker').val(start_datetime_date);

                                let hh = tmp.getHours();
                                let dd = "AM";
                                let h = hh;
                                if (h >= 12) {
                                    h = hh - 12;
                                    dd = "PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }

                                let start_datetime_time = h + ":" + twoDigits(tmp.getMinutes()) + " " + dd;
                                $('#edit-from-timepicker').val(start_datetime_time);

                                tmp = new Date(appointments[i].end_time);

                                let end_datetime_date = tmp.getFullYear() + "-" + twoDigits(1 + tmp.getMonth()) + "-" + twoDigits(tmp.getDate());
                                $('#edit-to-datepicker').val(end_datetime_date);

                                hh = tmp.getHours();
                                dd = "AM";
                                h = hh;
                                if (h >= 12) {
                                    h = hh - 12;
                                    dd = "PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }

                                let end_datetime_time = h + ":" + twoDigits(tmp.getMinutes()) + " " + dd;
                                $('#edit-to-timepicker').val(end_datetime_time);

                                $('#edit-attendees-multi').val([]);
                                let selected_arr = [];

                                if (appointments[i].secondary_user_id && appointments[i].secondary_user_id != null) {
                                    selected_arr.push(appointments[i].secondary_user_id);
                                }
                                if (appointments[i].tertiary_user_id && appointments[i].tertiary_user_id != null) {
                                    selected_arr.push(appointments[i].tertiary_user_id);
                                }

                                $('#edit-attendees-multi').val(selected_arr);
                                $("#edit-attendees-multi").chosen("destroy").chosen();
                                $('#edit_location_id').text(appointments[i].city);
                                $('#edit_note').val(appointments[i].notes);
                                // $('#edit_email').val(appointments[i].)

                                showeditmodal();

                            }
                        }
                        
                        for (let i in appointments_without_project) {
                            if (appointments_without_project[i].id == info.appointment_id) {
                                editing_appointmentid = appointments_without_project[i].id;
                                $('#edit_project_content').text("");
                                $('#edit_title').val(appointments_without_project[i].title);
                                $('#edit_estimator_modal').val(appointments_without_project[i].estimator_user_id);
                                $('#edit_appointment_type').val(appointments_without_project[i].appointment_type_id)
                                //datetime set
                                $('#edit-from-datepicker').datetimepicker({
                                    format: "YYYY-MM-DD",
                                    defaultDate: new Date(appointments_without_project[i].start_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                $('#edit-from-timepicker').datetimepicker({
                                    format: "hh:mm A",
                                    defaultDate: new Date(appointments_without_project[i].start_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                $('#edit-to-datepicker').datetimepicker({
                                    format: "YYYY-MM-DD",
                                    defaultDate: new Date(appointments_without_project[i].end_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                $('#edit-to-timepicker').datetimepicker({
                                    format: "hh:mm A",
                                    defaultDate: new Date(appointments_without_project[i].end_time),
                                    icons: {
                                        time: "fa fa-clock-o",
                                        date: "fa fa-calendar",
                                        up: "fa fa-arrow-up",
                                        down: "fa fa-arrow-down",
                                        previous: "fa fa-chevron-left",
                                        next: "fa fa-chevron-right",
                                        today: "fa fa-clock-o",
                                        clear: "fa fa-trash-o"
                                    }
                                });
                                let tmp = new Date(appointments_without_project[i].start_time);

                                let start_datetime_date = tmp.getFullYear() + "-" + twoDigits(1 + tmp.getMonth()) + "-" + twoDigits(tmp.getDate());
                                $('#edit-from-datepicker').val(start_datetime_date);

                                let hh = tmp.getHours();
                                let dd = "AM";
                                let h = hh;
                                if (h >= 12) {
                                    h = hh - 12;
                                    dd = "PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }

                                let start_datetime_time = h + ":" + twoDigits(tmp.getMinutes()) + " " + dd;
                                $('#edit-from-timepicker').val(start_datetime_time);

                                tmp = new Date(appointments_without_project[i].end_time);

                                let end_datetime_date = tmp.getFullYear() + "-" + twoDigits(1 + tmp.getMonth()) + "-" + twoDigits(tmp.getDate());
                                $('#edit-to-datepicker').val(end_datetime_date);

                                hh = tmp.getHours();
                                dd = "AM";
                                h = hh;
                                if (h >= 12) {
                                    h = hh - 12;
                                    dd = "PM";
                                }
                                if (h == 0) {
                                    h = 12;
                                }

                                let end_datetime_time = h + ":" + twoDigits(tmp.getMinutes()) + " " + dd;
                                $('#edit-to-timepicker').val(end_datetime_time);

                                $('#edit-attendees-multi').val([]);
                                let selected_arr = [];

                                if (appointments_without_project[i].secondary_user_id && appointments_without_project[i].secondary_user_id != null) {
                                    selected_arr.push(appointments_without_project[i].secondary_user_id);
                                }
                                if (appointments_without_project[i].tertiary_user_id && appointments_without_project[i].tertiary_user_id != null) {
                                    selected_arr.push(appointments_without_project[i].tertiary_user_id);
                                }

                                $('#edit-attendees-multi').val(selected_arr);
                                $("#edit-attendees-multi").chosen("destroy").chosen();
                                $('#edit_location_id').text(appointments_without_project[i].city);
                                $('#edit_note').val(appointments_without_project[i].notes);
                                // $('#edit_email').val(appointments[i].)

                                showeditmodal();

                            }
                        }

                    },
                    viewRender: function (view, element) {
                        if (firstloadstate) {
                            $calendar.find('.fc-toolbar > .fc-right').html(
                                '<p class="pt-2">Weekends Show:</p>' +
                                '<label class="css-control css-control-primary css-switch mr-2">' +
                                '<input class="css-control-input" type="checkbox" id="weekends_show" name="weekends_show">' +
                                '<span class="css-control-indicator"></span>' +
                                '</label>' +
                                $calendar.find('.fc-toolbar > .fc-right').html()
                            );

                            $('#weekends_show').change(function () {
                                if (this.checked) {

                                    $calendar.fullCalendar('changeView', 'agendaDaysNoWeekends');
                                } else {
                                    //agendaDaysNoWeekends
                                    $calendar.fullCalendar('changeView', 'agendaDays');
                                }
                            })
                        }
                        firstloadstate = false;
                    }
                });
            }

            function createDemoEvents() {
                let events = [];
                for (let i in appointments) {
                    let estimatorid;
                    for (let j in estimators) {
                        if (estimators[j].id === appointments[i].estimator_user_id)
                            estimatorid = j;
                    }
                    let event = {
                        title: appointments[i].last_name + "\n" + appointments[i].city,
                        start: new Date(appointments[i].start_time),
                        end: new Date(appointments[i].end_time),
                        backgroundColor: "#" + estimators[estimatorid].hex_color,
                        borderColor: "#" + estimators[estimatorid].hex_color,
                        appointment_id: appointments[i].id
                    }
                    events.push(event);
                }
                
                for (let i in appointments_without_project) {
                    let estimatorid;
                    for (let j in estimators) {
                        if (estimators[j].id == appointments_without_project[i].estimator_user_id)
                            estimatorid = j;
                    }
                    let event = {
                        title: appointments_without_project[i].title,
                        start: new Date(appointments_without_project[i].start_time),
                        end: new Date(appointments_without_project[i].end_time),
                        backgroundColor: "#" + estimators[estimatorid].hex_color,
                        borderColor: "#" + estimators[estimatorid].hex_color,
                        appointment_id: appointments_without_project[i].id
                    }
                    events.push(event);
                }
                return events;
            }

        });

        $(window).on('load', function () {
            var modal_show = <?php echo $target_project_enabled; ?>;
            modal_show = modal_show == 1 ? true:false;
            if (projects.length > 0 && modal_show) {

                showmodal();
            }
        });

        function showeditmodal() {
            $('#edit_appointment').modal('show');
            // $('.modal-backdrop.show').css('opacity', '0');
            // $('.modal-open').css('padding-right', '0');
            $('#edit_attendees_multi_chosen').css('width', '100%');
        }

        function hideeditmodal() {
            $('#edit_appointment').modal('hide');
        }


        function ChangedTimetoBackend(appointment_id, start_date, end_date) {

            $.ajax({
                type: 'POST',
                url: "/editappointment/date",
                data: {
                    "_token": "{{ csrf_token() }}",
                    appointment_id: appointment_id,
                    start_date: start_date,
                    end_date: end_date
                },
                success: function (res) {
                    if (res && res != 'success') {
                        alert('Error occurs!');
                    }
                }
            });
        }

        function edit_appointmentOk() {

            if ($('#edit_title').val() == null || $('#edit_title').val() == "" || $('#edit_title').val().trim() == "") {
                $('#edit_title').addClass('is-invalid');
                return;
            } else if ($('#edit_title').hasClass('is-invalid')) {
                $('#edit_title').removeClass('is-invalid')
            }

            if ($('#edit_estimator_modal').val() == null || $('#edit_estimator_modal').val() == "" || $('#edit_estimator_modal').val().trim() == "") {
                $('#edit_estimator_modal').addClass('is-invalid');
                return;
            } else if ($('#edit_estimator_modal').hasClass('is-invalid')) {
                $('#edit_estimator_modal').removeClass('is-invalid')
            }


            if ($('#edit_note').val() == null || $('#edit_note').val() == "" || $('#edit_note').val().trim() == "") {
                $('#edit_note').addClass('is-invalid');
                return;
            } else if ($('#edit_note').hasClass('is-invalid')) {
                $('#edit_note').removeClass('is-invalid')
            }

            // if ($('#edit_email').val() == null || $('#edit_email').val() == "" || $('#edit_email').val().trim() == ""){
            //    $('#edit_email').addClass('is-invalid');
            //    return;
            // }

            let fromdate = $('#edit-from-datepicker').val() + " " + $('#edit-from-timepicker').val();
            let todate = $('#edit-to-datepicker').val() + " " + $('#edit-to-timepicker').val();

            let start_date = new Date(fromdate);
            let end_date = new Date(todate);
            if (start_date.getTime() == end_date.getTime()) {
                // end_date = end_date.getHours() + 2;
                end_date.setHours(end_date.getHours() + 2);
            }
            start_date = start_date.getFullYear() + "-" + twoDigits(1 + start_date.getMonth()) + "-" + twoDigits(start_date.getDate()) + " " + twoDigits(start_date.getHours()) + ":" + twoDigits(start_date.getMinutes()) + ":" + twoDigits(start_date.getSeconds());
            end_date = end_date.getFullYear() + "-" + twoDigits(1 + end_date.getMonth()) + "-" + twoDigits(end_date.getDate()) + " " + twoDigits(end_date.getHours()) + ":" + twoDigits(end_date.getMinutes()) + ":" + twoDigits(end_date.getSeconds());

            //pop estimator_user_id from secondary_user_id
            for (let j in edit_selected_attendees) {
                if (edit_selected_attendees[j] == $('#edit_estimator_modal').val()) {
                    edit_selected_attendees.splice(j, 1);
                }
            }

            $.ajax({
                type: 'POST',
                url: "/editappointment/content",
                data: {
                    "_token": "{{ csrf_token() }}",
                    appointment_id: editing_appointmentid,
                    title: $('#edit_title').val(),
                    appointment_type : $('#edit_appointment_type').val(),
                    estimator_user_id: $('#edit_estimator_modal').val(),
                    secondary_user_id: edit_selected_attendees,
                    notes: $('#edit_note').val(),
                    start_timestamp: start_date,
                    end_timestamp: end_date
                },
                success: function (res) {
                    if (res && res != 'success') {
                        alert('error occurs!!');
                    } else {
                        hideeditmodal();
                        window.location.reload('/calendar');
                    }

                }
            });
        }

        function delete_appointmentOk() {
            $.ajax({
                type: 'DELETE',
                url: "/deleteappointment",
                data: {
                    "_token": "{{ csrf_token() }}",
                    appointment_id: editing_appointmentid
                },
                success: function (res) {
                    if (res && res != 'success') {
                        alert('error occurs!!');
                    } else {
                        hideeditmodal();
                        window.location.reload('/calendar');
                    }

                }
            });
        }
    </script>
@endsection
