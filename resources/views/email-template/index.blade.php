@extends('layouts.backend')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Email Templates List</h3>
            <div class="block-options">
                @include('includes.common.search', ['route' => route(Route::currentRouteName())])
            </div>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr class="text-uppercase">
                        <th style="width: 10px">#</th>
                        <th>Title</th>
                        <th>Subject</th>
                        <th>Stage</th>
                        <th>Status</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($templates as $template)
                        <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="font-w600">{{ $template->name }}</td>
                        <td>{{ $template->subject }}</td>
                        <td>Prospect</td>
                        <td>{{ $template->is_active['text'] }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ route('templates.emails.show', $template->id ) }}" class="btn btn-sm btn-secondary"
                                   data-toggle="tooltip" title="View">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a href="{{ route('templates.emails.edit', $template->id ) }}"
                                   class="btn btn-sm btn-secondary"
                                   data-toggle="tooltip" title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <a href="{{ route('templates.emails.change_status', $template->id) }}" class="btn btn-sm btn-secondary"
                                   data-toggle="tooltip" title="Active / Inactive">
                                    <i class="{{ $template->is_active['icon'] }}"></i>
                                </a>

                                <button data-toggle="modal" data-target="#confirm_template_{{ $template->id }}"
                                        class="btn btn-sm btn-outline-danger" title="">
                                    <i class="fa fa-trash"></i></button>
                                 @include('includes.modals.confirm', ['model' => 'template', 'route' => route('templates.emails.destroy', $template->id), 'form' => true])
                            </div>
                        </td>
                    </tr>
                    @empty
                    @endforelse
                    </tbody>
                </table>

                  <div class="pagination justify-content-center">
                      {{ $templates->links() }}
                  </div>
            </div>
        </div>
    </div>
@endsection
