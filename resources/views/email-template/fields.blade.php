@php($project = $project ?? null)

<div class="form-group ">
    <label for="name">Title:</label>
    <input id="name" type="text" name="name"
           class="form-control @error('name') is-invalid @enderror"
           value="{{ old('name', isset($template) ? $template->name : '') }}">
    @include('includes.partials.error', ['field' => 'name'])
</div>

<div class="form-group">
    <label for="subject">Subject:</label>
    <input id="subject" type="text" name="subject"
           class="form-control @error('subject') is-invalid @enderror"
           value="{{ old('subject', isset($template) ? $template->subject : '') }}">
    @include('includes.partials.error', ['field' => 'subject'])
</div>

{{--<div class="form-group">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-to-tab" data-toggle="tab" href="#nav-to" role="tab"
               aria-controls="nav-to" aria-selected="true">TO</a>
            <a class="nav-item nav-link" id="nav-cc-tab" data-toggle="tab" href="#nav-cc" role="tab"
               aria-controls="nav-cc" aria-selected="false">CC</a>
            <a class="nav-item nav-link" id="nav-bcc-tab" data-toggle="tab" href="#nav-bcc" role="tab"
               aria-controls="nav-bcc" aria-selected="false">BCC</a>
        </div>
    </nav>

    <div class="tab-content pt-1" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-to" role="tabpanel" aria-labelledby="nav-to-tab">
            <div class="form-group">
                <div class="col-lg-12 pl-0">
                    <input type="text" class="js-tags-input form-control" id="to-tag" name="recipients[to]"
                           value="{{ old('recipient.to', isset($template) ? $template->recipients['to']: '') }}">
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-cc" role="tabpanel" aria-labelledby="nav-cc-tab">
            <div class="form-group">
                <div class="col-lg-12 pl-0">
                    <input type="text" class="js-tags-input form-control" id="cc-tag" name="recipients[cc]"
                           value="{{ old('subject', isset($template) ? $template->recipients['cc'] : '') }}">
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="nav-bcc" role="tabpanel" aria-labelledby="nav-bcc-tab">
            <div class="form-group ">
                <div class="col-lg-12 pl-0">
                    <input type="text" class="js-tags-input form-control" id="bcc-tag" name="recipients[bcc]"
                           value="{{ old('subject', isset($template) ? $template->recipients['bcc'] : '') }}">
                </div>
            </div>
        </div>
    </div>
</div>--}}

<div class="form-group">
    <div class="form-check">
        <input class="form-check-input" type="checkbox" id="link">
        <label class="form-check-label" for="link">
            Link To Stage
        </label>
    </div>
</div>

<div class="form-group">
    <label for="content">Email Content: </label>
    <textarea name="content" class="js-summernote @error('content') is-invalid @enderror">
        {{ old('content', isset($template) ? $template->content : '') }}
    </textarea>
    @include('includes.partials.error', ['field' => 'content'])
</div>

<div class="form-group row">
    <label class="col-12" for="example-select2-multiple">Placeholders:</label>
    <div class="col-lg-12">
        <select class="js-select2 form-control" id="example-select2-multiple" name="placeholders[]"
                style="width: 100%;" data-placeholder="Select Placeholders" multiple>
            <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
            @forelse($adminDefinePlaceholders as $adminDefinePlaceholder)
                <option value="{{ $adminDefinePlaceholder->id }}">{{ $adminDefinePlaceholder->name }}</option>
            @empty
            @endforelse
        </select>
    </div>
</div>

@isset($placeholders)
    <div class="form-group">
        <label>Available Placeholders:</label>
        <p>
            @forelse($placeholders as $placeholder)
                <span class="mr-4 text-primary">%%{{ $placeholder['name'] }}%%</span>
            @empty
                <span class="mr-4 ">no place holder available</span>
            @endforelse
        </p>
    </div>
@endisset


