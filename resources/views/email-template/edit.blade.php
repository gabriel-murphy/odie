@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Email Template</a>
@endsection
@section('breadcrumb-active', 'Edit')

@section('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-tags-input/jquery.tagsinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('js/plugins/summernote/summernote.css')}}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            height: 22px;
            line-height: 22px;
            color: #fff;
            font-size: 13px;
            font-weight: 600;
            background-color: #3f9ce8;
            border: none;
            border-radius: 3px;
        }
    </style>
@endsection

@section('content')
    @include('includes.partials.errors')
    <div class="block">
        <div class="block-header block-header-default">Edit Email Template</div>
        <div class="block-content">
            @include('includes.partials.errors')
            <form action="{{ route('templates.emails.update', $template->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                @include('email-template.fields')
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/plugins/jquery-tags-input/jquery.tagsinput.min.js') }}"></script>
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        jQuery(function () {
            Codebase.helper('summernote');
        });
    </script>
    <script>jQuery(function(){ Codebase.helpers(['select2', 'tags-inputs']); });</script>
@endsection
