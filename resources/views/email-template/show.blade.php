@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Email Template</a>
@endsection
@section('breadcrumb-active', 'Show')

@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block block-link-shadow overflow-hidden">
                <div class="block-content block-content-full">
                    <div class="block-header p-0">
                        <h5 class="text-uppercase" title="Email Template Subject">
                            Email Template (<a href="#">{{ $template->name }}</a>)
                        </h5>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table-responsive table-borderless">
                                <tbody class="align-top">
                                <tr>
                                    <th>To:</th>
                                    <td class="pl-3">Customer</td>
                                </tr>
                                <tr>
                                    <th>CC:</th>
                                    <td class="pl-3">Salesman / Customer Rep</td>
                                </tr>
                                <tr>
                                    <th>BCC:</th>
                                    <td class="pl-3">gabriel@romanroofing.com</td>
                                </tr>
                                <tr>
                                    <th>Subject:</th>
                                    <td class="pl-3">{{ $template->title }}</td>
                                </tr>
                                <tr>
                                    <th>Content:</th>
                                    <td class="pl-3">{!! $template->content !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
