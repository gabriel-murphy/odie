@extends('layouts.auth')

@section('content')
    <div class="bg-image bg-pattern" style="background-image: url({{ asset('images/landing/auth-background.jpg') }});">
        <div class="row mx-0 justify-content-center bg-white-op-95">
            <div class="hero-static col-6">
                <div class="content content-full overflow-hidden">
                    <!-- Header -->
                    <div class="py-30 text-center">
                        <a class="link-effect text-pulse font-w700" href="index.html">
                            <img src="{{ asset('images/odie_logo_small.png') }}" alt="">
                        </a>
                        <h1 class="h4 font-w700 mt-30 mb-10">Welcome, {{ auth()->user()->name }}</h1>
                        {{--<h2 class="h5 font-w400 text-muted mb-0">Please verify your email address to continue</h2>--}}
                    </div>
                    <!-- END Header -->

                    <div class="block block-themed block-rounded block-shadow">
                        <div class="block-header bg-gd-pulse">
                            <h3 class="block-title">{{ __('Verify Your Email Address') }}</h3>
                        </div>
                        <div class="block-content">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif
                            <div class="mb-3">
                                {{ __('Before proceeding, please check your email for a verification link.') }}
                                {{ __('If you did not receive the email') }},
                                <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-link m-0 p-0">
                                        {{ __('click here to request another') }}
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
