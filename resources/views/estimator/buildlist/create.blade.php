@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Build Lists</a>
@endsection
@section('breadcrumb-active', 'Build Lists')

@section('content-heading', 'Build Lists')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/plugins/box-multi-selector/multi.min.css') }}">
    <style>
        .item{
            height: 25px ;
            font-size: unset;
            width: 100%;
            text-align: unset;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="block">
                <div class="block-header block-header">
                    <h3 class="block-title">Build Lists</h3>
                </div>
                <div class="block-content">
                    <div id="buildlist" role="tablist" aria-multiselectable="true">
                        @foreach($roofLayers as $layer)
                            <div class="block block-bordered block-rounded mb-2 pb-3">
                                <div class="block-header" role="tab" id="buildlist_{{ $layer->id }}">
                                    <a class="font-w600" data-toggle="collapse" data-parent="#buildlist"
                                       href="#buildlist_{{ $layer->id }}_content">{{ $layer->name }}</a>
                                </div>
                                <div id="buildlist_{{ $layer->id }}_content" class="collapse" role="tabpanel">
                                    <div class="block-content">
                                        <p>Select Materials to Add in Build List</p>
                                        <select class="multi-box" required multiple="multiple" name="favorite_fruits" id="material_select_{{ $layer->id }}">
                                            @foreach($materials as $material)
                                                <option>{{ $material->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/box-multi-selector/multi.min.js') }}"></script>
    <script>
        let select = document.getElementById("material_select_1");
        multi(select);
    </script>
@endsection
