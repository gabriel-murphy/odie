@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Build Lists</a>
@endsection
@section('breadcrumb-active', 'Build Lists')

@section('content-heading', 'Build Lists')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="block">
                <div class="block-header block-header">
                    <h3 class="block-title">Build Lists</h3>
                </div>
                <div class="block-content">
                    <table class="table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Roof Type</th>
                            <th>Address</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($roofTypes as $roofType)
                            <tr>
                                <td>{{ $roofType->name }}</td>
                                <td><a href="{{ route('estimator.buildlists.build') }}" class="btn btn-sm btn-primary">Modify</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="999" class="text-center">{{ _constant('no_record') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
