@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Estimator</a>
@endsection
@section('breadcrumb-active', 'Roof Properties')

@section('content-heading', 'Step 2 - Enter Roof Pitch & Measurements')

@section('content-header-action')
    <a class="btn btn-sm btn-primary" href="/project">
        <i class="fa fa-plus-circle"></i> Import EagleView
    </a>
@endsection

@section('content')
    <div class="block">
        <form action="{{ route('estimator.structure.store', $project->id) }}" method="post">
            @csrf
            <ul class="nav nav-tabs nav-tabs-block">
                <li class="nav-item">
                    <a class="nav-link active" href="#boots-selector" data-toggle="tab">Boots & Vents Selector</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#structure-details" data-toggle="tab">Roof Structure & Take off
                        Details</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active" id="boots-selector" role="tabpanel">
                    <penetration-selector
                        :penetration_types="{{ json_encode($penetration_types) }}"></penetration-selector>
                </div>
                <div class="tab-pane" id="structure-details" role="tabpanel">
                    @include('includes.partials.errors')
                    <div class="row">
                        <div class="col-4">
                            <div class="block border">
                                <div class="block-header block-header-default">Structure Features</div>
                                <div class="block-content">
                                    <div class="form-group">
                                        <label for="square_feet">Roof Square Footage</label>
                                        <input id="square_feet" type="text" name="measurements[square_feet]"
                                               class="form-control @error('measurements.square_feet') is-invalid @enderror"
                                               value="{{ old('measurements.square_feet') }}"  autofocus>
                                        @include('includes.partials.error', ['field' => 'measurements.square_feet'])
                                    </div>
                                    <div class="form-group">
                                        <label for="stories">Number of Stories</label>
                                        <select id="stories" name="measurements[stories]" required
                                                class="form-control @error('stories') is-invalid @enderror">
                                            <option>Select # of Stories</option>
                                            @foreach ($story_types as $story_type)
                                                <option value="{{ $story_type->value }}">{{ $story_type->name }}</option>
                                            @endforeach
                                        </select>
                                        @include('includes.partials.error', ['field' => 'stories'])
                                    </div>
                                    <div class="form-group">
                                        <label for="facets">Total Roof Facets</label>
                                        <input id="facets" type="text"
                                               class="form-control @error('facets') is-invalid @enderror"
                                               name="measurements[facets]" value="{{ old('measurements.facets') }}" required autofocus>
                                        @include('includes.partials.error', ['field' => 'facets'])
                                    </div>
                                    <div class="form-group">
                                        <label for="pitch">Predominant Roof Pitch</label>
                                        <select id="pitch" class="form-control @error('pitch') is-invalid @enderror"
                                                name="measurements[pitch]" required>
                                            <option>Select Roof Pitch</option>
                                            @foreach ($pitch_types as $pitch_type)
                                                <option value="{{ $pitch_type->value }}">{{ $pitch_type->name }}</option>
                                            @endforeach
                                        </select>
                                        @include('includes.partials.error', ['field' => 'pitch'])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="block border">
                                <div class="block-header block-header-default">Roof Take-Off Detail</div>
                                <div class="block-content">
                                    <div class="form-row">
                                        <div class="form-group col-6">
                                            <label for="ridges">Total Ridges</label>
                                            <input id="ridges" type="text"
                                                   class="form-control @error('ridges') is-invalid @enderror"
                                                   name="measurements[ridges]" value="{{ old('measurements.ridges') }}" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="ridge_vent">Total Ridge Vent</label>
                                            <input id="ridge_vent" type="text"
                                                   class="form-control @error('ridge_vent') is-invalid @enderror"
                                                   name="measurements[ridge_vent]" value="{{ old('measurements.ridge_vent') }}" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="hips">Total Hips</label>
                                            <input id="hips" type="text"
                                                   class="form-control @error('hips') is-invalid @enderror" name="measurements[hips]"
                                                   value="{{ old('measurements.hips') }}" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="valleys">Total Valleys</label>
                                            <input id="valleys" type="text"
                                                   class="form-control @error('valleys') is-invalid @enderror"
                                                   name="measurements[valleys]" value="{{ old('measurements.valleys') }}" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="rakes">Total Rakes</label>
                                            <input id="rakes" type="text"
                                                   class="form-control @error('rakes') is-invalid @enderror"
                                                   name="measurements[rakes]" value="{{ old('measurements.rakes') }}" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="eaves"> Total Eaves</label>
                                            <input id="eaves" type="text"
                                                   class="form-control @error('eaves') is-invalid @enderror"
                                                   name="measurements[eaves]" value="{{ old('measurements.eaves') }}" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="flashing">Flashing</label>
                                            <input id="flashing" type="text"
                                                   class="form-control @error('flashing') is-invalid @enderror"
                                                   name="measurements[flashing]" value="{{ old('measurements.flashing') }}" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="step_flashing">Step Flashing</label>
                                            <input id="step_flashing" type="text"
                                                   class="form-control @error('step_flashing') is-invalid @enderror"
                                                   name="measurements[step_flashing]" value="{{ old('measurements.step_flashing') }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="block border">
                                <div class="block-header block-header-default">Crickets, Dryer Vents & Satellites</div>
                                <div class="block-content">
                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <label for="additional[crickets]">Number of Crickets</label>
                                            <input id="crickets" type="text"
                                                   class="form-control @error('crickets') is-invalid @enderror"
                                                   name="additional[crickets]" value="{{ old('additional.crickets') }}">
                                        </div>
                                        <div class="form-group col-12">
                                            <label for="satellites">Number of Satellites</label>
                                            <input id="satellites" type="text"
                                                   class="form-control @error('satellites') is-invalid @enderror"
                                                   name="additional[satellites]" value="{{ old('measurements.satellites') }}">
                                        </div>
                                        <div class="form-group col-12">
                                            <label for="dryer_vents">Number of Dryer Vents</label>
                                            <input id="dryer_vents" type="text"
                                                   class="form-control @error('dryer_vents') is-invalid @enderror" name="additional[dryer_vents]"
                                                   value="{{ old('additional.dryer_vents') }}">
                                        </div>
                                        <div class="form-group col-12">
                                            <label for="solar_panels">Number of Solar Panels</label>
                                            <input id="solar_panels" type="text"
                                                   class="form-control @error('solar_panels') is-invalid @enderror"
                                                   name="additional[solar_panels]" value="{{ old('additional.solar_panels') }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 my-3 text-right">
                            <button class="btn btn-primary btn-sm w-25" type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
