@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Estimator</a>
@endsection
@section('breadcrumb-active', 'Select Project')

@section('content-heading', 'Select Project')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="block">
                <div class="block-header block-header">
                    <h3 class="block-title">Projects List</h3>
                    <div class="block-options">
                        @include('includes.common.search', ['route' => route('projects.index')])
                    </div>
                </div>
                <div class="block-content">
                    <table class="table table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Address</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($projects as $project)
                            <tr>
                                <td>{{ $project->title }}</td>
                                <td>{{ $project->address }}</td>
                                <td>{{ $project->created_at->format('D M j, Y g:i a') }}</td>
                                <td>{{ $project->updated_at->format('D M j, Y g:i a') }}</td>
                                @php($route = $step == 'roof-selector' ? 'estimator.roof_selector' : 'estimator.structure_details')
                                <td><a href="{{ route($route, $project->id) }}" class="btn btn-sm btn-primary">Proceed</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="999" class="text-center">{{ _constant('no_record') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
