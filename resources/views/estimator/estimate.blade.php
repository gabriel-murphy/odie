@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Estimator</a>
@endsection
@section('breadcrumb-active', 'Estimate Result')

@section('content-header-action')
    <button>fsf</button>
@endsection

@section('content')
        <!-- Project Details -->
        <h2 class="content-heading pt-2">Project Details</h2>
        <div class="row row-deck gutters-tiny">
            <!-- Billing Address -->
            <div class="col-md-6">
                <div class="block block-rounded">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Customer Details</h3>
                    </div>
                    <div class="block-content">
                        <div class="font-size-lg text-black mb-5">{{ $project->customer->name }}</div>
                        <address>
                            {{ $project->customer->address->street }}<br>
                            {{ $project->customer->address->city }} {{ $project->customer->address->zip_code }}<br>
                            {{ $project->customer->address->country }}<br><br>
                            <i class="fa fa-phone mr-5"></i> {{ $project->customer->phone }}<br>
                            <i class="fa fa-envelope-o mr-5"></i> <a href="javascript:void(0)">{{ $project->customer->email }}</a>
                        </address>
                    </div>
                </div>
            </div>
            <!-- END Billing Address -->

            <!-- Shipping Address -->
            <div class="col-md-6">
                <div class="block block-rounded">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Structure Details</h3>
                    </div>
                    <div class="block-content">
                        <div class="font-size-lg text-black mb-5">{{ $project->preferred_material_name }} - {{ $project->project_type }}</div>
                        <address>
                            5110 8th Ave<br>
                            New York 11220<br>
                            United States<br><br>
                            <i class="fa fa-phone mr-5"></i> (999) 111-222222<br>
                            <i class="fa fa-envelope-o mr-5"></i> <a href="javascript:void(0)">company@example.com</a>
                        </address>
                    </div>
                </div>
            </div>
            <!-- END Shipping Address -->
        </div>
        <!-- END Addresses -->

        <!-- Products -->
        <h2 class="content-heading">
            Build Sheets ({{ $estimate->buildSheets->count() }})
            <div class="float-right">
                <label class="css-control css-control-sm css-control-primary css-switch">
                    <input type="checkbox" class="css-control-input" checked="">
                    Show Actual Cost  <span class="css-control-indicator"></span>
                </label>
            </div>
        </h2>
        <div class="block block-rounded">
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-borderless table-striped">
                        <thead>
                        <tr>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Unit</th>
                            <th>Qty</th>
                            <th>Unit Price</th>
                            <th>Waste Factor</th>
                            <th>Total Cost</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($estimate->buildSheets as $build_sheet)
                        <tr>
                            <td>{{ Str::studly($build_sheet->type) }}</td>
                            <td>{{ $build_sheet->name }}</td>
                            <td>{{ $build_sheet->unit }}</td>
                            <td class="font-w600">
                                {{ $build_sheet->actual_quantity }}
                                @if($build_sheet->waste_factor) (+{{ $build_sheet->additional_quantity }}) @endif
                            </td>
                            <td>${{ $build_sheet->unit_price }}</td>
                            <td>{{ $build_sheet->waste_factor ? $build_sheet->waste_factor : '-' }}</td>
                            <td>
                                ${{ $build_sheet->cost }}
                                @if($build_sheet->waste_factor)(+{{ $build_sheet->waste_factor_cost }}) @endif
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="6" class="text-right font-w600">Sub Total:</td>
                            <td class="text-right">${{ $estimate->buildSheets->sum('cost') }}</td>
                        </tr>
                        <tr>
                            <td colspan="6" class="text-right font-w600">Additional Cost:</td>
                            <td class="text-right">${{ $estimate->buildSheets->sum('waste_factor_cost') }}</td>
                        </tr>
                        <tr>
                            <td colspan="6" class="text-right font-w600">Material Profit:</td>
                            <td class="text-right">${{ $estimate->buildSheets->sum('waste_factor_cost') }}</td>
                        </tr>
                        <tr>
                            <td colspan="6" class="text-right font-w600">Labor Profit:</td>
                            <td class="text-right">${{ $estimate->buildSheets->sum('waste_factor_cost') }}</td>
                        </tr>
                        <tr class="table-success">
                            <td colspan="6" class="text-right font-w600 text-uppercase">Total Cost:</td>
                            <td class="text-right font-w600">${{ $estimate->buildSheets->sum('total_cost') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Products -->
@endsection
