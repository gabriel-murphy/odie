@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Estimator</a>
@endsection
@section('breadcrumb-active', 'Roof Selector')

@section('content-heading', 'Step 1 - Select Roof Type')

@section('content')
    <form action="{{ route('estimator.estimate', $project->id) }}" method="post">
        @csrf
        <roof-selector :roof-type-groups="{{ json_encode($roof_type_groups) }}"></roof-selector>
    </form>
@endsection
