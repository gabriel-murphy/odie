@extends('layouts.backend')

@section('content')
<!-- Main section-->
      <section class="section-container">
         <!-- Page content-->
        <div class="content-wrapper" style="padding-bottom:0px">
            <div class="content-header">
               <div class="content-title">Estimator :: Create Estimates<small>Step 3 - Generate & Send Estimate</small></div>
            </div>
        </div>
    <div class="container-fluid">
        @if(Session::has('message'))
            <div class="alert alert-success" id="flash-message"><h3> {!! session('message') !!}</h3></div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="block">
                    <div class="block-header block-header-default">Generate Proposal for Project #%%##</div>
                    <div class="card-body row" style="padding:40px">
                        <div class="col-md-6" style="padding-left:0px;">
                            <div class="proposal-toemail" style="display:flex;">
                                <span style="padding-top:7px;margin-right:5px;white-space:nowrap">Customer's Email: </span>
                                <input id="toemail" type="text" class="form-control @error('toemail') is-invalid @enderror" name="squares" value="{{ old('toemail') }}" required autofocus>
                            </div>
                            <div class="proposal-fromemail" style="display:flex;margin-top:15px;">
                                <span style="padding-top:7px;margin-right:6px;white-space:nowrap">Estimator's Email: </span>
                                <input id="fromemail" type="text" class="form-control @error('fromemail') is-invalid @enderror" name="fromemail" value="{{ old('fromemail') }}" required autofocus>
                            </div>
                            <div class="proposal-subject" style="display:flex;margin-top:15px">
                                <span style="padding-top:7px;margin-right:10px">Subject: </span>
                                <input id="subject" type="text" class="form-control @error('subject') is-invalid @enderror" name="squares" value="{{ old('subject') }}" required autofocus>
                            </div>
                            <div class="proposal-attachtext" style="margin-top:15px">
                                <span style="padding-top:7px;margin-right:10px">Attach Text: </span>
                                <div contenteditable="true" id="attachtext"  class="form-control @error('attachtext') is-invalid @enderror" name="attachtext" value="{{ old('attachtext') }}" style="margin-top:5px;overflow:auto;height:300px">
                                    <?php echo $template[0]->email_message ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <object data="{{$proposal_pdffilepath}}" type="application/pdf" style="width:100%;height:483px;">
                                <embed src="{{$proposal_pdffilepath}}" type="application/pdf" />
                            </object>
                        </div>
                        <div class="col-md-6" style="padding-left:0px">
                            <p style="padding-top:7px;margin-top:10px;margin-bottom:16px;">Add Attachments: </p>
                            <select class="chzn-select" id="add-attachments-multi" multiple="true">
                                <option id="chzn_select_0" value="0">Oil Canning Waiver</option>
                            </select>
                        </div>
                        <div class="col-md-6" >
                            <p style="padding-top:7px;margin-top:10px">Reminder: </p>
                            <div class="custom-control custom-checkbox" style="display: flex; margin-top:5px">
                                <input type="checkbox" class="custom-control-input" id="reminder-check" checked>
                                <label class="custom-control-label" for="reminder-check" style="margin-top:10px">24 hour</label>
                                <select class="form-control" name="reminder_time" id="reminder_time" value="0" required style="width:200px;margin-left:10px" disabled>
                                    <option value="0">Does not repeat</option>
                                    <option value="1">1 Hour</option>
                                    <option value="1">2 Hour</option>
                                    <option value="1">5 Hour</option>
                                    <option value="1">10 Hour</option>
                                    <option value="1">15 Hour</option>
                                </select>
                            </div>
                        </div>
                        <div style="display:block; width:100%; margin-top:20px" >
                            <button onclick="sendProposal({{$project_id}})" class="btn btn-labeled btn-primary float-right">
                                <span class="btn-label">
                                    <i class="fa fa-plus-circle"></i>
                                </span>Send Email
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="loading-div" style="display:none">
        <div id="loading-bg"></div>
        <div id="loading-bar">
            <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                <path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                    <animateTransform 
                    attributeName="transform" 
                    attributeType="XML" 
                    type="rotate"
                    dur="1s" 
                    from="0 50 50"
                    to="360 50 50" 
                    repeatCount="indefinite" />
                </path>
            </svg>
        </div>
    </div>
    <div class="modal fade come-from-modal center" id="send_dialog" role="dialog" aria-labelledby="send_dialog-Label" aria-hidden="true">
        <div id="dialog-bg" class="dialog-bg"></div>
        <div class="modal-dialog" role="document" style="min-width:300px;top:35%;">
           <div class="modal-content">
              <div class="modal-header danger">
                 <h5 class="modal-title">
                    Email Confirm
                 </h5>
              </div>
              <div class="modal-body">
                <p>Email sent successfully!</p>
              </div>
              <div class="modal-footer">
                 <button class="btn btn-primary" onclick="Dialog_OK_Handler()" style="width:80px">OK</button>
              </div>
     
           </div>
        </div>
     </div>

    <script style="script/JavaScript">
        var template = <?php echo $template ?>;
        var selectedAttachments = [];
        // console.log(template);
        document.getElementById('subject').value = template[0].email_subject;
        var customer_email = '<?php echo $customer_email ?>';
        document.getElementById('toemail').value = customer_email;
        var user_email = '<?php echo $estimator_email ?>';
        document.getElementById('fromemail').value = user_email;
        
        // document.getElementById('attachtext').innerHTML  = template[0].email_message;
        function sendProposal(project_id){
            //send proposal to client's email
            customer_email = $('#toemail').val();
            user_email = $('#fromemail').val();
            
            //validation
            if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(customer_email)))
            {
                if (!$('#toemail').hasClass('is-invalid')){
                    $('#toemail').addClass('is-invalid');
                    $('#toemail').focus();
                    return;
                }
            }
            else if($('#toemail').hasClass('is-invalid')){
                $('#toemail').removeClass('is-invalid');
            }

            if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user_email)))
            {
                if (!$('#fromemail').hasClass('is-invalid')){
                    $('#fromemail').addClass('is-invalid');
                    $('#fromemail').focus();
                    return;
                }
            }
            else if($('#fromemail').hasClass('is-invalid')){
                $('#fromemail').removeClass('is-invalid');
            }
            $('#loading-div').css('display', 'block');
            $.ajax({
                type: 'POST',
                url: "/estimator/generateProposal/"+ project_id + "/send",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "customer_email" : $('#toemail').val(),
                    "estimator_email" : $('#fromemail').val(),
                    "subject" : $('#subject').val(),
                    "attachtext" : $('#attachtext').html(),
                    "proposal_filepath" : "{{$proposal_pdffilepath}}",
                    "attachments" : selectedAttachments
                },
                success: function(res) {
                    if (res == 'success'){
                        $('#loading-div').css('display', 'none');            
                        showmodal();
                    }
                }
            });
            
        }
        function showmodal(){
            $('#send_dialog').modal('show');
            $('#dialog-bg').css('display', 'block');
            $('.modal-backdrop.show').css('display', 'none');
        }
        function Dialog_OK_Handler(){
            window.location.replace('/signer/docs');
        }
    </script>
</section>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
<script style="script/JavaScript">
    (function($) {
        $('#reminder-check').change((e)=>{
            if(e.target.checked){
                $('#reminder_time').prop("disabled", true);
            }
            else {
                $('#reminder_time').prop("disabled", false);
            }
        })

        $("#add-attachments-multi").chosen({
            placeholder_text: "Add Attachments"
        }).change(function(event) {
            if (event.target == this) {
                selectedAttachments = $(this).val();
            }
        });
    })(jQuery);

</script>
@endsection
