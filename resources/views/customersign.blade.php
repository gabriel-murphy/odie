<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Welcome {{ Auth::user()->first_name ?? ""}} to Odie</title>
    <!-- Scripts -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <!--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">-->
    <script src="https://kit.fontawesome.com/0b514f50a8.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/vendor/animate.css/animate.css">
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.css">
    <!-- Datatables-->
    <link rel="stylesheet" href="/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
    <link rel="stylesheet" href="/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="/vendor/bootstrap/dist/js/bootstrap.js"></script>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/theme.css') }}" rel="stylesheet">
    <link href="{{asset('/css/jquery-pincode-autotab.min.css')}}" rel="stylesheet">
    <script src="{{asset('/js/jquery-pincode-autotab.min.js')}}"></script>
    <script src="/vendor/jquery-jsignature/libs/modernizr.js"></script>
    <style type="text/css">
        #signatureparent {
            color:darkblue;
            background-color:#fff;
            /*max-width:600px;*/
            padding:20px;
        }
        
        /*This is the div within which the signature canvas is fitted*/
        #signature {
            border: 2px dotted black;
            background-color:#fff;
        }

        /* Drawing the 'gripper' for touch-enabled devices */ 
        html.touch #content {
            float:left;
            width:92%;
        }
        html.touch #scrollgrabber {
            float:right;
            width:4%;
            margin-right:2%;
            background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAFCAAAAACh79lDAAAAAXNSR0IArs4c6QAAABJJREFUCB1jmMmQxjCT4T/DfwAPLgOXlrt3IwAAAABJRU5ErkJggg==)
        }
        html.borderradius #scrollgrabber {
            border-radius: 1em;
        }
    </style>
</head>
<body>
    <header class="topnavbar-wrapper">
        <!-- START Top Navbar-->
        <nav class="navbar topnavbar" style="justify-content: left">
            <!-- START navbar header-->
            <div class="navbar-header">
                <a class="navbar-brand" href="/dashboard">
                    <div class="brand-logo" style="color:white;font-weight:bold;font-size:38px;text-align:center;display:block;">Odie<sup style="font-size:50%;font-weight:normal;opacity:.5">®</sup></div>
                    <div class="brand-logo-collapsed">Odie</div>
                </a>
                
            </div><!-- END navbar header-->
            <h2 style="margin:0 auto;">
                Customer Sign
            </h2>
        </nav><!-- END Top Navbar-->
        
    </header>
    <div class="container" style="margin-top:20px">
        <div class="row">
            <div class="col-md-8" style="padding-left:25px;" id="pdf-body">
                <h4 id="show-message">Dear Customer! Please sign below estimate document.</h4>
                <object id="pdf-path" data="{{$document_path}}" type="application/pdf" style="width:100%;height:700px">
                </object>
            </div>
            <div class="col-md-4" style="padding-top:30px" id="sign-input-body">
                <p>Input your Name for sign</p>
                <input class="form-control" id="customer-name" value='{{$customer_name_first." ".$customer_name_last}}'>
                <p style="margin-top:20px">Select sign type</p>
                <select id="sign_type" class="form-control" name="sign_type" required>
                    <option value="0">Initial Sign</option>
                    <option value="1">Full Name Sign</option>
                    <option value="2">Manual Sign</option>
                </select>
                <div id="manual-sign" style="margin-top:20px;display:none">
                    <div id="content">
                        <div id="signatureparent">
                            <div id="signature" style="position:relative"></div>
                        </div>
                        <div id="tools"></div>
                        <div><div id="displayarea"></div></div>
                    </div>
                    <div id="scrollgrabber"></div>
                </div>
                <button onclick="customerSign()" class="btn btn-labeled btn-primary" style="margin:30px auto; display:block">
                    <span class="btn-label">
                        <i class="fa fa-file-signature"></i>
                    </span>
                    <span style="padding:0px 30px;">Sign</span>
                </button>
            </div>
        </div>

    </div>
    <div id="loading-div" style="display:none">
        <div id="loading-bg"></div>
        <div id="loading-bar">
            <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                <path fill="#fff" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                    <animateTransform 
                    attributeName="transform" 
                    attributeType="XML" 
                    type="rotate"
                    dur="1s" 
                    from="0 50 50"
                    to="360 50 50" 
                    repeatCount="indefinite" />
                </path>
            </svg>
        </div>
    </div>
    
    <div class="modal fade come-from-modal center" id="confirmdlg" role="dialog" aria-labelledby="confirmdlg-Label" aria-hidden="true">
        <div id="dialog-bg" class="dialog-bg"></div>
        
        <div class="modal-dialog" role="document" style="min-width:300px;top:35%;">
           <div class="modal-content">
              <div class="modal-header danger">
                 <h5 class="modal-title">
                    Sign Confirm
                 </h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <p>Do you agree to sign as "<span id="signname">123</span>"?</p>
              </div>
              <div class="modal-footer">
                 <button class="btn btn-primary" onclick="okBtnHandler()" style="width:80px">OK</button>
              </div>
     
           </div>
        </div>
    </div>

    <div class="modal fade come-from-modal center" id="verifydlg" role="dialog" aria-labelledby="verifydlg-Label" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
        <div id="dialog-bg" class="dialog-bg"></div>
        <div class="modal-dialog" role="document" style="min-width:300px;top:35%;">
           <div class="modal-content">
              <div class="modal-header danger">
                 <h5 class="modal-title">
                    Verify Confirm
                 </h5>
              </div>
              <div class="modal-body">
                <p>We sent 6 digits to your email address. Please check and input.</p>
                <div class="col-9 jpa" style="margin:auto">
                    <input type="tel" id="verify_firstnum" class="form-control" maxlength="1">
                    <input type="tel" id="verify_secondnum" class="form-control" maxlength="1">
                    <input type="tel" id="verify_thirdnum" class="form-control" maxlength="1">
                    <input type="tel" id="verify_fourthnum" class="form-control" maxlength="1">
                    <input type="tel" id="verify_fifthnum" class="form-control" maxlength="1">
                    <input type="tel" id="verify_sixthnum" class="form-control" maxlength="1">
                </div>
              </div>
     
           </div>
        </div>
    </div>
    <script src="/vendor/jquery-jsignature/src/jSignature.js"></script>
    <script src="/vendor/jquery-jsignature/src/plugins/jSignature.CompressorBase30.js"></script>
    <script src="/vendor/jquery-jsignature/src/plugins/jSignature.CompressorSVG.js"></script>
    <script src="/vendor/jquery-jsignature/src/plugins/jSignature.UndoButton.js"></script> 
    <script src="/vendor/jquery-jsignature/src/plugins/signhere/jSignature.SignHere.js"></script> 

    <script style="script/JavaScript">
        var signname = "";
        var first_verify_value = 0;
        var second_verify_value = 0;
        var third_verify_value = 0;
        var fourth_verify_value = 0;
        var fifth_verify_value = 0;
        var sixth_verify_value = 0;

        //signature
        (function($) {
            var topics = {};
            $.publish = function(topic, args) {
                if (topics[topic]) {
                    var currentTopic = topics[topic],
                    args = args || {};
            
                    for (var i = 0, j = currentTopic.length; i < j; i++) {
                        currentTopic[i].call($, args);
                    }
                }
            };
            $.subscribe = function(topic, callback) {
                if (!topics[topic]) {
                    topics[topic] = [];
                }
                topics[topic].push(callback);
                return {
                    "topic": topic,
                    "callback": callback
                };
            };
            $.unsubscribe = function(handle) {
                var topic = handle.topic;
                if (topics[topic]) {
                    var currentTopic = topics[topic];
            
                    for (var i = 0, j = currentTopic.length; i < j; i++) {
                        if (currentTopic[i] === handle.callback) {
                            currentTopic.splice(i, 1);
                        }
                    }
                }
            };
        })(jQuery);

        $(document).ready(function() {
		    $(".jpa input").jqueryPincodeAutotab();
            $('#verifydlg').modal('show');
            
            $('.jpa input').on('keydown',function() {
                send_verify_ajax();
            })
            $('.jpa input').on('paste',function(event) {
                event.preventDefault();
                send_verify_ajax();
            })

            //signature
            
            // This is the part where jSignature is initialized.
            var $sigdiv = $("#signature").jSignature({'UndoButton':true})
            
            // All the code below is just code driving the demo. 
            , $tools = $('#tools')
            , $extraarea = $('#displayarea')
            , pubsubprefix = 'jSignature.demo.'
            
            var export_plugins = $sigdiv.jSignature('listPlugins','export')
            

            
            $('<button class="btn btn-labeled btn-blue" style="float:right;margin-top:3px">Reset</button>').bind('click', function(e){
                $sigdiv.jSignature('reset')
            }).appendTo($tools)
            

            if (Modernizr.touch){
                $('#scrollgrabber').height($('#content').height())		
            }

            $('#sign_type').on('change', function(){
                if ($('#sign_type').val() == '2'){
                    $('#manual-sign').css('display', 'block');
                    $('#signature').css('height', '100px');
                    $('#signature canvas').css('height', '100px');
                    $('#signature canvas').attr('width', '306');
                    $('#signature canvas').attr('height', '100');
                    $sigdiv.jSignature('reset')
                }
                else {
                    $('#manual-sign').css('display', 'none');
                }
            })
        });
        
        function send_verify_ajax(){
            first_verify_value = $('#verify_firstnum').val();
            second_verify_value = $('#verify_secondnum').val();
            third_verify_value = $('#verify_thirdnum').val();
            fourth_verify_value = $('#verify_fourthnum').val();
            fifth_verify_value = $('#verify_fifthnum').val();
            sixth_verify_value = $('#verify_sixthnum').val();
            
            var pincode = first_verify_value + second_verify_value + third_verify_value + fourth_verify_value + fifth_verify_value + sixth_verify_value;
            $.ajax({
                type: 'POST',
                url: "/customersign/verify",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "pincode" : pincode
                },
                success: function(res) {
                    
                    if (res == 'success'){
                        $('#verifydlg').modal('hide');
                    }
                }
            });
        }

        function customerSign(){
            if ($('#sign_type').val() != "2"){
                if ($('#customer-name').val().trim() == ""){
                    $('#customer-name').addClass('is-invalid');
                    $('#customer-name').focus();
                    return;
                }
                else if ($('#customer-name').hasClass('is-invalid')){
                    $('#customer-name').removeClass('is-invalid')
                }
            }
            else {
                //if manual sign mode,  manual sign
                if ($('#undo_btn').css('display') == "none"){
                    $('#signhere-img').addClass('pulse');
                    setTimeout(() => {
                        $('#signhere-img').removeClass('pulse');
                    }, 1000);
                    return;
                }
            }
            
            $('#confirmdlg').modal('show');
            //if initial sign, then set initial
            if ($('#sign_type').val() == 0){
                var name = $('#customer-name').val();
                var initials = name.match(/\b\w/g) || [];
                initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
                signname = initials;
                $('#signname').html(signname);
            }
            else if ($('#sign_type').val() == 1){
                signname = $('#customer-name').val();
                $('#signname').html(signname);
            }
            else{
                $('#signname').html('your manual sign');
            }
            
            $('#dialog-bg').css('display', 'block');
            $('.modal-backdrop.show').css('display', 'none');
        }
        function okBtnHandler(){
            
            $('#confirmdlg').modal('hide');
            $('#loading-div').css('display', 'block');
            $.ajax({
                type: 'POST',
                url: "/customersign",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "sign_name" : signname,
                    "sign_type" : $('#sign_type').val(),
                    "sign_info" : {
                        "project_id" : {{$customer_info['project_id']}},
                        "document_id" : {{$customer_info['document_id']}}
                    },
                    'sign_image' : $('#signature').jSignature("getData","image")
                },
                success: function(res) {
                    
                    // return;
                    if (res && res.success == "1"){

                        $('#sign-input-body').css('display', 'none');
                        $('#show-message').html('Thanks! Successfully signed. Please see signed document below.');
                        $('#loading-div').css('display', 'none');
                        $('#pdf-body').css('max-width', '100%');
                        $('#pdf-body').css('flex', '0 0 100%');
                        
                        $('#pdf-path').attr('data', res.url);
                    }
                }
            });
        }
    </script>
</body>
