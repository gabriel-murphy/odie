<div class="form-row">
    <div class="form-group col-6">
        <label for="first_name">First Name:</label>
        <input id="first_name" type="text" name="first_name"
               class="form-control @error('first_name') is-invalid @enderror"
               value="{{ old('first_name', isset($user) ? $user->first_name : '') }}">
        @include('includes.partials.error', ['field' => 'first_name'])
    </div>
    <div class="form-group col-6">
        <label for="last_name">Last Name:</label>
        <input id="last_name" type="text" name="last_name"
               class="form-control @error('last_name') is-invalid @enderror"
               value="{{ old('last_name', isset($user) ? $user->last_name : '') }}">
        @include('includes.partials.error', ['field' => 'last_name'])
    </div>
</div>
<div class="form-row">
    <div class="form-group col-6">
        <label for="username">Username:</label>
        <input id="username" type="text" name="username"
               class="form-control @error('username') is-invalid @enderror"
               value="{{ old('username', isset($user) ? $user->username : '') }}">
        @include('includes.partials.error', ['field' => 'username'])
    </div>

    <div class="form-group col-6">
        <label for="title">Title:</label>
        <input id="title" type="text" name="title"
               class="form-control @error('title') is-invalid @enderror"
               value="{{ old('title', isset($user) ? $user->title : '') }}">
        @include('includes.partials.error', ['field' => 'title'])
    </div>
</div>
<div class="form-row">
    <div class="form-group col-6">
        <label for="email">Email:</label>
        <input id="email" type="email" name="email"
               class="form-control @error('email') is-invalid @enderror"
               value="{{ old('email', isset($user) ? $user->email : '') }}">
        @include('includes.partials.error', ['field' => 'email'])
    </div>

    <div class="form-group col-6">
        <label for="secondary_email">Secondary Email:</label>
        <input id="secondary_email" type="email" name="secondary_email"
               class="form-control @error('secondary_email') is-invalid @enderror"
               value="{{ old('secondary_email', isset($user) ? $user->secondary_email : '') }}">
        @include('includes.partials.error', ['field' => 'secondary_email'])
    </div>
</div>
<div class="form-group">
    <label for="password">Password:</label>
    <input id="password" type="text" name="password"
           class="form-control @error('password') is-invalid @enderror"
           value="{{ old('password')}}">
    @include('includes.partials.error', ['field' => 'password'])
</div>
<div class="form-row">
    <div class="form-group col-4">
        <label for="cell_phone">Cell Phone:</label>
        <input id="cell_phone" type="text" name="cell_phone"
               class="form-control @error('cell_phone') is-invalid @enderror"
               value="{{ old('cell_phone', isset($user) ? $user->cell_phone : '') }}">
        @include('includes.partials.error', ['field' => 'cell_phone'])
    </div>
    <div class="form-group col-4">
        <label for="home_phone">Home Phone:</label>
        <input id="home_phone" type="text" name="home_phone"
               class="form-control @error('home_phone') is-invalid @enderror"
               value="{{ old('home_phone', isset($user) ? $user->home_phone : '') }}">
        @include('includes.partials.error', ['field' => 'home_phone'])
    </div>
    <div class="form-group col-4">
        <label for="work_phone">Work Phone:</label>
        <input id="work_phone" type="text" name="work_phone"
               class="form-control @error('work_phone') is-invalid @enderror"
               value="{{ old('work_phone', isset($user) ? $user->work_phone : '') }}">
        @include('includes.partials.error', ['field' => 'work_phone'])
    </div>
</div>
<div class="form-group">
    <label for="signature_text">Signature Text:</label>
    <textarea class="form-control @error('signature_text') is-invalid @enderror"
              name="signature_text" id="signature_text"
              rows="5">{{ old('signature_text', isset($user) ? $user->signature_text : '') }}</textarea>
    @include('includes.partials.error', ['field' => 'signature_text'])
</div>
<div class="form-row">
    <div class="form-group col-6">
        <label>Signature Image:</label>
        <input type="file" class="form-control"
               placeholder="Enter Home Phone" name="signature_image">
        @include('includes.partials.error', ['field' => 'signature_image'])
        @if(isset($user))
            <div class="my-2">
                <img class="img-avatar img-avatar128 rounded" src="{{ $user->signature_path }}" alt="">
            </div>
        @endif
    </div>
    <div class="form-group col-6">
        <label>Profile Image:</label>
        <input type="file" class="form-control" name="avatar">
        @include('includes.partials.error', ['field' => 'avatar'])
        @if(isset($user))
            <div class="my-2">
                <img class="img-avatar img-avatar128 rounded" src="{{ $user->avatar_path }}" alt="">
            </div>
        @endif
    </div>
</div>
