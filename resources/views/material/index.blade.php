@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Materials</a>
@endsection
@section('breadcrumb-active', 'Index')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Materials List</h3>
            <div class="block-options">
                @include('includes.common.search', ['route' => route('materials.index')])
            </div>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th >Description</th>
                        <th>Cost</th>
                        <th>Unit</th>
                        <th>Item/Unit</th>
                        <th>Coverage Unit</th>
                        <th>Coverage Value</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($materials as $material)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td class="w-25">{{ $material->name }}</td>
                            <td class="w-25">{{ $material->description }}</td>
                            <td>${{ $material->cost }}</td>
                            <td>{{ app('constants')->constant($material->unit) }}</td>
                            <td>{{ $material->items_per_unit }}</td>
                            <td>{{ app('constants')->constant($material->coverage_unit) }}</td>
                            <td>{{ $material->coverage_value}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('materials.edit', $material->id) }}" class="btn btn-sm btn-secondary"
                                       data-toggle="tooltip" title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                        <button data-toggle="modal" data-target="#confirm_material_{{$material->id}}" class="btn btn-sm btn-outline-danger" title="">
                                            <i class="fa fa-trash"></i></button>
                                        @include('includes.modals.confirm', ['model' => 'material', 'route' => route('materials.destroy', $material->id), 'form' => true])
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="999" class="text-center">{{ _constant('no_record') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <div class="pagination justify-content-center">
                    {{ $materials->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
