@extends('layouts.backend')

@section('content')
<!-- Main section-->
    <section class="section-container">
         <!-- Page content-->
        <div class="content-wrapper" style="padding-bottom:0px">
            <div class="content-header">
               <div class="content-title">Documents</div>
            </div>
        </div>
        <div class="container-fluid">
            @if(Session::has('message'))
                <div class="alert alert-success" id="flash-message"><h3> {!! session('message') !!}</h3></div>
            @endif
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="block">
                        <div class="block-header block-header-default">All signed & request proposal documents</div>
                        <div class="block-content" style="padding:40px">
                            <table id="docs-table" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr style="text-align:center">
                                        <th>No</th>
                                        <th>Created Date</th>
                                        <th>Document Type</th>
                                        <th>Sign Status</th>
                                        <th>Document Name</th>
                                        <th>Customer Email</th>
                                        <th>Customer Name</th>
                                        <th>Customer PhoneNumber</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($documents as $item)
                                    <tr style="text-align:center">
                                        <td></td>
                                        <td>{{date('Y-m-d', strtotime($item->created_date))}}</td>
                                        <td>{{$item->docTypeName}}</td>
                                        <td>{{$item->require_customer_signature == 'yes' ? 'Send Request':'Signed'}}</td>
                                        <td><a target="_blank" href="{{$item->filename}}">{{basename($item->filename)}}</a></td>
                                        <td>{{$item->customer_email}}</td>
                                        <td>{{$item->first_name." ".$item->last_name}}</td>
                                        <td>{{$item->cell_phone}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                {{-- <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Office</th>
                                        <th>Age</th>
                                        <th>Start date</th>
                                        <th>Salary</th>
                                    </tr>
                                </tfoot> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="/js/documents.js"></script>
@endsection
