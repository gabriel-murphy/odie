@extends('layouts.backend')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">Edit User</div>
        <div class="block-content">
            <form action="{{ route('users.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                @include('user.form')
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
