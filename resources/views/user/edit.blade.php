@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Users</a>
@endsection
@section('breadcrumb-active', 'Edit')

@section('content')
    <div class="block" style="display:none;">
        <div class="block-header block-header-default">
			<h3 class="block-title">
                <i class="fa fa-user-circle mr-5 text-muted"></i> User Profile
            </h3>
		</div>
        <div class="block-content">
            <form action="{{ route('users.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('put')
                @include('user.form')
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
	
	<div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                <i class="fa fa-user-circle mr-5 text-muted"></i> User Profile
            </h3>
        </div>
        <div class="block-content">
            <form action="be_pages_generic_profile.edit.php" method="post" onsubmit="return false;">
                <div class="row items-push">
                    <div class="col-lg-3">
                        <p class="text-muted">
                            Your account’s vital info. Your username will be publicly visible.
                        </p>
                    </div>
                    <div class="col-lg-7 offset-lg-1">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-first-name">First Name</label>
                                <input type="text" class="form-control form-control-lg" id="profile-settings-first-name" name="profile-settings-first-name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-last-name">Last Name</label>
                                <input type="text" class="form-control form-control-lg" id="profile-settings-last-name" name="profile-settings-last-name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-username">Username</label>
                                <input type="text" class="form-control form-control-lg" id="profile-settings-username" name="profile-settings-username">
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-title">Title</label>
                                <input type="text" class="form-control form-control-lg" id="profile-settings-title" name="profile-settings-title">
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-email">Email</label>
                                <input type="email" class="form-control form-control-lg" id="profile-settings-email" name="profile-settings-email">
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-12">
                                <label for="signature_text">Signature Text</label>
                                <textarea class="form-control " name="signature_text" id="signature_text" rows="5"></textarea>
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-md-10 col-xl-6">
							<label for="signature_text">Signature Image</label>
                                <div class="push">
                                    <img class="img-avatar" src="assets/media/avatars/avatar15.jpg" alt="">
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input js-custom-file-input-enabled" id="profile-settings-avatar" name="profile-settings-avatar" data-toggle="custom-file-input">
                                    <label class="custom-file-label" for="profile-settings-avatar">Choose new avatar</label>
                                </div>
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-md-10 col-xl-6">
							<label for="signature_text">Profile Image</label>
                                <div class="push">
                                    <img class="img-avatar" src="assets/media/avatars/avatar15.jpg" alt="">
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input js-custom-file-input-enabled" id="profile-settings-avatar" name="profile-settings-avatar" data-toggle="custom-file-input">
                                    <label class="custom-file-label" for="profile-settings-avatar">Choose new avatar</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
	
	<div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                <i class="fa fa-phone mr-5 text-muted"></i> Contact Information
            </h3>
        </div>
        <div class="block-content">
            <form action="be_pages_generic_profile.edit.php" method="post" onsubmit="return false;">
                <div class="row items-push">
                    <div class="col-lg-3">
                        <p class="text-muted">
                            Change and update your contact information to keep your account updated.
                        </p>
                    </div>
                    <div class="col-lg-7 offset-lg-1">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-cell">Cell Phone</label>
                                <input type="text" class="form-control form-control-lg" id="profile-settings-cell" name="profile-settings-cell">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-home-phone">Home Phone</label>
                                <input type="text" class="form-control form-control-lg" id="profile-settings-home-phone" name="profile-settings-home-phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-work-phone">Work Phone</label>
                                <input type="text" class="form-control form-control-lg" id="profile-settings-work-phone" name="profile-settings-work-phone">
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-work-email">Secondary Email</label>
                                <input type="email" class="form-control form-control-lg" id="profile-settings-work-email" name="profile-settings-work-email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
	
	<div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                <i class="fa fa-asterisk mr-5 text-muted"></i> Change Password
            </h3>
        </div>
        <div class="block-content">
            <form action="be_pages_generic_profile.edit.php" method="post" onsubmit="return false;">
                <div class="row items-push">
                    <div class="col-lg-3">
                        <p class="text-muted">
                            Changing your sign in password is an easy way to keep your account secure.
                        </p>
                    </div>
                    <div class="col-lg-7 offset-lg-1">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-password">Current Password</label>
                                <input type="password" class="form-control form-control-lg" id="profile-settings-password" name="profile-settings-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-password-new">New Password</label>
                                <input type="password" class="form-control form-control-lg" id="profile-settings-password-new" name="profile-settings-password-new">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="profile-settings-password-new-confirm">Confirm New Password</label>
                                <input type="password" class="form-control form-control-lg" id="profile-settings-password-new-confirm" name="profile-settings-password-new-confirm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
