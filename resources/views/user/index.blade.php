@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Users</a>
@endsection
@section('breadcrumb-active', 'Index')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Users List</h3>
            <div class="block-options">
                @include('includes.common.search', ['route' => route('users.index')])
            </div>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                        <th>Title</th>
                        <th>Name</th>
                        <th >Email</th>
                        <th>Cell Phone</th>
                        <th>Home Phone</th>
                        <th>Work Phone</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $user)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td class="text-center">
                                <img class="img-avatar img-avatar48" src="{{ $user->avatar_path }}" alt="{{ $user->name }}">
                            </td>
                            <td class="font-w600">{{ $user->title }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->cell_phone }}</td>
                            <td>{{ $user->fax_phone }}</td>
                            <td>{{ $user->work_phone }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-secondary"
                                       data-toggle="tooltip" title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                        <button data-toggle="modal" data-target="#confirm_user_{{$user->id}}" class="btn btn-sm btn-outline-danger" title="">
                                            <i class="fa fa-trash"></i></button>
                                        @include('includes.modals.confirm', ['model' => 'user', 'route' => route('users.destroy', $user->id), 'form' => true])
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="999" class="text-center">{{ _constant('no_record') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                <div class="pagination justify-content-center">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
