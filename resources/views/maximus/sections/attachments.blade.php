<div class="js-filter" data-numbers="true">
    <div class="p-10 bg-white push">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link active" href="#" data-category-link="all">
                    <i class="fa fa-fw fa-image mr-5"></i> All
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-category-link="type-151">AOB</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-category-link="type-157">ITEL</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-category-link="type-155">Xactimate</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-category-link="type-160">Damage Photos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-category-link="type-162">Others</a>
            </li>
        </ul>
    </div>

    <x-attachments :attachments="$claim->media"/>
</div>
