<div>
    <form action="{{ route('maximus.escalate', $claim->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
            <div class="form-group col-4">
                <label for="">Select Escalator</label>
                <select class="form-control" name="escalator_id" id="escalator_id">
                    @foreach(app('assets')->getEscalators() as $escalator)
                        <option value="{{ $escalator->id }}">{{ $escalator->name }}</option>
                    @endforeach
                </select>
                @include('includes.partials.error', ['field' => 'escalator_id'])
            </div>
            <br><br>

            <div class="form-group col-12">
                <label for="">Select Documents To Send</label>
                <x-checktree.tree>
                    <x-checktree.node title="All Documents" id="all">
                        <x-checktree.node title="Initial Submission" id="ckbx_initial_submission">
                            @foreach($claim->getMedia('documents') as $attachment)
                                <x-checktree.leave :title="$attachment->name" :icon="$attachment->icon"
                                                   :value="$attachment->id"/>
                            @endforeach
                        </x-checktree.node>

                        @if($claim->tempRepair)
                            <x-checktree.node title="Temp Repair Documents" id="ckbx_temp_repair">
                                @foreach($claim->tempRepair->getMedia() as $attachment)
                                    <x-checktree.leave :title="$attachment->name" :icon="$attachment->icon"
                                                       :value="$attachment->id"/>
                                @endforeach
                            </x-checktree.node>
                        @endif

                        @if($claim->submissions->count())
                            <x-checktree.node title="Requested Info Submissions" id="ckbx_submissions">
                                @foreach($claim->submissions as $submission)
                                    @php($attachment = $submission->getFirstMedia())
                                    <x-checktree.leave :title="$attachment->name" :icon="$attachment->icon"
                                                       :value="$attachment->id"/>
                                @endforeach
                            </x-checktree.node>
                        @endif

                        @if($claim->carrierResponses->count())
                            <x-checktree.node title="Carrier Responses" id="ckbx_carrier_responses">
                                @foreach($claim->carrierResponses as $response)
                                    @php($attachment = $response->getFirstMedia('documents'))
                                    <x-checktree.leave :title="$attachment->name" :icon="$attachment->icon"
                                                       :value="$attachment->id"/>
                                @endforeach
                            </x-checktree.node>
                        @endif
                    </x-checktree.node>
                </x-checktree.tree>
            </div>

            <div class="form-group col-12">
                <label>Upload Insurance Policy</label>
                <input type="file" class="bootstrap-pdf-fileinput  form-control @error('documents') is-invalid @enderror"
                       name="document" data-browse-on-zone-click="true">
                @include('includes.partials.error', ['field' => 'document', 'class' => 'd-block'])
            </div>
        </div>
        <div class="form-group text-right">
            <x-modals.trigger target="review_email_{{$escalate_template->id}}" text="Edit Email"/>
            <x-modals.review-odmail :template="$escalate_template"/>
            <button class="btn btn-success btn-sm">Submit</button>
        </div>
    </form>
</div>
