<div class="table-responsive">
    <table class="table table-striped table-vcenter">
        <thead>
        <tr class="text-uppercase">
            <th style="width: 10px">#</th>
            <th>Email</th>
            <th>Action</th>
            <th class="text-center" style="width: 100px;">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($emails_to_resend as $odmail)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td class="font-w600">{{ $odmail->subject }}</td>
                <td class="w-50">
                    @foreach($odmail->recipients as $recipient)
                        <span class="badge badge-success">{{ $recipient->email }}</span>
                    @endforeach
                </td>
                <td class="text-center">
                    <div class="btn-group">
                        <a href="{{ route('emails.odmails.resend', $odmail ) }}"
                           class="btn btn-sm btn-secondary"
                           data-toggle="tooltip" title="Send Again">
                            <i class="fal fa-redo"></i>
                        </a>

                    </div>
                </td>
            </tr>
        @empty
        @endforelse
        </tbody>
    </table>
</div>
