<div class="block border rounded">
    <ul class="nav nav-tabs nav-tabs-block justify-content-end align-items-center" data-toggle="tabs" role="tablist">
        <li class="nav-item mr-auto ml-15">
            <h3 class="block-title mr-15">
                <i class="fal fa-upload"></i>
                Upload Center
            </h3>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#temp_repair">Temp Repair</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#supplement">Supplement</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#correspondence">Record Correspondence</a>
        </li>
    </ul>
    <div class="block-content tab-content">
        <div class="tab-pane active" id="supplement" role="tabpanel">
            <h5 class="font-w400">Submit Info To Carrier</h5>
            <form action="{{ route('maximus.supplement.store', $claim->id) }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="">Document Type</label>
                        <select class="form-control @error('document_type') is-invalid @enderror" name="document_type"
                                id="document_type">
                            <option value="itel">Itel</option>
                            <option value="photos">Photos</option>
                            <option value="other">Other</option>
                        </select>
                        @include('includes.partials.error', ['field' => 'document_type'])
                    </div>
                    <div class="form-group col-6 ">
                        <label for="">Choose File</label>
                        <input type="file" class="form-control @error('quick_measure') is-invalid @enderror"
                               name="quick_measure">
                        @include('includes.partials.error', ['field' => 'quick_measure'])
                    </div>
                    <div class="form-group col-12">
                        <label for="">Notes</label>
                        <textarea name="notes" class="form-control" id="" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-success btn-sm">Submit</button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="correspondence" role="tabpanel">
            <h5 class="font-w400">Record Correspondence</h5>
            <form action="{{ route('maximus.record_correspondence.store', $claim->id) }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="">Document Type</label>
                        <select class="form-control" name="record_document_type" id="record_document_type">
                            <option value="itel">Itel</option>
                            <option value="photos">Photos</option>
                            <option value="other">Other</option>
                        </select>
                        @include('includes.partials.error', ['field' => 'record_document_type'])
                    </div>
                    <div class="form-group col-6 ">
                        <label for="">Choose File</label>
                        <input type="file" class="form-control @error('record_quick_measure') is-invalid @enderror"
                               name="record_quick_measure">
                        @include('includes.partials.error', ['field' => 'record_quick_measure'])
                    </div>
                    <div class="form-group col-12">
                        <label for="">Notes</label>
                        <textarea name="record_notes" class="form-control" id="" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-success btn-sm">Submit</button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="temp_repair" role="tabpanel">
            <h5 class="font-w400">Temp Repair</h5>
            @if(!$claim->tempRepair)
                <form action="{{ route('maximus.temp_repair.store', $claim->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Temp Repair Invoice Number:</label>
                            <input type="text" class="form-control @error('invoice_number') is-invalid @enderror"
                                   name="invoice_number">
                            @include('includes.partials.error', ['field' => 'invoice_number'])
                        </div>

                        <div class="form-group col-6">
                            <label>Temp Repair Invoice Amount:</label>
                            <input type="number" class="form-control @error('invoice_amount') is-invalid @enderror"
                                   name="invoice_amount">
                            @include('includes.partials.error', ['field' => 'invoice_amount'])
                        </div>

                        <div class="form-group col-12">
                            <label>Upload Temp Repair Invoice:</label>
                            <input type="file" class="form-control @error('invoice') is-invalid @enderror"
                                   name="invoice">
                            @include('includes.partials.error', ['field' => 'invoice'])
                        </div>

                        <div class="form-group col-12">
                            <label>Upload Photos of Damage:</label>
                            <input type="file"
                                   class="bootstrap-fileinput  form-control @error('photos') is-invalid @enderror"
                                   multiple name="photos[]" data-browse-on-zone-click="true">
                            @include('includes.partials.error', ['field' => 'photos', 'class' => 'd-block'])
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button class="btn btn-success btn-sm">Submit</button>
                    </div>
                </form>
            @else
                <p class="text-success">Temp Repair Already Uploaded</p>
            @endif
        </div>
    </div>
</div>
