<div class="block border rounded">
    <ul class="nav nav-tabs nav-tabs-block justify-content-end align-items-center" data-toggle="tabs" role="tablist">
        <li class="nav-item mr-auto ml-15">
            <h3 class="block-title mr-15">
                <i class="fal fa-wrench"></i>
                Action Center
            </h3>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#actions">Actions</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#history">History</a>
        </li>
    </ul>
    <div class="block-content tab-content">
        <div class="tab-pane active" id="actions" role="tabpanel">
            <h5 class="font-w400">Update Info</h5>
            <form action="{{ route('maximus.claims.update', $claim->id) }}" method="POST">
                @csrf
                <div class="form-group ">
                    <label for="project_description">Status:</label>
                    <select name="status_id" class="form-control" id="status">
                        @foreach(app('constants')->claimStatuses as $status)
                            <option value="{{ $status->id }}" {{ $status->id == $claim->status_id ? 'selected' : '' }}>{{ $status->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group ">
                    <label for="project_description">Estimator Description:</label>
                    <textarea name="description" id="project_description" rows="5" class="form-control">{{ $claim->description }}</textarea>
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-success btn-sm">Submit</button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="history" role="tabpanel" >
            <div data-toggle="slimscroll" data-height="300px" data-always-visible="true">
                <h5 class="font-w400">Status History</h5>
                <ul class="list list-activity">
                    @forelse($claim->statusLogs as $log)
                        <li>
                            <i class="fal fa-history text-success" title="{{ $log->created_at->format('M d, Y h:i A') }}"></i>
                            <div class="font-w600">Status Changed to {{ $log->status->name }}</div>
                            <div>
                                By <a href="javascript:void(0)">{{ $log->user->name }}</a>
                            </div>
                            <div class="font-size-xs text-muted" title="{{ $log->created_at->format('M d, Y h:i A') }}">
                                {{ $log->created_at->diffForHumans() }}
                            </div>
                        </li>
                    @empty
                        <li>No History</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div>
