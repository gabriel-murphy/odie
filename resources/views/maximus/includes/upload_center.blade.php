<div class="block border rounded">
    <ul class="nav nav-tabs nav-tabs-block justify-content-end align-items-center" data-toggle="tabs" role="tablist">
        <li class="nav-item mr-auto ml-15">
            <h3 class="block-title mr-15">
                <i class="fal fa-upload"></i>
                Carrier Action Center
            </h3>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="#temp_repair">Temp Repair</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#supplement">Submit Info</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#correspondence">Record Correspondence</a>
        </li>
    </ul>
    <div class="block-content tab-content">
        <div class="tab-pane active" id="temp_repair" role="tabpanel">
            <h5 class="font-w400">Temp Repair</h5>
            @if(!$claim->tempRepair)
                <form action="{{ route('maximus.temp_repair.store', $claim->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Invoice Number:</label>
                            <input type="text" class="form-control @error('invoice_number') is-invalid @enderror"
                                   name="invoice_number">
                            @include('includes.partials.error', ['field' => 'invoice_number'])
                        </div>

                        <div class="form-group col-6">
                            <label>Invoice Amount:</label>
                            <input type="number" class="form-control @error('invoice_amount') is-invalid @enderror"
                                   name="invoice_amount">
                            @include('includes.partials.error', ['field' => 'invoice_amount'])
                        </div>

                        <div class="form-group col-12">
                            <label>Upload Invoice:</label>
                            <input type="file" class="form-control @error('invoice') is-invalid @enderror"
                                   name="invoice">
                            @include('includes.partials.error', ['field' => 'invoice'])
                        </div>

                        <div class="form-group col-12">
                            <label>Upload Photos of Damage:</label>
                            <input type="file"
                                   class="bootstrap-fileinput  form-control @error('photos') is-invalid @enderror"
                                   multiple name="photos[]" data-browse-on-zone-click="true">
                            @include('includes.partials.error', ['field' => 'photos', 'class' => 'd-block'])
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <x-modals.trigger target="review_email_{{$temp_repair_template->id}}" text="Edit Email"/>
                        <x-modals.review-odmail :template="$temp_repair_template"/>
                        <button class="btn btn-success btn-sm">Submit</button>
                    </div>
                </form>
            @else
                <p class="text-success">Temp Repair Already Uploaded</p>
            @endif
        </div>
        <div class="tab-pane " id="supplement" role="tabpanel">
            <h5 class="font-w400">Submit Info To Carrier</h5>
            <form action="{{ route('maximus.supplement.store', $claim->id) }}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-12">
                        <label for="">Document Type</label>
                        <select class="form-control @error('type_id') is-invalid @enderror" name="type_id"
                                id="type_id">
                            @foreach(app('constants')->requestedInfoTypes() as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                        @include('includes.partials.error', ['field' => 'type_id'])
                    </div>
                    <div class="form-group col-12">
                        <label>Upload Documents(s)</label>
                        <input type="file"
                               class="bootstrap-pdf-fileinput  form-control @error('documents') is-invalid @enderror"
                               name="document" data-browse-on-zone-click="true">
                        @include('includes.partials.error', ['field' => 'document', 'class' => 'd-block'])
                    </div>
                    <div class="form-group col-12">
                        <label for="">Notes</label>
                        <textarea name="notes" class="form-control" id="" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group text-right">
                    <x-modals.trigger target="review_email_{{$info_submission_template->id}}" text="Edit Email"/>
                    <x-modals.review-odmail :template="$info_submission_template"/>
                    <button class="btn btn-success btn-sm">Submit</button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="correspondence" role="tabpanel">
            <h5 class="font-w400">Record Correspondence</h5>
            <form action="{{ route('maximus.record_correspondence.store', $claim->id) }}" method="POST"
                  enctype="multipart/form-data">
                @csrf

                <correspondence-fields
                    :response_types="{{ json_encode(app('constants')->carrierResponseTypes($claim)) }}" :_errors="{{ $errors }}"
                    :insurance_coverage_types="{{ json_encode(app('constants')->groupConstantsResponse(7)) }}"
                    :coverage_denial_reasons="{{ json_encode(app('constants')->groupConstantsResponse(8)) }}" >
                </correspondence-fields>

                <div class="form-row">
                    <div class="form-group col-6">
                        <label>Dated:</label>
                        <input type="text"
                               class="js-flatpickr bg-white form-control @error('dated_on') is-invalid @enderror"
                               name="dated_on" placeholder="Select Date" data-date-format="M d, Y"
                               data-max-date="today">
                        @include('includes.partials.error', ['field' => 'dated_on'])
                    </div>

                    <div class="form-group col-6">
                        <label>Received:</label>
                        <input type="text"
                               class="js-flatpickr bg-white form-control @error('received_on') is-invalid @enderror"
                               name="received_on" placeholder="Select Date" data-date-format="M d, Y"
                               data-max-date="today">
                        @include('includes.partials.error', ['field' => 'received_on'])
                    </div>

                    <div class="form-group col-12">
                        <label for="">Notes</label>
                        <textarea name="notes" class="form-control" id="" rows="3"></textarea>
                    </div>
                    <div class="form-group col-12">
                        <label>Upload Documents(s) Received</label>
                        <input type="file"
                               class="bootstrap-pdf-fileinput  form-control @error('documents') is-invalid @enderror"
                               name="document" data-browse-on-zone-click="true">
                        @include('includes.partials.error', ['field' => 'document', 'class' => 'd-block'])
                    </div>
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-success btn-sm">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
