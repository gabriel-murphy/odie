<!DOCTYPE html>
<html>
<head>
    <title>Maximus Photos</title>
    <style>
        td{
            background: #f0f0f0;
            padding: 10px;
            border: 1px solid #d3d3d3;
        }
    </style>
</head>

<body>
<h5>Check the photos below for damage</h5>

<div style="text-align: center; width: 100%">
    <table width="100%" style="table-layout: fixed">
        <tr>
            <td width="50%"><img src="{{ ($photos[0]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
            <td width="50%"><img src="{{ ($photos[1]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
        </tr>
        <tr>
            <td width="50%"><img src="{{ ($photos[2]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
            <td width="50%"><img src="{{ ($photos[3]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
        </tr>
    </table>
    <div style="page-break-before: always;"></div>
    <table width="100%" style="table-layout: fixed">
        <tr>
            <td width="50%"><img src="{{ ($photos[4]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
            <td width="50%"><img src="{{ ($photos[5]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
        </tr>
        <tr>
            <td width="50%"><img src="{{ ($photos[6]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
            <td width="50%"><img src="{{ ($photos[7]) }}" style="width: 100%;height: 500px;object-fit: contain"></td>
        </tr>
    </table>
</div>



</body>
</html>
