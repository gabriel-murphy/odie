@extends('layouts.backend')
@section('styles')
    <link href="{{ asset('js/plugins/image-swiper/swiper.min.css') }}" rel='stylesheet'>
    <link href="{{ asset('js/plugins/magnific-popup/magnific-popup.css') }}" rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="{{asset('js/plugins/summernote/summernote.css')}}">

    <link href="{{ asset('js/plugins/bootstrap-fileinput/css/fileinput.css') }}" media="all" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('js/plugins/bootstrap-fileinput//themes/explorer-fas/theme.css') }}" media="all"
          rel="stylesheet" type="text/css"/>

    <style type="text/css">
        .swiper-container {
            width: 100%;
            height: 300px;
            margin-left: auto;
            margin-right: auto;
        }

        .swiper-slide {
            background-size: cover;
            background-position: center;
        }

        .swiper-slide a {
            opacity: 0;
            z-index: 100;
            width: 100%;
            height: 100%;
            position: absolute;
            cursor: pointer;
        }

        .swiper-slide a:hover {
            opacity: 0;
        }


        .swiper-slide p {
            display: none;
        }

        .swiper-slide:hover p {
            font-size: 15pt;
            position: relative;
            margin: 25% 35%;
            max-width: 200px;
            color: black;
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .swiper-wrapper div.swiper-slide {
            cursor: pointer;
        }

        .gallery-top {
            height: 80%;
            width: 100%;
        }

        .gallery-thumbs {
            height: 20%;
            box-sizing: border-box;
            padding: 10px 0;
        }

        .gallery-thumbs .swiper-slide {
            /*width: 10% !important;*/
            height: 100%;
            opacity: 0.4;
        }

        .gallery-thumbs .swiper-slide-thumb-active {
            opacity: 1;
        }

    </style>
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/checktree.css') }}">
@endsection

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/customers">Claim</a>
@endsection

@section('breadcrumb-active', 'Show')

@section('content')
    <div class="block">
        <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#details">Details</a></li>
            @if($claim->status_id == 29)
                <li class="nav-item"><a class="nav-link" href="#escalate">Escalate</a></li>
            @endif
            <li class="nav-item"><a class="nav-link" href="#resend">Send Again</a></li>
            <li class="nav-item"><a class="nav-link" href="#timeline">Timeline</a></li>
            <li class="nav-item"><a class="nav-link" href="#attachments">Attachments</a></li>
            <li class="nav-item"><a class="nav-link" href="#attachments">Notes</a></li>
            <li class="nav-item"><a class="nav-link" href="#emails">Email History</a></li>
        </ul>
        <div class="block-content tab-content">
            <div class="tab-pane active" id="details" role="tabpanel">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="block border rounded">
                            <div class="block-header">
                                <h3 class="block-title text-primary">
                                    <i class="si si-home font-w600 fa-fw"></i>
                                    Property Damage Photos
                                </h3>
                            </div>
                            <div class="block-content">
                                <div class="property-detail">
                                    <div style="height:290px">
                                        <div class="swiper-container gallery-top">
                                            <div class="swiper-wrapper js-gallery">
                                                @foreach($claim->getMedia('damage-photos') as $photo)
                                                    <div class="swiper-slide animated fadeIn"
                                                         style="background-image:url({{ _storage_url($photo->getUrl()) }})">
                                                        <a class="img-link img-link-zoom-in img-thumb img-lightbox"
                                                           href="{{ $photo->getUrl() }}">
                                                        </a>
                                                        <span class="bg"></span>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="swiper-button-next swiper-button-white"></div>
                                            <div class="swiper-button-prev swiper-button-white"></div>
                                        </div>
                                        <div class="swiper-container gallery-thumbs">
                                            <div class="swiper-wrapper">
                                                @foreach($claim->getMedia('damage-photos') as $photo)
                                                    <div class="swiper-slide"
                                                         style="background-image:url({{ $photo->getUrl() }})"></div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content">
                                <p class="font-weight-bold">
                                    <b class="font-w700" style="margin-right:20px"><i
                                            class="si font-w600 fa fa-map-marker mr-5"></i></b>
                                    {{ $claim->property->full_address }}
                                </p>
                                <hr>
                                <table>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Roof Age:</th>
                                        <td>12 Years</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <street-viewer
                            :lat="{{ $claim->property->address->latitude }}"
                            :lng="{{ $claim->property->address->longitude }}">
                        </street-viewer>
                    </div>
                    <div class="col-xl-8">
                        <div class="block rounded border">
                            <div class="block-header">
                                <h3 class="block-title text-primary">
                                    <i class="fa fa-file-o font-w600 fa-fw"></i>
                                    Claim #{{ $claim->id }} Details
                                </h3>
                                <div class="border rounded px-4 py-1">
                                    @if($claim->tempRepair)
                                        <i class="fal fa-house-damage" title="Temp Repair Submitted"
                                           data-toggle="tooltip"></i>
                                    @endif
                                    @if($claim->carrier_waiting_days)
                                        <i class="fal fa-exclamation-circle ml-2 text-danger"
                                           title="Claim waiting for response for {{ $claim->carrier_waiting_days }} days now"
                                           data-toggle="tooltip"></i>
                                    @endif
                                </div>
                            </div>
                            <div class="block-content">
                                <table class="table table-bordered ">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Carrier:</th>
                                        <td>{{ $claim->insuranceCarrier->name }}</td>
                                        <th scope="row">Policy #:</th>
                                        <td>{{ $claim->policy_number }}</td>
                                        <th scope="row">Claim #:</th>
                                        <td>{{ $claim->claim_number }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-6">
                                        <table class="table">
                                            <tbody>
                                            <tr class="tr-bt-0">
                                                <th scope="row">Policy Holder:</th>
                                                <td>{{ $claim->customer->name }}</td>
                                            </tr>
                                            @if($claim->customer->secondary_first_name)
                                                <tr>
                                                    <th scope="row">Second Holder:</th>
                                                    <td>{{ $claim->customer->secondary_name }}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <th scope="row">Signed On:</th>
                                                <td>{{ $claim->aob_signed_on }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Submitted:</th>
                                                <td>
                                                    {{ $claim->submitted_in_days }}
                                                    @if($claim->is_overdue)
                                                        <span class="ml-2" title="Acknowledgement date is overdue" data-toggle="tooltip">
                                                            <i class="far fa-exclamation-circle text-danger"></i>
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Acknowledged:</th>
                                                <td>[ Alert if > 15 Days or Date ]</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Status:</th>
                                                <td>{{ $claim->status->name }}</td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-6">
                                        <table class="table">
                                            <tbody>
                                            <tr class="tr-bt-0">
                                                <th scope="row">Estimator:</th>
                                                <td>{{ $claim->estimator->name }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Submitted By:</th>
                                                <td>{{ $claim->user->name }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Xactimate Amount:</th>
                                                <td>@usd($claim->xactimate_amount)</td>
                                            </tr>
                                            @if($claim->desk_adjuster_id)
                                                <tr>
                                                    <th scope="row">Desk Adjuster:</th>
                                                    <td>
                                                        {{ $claim->deskAdjuster->name }}<br>
                                                        {{ $claim->deskAdjuster->email }}<br>
                                                        <span
                                                            class="text-primary">{{ $claim->deskAdjuster->phone }}</span>
                                                    </td>
                                                </tr>
                                            @endif
                                            @if($claim->field_adjuster_id)
                                                <tr>
                                                    <th scope="row">Field Adjuster:</th>
                                                    <td>
                                                        {{ $claim->fieldAdjuster->name }}<br>
                                                        {{ $claim->fieldAdjuster->email }}<br>
                                                        <span
                                                            class="text-primary">{{ $claim->fieldAdjuster->phone }}</span>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('maximus.includes.action_center')
                        @if($claim->status_id < App\MaximusClaim::DECIDED)
                            @include('maximus.includes.upload_center')
                        @endif
                    </div>
                </div>
            </div>
            @if($claim->status_id == 29)
                <div class="tab-pane" id="escalate" role="tabpanel">
                    @include('maximus.sections.escalate')
                </div>
            @endif
            <div class="tab-pane" id="resend" role="tabpanel">
                @include('maximus.sections.resend')
            </div>
            <div class="tab-pane" id="attachments" role="tabpanel">
                @include('maximus.sections.attachments')
            </div>
            <div class="tab-pane" id="timeline" role="tabpanel">
                Timeline
            </div>
            <div class="tab-pane" id="timeline" role="tabpanel">
                Notes
            </div>
            <div class="tab-pane" id="emails" role="tabpanel">
                @include('maximus.sections.emails')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/image-swiper/swiper.min.js') }}"></script>
    <script src="{{ asset('js/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="{{ asset('js/checktree.js') }}"></script>
    <script>
        jQuery(function () {
            Codebase.helpers(['magnific-popup', 'slimscroll', 'content-filter', 'flatpickr', 'summernote']);
        });
    </script>

    <script>
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            slidesPerView: 4,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });
        var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: galleryThumbs
            }
        });
    </script>
    @include('includes.common.bootstrap-fileinput')
@endsection
