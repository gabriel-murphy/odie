@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('js/plugins/flatpickr/flatpickr.min.css') }}">

    <link href="{{ asset('js/plugins/bootstrap-fileinput/css/fileinput.css') }}" media="all" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('js/plugins/bootstrap-fileinput//themes/explorer-fas/theme.css') }}" media="all"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('js/plugins/jquery-tags-input/jquery.tagsinput.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('js/plugins/summernote/summernote.css')}}">
    <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            height: 22px;
            line-height: 22px;
            color: #fff;
            font-size: 13px;
            font-weight: 600;
            background-color: #3f9ce8;
            border: none;
            border-radius: 3px;
        }
    </style>
@endsection

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Maximus</a>
@endsection
@section('breadcrumb-active', 'AOB')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <ul class="nav nav-tabs nav-tabs-block  justify-content-end align-items-center" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link {{ _active_tab('step', 'customer', true) }}"
                       href="#customer">Customer</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ _active_tab('step', 'property') }}"
                       href="#property">Property</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ _active_tab('step', 'insurance') }}"
                       href="#insurance">Insurance</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ _active_tab('step', 'documents') }}"
                       href="#documents">Documents</a>
                </li>
            </ul>
        </div>
        <div class="block-content">
            @include('includes.partials.errors')
            {{--<form action="{{ route('maximus.claims.store') }}" method="POST" enctype="multipart/form-data">
                @csrf--}}
                @include('maximus.form')

           {{-- </form>--}}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/jquery-tags-input/jquery.tagsinput.min.js') }}"></script>
    <script src="{{asset('js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="{{ asset('js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/plugins/flatpickr/flatpickr.min.js') }}"></script>

    <script>
        jQuery(function () {
            Codebase.helper(['summernote', 'flatpickr', 'select2', 'tags-inputs']);
        });
    </script>
    <script>
        $(document).ready(function () {
            let check_box = "#adjuster_meeting_set";
            let parent_id = "#adjuster_meeting_at";
            let parent = $(parent_id).parent('.form-group')
            let meeting_set = $(check_box).is(':checked');

            $(check_box).change(function () {
                meeting_set = $(check_box).is(':checked');
                meeting_set ? parent.show() : visible();
            });
            meeting_set ? parent.show() : visible();

            function visible() {
                $(parent_id).val('');
                parent.hide();
            }
        });
        $(document).on("keydown", ":input:not(textarea):not(:submit)", function(event) {
            if(event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });
    </script>
    @include('includes.common.bootstrap-fileinput')

@endsection
