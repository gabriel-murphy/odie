<div class="form-row pb-3">
    <div class="block-content tab-content">
        <div class="tab-pane {{ _active_tab('step', 'customer', true) }}" id="customer"
             role="tabpanel">
            <form action="{{ route('maximus.claims.step.store', 'customer') }}" method="post">
                @csrf
                <input type="hidden" id="uuid" name="uuid" value="{{ $uuid }}">
                <input type="hidden" name="claim_id" value="{{ request()->query('claim_id') }}">

                <customer-fields :_errors="{{ $errors }}"
                                 :_old="{{ json_encode(Session::getOldInput()) }}">
                </customer-fields>
                <button class="btn btn-primary" type="submit">Next</button>
            </form>
        </div>

        <div class="tab-pane {{ _active_tab('step', 'property') }}" id="property" role="tabpanel">
            <form action="{{ route('maximus.claims.step.store', 'property') }}" method="post">
                @csrf
                <input type="hidden" id="uuid" name="uuid" value="{{ $uuid }}">
                <input type="hidden" name="claim_id" value="{{ request()->query('claim_id') }}">

                <div class="form-row">
                    <div class="form-group col-12">
                        <address-component :_old="{{ json_encode(Session::getOldInput()) }}"></address-component>
                        @include('includes.partials.error', ['field' => 'address.full_address', 'class' => 'd-block'])
                    </div>
                    <mortgage-company :_mortgage-companies="{{ json_encode(\App\MortgageCompany::all()) }}"
                                      :_errors="{{ $errors }}" :_old="{{ json_encode(Session::getOldInput()) }}">
                    </mortgage-company>
                    <div class="form-group col-6">
                        <label for="mortgage_account_number">Mortgage Account Number:</label>
                        <input id="mortgage_account_number" type="text" name="mortgage_account_number"
                               class="form-control @error('mortgage_account_number') is-invalid @enderror"
                               value="{{ old('mortgage_account_number', isset($claim) ? $claim->mortgage_account_number : '') }}">
                        @include('includes.partials.error', ['field' => 'mortgage_account_number'])
                    </div>
                    <div class="form-group col-12 my-4">
                        <div class="form-check d-inline-block w-25">
                            <input class="form-check-input  @error('temp_repair_needed') is-invalid @enderror"
                                   type="checkbox"
                                   id="temp_repair_needed" name="temp_repair_needed" value="1">
                            <label class="form-check-label" for="temp_repair_needed">
                                Temp Repair Needed?
                            </label>
                            @include('includes.partials.error', ['field' => 'temp_repair_needed',])
                        </div>
                        <div class="form-check d-inline-block">
                            <input class="form-check-input" type="checkbox" id="adjuster_meeting_set"
                                   name="adjuster_meeting_set"
                                   value="0" {{ old('adjuster_meeting_at')  ? 'checked' : '' }}>
                            <label class="form-check-label" for="adjuster_meeting_set">
                                Adjuster Meeting Scheduled?
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-12" style="display: none;">
                        <label for="adjuster_meeting_at">Adjuster Meeting Scheduled For:</label>
                        <input type="text"
                               class="js-flatpickr form-control bg-white @error('adjuster_meeting_at') is-invalid @enderror"
                               id="adjuster_meeting_at" name="adjuster_meeting_at" data-enable-time="true"
                               data-min-date="today"
                               value="{{ old('adjuster_meeting_at', isset($claim) ? $claim->adjuster_meeting_at : '') }}">
                        @include('includes.partials.error', ['field' => 'adjuster_meeting_at',])
                    </div>
                    <div class="form-group col-6">
                        <label for="minimum_damaged_units">Minimum Damage Units:</label>
                        <input id="minimum_damaged_units" type="number" name="minimum_damaged_units" min="0"
                               class="form-control @error('minimum_damaged_units') is-invalid @enderror"
                               value="{{ old('minimum_damaged_units', isset($claim) ? $claim->minimum_damaged_units : '') }}">
                        @include('includes.partials.error', ['field' => 'minimum_damaged_units'])
                    </div>
                    <div class="form-group col-6">
                        <label for="xactimate_amount">Xactimate Amount:</label>
                        <input id="xactimate_amount" type="number" name="xactimate_amount" min="0"
                               class="form-control @error('xactimate_amount') is-invalid @enderror"
                               value="{{ old('xactimate_amount', isset($claim) ? $claim->xactimate_amount : '') }}">
                        @include('includes.partials.error', ['field' => 'xactimate_amount'])
                    </div>
                    <div class="form-group col-12">
                        <label for="project_description">Estimator Description:</label>
                        <textarea name="description" id="project_description" rows="5"
                                  class="form-control @error('description') is-invalid @enderror">{{ old('description') }}
                    </textarea>
                        @include('includes.partials.error', ['field' => 'description'])
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Next</button>
            </form>
        </div>

        <div class="tab-pane {{ _active_tab('step', 'insurance') }}" id="insurance"
             role="tabpanel">
            <form action="{{ route('maximus.claims.step.store', 'claim') }}" method="post">
                @csrf
                <input type="hidden" id="uuid" name="uuid" value="{{ $uuid }}">
                <input type="hidden" name="claim_id" value="{{ request()->query('claim_id') }}">

                <div class="form-row">
                    <insurance-carrier :_insurances="{{ json_encode(\App\InsuranceCarrier::all()) }}"
                                       :_errors="{{ $errors }}" :_old="{{ json_encode(Session::getOldInput()) }}">
                    </insurance-carrier>
                    <div class="form-group col-6">
                        <label for="claim_type_id">Claim Type: </label>
                        <select class="form-control @error('claim_type_id') is-invalid @enderror"
                                id="claim_type_id" name="claim_type_id">
                            <option value="">Select</option>
                            @forelse(app('constants')->groupConstantsResponse(28) as $type)
                                <option value="{{ $type->id }}"
                                    {{ (collect(old('claim_type_id'))->contains($type->id)) ? 'selected':'' }}>
                                    {{ $type->name }}
                                </option>
                            @empty
                                <option>not claim type exist</option>
                            @endforelse
                        </select>
                        @include('includes.partials.error', ['field' => 'claim_type_id'])
                    </div>
                    <div class="form-group col-6">
                        <label for="claim_number">Claim Number:</label>
                        <input id="claim_number" type="text" name="claim_number"
                               class="form-control @error('claim_number') is-invalid @enderror"
                               value="{{ old('claim_number', isset($claim) ? $claim->claim_number : '') }}">
                        @include('includes.partials.error', ['field' => 'claim_number'])
                    </div>
                    <div class="form-group col-6">
                        <label for="policy_number">Policy Number:</label>
                        <input id="policy_number" type="text" name="policy_number"
                               class="form-control @error('policy_number') is-invalid @enderror"
                               value="{{ old('policy_number', isset($claim) ? $claim->policy_number : '') }}">
                        @include('includes.partials.error', ['field' => 'policy_number'])
                    </div>
                    <div class="form-group col-6">
                        <label for="date_of_loss">Date Of Loss:</label>
                        <input type="text"
                               class="js-flatpickr form-control bg-white @error('date_of_loss') is-invalid @enderror"
                               id="date_of_loss" name="date_of_loss" placeholder="Select Date" data-date-format="M d, Y"
                               data-max-date="today"
                               value="{{ old('date_of_loss', isset($claim) ? $claim->date_of_loss : 'Sep 10, 2017') }}">
                        @include('includes.partials.error', ['field' => 'date_of_loss'])
                    </div>

                </div>
                <button class="btn btn-primary" type="submit">Next</button>
            </form>
        </div>

        <div class="tab-pane {{ _active_tab('step', 'documents') }}" id="documents" role="tabpanel">
            <form action="{{ route('maximus.claims.step.store', 'documents') }}" method="post">
                @csrf
                <input type="hidden" id="uuid" name="uuid" value="{{ $uuid }}">
                <input type="hidden" name="claim_id" value="{{ request()->query('claim_id') }}">
                <div class="form-row">
                    <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active @error('aob') border-danger @enderror" href="#aob">Upload AoB</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('xactimate') border-danger @enderror" href="#xactimate">Upload
                                Xactimate</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('itel') border-danger @enderror" href="#itel">Upload Itel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('ariel') border-danger @enderror" href="#ariel">Upload Roof
                                Measurements</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('declaration') border-danger @enderror" href="#declaration">Upload
                                Declaration
                                Page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('photos') border-danger @enderror" href="#photos">Top Damage
                                Photos</a>
                        </li>
                    </ul>

                    {{--<ul class="nav nav-tabs nav-tabs-block  justify-content-end align-items-center" data-toggle="tabs"
                        role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active @error('aob') border-danger @enderror" href="#aob">Upload AoB</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('xactimate') border-danger @enderror" href="#xactimate">Upload
                                Xactimate</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('itel') border-danger @enderror" href="#itel">Upload Itel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('ariel') border-danger @enderror" href="#ariel">Upload Roof
                                Measurements</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('declaration') border-danger @enderror" href="#declaration">Upload
                                Declaration
                                Page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @error('photos') border-danger @enderror" href="#photos">Top Damage
                                Photos</a>
                        </li>

                    </ul>--}}
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="aob" role="tabpanel">
                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="estimator_id">Select Estimator: </label>
                                    <select class="form-control @error('estimator_id') is-invalid @enderror"
                                            id="estimator_id" name="estimator_id">
                                        <option value="">Select</option>
                                        @forelse(\App\User::role('estimator')->get() as $user)
                                            <option value="{{ $user->id }}"
                                                {{ (collect(old('estimator_id'))->contains($user->id)) ? 'selected':'' }}>
                                                {{ $user->name }}
                                            </option>
                                        @empty
                                            <option>No Estimators Found</option>
                                        @endforelse
                                    </select>
                                    @include('includes.partials.error', ['field' => 'estimator_id'])
                                </div>
                                <div class="form-group col-6">
                                    <label for="aob_signed_on">AoB Signature Date:</label>
                                    <input type="text"
                                           class="js-flatpickr form-control bg-white @error('aob_signed_on') is-invalid @enderror"
                                           id="aob_signed_on" name="aob_signed_on" placeholder="Select Date"
                                           data-date-format="M d, Y"
                                           data-max-date="today"
                                           value="{{ old('aob_signed_on', isset($claim) ? $claim->aob_signed_on : '') }}">
                                    @include('includes.partials.error', ['field' => 'aob_signed_on'])
                                </div>
                            </div>
                            <label class="font-italic font-size-xs text-muted">Upload AoB: &nbsp;Please note the AoB
                                must be
                                signed by
                                the policy holder(s) and {{ auth()->user()->name }}.</label>
                            <input type="file" id="aob_file"
                                   class="bootstrap-pdf-fileinput form-control @error('aob') is-invalid @enderror"
                                   name="aob" data-browse-on-zone-click="true" data-file="aob">
                            @include('includes.partials.error', ['field' => 'aob', 'class' => 'd-block'])
                        </div>

                        <div class="tab-pane" id="xactimate" role="tabpanel">
                            <label class="font-italic font-size-xs text-muted">Upload the Xactimate report and confirm
                                total
                                amount in
                                the form above.</label>
                            <input type="file"
                                   class="bootstrap-pdf-fileinput form-control @error('xactimate') is-invalid @enderror"
                                   name="xactimate" data-browse-on-zone-click="true">
                            @include('includes.partials.error', ['field' => 'xactimate', 'class' => 'd-block'])
                        </div>

                        <div class="tab-pane" id="itel" role="tabpanel">
                            <label class="font-italic font-size-xs text-muted">Upload iTEL report.</label>
                            <input type="file"
                                   class="bootstrap-pdf-fileinput form-control @error('itel') is-invalid @enderror"
                                   name="itel" data-browse-on-zone-click="true">
                            @include('includes.partials.error', ['field' => 'itel', 'class' => 'd-block'])
                        </div>

                        <div class="tab-pane" id="ariel" role="tabpanel">
                            <label class="font-italic font-size-xs text-muted">Upload roof measurement report and
                                specify
                                reporting
                                firm.</label>
                            <input type="file"
                                   class="bootstrap-pdf-fileinput form-control @error('ariel') is-invalid @enderror"
                                   name="ariel" data-browse-on-zone-click="true">
                            @include('includes.partials.error', ['field' => 'ariel', 'class' => 'd-block'])
                        </div>

                        <div class="tab-pane" id="declaration" role="tabpanel">
                            <label class="font-italic font-size-xs text-muted">Upload Declaration Page if you obtained
                                it
                                from the
                                policy holder(s).</label>
                            <input type="file"
                                   class="bootstrap-pdf-fileinput form-control @error('declaration') is-invalid @enderror"
                                   name="declaration" data-browse-on-zone-click="true">
                            @include('includes.partials.error', ['field' => 'declaration' , 'class' => 'd-block'])
                        </div>

                        <div class="tab-pane" id="photos" role="tabpanel">
                            <label class="font-italic font-size-xs text-muted">Upload eight photographs showing the
                                damage
                                you described
                                in the text area above.</label>
                            <input type="file"
                                   class="bootstrap-fileinput form-control @error('photos') is-invalid @enderror"
                                   multiple
                                   name="photos[]" data-browse-on-zone-click="true">
                            @include('includes.partials.error', ['field' => 'photos', 'class' => 'd-block'])
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <x-modals.trigger target="review_email_{{$template->id}}" text="Edit Email"/>
                    <button type="submit" class="btn btn-success btn-sm">Submit Notification to Carrier</button>
                </div>
                <x-modals.review-odmail :template="$template"/>
            </form>
        </div>
    </div>
</div>

{{--

<div id="customer_create" role="tablist" class="mb-3">
    <div class="block block-bordered block-rounded mb-2 ">
        <div class="block-header" role="tab" id="customer_create_h1">
            <a class="font-w600 collapsed" data-toggle="collapse" data-parent="#customer_create"
               href="#customer_create_q1">
                Create Customer
            </a>
        </div>
        <div id="customer_create_q1" class="collapse show" role="tabpanel" data-parent="#customer_create">
            <div class="block-content px-0">
                <customer-fields :_errors="{{ $errors }}"
                                 :_old="{{ json_encode(Session::getOldInput()) }}"></customer-fields>
            </div>
        </div>
    </div>
</div>
<br>

<div class="form-row">

      <insurance-carrier :_insurances="{{ json_encode(\App\InsuranceCarrier::all()) }}"
                         :_errors="{{ $errors }}" :_old="{{ json_encode(Session::getOldInput()) }}">
      </insurance-carrier>

    <mortgage-company :_mortgage-companies="{{ json_encode(\App\MortgageCompany::all()) }}"
                      :_errors="{{ $errors }}" :_old="{{ json_encode(Session::getOldInput()) }}">
    </mortgage-company>

        <div class="form-group col-6">
            <label for="estimator_id">Select Estimator: </label>
            <select class="form-control @error('estimator_id') is-invalid @enderror"
                    id="estimator_id" name="estimator_id">
                <option value="">Select</option>
                @forelse(\App\User::role('estimator')->get() as $user)
                    <option value="{{ $user->id }}"
                        {{ (collect(old('estimator_id'))->contains($user->id)) ? 'selected':'' }}>
                        {{ $user->name }}
                    </option>
                @empty
                    <option>No Estimators Found</option>
                @endforelse
            </select>
            @include('includes.partials.error', ['field' => 'estimator_id'])
        </div>

        <div class="form-group col-6">
            <label for="claim_type_id">Claim Type: </label>
            <select class="form-control @error('claim_type_id') is-invalid @enderror"
                    id="claim_type_id" name="claim_type_id">
                <option value="">Select</option>
                @forelse(app('constants')->groupConstantsResponse(28) as $type)
                    <option value="{{ $type->id }}"
                        {{ (collect(old('claim_type_id'))->contains($type->id)) ? 'selected':'' }}>
                        {{ $type->name }}
                    </option>
                @empty
                    <option>not claim type exist</option>
                @endforelse
            </select>
            @include('includes.partials.error', ['field' => 'claim_type_id'])
        </div>

        <div class="form-group col-6">
            <label for="minimum_damaged_units">Minimum Damage Units:</label>
            <input id="minimum_damaged_units" type="number" name="minimum_damaged_units" min="0"
                   class="form-control @error('minimum_damaged_units') is-invalid @enderror"
                   value="{{ old('minimum_damaged_units', isset($claim) ? $claim->minimum_damaged_units : '') }}">
            @include('includes.partials.error', ['field' => 'minimum_damaged_units'])
        </div>
    <div class="form-group col-6">
        <label for="xactimate_amount">Xactimate Amount:</label>
        <input id="xactimate_amount" type="number" name="xactimate_amount" min="0"
               class="form-control @error('xactimate_amount') is-invalid @enderror"
               value="{{ old('xactimate_amount', isset($claim) ? $claim->xactimate_amount : '') }}">
        @include('includes.partials.error', ['field' => 'xactimate_amount'])
    </div>
    <div class="form-group col-4">
        <label for="claim_number">Claim Number:</label>
        <input id="claim_number" type="text" name="claim_number"
               class="form-control @error('claim_number') is-invalid @enderror"
               value="{{ old('claim_number', isset($claim) ? $claim->claim_number : '') }}">
        @include('includes.partials.error', ['field' => 'claim_number'])
    </div>
    <div class="form-group col-4">
        <label for="policy_number">Policy Number:</label>
        <input id="policy_number" type="text" name="policy_number"
               class="form-control @error('policy_number') is-invalid @enderror"
               value="{{ old('policy_number', isset($claim) ? $claim->policy_number : '') }}">
        @include('includes.partials.error', ['field' => 'policy_number'])
    </div>
    <div class="form-group col-4">
        <label for="mortgage_account_number">Mortgage Account Number:</label>
        <input id="mortgage_account_number" type="text" name="mortgage_account_number"
               class="form-control @error('mortgage_account_number') is-invalid @enderror"
               value="{{ old('mortgage_account_number', isset($claim) ? $claim->mortgage_account_number : '') }}">
        @include('includes.partials.error', ['field' => 'mortgage_account_number'])
    </div>
        <div class="form-group col-6">
            <label for="date_of_loss">Date Of Loss:</label>
            <input type="text" class="js-flatpickr form-control bg-white @error('date_of_loss') is-invalid @enderror"
                   id="date_of_loss" name="date_of_loss" placeholder="Select Date" data-date-format="M d, Y"
                   data-max-date="today"
                   value="{{ old('date_of_loss', isset($claim) ? $claim->date_of_loss : 'Sep 10, 2017') }}">
            @include('includes.partials.error', ['field' => 'date_of_loss'])
        </div>
        <div class="form-group col-6">
            <label for="aob_signed_on">AoB Signature Date:</label>
            <input type="text" class="js-flatpickr form-control bg-white @error('aob_signed_on') is-invalid @enderror"
                   id="aob_signed_on" name="aob_signed_on" placeholder="Select Date" data-date-format="M d, Y"
                   data-max-date="today"
                   value="{{ old('aob_signed_on', isset($claim) ? $claim->aob_signed_on : '') }}">
            @include('includes.partials.error', ['field' => 'aob_signed_on'])
        </div>

       <div class="form-group col-12 my-4">
           <div class="form-check d-inline-block w-25">
               <input class="form-check-input  @error('temp_repair_needed') is-invalid @enderror" type="checkbox"
                      id="temp_repair_needed" name="temp_repair_needed" value="1">
               <label class="form-check-label" for="temp_repair_needed">
                   Temp Repair Needed?
               </label>
               @include('includes.partials.error', ['field' => 'temp_repair_needed',])
           </div>
           <div class="form-check d-inline-block">
               <input class="form-check-input" type="checkbox" id="adjuster_meeting_set" name="adjuster_meeting_set"
                      value="0" {{ old('adjuster_meeting_at')  ? 'checked' : '' }}>
               <label class="form-check-label" for="adjuster_meeting_set">
                   Adjuster Meeting Scheduled?
               </label>
           </div>
       </div>

       <div class="form-group col-12" style="display: none;">
           <label for="adjuster_meeting_at">Adjuster Meeting Scheduled For:</label>
           <input type="text" class="js-flatpickr form-control bg-white @error('adjuster_meeting_at') is-invalid @enderror"
                  id="adjuster_meeting_at" name="adjuster_meeting_at" data-enable-time="true" data-min-date="today"
                  value="{{ old('adjuster_meeting_at', isset($claim) ? $claim->adjuster_meeting_at : '') }}">
           @include('includes.partials.error', ['field' => 'adjuster_meeting_at',])
       </div>

    <div class="form-group col-12">
        <address-component :_old="{{ json_encode(Session::getOldInput()) }}"></address-component>
        @include('includes.partials.error', ['field' => 'address.full_address', 'class' => 'd-block'])
    </div>

</div>
<div class="form-group">
    <label for="project_description">Estimator Description:</label>
    <textarea name="description" id="project_description" rows="5"
              class="form-control @error('description') is-invalid @enderror">{{ old('description') }}
    </textarea>
    @include('includes.partials.error', ['field' => 'description'])
</div>


<div class="form-row">
    <ul class="nav nav-tabs nav-tabs-block  justify-content-end align-items-center" data-toggle="tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active @error('aob') border-danger @enderror" href="#aob">Upload AoB</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @error('xactimate') border-danger @enderror" href="#xactimate">Upload Xactimate</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @error('itel') border-danger @enderror" href="#itel">Upload Itel</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @error('ariel') border-danger @enderror" href="#ariel">Upload Roof Measurements</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @error('declaration') border-danger @enderror" href="#declaration">Upload Declaration
                Page</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @error('photos') border-danger @enderror" href="#photos">Top Damage Photos</a>
        </li>

    </ul>
    <div class="block-content tab-content">
        <div class="tab-pane active" id="aob" role="tabpanel">
            <label class="font-italic font-size-xs text-muted">Upload AoB: &nbsp;Please note the AoB must be signed by
                the policy holder(s) and {{ auth()->user()->name }}.</label>
            <input type="file" id="aob_file"
                   class="bootstrap-pdf-fileinput form-control @error('aob') is-invalid @enderror"
                   name="aob" data-browse-on-zone-click="true" data-file="aob">
            @include('includes.partials.error', ['field' => 'aob', 'class' => 'd-block'])
        </div>

        <div class="tab-pane" id="xactimate" role="tabpanel">
            <label class="font-italic font-size-xs text-muted">Upload the Xactimate report and confirm total amount in
                the form above.</label>
            <input type="file" class="bootstrap-pdf-fileinput form-control @error('xactimate') is-invalid @enderror"
                   name="xactimate" data-browse-on-zone-click="true">
            @include('includes.partials.error', ['field' => 'xactimate', 'class' => 'd-block'])
        </div>

        <div class="tab-pane" id="itel" role="tabpanel">
            <label class="font-italic font-size-xs text-muted">Upload iTEL report.</label>
            <input type="file" class="bootstrap-pdf-fileinput form-control @error('itel') is-invalid @enderror"
                   name="itel" data-browse-on-zone-click="true">
            @include('includes.partials.error', ['field' => 'itel', 'class' => 'd-block'])
        </div>

        <div class="tab-pane" id="ariel" role="tabpanel">
            <label class="font-italic font-size-xs text-muted">Upload roof measurement report and specify reporting
                firm.</label>
            <input type="file" class="bootstrap-pdf-fileinput form-control @error('ariel') is-invalid @enderror"
                   name="ariel" data-browse-on-zone-click="true">
            @include('includes.partials.error', ['field' => 'ariel', 'class' => 'd-block'])
        </div>

        <div class="tab-pane" id="declaration" role="tabpanel">
            <label class="font-italic font-size-xs text-muted">Upload Declaration Page if you obtained it from the
                policy holder(s).</label>
            <input type="file" class="bootstrap-pdf-fileinput form-control @error('declaration') is-invalid @enderror"
                   name="declaration" data-browse-on-zone-click="true">
            @include('includes.partials.error', ['field' => 'declaration' , 'class' => 'd-block'])
        </div>

        <div class="tab-pane" id="photos" role="tabpanel">
            <label class="font-italic font-size-xs text-muted">Upload eight photographs showing the damage you described
                in the text area above.</label>
            <input type="file" class="bootstrap-fileinput form-control @error('photos') is-invalid @enderror" multiple
                   name="photos[]" data-browse-on-zone-click="true">
            @include('includes.partials.error', ['field' => 'photos', 'class' => 'd-block'])
        </div>
    </div>
</div>
--}}



