@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Maximus</a>
@endsection
@section('breadcrumb-active', 'Claims')

@section('styles')
    <link href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}" rel='stylesheet'>
@endsection

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Assigned Claims Outstanding</h3>
            <div class="block-options">
                {{--@include('includes.common.search', ['route' => route('maximus.claims.index')])--}}
            </div>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter js-dataTable-full"  data-page-length='20' data-order='[[0, "desc"]]'>
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Policy Holder(s)</th>
                        <th>Property Address</th>
                        <th>Status</th>
                        <th>Submitted</th>
                        <th>Estimator</th>
                        <th style="width: 50px">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($claims as $claim)
                        <tr>
                            <td>{{ $claim->id }}</td>
                            <td>
                                {{ $claim->customer->name }}
                                @if($claim->customer->secondary_name)
                                    <br>
                                    {{ $claim->customer->secondary_name }}
                                @endif
                            </td>
                            <td>{{$claim->property? $claim->property->full_address : '' }}</td>
                            <td>{{ $claim->status->name }}</td>
                            <td>@formattedDate($claim->submitted_at)</td>
                            <td>{{ $claim->estimator->name }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('maximus.claims.show', $claim->id ) }}"
                                       class="btn btn-sm btn-secondary"
                                       data-toggle="tooltip" title="Show">
                                        <i class="fad fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="999" class="text-center">{{ app('constants')->constant('no_record') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                {{--<div class="pagination justify-content-center">
                    {{ $claims->links() }}
                </div>--}}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>
@endsection
