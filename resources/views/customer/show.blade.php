@extends('layouts.backend')

@section('styles')
<link href="{{ asset('js/plugins/image-swiper/swiper.min.css') }}" rel='stylesheet'>
<link href="{{ asset('js/plugins/magnific-popup/magnific-popup.css') }}" rel='stylesheet'>

<style type="text/css">
    .property-detail p {
        margin-bottom: 10px;
    }

    .estimate-list div {
        margin-bottom: 7px;
    }

    /* calendar appointment event css */

    .fc-content-col .fc-event-container .fc-content {
        color: white;
        text-align: center;
        padding: 10px;
    }

    .swiper-container {
        width: 100%;
        height: 300px;
        margin-left: auto;
        margin-right: auto;
    }

    .swiper-slide {
        background-size: cover;
        background-position: center;
    }

    .swiper-slide a {
        opacity: 0;
        z-index: 100;
        width: 100%;
        height: 100%;
        position: absolute;
        cursor: pointer;
    }
    .swiper-slide a:hover {
        opacity: 0;
    }
    
    .swiper-slide:hover .bg {
        width: 100%;
        background-color: #1f8ce4;
        height: 100%;
        opacity: 0.7;
        color: #3f9ce8;
        position: absolute;
    }
    
    .swiper-slide p {
        display: none;
    }

    .swiper-slide:hover p {
        font-size: 15pt;
        position: relative;
        margin: 25% 35%;
        max-width: 200px;
        color: #fff;
        display: block;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .swiper-wrapper div.swiper-slide{
        cursor: pointer;
    }

    .gallery-top {
        height: 80%;
        width: 100%;
    }

    .gallery-thumbs {
        height: 20%;
        box-sizing: border-box;
        padding: 10px 0;
    }

    .gallery-thumbs .swiper-slide {
        /*width: 10% !important;*/
        height: 100%;
        opacity: 0.4;
    }

    .gallery-thumbs .swiper-slide-thumb-active {
        opacity: 1;
    }

    .timeline {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
    }

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 50%;
        margin-left: -1.5px;
    }

    .timeline>li {
        margin-bottom: 20px;
        position: relative;
    }

    .timeline>li:before,
    .timeline>li:after {
        content: " ";
        display: table;
    }

    .timeline>li:after {
        clear: both;
    }

    .timeline>li:before,
    .timeline>li:after {
        content: " ";
        display: table;
    }

    .timeline>li:after {
        clear: both;
    }

    .timeline>li>.timeline-panel {
        width: 46%;
        float: left;
        border: 1px solid #d4d4d4;
        border-radius: 2px;
        padding: 20px;
        position: relative;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline>li>.timeline-panel:hover {
        cursor: pointer;
        box-shadow: 0 0 36px #d8dde5;
    }

    .timeline>li>.timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid #ccc;
        border-right: 0 solid #ccc;
        border-bottom: 15px solid transparent;
        content: " ";
    }

    .timeline>li>.timeline-panel:after {
        position: absolute;
        top: 27px;
        right: -14px;
        display: inline-block;
        border-top: 14px solid transparent;
        border-left: 14px solid #fff;
        border-right: 0 solid #fff;
        border-bottom: 14px solid transparent;
        content: " ";
    }

    .timeline>li>.timeline-badge {
        color: #fff;
        width: 50px;
        height: 50px;
        line-height: 50px;
        font-size: 1.4em;
        text-align: center;
        position: absolute;
        top: 16px;
        left: 50%;
        margin-left: -25px;
        background-color: #999999;
        z-index: 100;
        border-top-right-radius: 50%;
        border-top-left-radius: 50%;
        border-bottom-right-radius: 50%;
        border-bottom-left-radius: 50%;
    }

    .timeline>li.timeline-inverted>.timeline-panel {
        float: right;
    }

    .timeline>li.timeline-inverted>.timeline-panel:before {
        border-left-width: 0;
        border-right-width: 15px;
        left: -15px;
        right: auto;
    }

    .timeline>li.timeline-inverted>.timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }

    .timeline-badge.primary {
        background-color: #2e6da4 !important;
    }

    .timeline-badge.success {
        background-color: #3f903f !important;
    }

    .timeline-badge.warning {
        background-color: #f0ad4e !important;
    }

    .timeline-badge.danger {
        background-color: #d9534f !important;
    }

    .timeline-badge.info {
        background-color: #5bc0de !important;
    }

    .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .timeline-body>p,
    .timeline-body>ul {
        margin-bottom: 0;
    }

    .timeline-body>p+p {
        margin-top: 5px;
    }

    @media (max-width: 767px) {
        ul.timeline:before {
            left: 40px;
        }

        ul.timeline>li>.timeline-panel {
            width: calc(100% - 90px);
            width: -moz-calc(100% - 90px);
            width: -webkit-calc(100% - 90px);
        }

        ul.timeline>li>.timeline-badge {
            left: 15px;
            margin-left: 0;
            top: 16px;
        }

        ul.timeline>li>.timeline-panel {
            float: right;
        }

        ul.timeline>li>.timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline>li>.timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
    }
    .property_value p span{
        margin-right:10px;
    }
</style>
@endsection

@section('breadcrumbs')
<a class="breadcrumb-item" href="/customers">Customers</a>
@endsection

@section('breadcrumb-active', ucfirst($project->customer->first_name).' '.ucfirst($project->customer->last_name))

@section('content')
<div class="row">
    <div class="col-6 col-xl-4">
        <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-right mt-15 d-none d-sm-block">
                    <i class="si si-envelope-open fa-2x text-primary-light"></i>
                </div>
                <div class="font-size-h5 font-w600 text-primary" style="text-overflow: ellipsis; white-space: nowrap;overflow: hidden;">{{$project->property->address->street}}, {{$project->property->address->city}}, {{$project->property->address->state}}, {{$project->property->address->zip_code}}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted mt-20">Address</div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-4">
        <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-right mt-15 d-none d-sm-block">
                    <i class="si si-bag fa-2x text-earth-light"></i>
                </div>
                <div class="font-size-h5 font-w600 text-earth">{{$project->phase->name}}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted mt-20">Last Activity</div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-4">
        <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-right d-none d-sm-block">
                    <img src="/images/user/0.png" class="img-avatar" />
                </div>
                <div class="font-size-h5 font-w600">{{$project->estimator->first_name}} {{$project->estimator->last_name}}</div>
                <div class="font-size-sm font-w600 text-uppercase text-muted mt-20">Estimator</div>
            </div>
        </a>
    </div>
    <div class="col-xl-4">
        <div class="block block-themed">
            <div class="block-header">
                <h3 class="block-title">
                    <i class="si si-home font-w600 fa-fw"></i>
                    Property Details
                </h3>
            </div>
            <div class="block-content">
                <div class="property-detail">
                    <div style="height:300px">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper js-gallery">
                                <div class="swiper-slide animated fadeIn" style="background-image:url('/images/landing/chrome-app.png')">
                                    <a class="img-link img-link-zoom-in img-thumb img-lightbox" href="/images/landing/chrome-app.png">
                                    </a>
                                    <span class="bg"></span>
                                    <p>Roof Style 1</p>
                                </div>
                                <div class="swiper-slide animated fadeIn" style="background-image:url('/images/test-roof.jpg')">
                                    <a class="img-link img-link-zoom-in img-thumb img-lightbox" href="/images/test-roof.jpg">
                                    </a>
                                    <span class="bg"></span>
                                    <p>Roof Special Style</p>
                                </div>
                                <div class="swiper-slide animated fadeIn" style="background-image:url('/images/landing/background.jpg')">
                                    <a class="img-link img-link-zoom-in img-thumb img-lightbox" href="/images/landing/background.jpg">
                                    </a>
                                    <span class="bg"></span>
                                    <p>Click For Zoom</p>
                                </div>
                            </div>
                            <div class="swiper-button-next swiper-button-white"></div>
                            <div class="swiper-button-prev swiper-button-white"></div>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url('/images/landing/chrome-app.png')"></div>
                                <div class="swiper-slide" style="background-image:url('/images/test-roof.jpg')"></div>
                                <div class="swiper-slide" style="background-image:url('/images/landing/background.jpg')"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-header">
                <h3 class="block-title">
                    <i class="si si-home font-w600 fa-fw"></i>
                    Property Value
                </h3>
            </div>
            <div class="block-content property_value">
                    <p><span class="badge badge-info">Existing Material</span> {{$project->getCurrentMaterialName()}}</p>
                    <p><span class="badge badge-info">Preferred Material</span> {{$project->getPreferredMaterialName() }}</p>
                    <p><span class="badge badge-info">Classification</span> Residential</p>
                    <p><span class="badge badge-info">Job Type</span>{{$project->getProjectTypeName()}}</p>
                    <p><span class="badge badge-info">Roof Status</span> Old </p>
                    <p><span class="badge badge-info">Age</span> 10 years</p>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="block block-themed">
            <div class="block-header">
                <h3 class="block-title">
                    <i class="si si-users font-w600 fa-fw"></i>
                    Contact Profile
                </h3>
            </div>
            <div class="block-content">
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="si font-w600 fa-fw si-user mr-5"></i>Main Contact</span>{{$project->estimator->first_name}} {{$project->estimator->last_name}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="si font-w600 si-users mr-5"></i>Relationship</span> {{$project->getRelationshipName()}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="fa font-w600 fa-mobile-phone mr-5"></i>Cell Phone</span> {{$customer->cell_phone}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="fa font-w600 fa-volume-control-phone mr-5"></i>Work Phone</span> {{$customer->work_phone}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="fa font-w600 fa-phone mr-5"></i>Home Phone</span> {{$customer->home_phone}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="si font-w600 si-envelope mr-5"></i>Email</span> {{$customer->email}}</p>
                <div style="text-align:center">
                    @foreach ($customer_social as $social)
                        @if ($social['network'] == 'linkedin')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/linkedin-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'twitter')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/twitter-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'instagram')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/instagram-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'youtube')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/youtube-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'facebook')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/facebook-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'github')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/github-color.svg"></a>
                        @endif
                    @endforeach
                </div>
                <hr>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="si font-w600 fa-fw si-user mr-5"></i>Decision Maker</span> {{$project->estimator->first_name}} {{$project->estimator->last_name}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="fa font-w600 fa-mobile-phone mr-5"></i>Cell Phone</span> {{$project->estimator->cell_phone}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="fa font-w600 fa-volume-control-phone mr-5"></i>Work Phone</span> {{$project->estimator->work_phone}}1</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="fa font-w600 fa-phone mr-5"></i>Home Phone</span> {{$project->estimator->home_phone}}</p>
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="si font-w600 si-envelope mr-5"></i>Email</span> {{$project->estimator->email}}</p>
                <div style="text-align:center;margin-bottom:15px;">
                    @foreach ($estimator_social as $social)
                        @if ($social['network'] == 'linkedin')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/linkedin-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'twitter')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/twitter-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'instagram')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/instagram-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'youtube')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/youtube-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'facebook')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/facebook-color.svg"></a>
                        @endif
                        @if ($social['network'] == 'github')
                            <a target="_blank" href="{{$social['url']}}"><img class="social-link-icon" src="/images/social/github-color.svg"></a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="block-header">
                <h3 class="block-title">
                    <i class="si si-info font-w600 fa-fw"></i>
                    Customer Details
                </h3>
            </div>
            <div class="block-content">
                <p><span class="text-primary font-w600" style="margin-right:20px"><i class="si font-w600 fa-fw si-pointer mr-5"></i>Customer Portal:</span> 
                    <a href="#" style="text-decoration: underline;margin-right:15px;">2020-04-20-23.pdf</a>
                    <button type="button" class="btn btn-outline-primary">
                        <i class="fa fa-send mr-5"></i>Send
                    </button>
                </p>
                <div style="display: flex;margin-bottom:15px;">
                    <span class="text-primary font-w600" style="margin:auto;margin-right:20px;white-space:nowrap"><i class="fa font-w600 fa-fw fa-users mr-5"></i>Resend To</span>
                    <select id="lead_source" class="form-control " name="customers">
                        <option value="0">Select here</option>
                        @foreach ($estimators as $estimator)
                            <option value="{{ $estimator->id }}">{{ $estimator->name }}</option>
                        @endforeach
                    </select>
                </div>
                <p style="margin-top:15px;"><span class="text-primary font-w600" style="margin-right:20px;"><i class="si font-w600 si-note mr-5"></i>Notes:</span></p>
                <textarea id="customer_note" style="margin-bottom:23px;margin-top:10px" rows="10" name="" class="form-control">{{$project->customer_notes}}</textarea>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        
        <div class="block block-themed">
            <div class="block-header">
                <h3 class="block-title">
                    <i class="fa fa-file-o font-w600 fa-fw"></i>
                    Job Details
                </h3>
            </div>
            <div class="block-content">
                <div class="property-detail">
                    <p>Tear-Off Foreman: %%Foreman%%</p>
                    <hr>
                    <p class="text-primary font-w600"><i class="fa font-w600 fa-file-pdf-o si-pointer mr-5"></i>Estimates:</p> 
                    <div class="estimate-list">
                        <div>
                            <a href="#" style="padding-left:20px;text-decoration:underline">John / 2020-04-03</a> 
                            <button type="button" class="btn btn-outline-primary" style="padding:5px 10px;margin-left:20px;">
                                <i class="fa fa-send mr-5"></i>Send
                            </button>
                            <br>
                        </div>
                    </div>
                    <hr>
                    <p class="text-primary font-w600"><i class="fa font-w600 fa-th-list si-pointer mr-5"></i>Phase</p> 
                    <select id="lead_source" class="form-control " name="customers">
                        @foreach($phases as $phase)
                            <option
                                {{  old('phase_id', $project ? $project->phase_id : '' ) == $phase->id ? 'selected' : '' }}
                                value="{{$phase->id}}">{{$phase->name}}</option>
                        @endforeach
                    </select>
                    <p style="margin-top:15px;"><span class="text-primary font-w600" style="margin-right:20px;"><i class="fa font-w600 fa-save mr-5"></i>Last Status</span> {{$project->phase->name}}</p>
                    <p style="margin-top:15px;"><span class="text-primary font-w600" style="margin-right:20px;"><i class="si font-w600 si-note mr-5"></i>Job Notes:</span></p>
                    <textarea id="job_note" style="margin-bottom:20px;margin-top:5px" rows="6" name="" class="form-control">{{$project->description}}</textarea>

                </div>
            </div>
            <div class="block-header">
                <h3 class="block-title">
                    <i class="si si-direction font-w600 fa-fw"></i>
                    Customer Activity Feed
                </h3>
            </div>
            <div class="block-content">
                <ul class="list list-timeline list-timeline-modern pull-t">
                    <li>
                        <div class="list-timeline-time">03/20/2020 4:20 pm</div>
                        <i class="list-timeline-icon si si-tag bg-info"></i>
                        <div class="list-timeline-content">
                            <p>Customer profile created in Odie for %Full Name%%</p>
                        </div>
                    </li>
                    <li>
                        <div class="list-timeline-time">03/20/2020 4:20 pm</div>
                        <i class="list-timeline-icon si si-pointer bg-info"></i>
                        <div class="list-timeline-content">
                            <p>Scheduled Estimate for Repair with Marissa Romeo</p>
                        </div>
                    </li>
                    <li>
                        <div class="list-timeline-time">03/20/2020 4:20 pm</div>
                        <i class="list-timeline-icon fa fa-file-pdf-o bg-info"></i>
                        <div class="list-timeline-content">
                            <p>%%Full Name%% assigned to %%Estimator Full Name%%</p>
                        </div>
                    </li>
                    <li>
                        <div class="list-timeline-time">03/20/2020 4:20 pm</div>
                        <i class="list-timeline-icon si si-pencil bg-info"></i>
                        <div class="list-timeline-content">
                            <p>Odie emailed Estimate Appointment Email to %Full Name%%</p>
                        </div>
                    </li>
                    <li>
                        <div class="list-timeline-time">03/20/2020 4:20 pm</div>
                        <i class="list-timeline-icon si si-envelope bg-info"></i>
                        <div class="list-timeline-content">
                            <p>%%Full Name%% opened and viewed Estimate Appointment Email</p>
                        </div>
                    </li>
                    <li>
                        <div class="list-timeline-time">03/20/2020 4:20 pm</div>
                        <i class="list-timeline-icon si si-power bg-info"></i>
                        <div class="list-timeline-content">
                            <p>%%Estimator Full Name%% completed Estimate Appointment</p>
                        </div>
                    </li>
                    <li>
                        <div class="list-timeline-time">03/20/2020 4:20 pm</div>
                        <i class="list-timeline-icon fa fa-dollar bg-info"></i>
                        <div class="list-timeline-content">
                            <p>%%Estimator Full Name%% left notes about %%Full Name%%</p>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="col-xl-12">
        <div class="block block-themed">
            <div class="block-header">
                <h3 class="block-title">
                    <i class="si si-directions font-w600 fa-fw"></i>
                    Customer Timeline
                </h3>
            </div>
            <div class="block-content">
                <ul class="timeline">
                    <li>
                        <div class="timeline-badge"><i class="si si-tag"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Create Project</h4>
                                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11 hours ago by John Doe</small></p>
                            </div>
                            <div class="timeline-body">
                                <p>job created by John</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning"><i class="si si-pointer"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Meeting</h4>
                            </div>
                            <div class="timeline-body">
                                <p>John meets customer.</p>
                                <p>Meet at 10:00 AM - 11:00 AM, 5th March, 2020</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge danger"><i class="fa fa-file-pdf-o"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Send Proposal</h4>
                            </div>
                            <div class="timeline-body">
                                <p>send Estimate Document</p>
                                <p>2020-03-20 3:10 AM</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge info"><i class="si si-pencil"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Customer Signed</h4>
                            </div>
                            <div class="timeline-body">
                                <p>customer signed on estimate document</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Project started</h4>
                            </div>
                            <div class="timeline-body">
                                <p>The work started from 2020-03-20 10:00 AM</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge success"><i class="si si-power"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Project finished</h4>
                            </div>
                            <div class="timeline-body">
                                <p>2020-04-20 10:00 PM</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge success"><i class="fa fa-money"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Project invoice</h4>
                            </div>
                            <div class="timeline-body">
                                <p>10k$</p>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/plugins/image-swiper/swiper.min.js') }}"></script>
<script src="{{ asset('js/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script>jQuery(function(){ Codebase.helpers('magnific-popup'); });</script>
<script>
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });
</script>
@endsection