@extends('layouts.backend')

@section('styles')
<link href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}" rel='stylesheet'>
<style type="text/css">
tr:hover{
    background-color: #eee !important; 
}
</style>
@endsection
@section('breadcrumbs')
<a class="breadcrumb-item" href="javascript:void(0)">Customers</a>
@endsection

@section('breadcrumb-active', 'All')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Customers List</h3>
                </div>
                <div class="block-content">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                        <thead>
                            <tr>
                                <th class="text-center" style="width:25px;">No</th>
                                <th class="text-center">Customer Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center" style="width:40%" >Address</th>
                                <th class="text-center">Created Date</th>
                                <th class="text-center">Profile</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $index = 0; ?>
                            @foreach($projects as $project)
                            <tr>
                                <td class="text-center"><?php $index++; echo $index; ?></td>
                                <td class="text-center font-w600">{{ ucfirst($project->customer->first_name) }} {{ ucfirst($project->customer->last_name) }}</td>
                                <td class="text-center ">{{$project->customer->email}}</td>
                                <td class="text-center">
                                    {{$project->property->address->street}}, {{$project->property->address->city}}, {{$project->property->address->state}}, {{$project->property->address->zip_code}}
                                </td>
                                <td class="text-center ">{{ $project->created_at->format('D M j, Y g:i a') }}</td>
                                <td class="text-center">
                                    <a href="{{ route('customers.show', [$project->customer_id]) }}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="View Customer">
                                        <i class="fa fa-user"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script>
    jQuery(".js-dataTable-full").dataTable({
        columnDefs: [{ orderable: !1, targets: [4] }],
        pageLength: 10,
        lengthMenu: [
            [5, 10, 15, 20],
            [5, 10, 15, 20],
        ],
        autoWidth: !1,
        "oLanguage": {
         "sInfo": "Showing  _START_  to  _END_  of  _TOTAL_  Customers",
         "sInfoEmpty": "Showing  0  to  0  of  0  Customers",
         "sLengthMenu": "Show _MENU_ Customers",
        }
    });
</script>
@endsection
