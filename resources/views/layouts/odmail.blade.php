<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', config('app.name'))</title>

    <style type="text/css">
        @media screen and (max-width: 400px) {
            .outer-table {
                padding: 20px !important;
            }

            .wrapper {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
            }
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">

<div class="wrapper" style="background-color: #EAEAEA;padding-top: 40px;padding-bottom: 40px;">
    <table class="outer-table" cellpadding="0" cellspacing="0" width="100%"
           style="padding:40px;max-width: 600px;margin: 0 auto;background-color: #FFFFFf;">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <img width="130" src="{{ url('images/odmail/odie.png') }}" alt="odie-email.png">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="padding-top: 40px;" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <p style="margin: 0;font-size: 12px;">@yield('content')</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="outer-table" cellpadding="0" cellspacing="0" width="100%"
           style="padding:20px 40px;max-width: 600px;margin: 0 auto;">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td valign="top">
                            <a style="text-decoration: none;color:#0092ff;font-size: 10px;" href="#">
                                DOWNLOAD THE APP
                            </a>
                        </td>
                        <td valign="top" align="right">
                            <p style="margin-top: 0;color:#9e9e9e;font-size: 10px;">Available on Android and iOS</p>
                            <a href="#">
                                <img width="50" src="{{asset('images/odmail/apple.png')}}" alt="apple.png">
                            </a>
                            <a href="#">
                                <img width="50" src="{{asset('images/odmail/playstore.png')}}"
                                     alt="playstore.png">
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="outer-table" cellpadding="0" cellspacing="0" width="100%"
           style="border-top: 1px solid #9e9e9e;padding:20px 40px;max-width: 600px;margin: 0 auto;">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <p style="margin: 0;font-size: 12px;color: #9e9e9e;">
                                This daily newsletter was sent to company@sitename.com from company Name because you subscribed.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="margin: 0;display: inline-block;font-size: 12px;color: #9e9e9e;">
                                Rather not receive our newsletter anymore?
                            </p>
                            <a style="text-decoration: none;display: inline-block;font-size: 12px;color:#000000"
                               href="#">Unsubscribe</a>
                            <p style="margin: 0;display: inline-block;font-size: 12px;color: #9e9e9e;">instantly</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <p style="margin: 0;font-size: 12px;color: #9e9e9e;">Copyright ©2020 Company Name. All
                                Rights Reserved.</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <div>
                                <ul style="list-style: none;">
                                    <li style="display: inline-block;">
                                        <a href="#">
                                            <img width="30" src="{{asset('images/odmail/linkedin.png')}}"
                                                 alt="linkedin.png">
                                        </a>
                                    </li>
                                    <li style="display: inline-block;">
                                        <a href="#">
                                            <img width="30" src="{{asset('images/odmail/youtube.png')}}"
                                                 alt="youtube.png">
                                        </a>
                                    </li>
                                    <li style="display: inline-block;">
                                        <a href="#">
                                            <img width="30" src="{{asset('images/odmail/facebook.png')}}"
                                                 alt="facebook.png">
                                        </a>
                                    </li>
                                    <li style="display: inline-block;">
                                        <a href="#">
                                            <img width="30" src="{{asset('images/odmail/twitter.png')}}"
                                                 alt="twitter.png">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
