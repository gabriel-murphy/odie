<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162024710-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162024710-1');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Welcome {{ Auth::user()->first_name ?? ""}} to Odie</title>
    <!-- Scripts -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <!--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">-->
    <script src="https://kit.fontawesome.com/0b514f50a8.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/vendor/animate.css/animate.css">
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.css">
    <!-- Datatables-->
    <link rel="stylesheet" href="/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="/vendor/datatables.net-keytable-bs/css/keyTable.bootstrap.css">
    <link rel="stylesheet" href="/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.css">

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/theme.css') }}" rel="stylesheet">

</head>
<body>
        <div class="wrapper">
            @include('components.header')
            @include('components.sidebar')
            @yield('content')
        </div>
    <script src="/vendor/js-storage/js.storage.js"></script><!-- SCREENFULL-->
   <script src="/vendor/screenfull/dist/screenfull.js"></script><!-- i18next-->
   <script src="/vendor/i18next/i18next.js"></script>
   <script src="/vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script>
   <script src="/vendor/jquery/dist/jquery.js"></script>
   <script src="/vendor/popper.js/dist/umd/popper.js"></script>
   <script src="/vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- SLIMSCROLL-->
   <script src="/vendor/jquery-slimscroll/jquery.slimscroll.js"></script><!-- SPARKLINE-->
   <script src="/vendor/jquery-sparkline/jquery.sparkline.js"></script><!-- FLOT CHART-->
   <script src="/vendor/flot/jquery.flot.js"></script>
   <script src="/vendor/jquery.flot.tooltip/js/jquery.flot.tooltip.js"></script>
   <script src="/vendor/flot/jquery.flot.resize.js"></script>
   <script src="/vendor/flot/jquery.flot.pie.js"></script>
   <script src="/vendor/flot/jquery.flot.time.js"></script>
   <script src="/vendor/flot/jquery.flot.categories.js"></script>
   <script src="/vendor/flot-spline/js/jquery.flot.spline.js"></script><!-- EASY PIE CHART-->
   <script src="/vendor/easy-pie-chart/dist/jquery.easypiechart.js"></script><!-- MOMENT JS-->
   <script src="/vendor/moment/min/moment-with-locales.js"></script><!-- =============== APP SCRIPTS ===============-->

   <script src="/vendor/components-jqueryui/jquery-ui.js"></script>
   <script src="/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script><!-- MOMENT JS-->
   <script src="/vendor/moment/min/moment-with-locales.js"></script><!-- FULLCALENDAR-->
   <script src="/vendor/fullcalendar/dist/fullcalendar.js"></script>
   <script src="/vendor/fullcalendar/dist/gcal.js"></script>
   <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>


   <script src="/js/app.js"></script>

   @yield('script')
   <script style="script/JacaScript">
    (function($) {
        if (window.location.pathname == "/dashboard"){
            //first page load
            if (activeId !="dashboard")
            $("#" + activeId).collapse('hide');
            localStorage.setItem('activeId', 'dashboard');
            $('#dashboard').css('color', 'white');
        }
        if (window.location.pathname.substr(0,window.location.pathname.lastIndexOf('/')) == "/calendar/scheduler"){
            localStorage.setItem('activeId', 'scheduler');
            localStorage.setItem('activelink', 'view_calendar');
        }
        if (window.location.pathname == "/estimator/properties" || window.location.pathname == "/estimator/properties/"){
            localStorage.setItem('activeId', 'estimator');
            localStorage.setItem('activelink', 'roof_properties');
        }
        if (window.location.pathname == "/estimator/generateProposal/" || window.location.pathname == "/estimator/generateProposal"){
            localStorage.setItem('activeId', 'signer');
            localStorage.setItem('activelink', 'obtain_esign');
        }
        if (window.location.pathname == "/signer/docs/" || window.location.pathname == "/signer/docs"){
            localStorage.setItem('activeId', 'signer');
            localStorage.setItem('activelink', 'docs');
        }




        $('.sidebar').on('shown.bs.collapse', function(e) {
            var activeId = localStorage.getItem('activeId');

            if (activeId != e.target.id && activeId != "dashboard"){
                $("#" + activeId).collapse('hide');
            }

            var active = e.target.id;
            localStorage.setItem('activeId', active);
        })

        var activeId = localStorage.getItem('activeId');

        if(activeId){
            $("#" + activeId).collapse('show');
        }
        var pageId = localStorage.getItem('activelink');
        if (pageId){
            $('#' + pageId).css('color', 'white');
        }
        $('.sidebar a').click((e)=>{
           localStorage.setItem('activelink', e.currentTarget.id);
        })
    })(jQuery);
 </script>
</body>
</html>
