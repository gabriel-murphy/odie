<!doctype html>
<html lang="{{ config('app.locale') }}" class="no-focus">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162024710-1"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtsoWEUduR1G8IylC7M1YefQO1b4REHNc&libraries=places"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162024710-1');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>@yield('title', "Welcome ". Auth::user()->first_name ?? '' ." to Odie")</title>

    <meta name="description" content="Odie">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts and Styles -->
    @yield('css_before')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="{{ mix('/css/codebase.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/toastr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
    <link rel="stylesheet" id="css-theme" href="{{ mix('/css/themes/corporate.css') }}">
    @yield('styles')

<!-- Scripts -->
    <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
</head>
<body>

<div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-fixed sidebar-inverse">
    @include('includes.partials.side_overlay')

    @include('includes.partials.sidebar')

    @include('includes.partials.header')

    <main id="main-container">
        <div class="content">
            @hasSection('breadcrumbs')
                <nav class="breadcrumb bg-white">
                    @yield('breadcrumbs')
                    <span class="breadcrumb-item active">@yield('breadcrumb-active')</span>
                </nav>
            @endif

            @hasSection('content-heading')
                <h2 class="content-heading pt-2">
                    @yield('content-heading')
                    <div class="float-right">@yield('content-header-action')</div>
                </h2>
            @endif

            @yield('content')
        </div>

        @include('includes.modals.file_viewer')
    </main>

    @include('includes.partials.footer')
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
<script src="{{ mix('js/codebase.app.js') }}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>

<!-- Laravel Scaffolding JS -->
<script src="{{ mix('js/laravel.app.js') }}"></script>

<script>
    @php($notifications = array('error', 'success', 'warning', 'info'))
    @foreach($notifications as $notification)
        @if(session()->has($notification))
            {{ "toastr." . $notification }}{!! "('" !!}{{session()->get($notification)}}{!! "')" !!}
        @endif
    @endforeach

    $(function () {
        $('.file-viewer').click(function () {
            let path = $(this).data('path');
            $('#file_viewer').prop('src', path);
            $('#file_viewer_modal').modal().show();
        });

        $('.is-invalid').change(function () {
            $(this).removeClass('is-invalid').closest('.invalid-feedback').hide();
        });
    })
</script>

@yield('scripts')
</body>
</html>
