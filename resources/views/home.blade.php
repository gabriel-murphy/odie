<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162024710-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162024710-1');
    </script>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <link rel="icon" type="image/x-icon" href="favicon.ico">
   <title>Roofing Sales Booster & Project Management Platform - Raising the Roof on Efficiency & Profitability!</title><!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="vendor/@fortawesome/fontawesome-free/css/brands.css">
   <link rel="stylesheet" href="vendor/@fortawesome/fontawesome-free/css/regular.css">
   <link rel="stylesheet" href="vendor/@fortawesome/fontawesome-free/css/solid.css">
   <link rel="stylesheet" href="vendor/@fortawesome/fontawesome-free/css/fontawesome.css"><!-- ANIMATE.CSS-->
   <link rel="stylesheet" href="vendor/animate.css/animate.css"><!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="css/bootstrap.css" id="bscss"><!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="css/app.css" id="maincss">
   <link rel="stylesheet" href="css/landing.css">
</head>

<body>
   <div class="wrapper">
      <header>
         <nav class="d-flex align-items-center" style="background:#f8f9fa;position:fixed;width:100%;z-index:100;padding:13px 10px;">
            <div class="container">
               <a href="/" class="app-logo" style="float:left;">
                  <img src="/images/logo.png">
               </a>
               <div class="ml-auto" style="float:right;padding-top:10px;">
                  <div class="d-flex">
                     <a class="btn btn-primary btn-fw mr-3" href="/login" style="width:75px">
                        <strong>Login</strong>
                     </a>
                     <a class="btn btn-info btn-fw" href="/register" style="width:75px;">
                        <strong style="color:white">Register</strong>
                     </a>
                  </div>
               </div>
            </div>
         </nav>
         <div class="container">
            <div class="header-content d-flex align-items-center" style="padding-top:150px;">
               <div class="row row-flush">
                  <div class="col-xs-12 col-lg-5 align-middle">
                     <div class="browser-presentation animated fadeInLeftShort">
                        <img class="img-fluid perspective-left card-shadow" style="max-width:110%" src="images/landing/chrome-app.png" alt="App Name">
                     </div>
                  </div>
                  <div class="col-xs-12 col-lg-7 align-middle">
                     <div class="side-presentation mt-4 mt-lg-0" style="color:white;text-shadow:0px 0px 10px rgba(255, 255, 255, 0.6), 0px 0px 20px rgba(0, 0, 0, 0.3)">
                        <h1 class="text-lg" style="font-weight:1000">Roofing Sales Booster & Project Management Platform - Raising the Roof on Efficiency & Profitability!</h1>
                        <p class="lead">Built by roofers and web artisans, Odie is the successful roofer's trade secret.  Make it yours as well.</p>
                        <div class="d-flex store-list">
                           <a class="mr-3" href="#">
                              <img class="img-fluid" src="images/landing/Download@1x.svg" alt="App Name">
                           </a>
                           <a href="#">
                              <img class="img-fluid" src="images/landing/google-play-badge.svg" alt="App Name">
                           </a>
                        </div>
                        <p class="lead">Want to know how it works?</p>
                        <p><a class="btn btn-primary btn-lg btn-fw" href="#"><strong>Request Demo Now!</strong></a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <section id="clients">
         <div class="container">
            <h2 class="section-header">Odie Makes Roofers Professional & Profitable.<br><br><small class="text-muted text-center">Any roofer with a ladder and some common sense should employ Odie- a unique, software-as-a-service platform built on best-of-breed technologies. &nbsp;Odie optimizes your return on investment on lead sources while managing your roofing business workflows.  Aimed with your cost of materials and labor, Odie compiles roof take-offs and empowers your sales force with automated calculation of estimates. &nbsp;Odie's AI engine considers roof pitch, permitting costs and other factors in calculating your project cost - right down to commissions.  Odie also considers your customized waste factors and profit margins to prepare a professional, turnkey estimate for e-signature akin to professional e-signature platforms.</small></h2>
            <div class="row">
               <div class="col-lg-2 col-md-4 col-xs-6"><img class="img-fluid" src="images/landing/clients/jquery.png" alt="client"></div>
               <div class="col-lg-2 col-md-4 col-xs-6"><img class="img-fluid" src="images/landing/clients/node.png" alt="client"></div>
               <div class="col-lg-2 col-md-4 col-xs-6"><img class="img-fluid" src="images/landing/clients/gulp.png" alt="client"></div>
               <div class="col-lg-2 col-md-4 col-xs-6"><img class="img-fluid" src="images/landing/clients/pug.png" alt="client"></div>
               <div class="col-lg-2 col-md-4 col-xs-6"><img class="img-fluid" src="images/landing/clients/sass.png" alt="client"></div>
               <div class="col-lg-2 col-md-4 col-xs-6"><img class="img-fluid" src="images/landing/clients/bootstrap.png" alt="client"></div>
            </div>
         </div>
      </section>
      <section class="bg-primary" id="testimonial" style="background-color:#d74448 !important;color:white;">
         <div class="container">
            <div class="carousel slide" id="carousel-testimonial" data-ride="carousel">
               <ol class="carousel-indicators">
                  <li class="active" data-target="#carousel-testimonial" data-slide-to="0"></li>
                  <li data-target="#carousel-testimonial" data-slide-to="1"></li>
                  <li data-target="#carousel-testimonial" data-slide-to="2"></li>
               </ol>
               <div class="carousel-inner">
                  <div class="carousel-item active">
                     <div class="w-75 m-auto text-center">
                        <h4><em>This is way better than SugarCRM, it takes me minutes what use to take me hours to complete: a professional estimate.</em></h4>
                        <p><strong>- Future Roofer</strong></p>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="w-75 m-auto text-center">
                        <h4><em>We can't say enough good things about Odie. We fortuitously named our second child after our roofing partner!</em></h4>
                        <p><strong>- Lindsey Dopfer</strong></p>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="w-75 m-auto text-center">
                        <h4><em>I see why this platform has patented technology!  I just replaced half of my office staff with Odie and I am off to Malta!</em></h4>
                        <p><strong>- Heartless Roofer</strong></p>
                     </div>
                  </div>
               </div><a class="carousel-control-prev" href="#carousel-testimonial" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-testimonial" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>
            </div>
         </div>
      </section>
      <section class="bg-white">
         <div class="container">
            <h2 class="section-header">Not Just Another CRM - A Feature-Rich Roofing Business Booster<br><small class="text-muted text-center">Odie comes packaged with feature-rich modules, all of which can be heavily configured, including:</small></h2>
            <div class="row">
               <div class="col-lg-3">
                  <ul class="feature-list">
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Opportunities</h4>
                        <p>Odie turns opportunties from your lead sources into won projects through proprietary sources and methods.</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Scheduler</h4>
                        <p>Odie makes it simple to schedule various appointments for your estimators, complete with notifications.</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Workflows</h4>
                        <p>Odie makes decisions based on your workflows, then logs the process which you can track with ease.</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Notifications</h4>
                        <p>Odie is constantly communicating with your estimators, customers, vendors and crews via common channels.</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Integrations</h4>
                        <p>Since Odie loves API, it obtains hip, ridge, valley, eave and other measurements error-free and in hours.</p>
                     </li>
                  </ul>
               </div>
               <div class="col-lg-6"><img class="img-fluid" src="images/landing/odie.png" alt="Odie Roofing SaaS Platform"></div>
               <div class="col-lg-3">
                  <ul class="feature-list">
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Estimator</h4>
                        <p>Odie's algorthm emulates the way you calculate project cost & profit - considering waste, pitch, commissions, etc.</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Presenter</h4>
                        <p>Odie turns raw numbers into professional estimates, custom to your payment and project terms.</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Connecter</h4>
                        <p>Odie creates a web-portal for each customer - they can view photos, pay their invoice and leave a review!</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Signer</h4>
                        <p>Odie's last name is Acrobat, as he then presents your prospect with an e-Sign request akin to Adobe Sign.</p>
                     </li>
                     <li>
                        <h4><span class="point bg-primary point-lg"></span>Harvester</h4>
                        <p>Odie identifies homeowners in your service area ripe to re-roof their home! &nbsp;Do you love Odie now?</p>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section id="callout">
         <div class="container text-center">
            <h1>Ready to make the right decision today?<br>  Request a demo to get started on the path to enhancing your roofing profits!</h1>
            <h4>Feature sets for roofers that will boost your roofing leads and not to be found in any other platform.  Period.</h4>
            <div class="d-flex store-list align-items-center justify-content-center">
               <a class="mr-3" href="#"><img class="img-fluid" src="images/landing/Download@1x.svg" alt="App Name"></a>
               <a href="#"><img class="img-fluid" src="images/landing/google-play-badge.svg" alt="App Name"></a>
            </div>
            <p><br><a class="btn btn-primary btn-large btn-oval" href="#" style="width: 180px"><strong>Request Demo Today</strong></a></p>
         </div>
      </section>
      <footer class="footer-1 bg-inverse">
         <div class="container">
            <div class="row">
               <div class="col-sm-2 col-xs-6">
                  <nav>
                     <h5 style="color:#d74448">PRODUCT</h5>
                     <ul class="list-unstyled">
                        <li><a href="#">API Documentation</a></li>
                        <li><a href="#">Customers</a></li>
                        <li><a href="#">Tour</a></li>
                        <li><a href="#">Pricing Model</a></li>
                     </ul>
                  </nav>
               </div>
               <div class="col-sm-2 col-xs-6">
                  <nav>
                     <h5 style="color:#d74448">CONTACT</h5>
                     <ul class="list-unstyled">
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Sales</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Blog</a></li>
                     </ul>
                  </nav>
               </div>
               <div class="col-sm-2 col-xs-6">
                  <nav>
                     <h5 style="color:#d74448">COMPANY</h5>
                     <ul class="list-unstyled">
                        <li><a href="#">About</a></li>
                        <li><a href="#">Press Kit</a></li>
                        <li><a href="#">Education</a></li>
                        <li><a href="#">Non-profits</a></li>
                     </ul>
                  </nav>
               </div>
               <div class="col-sm-2 col-xs-6">
                  <nav>
                     <h5 style="color:#d74448">Say HELLO</h5>
                     <ul class="list-unstyled">
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Google+</a></li>
                        <li><a href="#">Blog</a></li>
                     </ul>
                  </nav>
               </div>
               <div class="col-lg-4 col-xs-12">
                  <form action="" method="post">
                     <h5 style="color:#d74448">SUBSCRIBE TO OUR NEWSLETTER!</h5>
                     <div class="input-group"><input class="form-control" type="email" name="email" placeholder="mail@example.com" required=""><span class="input-group-append"><button class="btn btn-success" type="submit">Join</button></span></div>
                  </form>
                  <p class="text-muted"><small>We will never send spam and you can unsubscribe any time</small></p>
               </div>
            </div>
         </div>
      </footer>
      <footer class="footer-2">
         <div class="container">
            <div class="row align-items-center" style="color:white;">
               <div class="col-lg-4">
                  <p><span>Copyright 2020</span><span class="mx-2">|</span><a href="#">Terms of Service</a><span class="mx-2">|</span><a href="#">Privacy Policy</a></p>
               </div>
               <div class="col-lg-4 logo-column"></a></div>
               <div class="col-lg-4 text-right">
                  <p><span>Crafted with </span><em class="fa fa-heart text-danger fa-fw mx-2"></em><span> in Cape Coral</span></p>
               </div>
            </div>
         </div>
      </footer>
   </div><!-- =============== VENDOR SCRIPTS ===============-->
   <!-- STORAGE API-->
   <script src="vendor/js-storage/js.storage.js"></script><!-- i18next-->
   <script src="vendor/i18next/i18next.js"></script>
   <script src="vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- JQUERY-->
   <script src="vendor/jquery/dist/jquery.js"></script><!-- BOOTSTRAP-->
   <script src="vendor/popper.js/dist/umd/popper.js"></script>
   <script src="vendor/bootstrap/dist/js/bootstrap.js"></script><!-- PARSLEY-->
   <script src="vendor/parsleyjs/dist/parsley.js"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="js/app.js"></script>
</body>

</html>
