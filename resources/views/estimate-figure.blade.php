@extends('layouts.backend')

@section('content')
    <!-- Main section-->
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="content-title">Estimator :: Create Estimates
                    <small>Step 4 - Show Estimate Calcuations</small>
                </div>
            </div>
            <div class="container-fluid">
                @if(Session::has('message'))
                    <div class="alert alert-success" id="flash-message"><h3> {!! session('message') !!}</h3></div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        @csrf
                        <div class="block">
                            <div class="block-header block-header-default">Calculated by input value you entered.</div>
                            <div class="block-content" style="margin-top:10px">
                                <div class="form-group row" style="margin-bottom:0px;margin-left:20px">
                                    <label class="text-md-left" style="color:blue">Projector:</label>
                                    <label class="text-md-left"
                                           style="margin-left:16px">{!!session('company_name')!!}</label>
                                </div>
                                <div class="form-group row" style="margin-bottom:0px;margin-left:20px">
                                    <label class="text-md-left" style="color:blue">Estimator:</label>
                                    <label class="text-md-left"
                                           style="margin-left:18px">{!!session('first_name')!!} {!!session('last_name')!!}</label>
                                </div>
                                <div class="form-group row" style="margin-bottom:0px;margin-left:20px">
                                    <label class="text-md-left" style="color:blue">Project Info</label>
                                    <div class="form-group" style="padding-left:30px">
                                        <label class="text-md-left">Contact : John Doe, +1 (111) 111-1111, Northeast 7th
                                            Terrace,Angelica (Town), New York</label><br>
                                        <label class="col-form-label text-md-left">Present Roof Type :
                                            Shingle</label><br>
                                        <label class="col-form-label text-md-left">Preferred Roof Type : Shingle</label>
                                    </div>
                                </div>
                                <div class="form-group row" style="margin-bottom:0px;">
                                    <div class="col-md-12" style="padding:10px 30px">
                                        <div class="card card-default">
                                            <div class="card-header d-flex">
                                                <div class="card-title">Build Sheet for __________</div>
                                                <div class="ml-auto text-muted-light">
                                                    <div class="d-inline-block mr-3" data-perform="card-collapse"><em
                                                            class="fa fa-minus"></em></div>
                                                    {{-- <div class="d-inline-block mr-0" data-perform="card-dismiss"><em class="fa fa-times"></em></div> --}}
                                                </div>
                                            </div><!-- START table-responsive-->
                                            <div class="card-wrapper"
                                                 style="max-height: 1726px; transition: max-height 0.5s ease 0s; overflow: hidden;">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover" id="table-ext-1">
                                                        <thead>
                                                        <tr>
                                                            <th>Item</th>
                                                            <th>Code</th>
                                                            <th>Name</th>
                                                            <th>Quan</th>
                                                            <th>Cost</th>
                                                            <th>Total</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($build_sheet_materials as $item)
                                                            <tr>
                                                                <td></td>
                                                                <td>{{ $item->code }}</td>
                                                                <td>{{ $item->name }}</td>
                                                                <td>{{ $item->material_quantity }}</td>
                                                                <td>{{ $item->cost }}</td>
                                                                <td>{{ $item->total_cost }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table><!-- END table-responsive-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" style="padding:10px 30px">
                                        <div class="card card-default">
                                            <div class="card-header d-flex">
                                                <div class="card-title">Measurement Data</div>
                                                <div class="ml-auto text-muted-light">
                                                    <div class="d-inline-block mr-3" data-perform="card-collapse"><em
                                                            class="fa fa-minus"></em></div>
                                                    {{-- <div class="d-inline-block mr-0" data-perform="card-dismiss"><em class="fa fa-times"></em></div> --}}
                                                </div>
                                            </div><!-- START table-responsive-->
                                            <div class="card-wrapper"
                                                 style="max-height: 1726px; transition: max-height 0.5s ease 0s; overflow: hidden;">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover" id="table-ext-1">
                                                        <thead>
                                                        <tr>
                                                            <th>Measurement</th>
                                                            <th>Actual</th>
                                                            <th>Rounded</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>Square Feet (acutal)</td>
                                                            <td>{!! number_format(session('roof_measurements.square_feet')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Squares (actual)</td>
                                                            <td>{!! number_format(session('estimate.squares')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Hips</td>
                                                            <td>{!! number_format(session('roof_measurements.hips')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Valley</td>
                                                            <td>{!! number_format(session('roof_measurements.valleys')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Rakes</td>
                                                            <td>{!! number_format(session('roof_measurements.rakes')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Eaves</td>
                                                            <td>{!! number_format(session('roof_measurements.eaves')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Flashing</td>
                                                            <td>{!! number_format(session('roof_measurements.flashing')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Step Flashing</td>
                                                            <td>{!! number_format(session('roof_measurements.step_flashing')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Drip Edge</td>
                                                            <td>{!! number_format(session('roof_measurements.drip_edge')) !!}</td>
                                                            <td></td>
                                                        </tr>
                                                        </tbody>
                                                    </table><!-- END table-responsive-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" style="margin-bottom:0px;margin-left:20px">
                                    <div class="col-md-6 form-group" style="padding-left:0px;">
                                        <label class="text-md-left" style="color:blue">Estimate Info</label>
                                        <div class="form-group" style="padding-left:20px">
                                            <label class="text-md-left">Roof Type : Shingle</label><br>
                                            <label class="col-form-label text-md-right">Roof Manufacturer : GAF -
                                                General Aniline & Film</label><br>
                                            <label class="col-form-label text-md-right">Roof Manufacturers Line :
                                                Timberline UHD</label><br>
                                            <label class="col-form-label text-md-right">Insurance : Timberline
                                                UHD</label><br>
                                            <label class="col-form-label text-md-right">Financing : Timberline
                                                UHD</label><br>
                                            <label class="col-form-label text-md-right">Warranty : GAF Golden
                                                Pledge</label><br>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group" style="padding-left:0px;">
                                        <label class="text-md-left" style="color:red">Our Total Cost : </label>
                                        <label class="text-md-left"
                                               style="color:red">{!!session('estimate.total_cost')!!}</label>

                                    </div>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <button id="sendProposal" class="btn btn-primary btn-lg btn-block"
                                                style="width:300px;margin:auto">
                                            Send Proposal
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script style="script/JavaScript">
        $('#insurance').on('change', () => {
        $('#insurance').is(':checked') == true ? $('#insurance_value').val(1) : $('#insurance_value').val(0);
        })
        $('#financing').on('change', () => {
        $('#financing').is(':checked') == true ? $('#financing_value').val(1) : $('#financing_value').val(0);
        })
        $('#sendProposal').on('click', ()=>{
        window.location.replace('/estimator/generateProposal/')
        })
    </script>
@endsection
