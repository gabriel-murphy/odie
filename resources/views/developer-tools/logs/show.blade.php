@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Developer Tools</a>
@endsection
@section('breadcrumb-active', 'Logs')

@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Logs</h3>
        </div>
        <div class="block-content">
            <pre>@foreach($logs as $log)@foreach($log as $line){{ $file_content[$line] }}@endforeach @endforeach</pre>
        </div>
    </div>
@endsection
