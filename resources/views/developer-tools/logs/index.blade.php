@extends('layouts.backend')

@section('breadcrumbs')
    <a class="breadcrumb-item" href="javascript:void(0)">Developer Tools</a>
@endsection

@section('breadcrumb-active', 'Logs')


@section('content')
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Logs</h3>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr class="text-uppercase">
                        <th style="width: 10px">#</th>
                        <th>File Name</th>
                        <th>Size</th>
                        <th>Last Modified</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($files as $file)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td class="font-w600">{{ $file['name'] }}</td>
                            <td>{{ $file['size'] }}</td>
                            <td>{{ $file['lastModified']->diffForHumans() }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('developer_tools.logs.show', $file['name'] ) }}"
                                       class="btn btn-sm btn-secondary"
                                       data-toggle="tooltip" title="View">
                                        <i class="fad fa-eye"></i>
                                    </a>
                                    <button data-toggle="modal" data-target="#confirm_truncate__{{ $loop->iteration }}"
                                            class="btn btn-sm btn-outline-danger" title="">
                                        <i class="fa fa-trash"></i></button>
                                    @include(
                                        'includes.modals.confirm', [
                                        'route' => route('developer_tools.logs.truncate', $file['name']),
                                        'form' => true, 'action' => 'truncate', 'id' => $loop->iteration
                                    ])
                                </div>
                            </td>
                        </tr>
                    @empty
                        <td colspan="5" class="text-center text-secondary">No Logs Found</td>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
