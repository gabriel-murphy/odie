@extends('layouts.backend')

@section('content')
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="content-title">Tasks :: Create Task</div>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="block">
                            <div class="block-header block-header-default">Create Task</div>
                            <div class="block-content">
                                <form method="post" action="{{ route('tasks.store') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="title" class="col-md-3 col-form-label">Title:</label>
                                        <div class="col-md-6">
                                            <input id="title" type="text" name="title" value="{{ old('title') }}"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                  required autofocus>
                                            @include('includes.partials.error', ['field' => 'title'])
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="due_date" class="col-md-3 col-form-label">Due Date:</label>
                                        <div class="col-xl-6 col-10">
                                            <div class="input-group date">
                                                <input id="due_date" class="form-control @error('due_date') is-invalid @enderror" name="due_date" type="text">
                                                @include('includes.partials.error', ['field' => 'due_date'])
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="client_id" class="col-md-3 col-form-label text-md-right">Select Client</label>
                                        <div class="col-md-6">
                                            <select id="client_id" class="form-control @error('client_id') is-invalid @enderror" name="client_id" required>
                                                <option value="">Select Client</option>
                                                @foreach (_clients() as $client)
                                                    <option value="{{ $client->id }}">{{ $client->company_name }}</option>
                                                @endforeach
                                            </select>
                                            @include('includes.partials.error', ['field' => 'client_id'])
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="project_id" class="col-md-3 col-form-label text-md-right">Select Project</label>
                                        <div class="col-md-6">
                                            <select id="project_id" class="form-control @error('project_id') is-invalid @enderror" name="project_id" >
                                                <option value="">Select Project</option>
                                                @foreach (_clients() as $project)
                                                    <option value="{{ $project->id }}">{{ $project->company_name }}</option>
                                                @endforeach
                                            </select>
                                            @include('includes.partials.error', ['field' => 'project_id'])
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="users" class="col-md-3 col-form-label text-md-right">Assign User</label>
                                        <div class="col-md-6">
                                            <select id="users" class="select2 form-control @error('users') is-invalid @enderror" name="users[]" multiple>
                                                <@foreach (_users() as $user)
                                                    <option value="{{ $user->id }}">{{ $user->username }}</option>
                                                @endforeach
                                            </select>
                                            @include('includes.partials.error', ['field' => 'users'])
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="description" class="col-md-3 col-form-label text-md-right">Description:</label>
                                        <div class="col-md-6">
                                            <textarea id="description" rows="5" name="description"
                                                      class="form-control @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
                                            @include('includes.partials.error', ['field' => 'description'])
                                        </div>
                                    </div>

                                    <div class="row justify-content-center">
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary  btn-block">
                                                Create Task
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css' rel='stylesheet' type='text/css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js' type='text/javascript'></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    <script>
        $('#due_date').datetimepicker({
            format: "YYYY-MM-DD",
            defaultDate: new Date(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                today: "fa fa-clock-o",
                clear: "fa fa-trash-o"
            }
        });
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: "Select Users"
            });
        });
    </script>
@endsection
