@extends('layouts.backend')

@section('content')
    <section class="section-container">
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="content-header">
                <div class="content-title">Tasks :: All Tasks</div>
            </div>
            <div class="container-fluid">
                @if(Session::has('success'))
                    <div class="alert alert-success" id="flash-message"><h3> {!! session('success') !!}</h3></div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="block">
                            <div class="card-header pb-0">
                                <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active border-0" id="home-tab" data-toggle="tab"
                                           href="#assigned_tasks" role="tab">Tasks Assigned To Me</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link border-0" id="profile-tab" data-toggle="tab"
                                           href="#created_tasks" role="tab">Tasks Created By Me</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="block-content">
                                <div class="tab-content p-0 border-0" id="myTabContent">
                                    <div class="tab-pane fade show active" id="assigned_tasks" role="tabpanel">
                                        @forelse($assigned_tasks as $task)
                                            <div class="list-group-item list-group-item-action">
                                                <div class="media">
                                                    <div class="media-body clearfix">
                                                        <div class="float-right">
                                                            @if(!$task->completed)
                                                                <a href="{{ route('tasks.complete', $task->id) }}"
                                                                   class="btn btn-sm btn-primary">
                                                                    <i class="fa fa-check"></i> Mark as complete
                                                                </a>
                                                            @else
                                                                <span class="badge badge-success">Completed</span>
                                                            @endif
                                                            <small class="mt-2 d-block">
                                                                <strong>Created
                                                                    By:</strong> {{ $task->creator->username }}
                                                            </small>
                                                            <small class="mt-2 d-block">
                                                                <strong>Created at:</strong> {{ $task->created_at }}
                                                            </small>
                                                        </div>
                                                        <p class="mb-1"><strong>{{ $task->title }}</strong></p>
                                                        <p class="mb-1">{{ $task->description }}</p>
                                                        <p class="m-0 mb-1">Due on: {{ $task->due_date }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <p class="text-center">No Record Found</p>
                                        @endforelse
                                        <div class="mt-3 justify-content-center pagination">
                                            {{ $assigned_tasks->links() }}
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="created_tasks" role="tabpanel">
                                        @forelse($created_tasks as $task)
                                            <div class="list-group-item list-group-item-action">
                                                <div class="media">
                                                    <div class="media-body clearfix">
                                                        <div class="float-right">
                                                            @if(!$task->completed)
                                                                <span class="badge badge-info">In-Complete</span>
                                                            @else
                                                                <span class="badge badge-success">Completed</span>
                                                                <small class="mt-2 d-block">
                                                                    <strong>Completed By:</strong> {{ $task->doer->username }}
                                                                </small>
                                                            @endif
                                                            <small class="mt-2 d-block">
                                                                <strong>Created at:</strong> {{ $task->created_at }}
                                                            </small>
                                                        </div>
                                                        <p class="mb-1"><strong>{{ $task->title }}</strong></p>
                                                        <p class="mb-1">{{ $task->description }}</p>
                                                        <p class="m-0 mb-1">Due on: {{ $task->due_date }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <p class="text-center">No Record Found</p>
                                        @endforelse
                                        <div class="mt-3 justify-content-center pagination">
                                            {{ $created_tasks->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
