require('./bootstrap');
import Vue from 'vue';

/*Components Imports*/
import VueGoogleAutocomplete from './components/VueGoogleAutocomplete'
import AddressComponent from './components/AddressComponent'
import Contact from './components/Contact'
import CustomerComponent from "./components/CustomerComponent"
import InsuranceCarrier from "./components/InsuranceCarrier";
import GoogleStreetViewer from "./components/GoogleStreetViewer";
import CorrespondenceFields from "./components/CorrespondenceFields";
import CustomerFieldsComponent from "./components/CustomerFieldsComponent";
import MortgageCompanyComponent from "./components/MortgageCompanyComponent";
import EmailComponent from "./components/EmailComponent";

/*Views Imports*/
import RoofSelector from './views/RoofSelector'
import PenetrationSelector from './views/PenetrationSelector'
import constant from './views/Constant'

/*Slot Import*/
import modal from "./slots/Modal";
import errors from "./slots/Errors";
import error from "./slots/Error";


/*Components Registration*/
Vue.component('vue-google-autocomplete', VueGoogleAutocomplete);
Vue.component('address-component', AddressComponent);
Vue.component('contact', Contact);
Vue.component('customer', CustomerComponent);
Vue.component('customer-fields', CustomerFieldsComponent);
Vue.component('insurance-carrier', InsuranceCarrier);
Vue.component('street-viewer', GoogleStreetViewer);
Vue.component('mortgage-company', MortgageCompanyComponent);
Vue.component('opportunity-email', EmailComponent);

/*Views Registration*/
Vue.component('roof-selector', RoofSelector);
Vue.component('penetration-selector', PenetrationSelector);
Vue.component('constant', constant);
Vue.component('correspondence-fields', CorrespondenceFields);

/*slots Registration*/
Vue.component('modal-slot', modal);
Vue.component('errors-slot', errors);
Vue.component('error-slot', error);


Vue.config.productionTip = false;

axios.interceptors.response.use(null, (error) => {
    if (error.response.status === 422) {
        return Promise.reject(error.response);
    }
});

const app = new Vue({
    el: '#page-container',
});
