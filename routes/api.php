<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth', 'namespace' => 'Api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('user', 'AuthController@user');
    Route::post('register', 'AuthController@register');
});

Route::apiResource('insurance-carriers', 'Api\InsuranceCarrierController');
Route::apiResource('customers', 'Api\CustomerController');
Route::apiResource('mortgageCompanies', 'Api\MortgageCompanyController');

Route::get('constantsGroups', 'Api\GroupConstantsController@group')->name('api.constantsGroup.index');
Route::post('constantGroups/{constantGroup}/constants/{constant}/changeStatus', 'Api\GroupConstantsController@changeStatus')->name('api.constants.change_status');
Route::apiResource('constantGroups.constants', 'Api\GroupConstantsController');
Route::post('/upload', 'Api\GroupConstantsController@upload');
