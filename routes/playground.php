<?php

Route::group(['middleware' => ['auth', 'verified'] ], function () {

    Route::get('/calendar/scheduler/{project}', 'CalendarController@scheduler')->name('scheduler');
    Route::post('/calendar/schedule/{id}', 'CalendarController@schedule');
    Route::get('/calendar', 'CalendarController@index')->name('calender');

    Route::post('/addappointment', 'CalendarController@addappointment');
    Route::post('/editappointment/date', 'CalendarController@editappointment_date');
    Route::post('/editappointment/content', 'CalendarController@editappointment_content');
    Route::delete('/deleteappointment', 'CalendarController@deleteappointment');
});

/* Play Ground */
Route::get('/appointment-mail', 'PlayGroundController@appointmentMail');
Route::get('/playground', 'PlayGroundController@index');
Route::get('/playground/canvas', 'PlayGroundController@designCanvas');
Route::view('/playground/send_sms', 'playground.send_sms');
Route::post('/playground/send_sms', 'PlayGroundController@sms')->name('playground.sms');
