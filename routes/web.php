<?php

Auth::routes(['verify' => true]);
Route::mailgunWebhooks('webhooks/mailgun');

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('/customersign/{token}', 'EstimateController@customersign')->name('customersign');
    Route::post('/customersign', 'EstimateController@customerDosign');
    Route::post('/customersign/verify', 'EstimateController@customerVerify');

    /*Project Routes*/
    //Route::resource('projects', 'ProjectController')->except(['destroy']);
    Route::get('/projects', 'ProjectController@index')->name('projects.index');
    Route::get('projects/create', 'ProjectController@create')->name('projects.create');
    Route::post('projects/{step}', 'ProjectController@store')->name('projects.steps.store');
    //Route::get("/testemail",'ProjectController@basic_email');

    Route::group(['prefix' => 'estimator'], function () {
        Route::get('/buildlists', 'BuildListController@index')->name('estimator.buildlists');
        Route::get('/buildlists/build', 'BuildListController@create')->name('estimator.buildlists.build');
        Route::get('/{step?}', 'EstimatorController@index')->name('estimator.index');

        Route::get('/{project}/structure-details', 'EstimatorController@getStructureDetails')->name('estimator.structure_details');
        Route::post('/{project}/structure', 'EstimatorController@storeStructure')->name('estimator.structure.store');

        Route::get('/{project}/roof-selector', 'EstimatorController@roofSelector')->name('estimator.roof_selector');
        Route::post('/{project}/roof-selector', 'StructureController@storeRoofType')->name('estimator.roof_selector');

        Route::post('/{project}/estimate', 'EstimatorController@estimate')->name('estimator.estimate');
    });

    Route::get('/estimator/numbers', 'EstimateController@numbers')->name('numbers');

    Route::post('/estimator/calculate', 'EstimateController@store');
    Route::post('/estimator/store', 'PropertyController@store')->name('properties.store');
    Route::get('/estimator/create-estimate', 'EstimateController@create')->name('myestimates');
    Route::get('/estimator/estimate-figures', 'EstimateController@showCalculationResult')->name('estimate-figures');
    Route::get('/estimator/generateProposal', 'EstimateController@generateProposal')->name('generateProposal');
    /*proposal send*/
    Route::post('/estimator/generateProposal/{id}/send', 'EstimateController@generateProposalSend')->name('generateProposalSend');

    /*Harvester Views*/
    Route::get('/harvester/market', 'HarvesterController@market')->name('market');                      // Gabe working on this interface
    Route::get('/harvester/candidates', 'HarvesterController@candidates')->name('candidates');          // Gabe working on this interface

    Route::get('/estimator/generatePDF', 'EstimateController@generatePDF')->name('generatePDF');


    Route::get('/signer/docs', 'DocumentsController@index')->name('docs');

    Route::post('appointments/{appointment}/update-time', 'AppointmentController@updateTime');
    Route::resource('appointments', 'AppointmentController');

    Route::resource('users', 'UserController');
    Route::resource('materials', 'MaterialController');

    Route::get('tasks/{task}/complete', 'TaskController@complete')->name('tasks.complete');
    Route::resource('tasks', 'TaskController')->only(['index', 'create', 'store']);

    ## Gabe Playground
    Route::get('/email', 'HomeController@email')->name('email');        // Gabe messing around with Sendgrid

    /*Project Routes*/
    Route::resource('customers', 'CustomerController')->except(['destroy']);

    /* Maximus routes */
    Route::group(['prefix' => 'maximus/claims'], function () {
        Route::get('/', 'MaximusController@index')->name('maximus.claims.index');
        Route::get('new', 'MaximusController@create')->name('maximus.claims.create');
        Route::post('/{step}', 'MaximusController@store')->name('maximus.claims.step.store');
//        Route::post('new', 'MaximusController@store')->name('maximus.claims.store');
        Route::get('{claim}/show', 'MaximusController@show')->name('maximus.claims.show');
        Route::post('{claim}/update', 'MaximusController@update')->name('maximus.claims.update');
        Route::post('{claim}/temp-repair', 'MaximusController@tempRepairStore')->name('maximus.temp_repair.store');
        Route::post('{claim}/supplement', 'MaximusController@supplement')->name('maximus.supplement.store');
        Route::post('{claim}/record-correspondence', 'MaximusController@recordCorrespondence')->name('maximus.record_correspondence.store');
        Route::post('{claim}/escalate', 'MaximusController@escalate')->name('maximus.escalate');
        Route::get('{claim}/test', 'MaximusController@test');
    });

    Route::resource('insurance-carriers', 'InsuranceCarrierController')->names([
        'create' => 'insurance_carriers.create',
        'store' => 'insurance_carriers.store',
        'index' => 'insurance_carriers.index',
        'destroy' => 'insurance_carriers.destroy',
        'update' => 'insurance_carriers.update',
        'show' => 'insurance_carriers.show',
        'edit' => 'insurance_carriers.edit',
    ]);

    ## Khawar working area
    Route::group(['prefix' => 'settings'], function () {
        Route::view('/company', 'settings/company/show')->name('settings.company.show');
        Route::view('/company/edit', 'settings/company/edit')->name('settings.company.edit');
    });

    Route::name('templates.')->prefix('templates')->group(function () {
        Route::get('emails/{template}/changeStatus', 'EmailTemplateController@changeStatus')->name('emails.change_status');

        Route::resource('emails', 'EmailTemplateController')->parameters(['emails' => 'template']);
    });


    Route::get('constants', 'GroupConstantController')->name('constants.index');
    Route::resource('placeholders', 'PlaceholderController');

    Route::get('emails/odmails', 'OdMailController@index')->name('emails.odmails');
    Route::get('emails/odmail/{odmail}', 'OdMailController@show')->name('emails.odmails.show');
    Route::get('emails/odmail/{odmail}/resend', 'OdMailController@resend')->name('emails.odmails.resend');
    Route::post('emails/odmail/{odmail}/forward', 'OdMailController@forward')->name('emails.odmails.forward');

    Route::resource('campaigns', 'CampaignController');
    Route::get('campaigns/{campaign}/recipients', 'CampaignController@recipients')->name('campaigns.recipients');

    Route::get('developer-tools/logs', 'DeveloperTools\LogsController@index')->name('developer_tools.logs');
    Route::get('developer-tools/logs/{file}', 'DeveloperTools\LogsController@show')->name('developer_tools.logs.show');
    Route::delete('developer-tools/logs/{file}', 'DeveloperTools\LogsController@truncate')->name('developer_tools.logs.truncate');

});
Route::view('/frontend', 'frontend/frontend')->name('frontend');
