<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'company', 'email', 'associate_type_id', 'raw_data', 'enhance_data',
    ];

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getPhoneAttribute()
    {
        if ($this->work_phone) {
            $phone = substr_replace($this->work_phone, '(', 0, 0);
            $phone = substr_replace($phone, ') ', 4, 0);
            $phone = substr_replace($phone, '-', 9, 0);
        } else $phone = null;
        return $this->extension ? $phone . ' x' . $this->extension : $phone;
    }
}
