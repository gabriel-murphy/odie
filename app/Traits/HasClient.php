<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 3/27/2020
 * Time: 4:58 PM
 */

namespace App\Traits;


use App\Client;
use Illuminate\Database\Eloquent\Builder;

trait HasClient
{
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /*protected static function boot()
    {
        parent::boot();
        if (auth()->check() && $client_id = auth()->user()->client_id)
        {
            if (self::class != 'App\User') {
                static::addGlobalScope('forCurrentClient', function (Builder $builder) use ($client_id) {
                    $builder->where('client_id', $client_id);
                });

                self::created(function ($model) use ($client_id){
                    $model->client_id = $client_id;
                    $model->save();
                });
            }
        }
    }*/
}
