<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 3/23/2020
 * Time: 4:25 AM
 */

namespace App\Traits;


trait UploadImage
{
    public function uploadImage($image, $dir)
    {
        if (request()->hasFile($image) && request()->file($image)->isValid()){
            /* Remove old image if present*/
            if($this->{$image}) unlink(public_path('storage/'.$this->{$image}));

            $path = NULL;
            $path = request()->{$image}->store($dir, 'public');
            $this->update([$image => $path]);
        }
    }
}
