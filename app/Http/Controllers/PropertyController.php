<?php

namespace App\Http\Controllers;

use App\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\RoofMeasurement;
use App\Structure;
use App\Appointment;
use App\Material;
use App\Project;

class PropertyController extends Controller
{

    /**
     * Show the form for creating a new view to enter measurements and pitch.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estimator.roof-properties', [
            'pitch_types' => Constant::group('pitch_types')->get(),
            'story_types' => Constant::group('story_types')->get(),
            'boot_types' => Material::getBoots(),
            'vent_types' => Material::getVents(),
            'appointments' => Appointment::cancelled(false)->get(),
            'penetration_types' => ['boot' => Material::getBoots(), 'vent' => Material::getVents()]
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
        $request->validate([
            'appointment_id' => 'required|numeric',
            'square_feet' => 'required|numeric',
            'stories' => 'required|numeric',
            'facets' => 'required|numeric',
            'pitch' => 'required|numeric',
            'boot_1' => 'required|numeric',
            'boot_1_quantity' => 'required|numeric',
            'ridges' => 'required|numeric',
            'ridge_vent' => 'required|numeric',
            'hips' => 'required|numeric',
            'valleys' => 'required|numeric',
            'rakes' => 'required|numeric',
            'eaves' => 'required|numeric',
            'flashing' => 'required|numeric',
            'step_flashing' => 'required|numeric',
        ]);

        $appointment = Appointment::find($request->appointment_id);
        $request->session()->put('project_id', $appointment->project_id);
        $project_id = $appointment->project_id;
        $project = Project::find($project_id);
        
        $request->session()->put('project.id', $project->id);
        $request->session()->put('project.project_type', $project->project_types_id);
        $request->session()->put('project.address', "$project->number $project->street");
        $request->session()->put('project.city', $project->city);
        $request->session()->put('project.city_id', $project->city_id);
        $request->session()->put('project.state', $project->state);
        $request->session()->put('project.state_id', $project->state_id);
        $request->session()->put('project.zip_code', $project->zip_code);

        $roof_measurements = RoofMeasurement::create([
            'square_feet' => $request->square_feet,
            'ridges' => $request->ridges,
            'hips' => $request->hips,
            'valleys' => $request->valleys,
            'rakes' => $request->rakes,
            'eaves' => $request->eaves,
            'flashing' => $request->flashing,
            'step_flashing' => $request->step_flashing,
            'drip_edge' => $request->eaves + $request->rakes,
            'ridges_hips_sum' => $request->ridges + $request->hips
        ]);

        $drip_edge = '1';
        $ridges_hips_sum = '1';
        $request->session()->put('roof_measurements.id', $roof_measurements->id);
        $request->session()->put('roof_measurements.square_feet', $request->square_feet);
        $request->session()->put('roof_measurements.stories', $request->stories);
        $request->session()->put('roof_measurements.pitch', $request->pitch);
        $request->session()->put('roof_measurements.ridges', $request->ridges);
        $request->session()->put('roof_measurements.ridge_vent', $request->ridge_vent);
        $request->session()->put('roof_measurements.hips', $request->hips);
        $request->session()->put('roof_measurements.valleys', $request->valleys);
        $request->session()->put('roof_measurements.rakes', $request->rakes);
        $request->session()->put('roof_measurements.eaves', $request->eaves);
        $request->session()->put('roof_measurements.flashing', $request->flashing);
        $request->session()->put('roof_measurements.step_flashing', $request->step_flashing);
        $request->session()->put('roof_measurements.drip_edge', $drip_edge);
        $request->session()->put('roof_measurements.ridges_hips_sum', $ridges_hips_sum);
           
        $request->session()->put('roof_measurements.boot_1', $request->boot_1);
        $request->session()->put('roof_measurements.boot_1_quantity', $request->boot_1_quantity);
        if ($request->boot_2 == '' || $request->boot_2_quantity == '') {
            unset($request->boot_2);
            unset($request->boot_2_quantity);
        } else {
            $request->session()->put('roof_measurements.boot_2', $request->boot_2);
            $request->session()->put('roof_measurements.boot_2_quantity', $request->boot_2_quantity);
        }
        if ($request->boot_3 == '' || $request->boot_3_quantity == '') {
            unset($request->boot_3);
            unset($request->boot_3_quantity);
        } else {
            $request->session()->put('roof_measurements.boot_3', $request->boot_3);
            $request->session()->put('roof_measurements.boot_3_quantity', $request->boot_3_quantity);
        }
        if ($request->boot_4 == '' || $request->boot_4_quantity == '') {
            unset($request->boot_4);
            unset($request->boot_4_quantity);
        } else {
            $request->session()->put('roof_measurements.boot_4', $request->boot_4);
            $request->session()->put('roof_measurements.boot_4_quantity', $request->boot_4_quantity);
        }
        if ($request->vent_1 == '' || $request->vent_1_quantity == '') {
            unset($request->vent_1);
            unset($request->vent_1_quantity);
        } else {
            $request->session()->put('roof_measurements.vent_1', $request->vent_1);
            $request->session()->put('roof_measurements.boot_1_quantity', $request->vent_1_quantity);
        }
        if ($request->vent_2 == '' || $request->vent_2_quantity == '') {
            unset($request->vent_2);
            unset($request->vent_2_quantity);
        } else {
            $request->session()->put('roof_measurements.vent_2', $request->vent_2);
            $request->session()->put('roof_measurements.vent_2_quantity', $request->vent_2_quantity);
        }
        if ($request->vent_3 == '' || $request->vent_3_quantity == '') {
            unset($request->vent_3);
            unset($request->vent_3_quantity);
        } else {
            $request->session()->put('roof_measurements.vent_3', $request->vent_3);
            $request->session()->put('roof_measurements.vent_3_quantity', $request->vent_3_quantity);
        }
        if ($request->vent_4 == '' || $request->vent_4_quantity == '') {
            unset($request->vent_4);
            unset($request->vent_4_quantity);
        } else {
            $request->session()->put('roof_measurements.vent_4', $request->vent_4);
            $request->session()->put('roof_measurements.vent_4_quantity', $request->vent_4_quantity);
        }

        $roof_structure = Structure::create([
            'name' => 'test', //should replace to real
            'project_id' => $project_id,
            'story_type_id' => $request->stories,
            'pitch_types_id' => $request->pitch,
            'roof_facets' => $request->facets,
            'boot_1' => $request->boot_1,
            'boot_1_quantity' => $request->boot_1_quantity,
            'boot_2' => $request->boot_2,
            'boot_2_quantity' => $request->boot_2_quantity,
            'boot_3' => $request->boot_3,
            'boot_3_quantity' => $request->boot_3_quantity,
            'boot_4' => $request->boot_4,
            'boot_4_quantity' => $request->boot_4_quantity,
            'vent_1' => $request->vent_1,
            'vent_1_quantity' => $request->vent_1_quantity,
            'vent_2' => $request->vent_2,
            'vent_2_quantity' => $request->vent_2_quantity,
            'vent_3' => $request->vent_3,
            'vent_3_quantity' => $request->vent_3_quantity,
            'vent_4' => $request->vent_4,
            'vent_4_quantity' => $request->vent_4_quantity,
            'roof_measurements_id' => $roof_measurements->id,
        ]);

        $request->session()->put('estimate_structure_id', $roof_structure->id);
        $request->session()->put('structure.id', $roof_structure->id);
        $request->session()->put('structure.name', $roof_structure->name);
        $request->session()->put('structure.story_type_id', $roof_structure->story_type_id);
        $request->session()->put('structure.pitch_types_id', $roof_structure->pitch_types_id);
        $request->session()->put('structure.roof_facets', $roof_structure->roof_facets);

        #dd(session()->all());
        #exit();

        $request->session()->flash('message', 'Roof Properties Saved');

        return redirect('/estimator/create-estimate');
    }

}
