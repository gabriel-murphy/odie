<?php

namespace App\Http\Controllers;

use App\Constant;
use App\Material;
use App\Project;
use App\RoofType;
use App\Services\Estimator;
use Illuminate\Http\Request;

class EstimatorController extends Controller
{
    public function index($step = null)
    {
        $projects = Project::all();
        info('inside Project');
        return view('estimator.index', compact('projects', 'step'));
    }

    public function getStructureDetails(Project $project)
    {
        $data = [
            'project' => $project,
            'pitch_types' => Constant::group('pitch_types')->get(),
            'story_types' => Constant::group('story_types')->get(),
            'penetration_types' => ['boot' =>  Material::boots()->get(), 'vent' => Material::vents()->get()]
        ];

        return view('estimator.structure-details', $data);
    }

    public function storeStructure(Request $request, Project $project)
    {
        $request->validate([
           'measurements.square_feet' => 'required'
        ]);
        $project->structures()->create($request->all());
        return redirect()->route('estimator.roof_selector', $project->id)->with('success', 'Structure Details Saved');
    }

    public function roofSelector(Project $project)
    {
        $data = array();
        $data['project'] = $project;
        $data['roof_type_groups'] = RoofType::with('roofTypes.manufacturers.productLines.series')->groups()->get();

        return view('estimator.roof-selector', $data);
    }

    public function estimate(Request $request, Project $project){
        $structure = $project->structures->first();

        /*$estimate = $structure->estimates()->firstOrCreate([
            'user_id' => auth()->user()->id,
            'roof_product_id' => $request->series_id ?? $request->product_id
        ]);
        $estimator = new Estimator($estimate);
        $buildSheets = $estimator->generateBuildSheets();
        $estimate->buildSheets()->createMany($buildSheets);*/
        $estimate = $structure->estimates->first();
        return view('estimator.estimate', compact('estimate', 'project'));
    }
}
