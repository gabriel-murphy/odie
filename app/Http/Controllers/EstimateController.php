<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Constant;
use App\Estimate;
use App\BuildSheet;
use File;
use \DateTime;
use \Nexmo;
use \Nexmo\Call\Call;
use ReallySimpleJWT\Encode;
use ReallySimpleJWT\Token;
use App\Mail\EstimateProposal;
use App\Mail\SendPinCodeForSign;
use App\Project;


class EstimateController extends Controller
{

    public function numbers()
    {
        $nexmo = app('Nexmo\Client');
        Nexmo::message()->send([
            'to' => '12393242365',
#    'to'   => '12399945597',
            'from' => '13312478278',
            'text' => 'Spencer Johnson just e-signed your roofing proposal, a copy of which has been sent to your email and posted in Signer'
        ]);

        return view('estimate-numbers');
    }

    public function create(Request $request)
    {
        return view('create-estimate', ['roofing_materials' => Constant::group('roof_types')->get(), 'roof_manufacturers' => Estimate::getRoofManufacturers(), 'roof_product_lines' => Estimate::getRoofProductLines(), 'roof_warranties' => Estimate::getRoofWarranties()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /* First, we valdiate & dump all the final data into the dB */

        $request->validate([
            'new_roofing_material' => 'required|numeric',
            'roof_product_manufacturer' => 'required|numeric',
            'roof_product_line' => 'required|numeric',
        ]);

        $estimate = Estimate::create([
            'client_id' => 1,
            'user_id' => Auth::id(),
            'name' => '', //should input this or ?  ## Should have an input field for this on the previous view but call it "Main Estimate" by default to support multiple estimates and to keep it organized with a name.
            'structure_id' => $request->session()->get('estimate_structure_id'),
            'roof_type_id' => $request->new_roofing_material,
            'roof_manufacturers_id' => $request->roof_product_manufacturer,
            'roof_manufacturers_lines_id' => $request->roof_product_line,
            'insurance' => $request->insurance_value == 0 ? 'No' : 'Yes',                // Nice Tyler, this syntax is new to me.
            'financing' => $request->financing_value == 0 ? 'No' : 'Yes',
        ]);

        $request->session()->put('estimate_id', $estimate->id);
        $request->session()->put('estimate._id', $estimate->id);
        $request->session()->put('estimate.roof_type_id', $request->new_roofing_material);
        $request->session()->put('estimate.roof_manufacturers_id', $request->roof_product_manufacturer);
        $request->session()->put('estimate.roof_manufacturers_lines_id', $request->roof_manufacturers_lines_id);
        $request->session()->put('estimate.insurance', $estimate->insurance);
        $request->session()->put('estimate.financing', $estimate->financing);

        /* Here is where we take all of the information in dB to calculate the quantity
        of materials to be used in the calculation of the cost of labor and material,
        when considering waste factors, roof pitch, permitting and profit margins on a
        per material basis.  This is the competitive advantage of the Platform.
        */

        /* NOTE: Right now, the Platform is built for shingle estimates only - Client's main line of business */

        return redirect('/estimator/estimate-figures');
    }

    public function showCalculationResult(Request $request)
    {
        // calculate values by estimate_id
        // $request->session()->get('estimate_id');
        // can get values from session : structure_id, project_id, roof_measurements_id

        // $request->session()->get('estimate_structure_id');
        // $request->session()->get('estimate_project_id');
        // $request->session()->get('estimate_roof_measurements_id');

        $estimate = DB::table('estimates')->select('*')->where('estimates.id', $request->session()->get('estimate_id'))->first();
        $structure = DB::table('structures')->select('*')->where('structures.id', $request->session()->get('estimate_structure_id'))->first();
        $roof_measurement = DB::table('roof_measurements')->select('*')->where('roof_measurements.id', $structure->roof_measurements_id)->first();
        
        $project = Project::find($request->session()->get('project_id'));

        #$classification = DB::table('classifications')->select('*')->where('classifications.id', $project->classification_id);
        #$project_type = DB::table('project_types')->select('*')->where('project_types.id', $project->project_types_id);

        #print_r($estimate);
        #exit();

        #$roof_measurement->squares = ($roof_measurement->square_feet/100);          // Do not round-up, only round-up after waste factor is applied
        #dd($roof_measurement->squares);
        #exit();
        
        $request->session()->put('estimate.squares', ($roof_measurement->square_feet / 100));
        $waste_factor = DB::table('waste_factors')->select('*')
            ->where('waste_factors.roof_type_id', $estimate->roof_type_id)
            ->first();

        #dd($waste_factor);
        #exit();
        
        if ($waste_factor && $waste_factor->overall_factor) {        // If client has overall waste that applies to materials and labor the same
            $waste_factor->material_factor = $waste_factor->labor_factor = $waste_factor->overall_factor;
        }

        #dd(session()->all());
        #exit();

        // Labor Cost Calculation - the Program finds the correct line item based on tear-off and replacement material, along with pitch and type of project (new, re-roof, etc.)



        if ($project->project_type == 'New Construction') {
            $labor = DB::table('labor')->select('*')
                ->where('labor.project_types_id', $project->project_types_id)->get();    // Not using ->get(1) so we can troubleshoot multiple returns
            $labor->total_cost = (1 + $waste_factor->labor_factor) * $labor->cost;
            $boots = 'NULL';               // Do Blind boot count since new construction
        } elseif ($project->project_type == 'Reroof') {       // Now, look for re-roof project type

            // Let's first get the dump fee

            // Let's check to see if a pitch-specific price exists in the labor dB already for this project type, pitch, tear-off and install material

            $match = DB::table('labor')->select('*')
                ->where('labor.project_types_id', '=', $project->project_types_id)// Match to reroof (tear off and install)
                ->where('labor.pitch_id', '=', $structure->pitch_types_id)// Match to pitch (if it exists in materials)
                ->where('labor.tearoff_roof_type_id', '=', $project->present_roof_types_id)// Match to present roof material
                ->where('labor.new_roof_type_id', '=', $estimate->roof_type_id)// Match to new roof material
                ->count();

            if ($match == '1') {                                                // This roof (or facet) is price-specific!
                $labor = DB::table('labor')->select('*')->where($where_conditions)->first();

                $labor->total_cost = (1 + $waste_factor->labor_factor) * $labor->cost * $roof_measurement->squares;

                # Add labor line item to the build_sheet for this roof, and we will need one of these damn things for every single facet WITH A DIFFERENT ROOF PITCH.... yes, a roof can have MULTIPLE pitches, and each pitch (CAN POSSIBLY) impact the cost of labor.  (Think about it - wouldn't you charge more for working and walking on a 45 degree angle 12/12 pitch versus a flat roof with no pitch?)  The labors WILL DO THE MATH on the squares for each pitch on the roof, so the system MUST do the same.
                # dd($labor->id);
                # exit();

                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'labor_id' => $labor->id,
                    'labor_quantity' => $roof_measurement->squares,
                    'waste_factor' => $waste_factor->labor_factor,
                    'total_cost' => $labor->total_cost,
                ]);

            } elseif ($match == '0') {

                $match2 = DB::table('labor')->select('*')
                    ->where('labor.project_types_id', '=', $project->project_types_id)// Match to reroof (tear of and install)
                    ->where('labor.tearoff_roof_type_id', '=', $project->present_roof_types_id)// Match to present roof material
                    ->where('labor.new_roof_type_id', '=', $estimate->roof_type_id)// Match to new roof material
                    ->whereNull('pitch_id')// Match to no pitch-price specitivity
                    ->count();
                #dd($match2);
                #exit();

                if ($match2 != '1') {
                    // Big problem here, should never have anything but a single match!
                } else {
                    // Only one match, so this is our labor unit
                    $labor = DB::table('labor')->select('*')
                        ->where('labor.project_types_id', '=', $project->project_types_id)// Match to reroof (tear of and install)
                        ->where('labor.tearoff_roof_type_id', '=', $project->present_roof_types_id)// Match to present roof material
                        ->where('labor.new_roof_type_id', '=', $estimate->roof_type_id)// Match to new roof material
                        ->whereNull('pitch_id')// Match to no pitch-price specitivity
                        ->first();
                    #dd($labor);
                    #exit();

                    $labor->total_cost = (1 + $waste_factor->labor_factor) * $labor->cost * ($roof_measurement->square_feet / 100);
                    $labor->total_cost = (1 + $waste_factor->labor_factor) * $labor->cost * ($roof_measurement->square_feet / 100);

                    $build_sheet = BuildSheet::create([
                        'estimate_id' => $request->session()->get('estimate_id'),
                        'labor_id' => $labor->id,
                        'labor_quantity' => ($roof_measurement->square_feet / 100),
                        'waste_factor' => $waste_factor->labor_factor,
                        'total_cost' => $labor->total_cost
                    ]);
                    #dd($build_sheet);
                    #exit();

                }

            } else {
                // Big problem here -> should never have more than 1 match!
            }

            ##  Now let's add in the dump fees
            $dump_fees = DB::table('dump_fees')->select('*')
                ->where('dump_fees.roof_types_id', '=', $project->present_roof_types_id)// Match to roof type
                ->first();
            #dd($dump_fees);
            #exit();
            #MATH HERE FOR INSERTION BELOW

            #$build_sheet = BuildSheet::create([
            #    'estimate_id' => $request->session()->get('estimate_id'),
            #    'other_costs_id' => '6',            // ID for Dump Fees in project_fees
            #    'other_costs_quantity' => ($roof_measurement->square_feet/100),
            #    'total_cost' => $labor->total_cost
            #]);

        } elseif ($project->project_type == "Repair") {
            // This is custom time, not pre-fixed pricing, so do nothing to calculate labor since we can't.
        } else {
            // Must be "Inspection", which has no labor and no materials and is fixed at $200 inspection fee.
        }

        // Now calculate the material items, quantities and total cost - we look at roof_components to get a list of the components to deterimine quanities as driven by square feet (squares) or lineal feet of something by looking at a location (table.column) in the dB and loop through each //

        goto skip;

        $roof_components = DB::table('roof_components')->where('link_estimating', '=', 'yes')->get();

        foreach ($roof_components as $component) {
            if (is_null($component->divisor)) {
                $component->divisor = '1';              // If component divisor is NULL, set to '1'.  This is used to convert from square_feet to squares by / 100
            }


            $materials = DB::table('materials')
                ->where('roof_measurements_column', '=', $component->roof_measurements_column)
                #->where([['roof_types_id', '=', 'NULL'], ['roof_types_id', '=', $estimate->roof_type_id],])   // Only select materials specific to this roof type or used for all roof types
                ->where('roof_types_id', '=', $estimate->roof_type_id)->orwhereNull('roof_types_id')// Only select materials specific to this roof type or used for all roof types
                ->where('active', '=', 'Yes')// Only select active materials from the materials list
                ->get();

            #dd($materials);

            foreach ($materials as $material) {
                if ($material->factor_value > '0') {
                    #dd($request->session()->get('roof_measurements.id'));
                    #exit();
                    $figure = DB::table('roof_measurements')->select($component->roof_measurements_column)
                        ->where('id', '=', $request->session()->get('roof_measurements.id'))->pluck($component->roof_measurements_column);
                    $material->quantity_needed = ceil((($figure[0] / $component->divisor) * (1 + $waste_factor->material_factor)) / $material->factor_value);               // This is dynamic based on the roof_component and the materials that fall within this component, and we always round-up after applying the waste factor
                    $material->total_cost = $material->quantity_needed * $material->cost;
                    $build_sheet = BuildSheet::create([
                        'estimate_id' => $request->session()->get('estimate_id'),
                        'material_id' => $material->id,
                        'material_quantity' => $material->quantity_needed,
                        'waste_factor' => $waste_factor->material_factor,
                        'total_cost' => $material->total_cost,
                    ]);
                }
            }
        }

        skip:
        
        $build_list = DB::table('roof_build_list')->select(
            'roof_build_list.*',
            'roof_layers.name as layer_name',
            'roof_material_categories.name as name'
            )->join('roof_material_categories', 'roof_build_list.roof_material_categories_id', '=', 'roof_material_categories.id')
            ->join('roof_layers', 'roof_build_list.roof_layers_id', '=', 'roof_layers.id')
            ->where('roof_type_id', '=', '1')->get();

        dd($build_list);
        exit();


        //Done with all auto-material calculations based on roof components

        // Let's get any materials which are included for free!
        #$included_materials = DB::table('materials')->where('included', '=', 'yes')->get();     // Get any materials which are included (free) in each estimate (such as 2 sheets of plywood in the case of Roman Roofing)
        #foreach ($included_materials as $included_item) {
        #    $build_sheet = BuildSheet::create([
        #        'estimate_id' => $request->session()->get('estimate_id'),
        #        'material_id' => $included_item->id,
        #        'material_quantity' => $included_item->quantity_included,
        #    ]);
        #}

        // Get boot costs and vents and add to build sheet
        if ($project->project_type == "Reroof") {
            $boot_1_detail = DB::table('materials')->select('*')->where('id', $structure->boot_1)->first();      // boot_1 is required - all roofs have at least a single penetration
            $build_sheet = BuildSheet::create([
                'estimate_id' => $request->session()->get('estimate_id'),
                'material_id' => $boot_1_detail->id,
                'material_quantity' => $request->session()->get('roof_measurements.boot_1_quantity'),
                'total_cost' => ($boot_1_detail->cost * $request->session()->get('roof_measurements.boot_1_quantity')),
            ]);
            if ($structure->boot_2) {
                $boot_2_detail = DB::table('materials')->select('*')->where('id', $structure->boot_2)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $boot_2_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.boot_2_quantity'),
                    'total_cost' => ($boot_2_detail->cost * $request->session()->get('roof_measurements.boot_2_quantity')),
                ]);
            }
            if ($structure->boot_3) {
                $boot_3_detail = DB::table('materials')->select('*')->where('id', $structure->boot_3)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $boot_3_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.boot_3_quantity'),
                    'total_cost' => ($boot_3_detail->cost * $request->session()->get('roof_measurements.boot_3_quantity')),
                ]);
            }
            if ($structure->boot_4) {
                $boot_4_detail = DB::table('materials')->select('*')->where('id', $structure->boot_4)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $boot_4_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.boot_4_quantity'),
                    'total_cost' => ($boot_4_detail->cost * $request->session()->get('roof_measurements.boot_4_quantity')),
                ]);
            }
            // Now let's check for vents.
            if ($structure->vent_1) {
                $vent_1_detail = DB::table('materials')->select('*')->where('id', $structure->vent_1)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_1_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_1_quantity'),
                    'total_cost' => ($vent_1_detail->cost * $request->session()->get('roof_measurements.vent_1_quantity')),
                ]);
            }
            if ($structure->vent_2) {
                $vent_2_detail = DB::table('materials')->select('*')->where('id', $structure->vent_2)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_2_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_2_quantity'),
                    'total_cost' => ($vent_2_detail->cost * $request->session()->get('roof_measurements.vent_2_quantity')),
                ]);
            }
            if ($structure->vent_3) {
                $vent_3_detail = DB::table('materials')->select('*')->where('id', $structure->vent_3)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_3_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_3_quantity'),
                    'total_cost' => ($vent_3_detail->cost * $request->session()->get('roof_measurements.vent_3_quantity')),
                ]);
            }
            if ($structure->vent_4) {
                $vent_4_detail = DB::table('materials')->select('*')->where('id', $structure->vent_4)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_4_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_4_quantity'),
                    'total_cost' => ($vent_4_detail->cost * $request->session()->get('roof_measurements.vent_4_quantity')),
                ]);
            }
        }


        // Next, we see if the address of the structure is within a permitting jurisdiction
        $permit_count = DB::table('permit_jurisdictions')->select('*')
            ->where('us_cities_id', $request->session()->get('project.city_id'))
            ->count();   // This should return the permitting jurisdiction (if it exists), based on the city, which came from Google (reliable)


        if ($permit_count == '1') {      // We have a match!, but is it a municipal (city) or county permit?
            $permit_details = DB::table('permit_jurisdictions')->select('*')->where('name', $request->session()->get('project.city'))->first();
        } elseif ($permit_count == '0') {

            // No matches for the address in the permitting jurisdictions for the city, so must be county
            $county = DB::table('us_cities')->select('county')->where('id', $request->session()->get('project.city_id'))->first();
            $county = rtrim($county->county, ' County');    // Get rid of 'County' from the name of the County

            $permit_details = DB::table('permit_jurisdictions')->select('*')
                ->where('permit_jurisdictions.name', '=', $county)
                #    ->where('permit_jurisdictions.type', '=', 'county')     // Add condition to type = county also
                ->first();

        } else {
            // Getting here should be impossible
        }

        #dd($permit_details);
        #exit();

        $permit_details->total_fee = '0';
        // Now that we have $permit_details, let's do the math on the permit
        if ($permit_details->permit_fixed_fee) {    // If there is a simple, fixed fee
            $permit_details->total_fee = $permit_details->total_fee + $permit_details->permit_fixed_fee_fee;
        }
        if ($permit_details->permit_tier_fee) {   // If it is a tier fee model
            $permit_tier = DB::table('permit_tier_fee')->select('*')->where('permit_tier_fee.permit_jurisdictions_id', $permit_details->id)->between(CONDITION);  // Fun eloqent SQL here that needs to be written by GCM
            $permit_details->total_fee = $permit_tier->tier_permit_fee;
        }

        if ($permit_details->permit_fee_per_unit) {   // If it is a fee based on squares (only thing supported now)
            $permit_details->total_fee = $permit_details->total_fee + ($permit_details->permit_fee_per_unit_fee * $estimate->squares);
        }

        $build_sheet = BuildSheet::create([
            'estimate_id' => $request->session()->get('estimate_id'),
            'other_costs_id' => '1',
            'other_costs_quantity' => '1',            ## BEWARE - May need to be based on # of structures versus just address!
            'total_cost' => $permit_details->total_fee,
        ]);


        #dd(session()->all());
        #exit();

        // Now let's add all of the other project costs, except for commissions, save that for last
        $other_costs = DB::table('project_fee_types')->select('*')
            ->where('fee_per_project', '<>', 'NULL')
            ->where('active', '=', 'yes')
            ->get();
        foreach ($other_costs as $cost) {
            $build_sheet = BuildSheet::create([
                'estimate_id' => $request->session()->get('estimate_id'),
                'other_costs_id' => $cost->id,
                'other_costs_quantity' => '1',
                'total_cost' => $cost->fee_per_project,
            ]);
        }

        $total_cost = DB::table('build_sheets')->select('*')
            ->where('estimate_id', '=', $request->session()->get('estimate_id'))
            ->sum('total_cost');

        #dd($total_cost);
        $request->session()->put('estimate.total_cost', $total_cost);
        #exit();
        #dd($request->session()->get('estimate_id'));
        #exit();

        # $build_sheet = DB::table('build_sheets')->select(
        #     'build_sheets.*',
        #     'materials.*',
        #     'labor.*'
        # )->join('materials', 'build_sheets.material_id', '=', 'materials.id')
        # ->join('labor', 'build_sheets.labor_id', '=', 'labor.id')
        # ->where('build_sheets.estimate_id', '=', $request->session()->get('estimate_id'))->get();

        $build_sheet_materials = DB::table('build_sheets')->select(
            'build_sheets.*',
            'materials.*'
        )->join('materials', 'build_sheets.material_id', '=', 'materials.id')
            ->where('build_sheets.estimate_id', '=', $request->session()->get('estimate_id'))->get();

        #var_dump($build_sheet_materials);
        #exit();

        // stores in session the calculation result for inserting in pdf

        return view('estimate-figure')->with(['build_sheet_materials' => $build_sheet_materials]);
    }

    public function CacluateCosts($estimate_id)
    {

        // Fetch estimate, and all properties associated with structure (measurements, project)
        // Estimates are done on a per-structure (not address/project) basis

    }

    public function generateProposal(Request $request)
    {

        // var_dump($id);
        if (!$request->session()->get('estimate_id')) {
            return redirect('/estimator/properties');
        }
        $id = 1;

        $template = DB::table('templates')->select('*')
            ->where('templates.name', 'Estimate')->get(1);

        $tokens = DB::table('tokens')->select('*')->get();

        $project_info = DB::table('projects')->select('roof_types.name', 'contacts.email_address', 'clients.company_name', 'users.email')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->join('contacts', 'contacts.id', '=', 'projects.contact1_id')
            ->join('roof_types', 'roof_types.id', '=', 'projects.preferred_roof_types_id')
            ->join('clients', 'clients.id', '=', 'projects.client_id')
            ->where('projects.id', $id)->get()[0];
        // Cycle through all tokens and search/replace from tokens table in all templates
        foreach ($tokens as $token) {
            $replace = $token->token_identifier . $token->token_name . $token->token_identifier;
            $template[0]->email_subject = str_replace("$replace", $project_info->name, $template[0]->email_subject);
            $template[0]->email_message = str_replace("$replace", $project_info->company_name, $template[0]->email_subject);
            $template[0]->sms_message = str_replace("$replace", $project_info->company_name, $template[0]->email_subject);
        }
        $generatePdfFileInfo = $this->generatePDF($request->session()->get('estimate_id'));
        return view('generateProposal', ['template' => $template, 'customer_email' => $project_info->email_address, 'estimator_email' => $project_info->email, 'proposal_pdffilepath' => $generatePdfFileInfo['url'], 'project_id' => $id]);

    }

    public function generateProposalSend(Request $request)
    {
        $attachFiles = [];
        array_push($attachFiles, \base_path('public/uploads/request_proposals/') . basename($request->proposal_filepath));
        if ($request->attachments != null) {
            foreach ($request->attachments as $i) {
                if ($i == "0") {
                    //oil waver pdf
                    array_push($attachFiles, storage_path('app/clients/1/estimate_forms/OilCanningWaiver.pdf'));
                }
            }
        }

        // add token link include project_id
        $document_id = DB::table('documents')->insertGetId(['estimate_id' => $request->session()->get('estimate_id'), 'document_types_id' => 1, 'location' => $request->proposal_filepath, 'require_customer_signature' => 'yes', 'require_customer_initials' => 'yes', 'customer_viewable' => 'yes', 'created_at' => date('Y-m-d h:i:s')]);

        $token_info = [
            'project_id' => $request->id,
            'document_id' => $document_id
        ];

        $token = Token::customPayload($token_info, env('JWT_SECRET'));  //encrypt
        //dd(Token::getPayload($token, env('JWT_SECRET')));   decrypt
        \Mail::to($request->customer_email)->send(new EstimateProposal($request->subject, $request->attachtext, $attachFiles, url("/") . "/customersign/" . $token));
        if (\Mail::failures()) {
            return 'fail';
        }

        return 'success';
    }

    public function generatePDF($estimate_id)
    {
        $pdf = new \setasign\Fpdi\Fpdi();
        $project_info = DB::table('estimates')
            ->select('projects.id as project_id', 'contacts.first_name', 'contacts.last_name', 'contacts.cell_phone as cus_cell_phone', 'contacts.email_address', 'addresses.number', 'addresses.street', 'us_cities.name as city', 'states.name as state', 'users.username', 'users.cell_phone as user_cell_phone', 'users.email')
            ->join('structures', 'estimates.structure_id', '=', 'structures.id')
            ->join('projects', 'structures.project_id', '=', 'projects.id')
            ->join('contacts', 'projects.contact1_id', '=', 'contacts.id')
            ->join('addresses', 'contacts.address_id', '=', 'addresses.id')
            ->join('us_cities', 'addresses.city_id', '=', 'us_cities.id')
            ->join('states', 'addresses.state_id', '=', 'states.id')
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->where('estimates.id', $estimate_id)->get()[0];

        $pageCount = $pdf->setSourceFile("$_SERVER[DOCUMENT_ROOT]/../storage/app/clients/1/estimate_forms/GAF_GP.pdf");

        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $pdf->AddPage();
            $pdf->useTemplate($templateId, ['adjustPageSize' => true]);
            $pdf->SetFont('Helvetica', '', 10);
            $pdf->SetTextColor(0, 0, 0);
            if ($pageNo == 1) {
                //date
                $pdf->SetXY(20, 37.5);
                $pdf->Write(0, date('Y-m-d'));
                //project_id
                $pdf->SetXY(129, 37.5);
                $pdf->Write(0, '# ' . $project_info->project_id);
                //customer name
                $pdf->SetXY(7, 48);
                $pdf->Write(0, $project_info->first_name . " " . $project_info->last_name);
                //customer's phone
                $pdf->SetXY(58.5, 48);
                $pdf->Write(0, $project_info->cus_cell_phone);
                //customer's email
                $pdf->SetXY(111, 48);
                $pdf->Write(0, $project_info->email_address);
                //customer's address
                $pdf->SetXY(6.5, 58.5);
                $pdf->Write(0, $project_info->street . " " . $project_info->number . " " . " " . $project_info->city . " " . $project_info->state);
                //customer's project address
                $pdf->SetXY(6.5, 70);
                $pdf->Write(0, $project_info->street . " " . $project_info->number . " " . " " . $project_info->city . " " . $project_info->state);
                //roman rep's name
                $pdf->SetXY(6.5, 80.5);
                $pdf->Write(0, $project_info->username);
                //roman rep's phone
                $pdf->SetXY(58.5, 80.5);
                $pdf->Write(0, $project_info->user_cell_phone);
                //roman rep's email
                $pdf->SetXY(111, 80.5);
                $pdf->Write(0, $project_info->email);

                $pdf->AddFont('AnthoniSignature', '', 'AnthoniSignature.php');

                $pdf->SetFont('AnthoniSignature', '', 20);
                //Authorized Signature
                $pdf->SetXY(118, 179);
                $pdf->Write(0, $project_info->username);

                // $pdf->SetFont('Helvetica','',10);
                // Date will be written when user sign
                // $pdf->SetXY(115, 197.5);
                // $pdf->Write(0, '2020-02-29');

            }

        }
        // insert in db this generated pdf file path
        $now = DateTime::createFromFormat('U.u', microtime(true));
        $proposal_pdf_name = $now->format("Y-m-d_H-i-s_u") . '.pdf';
        // if directory doens't exist, create
        if (!File::isDirectory(public_path() . '/uploads/request_proposals')) File::makeDirectory(public_path() . '/uploads/request_proposals', 0777, true, true);
        // save in public/uploads/request_proposals/ ...pdf
        $pdf->Output('F', public_path() . "/uploads/request_proposals/" . $proposal_pdf_name, true);
        // return saved file path
        return ['url' => url('uploads/request_proposals') . "/" . $proposal_pdf_name];
    }

    public function customersign(Request $request)
    {
        $token = $request->token;
        $customer_info = Token::getPayload($token, env('JWT_SECRET'));

        $customer_email = DB::table('projects')->select('contacts.email_address')->join('contacts', 'contacts.id', '=', 'projects.contact1_id')->where('projects.id', $customer_info['project_id'])->get()[0]->email_address;
        $random_pin = mt_rand(100000, 999999);
        $request->session()->put('verify_pin', $random_pin);
        \Mail::to($customer_email)->send(new SendPinCodeForSign($random_pin));
        if (\Mail::failures()) {
            return 'fail';
        }

        $document_path = DB::table('documents')->select('location as pdf_filepath')->where('id', $customer_info['document_id'])->get(0);
        $customer_name = DB::table('projects')->select('contacts.first_name', 'contacts.last_name')->join('contacts', 'contacts.id', '=', 'projects.contact1_id')->where('projects.id', $customer_info['project_id'])->get();

        return view('customersign', ['customer_name_first' => $customer_name[0]->first_name, 'customer_name_last' => $customer_name[0]->last_name, 'document_path' => $document_path[0]->pdf_filepath, 'customer_info' => $customer_info]);
    }

    public function customerVerify(Request $request)
    {
        $pincode = $request->session()->get('verify_pin');

        if ($pincode == $request->pincode) {
            return 'success';
        } else return 'fail';
    }

    public function customerDosign(Request $request)
    {
        // dd($request->sign_info);
        $document_path = DB::table('documents')->select('location')->where('id', $request->sign_info['document_id'])->get()[0]->location;
        // require_once('../vendor/setasign/setapdf-stamper_eval_ioncube_php7.1/library/SetaPDF/Autoload.php');
        // $writer = new \SetaPDF_Core_Writer_Http($document_path, true);
        // $document = \SetaPDF_Core_Document::loadByFilename(\base_path('public/uploads/request_proposals/'.basename($document_path)), $writer);
        // $stamper = new \SetaPDF_Stamper($document);
        // dd($stamper);
        // dd($document_path);
        // dd($request->sign_image);
        if (!File::isDirectory(public_path() . '/uploads/customer_manualsign')) File::makeDirectory(public_path() . '/uploads/customer_manualsign', 0777, true, true);
        $now = DateTime::createFromFormat('U.u', microtime(true));
        $customer_signpng_name = public_path() . '/uploads/customer_manualsign/' . $now->format("Y-m-d_H-i-s_u") . '.png';
        file_put_contents($customer_signpng_name, base64_decode($request->sign_image[1]));

        $pdf = new \setasign\Fpdi\Fpdi();

        $pageCount = $pdf->setSourceFile(\base_path('public/uploads/request_proposals/' . basename($document_path)));
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $pdf->AddPage();
            $pdf->useTemplate($templateId, ['adjustPageSize' => true]);

            $pdf->SetTextColor(0, 0, 0);
            if ($pageNo == 1) {

                if ($request->sign_type < 2) {
                    $pdf->AddFont('AnthoniSignature', '', 'AnthoniSignature.php');

                    $pdf->SetFont('AnthoniSignature', '', 15);
                    // Authorized Signature
                    if ($request->sign_type == 0) {
                        $pdf->SetXY(35, 179);
                    } else $pdf->SetXY(33, 179);

                    $pdf->Write(0, $request->sign_name);
                } else $pdf->Image($customer_signpng_name, 21.3, 172, 48.3, 14.8);

                $pdf->SetFont('Helvetica', '', 10);
                //customer date Signature
                $pdf->SetXY(36.5, 197.5);
                $pdf->Write(0, date('Y-m-d'));   // From here, Gabriel can do all of the writing (pull data from Db and place at xy coordinates on PDF)
                //Authorized date sign as customer's sign
                $pdf->SetXY(115, 197.5);
                $pdf->Write(0, date('Y-m-d'));

            }
        }
        $PublicIP = $request->ip();
        $json = file_get_contents("http://ipinfo.io/$PublicIP/geo");
        $json = json_decode($json, true);

        //signed timestamp
        $pdf->AddPage('P', 'A5');
        $pdf->SetXY(30, 30);
        $pdf->Write(0, 'Signed Date : ');
        $pdf->SetXY(55, 30);
        $pdf->Write(0, date('Y-m-d'));

        //signed customer ip
        $pdf->SetXY(30, 40);
        $pdf->Write(0, 'IP :   ' . $PublicIP);

        //geo
        $pdf->SetXY(30, 50);
        $pdf->Write(0, 'GEO Information : ');
        $pdf->SetXY(40, 55);
        $pdf->Write(0, 'City :  ' . $json['city']);
        $pdf->SetXY(40, 60);
        $pdf->Write(0, 'State :  ' . $json['region']);
        $pdf->SetXY(40, 65);
        $pdf->Write(0, 'Country :  ' . $json['country']);
        $pdf->SetXY(40, 70);
        $pdf->Write(0, 'LoC :  ' . $json['loc']);
        $pdf->SetXY(40, 75);
        $pdf->Write(0, 'postal :  ' . $json['postal']);

        //agent detail

        $pdf->SetXY(30, 85);
        $pdf->Write(0, 'Agent Information : ');

        $project_info = DB::table('documents')
            ->select('projects.id as project_id', 'users.username', 'users.cell_phone as user_cell_phone', 'users.email',
                'users.first_name', 'users.last_name', 'users.work_phone as work_phone', 'users.fax_phone as fax_phone', 'clients.company_name')
            ->join('estimates', 'estimates.id', '=', 'documents.estimate_id')
            ->join('structures', 'estimates.structure_id', '=', 'structures.id')
            ->join('projects', 'structures.project_id', '=', 'projects.id')
            ->join('contacts', 'projects.contact1_id', '=', 'contacts.id')
            ->join('addresses', 'contacts.address_id', '=', 'addresses.id')
            ->join('us_cities', 'addresses.city_id', '=', 'us_cities.id')
            ->join('states', 'addresses.state_id', '=', 'states.id')
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->join('clients', 'clients.id', '=', 'users.client_id')
            ->where('documents.id', $request->sign_info['document_id'])->get()[0];

        // dd($project_info);

        $pdf->SetXY(40, 90);
        $pdf->Write(0, 'Compamy Name : ' . $project_info->company_name);
        $pdf->SetXY(40, 95);
        $pdf->Write(0, 'User Name : ' . $project_info->first_name . ' ' . $project_info->last_name);
        $pdf->SetXY(40, 100);
        $pdf->Write(0, 'User Email : ' . $project_info->email);
        $pdf->SetXY(40, 105);
        $pdf->Write(0, 'Cell Phone : ' . $project_info->user_cell_phone);
        $pdf->SetXY(40, 110);
        $pdf->Write(0, 'Work Phone : ' . $project_info->work_phone);
        $pdf->SetXY(40, 115);
        $pdf->Write(0, 'Fax Phone : ' . $project_info->fax_phone);


        $now = DateTime::createFromFormat('U.u', microtime(true));
        $proposal_pdf_name = $now->format("Y-m-d_H-i-s_u") . '.pdf';

        DB::table('documents')->update(['location' => '/uploads/request_proposals/' . $proposal_pdf_name, 'require_customer_signature' => 'no', 'require_customer_initials' => 'no', 'customer_viewable' => 'yes', 'updated_at' => date('Y-m-d h:i:s')]);

        // save in public/uploads/request_proposals/ ...pdf
        $pdf->Output('F', public_path() . "/uploads/request_proposals/" . $proposal_pdf_name, true);
        // return saved file path
        return ['url' => url('uploads/request_proposals') . "/" . $proposal_pdf_name, 'success' => '1'];
    }

}
