<?php

namespace App\Http\Controllers;

use App\Phase;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }
}
