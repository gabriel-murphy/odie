<?php

namespace App\Http\Controllers\DeveloperTools;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class LogsController extends Controller
{
    public function index()
    {
        $logs_dir = storage_path('logs');
        $files = collect(File::files($logs_dir))->each(function ($file){
            return $file->getExtension() == 'log';
        })->map(function ($file){
            $filePath = $file->getPathname();
            return [
                'lastModified' => new Carbon(File::lastModified($filePath)),
                'size' => $this->getSize($filePath),
                'name' => File::basename($filePath),
            ];
        });

        return view('developer-tools.logs.index', compact('files')) ;
    }

    public function show($file)
    {
        $filePath = storage_path('logs/'. $file);

        if (!File::exists($filePath)){
            return redirect()->back()->with('error', 'File Not Found!');
        }


        $file_content = \file($filePath);

        $line_numbers = array();

        foreach ($file_content as $line_number => $line){
            if (! Str::startsWith($line, '[')){
                continue;
            }

            $date = Str::between(Str::words($line, 2, ''), '[', ']');

            if (strtotime($date)){
                $line_numbers[] = $line_number;
            }
        }
        $last_line = count($file_content) - 1;
        $last_n_errors = array_slice($line_numbers, -10);

        $logs = array();

        for($i= count($last_n_errors)-1; $i> -1; $i--){
            if ($i == count($last_n_errors)-1){
                $logs[] = range($last_n_errors[$i], $last_line);
            } else {
                $logs[] = range($last_n_errors[$i], $last_n_errors[$i+1]);
            }
        }

        $data = [
            'lastModified' => new Carbon(File::lastModified($filePath)),
            'size' => $this->getSize($filePath),
            'file' => File::get($filePath),
            'file_content' => $file_content,
            'logs' => $logs
        ];

        return view('developer-tools.logs.show', $data) ;
    }

    public function truncate($file)
    {
        $filePath = storage_path('logs/'. $file);
        if (!File::exists($filePath)){
            return redirect()->back()->with('error', 'File Not Found!');
        }

        File::put($filePath, '');
        return redirect()->back()->with('info', 'File Truncated');
    }

    private function getSize($file)
    {
        $bytes = File::size($file);
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
