<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HarvesterController extends Controller
{

    public function candidates()
    {
        $candidates = array();
        return view('harvester.candidates', ['candidates' => $candidates]);
    }
}
