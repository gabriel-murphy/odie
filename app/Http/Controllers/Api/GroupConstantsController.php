<?php

namespace App\Http\Controllers\Api;

use App\Constant;
use App\ConstantGroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupConstantsController extends Controller
{

    public function index(ConstantGroup $constantGroup)
    {
        $constant = $constantGroup->constants()->get();
        return response($constant);
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, ConstantGroup $constantGroup, Constant $constant)
    {
        $data['name'] = $request->name;
        $data['value'] = $request->value;
        $data['additional_data'] = $request->additional_data;
        $constant->update($data);
        return $this->index($constantGroup);
    }


    public function destroy($id)
    {
        //
    }

    public function group()
    {
        $q = request()->query('q');
        $constantGroups = ConstantGroup::whereAnyColumnLike($q)->get();
        return response($constantGroups);
    }

    public function changeStatus(ConstantGroup $constantGroup, Constant $constant)
    {
        $constant->update([
            'active' => !$constant->active
        ]);
        return $this->index($constantGroup);
    }

    public function upload(Request $request)
    {
        foreach ($request->files as $input => $file){
            if (is_array($request->{$input})){
                foreach ($request->{$input} as $_file){
                    $_file->storeAs("temp/{$request->folderName}", "{$input}_{$_file->getClientOriginalName()}");
                }
            } else {
                $request->{$input}->storeAs("temp/{$request->folderName}", "{$input}_{$request->{$input}->getClientOriginalName()}");
            }
        }

        return response()->json([
            'success' => 'true',
        ]);
    }
}
