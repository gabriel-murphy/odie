<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\InsuranceCarrier;
use App\MortgageCompany;
use Illuminate\Http\Request;

class MortgageCompanyController extends Controller
{

    public function index()
    {
        //
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $mortgageCompany = MortgageCompany::create($request->all());
        return response($mortgageCompany);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
