<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\InsuranceCarrier;
use Illuminate\Http\Request;

class InsuranceCarrierController extends Controller
{

    public function index()
    {
        //
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'website' => 'required',
            'phone' => 'required',
            'claims_phone' => 'required',
        ]);

        $insuranceCarrier = InsuranceCarrier::create($request->all());
        return response($insuranceCarrier);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
