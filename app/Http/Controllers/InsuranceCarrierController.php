<?php

namespace App\Http\Controllers;

use App\InsuranceCarrier;
use Illuminate\Http\Request;

class InsuranceCarrierController extends Controller
{

    public function index()
    {
        $q = request()->query('q');
        $insurance_carriers = InsuranceCarrier::whereAnyColumnLike($q)->paginate(15);
        return view('insurance-carrier/index', compact('insurance_carriers'));
    }


    public function create()
    {
        return view('insurance-carrier/create');
    }


    public function store(Request $request)
    {
        InsuranceCarrier::create($request->all());
        return redirect()->route('insurance_carriers.index')->with('success', 'Created Successfully');
    }


    public function show(InsuranceCarrier $insuranceCarrier)
    {
        return view('insurance-carrier/show', compact('insuranceCarrier'));
    }


    public function edit(InsuranceCarrier $insuranceCarrier)
    {
        return view('insurance-carrier/edit', compact('insuranceCarrier'));
    }


    public function update(Request $request, InsuranceCarrier $insuranceCarrier)
    {
        $insuranceCarrier->update($request->all());
        return redirect()->route('insurance_carriers.index')->with('success', 'Updated Successfully');

    }


    public function destroy(InsuranceCarrier $insuranceCarrier)
    {
        $insuranceCarrier->delete();
        return redirect()->back()->with('success', 'Removed Successfully');
    }
}
