<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Estimate;
use App\Notifications\AppointmentCreated;
use App\Services\Datalab\Datalab;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class PlayGroundController extends Controller
{
    public function index()
    {
        $estimate = Estimate::with('buildSheets')->first();
        return view('estimator.estimate', compact('estimate'));
    }

    public function appointmentMail()
    {
        $appointment = Appointment::first();

        \Notification::send($appointment->attendees, new AppointmentCreated($appointment));
        return 'success';
    }

    public function designCanvas()
    {
        return view('playground.canvas');
    }

    public function sms(Request $request)
    {
        $account_sid = env("TWILIO_SID");
        $auth_token = env("TWILIO_AUTH_TOKEN");
        $twilio_number = env("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);
        $client->messages->create($request->number, ['from' => $twilio_number, 'body' => $request->message] );
        return redirect()->back()->with('success', 'SMS sent successfully');
    }
}
