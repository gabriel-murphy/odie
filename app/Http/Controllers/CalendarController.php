<?php

namespace App\Http\Controllers;

use App\Constant;
use App\User;
use Illuminate\Http\Request;
use App\Appointment;
use App\Client;
use App\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = array();
        $data['appointments'] = Appointment::getAppointments();
        $data['estimators'] = User::role('estimator')->get();
        $data['teams'] = Client::getTeams();
        $data['projects'] = Project::whereNull('evaluation_date')->get();
        $data['latestproject'] = $data['projects']->first();
        $data['appointments_types'] = Constant::group('appointment_types')->get();
        $data['appointments_without_project'] = Appointment::cancelled(false)->doesntHave('project')->get();
        $data['target_project_enabled'] = 0;

        return view('calendar', $data);
    }

    public function addappointment(Request $request){

        $project_id = $request->project_id;
        if ($project_id == -1){
            $project_id = null;
        }
        $title = $request->title;
        $estimator_user_id = $request->estimator_user_id;
        $secondary_user_id = $request->secondary_user_id;
        $notes = $request->notes;
        $start_timestamp = $request->start_timestamp;
        $end_timestamp = $request->end_timestamp;
        
        if ($secondary_user_id && count((array)$secondary_user_id) > 0 ){
            $secondary_user_id = $secondary_user_id[0];
        }
        else $secondary_user_id = null;
        
        if ($secondary_user_id && count((array)$secondary_user_id) > 1 ){
            $tertiary_user_id = $secondary_user_id[1];
        }
        else $tertiary_user_id = null;
        DB::table('appointments')->insert([
            'client_id' => 1,
            'project_id' => $project_id,
            'created_by' => Auth::id(),
            'appointment_type_id' => $request->appointment_type,
            'title' => $title,
            'estimator_id' => $estimator_user_id,
            'secondary_user_id' => $secondary_user_id,
            'tertiary_user_id' => $tertiary_user_id,
            'notes' => $notes,
            'start_time' => $start_timestamp,
            'end_time' => $end_timestamp
        ]);

        DB::table('projects')->where('id', $project_id)->update(['evaluation_date' => now(), 'phase_id' => 3]);     // Update the dB to let the Platform know the evaluation was set now() and update the phase now that appointment is scheduled

        return 1;
    }

    public function editappointment_date(Request $request){
        $appointment_id = $request->appointment_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        DB::table('appointments')->where('id', $appointment_id)->update(['date'=>$start_date, 'start_time' => $start_date, 'end_time' => $end_date]);
        return 'success';
    }

    public function editappointment_content(Request $request){
        $appointment_id = $request->appointment_id;
        $title = $request->title;
        $estimator_user_id = $request->estimator_user_id;
        $secondary_user_id = $request->secondary_user_id;
        $notes = $request->notes;
        $start_timestamp = $request->start_timestamp;
        $end_timestamp = $request->end_timestamp;

        if ($secondary_user_id && count($secondary_user_id) > 0 ){
            $secondary_user_id_0 = $secondary_user_id[0];
        }
        else $secondary_user_id_0 = null;
        if ($secondary_user_id && count($secondary_user_id) > 1 ){
            $secondary_user_id_1 = $secondary_user_id[1];
        }
        else $secondary_user_id_1 = null;

        DB::table('appointments')->where('id', $appointment_id)->update([
            'title' => $title,
            'estimator_id' => $estimator_user_id,
            'appointment_type_id' => $request->appointment_type,
            'secondary_user_id' => $secondary_user_id_0,
            'tertiary_user_id' => $secondary_user_id_1,
            'notes' => $notes,
            'start_time' => $start_timestamp,
            'end_time' => $end_timestamp
        ]);
        return 'success';
    }

    public function deleteappointment (Request $request){
        $appointment_id = $request->appointment_id;
        $project_id = DB::table('appointments')->select('project_id')->where('id',$appointment_id)->first()->project_id;
        DB::table('projects')->where('id', $project_id)->update(['evaluation_date' => null, 'phase_id' => 1]);
        
        DB::table('appointments')->where('id', $appointment_id)->delete();

        return 'success';
    }

    public function scheduler (Project $project){
        $data = array();

        $data['appointments'] = Appointment::cancelled(false)->get();
        $data['appointments_without_project'] = Appointment::cancelled(false)->doesntHave('project')->get();
        $data['projects'] = Project::whereNull('evaluation_date')->get();
        $data['latestproject'] = $project;
        $data['target_project_enabled'] = 1;
        $data['appointments_types'] = Constant::group('appointment_types')->get();
        $data['teams'] = Client::getTeams();
        $data['estimators'] = User::role('estimator')->get();
        return view('calendar', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
