<?php

namespace App\Http\Controllers;

use App\Material;
use App\RoofLayer;
use App\RoofType;
use Illuminate\Http\Request;

class BuildListController extends Controller
{
    public function index()
    {
        $roofTypes = RoofType::all();
        return view('estimator.buildlist.index', compact('roofTypes'));
    }

    public function create()
    {
        $roofLayers = RoofLayer::all();
        $materials = Material::all();
        return view('estimator.buildlist.create', compact('roofLayers', 'materials'));
    }
}
