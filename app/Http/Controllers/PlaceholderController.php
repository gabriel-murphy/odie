<?php

namespace App\Http\Controllers;

use App\Placeholder;
use Illuminate\Http\Request;

class PlaceholderController extends Controller
{

    public function index()
    {
        $q = request()->query('q');
        $placeholders = Placeholder::whereAnyColumnLike($q)->latest('id')->paginate(15);
        return view('placeholder.index', compact('placeholders'));
    }


    public function create()
    {
        $placeholder=new Placeholder();
        return view('placeholder.create', compact('placeholder'));
    }


    public function store(Request $request)
    {
        Placeholder::create($request->all());
        return redirect()->route('placeholders.index')->with('success', 'Created Successfully');
    }


    public function show($id)
    {
        //
    }


    public function edit(Placeholder $placeholder)
    {
        return view('placeholder.edit', compact('placeholder'));
    }


    public function update(Request $request, Placeholder $placeholder)
    {
        $placeholder->update($request->all());
        return redirect()->route('placeholders.index')->with('success', 'Updated Successfully');
    }


    public function destroy(Placeholder $placeholder)
    {
        $placeholder->delete();
        return redirect()->back()->with('success', 'Removed Successfully');
    }
}
