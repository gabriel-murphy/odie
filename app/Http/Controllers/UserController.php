<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $q = request()->query('q');
        $users = User::whereAnyColumnLike($q)->latest()->paginate(10);
        return view('user.index', compact('users'));
    }

    public function create()
    {
       return view('user.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'signature_image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'avatar' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'username'=>'required|unique:users',
            'email'=>'required|string',
            'secondary_email'=>'required|string',
            'cell_phone'=>'required',
            'home_phone'=>'required',
            'work_phone'=>'required',
            'password'=>'required|min:8',
        ]);

        $request->merge(['client_id' => 1, 'password' => bcrypt($request->password)]);
        $user = User::create($request->except(['signature_image', 'avatar']));

        $user->uploadImage('signature_image', 'images/signatures');
        $user->uploadImage('avatar', 'images/users');

        return redirect(route('users.index'))->with('success','User Added Successfully!');
    }

    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'signature_image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'avatar' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'email'=>'required|string',
            'secondary_email'=>'required|string',
            'cell_phone'=>'required',
            'home_phone'=>'required',
            'work_phone'=>'required',
            'password'=>'nullable|min:8',
        ]);

        if($password = $request->input('password')) $user->update(['password' => bcrypt($request->password)]);

        $user->uploadImage('signature_image', 'images/signatures');
        $user->uploadImage('avatar', 'images/users');

        $user->update($request->except(['signature_image', 'avatar', 'password', 'username']));

        return redirect()->back()->with('info','User Updated Successfully!');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with("success","User Deleted Successfully!");
    }
}
