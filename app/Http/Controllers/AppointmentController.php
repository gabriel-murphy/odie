<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Client;
use App\Constant;
use App\Mail\TestEmail;
use App\Notifications\AppointmentCreated;
use App\Phase;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Validator;

class AppointmentController extends Controller
{
    public function index()
    {
        $data = array();
        $data['appointments'] = Appointment::cancelled(false)->get();
        $data['estimators'] = User::role('estimator')->get();
        $data['teams'] = Client::getTeams();
        $data['projects'] = Project::whereNull('evaluation_date')->get();
        $data['project'] = $data['projects']->first();
        $data['appointments_types'] = Constant::group('appointment_types')->get();
        $data['appointments_without_project'] = Appointment::cancelled(false)->doesntHave('project')->get();
        $data['events'] = $this->getEvents($data['appointments']);
        $data['target_project_enabled'] = 0;

        return view('appointment.index', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'appointment_type_id' => 'required',
            'estimator_id' => 'required',
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        Project::where('id', $request->project_id)->update(['phase_id' => Phase::where('name', 'Appointments')->first()->id]);
        $request->merge(['created_by' => auth()->id()]);
        $appointment = Appointment::create($request->all());
        $appointment->attendees()->attach($request->attendees);

        \Notification::send($appointment->attendees, new AppointmentCreated($appointment));

        return response()->json(['success' => true]);

    }

    public function show(Appointment $appointment)
    {
        return response()->json($appointment);
    }

    public function update(Request $request, Appointment $appointment)
    {
        $validator = Validator::make($request->all(), [
            'project_id' => 'required',
            'appointment_type_id' => 'required',
            'estimator_id' => 'required',
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 422);
        }
        $appointment->update($request->all());
        return response()->json(['success' => true]);
    }

    public function destroy(Appointment $appointment)
    {
        $appointment->delete();
        return response(['success' => true]);
    }

    public function updateTime(Request $request, Appointment $appointment)
    {
        $appointment->update([
            'date' => $request->date,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time
        ]);
        return response()->json(['success' => true]);
    }

    private function getEvents($appointments)
    {
        $events = array();
        foreach ($appointments as $appointment)
        {
            $events[] = [
                'title' => $appointment->project->label,
                'start' => $appointment->start_timestamp,
                'end' => $appointment->end_timestamp,
                'backgroundColor' => "#".$appointment->estimator->hex_color,
                'borderColor' => "#".$appointment->estimator->hex_color,
                'appointment_id' => $appointment->id,
                'appointment' => $appointment
            ];
        }
        return json_encode($events);
    }
}
