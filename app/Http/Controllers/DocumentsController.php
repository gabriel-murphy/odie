<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Project;
use App\Estimate;
use App\BuildSheet;
use File;
use \DateTime;
use \Nexmo;
use \Nexmo\Call\Call;
use PHPMailer\PHPMailer\Exception;
use ReallySimpleJWT\Encode;
use ReallySimpleJWT\Token;
use App\Mail\EstimateProposal;


class DocumentsController extends Controller
{

    public function index()
    {
        $documents = DB::table('documents')
        ->select('documents.created_at as created_date', 
        'document_types.name as docTypeName', 
        'documents.require_customer_signature',
        'documents.location as filename',
        'contacts.email_address as customer_email',
        'contacts.first_name',
        'contacts.last_name',
        'contacts.cell_phone'
        )
        ->join('document_types', 'document_types.id', '=', 'documents.document_types_id')
        ->join('estimates', 'estimates.id', '=', 'documents.estimate_id')
        ->join('structures', 'estimates.structure_id', '=', 'structures.id')
        ->join('projects', 'projects.id', '=', 'structures.project_id')
        ->join('contacts', 'contacts.id', '=', 'projects.contact1_id')
        ->get();
        return view('docs', ['documents' => $documents]);
    }

    public function numbers()
    {
        $nexmo = app('Nexmo\Client');
        Nexmo::message()->send([
    'to'   => '12393242365',
#    'to'   => '12399945597',
    'from' => '13312478278',
    'text' => 'Spencer Johnson just e-signed your roofing proposal, a copy of which has been sent to your email and posted in Signer'
]);

        return view('estimate-numbers');
    }

    public function create(Request $request)
    {
        return view('create-estimate',['roofing_materials' => Project::getRoofTypes(), 'roof_manufacturers' => Estimate::getRoofManufacturers(), 'roof_product_lines' => Estimate::getRoofProductLines(), 'roof_warranties' => Estimate::getRoofWarranties()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /* First, we valdiate & dump all the final data into the dB */
        
        $request->validate([
            'new_roofing_material' => 'required|numeric',
            'roof_product_manufacturer' => 'required|numeric',
            'roof_product_line' => 'required|numeric',
        ]);
        
        $estimate = Estimate::create([
            'client_id' => 1,
            'user_id' => Auth::id(),
            'name' => '', //should input this or ?  ## Should have an input field for this on the previous view but call it "Main Estimate" by default to support multiple estimates and to keep it organized with a name.
            'structure_id' => $request->session()->get('estimate_structure_id'),
            'roof_type_id' => $request->new_roofing_material,
            'roof_manufacturers_id' => $request->roof_product_manufacturer,
            'roof_manufacturers_lines_id' => $request->roof_product_line,
            'insurance' => $request->insurance_value == 0? 'No' : 'Yes',                // Nice Tyler, this syntax is new to me.
            'financing' => $request->financing_value == 0? 'No' : 'Yes',
        ]);

        $request->session()->put('estimate_id', $estimate->id);
        $request->session()->put('estimate._id', $estimate->id);
        $request->session()->put('estimate.roof_type_id', $request->new_roofing_material);
        $request->session()->put('estimate.roof_manufacturers_id', $request->roof_product_manufacturer);
        $request->session()->put('estimate.roof_manufacturers_lines_id', $request->roof_manufacturers_lines_id);
        $request->session()->put('estimate.insurance', $estimate->insurance);
        $request->session()->put('estimate.financing', $estimate->financing);

        /* Here is where we take all of the information in dB to calcuate the quantity 
        of materials to be used in the calcuation of the cost of labor and material, 
        when considering waste factors, roof pitch, permitting and profit margins on a 
        per material basis.  This is the competitive advantage of the Platform.
        */

        /* NOTE: Right now, the Platform is built for shingle estimates only - Client's main line of business */















        return redirect('/estimator/estimate-figures');
    }

    public function showCalculationResult(Request $request){
        
        //caculate values by estimate_id
        // $request->session()->get('estimate_id');
        // can get values from session : structure_id, project_id, roof_measurements_id

        // $request->session()->get('estimate_structure_id');
        // $request->session()->get('estimate_project_id');
        // $request->session()->get('estimate_roof_measurements_id');




        $estimate = DB::table('estimates')->select('*')->where('estimates.id', $request->session()->get('estimate_id'))->first();
        $structure = DB::table('structures')->select('*')->where('structures.id', $request->session()->get('estimate_structure_id'))->first();
        $roof_measurement = DB::table('roof_measurements')->select('*')->where('roof_measurements.id', $structure->roof_measurements_id)->first();

        $project = DB::table('projects')->select(
            'projects.*',
            'classifications.name as classification',
            'project_types.name as project_type',
            'addresses.number as number',
            'addresses.street as street',
            'us_cities.name as city',
            'us_cities.id as city_id',
            'states.name as state'
        )->join('classifications', 'projects.classification_id', '=', 'classifications.id')
        ->join('project_types', 'projects.project_types_id', '=', 'project_types.id')
        ->join('addresses', 'projects.project_address_id', '=', 'addresses.id')
        ->join('us_cities', 'addresses.city_id', '=', 'us_cities.id')
        ->join('states', 'addresses.state_id', '=', 'states.id')
        ->where('projects.id', '=', $request->session()->get('project_id'))->first();


#        $classification = DB::table('classifications')->select('*')->where('classifications.id', $project->classification_id);
#        $project_type = DB::table('project_types')->select('*')->where('project_types.id', $project->project_types_id);

        #print_r($estimate);
        #exit();

        $roof_measurement->squares = ($roof_measurement->square_feet/100);          // Do not round-up, only round-up after waste factor is applied
        #dd($roof_measurement->squares);
        #exit();

        $request->session()->put('estimate.squares', $roof_measurement->squares);
        $waste_factor = DB::table('waste_factors')->select('*')->where('waste_factors.roof_type_id', $estimate->roof_type_id)->first();
        if ($waste_factor->overall_factor) {        // If client has overall waste that applies to materials and labor the same
            $waste_factor->material_factor = $waste_factors->labor_factor = $waste_factors->overall_factor;
        }

        #dd(session()->all());
        #exit();

        // Labor Cost Calculation - the Program finds the correct line item based on tear-off and replacement material, along with pitch and type of project (new, re-roof, etc.)

        if ($project->project_type == 'New Construction') {
            $labor = DB::table('labor')->select('*')
                    ->where('labor.project_type_id', $project->project_types_id)->get();    // Not using ->get(1) so we can troubleshoot multiple returns
            $labor->total_cost = (1 + $waste_factor->labor_factor) * $labor->our_cost;
            $boots = 'NULL';               // Do Blind boot count since new construction
        } elseif ($project->project_type == 'Reroof') {       // Now, look for re-roof project type
            // Let's check to see if a pitch-specific price exists in the labor dB already for this project type, pitch, tear-off and install material

            $where_conditions = [
                'labor.project_types_id' => $project->project_types_id,                     // Match to reroof (tear off and install)
                'labor.pitch_id' => $structure->pitch_types_id,                 // Match to pitch (if it exists in materials)
                'labor.tearoff_roof_type_id' => $project->present_roof_types_id,    // Match to present roof material
                'labor.new_roof_type_id' => $estimate->roof_type_id,            // Match to new roof material
            ];
            $match = DB::table('labor')->select('*')->where($where_conditions)->count();
            #dd($match);
            #exit();

            if ($match == '1') {
                $labor = DB::table('labor')->select('*')->where($where_conditions)->first();
                #$labor = DB::table('labor')->select('*')->where('id', '=', $match->id)->get();
                #dd($labor);
                #exit();

                $labor->total_cost = (1 + $waste_factor->labor_factor) * $labor->our_cost * $roof_measurement->squares;

                ## Add labor line item to the build_sheet for this roof, and we will need one of these damn things for every single facet WITH A DIFFERENT ROOF PITCH.... yes, a roof can have MULTIPLE pitches, and each pitch (CAN POSSIBLY) impact the cost of labor.  (Think about it - wouldn't you charge more for working and walking on a 45 degree angle 12/12 pitch versus a flat roof with no pitch?)  The labors WILL DO THE MATH on the squares for each pitch on the roof, so the system MUST do the same.
#                        dd($labor->id);
#                        exit();

                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'labor_id' => $labor->id,
                    'labor_quantity' => $roof_measurement->squares,
                    'waste_factor' => $waste_factor->labor_factor,
                    'total_cost' => $labor->total_cost,
                ]);

                } elseif ($match == '0') {
                    // No matches, so not pitch specific pricing
#                    $where_conditions = [(
#                        ['labor.project_types_id', '=', $project->project_types_id]                         // Match to reroof (tear of and install)
#                        ['labor.tearoff_roof_type_id', '=',  $project->present_roof_types_id]       // Match to present roof material
#                        ['labor.new_roof_type_id', '=', $estimate->roof_type_id]                // Match to new roof material
#                        ['labor.pitch_id', '<>', 'NULL']                                        // Match to no pitch specitivity
#                    )];
                    #dd($where_conditions);
                    #exit();

#                    $match2 = DB::table('labor')->select('*')->where($where_conditions)->count();
                    $match2 = DB::table('labor')->select('*')->where('labor.project_types_id', '=', $project->project_types_id) // Match to reroof (tear of and install)
                    ->where('labor.tearoff_roof_type_id', '=',  $project->present_roof_types_id)                     // Match to present roof material
                    ->where('labor.new_roof_type_id', '=',  $estimate->roof_type_id)                             // Match to new roof material      
                    ->where('labor.pitch_id', '<>',  'NULL')                                                     // Match to no pitch specitivity
                    ->count();             
#                    dd($match2);
#                    exit();

                    if ($match2 != '1') {
                        // Big problem here, should never have anything but a single match!   
                    } else {
                        // Only one match, so this is our labor unit
                        $labor = DB::table('labor')->select('*')->where('labor.project_types_id', '=', $project->project_types_id)  // Match to reroof (tear of and install)
                        ->where('labor.tearoff_roof_type_id', '=',  $project->present_roof_types_id)                     // Match to present roof material
                        ->where('labor.new_roof_type_id', '=',  $estimate->roof_type_id)                             // Match to new roof material      
                        ->where('labor.pitch_id', '<>',  'NULL')                                                     // Match to no pitch specitivity
                        ->first();
                        #dd($labor);
                        #exit();

                        $labor->total_cost = (1 + $waste_factor->labor_factor) * $labor->our_cost * $roof_measurement->squares;

                    $build_sheet = BuildSheet::create([
                        'estimate_id' => $request->session()->get('estimate_id'),
                        'labor_id' => $labor->id,
                        'labor_quantity' => $roof_measurement->squares,
                        'waste_factor' => $waste_factor->labor_factor,
                        'total_cost' => $labor->total_cost
                    ]);
                    #dd($build_sheet);
                    #exit();

                    }
                } else {
                    // Big problem here -> should never have more than 1 match!
            }
        } elseif ($project->project_type == "Repair") {
                // This is custom time, not pre-fixed pricing, so do nothing.
        } else {
                    // Must be "Inspection", which has no labor and no materials and is fixed at $200 inspection fee.
        }

        // Now calculate the material cost - we look at roof_components to get a list of the components to deterimine quanities as driven by square feet (squares) or lineal feet of something by looking at a location (table.column) in the dB and loop through each //

#        $materials = DB::table('materials')->where($match_requirements)
#            ->whereExists(function($query)
#            {
#                $query->select('*')
#                ->from ('roof_components')
#                ->where('roof_components.link_estimating', 'yes');
#            })
#            ->get();

#        $material->total_cost = '0';


        ## Squares
        $materials = DB::table('materials')->where('roof_measurements_column', '=', "square_feet")->get();
#        dd($materials);

        foreach ($materials as $material) {
            if ($material->factor_value > '0') {
                $material->quantity_needed = ceil(($roof_measurement->squares * (1 + $waste_factor->material_factor)) / $material->factor_value);               // Always round-up to nearest integer!  $measurement->squares needs to be dynamic so it is squares, valleys, ridges_hips_sum, drip_edge, eaves...........
                $material->total_cost = $material->quantity_needed * $material->our_cost;

                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $material->id,
                    'material_quantity' => $material->quantity_needed,
                    'waste_factor' => $waste_factor->material_factor,
                    'total_cost' => $material->total_cost,
                ]);


            }
        }

        ## Valleys
        $materials = DB::table('materials')->where('roof_measurements_column', '=', "valleys")->get();
#        dd($materials);

        foreach ($materials as $material) {
            if ($material->factor_value > '0') {
                $material->quantity_needed = ceil(($roof_measurement->valleys * (1 + $waste_factor->material_factor)) / $material->factor_value);               // Always round-up to nearest integer!  $measurement->squares needs to be dynamic so it is squares, valleys, ridges_hips_sum, drip_edge, eaves...........
                $material->total_cost = $material->quantity_needed * $material->our_cost;

                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $material->id,
                    'material_quantity' => $material->quantity_needed,
                    'waste_factor' => $waste_factor->material_factor,
                    'total_cost' => $material->total_cost,
                ]);


            }
        }

        ## Drip Edge
        $materials = DB::table('materials')->where('roof_measurements_column', '=', "drip_edge")->get();
#        dd($materials);

        foreach ($materials as $material) {
            if ($material->factor_value > '0') {
                $material->quantity_needed = ceil(($roof_measurement->drip_edge * (1 + $waste_factor->material_factor)) / $material->factor_value);               // Always round-up to nearest integer!  $measurement->squares needs to be dynamic so it is squares, valleys, ridges_hips_sum, drip_edge, eaves...........
                $material->total_cost = $material->quantity_needed * $material->our_cost;

                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $material->id,
                    'material_quantity' => $material->quantity_needed,
                    'waste_factor' => $waste_factor->material_factor,
                    'total_cost' => $material->total_cost,
                ]);


            }
        }

        ## Drip Edge
        $materials = DB::table('materials')->where('roof_measurements_column', '=', "eaves")->get();
#        dd($materials);

        foreach ($materials as $material) {
            if ($material->factor_value > '0') {
                $material->quantity_needed = ceil(($roof_measurement->eaves * (1 + $waste_factor->material_factor)) / $material->factor_value);               // Always round-up to nearest integer!  $measurement->squares needs to be dynamic so it is squares, valleys, ridges_hips_sum, drip_edge, eaves...........
                $material->total_cost = $material->quantity_needed * $material->our_cost;

                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $material->id,
                    'material_quantity' => $material->quantity_needed,
                    'waste_factor' => $waste_factor->material_factor,
                    'total_cost' => $material->total_cost,
                ]);


            }
        }

        ## Hips & Ridge Sum
        $materials = DB::table('materials')->where('roof_measurements_column', '=', "hips_ridge_sum")->get();
#        dd($materials);

        foreach ($materials as $material) {
            if ($material->factor_value > '0') {
                $material->quantity_needed = ceil(($roof_measurement->hips_ridge_sum * (1 + $waste_factor->material_factor)) / $material->factor_value);               // Always round-up to nearest integer!  $measurement->squares needs to be dynamic so it is squares, valleys, ridges_hips_sum, drip_edge, eaves...........
                $material->total_cost = $material->quantity_needed * $material->our_cost;

                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $material->id,
                    'material_quantity' => $material->quantity_needed,
                    'waste_factor' => $waste_factor->material_factor,
                    'total_cost' => $material->total_cost,
                ]);


            }
        }

        //Done with all auto-material calculations
        // Get boot costs and vents and add to build sheet
        if ($project->project_type == "Reroof") {
            $boot_1_detail = DB::table('materials')->select('*')->where('id', $structure->boot_1)->first();      // boot_1 is required - all roofs have at least a single penetration
            $build_sheet = BuildSheet::create([
                'estimate_id' => $request->session()->get('estimate_id'),
                'material_id' => $boot_1_detail->id,
                'material_quantity' => $request->session()->get('roof_measurements.boot_1_quantity'),
                'total_cost' => ($boot_1_detail->our_cost * $request->session()->get('roof_measurements.boot_1_quantity')),
            ]);
            if ($structure->boot_2) {
                $boot_2_detail = DB::table('materials')->select('*')->where('id', $structure->boot_2)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $boot_2_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.boot_2_quantity'),
                    'total_cost' => ($boot_2_detail->our_cost * $request->session()->get('roof_measurements.boot_2_quantity')),
                ]);
            }
            if ($structure->boot_3) {
                $boot_3_detail = DB::table('materials')->select('*')->where('id', $structure->boot_3)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $boot_3_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.boot_3_quantity'),
                    'total_cost' => ($boot_3_detail->our_cost * $request->session()->get('roof_measurements.boot_3_quantity')),
                ]);
            }
            if ($structure->boot_4) {
                $boot_4_detail = DB::table('materials')->select('*')->where('id', $structure->boot_4)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $boot_4_detail->id,
                    'material_quantity' =>$request->session()->get('roof_measurements.boot_4_quantity'),
                    'total_cost' => ($boot_4_detail->our_cost * $request->session()->get('roof_measurements.boot_4_quantity')),
                ]);      
            }
            // Now let's check for vents.
            if ($structure->vent_1) {
                $vent_1_detail = DB::table('materials')->select('*')->where('id', $structure->vent_1)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_1_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_1_quantity'),
                    'total_cost' => ($vent_1_detail->our_cost * $request->session()->get('roof_measurements.vent_1_quantity')),
                ]);    
            }
            if ($structure->vent_2) {
                $vent_2_detail = DB::table('materials')->select('*')->where('id', $structure->vent_2)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_2_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_2_quantity'),
                    'total_cost' => ($vent_2_detail->our_cost * $request->session()->get('roof_measurements.vent_2_quantity')),
                ]);    
            }
            if ($structure->vent_3) {
                $vent_3_detail = DB::table('materials')->select('*')->where('id', $structure->vent_3)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_3_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_3_quantity'),
                    'total_cost' => ($vent_3_detail->our_cost * $request->session()->get('roof_measurements.vent_3_quantity')),
                ]);          
            } 
            if ($structure->vent_4) {
                $vent_4_detail = DB::table('materials')->select('*')->where('id', $structure->vent_4)->first();
                $build_sheet = BuildSheet::create([
                    'estimate_id' => $request->session()->get('estimate_id'),
                    'material_id' => $vent_4_detail->id,
                    'material_quantity' => $request->session()->get('roof_measurements.vent_4_quantity'),
                    'total_cost' => ($vent_4_detail->our_cost * $request->session()->get('roof_measurements.vent_4_quantity')),
                ]);      
            }         
        }


        // Next, we see if the address of the structure is within a permitting jurisdiction
        $permit_count = DB::table('permit_jurisdictions')->select('*')->where('us_cities_id', $request->session()->get('project.city_id'))->count();   // This should return the permitting jurisdiction (if it exists), based on the city, which came from Google (reliable)


        if ($permit_count == '1') {      // We have a match!, but is it a municipal (city) or county permit?
            $permit_details = DB::table('permit_jurisdictions')->select('*')->where('name', $request->session()->get('project.city'))->get();
        } elseif ($permit_count == '0') {

            // No matches for the address in the permitting jurisdictions for the city, so must be county
            $county = DB::table('us_cities')->select('county')->where('id', $request->session()->get('project.city_id'))->first();
            $county = rtrim($county->county, ' County');        // Get rid of County from the name of the County
        
            $where_conditions = [
                'permit_jurisdictions.name' => $county,
                'permit_jurisdictions.type' => 'county', 
            ];

            $permit_details = DB::table('permit_jurisdictions')->select('*')->where($where_conditions)->first();   // Add condition to type = county also

        } else {
            // Getting here should be impossible
        }

        #dd($permit_details);
        #exit();

        $permit_details->total_fee = '0';
        // Now that we have $permit_details, let's do the math on the permit
        if ($permit_details->permit_tier_fee) {   // If it is a tier fee model
            $permit_tier = DB::table('permit_tier_fee')->select('*')->where('permit_tier_fee.permit_jurisdictions_id', $permit_details->id)->between( CONDITION );  // Fun eloqent SQL here....
            $permit_details->total_fee =  $permit_tier->tier_permit_fee;
        }

        if ($permit_details->permit_fee_per_unit) {   // If it is a fee based on squares (only thing supported now)
            $permit_details->total_fee = $permit_details->total_fee + ($permit_details->permit_fee_per_unit_fee * $estimate->squares);
        }
        if ($permit_details->permit_fixed_fee) {    // If there is a simple, fixed fee
            $permit_details->total_fee = $permit_details->total_fee + $permit_details->permit_fixed_fee_fee;
        }

        $build_sheet = BuildSheet::create([
            'estimate_id' => $request->session()->get('estimate_id'),
            'other_costs_id' => '1',
            'other_costs_quantity' => '1',            ## BEWARE - May need to be based on # of structures versus just address!
            'total_cost' => $permit_details->total_fee,
        ]);


        #dd(session()->all());
        #exit();

        // Now let's add all of the other project costs, except for commissions, save that for last
        $other_costs = DB::table('project_fee_types')->select('*')
            ->where('fee_per_project', '<>', 'NULL')
            ->where('active', '=', 'yes')
            ->get();
        foreach ($other_costs as $cost) {
            $build_sheet = BuildSheet::create([
                'estimate_id' => $request->session()->get('estimate_id'),
                'other_costs_id' => $cost->id,
                'other_costs_quantity' => '1',
                'total_cost' => $cost->fee_per_project,
            ]);
        }

        $total_cost = DB::table('build_sheets')->select('*')
            ->where('estimate_id', '=', $request->session()->get('estimate_id'))
            ->sum('total_cost');

        // stores in session the calculation result for inserting in pdf

        return view('estimate-figure')->with(['project_id' => $request->session()->get('estimate_project_id')]);
    }


    public function CacluateCosts($estimate_id){

        // Fetch estimate, and all properties associated with structure (measurements, project)
        // Estimates are done on a per-structure (not address/project) basis

/*
            $estimate_id = 1;

        //First, we pull all of the measurements and properties together for each structure in the project





        
            Done with all auto-material calculations
            // Get boot costs
            if ($project_type->name == "Reroof") {
                $estimate->boot_1 = DB::table('materials')->select('*')->where('material.id', $structure->boot_1);      // boot_1 is required - all roofs have at least a single penetration
                $material->total_cost = $material->total_cost + $estimate->boot_1->our_cost;
                if ($structure->boot_2) {
                $estimate->boot_2 = DB::table('materials')->select('*')->where('material.id', $structure->boot_2);
                $material->total_cost = $material->total_cost + $estimate->boot_2->our_cost;       
                }
                if ($structure->boot_3) {
                    $estimate->boot_3 = DB::table('materials')->select('*')->where('material.id', $structure->boot_3);
                    $material->total_cost = $material->total_cost + $estimate->boot_3->our_cost;       
                }
                if ($structure->boot_4) {
                    $estimate->boot_4 = DB::table('materials')->select('*')->where('material.id', $structure->boot_4);
                    $material->total_cost = $material->total_cost + $estimate->boot_4->our_cost;       
                }
                // Now let's check for vents.
                if ($structure->vent_1) {
                    $estimate->vent_1 = DB::table('materials')->select('*')->where('material.id', $structure->vent_1);
                    $material->total_cost = $material->total_cost + $estimate->vent_1->our_cost;       
                }
                if ($structure->vent_2) {
                    $estimate->vent_2 = DB::table('materials')->select('*')->where('material.id', $structure->vent_2);
                    $material->total_cost = $material->total_cost + $estimate->vent_2->our_cost;      
                }
                if ($structure->vent_3) {
                    $estimate->vent_3 = DB::table('materials')->select('*')->where('material.id', $structure->vent_3);
                    $material->total_cost = $material->total_cost + $estimate->vent_3->our_cost;       
                } 
                if ($structure->vent_4) {
                    $estimate->vent_4 = DB::table('materials')->select('*')->where('material.id', $structure->vent_4);
                    $material->total_cost = $material->total_cost + $estimate->vent_4->our_cost;       
                }         
            }
        
            // Finally, we see if the address of the structure is within a permitting jurisdiction
            $permit_count = DB::table('permit_jurisdictions')->select('*')->where('us_cities.id', $address->city_id)->count();
            if ($permit_count = '1') {
                $permit_details = DB::table('permit_jurisdictions')->select('*')->where('us_cities.id', $address->city_id);
            } elsif ($permit_count = '0') {
                // No matches for the address in the permitting jurisdictions for the city, so must be county
                $permit_details = DB::table('permit_jurisdictions')->select('*')->where('permit_jurisdictions.name', $us_cities->county); // Lee County, Florida, such as Lehigh Acres or Fort Myers
            } else {
                // Getting here should be impossible
            }
            // Now that we have $permit_details, let's do the math on the permit
            if ($permit_details->permit_tier_fee) {   // If it is a tier fee model
                $permit_tier = DB::table('permit_tier_fee')->select('*')->where('permit_tier_fee.permit_jurisdictions_id', $permit_details->id)->between( CONDITION );  // Fun eloqent SQL here....
                $permit_details->total_fee =  $permit_tier->tier_permit_fee;
            }

            if ($permit_details->permit_fee_per_unit) {   // If it is a fee based on squares (only thing supported now)
                $permit_details->total_fee = $permit_details->total_fee + ($permit_details->permit_fee_per_unit_fee * $estimate->squares);
            }
            if ($permit_details->permit_fixed_fee) {    // If there is a simple, fixed fee
                $permit_details->total_fee = $permit_details->total_fee + $permit_details->permit_fixed_fee_fee;
            }
            
            $estimate->total_cost = $material->total_cost + $labor->total_cost + $permit_details->total_fee + $other_fees->total_fee;

        }

        */

    }

    public function generateProposal(Request $request){

        // var_dump($id);
        if (!$request->session()->get('estimate_id')){
            return redirect('/estimator/properties');
        }
        $id = $request->session()->get('estimate_project_id');
        
        $template = DB::table('templates')->select('*')
        ->where('templates.name', 'Estimate')->get(1);

        $tokens = DB::table('tokens')->select('*')->get();
        
        $project_info = DB::table('projects')->select('roof_types.name' ,'contacts.email_address', 'clients.company_name', 'users.email')
        ->join('users', 'users.id', '=', 'projects.user_id')
        ->join('contacts', 'contacts.id', '=', 'projects.contact1_id')
        ->join('roof_types', 'roof_types.id', '=', 'projects.preferred_roof_types_id')
        ->join('clients', 'clients.id', '=', 'projects.client_id')
        ->where('projects.id', $id)->get()[0];
        // Cycle through all tokens and search/replace from tokens table in all templates
        foreach ($tokens as $token) {    
            $replace = $token->token_identifier . $token->token_name . $token->token_identifier;
            $template[0]->email_subject = str_replace("$replace", $project_info->name, $template[0]->email_subject);
            $template[0]->email_message = str_replace("$replace", $project_info->company_name, $template[0]->email_subject);
            $template[0]->sms_message = str_replace("$replace", $project_info->company_name, $template[0]->email_subject);
        }
        $generatePdfFileInfo = $this->generatePDF($request->session()->get('estimate_id'));
        return view('generateProposal', ['template' => $template, 'customer_email' => $project_info->email_address, 'estimator_email' => $project_info->email, 'proposal_pdffilepath' => $generatePdfFileInfo['url'], 'project_id' => $id]);
        
    }

    public function generateProposalSend(Request $request){
        $attachFiles = [];
        array_push($attachFiles, \base_path('public/uploads/request_proposals/').basename($request->proposal_filepath));
        if ($request->attachments != null){
            foreach($request->attachments as $i){
                if ($i == "0"){
                    //oil waver pdf
                    array_push($attachFiles, storage_path('app/clients/1/estimate_forms/OilCanningWaiver.pdf'));
                }
            }
        }
        
        // add token link include project_id
        $document_id = DB::table('documents')->insertGetId(['estimate_id' => $request->session()->get('estimate_id'), 'document_types_id' => 1, 'location' =>$request->proposal_filepath , 'require_customer_signature' => 'yes', 'require_customer_initials' => 'yes', 'customer_viewable' => 'yes', 'created_at' => date('Y-m-d h:i:s') ]);
        
        $token_info = [
            'project_id' => $request->id,
            'document_id' => $document_id
        ];
        
        $token = Token::customPayload($token_info, env('JWT_SECRET'));  //encrypt
        //dd(Token::getPayload($token, env('JWT_SECRET')));   decrypt
        \Mail::to($request->customer_email)->send(new EstimateProposal($request->subject, $request->attachtext, $attachFiles, url("/")."/customersign/".$token));
        if (\Mail::failures()) {
            return 'fail';
        }
        
        return 'success';
    }

    public function generatePDF($estimate_id){
        $pdf = new \setasign\Fpdi\Fpdi();
        $project_info = DB::table('estimates')
        ->select('projects.id as project_id', 'contacts.first_name', 'contacts.last_name', 'contacts.cell_phone as cus_cell_phone', 'contacts.email_address', 'addresses.number', 'addresses.street', 'us_cities.name as city', 'states.name as state', 'users.username', 'users.cell_phone as user_cell_phone', 'users.email')
        ->join('structures', 'estimates.structure_id', '=', 'structures.id')
        ->join('projects', 'structures.project_id', '=', 'projects.id')
        ->join('contacts', 'projects.contact1_id', '=', 'contacts.id')
        ->join('addresses', 'contacts.address_id', '=', 'addresses.id')
        ->join('us_cities', 'addresses.city_id', '=', 'us_cities.id')
        ->join('states', 'addresses.state_id', '=', 'states.id')
        ->join('users', 'projects.user_id', '=', 'users.id')
        ->where('estimates.id', $estimate_id)->get()[0];
        
        $pageCount = $pdf->setSourceFile("$_SERVER[DOCUMENT_ROOT]/../storage/app/clients/1/estimate_forms/GAF_GP.pdf");
        
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $pdf->AddPage();
            $pdf->useTemplate($templateId, ['adjustPageSize' => true]);
            $pdf->SetFont('Helvetica','',10);
            $pdf->SetTextColor(0, 0, 0);
            if ($pageNo == 1){
                //date
                $pdf->SetXY(20, 37.5);
                $pdf->Write(0, date('Y-m-d'));
                //project_id
                $pdf->SetXY(129, 37.5);
                $pdf->Write(0, '# '.$project_info->project_id);
                //customer name
                $pdf->SetXY(7, 48);
                $pdf->Write(0, $project_info->first_name." ".$project_info->last_name);
                //customer's phone
                $pdf->SetXY(58.5, 48);
                $pdf->Write(0, $project_info->cus_cell_phone);
                //customer's email
                $pdf->SetXY(111, 48);
                $pdf->Write(0, $project_info->email_address);
                //customer's address
                $pdf->SetXY(6.5, 58.5);
                $pdf->Write(0, $project_info->street." ".$project_info->number." "." ".$project_info->city." ".$project_info->state);
                //customer's project address
                $pdf->SetXY(6.5, 70);
                $pdf->Write(0, $project_info->street." ".$project_info->number." "." ".$project_info->city." ".$project_info->state);
                //roman rep's name
                $pdf->SetXY(6.5, 80.5);
                $pdf->Write(0, $project_info->username);
                //roman rep's phone
                $pdf->SetXY(58.5, 80.5);
                $pdf->Write(0, $project_info->user_cell_phone);
                //roman rep's email
                $pdf->SetXY(111, 80.5);
                $pdf->Write(0, $project_info->email);

                $pdf->AddFont('AnthoniSignature','','AnthoniSignature.php');

                $pdf->SetFont('AnthoniSignature','',20);
                //Authorized Signature
                $pdf->SetXY(118, 179);
                $pdf->Write(0, $project_info->username);

                // $pdf->SetFont('Helvetica','',10);
                // Date will be written when user sign
                // $pdf->SetXY(115, 197.5);
                // $pdf->Write(0, '2020-02-29');   

            }
            
        }
        // insert in db this generated pdf file path
        $now = DateTime::createFromFormat('U.u', microtime(true));
        $proposal_pdf_name = $now->format("Y-m-d_H-i-s_u").'.pdf';
        // if directory doens't exist, create
        if (!File::isDirectory(public_path().'/uploads/request_proposals')) File::makeDirectory(public_path().'/uploads/request_proposals', 0777, true, true);
        // save in public/uploads/request_proposals/ ...pdf
        $pdf->Output('F', public_path()."/uploads/request_proposals/".$proposal_pdf_name, true);
        // return saved file path
        return ['url' => url('uploads/request_proposals')."/".$proposal_pdf_name];
    }

    public function customersign(Request $request){
        $token = $request->token;
        $customer_info = Token::getPayload($token, env('JWT_SECRET'));
        $document_path = DB::table('documents')->select('location as pdf_filepath')->where('id', $customer_info['document_id'])->get(0);
        $customer_name = DB::table('projects')->select('contacts.first_name', 'contacts.last_name')->join('contacts', 'contacts.id', '=', 'projects.contact1_id')->where('projects.id', $customer_info['project_id'])->get();
        
        return view('customersign', ['customer_name_first' =>$customer_name[0]->first_name, 'customer_name_last' => $customer_name[0]->last_name, 'document_path' => $document_path[0]->pdf_filepath, 'customer_info' => $customer_info]);
    }

    public function customerDosign(Request $request){
        // dd($request->sign_info);
        $document_path = DB::table('documents')->select('location')->where('id', $request->sign_info['document_id'])->get()[0]->location;
        
        $pdf = new \setasign\Fpdi\Fpdi();
        
        $pageCount = $pdf->setSourceFile(\base_path('public/uploads/request_proposals/'.basename($document_path)));
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $pdf->AddPage();
            $pdf->useTemplate($templateId, ['adjustPageSize' => true]);
            $pdf->SetFont('Helvetica','',10);
            $pdf->SetTextColor(0, 0, 0);
            if ($pageNo == 1){
                $pdf->AddFont('AnthoniSignature','','AnthoniSignature.php');

                $pdf->SetFont('AnthoniSignature','',20);
                
                //Authorized Signature
                if ($request->sign_type == 0){
                    $pdf->SetXY(40, 179);
                }
                else $pdf->SetXY(33, 179);
                
                $pdf->Write(0, $request->sign_name);

                $pdf->SetFont('Helvetica','',10);
                //customer date Signature
                $pdf->SetXY(36.5, 197.5);
                $pdf->Write(0, date('Y-m-d'));   // From here, Gabriel can do all of the writing (pull data from Db and place at xy coordinates on PDF)
                //Authorized date sign as customer's sign
                $pdf->SetXY(115, 197.5);
                $pdf->Write(0, date('Y-m-d'));
            }
        }

        $now = DateTime::createFromFormat('U.u', microtime(true));
        $proposal_pdf_name = $now->format("Y-m-d_H-i-s_u").'.pdf';
        
        DB::table('documents')->update(['location' =>'/uploads/request_proposals/'.$proposal_pdf_name , 'require_customer_signature' => 'no', 'require_customer_initials' => 'no', 'customer_viewable' => 'yes', 'updated_at' => date('Y-m-d h:i:s') ]);
        
        // save in public/uploads/request_proposals/ ...pdf
        $pdf->Output('F', public_path()."/uploads/request_proposals/".$proposal_pdf_name, true);
        // return saved file path
        return ['url' => url('uploads/request_proposals')."/".$proposal_pdf_name, 'success' => '1'];
    }

}
