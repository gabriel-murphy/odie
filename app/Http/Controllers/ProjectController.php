<?php

namespace App\Http\Controllers;

use App\Constant;
use App\Phase;
use App\Property;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Project;
use App\Customer;
use App\RoofType;
use Mail;



class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $phases = Phase::all();
        $phase = Phase::find(request('phase', ''));

        $projects = Project::query();
        if ($phase) $projects->where('phase_id', $phase->id);
        $projects = $projects->paginate(50);

        return view('project.index', compact('projects', 'phases', 'phase'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'roof_types' => RoofType::parent()->get(),
            'project_types' => Constant::group('project_types')->get(),
            'states' => Constant::group('states')->get(),
            'classifications' => Constant::group('classifications')->get(),
            'lead_sources' => Customer::getLeadSources(),
            'relationships' => Constant::group('property_relationships')->get(),
            'estimators' => User::role('estimator')->get()
        ];
        return view('project.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Datalab $datalab
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $step)
    {

        switch ($step){
            case 'contact_info_tab':

                $request->validate([
                    'customer.first_name' => 'required|min:2',
                    'customer.last_name' => 'required|min:2',
                    'customer.relationship_id' => 'required',
                    'customer.emails.*.email' => 'required|email:rfc,dns',                 // RFC and DNS validation on syntax and domain
                ]);
                $customer_data = $request->customer;
                $address = $customer_data['address'];
                $emails  = $customer_data['emails'];
                $phones  = $customer_data['phones'];

                $primary_email = $customer_data['emails'][0]['email'];

                $customer_data['associate_type_id'] = 1; // 1 stats that it is a customer
                $customer_data['client_id'] = 1; // 1 stats that it is a customer
                $customer = Customer::create($customer_data);
                // adding address of customer

                $customer->address()->create($address);

                //sending api calls for people data labs and estated in background.

                PeopleEstatedJob::dispatch($customer,$primary_email,$address);

                // adding phones of customer
                $phones_data = array();
                foreach ($phones as $phone){
                    $phones_data['phone_types_id'] = $this->getPhoneIdBasedOnType($phone['type']);
                    if(isset($phone['extension'])){
                        $phones_data['extension'] = $phone['extension'];
                    }
                    $phones_data['client_id'] = 1; // roman roofing
                    $phones_data['associate_type_id'] = 1; //customer
                    $phones_data['number'] = $phone['number'];
                    $phones_data['countries_id'] = 348; //USA
                    $customer->phones()->create($phones_data);
                }
                // adding emails of customer
                $email_data = array();
                foreach ($emails as $key=> $email){
                    // check if it's first element in array make that first primary.
                    if($key === array_key_first($emails)){
                        $email_data['is_primary'] = 1;
                    }
                    else {
                        $email_data['is_primary'] = 0;
                    }
                    $email_data['email'] = $email['email'];
                    $email_data['associate_type_id'] = 1; // 1 stats that it is a customer
                    $email_data['client_id'] = 1; // 1 stats that it is a customer
                    $customer->emails()->create($email_data);
                }
                return redirect()->route('projects.create',['customer_id'=> $customer->id,'step' => 'property_info_tab']);
                break;
            case 'property_info_tab':

                $request->validate([
                    'property.relationship_id' => 'required|numeric',
                    'property.classification_id' => 'required|numeric',
                    'project.project_type' => 'required|numeric',
                    'project.current_roof_type_id' => 'required|numeric',
                    'project.preferred_roof_type_id' => 'required|numeric',
                ]);

               // dd($request->same_address);

                $property_data = $request->property;
                if(isset($request->same_address) && $request->same_address =="1"){
                    unset($property_data['address']);
                }
                $property_data['customer_id'] = isset($request->customer_id)?$request->customer_id:null;
                $property_data['client_id'] = 1; //rooman roofing

                $property = Property::create($property_data);
                if(isset($property_data['address']) && !empty($property_data['address'])){

                    $property->address()->create($property_data['address']);
                }
                $project_data['customer_id'] = isset($request->customer_id)?$request->customer_id:null;
                $project_data['property_id'] = $property->id;
                $project_data['client_id'] = 1; // roman roofing
                $project_data['phase_id'] = Phase::where('name', 'Opportunities')->first()->id;
                $project_data['user_id'] = auth()->id();
                $project = Project::create($project_data);
                return redirect()->route('projects.create', ['project_id'=>$project->id,'step' => 'appointment_tab']);
                break;
            case  'appointment_tab':
                $lead_source_id = $request->project['lead_source_id'];
                $selected_items = implode(',',$lead_source_id);
                dd($request);

        }




        $property_data = $request->property;
        $project_data = $request->project;


        $property_data['customer_id'] = $customer->id;
        $property = Property::create($property_data);
        $property->address()->create($address);

        $project_data['customer_id'] = $customer->id;
        $project_data['property_id'] = $property->id;
        $project_data['phase_id'] = Phase::where('name', 'Opportunities')->first()->id;
        $project_data['user_id'] = auth()->id();
        $project = Project::create($project_data);

        return redirect(route('appointments.index'))->with('success', "Opportunity Created and Assigned Project ID #$project->id");

    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $data = [
            'project' => $project,
            'phases' => Phase::all(),
            'phases' => Phase::all(),
            'roof_types' => app('assets')->roofTypes->where('roof_type_id' == NULL),
            'project_types' => Constant::group('project_types')->get(),
            'states' => Constant::group('states')->get(),
            'lead_sources' => Customer::getLeadSources(),
            'relationships' => Constant::group('property_relationships')->get(),
            'appointments' => $project->appointments
        ];

        return view('project.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        // similar to create but a put
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return void
     */
    public function destroy(Project $project)
    {
        //
    }

    private function getPhoneIdBasedOnType($type){
        $id = DB::table('constants')->select('id')->where('name', $type)->first();
        return $id->id;
    }

    public function basic_email() {

        try {
            $data = array('name'=>"Ashar Saleem testing mailgun","email"=>"asharsaleem4@gmail.com");

            Mail::send('mail', $data, function($message) use ($data) {
                $message->to($data['email'], $data['name'])->subject
                ('Laravel Basic Testing Mail');
            });
        }
        catch (\Exception $e){
            echo $e;
        }
    }
}
