<?php

namespace App\Http\Controllers;

use App\Constant;
use App\Phase;
use App\Property;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Project;
use App\Address;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Http;
use App\Services\Datalab\Datalab;

class CustomerController extends Controller
{
    
    public function index()
    {
        $projects = Project::all();
        return view('customer.index', compact('projects'));
    }

    
    public function create()
    {
        return view('customer.create');
    }

    
    public function store(Request $request)
    {
        //

    }

    
    public function show(Customer $customer, Request $request, Datalab $datalab)
    {
        
        $customer_social = [];
        // customer social link
        // get social link by email
        $params = [
            'email' => $customer->email
        ];
        $response = $datalab->getPersonData($params);
        
        if (count($response) > 0 && count($response['profiles']) > 0){
            $customer_social = $response['profiles'];
        }
        
        //estimator social link
        //get by email
        $estimator_social = [];
        $params = [
            'email' => $customer->project()->estimator->email
        ];
        $response = $datalab->getPersonData($params);
        if (count($response) > 0 && count($response['profiles']) > 0){
            $estimator_social = $response['profiles'];
        }
        $data = [
            'customer' => $customer,
            'project' => $customer->project(),
            'estimators' => User::role('estimator')->get(),
            'phases' => Phase::all(),
            'customer_social' => $customer_social,
            'estimator_social' => $estimator_social
        ];
        return view('customer.show', $data);
    }

    
    public function edit(Customer $customer)
    {
        // similar to create but a put
    }

    
    public function update(Request $request, Customer $customer)
    {
        //
    }

    public function destroy(Customer $customer)
    {
        //
    }
}
