<?php

namespace App\Http\Controllers;

use App\Placeholder;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmailTemplateController extends Controller
{

    public function index()
    {
        $q = request()->query('q');
        $templates = Template::whereAnyColumnLike($q)->type('email')->paginate(15);
        return view('email-template/index', compact('templates'));
    }


    public function create()
    {
        $adminDefinePlaceholders = Placeholder::all();
        return view('email-template/create', compact('adminDefinePlaceholders'));
    }


    public function store(Request $request)
    {
        $request->merge(['key' => str::snake($request->name)]);
        $template=Template::create($request->all());
        $template->placeholders()->attach($request->placeholders);
        return redirect()->route('templates.emails.index')->with('success', 'Created Successfully');
    }


    public function show(Template $template)
    {
        return view('email-template/show', compact('template'));
    }


    public function edit(Template $template)
    {
        $data['template'] = $template;
        $data['placeholders'] = $template->placeholders()->get();
        $data['adminDefinePlaceholders'] = Placeholder::all();
        return view('email-template/edit', $data);
    }


    public function update(Request $request, Template $template)
    {
        $template->update($request->all());
        $template->placeholders()->syncWithoutDetaching($request->placeholders);
        return redirect()->route('templates.emails.index')->with('success', 'Updated Successfully');
    }


    public function destroy(Template $template)
    {
        $template->placeholders()->detach();
        $template->delete();
        return redirect()->back()->with('success', 'Removed Successfully');
    }

    public function changeStatus(Template $template)
    {
        $template->update([
            'active' => !$template->active
        ]);
        return redirect()->back()->with('success', 'Status Updated Successfully');
    }
}
