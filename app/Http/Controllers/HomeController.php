<?php

namespace App\Http\Controllers;
use App\RoofManufacturer;
use App\Mail\TestEmail;
use App\Estimate;
use App\Client;
use App\RoofType;
use Mail;

class HomeController extends Controller
{
    /**
     * Show the public home page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        return view('home');
    }


    public function lee_noc()
    {

        session(['key' => 'value']);

        $pdf = new \setasign\Fpdi\Fpdi();
        $pdf->AddPage();
        $pdf->setSourceFile("$_SERVER[DOCUMENT_ROOT]/../storage/app/clients/1/estimate_forms/GAF_GP.pdf");
        $temp = $pdf->importPage(1);
        $pdf->useTemplate($temp, 4, 4, 200);
        $pdf->SetFont('Helvetica','',10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(100, 49);
        $pdf->Write(0, 'This is just a simple text from Gabriel');
        $pdf->Output();
    }

    public function email()
    {
        ## Testing Emails
        $data = ['message' => 'This is a test from Odie!'];
        Mail::to('gabriel@odie.pro')->send(new TestEmail($data));
        return view('home');
    }

}

