<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $assigned_tasks = user()->assignedTasks()->paginate(15);
        $created_tasks = user()->tasks()->paginate(15);
        return view('task.index', compact('assigned_tasks', 'created_tasks'));
    }

    public function create()
    {
        return view('task.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'client_id' => 'required',
            'title' => 'required|string|max:50',
            'users' => 'required',
            'due_date' => 'required|date',
            'description' => 'required|string|max:200',
        ]);

        $task = user()->tasks()->create($request->except('users'));
        if ($request->input('users'))
        {
            $task->assignees()->attach($request->input('users'));
        }
        return redirect()->route('tasks.index')->with('success', 'Task Created');
    }

    public function complete(Task $task)
    {
        $assignees = $task->assignees->pluck('id')->toArray();
        if (!$task->completed && in_array( user()->id, $assignees))
        {
            $task->update(['completed_by' => auth()->id()]);
            return redirect()->route('tasks.index')->with('success', 'Task Completed');
        }
        return redirect()->back();
    }
}
