<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Jobs\RunCampaign;
use App\Mail\CampaignMail;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CampaignController extends Controller
{
    public function index()
    {
        $campaigns = Campaign::with('recipients')->withCount('recipients')->get();
        return view('campaign.index', compact('campaigns'));
    }

    public function create()
    {
        $templates = Template::type('campaign')->get();
        return view('campaign.create', compact('templates'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'template_id' => 'required',
            'recipients' => [
                'required',
                function ($attribute, $value, $fail) {
                    $this->validateCsv($attribute, $value, $fail);
                },
            ]
        ]);

        $campaign = Campaign::create($request->except('recipients'));

        $campaign->addMediaFromRequest('recipients')->toMediaCollection();

        RunCampaign::dispatch($campaign);
        return redirect()->route('campaigns.index')->with('Campaign Created Successfully');
    }

    public function show(Campaign $campaign)
    {
        return view('campaign.show', compact('campaign'));
    }

    public function recipients(Campaign $campaign)
    {
        $recipients = $campaign->recipients()->paginate(30);
        return view('campaign.recipients', compact('recipients', 'campaign'));
    }


    private function validateCsv($attribute, $value, $fail)
    {
        $filename = $value->getPathName();
        $delimiter = ',';

        $header = null;
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header){
                    $header = $row;
                    break;
                }
            }
            fclose($handle);
        }

        if (! in_array('email', $header)) {
            $fail($attribute . ' file must contains a column named email.');
        }
    }

    private function csvToArray($filename = '', $columns = array(), $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;

        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        $columns = $columns ? array_map(function ($value){
            return strtolower($value);
        }, $columns) : $columns;

        return collect($data)
            ->filter(function ($data) {
                return array_sum($data) && $data['email'];
            })
            ->map(function ($data) {
                return array_change_key_case($data, CASE_LOWER);
            })
            ->map(function ($data) use ($columns) {
                return $columns ? Arr::only($data, $columns) : $data;
            })
            ->filter(function ($data) {
                $job_type = $data['job type'];
                return
                    $job_type
                    &&  ! Str::contains($job_type, ['Shingle', 'Metal', 'Flat', 'TPO', 'Maintenance', 'Service', 'Warranty', 'Low Slope', 'Replacement'])
                    && ! Str::contains($data['created by'], 'Pedro');
            });
    }
}
