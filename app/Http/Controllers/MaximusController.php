<?php

namespace App\Http\Controllers;

use App\Client;
use App\Customer;
use App\Http\Requests\MaximusRequest;
use App\Jobs\ProcessMaximusClaimUploads;
use App\Mail\ForwardOdmail;
use App\Mail\Maximus\ClaimEscalated;
use App\Mail\Maximus\InfoSubmitted;
use App\Mail\Maximus\AobSubmitted;
use App\Mail\Maximus\TempRepairSubmitted;
use App\MaximusClaim;
use App\Media;
use App\Odmail;
use App\Placeholder;
use App\Property;
use App\Template;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Mail;
use PDF;

class MaximusController extends Controller
{
    public function index()
    {
        $claims = MaximusClaim::with(['customer', 'property.address', 'status', 'estimator', 'mails'])->latest('id')->get();
        return view('maximus.index', compact('claims'));
    }

    public function create()
    {
        $data['uuid'] = \Str::orderedUuid()->toString();
        $data['adminDefinePlaceholders'] = Placeholder::all();
        $data['template'] = Template::type('email')->key('new_aob_submitted')->first();

        return view('maximus.create', $data);
    }

    public function store(Request $request, $step)
    {
        /*$claim = MaximusClaim::create([
              'user_id' => auth()->id(),
              'estimator_id' => $request->estimator_id ?? auth()->id(),
              'customer_id' => $customer->id,
              'insurance_carrier_id' => $request->insurance_carrier_id,
              'claim_number' => $request->claim_number,
              'policy_number' => $request->policy_number,
              'date_of_loss' => $request->date_of_loss,
              'aob_signed_on' => $request->aob_signed_on,
              'claim_type_id' => $request->claim_type_id,
              'minimum_damaged_units' => $request->minimum_damaged_units,
          ]);*/
        $request->merge([
            'user_id' => auth()->id(),
            'estimator_id' => $request->estimator_id ?? auth()->id(),
        ]);

        $claim = $request->claim_id ? MaximusClaim::find($request->claim_id) : null;
        $customer = is_object($claim) ? $claim->customer : null;
        $property = is_object($claim) && isset($claim->property) ? $claim->property : null;

        switch ($step) {
            case 'customer':
                $customer = $this->customer($request);
                $claim = MaximusClaim::create(['customer_id' => $customer->id]);
                return redirect()->route('maximus.claims.create', ['claim_id' => $claim->id, 'step' => 'property']);
                break;

            case 'property':
                if (is_object($property)) {
                    $property = $this->updateProperty($request, $customer->id, $property);
                } else {
                    $property = $this->property($request, $customer->id);
                }
                $request->merge(['property_id' => $property->id]);
                $claim->update($request->all());
                return redirect()->route('maximus.claims.create', ['claim_id' => $claim->id, 'step' => 'insurance']);
                break;

            case 'claim':
                $claim->update($request->all());
                return redirect()->route('maximus.claims.create', ['claim_id' => $claim->id, 'step' => 'documents']);
                break;

            case 'documents':
                $claim->update($request->all());
                break;
        }

        /* $claim = MaximusClaim::create($request->all());

         $customer = Customer::create($request->customer);

         $property = Property::create([
             'client_id' => 1,
             'customer_id' => $customer->id,
             'classification_id' => 3,
             'relationship_id' => 96,
         ]);

         $property->address()->create($request->address);

         $claim->update(['property_id' => $property->id, 'customer_id' => $customer->id]);*/

        ProcessMaximusClaimUploads::dispatch($claim);
        /*$this->generatePhotosPdf($claim, "maximus/$claim->id/aob/");

        $this->uploadDocuments($claim);*/
        /*$this->uploadDocs($claim);*/

        /*Mail::send(new AobSubmitted($claim, $request->template));*/

        return redirect()->route('maximus.claims.index')->with('success', 'Claim Created');
    }

    private function customer(Request $request)
    {
        return Customer::create($request->customer);
    }

    private function property(Request $request, $customer)
    {
        $property = Property::create([
            'client_id' => 1,
            'customer_id' => $customer,
            'classification_id' => 3,
            'relationship_id' => 96,
        ]);
        $property->address()->create($request->address);
        return $property;
    }

    private function updateProperty(Request $request, $customer, $property)
    {
        $property = Property::updateOrCreate(['id' => $property->id], [
            'client_id' => 1,
            'customer_id' => $customer,
            'classification_id' => 3,
            'relationship_id' => 96,
        ]);
        $property->address()->updateOrCreate(['addressable_id' => $property->id], $request->address);
        return $property;
    }

    public function show(MaximusClaim $claim)
    {
        $claim->load([
            'carrierResponses', 'submissions', 'tempRepair', 'media.model',
            'mails.recipients', 'statusLogs.status', 'statusLogs.user',
        ]);

        $data = array();
        $data['emails_to_resend'] = $claim->mails()->with('recipients')->get()->where('resent', 0)->where('forwarded', 0);
        $data['claim'] = $claim;
        $data['temp_repair_template'] = app('assets')->getTemplate('temp_repair_created');
        $data['escalate_template'] = app('assets')->getTemplate('maximus_claim_escalated');
        $data['info_submission_template'] = app('assets')->getTemplate('maximus_info_submitted');

        return view('maximus.show', $data);
    }

    public function update(Request $request, MaximusClaim $claim)
    {
        $claim->update([
            'status_id' => $claim->logStatusChange($request->status_id),
            'description' => $request->description
        ]);
        return redirect()->back()->with('success', 'Claim Status Updated');
    }

    public function tempRepairStore(Request $request, MaximusClaim $claim)
    {
        $request->validate([
            'photos.*' => 'required|mimes:jpeg,png,jpg,gif,svg|min:2',
            'photos' => 'required|min:8|max:8',
            'invoice' => 'required|mimes:pdf',
            'invoice_number' => 'required',
            'invoice_amount' => 'required',
        ]);

        $tempRepair = $claim->tempRepair()->create($request->all());

        $this->generatePhotosPdf($tempRepair);

        $tempRepair->addMediaFromRequest('invoice')
            ->usingName('invoice')
            ->withCustomProperties(['media_type_id' => app('constants')->getAttachmentTypeId('invoice')])
            ->toMediaCollection('documents');


        Mail::send(new TempRepairSubmitted($claim, $tempRepair->getMedia('documents')));

        return redirect()->back()->with('success', 'Temporary Repair Uploaded Successfully');

    }

    public function supplement(Request $request, MaximusClaim $claim)
    {
        $request->validate([
            'type_id' => 'required',
            'document' => 'required|mimes:pdf',
        ]);

        $submission = $claim->submissions()->create();

        $document_name = app('constants')->findById($request->type_id)->name;

        $submission->addMediaFromRequest('document')
            ->usingName($document_name)
            ->withCustomProperties(['media_type_id' => $request->type_id])
            ->toMediaCollection();

        // Evaluating
        $claim->update(['status_id' => $claim->logStatusChange(27)]);

        Mail::send(new InfoSubmitted($claim, $submission->getLastMedia(), $request->template));

        return redirect()->back()->with('success', 'Requested Info Uploaded Successfully')->with('info', 'Status Changed to ' . $claim->status->name);

    }

    public function recordCorrespondence(Request $request, MaximusClaim $claim)
    {
        $request->validate([
            'response_type_id' => 'required',
            'document' => 'required|mimes:pdf',
        ]);

        $carrierResponse = $claim->carrierResponses()->create([
            'response_type_id' => $request->response_type_id,
            'dated_on' => $request->dated_on,
            'received_on' => $request->received_on,
        ]);

        switch ($request->response_type_id) {
            // if it's a decision
            case 55:
                $new_claim_status = 29; // Decided
                $claim->update([
                    'coverage_type_id' => $request->coverage_type_id,
                    'coverage_amount' => $request->coverage_amount,
                    'denial_reason' => $request->denial_reason_id ? app('constants')->findById($request->denial_reason_id)->name : '',
                ]);
                break;

            // if it's an acknowledgement
            case 53:
                $new_claim_status = 25; // Acknowledged
                break;

            default:
                $new_claim_status = 28; // Waiting
        }

        $document_name = app('constants')->findById($request->response_type_id)->name;

        $carrierResponse->addMediaFromRequest('document')
            ->usingName($document_name)
            ->withCustomProperties(['media_type_id' => $request->response_type_id])
            ->toMediaCollection('documents');

        $claim->update(['status_id' => $claim->logStatusChange($new_claim_status)]);

        return redirect()->back()->with('success', 'Record Correspondence Uploaded Successfully')->with('info', 'Status Changed to ' . $claim->status->name);

    }

    public function escalate(Request $request, MaximusClaim $claim)
    {
        $claim->update(['escalator_id' => $request->escalator_id]);

        $attachments = Media::find($request->options);

        $document_type = app('constants')->getAttachmentTypeId('policy');
        $attachment = $claim->addMediaFromRequest('document')
            ->usingName(app('constants')->findById($document_type)->name)
            ->withCustomProperties(['media_type_id' => $document_type])
            ->toMediaCollection('documents');
        $attachments->push($attachment);

        // Escalating
        $claim->update(['status_id' => $claim->logStatusChange(31)]);

        Mail::send(new ClaimEscalated($claim, $attachments, $request->template));

        return redirect()->back()->with('success', 'Escalated Notice Sent')->with('info', 'Status Changed to ' . $claim->status->name);
    }

    private function uploadDocs($claim)
    {
        $dir = storage_path("app/temp/{$claim->uuid}");

        $files = File::files($dir);

        $documents = [
            [
                'type_id' => app('constants')->getAttachmentTypeId('aob'),
                'file' => 'aob', 'name' => '01_AOB'
            ],
            ['path' => 'documents/maximus/02_Duties.pdf'],
            ['path' => 'documents/maximus/03_Deadline.pdf'],
            ['path' => 'documents/maximus/04_Brochure.pdf'],
            [
                'type_id' => app('constants')->getAttachmentTypeId('xactimate'),
                'file' => 'xactimate', 'name' => '06_Xactimate'
            ],
            [
                'type_id' => app('constants')->getAttachmentTypeId('itel'),
                'file' => 'itel', 'name' => '08_ITEL'
            ],
            [
                'type_id' => app('constants')->getAttachmentTypeId('ariel'),
                'file' => 'ariel', 'name' => '07_Measurements'
            ],
            ['path' => 'documents/maximus/09_License.pdf'],
            ['path' => 'documents/maximus/10_W-9.pdf'],
            ['path' => 'documents/maximus/11_Insurance.pdf'],
        ];

        foreach ($documents as $document) {
            if (isset($document['path'])) {
                $claim->addMedia(public_path($document['path']))
                    ->preservingOriginal()
                    ->toMediaCollection('documents');
            } else {
                $file = Arr::first(
                    Arr::where($files, function ($file) use ($document) {
                        return explode('_', $file->getBasename())[0] == $document['file'];
                    })
                );

                if (!$file) {
                    continue;
                }

                $claim->addMedia($file->getPathName())
                    ->usingName($document['name'])
                    ->withCustomProperties(['media_type_id' => $document['type_id']])
                    ->toMediaCollection('documents');
            }
        }

        $damage_photos = Arr::where($files, function ($file) {
            return explode('_', $file->getBasename())[0] == 'photos';
        });

        foreach ($damage_photos as $photo) {
            $media = $claim->addMedia($photo->getPathName())->toMediaCollection('damage-photos');
            $photos[] = $media->getUrl();
        }

        $pdf = PDF::loadView('maximus.photos', compact('photos'));

        $claim->addMediaFromString($pdf->output())
            ->usingName('05_Photos')
            ->usingFileName('photos.pdf')
            ->toMediaCollection('documents');

        if (!File::files($dir)) {
            File::deleteDirectory($dir);
        }

        return;
    }

    public function test(MaximusClaim $claim)
    {
        Mail::send(new AobSubmitted($claim));
        return 'success';
    }
}
