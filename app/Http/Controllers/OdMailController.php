<?php

namespace App\Http\Controllers;

use App\Mail\ForwardOdmail;
use App\Mail\ResendOdmail;
use App\Odmail;
use App\OdmailRecipient;
use App\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OdMailController extends Controller
{
    public function index($odmail_id=null)
    {
        $q = request()->query('q');
        $odmails = Odmail::latest()->paginate(20);

        return view('odmail.index', compact('odmails'));
    }

    public function show(Odmail $odmail)
    {
        $odmail->load('media.model');
        return view('odmail.show', compact('odmail'));
    }

    public function resend(Odmail $odmail)
    {
        Mail::send(new ResendOdmail($odmail));
        return redirect()->back()->with('success', 'Email Sent Successfully');
    }

    public function forward(Request $request, Odmail $odmail)
    {
        $request->validate([
            'recipients' => 'required'
        ]);

        Mail::send(new ForwardOdmail($odmail, $request->recipients));
        return redirect()->back()->with('success', 'Email Forwarded Successfully');
    }
}
