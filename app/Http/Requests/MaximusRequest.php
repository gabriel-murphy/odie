<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaximusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'customer_id' => 'required',
            'customer.first_name' => 'required',
            'customer.last_name' => 'required',
            'customer.email' => 'required|email',
            'customer.cell_phone' => 'required',

            'address.full_address' => 'required',
            'estimator_id' => 'required',
            'claim_type_id' => 'required',
            'minimum_damaged_units' => 'required',
            'xactimate_amount' => 'required',
            'insurance_carrier_id' => 'required',
            'claim_number' => 'required',
            'policy_number' => 'required',
            'date_of_loss' => 'required',
//            'property_id' => 'required',
            'aob' => 'required|mimes:pdf',
            'xactimate' => 'required|mimes:pdf',
            'itel' => 'nullable|mimes:pdf',
            'declaration' => 'nullable|mimes:pdf',
            'photos.*' => 'required|mimes:jpeg,png,jpg,gif,svg|min:2',
            'photos' => 'required|min:8|max:8',
            'ariel' => 'required|mimes:pdf',
            'template.subject' => 'required',
            'template.content' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'address.*' => 'The address field must required.',
            'photos.min' => 'The :attribute field must have at least :min photos.',
            'photos.max' => 'The :attribute field maximum :max photos.',
        ];
    }
}
