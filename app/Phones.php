<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Phones extends Model
{
    use SoftDeletes;

   protected $guarded = [];

    public function phoneable()
    {
        return $this->morphTo();
    }
}
