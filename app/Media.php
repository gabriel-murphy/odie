<?php

namespace App;

use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Models\Media as BaseMedia;

class Media extends BaseMedia
{
    public function getIconAttribute()
    {
        $media_types = [
            'pdf' => ['pdf'],
            'image' => ['jpeg', 'jpg'],
            'html' => ['html'],
            'text' => ['text'],
        ];

        $icon = asset('images/file_icons/file.png');

        foreach ($media_types as $type => $extensions) {
            if (Str::contains($this->extension, $extensions)) {
                $icon = asset("images/file_icons/{$type}.png");
                break;
            }
        }

        return $icon;
    }

    public function isMissing(string $conversionName = '')
    {
        return !file_exists($this->getPath($conversionName));
    }

    public function getFallbackImage()
    {
        return asset('images/defaults/no_preview.png');
    }

    public function getMediaType()
    {
        if(!$this->hasCustomProperty('media_type_id')){
            return false;
        }

        return app('constants')->find($this->getCustomProperty('media_type_id'));
    }

    public function getNameWithExtensionAttribute()
    {
        return "{$this->name}.{$this->extension}";
    }
}
