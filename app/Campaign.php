<?php

namespace App;

use App\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;

class Campaign extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['template_id', 'name', 'description', 'columns', 'status',];

    protected $casts = [
        'columns' => 'array'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('damage-photos');
        $this->addMediaCollection('documents');
    }

    public function recipients()
    {
        return $this->hasMany(CampaignRecipient::class);
    }

    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    public function getStats($status = null, $get_percentage = false)
    {
        $column = "{$status}_at";

        $total_recipients = $this->recipients->count();

        if (!$status) {
            $count = $this->sent_emails;
        } else {
            $count = $this->recipients->whereNotNull($column)->count();
        }

        $percentage = $total_recipients ? round($count / $total_recipients * 100) . '%' : '0%';

        return $get_percentage ? $percentage : $count;
    }
}
