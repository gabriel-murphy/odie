<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constant extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'key', 'active', 'description', 'additional_data'];

    protected $casts = [
        'additional_data' => 'json'
    ];

    protected static function boot()
    {
        parent::boot();
        Constant::retrieved(function ($constant) {
            if ($constant->additional_data) {
                foreach ($constant->additional_data as $key => $value) {
                    $constant->{$key} = $value;
                }
            }
//            unset($constant->additional_data);
        });
    }

    public function constantGroup()
    {
        return $this->belongsTo(ConstantGroup::class);
    }

    public function scopeKey($q, $key)
    {
        return $q->where('key', $key)->first();
    }

    public function scopeGroup($q, $group)
    {
        return $q->whereHas('constantGroup', function ($q) use ($group) {
            if (is_numeric($group)) {
                $q->where('id', $group ?? '');
            } else {
                $q->where('key', $group);
            }
        });
    }

    public function scopeRoofTypes($q, $root_categories = false, $category_id = null)
    {
        $roof_types = $q->group('roof_types')->get();
        return $roof_types->filter(function ($roof_type) use ($root_categories, $category_id) {
            if ($category_id) return isset($roof_type->roof_type_id) && $roof_type->roof_type_id == $category_id;
            else return $root_categories ? !isset($roof_type->roof_type_id) : isset($roof_type->roof_type_id);
        });
    }

    public static function getUnit($id)
    {
        return self::query()->group('units')->key($id);
    }

    public static function getUnitName($id)
    {
        return self::getUnit($id)->name;
    }

}
