<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 8/20/2020
 * Time: 8:08 PM
 */

namespace App\Helpers;


use App\Appointment;
use App\Contact;
use App\Template;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Assets
{

    public $roofTypes, $recentAppointments, $templates, $contacts;

    public function __construct()
    {
        if (!Schema::hasTable('roof_types')){
            return;
        }

        $this->roofTypes =  DB::table('roof_types')->select()->get();
        $this->recentAppointments =  $this->getRecentAppointments();
        $this->templates =  Template::all();
        $this->contacts =  Contact::all();
    }

    private function getRecentAppointments()
    {
        $data['appointments'] = Appointment::where('created_by', auth()->id())->take(5)->get();
        $data['count'] = Appointment::where('created_by', auth()->id())->count();
        return $data;
    }

    public function getTemplate($key)
    {
        return $this->templates->where('key', $key)->first();
    }

    public function getEscalators()
    {
        return $this->contacts->whereIn('client_relationship_id', [1, 2]);
    }

}
