<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 8/20/2020
 * Time: 8:08 PM
 */

namespace App\Helpers;


use App\Constant;
use App\ConstantGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ConstantsHelper
{
    public static $attachment_groups = [
        'odie' => 1,
        'requested_info' => 1
    ];

    public static $attachment_types = [
        'damage_photos' => 17,
        'invoice' => 8,
        'aob' => 3,
        'itel' => 12,
        'xactimate' => 10,
        'ariel' => 11,
        'policy' => 15,
        'other' => 21,
    ];

    public $constants = array();
    public $documentTypes = array();
    public $claimStatuses;

    public function __construct()
    {
        if (!Schema::hasTable('constants')){
            return;
        }

        $this->constants = Constant::all();
        $this->constantGroups = ConstantGroup::all();

        $this->documentTypes = $this->constants->where('constant_group_id', 6);
        $this->claimStatuses = $this->constants->where('constant_group_id', 4);
    }

    public function getAttachmentGroupId($group)
    {

    }

    public function getAttachmentTypeId($type)
    {
        return $this->documentTypes->where('key', self::$attachment_types[$type])->first()->id;
    }

    public function requestedInfoTypes()
    {
        return $this->documentTypes->whereIn('key', [12, 16, 17, 21]);
    }

    public function carrierResponseTypes($claim = null)
    {
        $types = $claim && $claim->status_id < 25 ? [18, 19, 20, 21] : [19, 20, 21];
        return $this->documentTypes->whereIn('key', $types);
    }

    public function findById($id)
    {
        return $this->constants->where('id', $id)->first();
    }

    public function find($id)
    {
        return $this->constants->where('id', $id)->first();
    }

    public function findByGroup($groupId)
    {
        if (is_numeric($groupId)) {
            $constantGroup = $this->constantGroups->find($groupId);
        } else {
            $constantGroup = $this->constantGroups->where('key', $groupId)->first();
        }

        if (!$constantGroup) {
            return array();
        }

        return $constantGroup->constants;
    }

    public function constant($key)
    {
        $constant = $this->constants->where('key', $key)->first();
        return $constant ? $constant->name : '';
    }

    public function getName($key, $group = null)
    {
        if ($group) {
            $constant = $this->findByGroup($group)->where('key', $key)->first();
        } else {
            $constant = $this->constants->where('key', $key)->first();
        }

        return $constant ? $constant->name : '';
    }

    public function groupConstantsResponse($id)
    {
        return $this->constants->where('constant_group_id', $id);
    }

}
