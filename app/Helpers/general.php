<?php

function _active_menu($route, $parent = false)
{
    if ($parent)
        return request()->is($route) || request()->is($route.'/*') ? ' open' : '';
    else
        return request()->is($route) ? ' active' : '';
}

function _parse_template($template, $models, $subject = false)
{
    $replace_data = [];
    foreach ($template->placeholders as $placeholder){
        $model = $models[$placeholder->model] ?? null;
        $key = "%%$placeholder->name%%";
        if ($model) $replace_data[$key] = $model->{$placeholder->attribute};
    }

    $content = $subject ? $template->subject : $template->content;

     return strtr($content, $replace_data);
}

function _sidebar_menu($text, $route = 'javascript:void(0)')
{
    $class = $route == request()->url() ? 'active' : '';
    return '<li><a class="' . $class . '" href="' . $route . '">' . $text . '</a></li>';
}

function _sidebar_menu_group($text, $icon)
{
    return
        '<a class="nav-submenu" data-toggle="nav-submenu" href="#">
            <i class="'.$icon.'"></i><span class="sidebar-mini-hide">'.$text.'</span>
        </a>';
}

function _storage_url($path, $append_storage = false)
{
    if ($append_storage)
        return asset('storage/' . $path);
    else return asset($path);
}

function _active_tab($property, $string, $empty_case = false)
{
    if ($empty_case) {
        $state = request()->query($property) == $string || empty(request()->query()) ? 'active' : '';
    } else {
        $state = request()->query($property) == $string ? 'active' : '';
    }
    return $state;
}


