<?php

namespace App;

use App\Casts\Date;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use App\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class CarrierResponse extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['dated_on', 'received_on', 'response_type_id'];

    protected $casts = [
      'dated_on' => Date::class,
      'received_on' => Date::class,
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->height(150)
            ->sharpen(10)
            ->watermark(public_path('images/logo.png'))
            ->watermarkOpacity(10)
            ->watermarkPosition('center');
    }
}
