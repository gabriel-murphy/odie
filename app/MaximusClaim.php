<?php

namespace App;

use App\Casts\Date;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use App\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MaximusClaim extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('damage-photos');
        $this->addMediaCollection('documents');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10)
            ->watermark(public_path('images/logo.png'))
            ->watermarkOpacity(10)
            ->watermarkPosition('center');
    }

    const ESCALATED = 31, DECIDED = 29;

    protected $fillable = [
        'uuid', 'user_id', 'estimator_id', 'customer_id', 'signature_date', 'insurance_carrier_id', 'claim_type_id', 'claim_number', 'policy_number',
        'xactimate_amount', 'date_of_loss', 'property_id', 'mail_status', 'aob_signed_on', 'status_id', 'minimum_damaged_units',
        'adjuster_meeting_at', 'temp_repair_needed', 'description', 'coverage_type_id', 'coverage_amount', 'denial_reason', 'escalator_id',
        'mortgage_company_id', 'mortgage_account_number'
    ];

    protected $casts = [
        'aob_signed_on' => Date::class,
        'date_of_loss' => Date::class,
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->status_id = 22;
        });
    }


    /************* Relationships **************/

    public function customer()
    {
        return $this->belongsTo(Customer::class)->withDefault();
    }

    public function estimator()
    {
        return $this->belongsTo(User::class, 'estimator_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function FieldAdjuster()
    {
        return $this->belongsTo(Contact::class, 'field_adjuster_id');
    }

    public function escalator()
    {
        return $this->belongsTo(Contact::class, 'escalator_id');
    }

    public function DeskAdjuster()
    {
        return $this->belongsTo(Contact::class, 'desk_adjuster_id');
    }

    public function tempRepair()
    {
        return $this->hasOne(TempRepair::class);
    }

    public function carrierResponses()
    {
        return $this->hasMany(CarrierResponse::class);
    }

    public function submissions()
    {
        return $this->hasMany(MaximusInfoSubmission::class);
    }


    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function insuranceCarrier()
    {
        return $this->belongsTo(InsuranceCarrier::class);
    }

    public function mails()
    {
        return $this->morphMany(Odmail::class, 'mailable');
    }

    public function statusLogs()
    {
        return $this->hasMany(MaximusStatusLog::class);
    }

    public function status()
    {
        // constant_group : claim statuses
        return $this->belongsTo(Constant::class, 'status_id');
    }

    /************* Accessors & Mutators **************/


    public function getSubmittedInDaysAttribute()
    {
        $diff = $this->submitted_at ? $this->submitted_at->diffInDays() : '';
        $string = '';
        if ($diff) {
            if ($diff == 0) $string = 'Today';
            else if ($diff == 1) $string = 'Yesterday';
            else  $string = $diff . ' days ago';
        }
        return $string;
    }

    public function getIsOverdueAttribute()
    {
        $diff = $this->submitted_at ? $this->submitted_at->diffInDays() : null;
        return in_array($this->status_id, [22, 24]) && $diff > 15;
    }

    public function getCarrierWaitingDaysAttribute()
    {
        $info_requested = $this->carrierResponses->where('response_type_id', 54)->last();
        return ($info_requested && $this->status_id == 28) ? $info_requested->created_at->diffInDays() : null;
    }

    public function getSubmittedAtAttribute()
    {
        $mail = $this->mails->where('template_id', 3)->where('resent', 0)->first();
        return $mail ? $mail->created_at : null;
    }

    public function getDecidedOnAttribute()
    {
        $decision = $this->carrierResponses->where('response_type_id', 55)->first();
        return $decision ? $decision->dated_on : null;
    }

    public function getDecisionAttribute()
    {
        return app('constants')->find($this->coverage_type_id)->name;
    }

    /************* Functions **************/
    public function logStatusChange($status_id)
    {
        if ($this->status_id != $status_id) {
            $this->statusLogs()->create([
                'claim_status_id' => $status_id,
            ]);
        }
        return $status_id;
    }

}
