<?php

namespace App\Providers;

use App\Helpers\Assets;
use App\Helpers\ConstantsHelper;
use App\Services\Datalab\Datalab;
use Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Org_Heigl\Ghostscript\Ghostscript;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Datalab::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        Blade::directive('formattedDate', function ($expression) {
            return "<span title='<?php echo $expression ? ($expression)->format('F d, Y h:m A') : ''; ?>'> <?php echo $expression ? ($expression)->diffForHumans() : ''; ?></span>";
        });

        Blade::directive('usd', function ($expression) {
            return "<?php echo '\$'.	(number_format($expression, 2, '.', ',')); ?>";
        });

        $this->app->singleton('constants', function ($app) {
            return new ConstantsHelper();
        });

        $this->app->singleton('assets', function ($app) {
            return new Assets();
        });

        View::share('_constants', app()->make('constants'));
        View::share('_assets', app()->make('assets'));

        if (env('GS_PATH')){
            Ghostscript::setGsPath(env('GS_PATH'));
        }
    }
}
