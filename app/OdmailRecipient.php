<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class OdmailRecipient extends Model
{
    protected $fillable = ['name', 'email', 'status', 'delivered_at', 'opened_at', 'type'];

    protected $dates = ['delivered_at', 'opened_at'];

    public function getStatusTextAttribute()
    {
        return config('constants.mail_statuses.' . $this->status);
    }

    public function getIconAttribute()
    {
        $icon = 'fal fa fa-clock';
        if ($this->status == 1) // delivered
            $icon =  'fal fa fa-envelope text-success';
        if ($this->status == 2) // opened
            $icon =  'fal fa fa-envelope-open text-primary';
        return $icon;
    }

    public function scopeType(Builder $builder, $type = 'to')
    {
        return $builder->where('type', $type);
    }

    public function odmail()
    {
        return $this->belongsTo(Odmail::class);
    }
}
