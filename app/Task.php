<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['client_id', 'title', 'description', 'project_id', 'due_date', 'completed_by'];

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function doer()
    {
        return $this->belongsTo(User::class, 'completed_by');
    }

    public function assignees()
    {
        return $this->belongsToMany(User::class);
    }

    public function scopeCompleted($q, $completed = true)
    {
        if ($completed) return $q->whereNotNull('completed_by');
        else $q->whereNull('completed_by');
    }

    public function getCompletedAttribute()
    {
        return boolval($this->completed_by);
    }
}
