<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cost extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    protected $casts = [
        'additional_data' => 'json',
        'criteria' => 'json'
    ];

    protected $fillable = [
        'name', 'type', 'description', 'criteria', 'cost', 'additional_data', 'active'
    ];
}
