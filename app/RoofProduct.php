<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoofProduct extends Model
{
    protected $casts = [
        'samples' => 'json',
        'options' => 'json',
        'details' => 'json'

    ];

    public function manufacturer()
    {
        return $this->belongsTo(RoofManufacturer::class);
    }

    public function type()
    {
        return $this->belongsTo(RoofType::class);
    }

    public function series()
    {
        return $this->hasMany(RoofProduct::class);
    }

    public function shingleColors()
    {
        return $this->hasMany(RoofProductColor::class, 'roof_product_id');
    }

    public function colors()
    {
        return $this->belongsToMany(RoofProductColor::class, 'product_colors', 'roof_manufacturers_products_id', 'roof_product_color_id');
    }

}
