<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDivisionGateCodes extends Model
{
    protected $table = "subdivision_gate_codes";
}
