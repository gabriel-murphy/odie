<?php

namespace App;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;

class InsuranceCarrier extends Model
{
    use Searchable;

    protected $fillable = ['name', 'email', 'website', 'phone', 'claims_url', 'claims_phone'];
}
