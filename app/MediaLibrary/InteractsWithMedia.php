<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 8/31/2020
 * Time: 2:17 AM
 */

namespace App\MediaLibrary;

use Spatie\MediaLibrary\InteractsWithMedia as BaseInteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait InteractsWithMedia
{
    use BaseInteractsWithMedia;

    public function getLastMedia(string $collectionName = 'default', $filters = []): ?Media
    {
        $media = $this->getMedia($collectionName, $filters);

        return $media->last();
    }
}
