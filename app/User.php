<?php

namespace App;

use App\Traits\HasClient;
use App\Traits\Searchable;
use App\Traits\UploadImage;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable, UploadImage, Searchable, HasRoles, HasClient;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'secondary_email', 'title', 'password', 'company', 'phone',
        'client_id', 'avatar', 'signature_image', 'cell_phone', 'home_phone', 'work_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function assignedTasks()
    {
        return $this->belongsToMany(Task::class);
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getAvatarPathAttribute()
    {
        $path = asset('/images/user/0.png');
        if ($this->avatar && file_exists('storage/' . $this->avatar))
            $path = asset('storage/' . $this->avatar);
        return $path;

    }

    public function getSignaturePathAttribute()
    {
        $path = asset('/images/user/0.png');
        if ($this->signature_image && file_exists('storage/' . $this->avatar))
            $path = asset('storage/' . $this->signature_image);
        return $path;
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
