<?php

namespace App\Jobs;

use App\Mail\Maximus\AobSubmitted;
use App\MaximusClaim;
use Arr;
use File;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;
use PDF;

class ProcessMaximusClaimUploads implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $claim, $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MaximusClaim $claim)
    {
        $this->claim = $claim;
        /*$this->request = $request;*/
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function handle()
    {
        $claim = $this->claim;

        $dir = storage_path("app/temp/{$claim->uuid}");

        $files = File::files($dir);

        $documents = [
            [
                'type_id' => app('constants')->getAttachmentTypeId('aob'),
                'file' => 'aob', 'name' => '01_AOB'
            ],
            ['path' => 'documents/maximus/02_Duties.pdf'],
            ['path' => 'documents/maximus/03_Deadline.pdf'],
            ['path' => 'documents/maximus/04_Brochure.pdf'],
            [
                'type_id' => app('constants')->getAttachmentTypeId('xactimate'),
                'file' => 'xactimate', 'name' => '06_Xactimate'
            ],
            [
                'type_id' => app('constants')->getAttachmentTypeId('itel'),
                'file' => 'itel', 'name' => '08_ITEL'
            ],
            [
                'type_id' => app('constants')->getAttachmentTypeId('ariel'),
                'file' => 'ariel', 'name' => '07_Measurements'
            ],
            ['path' => 'documents/maximus/09_License.pdf'],
            ['path' => 'documents/maximus/10_W-9.pdf'],
            ['path' => 'documents/maximus/11_Insurance.pdf'],
        ];

        foreach ($documents as $document) {
            if (isset($document['path'])) {
                $claim->addMedia(public_path($document['path']))
                    ->preservingOriginal()
                    ->toMediaCollection('documents');
            } else {
                $file = Arr::first(
                    Arr::where($files, function ($file) use ($document) {
                        return explode('_', $file->getBasename())[0] == $document['file'];
                    })
                );

                if (!$file) {
                    continue;
                }

                $claim->addMedia($file->getPathName())
                    ->usingName($document['name'])
                    ->withCustomProperties(['media_type_id' => $document['type_id']])
                    ->toMediaCollection('documents');
            }
        }

        $damage_photos = Arr::where($files, function ($file){
            return explode('_', $file->getBasename())[0] == 'photos';
        });

        foreach ($damage_photos as $photo) {
            $media = $claim->addMedia($photo->getPathName())->toMediaCollection('damage-photos');
            $photos[] = $media->getUrl();
        }

        $pdf = PDF::loadView('maximus.photos', compact('photos'));

        $claim->addMediaFromString($pdf->output())
            ->usingName('05_Photos')
            ->usingFileName('photos.pdf')
            ->toMediaCollection('documents');

        if (!File::files($dir)){
            File::deleteDirectory($dir);
        }

        $response = Mail::send(new AobSubmitted($claim));
        dd($response);
        return;
    }
}
