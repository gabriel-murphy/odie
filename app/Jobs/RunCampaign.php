<?php

namespace App\Jobs;

use App\Campaign;
use App\Mail\CampaignMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class RunCampaign implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Campaign
     */
    private $campaign;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $recipients = $this->csvToArray($this->campaign->getFirstMediaPath(), $this->campaign->columns);

        foreach ($recipients as $recipient){

            if ($this->campaign->recipients->where('email', $recipient['email'])->count()){
                continue;
            }

            $this->campaign->recipients()->create([
                'email' => $recipient['email'],
                'name' => $recipient['name'] ?? '',
                'additional_attributes' => Arr::except($recipient, ['email', 'name'])
            ]);
        }

        $this->campaign->load('recipients');

        foreach ($this->campaign->recipients as $recipient){
            if ($recipient->status){
                continue;
            }

            \Mail::send(new CampaignMail($this->campaign, $recipient));
        }

        $this->campaign->update([
            'status' => 1
        ]);
    }


    private function csvToArray($filename = '', $columns = array(), $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;

        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        $columns = $columns ? array_map(function ($value){
            return strtolower($value);
        }, $columns) : $columns;

        return collect($data)
            ->filter(function ($data) {
                return array_sum($data) && $data['email'];
            })
            ->map(function ($data) {
                return array_change_key_case($data, CASE_LOWER);
            })
            ->map(function ($data) use ($columns) {
                return $columns ? Arr::only($data, $columns) : $data;
            })
            ->filter(function ($data) {
                $job_type = $data['job type'];
                return
                    $job_type
                    &&  ! Str::contains($job_type, ['Shingle', 'Metal', 'Flat', 'TPO', 'Maintenance', 'Service', 'Warranty', 'Low Slope', 'Replacement'])
                    && ! Str::contains($data['created by'], 'Pedro');
            });
    }
}
