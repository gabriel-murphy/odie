<?php

namespace App\Jobs\MailgunWebhooks;

use App\CampaignRecipient;
use App\MaximusClaim;
use App\Odmail;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Models\WebhookCall;

class HandleFailed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public  $webhookCall;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $payload = $this->webhookCall->payload;
        $variables = $payload['event-data']['user-variables'];
        $odmail_id = isset($variables['odmail_id']) ? $variables['odmail_id'] : null;
        $campaign_recipient_id = isset($variables['campaign_recipient_id']) ? $variables['campaign_recipient_id'] : null;

        if ($odmail_id){
            $this->handleOdmail($odmail_id, $payload);
        }

        if ($campaign_recipient_id){
            $this->handleCampaign($campaign_recipient_id, $payload);
        }

    }

    private function handleOdmail($odmail_id, $payload)
    {
        $odmail = Odmail::find($odmail_id);

        if (!$odmail){
            return;
        }

        $recipient = $odmail->recipients()->where('email', $payload['event-data']['recipient'])->first();

        if (!$recipient){
            return;
        }

        $recipient->update([
            'status' => 3, // Failed
            'opened_at' => Carbon::createFromTimestamp($payload['event-data']['timestamp'])->toDateTimeString()
        ]);
    }

    private function handleCampaign($recipient_id, $payload)
    {
        $recipient = CampaignRecipient::find($recipient_id);

        if (!$recipient){
            return;
        }

        $recipient->update([
            'status' => 3, // Failed
            'failed_at' => Carbon::createFromTimestamp($payload['event-data']['timestamp'])->toDateTimeString()
        ]);
    }
}
