<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\Datalab\Datalab;
class PeopleEstatedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $primary_email;
    public $customer;
    public $address;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($customer,$primary_email,$address)
    {
        $this->primary_email = $primary_email;
        $this->customer = $customer;
        $this->address = $address;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Datalab $datalab)
    {
        $params = [
            'email' => $this->primary_email,
        ];
        $people_datalab_response = $datalab->getPersonData($params);
        if (isset($people_datalab_response) && count($people_datalab_response) > 0 ){
            $this->customer->update(['enhanced_data'=>$people_datalab_response]);
        }

        $params = [
            'street' =>     $this->address['street'],
            'zip_code' =>   $this->address['zip_code'],
            'state' =>      $this->address['state'],
            'city' =>       $this->address['city'],
        ];
        $estated_response = $datalab->getPropertyData($params);
        if (isset($estated_response) && count($estated_response) > 0 ){
            $this->customer->address()->update(['enhanced_data'=>$estated_response]);
        }
    }
}
