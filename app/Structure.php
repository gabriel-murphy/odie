<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    protected $fillable = ['name', 'measurements', 'penetrations'];

    protected $casts = [
        'measurements' => 'json',
        'penetrations' => 'json'
    ];

    protected static function boot()
    {
        parent::boot();
        Structure::retrieved(function ($model) {

        });
    }

    /************* Relationships **************/
    public function estimates()
    {
        return $this->hasMany(Estimate::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /************* Attributes **************/
    public function getMeasurementsAttribute($value)
    {
        $value = json_decode($value, true);
        if ($value && isset($value['square_feet'])){
            $value['square'] = $value['square_feet']/100;
            $value['drip_edge'] = $value['rakes'] + $value['eaves'];
            $value['caps'] = $value['ridges'] + $value['hips'];
        }
        return $value;
    }
}
