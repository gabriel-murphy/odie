<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class PermitJurisdiction extends Model
{
    public static function getJurisdictionDetails(Address $address)
    {
        $city = $address->city;
        $county = Str::before($address->county, ' county');
        $details = self::where('type', 'city')->where('name', $city)->first();
        if (!$details){
            $details = self::where('type', 'county')->where('name', $county)->first();
        }
        return $details;
    }

    public function getFee($squares)
    {
        $total_fee = 0;
        // Now that we have $permit_details, let's do the math on the permit
        $total_fee += $this->fixed_fee;
        $total_fee += $this->per_square_fee * $squares;

        if ($this->tier_fee) {   // If it is a tier fee model
            $permit_tier = 100;
            $total_fee += $permit_tier;
        }
        return $total_fee;
    }
}
