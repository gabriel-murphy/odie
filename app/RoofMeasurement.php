<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class RoofMeasurement extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'square_feet', 'ridges', 'ridge_count', 'hips', 'hip_count', 'valleys', 'valley_count', 'rakes', 'rake_count', 'eaves', 'eave_count',
        'flashing', 'flashing_lengths', 'step_flashing', 'step_flashing_lengths', 'ev_suggested_waste', 'drip_edge', 'ridges_hips_sum'
    ];

}
