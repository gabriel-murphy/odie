<?php

namespace App;

use App\Traits\HasClient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Project extends Model
{
    use SoftDeletes;
    use HasClient;

    public $timestamps = true;

    protected $fillable = [
        'customer_id', 'phase_id', 'user_id', 'project_type', 'property_id', 'name', 'billing_address_id',
        'description', 'current_roof_type_id', 'preferred_roof_type_id', 'customer_notes'
    ];

    /************* Relationships **************/
    public function phase()
    {
        return $this->belongsTo(Phase::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function structures()
    {
        return $this->hasMany(Structure::class);
    }

    public function estimator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function estimates()
    {
        return $this->hasMany(Estimate::class);
    }

    /************* Attributes **************/

    public function getLabelAttribute()
    {
        return $this->customer->last_name .'-'. $this->property->city_name;
    }

    public function getTitleAttribute()
    {
        return $this->customer->last_name .'-'. $this->property->address->city;
    }

    public function getAddressAttribute()
    {
        return $this->property->address->full_address;
    }

    public function getCurrentMaterialNameAttribute()
    {
        return RoofType::where('id', $this->current_roof_type_id)->first()->name;
    }

    public function getPreferredMaterialNameAttribute()
    {
        return RoofType::where('id', $this->preferred_roof_type_id)->first()->name;
    }

    public function getProjectTypeAttribute()
    {
        return app('constants')->getName($this->project_type_id, 'project_types');
    }

    public function getRelationshipName()
    {
        return app('constants')->getName($this->property->relationship_id);
    }
}
