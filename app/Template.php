<?php

namespace App;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use Searchable;

    public $timestamps = false;

    protected $fillable = ['name', 'subject', 'content', 'type', 'key', 'active', 'recipients'];

    protected $casts = [
        'recipients' => 'json',
    ];

    public function placeholders()
    {
        return $this->belongsToMany(Placeholder::class, 'template_placeholder');
    }

    public function getIsActiveAttribute()
    {
        $ACTIVE = ['text' => 'Active', 'icon' => 'fa fa-unlock-alt fa'];
        $INACTIVE = ['text' => 'Inactive', 'icon' => 'fa fa-lock'];
        return $this->active ? $ACTIVE : $INACTIVE;
    }

    public function scopeType(Builder $query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeKey(Builder $query, $key)
    {
        return $query->where('key', $key);
    }
}
