<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Queue\InteractsWithQueue;

class OdmailSending
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        $data = $event->data;

        if (isset($data['mailable'])){
            $this->handleOdmail($event);
        }

        if (isset($data['campaignRecipient'])){
            $this->handleCampaign($event);
        }
    }

    private function handleOdmail(MessageSending $event)
    {
        $data = $event->data;
        $mail = $event->message;

        $attachments = $data['attachments'];
        $mailable = $data['mailable'];
        $template = $data['template'];

        $odmail = $mailable->mails()->create([
            'from' => array_key_first($mail->getFrom()),
            'subject' => $mail->getSubject(),
            'body' => $template->content,
            'template_id' => $template->id,
        ]);

        foreach ($mail->getTo() as $address => $recipient) $odmail->recipients()->create(['email' => $address, 'type' => 'to']);
        if ($mail->getCc()) foreach ($mail->getCc() as $address => $recipient) $odmail->recipients()->create(['email' => $address, 'type' => 'cc']);
        if ($mail->getBcc()) foreach ($mail->getBcc() as $address => $recipient) $odmail->recipients()->create(['email' => $address, 'type' => 'bcc']);


        foreach ($attachments as $attachment)
        {
            $odmail->addMedia($attachment['file'])
                ->usingName($attachment['options']['as'] ?? null)
                ->preservingOriginal()
                ->toMediaCollection();
        }

        $mail->getHeaders()->addTextHeader('X-Mailgun-Variables', json_encode([
            'odmail_id' => $odmail->id,
        ]));
    }

    private function handleCampaign(MessageSending $event)
    {
        $data = $event->data;
        $mail = $event->message;

        $recipient = $data['campaignRecipient'];

        $mail->getHeaders()->addTextHeader('X-Mailgun-Variables', json_encode([
            'campaign_recipient_id' => $recipient->id,
        ]));
    }
}
