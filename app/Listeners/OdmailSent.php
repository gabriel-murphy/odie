<?php

namespace App\Listeners;

use App\Mail\ForwardOdmail;
use App\Odmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class OdmailSent
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        $this->handleOdmail($event);
        $this->handleCampaign($event);
    }

    private function handleOdmail(MessageSent $event)
    {
        $data = $event->data;
        $mail = $event->message;

        $forwardTo = $data['forwardTo'] ?? null;
        $odmail = null;

        $header = $mail->getHeaders()->get('X-Mailgun-Variables');

        if (!$header) {
            return;
        }

        $parsed_header = json_decode($header->getFieldBody(), true);

        $odmail_id = $parsed_header['odmail_id'] ?? null;

        $odmail = Odmail::find($odmail_id);

        if (!$odmail){
            return;
        }

        if ($forwardTo){
            Mail::send(new ForwardOdmail($odmail, $forwardTo));
        }
    }

    private function handleCampaign(MessageSent $event)
    {
        $data = $event->data;

        $recipient = $data['campaignRecipient'] ?? null;

        if (!$recipient) {
            return;
        }

        $recipient->update([
            'sent_at' => now()
        ]);
    }
}
