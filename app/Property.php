<?php

namespace App;

use App\Traits\HasClient;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasClient;

    protected $fillable = [
        'client_id', 'customer_id', 'relationship_id', 'raw_data', 'enhanced_data', 'classification_id'
    ];

    protected $casts = [
        'raw_data' => 'array',
        'enhanced_data' => 'array',
    ];

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable')->withDefault();
    }

    public function claim()
    {
        return $this->belongsTo(MaximusClaim::class);
    }

    public function getFullAddressAttribute()
    {
        return $this->address ? implode(', ', $this->address->only(['street', 'city', 'state'])) : '';
    }
}
