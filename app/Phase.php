<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Phase extends Model
{
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
