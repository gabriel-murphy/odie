<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoofLayer extends Model
{
    public $timestamps = false;

    public function buildLists()
    {
        return $this->hasMany(RoofBuildList::class);
    }

    public function materials()
    {
        return $this->belongsToMany(Material::class, 'roof_build_list')->withPivot(['roof_type_id', 'client_id', 'component']);
    }
}
