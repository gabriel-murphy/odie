<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class RoofType extends Model
{
    public $timestamps = false;

    public $casts = [
        'additional_data' => 'json'
    ];

    protected static function boot()
    {
        parent::boot();
        RoofType::retrieved(function ($roof_type) {
            if ($roof_type->additional_data){
                foreach ($roof_type->additional_data as $key => $value)
                {
                    $roof_type->{$key} = $value;
                }
            }
            unset($roof_type->additional_data);
        });
    }

    public function roofTypes()
    {
        return $this->hasMany(RoofType::class);
    }

    public function manufacturers()
    {
        return $this->belongsToMany(RoofManufacturer::class);
    }

    public function scopeGroups(Builder $builder)
    {
        return $builder->whereNull('roof_type_id');
    }

    public function scopeTypes(Builder $builder)
    {
        return $builder->whereNotNull('roof_type_id');
    }

    public function scopeParent(Builder $builder)
    {
        return $builder->whereNull('roof_type_id');
    }
}
