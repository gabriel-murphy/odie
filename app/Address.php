<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'client_id', 'street', 'city', 'county', 'state', 'zip_code', 'country', 'addressable_type',
        'latitude', 'longitude', 'addressable_id', 'latitude', 'longitude','enhanced_data'
    ];

    protected $casts = [
        'enhanced_data' => 'array',
    ];

    public function addressable()
    {
        return $this->morphTo();
    }

    public function getFullAddressAttribute()
    {
        return implode(', ', $this->only(['street', 'city', 'state', 'zip_code']));
    }

}
