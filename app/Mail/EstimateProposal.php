<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EstimateProposal extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $mailText;
    protected $attachFilePaths;
    protected $signlink;
    public $subject;

    public function __construct($subject, $mailText, $attachFilePaths, $signlink)
    {
        $this->subject = $subject;
        $this->mailText = $mailText;
        $this->attachFilePaths = $attachFilePaths;
        $this->signlink = $signlink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->markdown('emails.estimateProposal')->subject($this->subject)
        ->with(['mailText' => $this->mailText, 'signlink' => $this->signlink]);
        foreach($this->attachFilePaths as $path){
            $email->attach($path);
        }
        return $email;
    }
}
