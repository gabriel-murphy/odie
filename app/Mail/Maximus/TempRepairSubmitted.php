<?php

namespace App\Mail\Maximus;

use App\MaximusClaim;
use App\Odmail;
use App\Template;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TempRepairSubmitted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $claim, $files, $modifications;

    public $attachments, $mailable, $template, $forwardTo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MaximusClaim $claim, $files, $modifications = array())
    {
        $this->claim = $claim;
        $this->modifications = $modifications;
        $this->mailable = $this->claim;
        $this->files = $files;

        $this->template = Template::type('email')->key('temp_repair_created')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Setting Models
        $models['customer'] = $this->claim->customer;
        $models['address'] = $this->claim->property->address;
        $models['maximus_claim'] = $this->claim;
        $models['insurance_carrier'] = $this->claim->insuranceCarrier;

        // Setting modifications
        $this->template->subject = $this->modifications['subject'] ?? $this->template->subject;
        $this->template->content = $this->modifications['content'] ?? $this->template->content;

        // Parsing body and subject
        $content = _parse_template($this->template, $models);
        $subject = _parse_template($this->template, $models, true);

        // Setting parsed body to template
        $this->template->content = $content;

        $email = $this->view('emails.template', compact('content'));
        $email->subject($subject);

        $email->to($this->claim->insuranceCarrier->email);
        $email->bcc(['claims@romanroofinginc.com', 'gabriel@gabrielmurphy.com']);

        // Forward this email to these recipients
        $this->forwardTo = $this->claim->customer->email;

        foreach ($this->files as $attachment)
        {
            $email->attach($attachment->getPath(), [
                'as' => $attachment->name_with_extension,
                'mime' => $attachment->mime_type,
            ]);
        }

        return $email;
    }
}
