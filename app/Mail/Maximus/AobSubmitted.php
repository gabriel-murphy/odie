<?php

namespace App\Mail\Maximus;

use App\MaximusClaim;
use App\Media;
use App\Odmail;
use App\Template;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AobSubmitted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $claim, $modifications;

    public $attachments, $mailable, $template, $forwardTo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MaximusClaim $claim,  $modifications = array())
    {
        $this->claim = $claim;
        $this->mailable = $this->claim;
        $this->modifications = $modifications;

        $this->template = Template::type('email')->key('new_aob_submitted')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Setting Models
        $models['customer'] = $this->claim->customer;
        $models['address'] = $this->claim->property->address;
        $models['maximus_claim'] = $this->claim;
        $models['insurance_carrier'] = $this->claim->insuranceCarrier;

        // Setting modifications
        $this->template->subject = $this->modifications['subject'] ?? $this->template->subject;
        $this->template->content = $this->modifications['content'] ?? $this->template->content;

        // Parsing body and subject
        $content = _parse_template($this->template, $models);
        $subject = _parse_template($this->template, $models, true);

        // Setting parsed body to template
        $this->template->content = $content;

        $email = $this->view('emails.template', compact('content'));
        $email->subject($subject);

        $email->to('asharsaleem4@gmail.com');
        //$email->bcc(['claims@romanroofinginc.com', 'gabriel@gabrielmurphy.com']);

        // Forward this email to these recipients
        //$this->forwardTo = $this->claim->customer->email;


        foreach ($this->claim->getMedia('documents') as $attachment) {
            $email->attach($attachment->getPath(), [
                'as' => $attachment->name,
                'mime' => $attachment->mime_name,
            ]);
        }

        return $email;
    }

    private function getAttachments()
    {
        $document_types = ['aob', 'other', 'xactimate', 'ariel', 'itel'];
        $aob = $ariel = $other = $xactimate = $ariel = $itel = null;

        foreach ($document_types as $document_type) {
            $$document_type = $this->claim->getMedia('documents', function (Media $media) use ($document_type) {
                $media_type_id = $media->custom_properties['media_type_id'] ?? null;
                return $media_type_id == app('constants')->getAttachmentTypeId($document_type);
            });
        }

        return [
            ['name' => '01_AOB.pdf', 'path' => $aob->getPath()],
            ['name' => '02_Duties.pdf', 'path' => 'documents/maximus/2_Duties.pdf'],
            ['name' => '03_Deadline.pdf', 'path' => 'documents/maximus/3_Deadline.pdf'],
            ['name' => '04_Brochure.pdf', 'path' => 'documents/maximus/4_Brochure.pdf'],
            ['name' => '05_Photos.pdf', 'path' => $other->getPath()],
            ['name' => '06_Xactimate.pdf', 'path' => $xactimate->getPath()],
            ['name' => '07_Measurements.pdf', 'path' => $ariel->getPath()],
            ['name' => '08_ITEL.pdf', 'path' => $itel->getPath()],
            ['name' => '09_License.pdf', 'path' => 'documents/maximus/9_License.pdf'],
            ['name' => '10_W-9.pdf', 'path' => 'documents/maximus/10_W-9.pdf'],
            ['name' => '11_Insurance.pdf', 'path' => 'documents/maximus/11_Insurance.pdf'],
        ];
    }
}
