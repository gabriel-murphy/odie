<?php

namespace App\Mail;

use App\MaximusClaim;
use App\Odmail;
use App\Template;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResendOdmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $odmail;
    public $attachments, $mailable, $template;
    public $resend = 1;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Odmail $odmail)
    {
        $this->odmail = $odmail;
        $this->mailable = $this->odmail->mailable;
        $this->template = Template::type('email')->key('resend_email')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $models['odmail'] = $this->odmail;

        $content = _parse_template($this->template, $models);
        $subject = _parse_template($this->template, $models, true);

        $this->template->content = $content;

        $email = $this->view('emails.template', compact('content'));

        $recipients = $this->getRecipients();

        $email->to($recipients['to']);
        $email->cc($recipients['cc']);
        $email->bcc($recipients['bcc']);

        $email->subject($subject);

        foreach ($this->odmail->getMedia() as $attachment) {
            $email->attach($attachment->getPath(), [
                'as' => $attachment->name_with_extension,
                'mime' => $attachment->mime_type,
            ]);
        }

        return $email;
    }

    private function getRecipients()
    {
        $recipients = $this->odmail->recipients;

        return [
            'to' => $recipients->where('type', 'to')->pluck('email')->toArray(),
            'cc' => $recipients->where('type', 'cc')->pluck('email')->toArray(),
            'bcc' => $recipients->where('type', 'bcc')->pluck('email')->toArray(),
        ];
    }
}
