<?php

namespace App\Mail;

use App\MaximusClaim;
use App\Odmail;
use App\Template;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForwardOdmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $odmail, $recipients;
    public $attachments, $mailable, $template;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Odmail $odmail, $recipients)
    {
        $this->odmail = $odmail;
        $this->mailable = $this->odmail->mailable;
        $this->prepareRecipients($recipients);
        $this->template = Template::type('email')->key('forward_email')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $models['odmail'] = $this->odmail;

        $content = _parse_template($this->template, $models);
        $subject = _parse_template($this->template, $models, true);

        $this->template->content = $content;

        $email = $this->view('emails.template', compact('content'));

        $email->to($this->recipients['to']);
        if ($this->recipients['cc']) $email->cc($this->recipients['cc']);
        if ($this->recipients['bcc']) $email->bcc($this->recipients['bcc']);
        $email->subject($subject);

        foreach ($this->odmail->getMedia() as $attachment) {
            $email->attach($attachment->getPath(), [
                'as' => $attachment->name_with_extension,
                'mime' => $attachment->mime_type,
            ]);
        }

        return $email;
    }

    private function prepareRecipients($recipients)
    {
        $prepared_recipients = array();

        $recipients = (!is_array($recipients) && $recipients) ? array($recipients) : $recipients;

        if (!isset($recipients['to']) && !isset($recipients['cc']) && !isset($recipients['bcc'])) {
            $recipients['to'] = $recipients;
        }

        $prepared_recipients['to'] = $recipients['to'] ?? array();
        $prepared_recipients['cc'] = $recipients['cc'] ?? array();
        $prepared_recipients['bcc'] = $recipients['bcc'] ?? array();

        $this->recipients = $prepared_recipients;
    }
}
