<?php

namespace App\Mail;

use App\Campaign;
use App\CampaignRecipient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CampaignMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var Campaign
     */
    private $campaign;
    /**
     * @var CampaignRecipient
     */
    public $campaignRecipient, $template;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Campaign $campaign, CampaignRecipient $recipient)
    {
        $this->campaign = $campaign;
        $this->campaignRecipient = $recipient;
        $this->template = $campaign->template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Parsing body and subject
        $content = $this->parseTemplate($this->template->content);
        $subject = $this->parseTemplate($this->template->subject);

        $email = $this->view('emails.campaigns.irma', compact('content'));
        $email->subject($subject);

        $email->to($this->campaignRecipient->email);

        return $email;
    }

    private function parseTemplate($content)
    {
        $replace_data = [];
        $recipient = $this->campaignRecipient;

        $replace_data['%%name%%'] = $recipient->name;

        if ($recipient->additional_attributes) {
            foreach ($recipient->additional_attributes as $key => $attribute) {
                $key = "%%$key%%";
                $replace_data[$key] = $attribute;
            }
        }

        return strtr($content, $replace_data);
    }
}
