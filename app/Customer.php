<?php

namespace App;

use App\Traits\HasClient;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Customer extends People
{
    use Notifiable;
    use HasClient;

    protected $table = 'peoples';

    public static function getLeadSources() {
        $lead_sources = DB::table('lead_sources')->get();
        return $lead_sources;
    }

    public function project()
    {
        return $this->hasOne(Project::class)->first();
    }

}
