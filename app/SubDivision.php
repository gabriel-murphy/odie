<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDivision extends Model
{
    protected $table = "subdivisions";

    public function phones()
    {
        return $this->morphMany(Phones::class, 'phoneable');
    }
}
