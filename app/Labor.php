<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Labor extends Model
{
    /************* Scopes *************/

    public function scopeCriteria(Builder $builder, $criteria)
    {
        if (is_array($criteria)){
            foreach ($criteria as $condition => $value)
            {
                $builder->where($condition, $value);
            }
        }
        return $builder;
    }

    /************* Function *************/

    public static function getLabor($criteria)
    {
        if (self::criteria($criteria)->count()) {
            return self::criteria($criteria)->first();
        }
        unset($criteria['pitch_id']);

        return self::criteria($criteria)->first();
    }
}
