<?php

namespace App;

use App\Traits\HasClient;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Auth;

class Appointment extends Model
{
    use SoftDeletes;
    use HasClient;

    protected $fillable = [
        'estimator_id', 'created_by', 'description', 'project_id', 'customer_id', 'appointment_type_id', 'title',
        'notes','date', 'start_time', 'end_time'
    ];

    public function scopeCancelled($q, $cancelled = true)
    {
        return $q->where('cancelled', boolval($cancelled));
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function estimator()
    {
        return $this->belongsTo(User::class, 'estimator_id');
    }

    public function attendees()
    {
        return $this->belongsToMany(User::class, 'appointment_user', 'appointment_id', 'user_id');
    }

    public function getStartTimeStampAttribute()
    {
        $datetime = "$this->date $this->start_time";
        return Carbon::createFromTimeString($datetime)->toDayDateTimeString();
    }

    public function getEndTimeStampAttribute()
    {
        $datetime = "$this->date $this->end_time";
        return Carbon::createFromTimeString($datetime)->toDayDateTimeString();
    }
}
