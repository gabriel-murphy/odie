<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoofComponent extends Model
{
    public function materials()
    {
        return $this->hasMany(Material::class);
    }
}
