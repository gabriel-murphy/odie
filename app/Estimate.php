<?php

namespace App;

use App\Traits\HasClient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Estimate extends Model
{
    use SoftDeletes;
    use HasClient;

    protected $fillable = [
        'user_id', 'client_id', 'structure_id', 'roof_product_id', 'name', 'financing', 'insurance',
    ];

    public static function getRoofManufacturers() {
        $roof_manufacturers = DB::table('roof_manufacturers')->get();
        return $roof_manufacturers;
    }
    public static function getRoofProductLines() {
        $roof_manufacturers_lines = DB::table('roof_manufacturers_products')->get();
        return $roof_manufacturers_lines;
    }
    public static function getRoofWarranties() {
        $roof_warranties = DB::table('roof_warranties')->get();
        return $roof_warranties;
    }
    public static function getRoofComponents() {
        $roof_components = DB::table('roof_components')->get();
        return $roof_components;
    }

    /************* Relationships **************/
    public function buildSheets()
    {
        return $this->hasMany(BuildSheet::class);
    }

    public function structure()
    {
        return $this->belongsTo(Structure::class);
    }
}
