<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplyHouses extends Model
{
    public function address()
    {
        return $this->morphOne(Address::class, 'addressable')->withDefault();
    }
    public function emails()
    {
        return $this->morphMany(Emails::class, 'emailable');
    }
    public function phones()
    {
        return $this->morphMany(Phones::class, 'phoneable');
    }
}
