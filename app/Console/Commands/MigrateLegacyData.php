<?php

namespace App\Console\Commands;

use App\MaximusClaim;
use File;
use Illuminate\Console\Command;
use Log;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\OutputStyle;

class MigrateLegacyData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'legacy:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate all the legacy data mainly includes documents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (MaximusClaim::all() as $claim)
        {

            $dir = storage_path("app/clients/1/legacy/maximus/claims/$claim->id");
            $dir_exists = is_dir($dir);

            // Remove claim data from storage folder if exists
            if ($dir_exists) {
                File::deleteDirectory(public_path("storage/maximus/$claim->id"));
            }

            // copy legacy files to storage
            dump('Migrating files for claim ID '. $claim->id);
            File::copyDirectory($dir, public_path("storage/maximus/$claim->id"));

            dump('Migration complete for claim ID '. $claim->id);
        }
    }
}
