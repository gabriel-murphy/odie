<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuildSheet extends Model
{
    use SoftDeletes;

    protected $casts = [
        'additional_data' => 'json'
    ];

    protected $fillable = [
        'estimate_id', 'type', 'name', 'unit', 'unit_price', 'actual_quantity', 'adjusted_quantity',
        'cost', 'waste_factor', 'waste_factor_cost', 'total_cost', 'additional_data'
    ];

    public function getAdditionalQuantityAttribute()
    {
        return $this->adjusted_quantity - $this->actual_quantity;
    }

}
