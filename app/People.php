<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class People extends Model
{
    use SoftDeletes;

    protected $fillable = ['associate_type_id','first_name','middle_name','last_name','company','enhanced_data','insurance_carrier_id','carrier_relationship_id','client_relationship_id','title','relationship_id','customer_notes','client_id'];

    protected $casts = [
        'enhanced_data' => 'array',
    ];

    public function getNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

//    public function getPhoneAttribute()
//    {
//        if (!$this->phones) {
//            return;
//        }
//        $phone = $this->phones[0] ?? null;
//        return $phone['extension'] ? $phone['number'] . ' x ' . $phone['extension'] : $phone['number'];
//    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable')->withDefault();
    }
    public function emails()
    {
        return $this->morphMany(Emails::class, 'emailable');
    }
    public function phones()
    {
        return $this->morphMany(Phones::class, 'phoneable');
    }
}
