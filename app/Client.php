<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    public static function getTeams() {
    	$client_teams = DB::table('client_teams')->get();
    	return $client_teams;
    }

    public function getNameAttribute()
    {
        return $this->company_name;
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

}
