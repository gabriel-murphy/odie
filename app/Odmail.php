<?php

namespace App;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use App\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Odmail extends Model implements HasMedia
{
    use Searchable;
    use InteractsWithMedia;

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->height(150)
            ->sharpen(10)
            ->watermark(public_path('images/logo.png'))
            ->watermarkOpacity(10)
            ->watermarkPosition('center');
    }

    protected $fillable = [
        'mailable_type', 'mailable_id', 'template_id', 'from', 'subject', 'body', 'status', 'module', 'resent'
    ];

    protected $appends = ['resent', 'forwarded'];


    public function mailable()
    {
        return $this->morphTo();
    }

    public function recipients()
    {
        return $this->hasMany(OdmailRecipient::class);
    }

    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    public function getSentAtAttribute()
    {
        return $this->created_at->format('l F d, Y g:i A');
    }

    public function getResentAttribute()
    {
        return $this->template_id == 6;
    }

    public function getForwardedAttribute()
    {
        return $this->template_id == 9;
    }

    public function getRecipients($type, $stringify = false)
    {
        $recipients = $this->recipients->where('type', $type);

        if ($stringify){
            return implode(', ', $recipients->pluck('email')->toArray());
        }

        return $recipients;
    }

    public function getRecipientsHtmlAttribute()
    {
        $recipients = "";
        if ($to_string = $this->getRecipients('to', true)) $recipients .= "<strong>To:</strong> $to_string <br>";
        if ($cc_string = $this->getRecipients('cc', true)) $recipients .= "<strong>CC:</strong> $cc_string <br>";

        return $recipients;
    }

    public function getRecipientsHtml($include_bcc = true)
    {
        $recipients = "";

        if ($to_string = $this->getRecipients('to', true)) $recipients .= "<strong>To:</strong> $to_string <br>";
        if ($cc_string = $this->getRecipients('cc', true)) $recipients .= "<strong>CC:</strong> $cc_string <br>";

        if ($include_bcc){
            if ($bcc_string = $this->getRecipients('bcc', true)) $recipients .= "<strong>CC:</strong> $bcc_string <br>";
        }

        return $recipients;
    }
}
