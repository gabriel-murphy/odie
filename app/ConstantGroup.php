<?php

namespace App;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;

class ConstantGroup extends Model
{
    use Searchable;

    public $timestamps = false;

    public function scopeKey($q, $key)
    {
        return $q->where('key', $key)->first();
    }

    public function constants()
    {
        return $this->hasMany(Constant::class);
    }
}
