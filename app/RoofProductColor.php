<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoofProductColor extends Model
{
    protected $fillable = [
        'roof_product_id',
        'name',
        'palette_id',
        'image_url',
        'active',
        'description',
        'pairing',
    ];
    protected $table = 'roof_product_colors';

        public function  products()
    {
          return  $this->belongsToMany(RoofProduct::class, 'product_colors',
              'roof_product_color_id','roof_manufacturers_products_id');
    }

    public function  shingleProduct()
    {
          return  $this->belongsTo(RoofProduct::class, 'roof_product_id');
    }

}
