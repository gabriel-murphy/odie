<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use App\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MaximusInfoSubmission extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['notes'];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->height(150)
            ->sharpen(10)
            ->watermark(public_path('images/logo.png'))
            ->watermarkOpacity(10)
            ->watermarkPosition('center');
    }
}
