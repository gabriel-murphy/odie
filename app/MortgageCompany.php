<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MortgageCompany extends Model
{
    protected $fillable = ['name'];
}
