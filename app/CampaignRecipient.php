<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignRecipient extends Model
{
    protected $fillable = ['name', 'email', 'additional_attributes', 'status', 'delivered_at', 'opened_at', 'failed_at', 'sent_at'];

    protected $casts = [
        'additional_attributes' => 'array'
    ];

    public function getStatusTextAttribute()
    {
        $statuses = ['Pending', 'Sent', 'Delivered', 'Opened', 'Failed'];
        return $statuses[$this->status];
    }
}
