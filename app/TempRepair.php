<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use App\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class TempRepair extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('damage-photos');
        $this->addMediaCollection('documents');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10)
            ->watermark(public_path('images/logo.png'))
            ->watermarkOpacity(10)
            ->watermarkPosition('center');
    }

    protected $fillable = ['maximus_claim_id', 'invoice_number', 'invoice_amount'];

    public function maximusClaim()
    {
        return $this->belongsTo(MaximusClaim::class);
    }
}
