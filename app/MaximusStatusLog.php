<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaximusStatusLog extends Model
{
    protected $fillable = ['claim_status_id', 'user_id'];

    public function status()
    {
        // constant_group : claim statuses
        return $this->belongsTo(Constant::class, 'claim_status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault([
            'first_name' => 'Odie'
        ]);
    }
}
