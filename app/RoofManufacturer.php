<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoofManufacturer extends Model
{
    public function roofTypes()
    {
        return $this->belongsToMany(RoofType::class);
    }

    public function products()
    {
        return $this->hasMany(RoofProduct::class);
    }

    public function productLines()
    {
        return $this->products()->whereNull('roof_product_id');
    }

    public function series()
    {
        return $this->products()->whereNotNull('roof_product_id');
    }
}

