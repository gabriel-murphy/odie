<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WasteFactor extends Model
{
    public function getMaterialFactorAttribute($value)
    {
        return $this->overall_factor ?? $value;
    }

    public function getLaborFactorAttribute($value)
    {
        return $this->overall_factor ?? $value;
    }
}
