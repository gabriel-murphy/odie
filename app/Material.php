<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Material extends Model
{
    use SoftDeletes;

    protected $casts = [
        'properties' => 'json'
    ];

    protected $fillable = [
        'client_id', 'location_id', 'trade_type_id', 'roof_material_categories_id', 'code', 'giddy_up_code',
        'name', 'description', 'our_cost', 'unit_id', 'options', 'active'
    ];

    public function getUnitNameAttribute()
    {
        return app('constants')->constant($this->unit);
    }

    public function scopeBoots(Builder $builder)
    {
        return $builder->where('roof_material_category', 1);
    }

    public function scopeVents(Builder $builder)
    {
        return $builder->where('roof_material_category', 13);
    }

}
