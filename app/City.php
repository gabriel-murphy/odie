<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    protected $table = 'us_cities';

    use SoftDeletes;

    public $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = array(
        'name', 'country', 'state_code', 'state', 'us_zip_codes', 'latitude', 'longitude'
    );
}
