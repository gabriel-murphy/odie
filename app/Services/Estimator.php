<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 4/15/2020
 * Time: 12:28 AM
 */

namespace App\Services;


use App\Constant;
use App\DumpFees;
use App\Estimate;
use App\Labor;
use App\Material;
use App\PermitJurisdiction;
use App\RoofLayer;
use App\WasteFactor;

class Estimator
{
    private $estimate;
    private $measurements;
    private $structure;
    private $project;
    private $materialWasteFactor;
    private $laborWasteFactor;
    private $buildSheets = array();

    public function __construct(Estimate $estimate)
    {
        $this->estimate = $estimate;
        $this->structure = $estimate->structure;
        $this->measurements = $this->structure->measurements;
        $this->project = $this->structure->project;
        self::setWasteFactors();
    }

    public function generateBuildSheets()
    {
        // TODO: get any materials which are included for free!
        // TODO: estimate for new construction

        self::calculateMaterialsCost();
        self::calculatePenetrationsCost();
        self::calculateLabor();
        self::calculateDumpFee();
        self::calculateJurisdictionsCost();
        return $this->buildSheets;
    }

    private function setWasteFactors()
    {
        $waste_factor = WasteFactor::where('roof_type_id', $this->project->preferred_roof_type_id)->first();

        if (!$waste_factor){
            $this->materialWasteFactor = $this->laborWasteFactor = 0;
            return;
        }

        $this->materialWasteFactor = $waste_factor->material_factor;
        $this->laborWasteFactor = $waste_factor->labor_factor;
    }

    /*
     * the Program finds the correct line item based on tear-off and replacement material,
     * along with pitch and type of project (new, re-roof, etc.)
    */
    private function calculateLabor()
    {
        $criteria = [
            'project_type_id' => $this->project->project_type_id,
            'pitch_id' => $this->measurements['pitch'],
            'tearoff_roof_type_id' => 1,
            'new_roof_type_id' => 1,
        ];
        $labor = Labor::getLabor($criteria);

        if (!$labor) return;

        $this->addBuildSheet([
            'type' => 'labor',
            'name' => $labor->name,
            'unit' => Constant::getUnitName(1),
            'unit_price' => $labor->cost,
            'quantity' => $this->measurements['square'],
            'waste_factor' => $this->laborWasteFactor,
            'additional_data' => [
                'labor_id' => $labor->id,
                'unit_id' => 1,
            ]
        ]);
    }

    private function calculateDumpFee()
    {
        $dump_fees = DumpFees::where('roof_type_id', $this->project->current_roof_type_id)->first();
        if (!$dump_fees) return;

        $this->addBuildSheet([
            'type' => 'dump_fees',
            'name' => $dump_fees->name,
            'unit' => Constant::getUnitName(1),
            'unit_price' => $dump_fees->cost_per_dump,
            'quantity' => $this->measurements['square'] / $dump_fees->squares_per_dump,
            'waste_factor' => $this->laborWasteFactor,
            'additional_data' => [
                'dump_fees_id' => $dump_fees->id,
                'unit_id' => 1,
            ]
        ]);
    }

    /*
     * Now calculate the material items, quantities and total cost
     * - we look at roof_components to get a list of the components to determine quantities as driven by square feet (squares)
     * or lineal feet of something by looking at a location (table.column) in the dB and loop through each
    */
    private function calculateMaterialsCost()
    {
        $roof_layers = RoofLayer::all();

        foreach ($roof_layers as $layer) {
            $materials = $layer->materials()->wherePivot('roof_type_id', 1)->whereNotNull('coverage_unit')->get();

            foreach ($materials as $material) {
                $roof_component = $material->pivot->component ?? null;

                switch ($material->id) {
                    case 63: // Valley metal Roll
                        $measurement = $this->measurements['valleys'];
                        break;
                    case 66: // Drip Edge
                        $measurement = $this->measurements['drip_edge'];
                        break;
                    default:
                        // area in squares
                        $measurement = $roof_component ? $this->measurements[$roof_component] : $this->measurements['square'];

                }


                $quantity = $measurement / $material->coverage_value;

                $this->addBuildSheet([
                    'type' => 'material',
                    'name' => $material->name,
                    'unit' => Constant::getUnitName($material->coverage_unit),
                    'unit_price' => $material->cost,
                    'quantity' => $quantity,
                    'waste_factor' => $this->materialWasteFactor,
                    'additional_data' => [
                        'material_id' => $material->id,
                        'unit_id' => $material->coverage_unit,
                    ]
                ]);
            }
        }
    }

    private function calculatePenetrationsCost()
    {
        // return, if not a re-roof
        if ($this->project->project_type_id !== 1) return;

        foreach ($this->structure->penetrations as $type => $penetrations) {
            foreach ($penetrations as $penetration) {
                $material = Material::find($penetration['material_id']);

                $this->addBuildSheet([
                    'type' => $type,
                    'name' => $material->name,
                    'unit' => Constant::getUnitName($material->unit),
                    'unit_price' => $material->cost,
                    'additional_data' => [
                        'material_id' => $material->id,
                        'unit_id' => $material->unit,
                    ]
                ]);
            }
        }
    }

    private function calculateJurisdictionsCost()
    {
        $permit = PermitJurisdiction::getJurisdictionDetails($this->project->property->address);
        if (!$permit) return;
        $permit_fee = $permit->getFee($this->measurements['square']);

        $this->addBuildSheet([
            'type' => 'other',
            'name' => $permit->name,
            'unit_price' => $permit_fee,
            'additional_data' => [
                'permit_jurisdiction_id' => $permit->id,
                'unit_id' => 8,
            ]
        ]);
    }

    private function addBuildSheet($data)
    {
        $unit_price = $data['unit_price'];
        $quantity = round($data['quantity'] ?? 1, 2);
        $waste_factor = $data['waste_factor'] ?? 0;
        $adjusted_quantity = ceil((1 + $waste_factor) * $quantity);
        $actual_cost = $unit_price * $quantity;
        $cost = $unit_price * $adjusted_quantity;
        $waste_factor_cost = $unit_price * $quantity * $waste_factor;

        $this->buildSheets[] = [
            'type' => $data['type'],
            'name' => $data['name'] ?? $data['type'],
            'unit' => $data['unit'] ?? Constant::getUnitName(8), // Default unit will be each
            'unit_price' => $unit_price,
            'actual_quantity' => $quantity,
            'waste_factor' => $waste_factor ,
            'adjusted_quantity' => $adjusted_quantity,
            'waste_factor_cost' => round($waste_factor_cost, 2),
            'actual_cost' => round($actual_cost, 2),
            'cost' => round($cost, 2),
            'additional_data' => $data['additional_data'] ?? []
        ];
    }

}
