<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 4/3/2020
 * Time: 9:55 PM
 */

namespace App\Services\Datalab;

use App\Constant;

class Datalab
{
    private $person;
    private $property;

    public function __construct(Person $person, Property $property)
    {
        $this->person = $person;
        $this->property = $property;
    }

    public function getPersonData($inputs)
    {
        return $this->person->getData($inputs);
    }

    public function getPropertyData($inputs)
    {
        $inputs['street_address'] = $inputs['street'];
        return $this->property->getData($inputs);
    }

}
