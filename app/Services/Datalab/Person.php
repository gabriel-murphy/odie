<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 4/3/2020
 * Time: 11:15 PM
 */

namespace App\Services\Datalab;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Person
{
    private $url;
    private $apiKey;
    private $client;


    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->url = config('services.datalab.person.url');
        $this->apiKey = config('services.datalab.person.api_key');
    }

    public function getData($inputs = array())
    {
        //$this->client->setDefaultOption('verify', false);
        $data = [
            'pretty' => true,
            'api_key' => $this->apiKey,
        ];

        $data = array_merge($data, $inputs);

        $result = array();

        try {
            $response = $this->client->request('GET', $this->url, [
                'verify' =>false,
                'query' => $data
            ]);
            $response = json_decode($response->getBody(), true);
            $result = $response['data'];
        } catch (ClientException $exception) {
            //dd($exception);
        }

        return $result;
    }
}
