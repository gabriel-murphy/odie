<?php
/**
 * Created by PhpStorm.
 * User: Shehzad
 * Date: 4/3/2020
 * Time: 11:14 PM
 */

namespace App\Services\Datalab;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Property
{
    private $url;
    private $token;
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->url = config('services.datalab.property.url');
        $this->token = config('services.datalab.property.api_key');
    }

    public function getData($inputs = array())
    {
        $data = [
            'token' => $this->token,
        ];

        $data = array_merge($data, $inputs);

        $result = array();

        try {
            $response = $this->client->request('GET', $this->url, [
                'verify' =>false,
                'query' => $data
            ]);
            $response = json_decode($response->getBody(), true);
            $result = $response['data'];
        } catch (ClientException $exception) {
            //dd($exception);
        }

        return $result;
    }

}
