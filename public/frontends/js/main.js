$(document).ready(function () {
    /*Feedback Carousel*/

    $('.feedback-slider.owl-carousel').owlCarousel({
        loop:true,
        margin:40,
        nav:true,
        autoplay: 1000,
        smartSpeed: 1000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });

    /*Showcase Carousel*/

    $('.showcase-slider.owl-carousel').owlCarousel({
        loop:true,
        margin:40,
        nav:true,
        dotsEach:0,
        center: true,
        autoplay: 1000,
        smartSpeed: 1000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    });

    /*Showcase Carousel*/

    $('.team-slider.owl-carousel').owlCarousel({
        loop:true,
        margin:40,
        nav:true,
        dots: false,
        autoplay: 1000,
        smartSpeed: 1000,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
});
var gallery_init = {
    group : ['houses', 'cars', 'mountains'],
    //set_svg_color : '#ff6666',
    //set_image_hover_transparency : false
};
$(gallery.construct(gallery_init));
