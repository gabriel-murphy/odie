<?php

return [
    'recipients' =>[
        1 => "Customer Rep",
        2 => "Job Rep / Estimator",
        3 => "Labor / Sub Contractor",
        4 => "Customer",
        5 => "Any",
    ],

    'mail_statuses' => [
        0 => "Pending",
        1 => 'Delivered',
        2 => 'Opened',
        3 => 'Failed',
    ],


];
