<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'datalab' => [
        'property' => [
            'url' => env('ESTATED_URL', 'https://apis.estated.com/v4/property'),
            'api_key' => env('ESTATED_API_KEY'),
        ],
        'person' => [
            'url' => env('PEOPLE_DATALABS_URL', 'https://api.peopledatalabs.com/v4/person'),
            'api_key' => env('PEOPLE_DATALABS_API_KEY'),
        ],
    ],

];
